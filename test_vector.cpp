#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <random>
#include <vector>
#include <typeinfo>

#include "efield.h"
//#include <mpi.h>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>

namespace champion{
    
int main(int argc, char *argv[])
{
    std::vector<double> w{1,2,3};
    std::vector<double> e{1,2,3};
    std::vector<double> k{1,2,3};
    EMWave<double>* a = new EMWave<double>(w,e,k);
    return 0;
}
}
