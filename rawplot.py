import scipy
import matplotlib.pyplot as plt
from scipy.fftpack import fft, ifft
import math
import numpy as np
import csv
from scipy import optimize

def test_func(x, a, b,c,d):
    return np.add(d,np.multiply(a,np.sin(np.add(np.multiply(b,x),c))))

read=0
if (read):
    track=0
    p=0
    worklist = [[np.zeros(2048) for n in range(2048)] for m in range(10)]
    mlist = [np.zeros([2048,2048]) for n in range(5)]


    t=np.array([])
    for i in range(7):
        p=0
        print(i)
        with open('green-raw_' + str(i) + '.csv') as infile:
            reader = csv.reader(infile,dialect='excel')
            for row in reader:
                t=np.append(t,row)
                if np.shape(t) == (2048,):
                    worklist[i][p] = t.astype(float)
                    p=p+1
                    t = np.array([])
    i=7
    p=0
    print(i)
    with open('green_dataRetardance.csv') as infile:
        reader = csv.reader(infile,dialect='excel')
        for row in reader:
            t=np.append(t,row)
            if np.shape(t) == (2048,):
                worklist[i][p] = t.astype(float)
                p=p+1
                t = np.array([])
    i=8

    p=0
    print(i)
    with open('green_dataIntensity.csv') as infile:
        reader = csv.reader(infile,dialect='excel')
        for row in reader:
            t=np.append(t,row)
            if np.shape(t) == (2048,):
                worklist[i][p] = t.astype(float)
                p=p+1
                t = np.array([])

    i=9
    p=0
    print(i)
    with open('green_dataAngleOfFastAxis.csv') as infile:
        reader = csv.reader(infile,dialect='excel')
        for row in reader:
            t=np.append(t,row)
            if np.shape(t) == (2048,):
                worklist[i][p] = t.astype(float)
                p=p+1
                t = np.array([])
    i=0
    ind=754

    #calib_matrix = np.array([[ 0.5, 0.071481788530538, 0.114316805401975, 0.254317746228415, 0.406716072772049],  [0.5, 0.12820083692159, 0.00263234687614001, 0.456112648840151, 0.00936535778683456],  [0.5, 0.0630607888871788, -0.119169270341662, 0.224357532664725,-0.42398016160478],  [0.5, 0.235943018963816, 0.37733040455917, 0.00100713987175113, 0.00161066217141947],  [0.5 ,0.42315802554444, 0.00868870074020474, 0.00180628069204559, 3.70883486513164E-05],  [0.5, 0.208147462649517, -0.393347144638314, 0.000888492525690304, -0.00167903078694376],  [0.5, 0.109582487198283,0.00225005643319226, -0.460940214690379, -0.00946448216223144 ] ])

    calib_matrix=np.linalg.pinv(np.array([[ 0.5, 0.0678314144879769, 0.115351896910323, 0.243315488987608, 0.413774405476362],  [0.5, 0.128877940997123, 0.00257915549035069, 0.462293164164952, 0.00925159063903916],  [0.5, 0.0594861915669235, -0.119868903077957, 0.213380657006379, -0.429977186632177],  [0.5, 0.223892455588707, 0.380744226712908, 0.000942678807722846, 0.00160308891490485],  [0.5, 0.425389900813242, 0.00851306817701797, 0.00179106546248535, 3.58434987819192E-05],  [0.5, 0.196347217643522, -0.39565359592502, 0.00082670209025672, -0.00166586345706538],  [0.5, 0.109257083564063, 0.00218649525864358, -0.46731935868352, -0.00935217679899671 ]]))

    print([worklist[k][5][8] for k in range(7)])
    tempvar=[]
    for i in range(2048):
        print(i)
        for j in range(2048):
            tempvar= np.matmul((calib_matrix),np.transpose([worklist[0][i][j],worklist[1][i][j],worklist[2][i][j],worklist[3][i][j],worklist[4][i][j],worklist[5][i][j],worklist[6][i][j]]))
            for p in range(5):
                mlist[p][i][j] = tempvar[p]
    ind=645
    for p in range(5):
        np.savetxt('calibmats'+str(p),mlist[p],fmt='%f')
        
    for p in range(3):
        np.savetxt('outputmats'+str(p),worklist[p+7],fmt='%f')
else:
    mlist=[[] for a in range(5)]
    worklist=[[] for a in range(10)]
    for p in range(5):
        mlist[p] = np.loadtxt('calibmats'+str(p),dtype=float)
    for p in range(3):
        worklist[p+7] = np.loadtxt('outputmats'+str(p),dtype=float)
    
#for i in range(10):
    #print(str(i)+': '+str(worklist[i][ind][ind]) + ' : '+ str(float(worklist[i][ind][ind])*float(535)/(math.pi*2.0)) + ' : ' + str(float(worklist[i][ind][ind]+math.pi/2.0)*float(360)/(math.pi*2.0))+ ' : '+ str(float(worklist[i][ind][ind])/(float(530)/(math.pi*2.0))) + ' : ' + str(float(worklist[i][ind][ind]+math.pi/2.0)/(float(360)/(math.pi*2.0))))
#print('Modified by matrix: '+str(np.matmul(np.transpose(calib_matrix),np.array([worklist[i][ind][ind] for i in range(7)]))))
#modmat=np.matmul(np.transpose(calib_matrix),np.array([worklist[i][ind][ind] for i in range(7)]))
#print('Modified by matrix (Normed): '+str(np.divide(modmat,169.513403285071/0.0729)))

#print(np.transpose(calib_matrix))

#ip = np.divide(mlist[1]-mlist[2],mlist[1]+mlist[2])
#ipp = np.divide(mlist[3]-mlist[4],mlist[3]+mlist[4])

#delmat = np.arctan(np.divide(ip,ipp))
#delmat2=np.arctan(np.divide(ipp,ip))
#psimat = 0.5*np.arcsin(np.sqrt(np.square(ip) + np.square(ipp)))

#print((np.square(ip) + np.square(ipp))[1:10][1:50])
delmat3 = -np.multiply(1,0.5*np.arctan2(mlist[3],mlist[2]))+math.pi/4
for i in range(2048):
    for j in range(2048):
        if delmat3[i][j]>math.pi/2:
            delmat3[i][j] = (delmat3[i][j]-math.pi)
delmat4=np.multiply(np.sign(mlist[4]),np.arctan(np.sqrt(np.square(np.divide(mlist[3],mlist[4])) + np.square(np.divide(mlist[2],mlist[4])))))
for i in range(2048):
    for j in range(2048):
        if delmat4[i][j]<0:
            delmat4[i][j] = math.pi + delmat4[i][j]
delmat6=(np.arccos(np.divide(mlist[4],mlist[0])))
print("THMAX: " + str(np.amax(delmat3)) + " THMIN: " + str(np.amin(delmat3)) + "D1MAX: " + str(np.amax(delmat4)) + "D1MIN: " + str(np.amin(delmat4)) +"D2MAX: " + str(np.amax(delmat6)) + "D2MIN: " + str(np.amin(delmat6)) + "00MAX: " + str(np.amax(np.divide(mlist[0],np.amax(mlist[0])/math.pi)-worklist[8])))
print(np.amax(mlist[0]))

print("THMAX: " + str(np.amax(worklist[9])) + " THMIN: " + str(np.amin(worklist[9])) + "DMAX: " + str(np.amax(worklist[7])) + "DMIN: " + str(np.amin(worklist[7])) + "IMAX: " + str(np.amax(worklist[8])) + "IMIN: " + str(np.amin(worklist[8])))
for j in range(5):
    print([mlist[0][j*100][j*100], mlist[1][j*100][j*100], mlist[2][j*100][j*100], mlist[3][j*100][j*100], mlist[4][j*100][j*100], worklist[9][j*100][j*100], worklist[7][j*100][j*100]])

calib_matrix=np.linalg.pinv(np.array([[ 0.5, 0.0678314144879769, 0.115351896910323, 0.243315488987608, 0.413774405476362],  [0.5, 0.128877940997123, 0.00257915549035069, 0.462293164164952, 0.00925159063903916],  [0.5, 0.0594861915669235, -0.119868903077957, 0.213380657006379, -0.429977186632177],  [0.5, 0.223892455588707, 0.380744226712908, 0.000942678807722846, 0.00160308891490485],  [0.5, 0.425389900813242, 0.00851306817701797, 0.00179106546248535, 3.58434987819192E-05],  [0.5, 0.196347217643522, -0.39565359592502, 0.00082670209025672, -0.00166586345706538],  [0.5, 0.109257083564063, 0.00218649525864358, -0.46731935868352, -0.00935217679899671 ]]))
    
print(calib_matrix)
print[np.sum(calib_matrix[0])]
#ip = np.divide(mlist[1]-mlist[4],mlist[1]+mlist[4])
#ipp = np.divide(mlist[2]-mlist[3],mlist[2]+mlist[3])

#delmat5 = np.arctan(np.divide(ip,ipp))
#delmat6=np.arctan(np.divide(ipp,ip))

#psimat3 = 0.5*np.arcsin(np.sqrt(np.square(ip) + np.square(ipp)))

#print((np.square(ip) + np.square(ipp))[1:10][1:50])

plt.subplot(333)
plt.imshow(delmat3,cmap='Spectral')
plt.subplot(332)
plt.imshow(delmat4,norm=plt.Normalize(0,math.pi),cmap='inferno')

plt.subplot(338)
plt.imshow(np.abs(delmat4-worklist[7]),norm=plt.Normalize(0,math.pi),cmap='Greys_r')

plt.subplot(331)
plt.imshow(np.divide(mlist[0],2*4096),norm=plt.Normalize(0,1),cmap='RdYlBu')

plt.subplot(337)
plt.imshow(np.abs(np.divide(mlist[0],2*4096)-worklist[8]),norm=plt.Normalize(0,1),cmap='Greys_r')
plt.subplot(339)
plt.imshow(np.abs(worklist[9]-delmat3),norm=plt.Normalize(0,math.pi/2),cmap='Greys_r')
plt.subplot(335)
plt.imshow(worklist[7],cmap='inferno')
plt.subplot(334)
plt.imshow(worklist[8],norm=plt.Normalize(0,1),cmap='RdYlBu')
plt.subplot(336)
plt.imshow(worklist[9],cmap='Spectral')

print("Errors - Intensity:" +  str(np.mean(np.divide(np.abs(np.divide(mlist[0],2*4096)-worklist[8]),np.abs(np.divide(mlist[0],2*4096))))) +", Delta/Retardance: "  + str(np.mean(np.divide(np.abs(delmat4-worklist[7]),np.abs(delmat4)))) + "Angle:" + str(np.mean(np.divide(np.abs(worklist[9]-delmat3),np.abs(worklist[9]))))) #Good average statistics, <1%.
plt.show()

