import matplotlib.pyplot as plt
import math
import numpy as np
import csv
worklist = [[np.zeros(2048) for n in range(2048)] for m in range(3)]

filenames=["APFA.csv", "APR.csv", "API.csv"]
t=np.array([])
for i in range(3):
    p=0
    print(i)
    with open(filenames[i]) as infile:
        reader = csv.reader(infile,dialect='excel')
        for row in reader:
            t=np.append(t,row)
            if np.shape(t) == (2048,):
                worklist[i][p] = t.astype(float)
                p=p+1
                t = np.array([])
                
fig,(ax1,ax2,ax3) = plt.subplots(ncols=3,nrows=1)
pos1 = ax1.imshow(worklist[0],cmap="RdBu",norm=plt.Normalize(-math.pi/2,math.pi/2))
pos2 = ax2.imshow(worklist[1],cmap="Greys_r",norm=plt.Normalize(0,math.pi))
pos3 = ax3.imshow(worklist[2],cmap="PuBuGn",norm=plt.Normalize(0,1))


fig.colorbar(pos1, ax=ax1)
fig.colorbar(pos2, ax=ax2)
fig.colorbar(pos3, ax=ax3)

for i in range(2048):
    for j in range(2048):
        worklist[0][i][j] = worklist[0][i][j] - math.pi/2.0
        if worklist[0][i][j] < -math.pi/2.0:
            worklist[0][i][j] = worklist[0][i][j] + math.pi


fig2,(ax4) = plt.subplots(ncols=1,nrows=1)
posx = ax4.imshow(worklist[0],cmap="hsv",norm=plt.Normalize(-math.pi/2,math.pi/2))

fig.colorbar(posx, ax=ax4)

# We'll also create a grey background into which the pixels will fade
greys =np.full((2048,2048, 3),180, dtype=np.uint8)
for i in range(2048):
    for j in range(2048):
        for k in range(3):
            greys[i][j][k] = np.multiply(greys[i][j][k],np.divide(worklist[1][2047-i][j],math.pi))
# First we'll plot these blobs using only ``imshow``.
vmax = 1
vmin = -1
cmap = plt.cm.get_cmap(name="hsv")

# Create an alpha channel of linearly increasing values moving to the right.
alphas = np.subtract(1,worklist[2])
# Normalize the colors b/w 0 and 1, we'll then pass an MxNx4 array to imshow
colors = (worklist[0])
print(colors[205][205])
colors = cmap(colors)
print(colors[205][205])

# Now set the alpha channel to the one we created above
colors[..., -1] = alphas
figk, axk = plt.subplots()
axk.imshow(greys)
akx = axk.imshow(worklist[0], extent=(0, 2048, 0, 2048),cmap=cmap)
fig.colorbar(akx, ax=axk)
# Create the figure and image
# Note that the absolute values may be slightly different
figk, axk = plt.subplots()
axk.imshow(greys)
akx = axk.imshow(colors, extent=(0, 2048, 0, 2048),cmap=cmap,norm=plt.Normalize(-math.pi/2, math.pi/2))
fig.colorbar(akx, ax=axk)

plt.show()
