#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <random>
#include <vector>
#include <typeinfo>

#include "efield.h"
//#include <mpi.h>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
    std::vector<double> w{1,2,3};
    std::vector<double> e{1,2,3};
    std::vector<double> k{1,2,3};
    champion::EMWave<double>* a = new champion::EMWave<double>(w,e,k);
    std::cout << a->w[1] << std::endl;
    return 0;
}
