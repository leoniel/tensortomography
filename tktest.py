import tkFileDialog
from Tkinter import *
import ttk
import tkMessageBox
import matplotlib.pyplot as plt
import math
import numpy as np
import csv
import time
import os
import threading as thr
import time



global finished
global globlist
global outPut
global PrOP
globlist = [[np.zeros(2048) for n in range(2048)] for n in range(7)]
outPut = np.array([[np.zeros(2048) for n in range(2048)] for n in range(5)])
PrOP = np.array([[np.zeros(2048) for n in range(2048)] for n in range(3)])
POLLING_DELAY = 2000 # ms
lock = thr.Lock()  # Lock for shared resources.
finished = 0

def plotCombinedMethod():
    indAlpha = [0,0,1,0]
    indChrom = [1,2,2,2]
    print("Creating grayscale...")
    if csclv.get()==3:
        greys =np.full((2048,2048, 3),int(tvar.get()), dtype=np.uint8)
        for i in range(2048):
            for j in range(2048):
                for k in range(3):
                    greys[i][j][k] = np.multiply(greys[i][j][k],PrOP[0][2047-i][j])
    else:
        greys =np.full((2048,2048, 3),int(tvar.get()), dtype=np.uint8)
    
    if csclv.get()!=0:
        print("Normalizing angle...")
        for i in range(2048):
            for j in range(2048):
                PrOP[2][i][j] = PrOP[2][i][j] - math.pi/2.0
                if PrOP[2][i][j] < -math.pi/2.0:
                    PrOP[2][i][j] = PrOP[2][i][j] + math.pi
    print("Plotting...")
    cmap = plt.cm.get_cmap(name="hsv")
    if csclv.get()>1:
        print(csclv.get())
        alphas = np.add(float(avar.get()),np.multiply((1.0-float(avar.get())),np.divide(PrOP[1],math.pi)))
    else:
        alphas = np.add(float(avar.get()),np.multiply((1.0-float(avar.get())),PrOP[0]))
    print(np.shape(PrOP[indChrom[csclv.get()]]))
    print(PrOP[indChrom[csclv.get()]][205][205])
    colors = (PrOP[indChrom[csclv.get()]])
    print(np.shape(colors))
    print(colors[205][205])
    colors = cmap(colors)
    colors[..., -1] = alphas
    print(colors[205][205])
    figk, axk = plt.subplots()
    axk.imshow(greys)
    akx = axk.imshow(colors, extent=(0, 2048, 0, 2048),cmap=cmap,norm=plt.Normalize(0, 1))
    figk.colorbar(akx, ax=axk)

    plt.show()

def plotMethod(scl,mat,lab):
    fig,(ax) = plt.subplots(ncols=1,nrows=1)
    pos = ax.imshow(mat,cmap=scl,norm=plt.Normalize(np.amin(mat),np.amax(mat)))
    ax.set_title(lab)
    fig.colorbar(pos, ax=ax)
    plt.show()

def chooseCalib():
    if rVar.get()== 1:
        if cvar.get()==0:
            cm = ("0.5 0.0635648533981375 0.13711113346642 0.200098617382466 0.431618209870321 0.5 0.151121607468367 0.00148770430897562 0.475722401523261 0.00468321028659341 0.5 0.0553273351844126 -0.140637262094243 0.174167368947029 -0.442718266354692 0.5 0.192492274003726 0.415211118428425 -0.00234612797712189 -0.00506066244164242 0.5 0.457638778626314 0.0045051875395067 -0.00557777784852965 -5.49099779891061e-05 0.5 0.167546749419955 -0.425889228763203 -0.00204208775819427 0.00519080903338942 0.5 0.159782212430189 0.0015729629264287 -0.472883905568582 -0.00465526694524383".split())

        if cvar.get()==1:
            cm = ("0.5 0.0600142572159182 0.14278413723057 0.183792211258531 0.437272966985707 0.5 0.154869519031824 0.00211020901368058 0.47428415646298 0.00646246406827422 0.5 0.0513660938279309 -0.146118257992342 0.157307420041592 -0.447483631181792 0.5 0.175801544580146 0.418261810295632 -0.000844790939971299 -0.00200990149840612 0.5 0.453663877838639 0.0061814978840327 -0.00218002142532716 -2.97043659107436e-05 0.5 0.150468222934262 -0.42802854918282 -0.000723055032263933 0.00205683426310205 0.5 0.173521598725619 0.00236435706644006 -0.467782303825887 -0.00637387163171091".split())

        if cvar.get()==2:
            cm = ("0.5 0.0647069031908922 0.127582786825517 0.215940420746949 0.425770347653543 0.5 0.14134691251989 0.00304178941179066 0.471704103514003 0.0101510851704327 0.5 0.055570144241397 -0.131819231843123 0.185449152048818 -0.439908247544602 0.5 0.203104748609873 0.400462216043603 -0.00188271180511642 -0.00371214827230151 0.5 0.443665632543743 0.00954769651057685 -0.00411262922029954 -8.85038929673654e-05 0.5 0.174425905425682 -0.413759747804042 -0.00161686870204023 0.00383541186015774 0.5 -0.0341207814626758 -0.000734280147510016 -0.4912427945764 -0.0105715583348924".split())
        if cvar.get()==3:
            cm = ("0.5 0.0570142340675644 0.135363276550162 0.185379998987274 0.44012946030365 0.5 0.144405618721873 0.00407145375284715 0.469530353081547 0.0132382045452914 0.5 0.0479006115689778 -0.138850174418416 0.155747340455788 -0.451466999671795 0.5 0.187675386869818 0.445579173516783 -0.00190551798495513 -0.0045240835413683 0.5 0.475344110168815 0.0134021209033985 -0.0048262948385313 -0.000136075288528289 0.5 0.157675814724715 -0.457057095076242 -0.00160092437139244 0.00464062192355169 0.5 -0.0188469489859409 -0.000531381547738245 -0.490873229762278 -0.0138399576912384".split())
    
    if rVar.get()== 0:
        if cvar.get()==3:
            cm = ("0.5 0.03915811209265 0.157274971779476 0.114039705512558 0.458030035354892 0.5 0.161904337801833 0.00724725093776236 0.47151208312702 0.0211060829685187 0.5 0.0382501526773297 -0.157498254946068 0.111395466073856 -0.458680300273276 0.5 0.115658051351864 0.464529973237599 -0.00232749094998717 -0.00934815428818709 0.5 0.478203345740174 0.0214056009425668 -0.00962331585613299 -0.000430764152521388 0.5 0.112976287317018 -0.465189466113215 -0.00227352340126792 0.00936142585624167 0.5 -0.154808384380711 -0.00692961797138382 -0.473889252103554 -0.0212124911125395".split())

        if cvar.get()==2:
            cm = ("0.5 0.0440758779017302 0.143568537629763 0.139462210927367 0.45427083090884 0.5 0.150030210330752 0.00674862816161821 0.474716462489408 0.0213535985884247 0.5 0.0426147977696061 -0.144008982431696 0.134839150077112 -0.455664459550928 0.5 0.128997663077466 0.420184616333227 -0.000527098406238488 -0.0017169197977034 0.5 0.439096109823099 0.0197513311877414 -0.00179419420593911 -8.07060759224376e-05 0.5 0.124721493626385 -0.421473674041632 -0.000509625515267462 0.00172218702694958 0.5 0.0837411132992872 0.0037668255896659 -0.490766960699009 -0.0220755788081853".split())

        if cvar.get()==1:
            cm = ("0.5 0.046831754439431 0.151242576878561 0.139955384955457 0.451984200080965 0.5 0.158224341362577 0.00570859291406669 0.472849007469134 0.0170599698517702 0.5 0.0452064564399754 -0.151736306073673 0.135098227458287 -0.453459695936132 0.5 0.134341252838866 0.433853429230856 -0.000429000707703864 -0.00138545252665636 0.5 0.453881271429713 0.0163756308770078 -0.00144940874483599 -5.22933729355635e-05 0.5 0.12967893403196 -0.435269737447969 -0.000414112220173515 0.00138997531630308 0.5 0.18833529895075 0.00679496936944223 -0.461682727891458 -0.0166571004580684".split())

        if cvar.get()==0:
            cm = ("0.5 0.0538097637025962 0.145775030119081 0.164244295546785 0.444951166530854 0.5 0.155314053400712 0.00483682657735166 0.474067260921366 0.0147635135190301 0.5 0.0524197249905055 -0.146280629298167 0.160001460913429 -0.446494413987897 0.5 0.158642237280644 0.429774734666385 -0.00117082336027946 -0.00317185579093386 0.5 0.457897734857095 0.014259958356452 -0.00337941127016087 -0.000105242416184606 0.5 0.154544117608319 -0.431265344909182 -0.00114057810953272 0.00318285690465523 0.5 0.188864238291601 0.00588165428226408 -0.461727758052058 -0.0143792338345923".split())
    if rVar.get()==2:
        if cvar.get()==3:
            cm = ("0.5 0.0682054992690202 0.109652441194078 0.254505035373642 0.409161999017588 0.5 0.126236246637187 0.00313657843641321 0.471043548689717 0.0117039683671685 0.5 0.0589535452265559 -0.114891807782702 0.219981882312841 -0.428712404677898 0.5 0.238072932657923 0.382744478494446 0.000860642132494793 0.00138363492436739 0.5 0.440630649533192 0.0109483023344376 0.00159289549476718 3.95785029533255e-05 0.5 0.20577876495426 -0.401032613357354 0.000743897565822194 -0.00144974718337996 0.5 0.082076694010384 0.00203935078412905 -0.480708883865872 -0.0119441219102421".split())

        if cvar.get()==2:
            cm = ("0.5 0.071481788530538 0.114316805401975 0.254317746228415 0.406716072772049 0.5 0.12820083692159 0.00263234687614001 0.456112648840151 0.00936535778683456 0.5 0.0630607888871788 -0.119169270341662 0.224357532664725 -0.42398016160478 0.5 0.235943018963816 0.37733040455917 0.00100713987175113 0.00161066217141947 0.5 0.42315802554444 0.00868870074020474 0.00180628069204559 3.70883486513164e-05 0.5 0.208147462649517 -0.393347144638314 0.000888492525690304 -0.00167903078694376 0.5 0.109582487198283 0.00225005643319226 -0.460940214690379 -0.00946448216223144".split())

        if cvar.get()==1:
            cm = ("0.5 0.0744763046089359 0.130242922123579 0.236079054795496 0.412851122383006 0.5 0.150032226194211 0.000519431283583023 0.475580338401264 0.00164652162997864 0.5 0.0655326193631056 -0.134964493519229 0.207728873213437 -0.427817817066469 0.5 0.222112784376259 0.388427141089615 0.000288753893727234 0.000504968004133288 0.5 0.447445340919433 0.00154911457133329 0.000581693596716748 2.01389967515032e-06 0.5 0.195439779546507 -0.402508416668819 0.000254078113926332 -0.000523274123538153 0.5 0.174054435361525 0.00060259932860107 -0.467323635932992 -0.00161793584097818".split())

        if cvar.get()==0:
            cm = ("0.5 0.0743584761747456 0.124895139665402 0.24410539436496 0.410008097135325 0.5 0.14535208792033 0.000865693182349383 0.477164548936963 0.00284191374739635 0.5 0.066280830726774 -0.129363172383949 0.217587949023778 -0.42467583839233 0.5 0.230427478065535 0.387034182735388 0.000227683284310298 0.000382424937361381 0.5 0.450427668425201 0.0026826732747792 0.00044506346097366 2.65072493550239e-06 0.5 0.205395880256706 -0.400880048925942 0.000202949791375823 -0.000396105911153422 0.5 0.177093590207712 0.00105474036096835 -0.466316519686315 -0.0027773046654177".split())
    return np.array(list(map(float,cm)))

def readFilesCallback(i):
    global finished
    global globlist
    print("Number " + str(i))
    PrOP = [np.zeros(2048) for n in range(2048)]
    t=np.array([])
    p=0
    with open('green-raw_' + str(i) + '.csv') as infile:
        reader = csv.reader(infile,dialect='excel')
        for row in reader:
            t=np.append(t,row)
            if np.shape(t) == (2048,):
                PrOP[p] = t.astype(float)
                p=p+1
                t = np.array([])
                
    print("Done " + str(i))
    with lock:
        finished = finished + 1
        globlist[i]=PrOP
    return

def readData(infiles):
    t=np.array([])
    ind = [1,2,0]
    for i in range(3):
        p=0
        print(i)
        with open(infiles[ind[i]]) as infile:
            reader = csv.reader(infile,dialect='excel')
            for row in reader:
                t=np.append(t,row)
                if np.shape(t) == (2048,):
                    PrOP[i][p] = t.astype(float)
                    p=p+1
                    t = np.array([])

def transForm(k,M,t,s):
    global outPut
    tempv = [0,0,0,0,0]
    op=np.array([[np.zeros(2048) for n in range(512)] for n in range(5)])
    for j in range(2048):
        for i in range(512):
            tempv= np.matmul((M),np.transpose([globlist[0][i][j],globlist[1][i][j],globlist[2][i][j],globlist[3][i][j],globlist[4][i][j],globlist[5][i][j],globlist[6][i][j]]))
            for p in range(5):
                op[p][i][j] = tempv[p]
    with lock:
        for q in range(5):
            outPut[q][s] = op[q]
    return
  
def readFiles(infiles):
    global finished
    global globlist
    global outPut
    global PrOP
    if iVar.get()== 0:
        readData(infiles)
        return
    if len(infiles) != 7:
        tkMessageBox.showinfo("Error!", "Select exactly 7 files!")
        return
    calib_matrix=np.linalg.pinv(np.reshape(chooseCalib(),(7,5)))
    threads=[[] for j in range(4)]
    start = time.time()
    print("Working...")
    tup = [range(0,512),range(512,1024),range(1024,1536),range(1536,2048)]
    sl = [slice(0,512,1),slice(512,1024,1),slice(1024,1536,1),slice(1536,2048,1)]
    for i in range(7):
        readFilesCallback(i)
    for i in range(4):
        threads[i] = thr.Thread(target=transForm,args=(i,calib_matrix,tup[i],sl[i]))    
        threads[i].start()
    for i in range(4):
        threads[i].join()
    PrOP[2] = -np.multiply(1,0.5*np.arctan2(outPut[3],outPut[2]))+math.pi/4
    for i in range(2048):
        for j in range(2048):
            if PrOP[2][i][j]>math.pi/2:
                PrOP[2][i][j] = (PrOP[2][i][j]-math.pi)
    PrOP[1]=np.multiply(np.sign(outPut[4]),np.arctan(np.sqrt(np.square(np.divide(outPut[3],outPut[4])) + np.square(np.divide(outPut[2],outPut[4])))))
    for i in range(2048):
        for j in range(2048):
            if PrOP[1][i][j]<0:
                PrOP[1][i][j] = math.pi + PrOP[1][i][j]
    PrOP[0] = np.divide(outPut[0],2*4096)
    print("Done!")
    
    end = time.time()
    print(end - start)
    print(np.amax(outPut))
    return

def enablePlots():  
    if datVar[0]:
        pButton[0].configure(state="normal")
        for n in range(3):
            IntScale[n].configure(state="normal")
    if datVar[1]:
        pButton[1].configure(state="normal")
        for n in range(3):
            RetScale[n].configure(state="normal")
    if datVar[2]:
        pButton[2].configure(state="normal")
        for n in range(3):
            AngScale[n].configure(state="normal")
    if datVar[0].get()*datVar[1].get()*datVar[2].get():
        pButton[3].configure(state="normal")
        for n in range(4):
            cScale[n].configure(state="normal")
    if (datVar[0]==0):
        pButton[0].configure(state="disabled")
        for n in range(3):
            IntScale[n].configure(state="disabled")
    if (datVar[1]==0):
        pButton[1].configure(state="disabled")
        for n in range(3):
            RetScale[n].configure(state="disabled")
    if (datVar[2]==0):
        pButton[2].configure(state="disabled")
        for n in range(3):
            AngScale[n].configure(state="disabled")
    if (datVar[0].get()+datVar[1].get()+datVar[2].get()!=3):
        pButton[3].configure(state="disabled")
        for n in range(4):
            cScale[n].configure(state="disabled")
def callback():
    root.filename =  tkFileDialog.askopenfilenames(initialdir = "./",title = "Select file",filetypes = (("CSV files","*.csv"),("all files","*.*")))
    enablePlots()
    print (sorted(root.tk.splitlist(root.filename)))
    readFiles(sorted(root.tk.splitlist(root.filename)))
    
def enableOptions(i,coloropt,resopt):
    if i == 0:
        ll.configure(text=textop[0])
        for n in range(3):
            datopt[n].configure(state="normal")
            coloropt[n].configure(state="disabled")
            resopt[n].configure(state=("disabled"))
            coloropt[n+1].configure(state="disabled")
    else:
        ll.configure(text=textop[1])
        for n in range(3):
            datopt[n].configure(state="disabled")
            coloropt[n].configure(state="normal")
            resopt[n].configure(state="normal")
            coloropt[n+1].configure(state="normal")
root = Tk()
#root.title =("Bireftest")
mainframe = ttk.Frame(root, padding="1 1 10 2")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)
root.geometry("800x800")
tvar = StringVar()
avar = StringVar()
ttk.Label(mainframe, text="BG brightness\n(0 = black,\n255 = white)").grid(column=0, row=28, sticky=(W))
gsc_entry = ttk.Entry(mainframe, width=7, textvariable=tvar)
gsc_entry.grid(column=0, row=27, sticky=(W, E))
gsc_entry.insert(0,"70")
ttk.Label(mainframe, text="Min Alpha\n0 = transparent\n1 = opaque").grid(column=1, row=28, sticky=(W))
a_entry = ttk.Entry(mainframe, width=7, textvariable=avar)
a_entry.grid(column=1, row=27, sticky=(W, E))
a_entry.insert(0,"0")
c = [[] for n in range(4)]
coloropt=[0,0,0,0]
cvar = IntVar()
color = ["Red", "Orange", "Green", "Blue"]
ttk.Label(mainframe, text="LED Color").grid(column=3, row=1, sticky=(W))
for n in range(4):
    coloropt[n] = ttk.Radiobutton(mainframe, text=color[n],value=n,variable=cvar, state=DISABLED)
    coloropt[n].grid(column=3,pady=10, row=n+2, sticky=W)
rVar = IntVar()
res = ["2x", "10x", "20x"]
resopt=[0,0,0]
ttk.Label(mainframe, text="Lens").grid(column=2, row=1, sticky=(W))
for n in range(3):
    resopt[n] = ttk.Radiobutton(mainframe, text=res[n],value=n,variable=rVar,state=DISABLED)
    resopt[n].grid(column=2, row=n+2,pady=10, sticky=W)
datVar = [IntVar() for n in range(3)]
dattext = ["Intensity", "Retardance", "AngleOfFastAxis"]
datopt=[0,0,0]
ttk.Label(mainframe, text="Data type").grid(column=1, row=1, sticky=(W))
for n in range(3):
    datopt[n] = ttk.Checkbutton(mainframe, text=dattext[n],variable=datVar[n],state=NORMAL)
    datopt[n].grid(column=1, row=n+2,pady=10, sticky=W)
    datopt[n].invoke()
iVar = IntVar()
dtb=[0,0]
pButton = [0,0,0,0]
inp = ["Processed","Raw"]
ttk.Label(mainframe, text="Input data type").grid(column=0, row=1, sticky=(W))
for n in range(2):
    dtb[n] = ttk.Radiobutton(mainframe, text=inp[n],value=n,variable=iVar,command=(lambda: enableOptions(iVar.get(),coloropt,resopt)))
    dtb[n].grid(column=0, row=n+2,pady=10, sticky=W)

ttk.Label(mainframe, text="Intensity Plot").grid(column=0, row=8, sticky=(W))

iscl = ["Spectral","Grayscale","RedYellowBlue"]
isclv = IntVar()
isclvs = ["Spectral","Greys","RdYlBu"]
IntScale=[0,0,0]
for n in range(3):
    IntScale[n] = ttk.Radiobutton(mainframe, text=iscl[n],value=n,variable=isclv,state=DISABLED)
    IntScale[n].grid(column=n, row=9,pady=10, sticky=W)
ttk.Label(mainframe, text="Retardance Plot").grid(column=0, row=15, sticky=(W))
pButton[0] = ttk.Button(mainframe, text="Plot!", command=(lambda: plotMethod(isclvs[isclv.get()],PrOP[0],"Intensity")),state=DISABLED)
pButton[0].grid(column=1, row=8, sticky=S)
rscl = ["Spectral","Grayscale","PurpleBlueGreen"]
rsclv = IntVar()
rsclvs = ["Spectral","Greys","PuBuGn"]
RetScale=[0,0,0]
for n in range(3):
    RetScale[n] = ttk.Radiobutton(mainframe, text=rscl[n],value=n,variable=rsclv,state=DISABLED)
    RetScale[n].grid(column=n, row=16,pady=10, sticky=W)
ttk.Label(mainframe, text="Angle Plot").grid(column=0, row=22, sticky=(W))
pButton[1] = ttk.Button(mainframe, text="Plot!", command=(lambda: plotMethod(rsclvs[rsclv.get()],PrOP[1],"Retardance")),state=DISABLED)
pButton[1].grid(column=1, row=15, sticky=S)
ascl = ["Spectral","Twilight","Twilight_Shifted"]
asclv = IntVar()
asclvs = ["hsv","twilight","twilight_shifted"]
AngScale=[0,0,0]
for n in range(3):
    AngScale[n] = ttk.Radiobutton(mainframe, text=ascl[n],value=n,variable=asclv,state=DISABLED)
    AngScale[n].grid(column=n, row=23,pady=10, sticky=W)
pButton[2] = ttk.Button(mainframe, text="Plot!", command=(lambda: plotMethod(asclvs[asclv.get()],PrOP[2],"Angle of Fast Axis")),state=DISABLED)
pButton[2].grid(column=1, row=22, sticky=S)
ttk.Label(mainframe, text="Combined").grid(column=0, row=24, sticky=(W))
cscl = ["Int + Ret", "Int + Angle", "Angle + Ret","Angle + Ret + Int"]
csclv = IntVar()
cScale=[0,0,0,0]
for n in range(4):
    cScale[n] = ttk.Radiobutton(mainframe, text=cscl[n],value=n,variable=csclv,state=DISABLED)
    cScale[n].grid(column=n, row=25,pady=10, sticky=W)
pButton[3] = ttk.Button(mainframe, text="Plot!", command=(lambda: plotCombinedMethod()),state=DISABLED)
pButton[3].grid(column=1, row=24, sticky=S)

textop = ["Click open and select the processed data files you have chosen.\nThey should be named e.g. *Intensity.csv, *Retardance.csv, and *AngleOFFastAxis.csv where * is arbitrary.\nAlternatively, you can give them any way which sorts them in the same alphabetical order (e.g. *I.csv, *R.csv, *A.csv).","Click open and select the 7 raw data files. \nThey must be named so that they can be sorted \n(e.g., name_0.csv to name_6.csv)"]
ll = ttk.Label(root, text=textop[0])
ll.grid(column=0, row=3, sticky=(W))
for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)
b = ttk.Button(root, text="Open", command=(lambda: callback())).grid(column=0, row=2, sticky=W)


mainloop()
