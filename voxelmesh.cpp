#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <random>
#include <vector>
#include <typeinfo>
#include <tuple>
#include <vector>

#include <"Matrix.hpp">
#include <mpi.h>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
namespace champion{
template <typename T> class Voxel {
  public:
    using iterator = T *;
    using const_iterator = const T *;
    // Default constructor
    Jones() : j0{0}, j1{0} {}
    // Copy constructor
    Jones(const Jones &) = default;
    // Copy assignment
    Jones &operator=(const Jones &) = default;
    // Move constructor
    Jones(Jones &&) = default;
    // Move assignment
    Jones &operator=(Jones &&) = default;
    // Generalized copy constructor
    template <typename T2>
    Jones(const Jones<T2> &V)
        : s0{static_cast<T>(J.j0)}, s1{static_cast<T>(J.j1)} {}
    // Generalized copy assignment
    template <typename T2> Jones &operator=(const Jones<T2> &V) {
        for (size_type i = 0; i != this->size(); ++i) {
            (*this)[i] = V[i];
        }
        return *this;
    }
    // Construct from parameters
    Jones(T j0_, T j1_) : j0{j0_}, j1{j1_} {}
}

tenplate <typename T, size_type N = 0>
class VoxelMesh: public VoxelMesh {
public:
    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;

    
}

template <typename T>
class Voxel: public Point <T>{
  public:
   Voxel<T>(std::pair<T, T> bounds)
        : x{bounds, 0}, y{bounds, 0}, z{bounds, 0} {}
  private:

};
}
