from tkinter import filedialog
from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
from matplotlib import colors
import math
import numpy as np
import csv
import time
import os
import threading as thr
import time

from scipy import signal as sps
from scipy import fftpack as spf
from scipy import linalg as spl
from scipy import ndimage as spn


global finished
global globlist
global outPut
global PrOP
globlist = np.zeros((7,2048,2048))
outPut = np.zeros((5,2048,2048))
PrOP = np.zeros((3,2048,2048))
POLLING_DELAY = 2000 # ms
nano = np.array([475,535,615,655])
lock = thr.Lock()  # Lock for shared resources.
finished = 0
cdict = {'red':   [[0.0,  1.0, 1.0],
                   [0.25,  1.0, 1.0],
                   [0.5,  0.0, 0.0],
                   [0.75,  1.0, 1.0],
                   [1.0,  1.0, 1.0]],
         'blue': [[0.0,  0.0, 0.0],
                   [0.25,  1.0, 1.0],
                   [0.5,  1.0, 1.0],
                   [0.75,  0.0, 0.0],
                   [1.0,  0.0, 0.0]],
         'green':  [[0.0,  0.0, 0.0],
                   [0.25,  0.0, 0.0],
                   [0.5,  1.0, 1.0],
                   [0.75,  1.0, 1.0],
                   [1.0,  0.0, 0.0]]}
indAlpha = [0,0,1,0]
indChrom = [0,2,2,2]
indGrays = [1,1,0,1]
softmap = colors.LinearSegmentedColormap('testCmap', segmentdata=cdict, N=256)

cmap = plt.cm.get_cmap("hsv",256)
newcmt = cmap(np.linspace(0,1,256))
hsshift = colors.ListedColormap(newcmt[range(255,0,-1),:])

def comboPlotHelper(mat,mat_g,mat_a):
    fig = plt.figure(constrained_layout=True)
    greys =np.full((mat.shape[0],mat.shape[1], 3),float(tvar.get()))
    if csclv.get()==3:
        for i in range(mat.shape[0]):
            for j in range(mat.shape[1]):
                    greys[i][j][:]*=mat_g[mat_g.shape[0]-1-i][j]
    greys=np.ndarray.astype(greys,np.uint8)
    cmap = asclvs[asclv.get()]
    indAlpha = [1,1,0,1]
    indChrom = [0,2,2,2]
    if csclv.get()>1:
        alphas = np.clip(np.add(float(avar.get()),np.multiply((1.0-float(avar.get())),np.divide(mat_a,float(rlim[1].get()) - float(rlim[0].get())))),0,1)
    else:
        alphas =  np.clip(np.add(float(avar.get()),np.multiply((1.0-float(avar.get())),np.divide(mat_a,float(ilim[1].get()) - float(ilim[0].get())))))
    if csclv.get() != 0:
        mat += ((float(atvar.get())/360)*2*np.pi)*math.pi
        mat-= (mat>math.pi/2)*math.pi
        mat = np.divide(np.add(mat,math.pi/2.0),math.pi)
    clrs = mat
    clrs = cmap(clrs)
    clrs[..., -1] = alphas
    if indChrom[csclv.get()] == 0:
        ax=fig.add_subplot(111)
        ax.imshow(greys)
        pos = ax.imshow(clrs,extent=(0, mat.shape[0], 0, mat.shape[1]),cmap=cmap)
        fig.colorbar(pos, ax=ax)
    else:
        wheelPlotHelper(clrs,fig,0,1,'combined',greys,np.amin(alphas))
    return fig
    
def readfun(infiles,worklist,i=1,s1=[0],s2=[0]):
    if loadText.get()=='.bin':
        worklist*=0
        if iVar.get():
            worklist+=np.reshape(np.fromfile(infiles[0],dtype=np.uint16)[0:(7*2048*2048+10)],(7,2048,2048),order='C')
        else:
            multip=(1,np.pi,np.pi)
            addv=np.array((0.5,np.pi/2,0))*0
            for j in range(i):
                worklist[s2[j]]=np.sqrt(np.reshape(np.fromfile(infiles[s1[j]],dtype=np.uint16)[7::4] +np.fromfile(infiles[s1[j]],dtype=np.uint16)[6::4] + np.fromfile(infiles[s1[j]],dtype=np.uint16)[8::4],(2048,2048),order='F')/(np.reshape(np.fromfile(infiles[s1[j]],dtype=np.uint16)[9::4],(2048,2048),order='F')*2))
        np.nan_to_num(worklist,copy=False)
    else:
        
        if loadText.get()=='.bin (Hinds)':
            worklist+=np.reshape(np.fromfile(infiles[0],dtype=np.uint16)[10:(7*2048*2048+10)],(7,2048,2048),order='F')
        else:
            for j in range(i):
                worklist[s2[j]]*=0.0
                if loadText.get()=='.csv':
                    print("Reading in: " + str(infiles[s1[j]]))
                    worklist[s2[j]] += np.nan_to_num(np.loadtxt(infiles[s1[j]],delimiter=',')).reshape((2048,2048),order='C')
                else: 
                    if loadText.get()=='.dat':
                        worklist[s2[j]]+=np.loadtxt(infiles[s1[j]])
        
        np.nan_to_num(worklist[s2[j]],copy=False)    
    
def wheelPlotHelper(mat,fig,mins,maxs,ptype='none',greys=[],alpha=1.0):

    mat += ((float(atvar.get())/360)*2*np.pi)*math.pi
    mat-= (mat>math.pi/2)*math.pi
    mat +=(mat<-math.pi/2)*math.pi
    gs = GridSpec(3, 4, figure=fig)
    newcm = asclvs[asclv.get()]
    if wVar.get():
        ax = fig.add_subplot(gs[:,:-1])
        ax1 = fig.add_subplot(gs[0,3],projection='polar')
        r=np.transpose([[np.linspace(0.2,math.pi)] for n in range(50)])
        th =np.array([[np.linspace(-math.pi/2,math.pi/2-math.pi/50)] for n in range(50)])
        th2 = np.array([[np.linspace(math.pi/2,3*math.pi/2-math.pi/50)] for n in range(50)])
        ax1.grid(False)
        ax1.set_yticklabels([])
        ax1.set_xticklabels([])
        ax1.set_rmax(math.pi)
        ax.set_title("Angle of Fast Axis")
        if ptype=='combined':
            ax.imshow(greys)
            thx = (th+math.pi/2)/math.pi
            thx=(newcm(thx.flatten()))
            rx=np.square(np.square(r/math.pi))
            thx[:,-1]*=thx[:,-1]*rx.flatten()
            print(thx)
            ax1.scatter(th,r,c=thx,s=float(csize.get())) 
            ax1.scatter(th2,r,c=thx,s=float(csize.get()))
            ax1.set_facecolor((float(tvar.get())/255,float(tvar.get())/255,float(tvar.get())/255))
            pos = ax.imshow(mat,extent=(0, mat.shape[0], 0, mat.shape[1]),cmap=newcm)
            ax1.set_rmax(math.pi)
        else: 
            
            ax1.scatter(th,r,c=(th),alpha=float(calph.get()),s=float(csize.get()),cmap=newcm) 
            ax1.scatter(th2,r,c=(th2),alpha=float(calph.get()),s=float(csize.get()), cmap=newcm)
            pos = ax.imshow(mat,cmap=newcm,norm=plt.Normalize(mins,maxs))
    else:
         plotMethod(newcm,mat*360/(np.pi*2),"Angle of Fast Axis",2)

def saveplot(fig,s,M):   
        if savText.get() == '.dat':
            np.savetxt(fname.get() + s + savText.get(),M)
            print("Saved as "+fname.get() + s + savText.get() + "!")
        else:
            plt.savefig(fname.get() + s + savText.get(),dpi=300)  
            print("Saved as "+fname.get() + s + savText.get() + "!")

#Only takes infiles as argument. PasteVar tells the method which plots (retardance, angle, etc)
#are to be created. [0] -> Intensity, [1] -> Retardance, [2] -> Angle, [3] -> Combined. 
#It uses the same radio button settings as the regular plot.
def paste(infiles):
    mins = [ilim[0].get(),rlim[0].get(),alim[0].get()]
    maxs = [ilim[1].get(),rlim[1].get(),alim[1].get()]
    dispVec = [tuple(np.zeros(2)) for n in range(100)]
    sel=['Greys_r','PuBuGn',asclvs[asclv.get()]]
    paLen = pasteVar[0].get()+pasteVar[1].get()+pasteVar[2].get()
    if pasteVar[3].get():
        paLen=3
    worklist = np.array([np.zeros((2048,2048)) for m in range(2)])
    if paLen==3:
        filenames=[infiles[1:len(infiles):3],infiles[2:len(infiles)-1:3],infiles[0:len(infiles)-2:3]]
        scl = [isclvs[isclv.get()],rsclvs[rsclv.get()],asclvs[asclv.get()]]
    else:
        if paLen==2:
            
            if pasteVar[1].get():
                filenames=[infiles[0:len(infiles):2],infiles[1:len(infiles)-1:2]]
                scl = [isclvs[rsclv.get()],rsclvs[isclv.get()]]
            else:
                filenames=[infiles[1:len(infiles):2],infiles[0:len(infiles)-1:2]]
                scl = [isclvs[rsclv.get()],asclvs[asclv.get()]]
        else:
            if (paLen==1)&(pasteVar[0].get()):
                filenames=[infiles]
                scl=[isclvs[isclv.get()]]    
                
    t=np.zeros(2048)
    FT_DIMF = int(sv.get().split(',')[0])/2
    FT_DIMF2 = int(sv.get().split(',')[1])/2
    FT_DIM=int(2048*float(sv.get().split(',')[0]))
    i = 0
    p=0
    indp1 = int((indpvar.get().split(','))[0])
    indp2 = int((indpvar.get().split(','))[1])
    print("Starting position: " + str((indp1,indp2)))
    ftr=np.ones((FT_DIM,FT_DIM))
    ftr[int((2048-200)*FT_DIMF):int((2048+200)*FT_DIMF),int((2048-200)*FT_DIMF):int((2048+200)*FT_DIMF)] = 0
    fftwl = np.ndarray.astype(np.zeros((FT_DIM,FT_DIM)),complex)
    fftwlx = np.ndarray.astype(np.zeros((FT_DIM,FT_DIM)),complex)
    ox1 = np.zeros((60,FT_DIM))
    ox2 = np.zeros((60,FT_DIM))
    out1 = np.zeros((60,60))
    outarray = np.array([np.zeros((10000,10000)) for m in range(paLen)])
    displacement=(0,0)
    distot = (0,0)
    counter=0
    debugmode=1
    print(filenames)
    ##This loop initializes each data type, starting with Retardance.
    for q in range(paLen):
        worklist*=0
        start = time.time()
        readfun(filenames[q],worklist,2,[0, 1],[0, 1]) #must be sent as lists or arrays
        print(np.amax(worklist[1]))
        print(np.amax(worklist[0]))
        end=time.time()
        print("Reading time:" + str(end - start))
        outarray[q][indp1:2048+indp1,indp2:2048+indp2] = np.copy(worklist[0])
        ftrs = ftr
        ##This loop does the Fourier stuff and pastes the images together.
        for i in (range(((len(infiles)//paLen)-1))):
            if i>0:
                start = time.time()
                print(np.amax(worklist[0]))
                print(np.amax(worklist[1]))
                worklist[0] = np.copy(worklist[1])
                print(np.amax(worklist[0]))
                print(np.amax(worklist[1]))
                p=0
                print(i)
                readfun(filenames[q],worklist,1,[i+1],[1])
                end=time.time()
                print("Reading time:" + str(end - start))
            if q==0:
                print(np.amax(worklist[0]))
                print(np.amax(worklist[1]))
                start = time.time()
                fftwl*=0
                fftwlx*=0
                fftwl += spf.fftshift(spf.fft2(worklist[0],(FT_DIM,FT_DIM)))
                fftwl*=spf.fftshift(np.conj(spf.fft2(worklist[1],(FT_DIM,FT_DIM))))
                fftwlx += spn.maximum_filter(np.abs(spf.fftshift(spf.ifft2((fftwl)))),size=(4,4))
                fftwlx*=ftr
                fftwlx -=(ftr*np.abs(spf.fftshift(spf.ifft2((fftwl)))))
                if (four_var.get()&(i>0)):
                    fig = plt.figure()
                    ax = fig.add_subplot(111)
                    ax.imshow(np.nan_to_num(abs(fftwlx)))
                    ax.imshow(np.nan_to_num(abs(fftwl)))
                    plt.show()
                #fftwlx = spf.ifftshift(fftwlx)
                end = time.time()
                print("Initial Fourier time:" + str(end - start))
                start = time.time()
                mm = np.ndarray.astype(((np.unravel_index(np.argmax((fftwlx)),(FT_DIM,FT_DIM)))-np.array((int(2048*FT_DIMF),int(2048*FT_DIMF)))),int)
                ox1 = np.pad(np.transpose(fftwl)[max(mm[1]-8,0):min(max(mm[1]-8,0)+16,FT_DIM)],((int(8*FT_DIMF2),int(8*FT_DIMF2)),(0,0)),'constant')

                ox2 = np.pad((fftwl[max(mm[0]-8,0):min(max(mm[0]-8,0)+16,FT_DIM)]),((int(8*FT_DIMF2),int(8*FT_DIMF2)),(0,0)),'constant')

                out1 = (spf.ifft2((ox2@spl.dft(FT_DIM)@np.transpose(ox1))))
                mmx = (np.unravel_index(np.argmax((spf.fftshift(out1))),np.shape(out1)))-np.ones(2)*(8*FT_DIMF2+8)
                end = time.time()
                print("Secondary Fourier time:" + str(end - start))
                print("Initial displacement estimate:" + str(mm))
                displacement=np.ndarray.astype(np.add(mm,np.divide((np.array(mmx)),((17+8*FT_DIMF2)/17))),int)
                print("Adjusted estimate:" + str(displacement))
                if True:
                    distot = distot + displacement
                else:
                    distot = displacement
            start = time.time()
            if q == 0:
                dispVec[i]=distot
                print("Total displacement: " + str(distot) + " (Negative must not exceed starting position value!)")
            else:
                distot=np.ndarray.astype(dispVec[i],int)
            aux=np.array(outarray[q][0+indp1+distot[0]:indp1+2048 + distot[0],distot[1]+indp2:indp2+2048+distot[1]])
            print(aux.size)
            print((0+indp1+distot[0],indp1+2048 + distot[0],distot[1]+indp2,indp2+2048+distot[1]))
            logic1=np.array((worklist[1]!=0.0) ^ (aux!=0.0))
            aux[logic1]+= worklist[1][logic1]
            logic1=np.array((worklist[1]!=0.0) & (aux!=0.0))
            if ((q == 2)|((paLen==2)&(q==1)&pasteVar[2].get())):
                print("Averaging angles...")
                l2 = np.multiply(np.ndarray.astype((worklist[1][logic1] <= 0)&(aux[logic1]-worklist[1][logic1]>=math.pi/2.0),int),math.pi)
                l3 = np.multiply(np.ndarray.astype((aux[logic1] <= 0)&(worklist[1][logic1]-aux[logic1]>=math.pi/2.0),int),math.pi)
                aux[logic1]=np.arctan2(np.sin(worklist[1][logic1]+l2) + np.sin(aux[logic1]+l3),np.cos(worklist[1][logic1]+l2) + np.cos(aux[logic1])+l3)
                l3 = np.multiply(np.ndarray.astype(aux[logic1] >=math.pi/2.0,int),math.pi)
                aux[logic1]-=l3
            else:
                aux[logic1] +=(worklist[1][logic1])
                aux[logic1]*=0.5
            outarray[q][indp1+distot[0]:2048+indp1 + distot[0],distot[1]+indp2:indp2+2048+distot[1]] = np.copy(aux)
            end = time.time()
            print("Pasting time:" + str(end - start))
    #This loop plots. It's similar to the individual plotting methods, so see them for some more info.
    for q in range(paLen+pasteVar[3].get()):
        fig = [0,0,0]
        z= [0,0,0]
        titlec=["Intensity","Retardance","Angle of fast axis"]
        if q != 3:
            print("Plotting...")
            fig[q]=plt.figure(constrained_layout=True)
            if (q == 2)|((paLen==2)&(q==1)&pasteVar[2].get())|((paLen==1)&pasteVar[2].get()):
                outarray[q]+=((float(atvar.get())/360)*2*np.pi)*math.pi
                outarray[q]-=(outarray[q]>math.pi/2)*math.pi
                wheelPlotHelper(outarray[q],fig[q],((float(alim[0].get())/360)*2*np.pi),((float(alim[1].get())/360)*2*np.pi))
            else:
                ax=fig[q].add_subplot(111)
                z[q]=ax.imshow(outarray[q],cmap=sel[q],norm=plt.Normalize(mins[q],maxs[q]))
                ax.set_title(titlec[q])
                fig[q].colorbar(z[q], ax=ax)
        else:
            comboPlotHelper(outarray[indChrom[csclv.get()]],outarray[indGrays[csclv.get()]],outarray[indAlpha[csclv.get()]])
    print("Plotting finished!")
    plt.show()
                

#Takes no input arguments. The configuration given by csclv determines the type of plot - I + R, I + A, A + R, I + A + R.
def plotCombinedMethod():
    fig=comboPlotHelper(PrOP[indChrom[csclv.get()]],PrOP[indGrays[csclv.get()]],PrOP[indAlpha[csclv.get()]])
    plt.show()
    t = ["I","R","A"]
    if savVar.get():
        saveplot(fig,t[indChrom[csclv.get()]],PrOP[indChrom[csclv.get()]])
        saveplot(fig,t[indGrays[csclv.get()]],PrOP[indGrays[csclv.get()]])
        saveplot(fig,t[indAlpha[csclv.get()]],PrOP[indAlpha[csclv.get()]])

#Identical, just takes the number saves as an image as part of batch instead.
def plotCombinedMethodB(n):
    fig = comboPlotHelper(PrOP[indChrom[csclv.get()]],PrOP[indGrays[csclv.get()]],PrOP[indAlpha[csclv.get()]])
    t = ["I","R","A"]
    saveplot(fig,bvar.get() + str(n)+t[indChrom[csclv.get()]],PrOP[indChrom[csclv.get()]])
    saveplot(fig,bvar.get() + str(n)+t[indGrays[csclv.get()]],PrOP[indGrays[csclv.get()]])
    saveplot(fig,bvar.get() + str(n)+t[indAlpha[csclv.get()]],PrOP[indAlpha[csclv.get()]])

#Standard plot. Takes more arguments for no good reason - the global variables could just as well be read.
def plotMethod(scl,mat,lab,ptype,show=1):
    mat=np.copy(mat)
    fig = plt.figure(constrained_layout=True)
    mins = [ilim[0].get(),rlim[0].get(),alim[0].get()]
    maxs = [ilim[1].get(),rlim[1].get(),alim[1].get()]
    #If angle plot, create color wheel.
    suf = ['I','R','A']
    if (ptype == 2)&(wVar.get()): 
        wheelPlotHelper(mat,fig,((float(mins[ptype])/360)*2*np.pi),((float(maxs[ptype])/360)*2*np.pi))
    else:
        if (ptype == 1)&(qvar.get()): 
            ax = fig.add_subplot(111)
            ax.set_title(lab + " (nm)")
            pos = ax.imshow(mat*nano[cvar.get()]/(np.pi),cmap=scl,norm=plt.Normalize(float(mins[ptype])*nano[cvar.get()]/(np.pi),float(maxs[ptype])*nano[cvar.get()]/(np.pi)))
        else:
            ax = fig.add_subplot(111)
            ax.set_title(lab)
            if ptype==2:
                pos = ax.imshow(mat*360/(np.pi*2),cmap=scl,norm=plt.Normalize(mins[ptype],maxs[ptype]))
            else:
                pos = ax.imshow(mat,cmap=scl,norm=plt.Normalize(mins[ptype],maxs[ptype]))
        fig.colorbar(pos, ax=ax)
    if savVar.get():
        saveplot(fig,suf[ptype],mat) 
    if show==1:
        plt.show()
    
def plotMethodB(scl,mat,lab,ptype,n,t):
    plotMethod(scl,mat,lab,ptype,0)
    saveplot(fig,bvar.get() + str(n)+t,mat)

#This is calibration data from the microscope's configuration files.
def chooseCalib():
    if rVar.get()== 1:
        if cvar.get()==0:
            cm = ("0.5 0.0635648533981375 0.13711113346642 0.200098617382466 0.431618209870321 0.5 0.151121607468367 0.00148770430897562 0.475722401523261 0.00468321028659341 0.5 0.0553273351844126 -0.140637262094243 0.174167368947029 -0.442718266354692 0.5 0.192492274003726 0.415211118428425 -0.00234612797712189 -0.00506066244164242 0.5 0.457638778626314 0.0045051875395067 -0.00557777784852965 -5.49099779891061e-05 0.5 0.167546749419955 -0.425889228763203 -0.00204208775819427 0.00519080903338942 0.5 0.159782212430189 0.0015729629264287 -0.472883905568582 -0.00465526694524383".split())

        if cvar.get()==1:
            cm = ("0.5 0.0600142572159182 0.14278413723057 0.183792211258531 0.437272966985707 0.5 0.154869519031824 0.00211020901368058 0.47428415646298 0.00646246406827422 0.5 0.0513660938279309 -0.146118257992342 0.157307420041592 -0.447483631181792 0.5 0.175801544580146 0.418261810295632 -0.000844790939971299 -0.00200990149840612 0.5 0.453663877838639 0.0061814978840327 -0.00218002142532716 -2.97043659107436e-05 0.5 0.150468222934262 -0.42802854918282 -0.000723055032263933 0.00205683426310205 0.5 0.173521598725619 0.00236435706644006 -0.467782303825887 -0.00637387163171091".split())

        if cvar.get()==2:
            cm = ("0.5 0.0647069031908922 0.127582786825517 0.215940420746949 0.425770347653543 0.5 0.14134691251989 0.00304178941179066 0.471704103514003 0.0101510851704327 0.5 0.055570144241397 -0.131819231843123 0.185449152048818 -0.439908247544602 0.5 0.203104748609873 0.400462216043603 -0.00188271180511642 -0.00371214827230151 0.5 0.443665632543743 0.00954769651057685 -0.00411262922029954 -8.85038929673654e-05 0.5 0.174425905425682 -0.413759747804042 -0.00161686870204023 0.00383541186015774 0.5 -0.0341207814626758 -0.000734280147510016 -0.4912427945764 -0.0105715583348924".split())
        if cvar.get()==3:
            cm = ("0.5 0.0570142340675644 0.135363276550162 0.185379998987274 0.44012946030365 0.5 0.144405618721873 0.00407145375284715 0.469530353081547 0.0132382045452914 0.5 0.0479006115689778 -0.138850174418416 0.155747340455788 -0.451466999671795 0.5 0.187675386869818 0.445579173516783 -0.00190551798495513 -0.0045240835413683 0.5 0.475344110168815 0.0134021209033985 -0.0048262948385313 -0.000136075288528289 0.5 0.157675814724715 -0.457057095076242 -0.00160092437139244 0.00464062192355169 0.5 -0.0188469489859409 -0.000531381547738245 -0.490873229762278 -0.0138399576912384".split())
    
    if rVar.get()== 0:
        if cvar.get()==3:
            cm = ("0.5 0.03915811209265 0.157274971779476 0.114039705512558 0.458030035354892 0.5 0.161904337801833 0.00724725093776236 0.47151208312702 0.0211060829685187 0.5 0.0382501526773297 -0.157498254946068 0.111395466073856 -0.458680300273276 0.5 0.115658051351864 0.464529973237599 -0.00232749094998717 -0.00934815428818709 0.5 0.478203345740174 0.0214056009425668 -0.00962331585613299 -0.000430764152521388 0.5 0.112976287317018 -0.465189466113215 -0.00227352340126792 0.00936142585624167 0.5 -0.154808384380711 -0.00692961797138382 -0.473889252103554 -0.0212124911125395".split())

        if cvar.get()==2:
            cm = ("0.5 0.0440758779017302 0.143568537629763 0.139462210927367 0.45427083090884 0.5 0.150030210330752 0.00674862816161821 0.474716462489408 0.0213535985884247 0.5 0.0426147977696061 -0.144008982431696 0.134839150077112 -0.455664459550928 0.5 0.128997663077466 0.420184616333227 -0.000527098406238488 -0.0017169197977034 0.5 0.439096109823099 0.0197513311877414 -0.00179419420593911 -8.07060759224376e-05 0.5 0.124721493626385 -0.421473674041632 -0.000509625515267462 0.00172218702694958 0.5 0.0837411132992872 0.0037668255896659 -0.490766960699009 -0.0220755788081853".split())

        if cvar.get()==1:
            cm = ("0.5 0.046831754439431 0.151242576878561 0.139955384955457 0.451984200080965 0.5 0.158224341362577 0.00570859291406669 0.472849007469134 0.0170599698517702 0.5 0.0452064564399754 -0.151736306073673 0.135098227458287 -0.453459695936132 0.5 0.134341252838866 0.433853429230856 -0.000429000707703864 -0.00138545252665636 0.5 0.453881271429713 0.0163756308770078 -0.00144940874483599 -5.22933729355635e-05 0.5 0.12967893403196 -0.435269737447969 -0.000414112220173515 0.00138997531630308 0.5 0.18833529895075 0.00679496936944223 -0.461682727891458 -0.0166571004580684".split())

        if cvar.get()==0:
            cm = ("0.5 0.0538097637025962 0.145775030119081 0.164244295546785 0.444951166530854 0.5 0.155314053400712 0.00483682657735166 0.474067260921366 0.0147635135190301 0.5 0.0524197249905055 -0.146280629298167 0.160001460913429 -0.446494413987897 0.5 0.158642237280644 0.429774734666385 -0.00117082336027946 -0.00317185579093386 0.5 0.457897734857095 0.014259958356452 -0.00337941127016087 -0.000105242416184606 0.5 0.154544117608319 -0.431265344909182 -0.00114057810953272 0.00318285690465523 0.5 0.188864238291601 0.00588165428226408 -0.461727758052058 -0.0143792338345923".split())
    if rVar.get()==2:
        if cvar.get()==3:
            cm = ("0.5 0.0682054992690202 0.109652441194078 0.254505035373642 0.409161999017588 0.5 0.126236246637187 0.00313657843641321 0.471043548689717 0.0117039683671685 0.5 0.0589535452265559 -0.114891807782702 0.219981882312841 -0.428712404677898 0.5 0.238072932657923 0.382744478494446 0.000860642132494793 0.00138363492436739 0.5 0.440630649533192 0.0109483023344376 0.00159289549476718 3.95785029533255e-05 0.5 0.20577876495426 -0.401032613357354 0.000743897565822194 -0.00144974718337996 0.5 0.082076694010384 0.00203935078412905 -0.480708883865872 -0.0119441219102421".split())

        if cvar.get()==2:
            cm = ("0.5 0.071481788530538 0.114316805401975 0.254317746228415 0.406716072772049 0.5 0.12820083692159 0.00263234687614001 0.456112648840151 0.00936535778683456 0.5 0.0630607888871788 -0.119169270341662 0.224357532664725 -0.42398016160478 0.5 0.235943018963816 0.37733040455917 0.00100713987175113 0.00161066217141947 0.5 0.42315802554444 0.00868870074020474 0.00180628069204559 3.70883486513164e-05 0.5 0.208147462649517 -0.393347144638314 0.000888492525690304 -0.00167903078694376 0.5 0.109582487198283 0.00225005643319226 -0.460940214690379 -0.00946448216223144".split())

        if cvar.get()==1:
            cm = ("0.5 0.0744763046089359 0.130242922123579 0.236079054795496 0.412851122383006 0.5 0.150032226194211 0.000519431283583023 0.475580338401264 0.00164652162997864 0.5 0.0655326193631056 -0.134964493519229 0.207728873213437 -0.427817817066469 0.5 0.222112784376259 0.388427141089615 0.000288753893727234 0.000504968004133288 0.5 0.447445340919433 0.00154911457133329 0.000581693596716748 2.01389967515032e-06 0.5 0.195439779546507 -0.402508416668819 0.000254078113926332 -0.000523274123538153 0.5 0.174054435361525 0.00060259932860107 -0.467323635932992 -0.00161793584097818".split())

        if cvar.get()==0:
            cm = ("0.5 0.0743584761747456 0.124895139665402 0.24410539436496 0.410008097135325 0.5 0.14535208792033 0.000865693182349383 0.477164548936963 0.00284191374739635 0.5 0.066280830726774 -0.129363172383949 0.217587949023778 -0.42467583839233 0.5 0.230427478065535 0.387034182735388 0.000227683284310298 0.000382424937361381 0.5 0.450427668425201 0.0026826732747792 0.00044506346097366 2.65072493550239e-06 0.5 0.205395880256706 -0.400880048925942 0.000202949791375823 -0.000396105911153422 0.5 0.177093590207712 0.00105474036096835 -0.466316519686315 -0.0027773046654177".split())
    return np.array(list(map(float,cm)))


#Processed data reading
def readData(infiles):
    t=np.zeros(2048)
    #Because the infiles list is sorted alphabetically, but the order intensity, retardance, angle is consistently used, the indexing gets a bit confusing.
    if datVar[0].get()+datVar[1].get()+datVar[2].get()==3:
        ind = [1,2,0]
        prInd =[0,1,2]
    else:
        if datVar[0].get()+datVar[1].get()+datVar[2].get()==2:
            if datVar[0].get()+datVar[1].get()==2:
                ind=[1,0]
                prInd=[0,1]
            if datVar[0].get()+datVar[2].get()==2:
                ind=[1,0]
                prInd=[0,2]
            if datVar[1].get()+datVar[2].get()==2:
                ind=[0,1]
                prInd=[1,2]
        else:
            if datVar[0].get()+datVar[1].get()+datVar[2].get()==1:
                ind=[0]
                if datVar[2].get()==1:
                    prInd=[2]
                if datVar[1].get()==1:
                    prInd=[1]
                if datVar[0].get()==1:
                    prInd=[0]
    readfun(infiles,PrOP,len(prInd),ind,prInd)
    print(id(PrOP))            

#This applies the calibration matrix transform to the raw data.
def transForm(wl,k,M,t,s):
    global outPut
    tempv = [0,0,0,0,0]
    op=np.zeros((5,512,2048))
    for j in range(2048):
        il = 0
        for i in t:
            tempv= np.matmul((M),wl[i,j])
            for p in range(5):
                op[p][il][j] = tempv[p]
            il+=1
    with lock:
        for q in range(5):
            outPut[q][s] = op[q]
    return

#Parent function for reading raw data. I recommend not bothering with it, just use processed data.
def readFiles(infiles):
    global finished
    global globlist
    global outPut
    global PrOP
    if iVar.get()== 0:
        readData(infiles)
        return
    if ((len(infiles) != 7) and not(loadText.get()=='.bin')):
        messagebox.showinfo("Error!", "Select exactly 7 files!")
        return
    calib_matrix=np.linalg.pinv(np.reshape(chooseCalib(),(7,5)))
    threads=[[] for j in range(4)]
    start = time.time()
    print("Working...")
    tup = [range(0,512),range(512,1024),range(1024,1536),range(1536,2048)]
    sl = [slice(0,512,1),slice(512,1024,1),slice(1024,1536,1),slice(1536,2048,1)]
    wlist=np.zeros((7,2048,2048))
    readfun(infiles,wlist,7)
    wlist=wlist.transpose()
    print(np.amax(wlist))
    print(np.mean(wlist))
    for i in range(4):
        threads[i] = thr.Thread(target=transForm,args=(wlist,i,calib_matrix,tup[i],sl[i]))    
        threads[i].start()
    for i in range(4):
        threads[i].join()
    PrOP[2] = -np.multiply(1,0.5*np.arctan2(outPut[3],outPut[2]))+math.pi/4
    for i in range(2048):
        for j in range(2048):
            if PrOP[2][i][j]>math.pi/2:
                PrOP[2][i][j] = (PrOP[2][i][j]-math.pi)
    PrOP[1]=np.multiply(np.sign(outPut[4]),np.arctan(np.sqrt(np.square(np.divide(outPut[3],outPut[4])) + np.square(np.divide(outPut[2],outPut[4])))))
    np.nan_to_num(PrOP[1],copy=False)
    for i in range(2048):
        for j in range(2048):
            if PrOP[1][i][j]<0:
                PrOP[1][i][j] = math.pi + PrOP[1][i][j]
    PrOP[0] = np.divide(outPut[0],2*4096)
    print("Done!")
    
    end = time.time()
    print("Elapsed time: " + str(end - start))
    return

#Interface control
def enablePlots():  
    if datVar[0]:
        pButton[0].configure(state="normal")
        for n in range(3):
            IntScale[n].configure(state="normal")
    if datVar[1]:
        pButton[1].configure(state="normal")
        for n in range(3):
            RetScale[n].configure(state="normal")
    if datVar[2]:
        pButton[2].configure(state="normal")
        for n in range(3):
            AngScale[n].configure(state="normal")
    if datVar[0].get()*datVar[1].get()*datVar[2].get():
        pButton[3].configure(state="normal")
        for n in range(4):
            cScale[n].configure(state="normal")
    if (datVar[0]==0):
        pButton[0].configure(state="disabled")
        for n in range(3):
            IntScale[n].configure(state="disabled")
    if (datVar[1]==0):
        pButton[1].configure(state="disabled")
        for n in range(3):
            RetScale[n].configure(state="disabled")
    if (datVar[2]==0):
        pButton[2].configure(state="disabled")
        for n in range(3):
            AngScale[n].configure(state="disabled")
    if (datVar[0].get()+datVar[1].get()+datVar[2].get()!=3):
        pButton[3].configure(state="disabled")
        for n in range(4):
            cScale[n].configure(state="disabled")

#File reading callback
def callback():
    root.filename =  filedialog.askopenfilenames(initialdir = "./",title = "Select files")
    enablePlots()
    print (sorted(root.tk.splitlist(root.filename)))
    readFiles(sorted(root.tk.splitlist(root.filename)))
#Batch reading function
def readBatch(infiles):
    if iVar.get()!=0:
        if np.mod(len(infiles),7)  != 0:
            messagebox.showinfo("Error!", "Select batch of sets of 7 files!")
            return
        for n in range(int(len(infiles)/7)):
            print("Reading batch " + str(n))
            readFiles(infiles[n*7:(n+1)*7-1])
            if batchVar[0].get():
                plotMethodB(isclvs[isclv.get()],PrOP[0],"Intensity",0,n,"I")
            if batchVar[1].get():
                plotMethodB(rsclvs[rsclv.get()],PrOP[1],"Retardance",1,n,"R")
            if batchVar[2].get():
                plotMethodB(asclvs[asclv.get()],PrOP[2],"Angle",2,n,"A")
            if batchVar[3].get():
                plotCombinedMethodB(n)
    else:
        if np.mod(len(infiles),3) != 0:
            messagebox.showinfo("Error!", "Select batch of sets of 3 files!")
            return
        print(((np.floor((len(infiles))/3)-1)).astype(np.int64))
        for n in range(((np.floor((len(infiles))/3))).astype(np.int64)):
            print("Reading batch " + str(n))
            readData(infiles[n*3:(n+1)*3])
            if batchVar[0].get():
                plotMethodB(isclvs[isclv.get()],PrOP[0],"Intensity",0,n,"I")
            if batchVar[1].get():
                plotMethodB(rsclvs[rsclv.get()],PrOP[1],"Retardance",1,n,"R")
            if batchVar[2].get():
                plotMethodB(asclvs[asclv.get()],PrOP[2],"Angle",2,n,"A")
            if batchVar[3].get():
                plotCombinedMethodB(n)

#Paste reading
def PasteCallback():
    root.filename =  filedialog.askopenfilenames(initialdir = "./",title = "Select files")
    print (sorted(root.tk.splitlist(root.filename)))
    paste(sorted(root.tk.splitlist(root.filename)))
        
#Batch reading
def BatchCallback():
    root.filename =  filedialog.askopenfilenames(initialdir = "./",title = "Select files")
    enablePlots()
    readBatch(sorted(root.tk.splitlist(root.filename)))
    

#More interface control    
def enableOptions(i,coloropt,resopt):
    if (i == 0):
        ll.configure(text=textop[0])
        for n in range(3):
            datopt[n].configure(state="normal")
            resopt[n].configure(state=("disabled"))
    else:
        ll.configure(text=textop[1])
        for n in range(3):
            datopt[n].configure(state="disabled")
            resopt[n].configure(state="normal")
            
            
#The below mess simply constructs the interface. It is very ad hoc, as can be seen. At some point I will
#probably rewrite it from scratch!

root = Tk()

csize=StringVar()
calph=StringVar()
#root.title =("Bireftest")
mainframe = ttk.Frame(root, padding="1 1 10 2")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)
root.geometry("1280x768")
tvar = StringVar()
avar = StringVar()
ttk.Label(mainframe, text="BG brightness\n(0 = black,\n255 = white)").grid(column=0, row=28, sticky=(W))
gsc_entry = ttk.Entry(mainframe, width=7, textvariable=tvar)
gsc_entry.grid(column=0, row=27, sticky=(W, E))
gsc_entry.insert(0,"70")
ttk.Label(mainframe, text="Min Alpha\n0 = transparent\n1 = opaque").grid(column=1, row=28, sticky=(W))
a_entry = ttk.Entry(mainframe, width=7, textvariable=avar)
a_entry.grid(column=1, row=27, sticky=(W, E))
a_entry.insert(0,"0")
b_entry = ttk.Entry(mainframe, width=7, textvariable=csize)
b_entry.grid(column=0, row=29, sticky=(W, E))
b_entry.insert(0,"12.0")
c_entry = ttk.Entry(mainframe, width=7, textvariable=calph)
c_entry.grid(column=1, row=29, sticky=(W, E))
c_entry.insert(0,"0.3")
ttk.Label(mainframe, text="Color wheel dot size \n (adjust for desired plot size)").grid(column=0, row=30, sticky=(W))
ttk.Label(mainframe, text="Color wheel dot alpha \n (adjust for desired plot size)").grid(column=1, row=30, sticky=(W))
c = [[] for n in range(4)]
coloropt=[0,0,0,0]
cvar = IntVar()
color = ["Red", "Orange", "Green", "Blue"]
ttk.Label(mainframe, text="LED Color").grid(column=3, row=1, sticky=(W))
for n in range(4):
    coloropt[n] = ttk.Radiobutton(mainframe, text=color[n],value=n,variable=cvar)
    coloropt[n].grid(column=3,pady=10, row=n+2, sticky=W)
rVar = IntVar()
res = ["2x", "10x", "20x"]
resopt=[0,0,0]
ttk.Label(mainframe, text="Lens").grid(column=2, row=1, sticky=(W))
for n in range(3):
    resopt[n] = ttk.Radiobutton(mainframe, text=res[n],value=n,variable=rVar,state=DISABLED)
    resopt[n].grid(column=2, row=n+2,pady=10, sticky=W)
datVar = [IntVar() for n in range(3)]
dattext = ["Intensity", "Retardance", "AngleOfFastAxis"]
datopt=[0,0,0]
ttk.Label(mainframe, text="Data type").grid(column=1, row=1, sticky=(W))
for n in range(3):
    datopt[n] = ttk.Checkbutton(mainframe, text=dattext[n],variable=datVar[n],state=NORMAL)
    datopt[n].grid(column=1, row=n+2,pady=10, sticky=W)
    datopt[n].invoke()
iVar = IntVar()
dtb=[0,0]
pButton = [0,0,0,0]
inp = ["Processed","Raw"]
ttk.Label(mainframe, text="Input data type").grid(column=0, row=1, sticky=(W))
ttk.Label(mainframe, text="Min").grid(column=1, row=7, sticky=(W))
ttk.Label(mainframe, text="Max").grid(column=2, row=7, sticky=(W))
for n in range(2):
    dtb[n] = ttk.Radiobutton(mainframe, text=inp[n],value=n,variable=iVar,command=(lambda: enableOptions(iVar.get(),coloropt,resopt)))
    dtb[n].grid(column=0, row=n+2,pady=10, sticky=W)
savVar = IntVar()
sbt = ttk.Checkbutton(mainframe, text="Save plots as?",variable=savVar,state=NORMAL)
four_var = IntVar()
fbt = ttk.Checkbutton(mainframe, text="Plot Pasting correlations?",variable=four_var,state=NORMAL)
fbt.grid(column=3,row=36,sticky=W)
sbt.grid(column=5,row=0,sticky=W)
savText = StringVar(root)
wVar=IntVar()
wbt = ttk.Checkbutton(mainframe, text="Angle wheel plot?",variable=wVar,state=NORMAL)
wbt.grid(column=4,row=3,sticky=W)
wbt.invoke()
qvar=IntVar()
qbt = ttk.Checkbutton(mainframe, text="Retardance in nm?",variable=qvar,state=NORMAL)
qbt.grid(column=4,row=4,sticky=W)

choices = { '.png','.eps','.tiff','.svg','.dat'}
savText.set('.png') # set the default option
popupMenu = OptionMenu(mainframe, savText, *choices)
popupMenu.grid(column=5,row=1,sticky=W)
loadText = StringVar(root)
lbt = ttk.Label(mainframe, text="Load plots as:")
lbt.grid(column=4,row=0,sticky=W)
choices2 = { '.csv','.dat','.bin','bin (Hinds)'}
loadText.set('.csv') # set the default option
popup2Menu = OptionMenu(mainframe, loadText, *choices2)
popup2Menu.grid(column=4,row=1,sticky=W)
fname = StringVar()
sbe = ttk.Entry(mainframe,width=7,textvariable=fname)
sbe.insert(0,"output_")
sbe.grid(column=5,row=2,sticky=W)
ttk.Label(mainframe, text="Intensity Plot").grid(column=0, row=8, sticky=(W))
ilim=[StringVar(),StringVar()]
ilime1 = ttk.Entry(mainframe, width=7, textvariable=ilim[0])
ilime1.grid(column=1, row=8, sticky=(W, E))
ilime1.insert(0,"0")
ilime2 = ttk.Entry(mainframe, width=7, textvariable=ilim[1])
ilime2.grid(column=2, row=8, sticky=(W, E))
ilime2.insert(0,"1")
iscl = ["Spectral","Grayscale","RedYellowBlue"]
isclv = IntVar()
isclvs = ["Spectral","Greys_r","RdYlBu"]
IntScale=[0,0,0]
for n in range(3):
    IntScale[n] = ttk.Radiobutton(mainframe, text=iscl[n],value=n,variable=isclv,state=NORMAL)
    IntScale[n].grid(column=n, row=9,pady=10, sticky=W)
ttk.Label(mainframe, text="Retardance Plot").grid(column=0, row=15, sticky=(W))
rlim=[StringVar(),StringVar()]
rlime1 = ttk.Entry(mainframe, width=7, textvariable=rlim[0])
rlime1.grid(column=1, row=15, sticky=(W, E))
rlime1.insert(0,"0")
rlime2 = ttk.Entry(mainframe, width=7, textvariable=rlim[1])
rlime2.grid(column=2, row=15, sticky=(W, E))
rlime2.insert(0,str(math.pi))
pButton[0] = ttk.Button(mainframe, text="Plot!", command=(lambda: plotMethod(isclvs[isclv.get()],PrOP[0],"Intensity",0)),state=DISABLED)
pButton[0].grid(column=3, row=8, sticky=S)
rscl = ["Spectral","Grayscale","PurpleBlueGreen"]
rsclv = IntVar()
rsclvs = ["Spectral","Greys","PuBuGn"]
RetScale=[0,0,0]
for n in range(3):
    RetScale[n] = ttk.Radiobutton(mainframe, text=rscl[n],value=n,variable=rsclv,state=NORMAL)
    RetScale[n].grid(column=n, row=16,pady=10, sticky=W)
ttk.Label(mainframe, text="Angle Plot").grid(column=0, row=22, sticky=(W))
alim=[StringVar(),StringVar()]
alime1 = ttk.Entry(mainframe, width=7, textvariable=alim[0])
alime1.grid(column=1, row=22, sticky=(W, E))
alime1.insert(0,str(-90))
alime2 = ttk.Entry(mainframe, width=7, textvariable=alim[1])
alime2.grid(column=2, row=22, sticky=(W, E))
alime2.insert(0,90)
pButton[1] = ttk.Button(mainframe, text="Plot!", command=(lambda: plotMethod(rsclvs[rsclv.get()],PrOP[1],"Retardance",1)),state=DISABLED)
pButton[1].grid(column=3, row=15, sticky=S)
ascl = ["Spectral","Soft spectral","Twilight"]
asclv = IntVar()
asclvs = [hsshift,softmap,plt.cm.get_cmap("twilight",256)]
AngScale=[0,0,0]
for n in range(3):
    AngScale[n] = ttk.Radiobutton(mainframe, text=ascl[n],value=n,variable=asclv,state=NORMAL)
    AngScale[n].grid(column=n, row=23,pady=10, sticky=W)
pButton[2] = ttk.Button(mainframe, text="Plot!", command=(lambda: plotMethod(asclvs[asclv.get()],PrOP[2],"Angle of Fast Axis",2)),state=DISABLED)
pButton[2].grid(column=3, row=22, sticky=S)
ttk.Label(mainframe, text="Combined").grid(column=0, row=24, sticky=(W))
cscl = ["Int + Ret", "Int + Angle", "Angle + Ret","Angle + Ret + Int"]
csclv = IntVar()
cScale=[0,0,0,0]
for n in range(4):
    cScale[n] = ttk.Radiobutton(mainframe, text=cscl[n],value=n,variable=csclv,state=NORMAL)
    cScale[n].grid(column=n, row=25,pady=10, sticky=W)
batchVar = [IntVar() for n in range(4)]
batchopt=[0,0,0,0]
batchrows=[8,15,22,24]
ttk.Label(mainframe, text="Data type").grid(column=1, row=1, sticky=(W))
for n in range(4):
    batchopt[n] = ttk.Checkbutton(mainframe, text="Add to batch",variable=batchVar[n],state=NORMAL)
    batchopt[n].grid(column=4, row=batchrows[n],pady=10, sticky=W)
pasteVar = [IntVar() for n in range(4)]
pasteopt=[0,0,0,0]
pasterows=[8,15,22,24]
for n in range(4):
    pasteopt[n] = ttk.Checkbutton(mainframe, text="Add to pastejob",variable=pasteVar[n],state=NORMAL)
    pasteopt[n].grid(column=5, row=pasterows[n],pady=10, sticky=W)


pButton[3] = ttk.Button(mainframe, text="Plot!", command=(lambda: plotCombinedMethod()),state=DISABLED)
pButton[3].grid(column=3, row=24, sticky=S)


textop = ["Click open and select the processed data files you have chosen.\nThey should be named e.g. *Intensity.csv, *Retardance.csv, and *AngleOFFastAxis.csv where * is arbitrary.\nAlternatively, you can give them any way which sorts them in the same alphabetical order (e.g. *I.csv, *R.csv, *A.csv).","Click open and select the 7 raw data files. \nThey must be named so that they can be sorted \n(e.g., name_0.csv to name_6.csv)"]
ll = ttk.Label(mainframe, text=textop[0])
ll.grid(column=0, row=35,columnspan=6, sticky=(W))
b = ttk.Button(mainframe, text="Open", command=(lambda: callback())).grid(column=4, row=2, sticky=W)
b2 = ttk.Button(mainframe, text="Batch Plot", command=(lambda: BatchCallback())).grid(column=2, padx=0, row=27,pady=10, sticky=W)
ttk.Label(mainframe, text="Batch name prefix").grid(column=2, padx=0,ipady=10, row=29, sticky=(W))
b3 = ttk.Button(mainframe, text="Paste plot", command=(lambda: PasteCallback())).grid(column=3, padx=0, row=27,pady=10, sticky=W)
sv = StringVar()
psc_entry = ttk.Entry(mainframe, width=7, textvariable=sv)
psc_entry.grid(column=3, row=28, sticky=(W, E))
psc_entry.insert(0,"2,20")
fourier_label=ttk.Label(mainframe,text="Fourier Upscale Factors")
fourier_label.grid(column=3,row=29,sticky=W)
indpvar = StringVar()
insc_entry = ttk.Entry(mainframe, width=7, textvariable=indpvar)
insc_entry.grid(column=4, row=28, sticky=(W, E))
insc_entry.insert(0,"2048,2048")
fourier_label=ttk.Label(mainframe,text="Initial pasting position")
fourier_label.grid(column=4,row=29,sticky=W)
bvar=StringVar()
gsc_entry = ttk.Entry(mainframe, width=5, textvariable=bvar)
gsc_entry.grid(column=2, row=28, ipady=0, sticky=(W, E))
gsc_entry.insert(0,"BATCH")
atvar = StringVar()
angle_entry = ttk.Entry(mainframe,width=7,textvariable=atvar)
angle_entry.grid(column=2,row=5,sticky=W)
angle_entry.insert(0,"0.0")
angle_label=ttk.Label(mainframe,text="Angle adjustment (degrees):")
angle_label.grid(column=1,row=5,sticky=W)


for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)
mainloop()
