/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: BondType.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:


\******************************************************************************/

#ifndef CHAMPION_BOND_TYPE_HPP
#define CHAMPION_BOND_TYPE_HPP

namespace champion {

/***************************** Class declarations *****************************/

// Forward declarations
template <level_type n> class ObjectRegistry;
template <level_type n, level_type level> class ForceCenterType;

template <level_type n, level_type level> class BondType {
  public:
    // Public types
    using constituent_type = ForceCenterType<n, level>;
    using iterator = RegistryIterator<BondType<n, level>, false>;
    using const_iterator = RegistryIterator<BondType<n, level>, true>;

    template <level_type ll>
    using bt_index = ObjectIndex<::champion::BondType, n, ll>;
    template <level_type ll>
    using fct_index = ObjectIndex<ForceCenterType, n, ll>;
    // Public member functions
    // Construct from ObjectRegistry, ID and constituents
    BondType(ObjectRegistry<n> &reg, bt_index<level> ID, fct_index<level> fct1,
             fct_index<level> fct2)
        : reg_{&reg}, id_{ID}, constituents_{fct1, fct2} {
        if (constituents_[1] < constituents_[0]) {
            std::swap(constituents_[0], constituents_[1]);
        }
    }

    // Access

    // Index access
    static constexpr size_type size() { return 2; }
    bt_index<level> get_id() const { return id_; }
    fct_index<level> get_index(size_type i) const { return constituents_[i]; }
    fct_index<level> first() const { return constituents_[0]; }
    fct_index<level> second() const { return constituents_[1]; }

    // Object access
    iterator begin() { return iterator{*this}; }
    iterator end() { return iterator{*this, size()}; }
    const_iterator begin() const { return const_iterator{*this, size()}; }
    const_iterator end() const { return const_iterator{*this, size()}; }
    const_iterator cbegin() const { return const_iterator{*this}; }
    const_iterator cend() const { return const_iterator{*this, size()}; }

    const ForceCenterType<n, level> &operator[](size_type i) const {
        return reg_->template get_object<ForceCenterType, level>(
            constituents_[i]);
    }
    ForceCenterType<n, level> &operator[](size_type i) {
        return const_cast<ForceCenterType<n, level> &>(
            static_cast<const BondType &>(*this)[i]);
    }

  private:
    // Private data members
    ObjectRegistry<n> *reg_;
    bt_index<level> id_;
    std::vector<fct_index<level>> constituents_;
};

template <level_type n, level_type level>
bool operator==(const BondType<n, level> &bt1, const BondType<n, level> &bt2) {
    assert(bt1.size() == bt2.size());
    for (size_type i = 0; i != bt1.size(); ++i) {
        if (bt1[i] != bt2[i]) {
            return false;
        }
    }
    return true;
}

template <level_type n, level_type level>
bool operator!=(const BondType<n, level> &bt1, const BondType<n, level> &bt2) {
    return !(bt1 == bt2);
}

template <level_type n, level_type level>
std::ostream &print_brief(std::ostream &os, const BondType<n, level> &bt) {
    os << "BondType<" << level << "> #" << bt.get_id();
    return os;
}

template <level_type n, level_type level>
std::ostream &print_full(std::ostream &os, const BondType<n, level> &bt) {
    print_brief(os, bt) << ": {";
    print_brief(os, bt[0]) << ", ";
    print_brief(os, bt[1]) << "}";
    return os;
}

template <level_type n, level_type level>
std::ostream &print_recursive(std::ostream &os, const BondType<n, level> &bt) {
    os << "BondType<" << level << "> #" << bt.id() << ": {";
    print_recursive(os, bt[0]) << ", ";
    print_recursive(os, bt[1]) << "}";
    return os;
}

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os, const BondType<n, level> &bt) {
    print_full(os, bt);
    return os;
}

} // namespace champion

#endif
