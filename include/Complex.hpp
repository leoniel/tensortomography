#ifndef CHAMPION_COMPLEX_HPP
#define CHAMPION_COMPLEX_HPP

#include <cmath>
#include <iostream>
#include <stdexcept>
#include <utility>

#include "Point.hpp"
#include "Vector.hpp"
#include "algorithm.hpp"
#include "set_precision.hpp"
#include "type_inference.hpp"

namespace champion {

// Forward declarations

template <typename T> class Complex {
    public:
    using iterator = T *;
    using const_iterator = const T *;
    // Default constructor
    Complex() : c0{0}, c1{0} {}
    // Copy constructor
    Complex(const Complex &) = default;
    // Copy assignment
    Complex &operator=(const Complex &) = default;
    // Move constructor
    Complex(Complex &&) = default;
    // Move assignment
    Complex &operator=(Complex &&) = default;
    // Generalized copy constructor
    template <typename T2>
    Complex(const Complex<T2> &C)
        : c0{static_cast<T>(C.c0)}, c1{static_cast<T>(C.c1)} {}
    // Generalized copy assignment
    template <typename T2> Complex &operator=(const Complex<T2> &C) {
        for (size_type i = 0; i != this->size(); ++i){
            (*this)[i] = C[i];
        }
        return *this;
    }
    // Construct from components
    Complex(T c0_, T c1_) : c0{c0_}, c1{c1_} {}

    // Access functions
    static constexpr size_type size() { return 2; }
    // Iterators to support ranges
    iterator begin() { return &c0; }
    iterator end() { return &c1 + 1; }
    const_iterator begin() const { return &c0; }
    const_iterator end() const { return &c1 + 1; }
    const_iterator cbegin() { return &c0; }
    const_iterator cend() { return &c1 + 1; }
    // Read-write access to non-const Complex<T>
    T &operator[](size_type i) { return *(begin() + i); }
    // Read-only access to const Complex<T>
    const T &operator[](size_type i) const { return *(begin() + i); }

    //Mathematical operation member funcions
    // Unary operations
    Complex<T> normalize() const;
    Square_type<T> squared_magnitude() const;
    T modulus() const;
    T argument() const;
    Complex<T> compl_conj() const;
    T Re() const;
    T Im() const;
    //Do inverse and angle make sense?
    Complex<T> exp() const;
    Complex<T> compl_exp() const;

    // Binary operations with another Complex
    template <typename T2>
    Complex<Product_type<T2, T, Inverse_type<T2>>>
    conjugate(const Complex<T2> &C) const;

    // Public data members
    T c0, c1;
}; 

// Forward declaration 
template <typename T>
std::ostream &operator<<(std::ostream &os, const Complex<T> &C);

// Mathematical operations on Complex<T>

//Unary operators
template <typename T> Complex<T> operator-(const Complex<T> &C) {
    Complex<T> res = C;
    for (auto &c : res) {
        c = -c;
    }
    return res;
}
// Forward declaration needed for squared_magnitude()
template <typename T1, typename T2>
Complex<Product_type<T1, T2>> operator*(const Complex<T1> &C1,
                                        const Complex<T2> &C2);

template <typename T> Square_type<T> squared_magnitude(const Complex<T> &C) {
    Square_type<T> res = 0;
    for (auto c : C) {
        res += c * c;
    }
    return res;
}
template <typename T> Square_type<T> Complex<T>::squared_magnitude() const {
    return ::champion::squared_magnitude(*this);
}
template <typename T> T modulus(const Complex<T> &C) {
    return sqrt(squared_magnitude(C));
}
template <typename T> T Complex<T>::modulus() const {
    return ::champion::modulus(*this);
}
template <typename T> T argument(const Complex<T> &C) {
    return atan(C.c1/C.c0);
}
template <typename T> T Complex<T>::argument() const {
    return ::champion::argument(*this);
}

template <typename T> Complex<T> exp(const Complex<T> &C) {
    Complex<T> res;
    auto theta = C.argument();
    auto r = C.modulus();
    res.c0 = cos(theta);
    if(C.c1 < 0){
        res.c1 = sin(theta);
    }
    else{
        res.c1 = -sin(theta);
    }
    res = r*res;
    return res;
}
template <typename T> Complex<T> compl_exp(const T & theta) {
    Complex<T> res;
    res.c0 = cos(theta);
    if(theta < 0){
        res.c1 = sin(theta);
    }
    else{
        res.c1 = -sin(theta);
    }
    //res = r*res;
    return res;
}
// Forward declaration needed for normalize()
template <typename T1, typename T2>
Complex<Quotient_type<T1, T2>> operator/(const Complex<T1> &C, T2 x);

template <typename T> Complex<T> normalize(const Complex<T> &C) {
    auto mag = C.magnitude();
    if (mag == 0) {
        return C;
    }
    return Complex<T>{C} / mag;
}
template <typename T> Complex<T> Complex<T>::normalize() const {
    return ::champion::normalize(*this);
}
template <typename T> Complex<T> compl_conj(const Complex<T> &C) {
    auto res = C;
    res[1] = -res[1];
    return res;
}
template <typename T> Complex<T> Complex<T>::compl_conj() const {
    return ::champion::compl_conj(*this);
}
template <typename T> T Re(const Complex<T> &C) {
    return C.c0;
}
template <typename T> T Complex<T>::Re() const {
    return ::champion::Re(*this);
}
template <typename T> T Im(const Complex<T> &C) {
    return C.c1;
}
template <typename T> T Complex<T>::Im() const {
    return ::champion::Im(*this);
}
// Arithmetic operators

// Addition of Complex<T>s
template <typename T1, typename T2>
Complex<T1> operator+=(Complex<T1> &C1, const Complex<T2> &C2) {
    for (size_type i = 0; i != C1.size(); ++i){
        C1[i] += C2[i];
    }
    return C1;
}
template <typename T1, typename T2>
Complex<Sum_type<T1, T2>> operator+(const Complex<T1> &C1,
                                    const Complex<T2> &C2) {
    Complex<Sum_type<T1, T2>> res;
    for(size_type i = 0; i != res.size(); ++i) {
        res[i] = C1[i] + C2[i];
    }
    return res;
}
// Subtraction
template <typename T1, typename T2>
Complex<T1> operator-=(Complex<T1> &C1, const Complex<T2> &C2) {
    return C1 += -C2;
}
template <typename T1, typename T2>
Complex<Sum_type<T1, T2>> operator-(const Complex<T1> &C1,
                                    const Complex<T2> &C2) {
    return C1 + -C2;
}
// Multiplication
template <typename T1, typename T2>
Complex<Product_type<T1, T2>> operator*(const Complex<T1> &C,
                                        const Complex<T2> &Z) {
    Complex<Product_type<T1, T2>> C_res;
    C_res[0] = C[0]*Z[0] - C[1]*Z[1];
    C_res[1] = C[0]*Z[1] + C[1]*Z[0];
}

// Logical operators
template <typename T1, typename T2>
bool operator==(const Complex<T1> &C, const Complex<T2> &Z) {
    auto C_it = C.begin(), Z_it = Z.begin();
    while (C_it != C.end()) {
        if(*C_it++ != *Z_it++) {
            return false;
        }
    }
    return true;
}
template <typename T1, typename T2>
bool operator!=(const Complex<T1> &C, const Complex<T2> &Z) {
    return !(C == Z);
}

// Scaling of Complex<T>s by numeric types
template <typename T1, typename T2>
Complex<T1> operator*=(Complex<T1> &C, T2 r) {
    for (auto &ci : C) {
        ci *= r;
    }
    return C;
}
template <typename T1, typename T2>
Complex<Product_type<T1, T2>> operator*(const Complex<T1> &C, T2 r) {
    Complex<Product_type<T1, T2>> res;
    for (size_type i = 0; i != res.size(); ++i) {
        res[i] = C[i] * r;
    }
    return res;
}
template <typename T1, typename T2>
Complex<Product_type<T1, T2>> operator*(T1 r, const Complex<T2> &C) {
    return C * r;
}
template <typename T1, typename T2>
Complex<T1> operator/=(Complex<T1> &C, T2 r) {
    return C *= (1 / r);
}
template <typename T1, typename T2>
Complex<Quotient_type<T1, T2>> operator/(const Complex<T1> &C, T2 r) {
    return C * (1 / r);
}

} // namespace champion
#endif
