#ifndef CHAMPION_GEOMETRY_HPP
#define CHAMPION_GEOMETRY_HPP

#include <functional>
#include <iostream>
#include <memory>
#include <vector>

#include "CostFunction.hpp"
#include "Function.hpp"
#include "Matrix.hpp"
#include "Optimizer.hpp"
#include "Point.hpp"
#include "Quaternion.hpp"
#include "Rotation.hpp"
#include "Transformation.hpp"
#include "Vector.hpp"
#include "algorithm.hpp"
#include "set_precision.hpp"

namespace champion {

template <typename T> class Geometry {
  public:
    // Public member types
    using iterator = typename std::vector<Vector<T>>::iterator;
    using const_iterator = typename std::vector<Vector<T>>::const_iterator;
    // Default-construct from size
    Geometry(size_type n) : coordinates_(n), center_{} {}
    // Construct from vector of Vectors and Vector center
    Geometry(std::vector<Vector<T>> positions = {}, Vector<T> center = {})
        : coordinates_{positions}, center_{center} {}
    // Construct from vector of Points and vector of masses
    Geometry(const std::vector<Point<T>> &points,
             const std::vector<real> &masses)
        : coordinates_{}, center_{0, 0, 0} {
        auto center = compute_center_of_mass(points, masses);
        for (auto &P : points) {
            coordinates_.push_back(Vector{P - center_});
        }
    }

    // Access
    size_type size() const { return coordinates_.size(); }
    const std::vector<Vector<T>> &get_coordinates() const {
        return coordinates_;
    }
    Vector<T> get_center() const { return center_; }
    // Iterators to support range based operations
    Vector<T> *begin() { return &*coordinates_.begin(); }
    Vector<T> *end() { return &*coordinates_.end(); }
    const Vector<T> *begin() const { return &*coordinates_.cbegin(); }
    const Vector<T> *end() const { return &*coordinates_.cend(); }
    // Read-write access to non-const Geometry
    Vector<T> &operator[](size_type i) { return coordinates_[i]; }
    // Read-only access to const Geometry
    const Vector<T> &operator[](size_type i) const { return coordinates_[i]; }

    // Modifications
    void push_back(Vector<T> v) { coordinates_.push_back(v); }
    template <typename T2> void set_center(const Vector<T2> &p) { center_ = p; }

    template <typename T2, typename T3>
    Geometry rotate(const Rotation<T2> &rot, const Vector<T3> &center) const {
        Geometry g = *this;
        for (Vector<T> &p : g) {
            p = p.rotate(rot, center);
        }
        return g;
    }
    template <typename T2> Geometry rotate(const Rotation<T2> &rot) const {
        return rotate(rot, center_);
    }

    template <typename T2> Geometry transform(Transformation<T2> t);

    template <typename T2>
    Transformation<T> match_orientation(const Geometry<T2> &g) const;

  private:
    // Private data members
    std::vector<Vector<T>> coordinates_;
    Vector<T> center_;
};

// Mathematical & geometric operations

// Unary operators

// Invert coordinates
template <typename T> Geometry<T> operator-(const Geometry<T> &g) {
    Geometry<T> g_res = g;
    for (auto &gi : g_res) {
        gi = -gi;
    }
    g_res.set_center(-g.get_center());
    return g_res;
}

// Binary operators

// "Vectorial" difference between two geometries
// (Geometry of Vectors representing the difference between each pair of
// Vectors)
template <typename T1, typename T2>
Geometry<T1> operator-(const Geometry<T1> &g1, const Geometry<T2> &g2) {
    Geometry<T1> diff = g1;
    for (size_type i = 0; i != size(g1); ++i) {
        diff[i] = g1[i] - g2[i];
    }
    diff.set_center(g1.get_center() - g2.get_center());
    return diff;
}

// Scalar squared distance between two geometries
template <typename T1, typename T2>
real squared_distance(const Geometry<T1> &g1, const Geometry<T2> &g2) {
    auto diff = g1 - g2;
    size_type sq_dist = 0;
    for (auto di : diff) {
        sq_dist += squared_magnitude(di);
    }
    return sq_dist;
}

// Scalar distance between two geometries
template <typename T1, typename T2>
T1 distance(const Geometry<T1> &g1, const Geometry<T2> &g2) {
    return std::sqrt(squared_distance(g1, g2));
}

// Translation
template <typename T1, typename T2>
Geometry<T1> operator+=(Geometry<T1> &g, const Vector<T2> &p) {
    for (auto &gi : g) {
        gi += p;
    }
    g.set_center(g.get_center() + p);
    return g;
}
template <typename T1, typename T2>
Geometry<T1> operator+(const Geometry<T1> &g, const Vector<T2> &p) {
    Geometry<T1> g_res = g;
    return g_res += p;
}
template <typename T1, typename T2>
Geometry<T1> operator+(const Vector<T1> &p, const Geometry<T2> &g) {
    return g + p;
}
template <typename T1, typename T2>
Geometry<T1> operator-=(Geometry<T1> &g, const Vector<T2> &p) {
    return g += -p;
}
template <typename T1, typename T2>
Geometry<T1> operator-(const Geometry<T1> &g, const Vector<T2> &p) {
    Geometry<T1> g_res = g;
    return g_res -= p;
}
template <typename T1, typename T2>
Geometry<T1> operator-(const Vector<T1> &p, const Geometry<T2> &g) {
    return -g + p;
}

// Coordinate scaling
template <typename T1, typename T2>
Geometry<T1> operator*=(Geometry<T1> &g, T2 r) {
    for (auto &gi : g) {
        gi *= r;
    }
    g.set_center(g.get_center() * r);
    return g;
}
template <typename T1, typename T2>
Geometry<T1> operator*(const Geometry<T1> &g, T2 r) {
    Geometry<T1> g_res = g;
    return g_res *= r;
}
template <typename T1, typename T2>
Geometry<T1> operator*(T1 r, const Geometry<T2> &g) {
    return g * r;
}
template <typename T1, typename T2>
Geometry<T1> operator/=(Geometry<T1> &g, T2 r) {
    return g *= (1 / r);
}
template <typename T1, typename T2>
Geometry<T1> operator/(const Geometry<T1> &g, T2 r) {
    Geometry<T1> g_res = g;
    return g_res /= r;
}

template <typename T1, typename T2>
bool operator==(const Geometry<T1> &g1, const Geometry<T2> &g2) {
    return (g1.get_coordinates() == g2.get_coordinates() &&
            g1.get_center() == g2.get_center());
}

template <typename T1, typename T2>
bool operator!=(const Geometry<T1> &g1, const Geometry<T2> &g2) {
    return !(g1 == g2);
}
template <typename T>
std::ostream &operator<<(std::ostream &os, const Geometry<T> &g) {
    auto coords = g.get_coordinates();
    os << "{";
    for (auto it = coords.begin(); it != coords.end() - 1; ++it) {
        os << *it << ", ";
    }
    os << coords[coords.size() - 1];
    os << "}";
    return os;
}

template <typename T1>
template <typename T2>
Geometry<T1> Geometry<T1>::transform(Transformation<T2> t) {
    return this->rotate(t.rotation()) + t.translation();
}

template <typename T1>
template <typename T2>
Transformation<T1>
Geometry<T1>::match_orientation(const Geometry<T2> &g) const {
    auto N = this->size();
    assert(g.size() == N);
    // Calculate translational part of transformation
    auto dr = g.center_ - this->center_;
    // Create copies centered at the origin
    Geometry<T1> g1 = *this - center_;
    Geometry<T1> g2 = g - g.center_;
    // Map all points of both geometries to the unit sphere around their centers
    for (auto &p : g1) {
        p = p.normalize();
    }
    for (auto &p : g2) {
        p = p.normalize();
    }
    // Compute all displacements from g1 to g2
    auto displacements = std::vector<Vector<T1>>(N);
    for (size_type i = 0; i != N; ++i) {
        displacements[i] = g2[i] - g1[i];
    }
    auto axis_estimates = std::vector<Vector<T1>>(N);
    for (size_type i = 0; i != N; ++i) {
        axis_estimates[i] =
            cross(displacements[i], displacements[(i + 1) % N]).normalize();
    }
    auto axis = Vector<T1>{0, 0, 0};
    for (size_type i = 0; i != N; ++i) {
        axis += axis_estimates[i];
    }
    axis = axis.normalize();
    // Compute the best estimate for the angle rotated through the
    // determined rotational angle by projecting the
    auto angles = std::vector<T1>{};
    for (size_type i = 0; i != N; ++i) {
        // Project all points on the unit circle parallell to the rotational
        // axis
        Vector<T1> zero = {0, 0, 0};
        Vector<T1> a = g1[i];
        Vector<T1> b = g2[i];
        if (a == zero || b == zero) {
            continue;
        }
        a -= axis * dot(a, axis);
        a = a.normalize();
        b -= axis * dot(b, axis);
        b = b.normalize();
        angles.push_back(std::acos(dot(a, b)));
    }
    auto angle = std::accumulate(angles.begin(), angles.end(), 0.0);
    angle /= angles.size();
    auto R = Rotation<T1>{axis, angle};
    return Transformation<T1>{dr, R};
}

} // namespace champion

#endif
