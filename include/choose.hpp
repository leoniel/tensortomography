/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: RegistryIterator.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-27
MODIFIED BY:

DESCRIPTION: Compile-time dispatch of a type or value depending on the truth
value of a compile-time evaluable predicate. Note that choose_type is equivalent
to the standard library's conditional, but in my opinion better named. As far as
I know there is no direct equivalent to choose_value in the standard library.

\******************************************************************************/

#ifndef CHAMPION_CHOOSE_HPP
#define CHAMPION_CHOOSE_HPP

/***************************** Class declarations *****************************/

namespace champion {

// Type dispatch

template <bool predicate, typename true_type, typename false_type>
struct choose_type;

template <typename true_type, typename false_type>
struct choose_type<true, true_type, false_type> {
    using type = true_type;
};

template <typename true_type, typename false_type>
struct choose_type<false, true_type, false_type> {
    using type = false_type;
};

// Convenience wrapper
template <bool predicate, typename true_type, typename false_type>
using Choose_type =
    typename choose_type<predicate, true_type, false_type>::type;

// Value dispatch

template <typename T, bool predicate, T true_value, T false_value>
struct choose_value;

template <typename T, T true_value, T false_value>
struct choose_value<T, true, true_value, false_value> {
    static constexpr T value = true_value;
};

template <typename T, T true_value, T false_value>
struct choose_value<T, false, true_value, false_value> {
    static constexpr T value = false_value;
};

// Convenience wrapper
template <typename T, bool predicate, T true_value, T false_value>
auto constexpr Choose_value =
    choose_value<T, predicate, true_value, false_value>::value;

} // namespace champion

#endif
