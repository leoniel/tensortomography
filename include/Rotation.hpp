#ifndef CHAMPION_ROTATION_HPP
#define CHAMPION_ROTATION_HPP

#include <cmath>
#include <iostream>
#include <memory>

#include "Point.hpp"
#include "Quaternion.hpp"
#include "Vector.hpp"
#include "algorithm.hpp"
#include "random.hpp"
#include "set_precision.hpp"
#include "units.hpp"

namespace champion {

// Forward declarations
template <typename T> class Rotation;
template <typename T1, typename T2>
Rotation<T1> operator+=(Rotation<T1> &R1, const Rotation<T2> &R2);

template <typename T> class Rotation {
  public:
    // Default constructor
    Rotation<T>() : rot_vec_{0, 0, 0} {}
    // Copy constructor
    Rotation(const Rotation &) = default;
    // Copy assignment
    Rotation &operator=(const Rotation &) = default;
    // Move constructor
    Rotation(Rotation &&) = default;
    // Move assignment
    Rotation &operator=(Rotation &&) = default;
    // Generalized copy constructor
    template <typename T2> Rotation(const Rotation<T2> &R) : rot_vec_{} {
        for (const auto &Ri : R) {
            rot_vec_.push_back(static_cast<T>(Ri));
        }
    }
    // Generalized copy assignment
    template <typename T2> Rotation &operator=(const Rotation<T2> &R) {
        rot_vec_ = {};
        for (const auto &Ri : R) {
            rot_vec_.push_back(static_cast<T>(Ri));
        }
        return *this;
    }
    // Construct from axis and angle
    Rotation<T>(Vector<T> axis, T angle) : rot_vec_{axis.normalize() * angle} {}
    // Construct from rotation vector
    Rotation<T>(Vector<T> v) : rot_vec_{v} {}
    // Construct from Quaternion<T>
    Rotation<T>(Quaternion<T> q) {
        auto angle = 2 * std::acos(q[0]);
        auto axis = Vector<T>(q[1], q[2], q[3]).normalize();
        rot_vec_ = axis * angle;
    }
    // Construct from rotation vector components
    Rotation<T>(T rx, T ry, T rz) : rot_vec_{rx, ry, rz} {}
    // Construct from iterator to first component
    template <typename iter> explicit Rotation<T>(iter it) {
        for (auto &xi : *this) {
            xi = *(it++);
        }
    }

    // Access
    const Vector<T> &rotation_vector() const { return rot_vec_; }
    Vector<T> &rotation_vector() { return rot_vec_; }
    Vector<T> axis() const { return rot_vec_.normalize(); }
    Dimensionless_type<T> angle() const { return rot_vec_.magnitude(); }
    Quaternion<T> quaternion() const {
        if (angle() == 0) {
            return {1, 0, 0, 0};
        }
        return Quaternion<T>{rot_vec_, angle()};
    }

    const T *begin() const { return rot_vec_.begin(); }
    const T *end() const { return rot_vec_.end(); }
    T *begin() { return rot_vec_.begin(); }
    T *end() { return rot_vec_.end(); }
    T operator[](size_type i) const { return rot_vec_[i]; }
    T &operator[](size_type i) { return rot_vec_[i]; }
    size_type size() const { return 3; }

    // Mathematical operation member functions
    // Unary operators
    Rotation<T> normalize() const;
    // Binary operators
    template <typename T2>
    Rotation<Product_type<T, T2>> rotate(const Rotation<T2> &R) const;

  private:
    // Private data members
    Vector<T> rot_vec_;
};

// Forward declaration to facilitate debugging
template <typename T>
std::ostream &operator<<(std::ostream &os, const Rotation<T> &R);

// Mathematical operators on Rotation<T>s

// Unary operators
template <typename T> Rotation<T> operator-(const Rotation<T> &R) {
    return Rotation<T>(-R.rotation_vector());
}
template <typename T> Rotation<T> Rotation<T>::normalize() const {
    Rotation<T> R = *this;
    R.rot_vec_ = R.rot_vec_.normalize();
    return R;
}
template <typename T> Rotation<T> normalize(const Rotation<T> &R) {
    return R.normalize();
}

template <typename T>
Rotation<T> random_orientation(RandomNumberGenerator &rng) {
    return Rotation<T>(random_direction<T>(rng),
                       uniform_real_distribution<T>{rng, 0, pi}());
}

// Binary operator between Rotations

// Logical operators
template <typename T1, typename T2>
bool operator==(const Rotation<T1> &R1, const Rotation<T2> &R2) {
    return R1.rotation_vector() == R2.rotation_vector();
}
template <typename T1, typename T2>
bool operator!=(const Rotation<T1> &R1, const Rotation<T2> &R2) {
    return !(R1 == R2);
}

// Arithmetic operators

// Note: Addition and subtraction of rotations are not commutative
// R1 + R2 is the net resulting rotation when rotating a target through R1
// followed by R2
// Negation reverses rotation direction

template <typename T1, typename T2>
Rotation<T1> operator+=(Rotation<T1> &R1, const Rotation<T2> &R2) {
    return R1 = Rotation<T1>(R2.quaternion() * R1.quaternion());
}
template <typename T1, typename T2>
Rotation<Product_type<T1, T2>> operator+(const Rotation<T1> &R1,
                                         const Rotation<T2> &R2) {
    Rotation<Product_type<T1, T2>> res = R1;
    return res += R2;
}
template <typename T1, typename T2>
Rotation<T1> operator-=(Rotation<T1> &R1, const Rotation<T2> &R2) {
    return R1 += -R2;
}
template <typename T1, typename T2>
Rotation<T1> operator-(const Rotation<T1> &R1, const Rotation<T2> &R2) {
    return R1 + -R2;
}

// Rotate R1 by R2
template <typename T1, typename T2>
Rotation<Product_type<T1, T2>> rotate(const Rotation<T1> &R1,
                                      const Rotation<T2> &R2) {
    return R1 + R2;
}
template <typename T1>
template <typename T2>
Rotation<Product_type<T1, T2>>
Rotation<T1>::rotate(const Rotation<T2> &R2) const {
    return ::champion::rotate(*this, R2);
}

// Scaling
template <typename T1, typename T2>
Rotation<T1> operator*=(Rotation<T1> &R, const T2 &x) {
    return R = Rotation<T1>(x * R.rotation_vector());
}
template <typename T1, typename T2>
Rotation<Product_type<T1, T2>> operator*(const Rotation<T1> &R, const T2 &x) {
    Rotation<Product_type<T1, T2>> res = R;
    return res *= x;
}
template <typename T1, typename T2>
Rotation<Product_type<T1, T2>> operator*(const T1 &x, const Rotation<T2> &R) {
    return R * x;
}
template <typename T1, typename T2>
Rotation<T1> operator/=(Rotation<T1> &R, const T2 &x) {
    return R *= (1 / x);
}
template <typename T1, typename T2>
Rotation<Quotient_type<T1, T2>> operator/(const Rotation<T1> &R, T2 x) {
    return Rotation<Quotient_type<T1, T2>>(R.rotation_vector() / x);
}

template <typename T1>
template <typename T2, typename T3>
Vector<Sum_type<Product_type<T2, Sum_type<T1, T3>, Inverse_type<T2>>, T3>>
Vector<T1>::rotate(const Rotation<T2> &rot, const Vector<T3> &center) const {
    if (::champion::squared_magnitude(rot) == 0) {
        return *this;
    }
    // Create copy shifted to center frame of reference
    auto v = *this - center;
    // Rotate using Quaternion<T> algebra
    v = conjugate(v, rot.quaternion());
    // Translate to original frame of reference
    v += center;
    return v;
}

template <typename T1>
template <typename T2>
Vector<Product_type<T2, T1, Inverse_type<T2>>>
Vector<T1>::rotate(const Rotation<T2> &rot) const {
    if (::champion::squared_magnitude(rot) == 0) {
        return *this;
    }
    // Rotate using Quaternion<T> algebra
    auto v = conjugate(*this, rot.quaternion());
    return v;
}

template <typename T1>
template <typename T2, typename T3>
Point<Sum_type<Product_type<T2, Sum_type<T1, T3>, Inverse_type<T2>>, T3>>
Point<T1>::rotate(const Rotation<T2> &rot, const Vector<T3> &center) const {
    if (::champion::squared_magnitude(rot) == 0) {
        return *this;
    }
    // Create copy shifted to center frame of reference
    auto p = *this - center;
    // Rotate using Quaternion<T> algebra
    p = conjugate(p, rot.quaternion());
    // Translate to original frame of reference
    p += center;
    return p;
}

template <typename T1>
template <typename T2, typename T3>
Point<Sum_type<Product_type<T2, Sum_type<T1, T3>, Inverse_type<T2>>, T3>>
Point<T1>::rotate(const Rotation<T2> &rot, const Point<T3> &center) const {
    for (size_type i = 0; i != this->size(); ++i) {
        check_bounds((*this)[i], center[i]);
    }
    auto center_vec = Vector<T3>{center};
    return this->rotate(rot, center_vec);
}

template <typename T1>
template <typename T2>
Point<Product_type<T2, T1, Inverse_type<T2>>>
Point<T1>::rotate(const Rotation<T2> &rot) const {
    if (::champion::squared_magnitude(rot) == 0) {
        return *this;
    }
    // Rotate using Quaternion<T> algebra
    auto p = conjugate(*this, rot.quaternion());
    return p;
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const Rotation<T> &R) {
    os << "{Angle: " << degrees(R.angle()) << ", Axis: " << R.axis() << "}";
    return os;
}

} // namespace champion

#endif
