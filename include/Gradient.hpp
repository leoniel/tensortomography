#ifndef CHAMPION_GRADIENT_HPP
#define CHAMPION_GRADIENT_HPP

#include <functional>

namespace champion {

template <typename grad_type, typename arg_type>
class Gradient {
public:
    friend Optimizer<grad_type, arg_type>;
    using func_type = std::function<real(arg_type)>;
    virtual ~Gradient() {}
    virtual grad_type operator()() = 0;
    virtual arg_type advance(grad_type direction, real step = delta_) = 0;
    virtual real directional_derivative(grad_type direction) = 0;
    inline void reset_arg(arg_type x) { x_ = x; }
    inline real eval_func(arg_type x) { return func_(x); }
    inline real eval_func() { return func_(x_); }
    inline real delta() const { return delta_; }

protected:
    Gradient(const func_type &func, arg_type &x0, real delta = 1e-6)
	: func_{func}, x_{x0}, delta_{delta}, f_val_{0} {}
    func_type func_;
    arg_type x_;
    real delta_;
};

}

#endif
