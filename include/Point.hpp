#ifndef CHAMPION_POINT_HPP
#define CHAMPION_POINT_HPP

#include <cassert>
#include <cmath>
#include <exception>
#include <iostream>
#include <limits>
#include <memory>
#include <utility>

#include "Range.hpp"
#include "Vector.hpp"
#include "set_precision.hpp"
#include "type_inference.hpp"

namespace champion {

// Forward declaration
template <typename T> class Rotation;

template <typename T> class Point {
  public:
    using iterator = Range<T> *;
    using const_iterator = const Range<T> *;

    // Constructors
    // Default constructor (creates Point<T> at the origin with maximum
    // numerically representable bounds
    explicit Point<T>()
        : x{std::numeric_limits<T>::lowest(), std::numeric_limits<T>::max(), 0},
          y{std::numeric_limits<T>::lowest(), std::numeric_limits<T>::max(), 0},
          z{std::numeric_limits<T>::lowest(), std::numeric_limits<T>::max(),
            0} {}
    // Copy constructor
    Point(const Point &) = default;
    // Copy assignment
    Point &operator=(const Point &) = default;
    // Move constructor
    Point(Point &&) = default;
    // Move assignment
    Point &operator=(Point &&) = default;
    // Generalized copy constructor
    template <typename T2> Point(const Point<T2> &P) {
        x = P.x;
        y = P.y;
        z = P.z;
    }
    // Generalized copy assignment
    template <typename T2> Point &operator=(const Point<T2> &P) {
        for (size_type i = 0; i != this->size(); ++i) {
            (*this)[i] = P[i];
        }
        return *this;
    }
    // Construct with coordinates and maximum numerically representable bounds
    explicit Point<T>(T x_, T y_, T z_)
        : x{std::numeric_limits<T>::lowest(), std::numeric_limits<T>::max(),
            x_},
          y{std::numeric_limits<T>::lowest(), std::numeric_limits<T>::max(),
            y_},
          z{std::numeric_limits<T>::lowest(), std::numeric_limits<T>::max(),
            z_} {}
    // Construct in cubic box from std::pair of bounds
    // (all components initialized to 0)
    Point<T>(std::pair<T, T> bounds)
        : x{bounds, 0}, y{bounds, 0}, z{bounds, 0} {}
    // Construct from std::pairs of bounds (all components initizalized to 0)
    Point<T>(std::pair<T, T> x_bounds, std::pair<T, T> y_bounds,
             std::pair<T, T> z_bounds)
        : x{x_bounds, 0}, y{y_bounds, 0}, z{z_bounds, 0} {}
    // Construct from Ranges
    Point<T>(Range<T> x_, Range<T> y_, Range<T> z_) : x{x_}, y{y_}, z{z_} {}
    // Construct in cubic box from std::pair of bounds and coordinate values
    Point<T>(std::pair<T, T> bounds, T x_, T y_, T z_)
        : x{bounds, x_}, y{bounds, y_}, z{bounds, z_} {}
    // Construct from std::pairs of bounds and coordinate values
    Point<T>(std::pair<T, T> x_bounds, std::pair<T, T> y_bounds,
             std::pair<T, T> z_bounds, T x_, T y_, T z_)
        : x{x_bounds, x_}, y{y_bounds, y_}, z{z_bounds, z_} {}
    // Construct in cubic box from std::pair of bounds and Vector<T>
    Point<T>(std::pair<T, T> bounds, Vector<T> v)
        : x{bounds, v.x}, y{bounds, v.y}, z{bounds, v.y} {}
    // Construct from std::pairs of bounds and Vector<T>
    explicit Point<T>(std::pair<T, T> x_bounds, std::pair<T, T> y_bounds,
                      std::pair<T, T> z_bounds, Vector<T> v)
        : x{x_bounds, v.x}, y{y_bounds, v.y}, z{z_bounds, v.z} {}

    // Access functions
    static constexpr size_type size() { return 3; }
    // Iterators to support ranges
    iterator begin() { return &x; }
    iterator end() { return &z + 1; }
    const_iterator begin() const { return &x; }
    const_iterator end() const { return &z + 1; }
    const_iterator cbegin() { return &x; }
    const_iterator cend() { return &z + 1; }
    // Read-write access to non-const Point<T>
    const Range<T> &operator[](size_type i) const { return *(begin() + i); }
    Range<T> &operator[](size_type i) {
        return const_cast<Range<T> &>(static_cast<const Point<T> &>(*this)[i]);
    }
    // Range &operator[](size_type i) { return *(begin() + i); }
    // // Read-only access to const Point<T>
    // Range operator[](size_type i) const { return *(begin() + i); }

    // Modification
    void set_bounds(std::pair<T, T> x_bounds, std::pair<T, T> y_bounds,
                    std::pair<T, T> z_bounds) {
        x.set_bounds(x_bounds);
        y.set_bounds(y_bounds);
        z.set_bounds(z_bounds);
    }
    void set_bounds(std::pair<T, T> bounds) {
        x.set_bounds(bounds);
        y.set_bounds(bounds);
        z.set_bounds(bounds);
    }
    template <typename T2> Point<T> &operator=(const Vector<T2> &V) {
        for (size_type i = 0; i != this->size(); ++i) {
            (*this)[i].value() = V[i];
        }
        return *this;
    }

    // Mathematical operation member functions
    // Unary operations
    Point<T> normalize() const;
    T sum() const;
    Square_type<T> squared_magnitude() const;
    T magnitude() const;
    // Binary operations with another Point
    template <typename T2> Product_type<T, T2> dot(const Point<T2> &P) const;
    template <typename T2>
    Vector<Product_type<T, T2>> cross(const Point<T2> &P) const;
    template <typename T2>
    Product_type<T, T2> squared_distance(const Point<T2> &P) const;
    template <typename T2>
    Sqrt_type<Product_type<T, T2>> distance(const Point<T2> &P) const;
    // Binary operations with Vector<T>
    template <typename T2> Product_type<T, T2> dot(const Vector<T2> &V) const;
    template <typename T2>
    Vector<Product_type<T, T2>> cross(const Vector<T2> &V) const;
    // Binary operations with Rotation (defined in Rotation.hpp)
    template <typename T2, typename T3>
    Point<Sum_type<Product_type<T2, Sum_type<T, T3>, Inverse_type<T2>>, T3>>
    rotate(const Rotation<T2> &rot, const Vector<T3> &center) const;
    template <typename T2, typename T3>
    Point<Sum_type<Product_type<T2, Sum_type<T, T3>, Inverse_type<T2>>, T3>>
    rotate(const Rotation<T2> &rot, const Point<T3> &center) const;
    template <typename T2>
    Point<Product_type<T2, T, Inverse_type<T2>>>
    rotate(const Rotation<T2> &rot) const;

    // Public data members
    Range<T> x, y, z;
};

// Forward declaration (to facilitate debugging)
template <typename T>
std::ostream &operator<<(std::ostream &os, const Point<T> &P);

// Mathematical operators on Point<T>s

// Unary operators
template <typename T> Point<T> operator-(const Point<T> &P) {
    Point<T> res = P;
    for (auto &xi : res) {
        xi = -xi;
    }
    return res;
}
template <typename T> T sum(const Point<T> &P) {
    T res = 0;
    for (auto pi : P) {
        res += pi.value();
    }
    return res;
}
template <typename T> T Point<T>::sum() const { return ::champion::sum(*this); }

// Forward declaration needed for squared_magnitude()
template <typename T1, typename T2>
Product_type<T1, T2> dot(const Point<T1> &P1, const Point<T2> &P2);
template <typename T> Square_type<T> squared_magnitude(const Point<T> &P) {
    return dot(P, P);
}

template <typename T> Square_type<T> Point<T>::squared_magnitude() const {
    return ::champion::squared_magnitude(*this);
}
template <typename T> T magnitude(const Point<T> &P) {
    return sqrt(P.squared_magnitude());
}
template <typename T> T Point<T>::magnitude() const {
    return ::champion::magnitude(*this);
}

// Forward declarations needed for normalize()
template <typename T1, typename T2>
Point<Quotient_type<T1, T2>> operator/(const Point<T1> &P, const T2 &t);
template <typename T> Point<T> normalize(const Point<T> &P) {
    auto mag = P.magnitude();
    if (mag == 0) {
        return P;
    }
    return P / P.magnitude();
}
template <typename T> Point<T> normalize(Point<T> &&P) {
    Point<T> res = P;
    auto mag = res.magnitude();
    if (mag == 0) {
        return res;
    }
    return res / mag;
}
template <typename T> Point<T> Point<T>::normalize() const {
    return ::champion::normalize(*this);
}

// Binary operators between Points

// Logical operators
template <typename T1, typename T2>
bool operator==(const Point<T1> &P1, const Point<T2> &P2) {
    assert(P1.size() == P2.size());
    for (size_type i = 0; i != P1.size(); ++i) {
        assert(P1[i].low() == P2[i].low());
        assert(P1[i].high() == P2[i].high());
        if (P1[i] != P2[i]) {
            return false;
        }
    }
    return true;
}
template <typename T1, typename T2>
bool operator!=(const Point<T1> &P1, const Point<T2> &P2) {
    return !(P1 == P2);
}

// Arithmetic operators

// Addition of Point<T>s (result is a Point<T> within the same bounds)
template <typename T1, typename T2>
Point<T1> operator+=(Point<T1> &P1, const Point<T2> &P2) {
    for (size_type i = 0; i != P1.size(); ++i) {
        P1[i] += P2[i];
    }
    return P1;
}
template <typename T1, typename T2>
Point<Sum_type<T1, T2>> operator+(const Point<T1> &P1, const Point<T2> &P2) {
    Point<Sum_type<T1, T2>> res;
    for (size_type i = 0; i != res.size(); ++i) {
        res[i] = P1[i] + P2[i];
    }
    return res;
}

// Subtraction
// Calculates Vector<T> distance using minimum image convention
// Returns Point<T> with half the range in each dimension of the original
// Point<T>s (operator-=() makes no sense as result has different bounds)
template <typename T1, typename T2>
Point<Sum_type<T1, T2>> operator-(const Point<T1> &P1, const Point<T2> &P2) {
    assert(P1.size() == P2.size());
    Point<Sum_type<T1, T2>> res = P1;
    for (size_type i = 0; i != P1.size(); ++i) {
        res[i] = P1[i] - P2[i];
    }
    return res;
}
template <typename T1, typename T2>
Point<Sum_type<T1, T2>> operator-(Point<T1> &&P1, const Point<T2> &P2) {
    assert(P1.size() == P2.size());
    Point<Sum_type<T1, T2>> res = P1;
    for (size_type i = 0; i != P1.size(); ++i) {
        res[i] = res[i] - P2[i];
    }
    return res;
}

// Average of Points (results in Point of the same bounds as operands)
// Bounds must be accounted for only after summing and dividing
template <template <typename, typename...> class C, typename T, typename... Ts>
Point<T> average(const C<Point<T>, Ts...> &points) {
    auto displacements = std::vector<Vector<T>>{};
    for (const auto &point : points) {
        displacements.push_back(Vector{point - points[0]});
    }
    auto avg_displacement = average(displacements);
    auto avg_point = points[0] + avg_displacement;
    return avg_point;
}

// Multiplication of Points
template <typename T1, typename T2>
Product_type<T1, T2> dot(const Point<T1> &P1, const Point<T2> &P2) {
    assert(P1.size() == P2.size());
    Product_type<T1, T2> sum = 0;
    for (size_type i = 0; i != P1.size(); ++i) {
        sum += P1[i].value() * P2[i].value();
    }
    return sum;
}
template <typename T1>
template <typename T2>
Product_type<T1, T2> Point<T1>::dot(const Point<T2> &P) const {
    return ::champion::dot(*this, P);
}
// Results in a Vector<T> (not Point<T>)
template <typename T1, typename T2>
Vector<Product_type<T1, T2>> cross(const Point<T1> &P1, const Point<T2> &P2) {
    auto N = P1.size();
    assert(P2.size() == N);
    Vector<Product_type<T1, T2>> res;
    for (size_type i = 0; i != N; ++i) {
        auto j = (i + 1) % N;
        auto k = (i + 2) % N;
        res[i] = P1[j].value() * P2[k].value() - P1[k].value() * P2[j].value();
    }
    return res;
}
template <typename T1>
template <typename T2>
Vector<Product_type<T1, T2>> Point<T1>::cross(const Point<T2> &P) const {
    return ::champion::cross(*this, P);
}

// Distance measures
template <typename T1, typename T2>
Product_type<T1, T2> squared_distance(const Point<T1> &P1,
                                      const Point<T2> &P2) {
    auto diff = P1 - P2;
    return dot(diff, diff);
}
template <typename T1>
template <typename T2>
Product_type<T1, T2> Point<T1>::squared_distance(const Point<T2> &P) const {
    return ::champion::squared_distance(*this, P);
}
template <typename T1, typename T2>
Sqrt_type<Product_type<T1, T2>> distance(const Point<T1> &P1,
                                         const Point<T2> &P2) {
    return sqrt(squared_distance(P1, P2));
}
template <typename T1>
template <typename T2>
Sqrt_type<Product_type<T1, T2>> Point<T1>::distance(const Point<T2> &P) const {
    return ::champion::distance(*this, P);
}

// Binary operators between Point<T>s and other types

// Point<T> and Vector<T>

// Translation of Point<T> by Vector<T>
template <typename T1, typename T2>
Point<T1> operator+=(Point<T1> &P, const Vector<T2> &V) {
    for (size_type i = 0; i != P.size(); ++i) {
        P[i] += V[i];
    }
    return P;
}
template <typename T1, typename T2>
Point<Sum_type<T1, T2>> operator+(const Point<T1> &P, const Vector<T2> &V) {
    Point<Sum_type<T1, T2>> res;
    for (size_type i = 0; i != res.size(); ++i) {
        res[i] = P[i] + V[i];
    }
    return res;
}
template <typename T1, typename T2>
Point<T1> operator-=(Point<T1> &P, const Vector<T2> &V) {
    return P += -V;
}

template <typename T1, typename T2>
Point<Sum_type<T1, T2>> operator-(const Point<T1> &P, const Vector<T2> &V) {
    return P + -V;
}

// Translation of Vector<T> by Point<T>
template <typename T1, typename T2>
Vector<T1> operator+=(Vector<T1> &V, const Point<T2> &P) {
    return V += Vector<T2>(P);
}
template <typename T1, typename T2>
Vector<Sum_type<T1, T2>> operator-=(Vector<T1> &V, const Point<T2> &P) {
    return V += -Vector<T2>(P);
}
template <typename T1, typename T2>
Vector<Sum_type<T1, T2>> operator+(const Vector<T1> &V, const Point<T2> &P) {
    return V + Vector<T2>(P);
}
template <typename T1, typename T2>
Vector<Sum_type<T1, T2>> operator-(const Vector<T1> &V, const Point<T2> &P) {
    return Vector<T1>(V) - Vector<T2>(P);
}

// Multiplication of Point<T> and Vector<T>
template <typename T1, typename T2>
Product_type<T1, T2> dot(const Point<T1> &P, const Vector<T2> &V) {
    return dot(Vector<T2>(P), V);
}
template <typename T1>
template <typename T2>
Product_type<T1, T2> Point<T1>::dot(const Vector<T2> &V) const {
    return ::champion::dot(Vector<T1>(*this), V);
}
template <typename T1, typename T2>
Product_type<T1, T2> dot(const Vector<T1> &V, const Point<T2> &P) {
    return dot(P, V);
}
template <typename T1>
template <typename T2>
Product_type<T1, T2> Vector<T1>::dot(const Point<T2> &P) const {
    return ::champion::dot(P, *this);
}
template <typename T1, typename T2>
Vector<Product_type<T1, T2>> cross(const Point<T1> &P, const Vector<T2> &V) {
    return cross(Vector<T1>(P), V);
}
template <typename T1, typename T2>
Vector<Product_type<T1, T2>> cross(const Vector<T1> &V, const Point<T2> &P) {
    return cross(V, Vector<T2>(P));
}
template <typename T1>
template <typename T2>
Vector<Product_type<T1, T2>> Vector<T1>::cross(const Point<T2> &P) const {
    return ::champion::cross(Vector<T1>(P), *this);
}
template <typename T1>
template <typename T2>
Vector<Product_type<T1, T2>> Point<T1>::cross(const Vector<T2> &V) const {
    return ::champion::cross(*this, V);
}

// Scaling of Point<T>s by numeric types (implicitly convertible to T1)
template <typename T1, typename T2>
Point<T1> operator*=(Point<T1> &P, const T2 &t) {
    for (auto &xi : P) {
        xi *= t;
    }
    return P;
}
template <typename T1, typename T2>
Point<Product_type<T1, T2>> operator*(const Point<T1> &P, const T2 &t) {
    Point<Product_type<T1, T2>> res;
    for (size_type i = 0; i != res.size(); ++i) {
        res[i] = P[i] * t;
    }
    return res;
}
template <typename T1, typename T2>
Point<Product_type<T1, T2>> operator*(const T1 &t, const Point<T2> &P) {
    return P * t;
}

template <typename T1, typename T2> Point<T1> operator/=(Point<T1> &P, T2 t) {
    return P *= (1 / t);
}
template <typename T1, typename T2>
Point<Quotient_type<T1, T2>> operator/(const Point<T1> &P, const T2 &t) {
    return P * (1 / t);
}

// Construct a Vector<T> from a Point<T> (explicit)
template <typename T>
Vector<T>::Vector(const Point<T> &P)
    : x{P.x.value()}, y{P.y.value()}, z{P.z.value()} {}
template <typename T>
Vector<T>::Vector(Point<T> &&P)
    : x{P.x.value()}, y{P.y.value()}, z{P.z.value()} {}
// Assign a Point<T> to Vector<T>
template <typename T> Vector<T> &Vector<T>::operator=(const Point<T> &P) {
    for (size_type i = 0; i != this->size(); ++i) {
        (*this)[i] = P[i].value();
    }
    return *this;
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const Point<T> &P) {
    auto p2 = P;
    for (auto &coord : p2) {
        if (fabs(coord.value()) < 1e-10) {
            coord = 0;
        }
    }
    os << "(" << p2.x.value() << ", " << p2.y.value() << ", " << p2.z.value()
       << ")";
    return os;
}
} // namespace champion

#endif
