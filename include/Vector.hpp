#ifndef CHAMPION_VECTOR_HPP
#define CHAMPION_VECTOR_HPP

#include <cassert>
#include <cmath>
#include <exception>
#include <iostream>
#include <limits>
#include <memory>
#include <utility>

#include "random.hpp"
#include "set_precision.hpp"
#include "type_inference.hpp"

namespace champion {

// Forward declarations
template <typename T> class Point;
template <typename T> class Quaternion;
template <typename T> class Rotation;
template <typename T> class Transformation;

template <typename T> class Vector {
  public:
    using iterator = T *;
    using const_iterator = const T *;
    // Default constructor
    Vector() : x{0}, y{0}, z{0} {}
    // Copy constructor
    Vector(const Vector &) = default;
    // Copy assignment
    Vector &operator=(const Vector &) = default;
    // Move constructor
    Vector(Vector &&) = default;
    // Move assignment
    Vector &operator=(Vector &&) = default;
    // Generalized copy constructor
    template <typename T2>
    Vector(const Vector<T2> &V)
        : x{static_cast<T>(V.x)}, y{static_cast<T>(V.y)}, z{static_cast<T>(
                                                              V.z)} {}
    // Generalized copy assignment
    template <typename T2> Vector &operator=(const Vector<T2> &V) {
        for (size_type i = 0; i != this->size(); ++i) {
            (*this)[i] = V[i];
        }
        return *this;
    }
    // Construct from coordinates
    Vector(T x_, T y_, T z_) : x{x_}, y{y_}, z{z_} {}
    // Construct from Point<T> (definition in Point<T>.hpp)
    explicit Vector(const Point<T> &P);
    explicit Vector(Point<T> &&P);
    // Construct from vector part of Quaternion (definition in Quaternion.hpp)
    explicit Vector(const Quaternion<T> &Q);

#ifdef CHAMPION_MATRIX_HPP
    // Construct from statically dimensions-checked Matrix
    explicit Vector(const Matrix<T, 3, 1> &M) {
        x = M(0, 0);
        y = M(1, 0);
        z = M(2, 0);
    }
    // Construct from dynamically dimensions-checked Matrix
    explicit Vector(const Matrix<T, 0, 0> &M) {
        if (M.num_rows() != 3 || M.num_cols() != 1) {
            throw std::out_of_range{
                "CHAMPION::Vector(): Tried to initialize Vector from Matrix of "
                "wrong dimensions."};
        }
        x = M(0, 0);
        y = M(1, 0);
        z = M(2, 0);
    }
#endif

    // Construct from iterator to first component
    template <typename Iter> explicit Vector<T>(Iter it) {
        for (auto &xi : *this) {
            xi = *(it++);
        }
    }
    // Assignment from Point<T> (definition in Point<T>.hpp)
    Vector<T> &operator=(const Point<T> &P);

    // Access functions
    static constexpr size_type size() { return 3; }
    // Iterators to support ranges
    inline iterator begin() { return &x; }
    inline iterator end() { return &z + 1; }
    inline const_iterator begin() const { return &x; }
    inline const_iterator end() const { return &z + 1; }
    inline const_iterator cbegin() { return &x; }
    inline const_iterator cend() { return &z + 1; }
    // Read-write access to non-const Vector<T>
    inline T &operator[](size_type i) { return *(begin() + i); }
    // Read-only access to const Vector<T>
    inline T operator[](size_type i) const { return *(begin() + i); }

    // Mathematical operation member functions
    // Unary operations
    Vector<T> normalize() const;
    T sum() const;
    Square_type<T> squared_magnitude() const;
    T magnitude() const;
    // Binary operations with another Vector
    template <typename T2> Product_type<T, T2> dot(const Vector<T2> &V) const;
    template <typename T2>
    Vector<Product_type<T, T2>> cross(const Vector<T2> &V) const;
    template <typename T2>
    Product_type<T, T2> squared_distance(const Vector<T2> &V) const;
    template <typename T2>
    Sqrt_type<Product_type<T, T2>> distance(const Vector<T2> &V) const;
    // Binary operations with Point<T>s
    template <typename T2> Product_type<T, T2> dot(const Point<T2> &P) const;
    template <typename T2>
    Vector<Product_type<T, T2>> cross(const Point<T2> &P) const;
    template <typename T2>
    Sqrt_type<Product_type<T, T2>> distance(const Point<T2> &P) const;
    // Binary operations with Rotation (defined in Rotation.hpp)
    template <typename T2, typename T3>
    Vector<Sum_type<Product_type<T2, Sum_type<T, T3>, Inverse_type<T2>>, T3>>
    rotate(const Rotation<T2> &rot, const Vector<T3> &center) const;
    template <typename T2>
    Vector<Product_type<T2, T, Inverse_type<T2>>>
    rotate(const Rotation<T2> &rot) const;
    // Binary operations with Transformation (defined in Transformation.hpp)
    template <typename T2>
    Vector<Sum_type<Product_type<T2, T, Inverse_type<T2>>, T2>>
    transform(const Transformation<T2> &transf) const;

  public:
    // Public data members
    T x, y, z;
};

// Forward declaration (to allow use for debugging)
template <typename T>
std::ostream &operator<<(std::ostream &os, const Vector<T> &P);

// Mathematical operators on Vectors

// Unary operators
template <typename T> Vector<T> operator-(const Vector<T> &V) {
    Vector<T> res = V;
    for (auto &xi : res) {
        xi = -xi;
    }
    return res;
}
template <typename T> Vector<T> operator-(Vector<T> &&V) {
    Vector<T> res = V;
    for (auto &xi : res) {
        xi = -xi;
    }
    return res;
}
template <typename T> T sum(const Vector<T> &V) {
    T res = 0;
    for (auto xi : V) {
        res += xi;
    }
    return res;
}
template <typename T> T Vector<T>::sum() const {
    return ::champion::sum(*this);
}
template <typename T> Square_type<T> squared_magnitude(const Vector<T> &V) {
    return V.dot(V);
}
template <typename T> Square_type<T> Vector<T>::squared_magnitude() const {
    return ::champion::squared_magnitude(*this);
}
template <typename T> T magnitude(const Vector<T> &V) {
    return sqrt(V.squared_magnitude());
}
template <typename T> T Vector<T>::magnitude() const {
    return ::champion::magnitude(*this);
}
// Forward declaration for use in normalize()
template <typename T1, typename T2>
Vector<Quotient_type<T1, T2>> operator/(const Vector<T1> &V, const T2 &t);

template <typename T> Vector<T> normalize(const Vector<T> &V) {
    auto mag = V.magnitude();
    if (mag == 0) {
        return V;
    }
    return Vector<T>(V) / mag;
}
template <typename T> Vector<T> normalize(Vector<T> &&V) {
    Vector<T> res = V;
    auto mag = res.magnitude();
    if (mag == 0) {
        return res;
    }
    return res / mag;
}
template <typename T> Vector<T> Vector<T>::normalize() const {
    return ::champion::normalize(*this);
}

// Binary operators between Vectors

// Logical operators
template <typename T1, typename T2>
bool operator==(const Vector<T1> &V1, const Vector<T2> &V2) {
    assert(V1.size() == V2.size());
    for (size_type i = 0; i != V1.size(); ++i) {
        if (V1[i] != V2[i]) {
            return false;
        }
    }
    return true;
}
template <typename T1, typename T2>
bool operator!=(const Vector<T1> &V1, const Vector<T2> &V2) {
    return !(V1 == V2);
}

// Arithmetic operators

// Addition
template <typename T1, typename T2>
Vector<T1> operator+=(Vector<T1> &V1, const Vector<T2> &V2) {
    assert(V1.size() == V2.size());
    for (size_type i = 0; i != V1.size(); ++i) {
        V1[i] += V2[i];
    }
    return V1;
}

template <typename T1, typename T2>
Vector<Sum_type<T1, T2>> operator+(const Vector<T1> &V1, const Vector<T2> &V2) {
    Vector<Sum_type<T1, T2>> res;
    for (size_type i = 0; i != res.size(); ++i) {
        res[i] = V1[i] + V2[i];
    }
    return res;
}

// Subtraction
template <typename T1, typename T2>
Vector<Sum_type<T1, T2>> operator-=(Vector<T1> &V1, const Vector<T2> &V2) {
    return V1 += -V2;
}

template <typename T1, typename T2>
Vector<Sum_type<T1, T2>> operator-(const Vector<T1> &V1, const Vector<T2> &V2) {
    return V1 + -V2;
}

// Multiplication
template <typename T1, typename T2>
Product_type<T1, T2> dot(const Vector<T1> &V1, const Vector<T2> &V2) {
    assert(V1.size() == V2.size());
    Product_type<T1, T2> sum = 0;
    for (size_type i = 0; i != V1.size(); ++i) {
        sum += V1[i] * V2[i];
    }
    return sum;
}
template <typename T1>
template <typename T2>
Product_type<T1, T2> Vector<T1>::dot(const Vector<T2> &V) const {
    return ::champion::dot(*this, V);
}
template <typename T1, typename T2>
Vector<Product_type<T1, T2>> cross(const Vector<T1> &V1, const Vector<T2> &V2) {
    auto N = V1.size();
    assert(V2.size() == N);
    Vector<Product_type<T1, T2>> res;
    for (size_type i = 0; i != N; ++i) {
        auto j = (i + 1) % N;
        auto k = (i + 2) % N;
        res[i] = V1[j] * V2[k] - V1[k] * V2[j];
    }
    return res;
}
template <typename T1>
template <typename T2>
Vector<Product_type<T1, T2>> Vector<T1>::cross(const Vector<T2> &V) const {
    return ::champion::cross(*this, V);
}

// Distance measures
template <typename T1, typename T2>
Product_type<T1, T2> squared_distance(const Vector<T1> &V1,
                                      const Vector<T2> &V2) {
    auto diff = V1 - V2;
    return dot(diff, diff);
}
template <typename T1>
template <typename T2>
Product_type<T1, T2> Vector<T1>::squared_distance(const Vector<T2> &V) const {
    return ::champion::squared_distance(*this, V);
}
template <typename T1, typename T2>
Sqrt_type<Product_type<T1, T2>> distance(const Vector<T1> &V1,
                                         const Vector<T2> &V2) {
    return sqrt(squared_distance(V1, V2));
}
template <typename T1>
template <typename T2>
Sqrt_type<Product_type<T1, T2>>
Vector<T1>::distance(const Vector<T2> &V) const {
    return ::champion::distance(*this, V);
}

// Scaling of Vectors by numeric types (implicitly convertible to T1)
template <typename T1, typename T2>
Vector<T1> &operator*=(Vector<T1> &V, const T2 &t) {
    for (size_type i = 0; i != V.size(); ++i) {
        V[i] *= t;
    }
    return V;
}
template <typename T1, typename T2>
Vector<Product_type<T1, T2>> operator*(const Vector<T1> &V, const T2 &t) {
    Vector<Product_type<T1, T2>> res;
    for (size_type i = 0; i != V.size(); ++i) {
        res[i] = V[i] * t;
    }
    return res;
}

template <typename T1, typename T2>
Vector<Product_type<T2, T1>> operator*(const T2 &t, const Vector<T1> &V) {
    return V * t;
}

// Division (scaling)
template <typename T1, typename T2>
Vector<T1> operator/=(Vector<T1> &V, const T2 &t) {
    V *= (1 / static_cast<double>(t));
    return V;
}
template <typename T1, typename T2>
Vector<Quotient_type<T1, T2>> operator/(const Vector<T1> &V, const T2 &t) {
    return V * (1 / t);
}

// Averages
template <template <typename, typename...> class C, typename T, typename... Ts>
Vector<T> average(const C<Vector<T>, Ts...> &vecs) {
    auto sum = Vector<T>{};
    for (const auto &vec : vecs) {
        sum += vec;
    }
    sum /= vecs.size();
    return sum;
}

// Generate a random unit vector
template <typename T> Vector<T> random_direction() {
    // Construct random angle generator
    auto r = uniform_real_distribution<T>{0, 1};
    return Vector<T>(r(), r(), r()).normalize();
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const Vector<T> &p) {
    Vector<T> p2(p);
    if (sqrt(p2.x * p2.x) < 1e-10)
        p2.x = 0;
    if (sqrt(p2.y * p2.y) < 1e-10)
        p2.y = 0;
    if (sqrt(p2.z * p2.z) < 1e-10)
        p2.z = 0;
    os << "(" << p2.x << ", " << p2.y << ", " << p2.z << ")";
    return os;
}

template <typename T1, size_type N, size_type M>
template <typename T2>
Matrix<T1, N, M>::Matrix(const Vector<T2> &V) {
    if constexpr ((N != 0 && N != 3) || (M != 0 && M != 1)) {
        throw std::runtime_error{
            "CHAMPION::Matrix: Cannot initialize Matrix from Vector unless "
            "the Matrix to be initialized has dimensions 3x1."};
    }
    number_of_rows_ = 3;
    number_of_cols_ = 1;
    this->elements_ = std::vector<T1>(3);
    for (size_type i = 0; i != 3; ++i) {
        this->elements_[i] = V[i];
    }
}

} // namespace champion

#endif
