#ifndef CHAMPION_RANGE_HPP
#define CHAMPION_RANGE_HPP

#include <cassert>
#include <cmath>
#include <iostream>

#include "set_precision.hpp"
#include "type_inference.hpp"

namespace champion {

// Forward declarations
template <typename T> class Range;
template <typename T1, typename T2>
void check_bounds(const Range<T1> &R1, const Range<T2> &R2);
template <typename T>
std::ostream &operator<<(std::ostream &os, const Range<T> &R);

template <typename T> class Range {
  public:
    // Public member functions
    // Default constructor
    Range<T>() : low_{0}, high_{0}, val_{0} {}
    // Copy constructor
    Range(const Range &) = default;
    // Copy assignment
    Range &operator=(const Range &) = default;
    // Move constructor
    Range(Range &&) = default;
    // Move assignment
    Range &operator=(Range &&) = default;
    // Generalized copy constructor
    template <typename T2>
    Range(const Range<T2> &R)
        : low_{static_cast<T>(R.low_)}, high_{static_cast<T>(R.high_)},
          val_{static_cast<T>(R.val_)} {}
    // Generalized copy assignment
    template <typename T2> Range &operator!=(const Range<T2> &R) {
        low_ = R.low_;
        high_ = R.high_;
        val_ = R.val_;
        return *this;
    }
    // Construct from lower bound, higher bound and value
    Range<T>(T low, T high, T val) : low_{low}, high_{high}, val_{val} {
        bring_within_bounds();
    }
    // Construct from std::pair bounds and value
    Range<T>(std::pair<T, T> bounds, T val)
        : low_{bounds.first}, high_{bounds.second}, val_{val} {
        bring_within_bounds();
    }
    void bring_within_bounds();
    // Assignment from numeric type
    Range<T> &operator=(const T &t) {
        val_ = t;
        bring_within_bounds();
        return *this;
    }
    // Give copy of the value if *this is const
    T value() const { return val_; }
    // Give mutable reference to value if *this is non-const
    T &value() { return val_; }
    T width() const { return high_ - low_; }
    T low() const { return low_; }
    T high() const { return high_; }
    std::pair<T, T> bounds() const { return std::pair<T, T>{low_, high_}; }

    void set_bounds(std::pair<T, T> bounds) {
        low_ = bounds.first;
        high_ = bounds.second;
        bring_within_bounds();
    }
    void set_bounds(T low, T high) {
        low_ = low;
        high_ = high;
        bring_within_bounds();
    }

    // Mathematical operators
    // Unary operators
    Range &operator-();
    // Binary operators between ranges
    // Categorically requires ranges to have the same bounds
    template <typename T2> Range &operator+=(const Range<T2> &R);

    // Binary operators between ranges and other types
    Range &operator+=(const T &t);
    Range &operator*=(const T &t);

    // Assignment
    template <typename T2> Range &operator=(const T2 &x);

  private:
    // Friends
    template <typename T2> friend class Range;
    // Private data members
    T low_, high_, val_;
};

// Exceptions

class range_mismatch_error : public std::runtime_error {
    std::string msg =
        "Operators on Range<T>s require them to have the same bounds.\n";

  public:
    range_mismatch_error(const std::string &arg) : std::runtime_error(arg) {
        msg += arg;
    }
    const char *what() const throw() { return msg.c_str(); }
};

template <typename T> void Range<T>::bring_within_bounds() {
    if (std::isnan(val_)) {
        throw std::runtime_error{"Range<T>: value is NaN"};
    }
    if (val_ < low_) {
        auto w = width();
        unsigned N = std::floor((low_ - val_) / w + 1);
        val_ += N * w;
    } else if (val_ > high_) {
        auto w = width();
        auto N = std::floor((val_ - high_) / w + 1);
        val_ -= N * w;
    }
    if (val_ < low_ || val_ > high_) {
        throw std::runtime_error{
            std::string{
                "champion::Range<T>: Value is not within bounds. Value = "} +
            std::to_string(val_) + ", low = " + std::to_string(low_) +
            ", high = " + std::to_string(high_) + "."};
    }
}

// Mathematical operators on Range<T>s

// Unary operators
template <typename T> Range<T> &Range<T>::operator-() {
    val_ = -val_;
    bring_within_bounds();
    return *this;
}

// Binary operators between Ranges

template <typename T1, typename T2>
void check_bounds(const Range<T1> &R1, const Range<T2> &R2) {
    if (R1.low() != R2.low() || R1.high() != R2.high()) {
        std::string err_str =
            std::string{"First range: "} + std::to_string(R1.value()) + " ∈[" +
            std::to_string(R1.low()) + "," + std::to_string(R1.high()) + "]\n" +
            "Second range: " + std::to_string(R2.value()) + " ∈[" +
            std::to_string(R2.low()) + "," + std::to_string(R2.high()) + "]";
        throw range_mismatch_error(err_str);
    }
}

// Logic operators
template <typename T1, typename T2>
bool operator==(const Range<T1> &R1, const Range<T2> &R2) {
    check_bounds(R1, R2);
    if (R1.value() != R2.value()) {
        return false;
    }
    return true;
}
template <typename T1, typename T2>
bool operator<(const Range<T1> &R1, const Range<T2> &R2) {
    check_bounds(R1, R2);
    return R1.value() < R2.value();
}
template <typename T1, typename T2>
bool operator!=(const Range<T1> &R1, const Range<T2> &R2) {
    return !(R1 == R2);
}
template <typename T1, typename T2>
bool operator>(const Range<T1> &R1, const Range<T2> &R2) {
    return R2 < R1;
}
template <typename T1, typename T2>
bool operator<=(const Range<T1> &R1, const Range<T2> &R2) {
    return !(R2 < R1);
}
template <typename T1, typename T2>
bool operator>=(const Range<T1> &R1, const Range<T2> &R2) {
    return !(R1 < R2);
}

// Arithmetic operators

template <typename T1>
template <typename T2>
Range<T1> &Range<T1>::operator+=(const Range<T2> &R) {
    check_bounds(*this, R);
    val_ += R.value();
    bring_within_bounds();
    return *this;
}

template <typename T> Range<T> &Range<T>::operator+=(const T &r) {
    val_ += r;
    bring_within_bounds();
    return *this;
}

template <typename T1, typename T2>
Range<Sum_type<T1, T2>> operator+(const Range<T1> &R1, const Range<T2> &R2) {
    Range<Sum_type<T1, T2>> R = R1;
    return R += R2;
}

// Compute average value of two ranges
template <typename T1, typename T2>
Range<T1> average(const Range<T1> &R1, const Range<T2> &R2) {
    // R1.val and R2.val accessed because bounds must be considered after
    // summing and dividing
    auto avg_val = (R1.value() + R2.value()) / 2;
    auto res = Range<T1>(R1.low(), R1.high(), avg_val);
    return res;
}
// Compute average value of a container of Range<T>s
// Requires all Range<T>s to have identical bounds
template <template <typename> class C, typename T>
Range<T> average(const C<T> &container) {
    // Sum is declared T because bounds must be considered after summing
    // and dividing
    T sum = 0;
    size_type num_els = 0;
    for (auto ci : container) {
        check_bounds(ci, container[0]);
        sum += ci.value();
        ++num_els;
    }
    auto res = Range<T>(container[0].low(), container[0].high(), sum / num_els);
    return res;
}
// Calculates shortest distance between periodic images
// Returns Range<T> between -width()/2 and +width()/2
template <typename T1, typename T2>
Range<Sum_type<T1, T2>> operator-(const Range<T1> &R1, const Range<T2> &R2) {
    check_bounds(R1, R2);
    auto new_high = R1.width() / 2;
    auto new_low = -new_high;
    auto res1 = Range<Sum_type<T1, T2>>(new_low, new_high, R1.value());
    auto res2 = Range<Sum_type<T1, T2>>(new_low, new_high, -R2.value());
    auto res = res1 + res2;
    return res;
}

// Binary operators between Ranges and other numeric types

// Logical operators
template <typename T1, typename T2>
bool operator==(const Range<T1> &R, const T2 &x) {
    return R.value() == x;
}
template <typename T1, typename T2>
bool operator==(const T1 &x, const Range<T2> &R) {
    return R == x;
}
template <typename T1, typename T2>
bool operator!=(const Range<T1> &R, const T2 &x) {
    return !(R == x);
}
template <typename T1, typename T2>
bool operator!=(const T2 &x, const Range<T1> &R) {
    return !(R == x);
}
template <typename T1, typename T2>
bool operator<(const Range<T1> &R, const T2 &x) {
    return R.value() < x;
}
template <typename T1, typename T2>
bool operator>(const T2 &x, const Range<T1> &R) {
    return R < x;
}
template <typename T1, typename T2>
bool operator>(const Range<T1> &R, const T2 &x) {
    return R.value() > x;
}
template <typename T1, typename T2>
bool operator<(const T2 &x, const Range<T1> &R) {
    return R > x;
}
template <typename T1, typename T2>
bool operator<=(const Range<T1> &R, const T2 &x) {
    return !(R > x);
}
template <typename T1, typename T2> bool operator>=(const T2 &x, Range<T1> &R) {
    return R <= x;
}
template <typename T1, typename T2>
bool operator>=(const Range<T1> &R, const T2 &x) {
    return !(R < x);
}
template <typename T1, typename T2>
bool operator<=(const T2 &x, const Range<T1> &R) {
    return R >= x;
}

// Assignment
template <typename T1>
template <typename T2>
Range<T1> &Range<T1>::operator=(const T2 &x) {
    val_ = x;
    bring_within_bounds();
    return *this;
}

// Arithmetic operators

template <typename T> Range<T> &operator-=(Range<T> &R, const T &x) {
    return R += -x;
}
template <typename T1, typename T2>
Range<Sum_type<T1, T2>> operator+(const Range<T1> &R, const T2 &x) {
    Range<Sum_type<T1, T2>> res = R;
    return res += x;
}
template <typename T1, typename T2>
Range<Sum_type<T1, T2>> operator+(const T1 &x, const Range<T2> &R) {
    return R + x;
}
template <typename T1, typename T2>
Range<Sum_type<T1, T2>> operator-(const Range<T1> &R, const T2 &x) {
    return R + -x;
}
template <typename T1, typename T2>
Range<Sum_type<T1, T2>> operator-(const T1 &x, const Range<T2> &R) {
    return -R + x;
}

// Scaling of Range by other types
template <typename T> Range<T> &Range<T>::operator*=(const T &x) {
    val_ *= x;
    bring_within_bounds();
    return *this;
}

template <typename T> Range<T> &operator/=(Range<T> &R, const T &x) {
    return R *= (1 / x);
}

template <typename T1, typename T2>
Range<Product_type<T1, T2>> operator*(const Range<T1> &R, const T2 &x) {
    Range<Product_type<T1, T2>> res = R;
    return res *= x;
}
template <typename T1, typename T2>
Range<Product_type<T1, T2>> operator*(const T1 &x, const Range<T2> &R) {
    return R * x;
}
template <typename T1, typename T2>
Range<Quotient_type<T1, T2>> operator/(const Range<T1> &R, const T2 &x) {
    Range<Quotient_type<T1, T2>> res = R;
    return res /= x;
}
template <typename T1, typename T2>
Quotient_type<T1, T2> operator/(const T1 &x, const Range<T2> &R) {
    return x / R.value();
}
template <typename T1, typename T2> T1 operator%=(T1 &x, const Range<T2> &R) {
    Range<T1> tmp = R; // Copy bounds from R
    tmp = x;           // Set val to t and bring within bounds
    return tmp.value();
}
template <typename T1, typename T2>
T1 operator%(const T1 &x, const Range<T2> &R) {
    T1 res = x;
    return res %= R;
}

// IO operations
template <typename T>
std::ostream &operator<<(std::ostream &os, const Range<T> &R) {
    os << R.value() << " ∈[" << R.low() << "," << R.high() << "]";
    return os;
}

} // namespace champion

namespace std {
// Overload std::fabs for Range<T>s
template <typename T> T fabs(const champion::Range<T> &R) {
    return fabs(R.value());
}
} // namespace std

#endif