/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================

______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017-2018 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

SOURCE FILE: forces.hpp

AUTHOR: Rasmus Andersson 2018-07-25
MODIFIED BY:

DESCRIPTION:

\******************************************************************************/

#ifndef CHAMPION_FORCES_HPP
#define CHAMPION_FORCES_HPP

#include <chrono>
#include <cmath>
#include <complex>
#include <vector>

#include "ObjectRegistry.hpp"
#include "set_precision.hpp"
#include "units.hpp"

using namespace std::chrono;

extern int MPI_root, MPI_rank, MPI_size;

namespace champion {

// Must be set to the number of options in FF_param_type
// Under current implementation, optimize_force_field() assumes that the
// nonbonded parameter come first, so please don't change this without
// refactoring optimize_force_field().
const size_type no_interaction_types = 19;
enum FF_param_type {
    charge,
    epsilon,
    sigma,
    eq_bond,
    k_bond,
    eq_angle,
    k_angle,
    periodicity_proper,
    eq_proper,
    k_proper,
    periodicity_improper,
    eq_improper,
    k_improper,
    Coulomb_12scaling,
    Coulomb_13scaling,
    Coulomb_14scaling,
    LJ_12scaling,
    LJ_13scaling,
    LJ_14scaling
};

template <level_type n, level_type level> class ForceField {
  public:
    // Public classes
    // Default constructor
    ForceField();
    // Construct from std::vector<size_type> and vector of doubles
    ForceField(const ObjectRegistry<n> &reg, std::vector<size_type> types,
               std::vector<double> values);
    // Construct from std::vector<size_type>
    ForceField(const ObjectRegistry<n> &reg, std::vector<size_type> types);

    size_type size() const { return types_.size(); }

    // get const ref to vector of all values
    const std::vector<double> &get_values() const;
    std::vector<double> &get_values();
    const double &get_value(size_type type, size_type i) const;
    // Get mutable value number i of FF_param_type type
    double &get_value(size_type type, size_type i);
    // Get the number of parameters of a specific type
    size_type no_parameters(size_type type) const;
    // Set all values of the force field if *this is mutable
    void set_values(std::vector<double> values);

  private:
    // Private member functions
    void enforce_charge_neutrality();
    // Private data members
    const ObjectRegistry<n> *reg_;
    std::vector<size_type> types_;
    std::vector<double> values_;
};

template <level_type n, level_type level>
ForceField<n, level>::ForceField(const ObjectRegistry<n> &reg,
                                 std::vector<size_type> types,
                                 std::vector<double> values)
    : reg_{&reg}, types_(types), values_(values) {
    if (types.size() != values.size()) {
        throw std::runtime_error{
            std::string{"champion::ForceField: Size mismatch for types and "
                        "values, types.size() = "} +
            std::to_string(types.size()) +
            ", values.size() = " + std::to_string(values.size())};
    }
}

template <level_type n, level_type level>
ForceField<n, level>::ForceField() : reg_{nullptr}, types_{}, values_{} {}

template <level_type n, level_type level>
ForceField<n, level>::ForceField(const ObjectRegistry<n> &reg,
                                 std::vector<size_type> types)
    : reg_{&reg}, types_(types), values_(types.size(), 0) {}

template <level_type n, level_type level>
const std::vector<double> &ForceField<n, level>::get_values() const {
    return values_;
}

template <level_type n, level_type level>
std::vector<double> &ForceField<n, level>::get_values() {
    return const_cast<std::vector<double> &>(
        static_cast<const ForceField<n, level> &>(*this).get_values());
}

template <level_type n, level_type level>
const double &ForceField<n, level>::get_value(size_type type,
                                              size_type i) const {
    size_type j = 0;
    for (size_type nn = 0; nn != values_.size(); ++nn) {
        if (types_[nn] == type) {
            if (j == i) {
                return values_[nn];
            } else {
                ++j;
            }
        }
    }
    throw std::runtime_error{std::string{"Fewer than "} +
                             std::to_string(i + 1) + " parameters of type " +
                             std::to_string(type)};
}

template <level_type n, level_type level>
double &ForceField<n, level>::get_value(size_type type, size_type i) {
    return const_cast<double &>(
        static_cast<const ForceField<n, level> *>(this)->get_value(type, i));
}

template <level_type n, level_type level>
size_type ForceField<n, level>::no_parameters(size_type type) const {
    size_type num = 0;
    for (const auto &type_i : types_) {
        if (type_i == type) {
            ++num;
        }
    }
    return num;
}

template <level_type n, level_type level>
void ForceField<n, level>::set_values(std::vector<double> values) {
    values_ = values;
    enforce_charge_neutrality();
}

template <level_type n, level_type level>
void ForceField<n, level>::enforce_charge_neutrality() {
    auto body_types = reg_->template get_objects<BodyType, level>();
    auto no_body_types = body_types.size();
    auto amounts = std::vector<unsigned>(no_body_types);
    for (const auto &body_el : reg_->template get_objects<Body, level>()) {
        ++amounts[body_el.second.type().id()];
    }
    double total_charge = 0.0;
    for (size_type i = 0; i != no_body_types; ++i) {
        total_charge += amounts[i] * get_value(FF_param_type::charge, i);
    }
    auto total_no_atoms = std::accumulate(amounts.begin(), amounts.end(), 0);
    for (size_type i = 0; i != no_body_types; ++i) {
        get_value(FF_param_type::charge, i) -= total_charge / total_no_atoms;
    }
    // total_charge = 0;
    // for (size_type i = 0; i != no_body_types; ++i) {
    //     total_charge += amounts[i] * get_value(FF_param_type::charge, i);
    // }
    // assert(std::fabs(total_charge) < 1e-6);
}

// Function declarations

template <level_type n, level_type level>
std::vector<Vector<real>>
compute_forces(const ObjectRegistry<n> &reg, size_type trajectory,
               size_type snapshot, ForceField<n, level> &force_field,
               real tol = 1e-3, bool print_debug = false);

template <level_type n, level_type level>
Matrix<Vector<real>> compute_separations(const ObjectRegistry<n> &reg,
                                         size_type trajectory,
                                         size_type snapshot);

template <level_type n, level_type level>
std::pair<SymmetricMatrix<real>, SymmetricMatrix<real>>
compute_nonbonded_scaling(const ObjectRegistry<n> &reg, size_type trajectory,
                          size_type snapshot,
                          ForceField<n, level> &force_field);

template <level_type n, level_type level>
std::vector<Vector<real>> compute_Coulomb_forces(
    const ObjectRegistry<n> &reg, size_type trajectory, size_type snapshot,
    const ForceField<n, level> &force_field, real tol = 1e-6);

template <level_type n, level_type level>
std::vector<Vector<real>>
compute_LJ_forces(const ObjectRegistry<n> &reg, size_type trajectory,
                  size_type snapshot, ForceField<n, level> &force_field);

template <level_type n, level_type level>
std::vector<Vector<real>>
compute_harmonic_bond_forces(const ObjectRegistry<n> &reg, size_type trajectory,
                             size_type snapshot,
                             ForceField<n, level> &force_field);

template <level_type n, level_type level>
std::vector<Vector<real>>
compute_harmonic_angle_forces(const ObjectRegistry<n> &reg,
                              size_type trajectory, size_type snapshot,
                              ForceField<n, level> &force_field);

real compute_angle(const Vector<real> &r_1, const Vector<real> &r_2,
                   const Vector<real> &r_3);

template <level_type n, level_type level>
void print_force_field(const ObjectRegistry<n> &reg,
                       const ForceField<n, level> &force_field);

// Function definitions

template <level_type n, level_type level>
std::vector<Vector<real>>
compute_forces(const ObjectRegistry<n> &reg, size_type trajectory,
               size_type snapshot, ForceField<n, level> &force_field, real tol,
               bool print_debug) {
    high_resolution_clock::time_point t0 = high_resolution_clock::now();
    auto N = reg.template get_object<Trajectory, level>(trajectory)
                 .get_snapshot(snapshot)
                 .size();
    auto F_Coulomb = compute_Coulomb_forces<n, level>(reg, trajectory, snapshot,
                                                      force_field, tol);
    auto F_LJ =
        compute_LJ_forces<n, level>(reg, trajectory, snapshot, force_field);
    auto F_bond = compute_harmonic_bond_forces<n, level>(reg, trajectory,
                                                         snapshot, force_field);
    auto F_angle = compute_harmonic_angle_forces<n, level>(
        reg, trajectory, snapshot, force_field);

    auto forces = std::vector<Vector<real>>(N, {0, 0, 0});
    for (size_type i = 0; i != N; ++i) {
        forces[i] = F_Coulomb[i] + F_LJ[i] + F_bond[i] + F_angle[i];

        if (print_debug) {
            high_resolution_clock::time_point t1 = high_resolution_clock::now();
            auto duration = duration_cast<milliseconds>(t1 - t0).count();
            std::cout << "Total time for evaluating forces: " << duration
                      << " ms" << std::endl;

            auto F_ref = reg.template get_object<Trajectory, 0>(trajectory)
                             .get_forces(snapshot)[i];
            auto error = magnitude(F_ref - forces[i]);
            auto rel_error = error / magnitude(F_ref);
            auto mag_cost = std::pow(std::log(squared_magnitude(F_ref) /
                                              squared_magnitude(forces[i])),
                                     2);
            auto dir_cos = direction_cosine(F_ref, forces[i]);
            auto dir_cost = std::tan(units::pi / 4 * (1 - dir_cos));
            std::cout << "[" << i << "]"
                      << "\n\tF_ref = " << F_ref << "\n\tF_tot =  " << forces[i]
                      << "\n\tF_Coulomb = " << F_Coulomb[i]
                      << "\n\tF_LJ = " << F_LJ[i]
                      << "\n\tF_bond = " << F_bond[i]
                      << "\n\tF_angle = " << F_angle[i]
                      << "\n\terror = " << error
                      << "\n\trelative: " << rel_error << "\n\tfactor: "
                      << magnitude(forces[i]) / magnitude(F_ref) << " ("
                      << magnitude(F_ref) / magnitude(forces[i]) << ")"
                      << "\n\tAngle: "
                      << convert_units(std::acos(dir_cos), units::rad,
                                       units::degrees)
                      << "\n\tMagnitude cost: " << mag_cost
                      << "\n\tDirection cost: " << dir_cost
                      << "\n\tTotal cost: " << mag_cost + dir_cost << std::endl;
        }
    }

    return forces;
}

// Compute Coulomb forces acting on all ions by Ewald summation
template <level_type n, level_type level>
std::vector<Vector<real>>
compute_Coulomb_forces(const ObjectRegistry<n> &reg, size_type trajectory,
                       size_type snapshot, ForceField<n, level> &force_field,
                       real tol) {
    // Get Coulomb scaling matrix
    auto scaling_matrix = compute_nonbonded_scaling<n, level>(
                              reg, trajectory, snapshot, force_field)
                              .second;
    // Compute width of Gaussians in Ewald summation
    auto conf = reg.template get_object<Trajectory, level>(trajectory)
                    .get_snapshot(snapshot);
    auto L = conf.get_position(0).x.width();
    auto V = L * L * L;
    auto N = conf.size();
    // Store vector of body types
    auto types = std::vector<real>(N, 0);
    for (size_type i = 0; i != N; ++i) {
        types[i] = conf.get_body(i).type().id();
    }

    // Set width of Gaussians
    // (controls relative computational cost of SR and LR)
    auto sigma = 2.5e-1; // Empirically optimized

    // Compute short-range (real space) interactions

    // Initialize clock
    // high_resolution_clock::time_point t0 = high_resolution_clock::now();

    // Calculate how great distances between particles need to be considered
    // to give errors below tolerance
    auto r_max = L / 2;
    while ((1 / (4 * units::pi * units::epsilon0 * r_max * r_max)) *
               (std::erfc(r_max / (std::sqrt(2) * sigma)) +
                (2 * r_max / (std::sqrt(2 * pi) * sigma)) *
                    std::exp(-std::pow(r_max / sigma, 2) / 2)) >
           tol) {
        r_max *= 1.1;
    }
    // Calculate how many supercell lengths away must be considered in the
    // real space sum
    int n_max = 2;
    while ((n_max - 1) * L < r_max) {
        ++n_max;
    }
    // Create translation vectors to be considered in sum
    std::vector<Vector<real>> T_vec{};
    for (int mm = -n_max; mm < n_max; ++mm) {
        for (int nn = -n_max; nn < n_max; ++nn) {
            for (int pp = -n_max; pp < n_max; ++pp) {
                T_vec.push_back({mm * L, nn * L, pp * L});
            }
        }
    }

    // Construct matrix of pairwise forces
    std::vector<Vector<real>> forces_SR(N, {0, 0, 0});
    for (size_type i = 0; i != N; ++i) {
        auto q_i = force_field.get_value(FF_param_type::charge, types[i]);
        auto r_i = Vector<real>{conf.get_position(i)};

        for (size_type j = i + 1; j != N; ++j) {
            auto q_j = force_field.get_value(FF_param_type::charge, types[j]);
            auto r_j = Vector<real>{conf.get_position(j)};
            auto r_ij = r_j - r_i;
            for (size_type T = 0; T != T_vec.size(); ++T) {
                // Only include in short range force computations if
                // separation below r_max
                auto r_ij_T = r_ij + T_vec[T];
                auto d_ij_T = magnitude(r_ij_T);
                auto scaling = 1;
                if (T_vec[T] == Vector<real>{0, 0, 0}) {
                    scaling = scaling_matrix(i, j);
                }
                if (d_ij_T < r_max && d_ij_T != 0) {
                    auto F =
                        -scaling * (q_i * q_j * r_ij_T) /
                        (4 * units::pi * units::epsilon0 *
                         std::pow(d_ij_T, 3)) *
                        (std::erfc(d_ij_T / (std::sqrt(2) * sigma)) +
                         2 * d_ij_T / (sqrt(2 * pi) * sigma) *
                             std::exp(-d_ij_T * d_ij_T / (2 * sigma * sigma)));
                    forces_SR[i] += F;
                    forces_SR[j] -= F;
                }
            }
        }
    }
    // high_resolution_clock::time_point t1 = high_resolution_clock::now();

    // Compute long-range (reciprocal space) interactions

    // Choose G_max for reciprocal space sum
    // Estimate errors along k_x axis starting from 2nd Brillouin zone
    // (1st BZ excluded in sum) until below tolerance.
    auto ceiling = V * units::epsilon0 * tol;
    real b = 2 * pi / L;
    int BZ_max = 1;
    while (std::exp(-std::pow(sigma * BZ_max * b, 2) / 2) / (BZ_max * b) >
           ceiling) {
        ++BZ_max;
    }
    auto G_max = b * BZ_max;
    // Collect reciprocal vectors to be included in sum
    std::vector<Vector<real>> G_vecs{};
    // Magnitudes of the G vectors
    std::vector<real> G_mags{};
    // Structure factors
    std::vector<std::complex<real>> S{};
    for (int mm = -BZ_max; mm != BZ_max; ++mm) {
        for (int nn = -BZ_max; nn != BZ_max; ++nn) {
            for (int pp = -BZ_max; pp != BZ_max; ++pp) {
                if (mm == 0 && nn == 0 && pp == 0) {
                    continue;
                }
                auto G = b * Vector<real>{static_cast<real>(mm),
                                          static_cast<real>(nn),
                                          static_cast<real>(pp)};
                auto G_mag = magnitude(G);
                // Only include in sum if reciprocal lattice vector is
                // smaller than G_max
                if (G_mag < G_max) {
                    G_vecs.push_back(G);
                    G_mags.push_back(G_mag);
                    // Compute structure factors
                    std::complex<real> S_sum = 0;
                    for (size_type i = 0; i != N; ++i) {
                        auto q_i = force_field.get_value(FF_param_type::charge,
                                                         types[i]);
                        auto r_i = Vector<real>{
                            reg.template get_object<Trajectory, level>(
                                   trajectory)
                                .get_snapshot(snapshot)
                                .get_position(i)};
                        S_sum +=
                            q_i * exp(std::complex<real>{0, 1} * dot(G, r_i));
                    }
                    S.push_back(S_sum);
                }
            }
        }
    }
    std::vector<Vector<real>> forces_LR(N);
    for (size_type i = 0; i != N; ++i) {
        std::vector<real> F = {0, 0, 0};
        auto q_i = force_field.get_value(FF_param_type::charge, types[i]);
        auto r_i = Vector<real>{conf.get_position(i)};
        for (size_type k = 0; k != G_vecs.size(); ++k) {
            auto coeff = (exp(-std::pow(sigma * G_mags[k], 2) / 2) /
                          (G_mags[k] * G_mags[k]));
            for (size_type xi = 0; xi != 3; ++xi) {
                F[xi] -=
                    coeff *
                    (std::complex<real>{0, 1} * G_vecs[k][xi] * q_i *
                     std::exp(-std::complex<real>{0, 1} * dot(G_vecs[k], r_i)) *
                     S[k])
                        .real();
            }
        }
        forces_LR[i] = Vector<real>{F[0], F[1], F[2]} / (V * units::epsilon0);
    }
    // high_resolution_clock::time_point t2 = high_resolution_clock::now();
    // auto duration_SR = duration_cast<milliseconds>(t1 - t0).count();
    // auto duration_LR = duration_cast<milliseconds>(t2 - t1).count();
    // std::cout << "Duration of short-range calculation: " << duration_SR
    // << " ms"
    //           << std::endl;
    // std::cout << "Duration of long-range calculation: " << duration_LR <<
    // " ms"
    //           << std::endl;
    // std::cout << "t_SR/t_LR = " << duration_SR / (double)duration_LR
    //           << std::endl;

    auto forces = std::vector<Vector<real>>(N, {0, 0, 0});
    for (size_type i = 0; i != N; ++i) {
        forces[i] = forces_SR[i] + forces_LR[i];
    }
    return forces;
}

// Compute Lennard-Jones dispersion and exclusion forces, using
// Lorentz-Berthelot combining rules for parameters
template <level_type n, level_type level>
std::vector<Vector<real>>
compute_LJ_forces(const ObjectRegistry<n> &reg, size_type trajectory,
                  size_type snapshot, ForceField<n, level> &force_field) {
    auto conf = reg.template get_object<Trajectory, level>(trajectory)
                    .get_snapshot(snapshot);
    auto N = conf.size();
    // Store vector of body types
    auto types = std::vector<real>(N, 0);
    for (size_type i = 0; i != N; ++i) {
        types[i] = conf.get_body(i).type().id();
    }
    auto scaling_matrix = compute_nonbonded_scaling<n, level>(
                              reg, trajectory, snapshot, force_field)
                              .second;
    Matrix<Vector<real>> F_ij{N, N};
    for (size_type i = 0; i < N; ++i) {
        auto r_i = conf.get_position(i);
        auto sigma_i = force_field.get_value(FF_param_type::sigma, types[i]);
        auto epsilon_i =
            force_field.get_value(FF_param_type::epsilon, types[i]);
        for (size_type j = i + 1; j < N; ++j) {
            auto r_j = conf.get_position(j);
            auto sigma_j =
                force_field.get_value(FF_param_type::sigma, types[j]);
            auto epsilon_j =
                force_field.get_value(FF_param_type::epsilon, types[j]);
            // Compute effective parameters
            auto sigma = (sigma_i + sigma_j) / 2;
            auto epsilon = std::sqrt(epsilon_i * epsilon_j);
            auto r_ij = Vector<real>{r_j - r_i};
            auto d_ij = magnitude(r_ij);
            auto sigma_r_6 = std::pow(sigma / d_ij, 6);
            F_ij(i, j) = -scaling_matrix(i, j) * r_ij *
                         ((24 * epsilon) / (d_ij * d_ij)) * sigma_r_6 *
                         (2 * sigma_r_6 - 1);
            F_ij(j, i) = -F_ij(i, j);
        }
    }
    std::vector<Vector<real>> forces(N, {0, 0, 0});
    for (size_type i = 0; i != N; ++i) {
        for (size_type j = 0; j != N; ++j) {
            forces[i] += F_ij(i, j);
        }
    }
    return forces;
}

template <level_type n, level_type level>
std::vector<Vector<real>>
compute_harmonic_bond_forces(const ObjectRegistry<n> &reg, size_type trajectory,
                             size_type snapshot,
                             ForceField<n, level> &force_field) {
    auto conf = reg.template get_object<Trajectory, level>(trajectory)
                    .get_snapshot(snapshot);
    auto N = conf.size();
    Matrix<Vector<real>> F{N, N};
    // Loop over bonds
    for (const auto &bond_el : reg.template get_objects<Bond, level>()) {
        auto bond = bond_el.second;
        if (bond.get_trajectory_index() == trajectory) {
            auto bond_type = bond.type().id();
            auto l0 = force_field.get_value(FF_param_type::eq_bond, bond_type);
            auto k = force_field.get_value(FF_param_type::k_bond, bond_type);
            auto i = conf.get_index_by_id(bond[0].body().id());
            auto j = conf.get_index_by_id(bond[1].body().id());
            auto r_i = conf.get_position(i);
            auto r_j = conf.get_position(j);
            auto r_ij = Vector<real>{r_j - r_i};
            auto l = magnitude(r_ij);
            F(i, j) = k * (l - l0) * r_ij / l;
            F(j, i) = -F(i, j);
        }
    }
    std::vector<Vector<real>> forces(N, {0, 0, 0});
    for (size_type i = 0; i != N; ++i) {
        for (size_type j = 0; j != N; ++j) {
            forces[i] += F(i, j);
        }
    }
    return forces;
}

template <level_type n, level_type level>
std::vector<Vector<real>>
compute_harmonic_angle_forces(const ObjectRegistry<n> &reg,
                              size_type trajectory, size_type snapshot,
                              ForceField<n, level> &force_field) {
    auto conf = reg.template get_object<Trajectory, level>(trajectory)
                    .get_snapshot(snapshot);
    auto N = conf.size();
    Matrix<Vector<real>> F{N, N};
    // Loop over bond angles
    for (const auto &angle_el : reg.template get_objects<BondAngle, level>()) {
        auto angle = angle_el.second;
        if (angle.get_trajectory_index() == trajectory) {
            auto angle_type = angle.type().id();
            auto theta0 =
                force_field.get_value(FF_param_type::eq_angle, angle_type);
            auto k_theta =
                force_field.get_value(FF_param_type::k_angle, angle_type);
            auto i = conf.get_index_by_id(angle[0].body().id());
            auto j = conf.get_index_by_id(angle[1].body().id());
            auto k = conf.get_index_by_id(angle[2].body().id());
            auto r_i = conf.get_position(i);
            auto r_j = conf.get_position(j);
            auto r_k = conf.get_position(k);
            auto theta = compute_angle(Vector<real>{r_i}, Vector<real>{r_j},
                                       Vector<real>{r_k});
            auto r_ji = Vector<real>{r_i - r_j};
            auto r_jk = Vector<real>{r_k - r_j};
            auto normal = cross(r_ji, r_jk);
            auto theta_hat_1 = cross(normal, r_ji).normalize();
            auto theta_hat_2 = cross(-normal, r_jk).normalize();
            auto F_mag = k_theta * (theta - theta0);
            F(i, k) = F_mag * theta_hat_1;
            F(k, i) = F_mag * theta_hat_2;
        }
    }
    std::vector<Vector<real>> forces(N, {0, 0, 0});
    for (size_type i = 0; i != N; ++i) {
        for (size_type j = 0; j != N; ++j) {
            forces[i] += F(i, j);
        }
    }
    return forces;
}

template <level_type n, level_type level>
std::pair<SymmetricMatrix<real>, SymmetricMatrix<real>>
compute_nonbonded_scaling(const ObjectRegistry<n> &reg, size_type trajectory,
                          size_type snapshot,
                          ForceField<n, level> &force_field) {
    auto conf = reg.template get_object<Trajectory, level>(trajectory)
                    .get_snapshot(snapshot);
    auto N = conf.size();
    SymmetricMatrix<real> Coulomb_scaling{N, N};
    // Default scaling is 1
    for (auto &el : Coulomb_scaling) {
        el = 1;
    }
    auto LJ_scaling = Coulomb_scaling;
    // Loop over bonds and record 1-2 scaling
    for (auto &bond_el : reg.template get_objects<Bond, level>()) {
        auto bond = bond_el.second;
        if (bond.get_trajectory_index() == trajectory) {
            auto b1 = bond[0].body().id();
            auto b2 = bond[1].body().id();
            Coulomb_scaling(conf.get_index_by_id(b1),
                            conf.get_index_by_id(b2)) =
                force_field.get_value(FF_param_type::Coulomb_12scaling, 0);
            LJ_scaling(conf.get_index_by_id(b1), conf.get_index_by_id(b2)) =
                force_field.get_value(FF_param_type::LJ_12scaling, 0);
        }
    }
    // Loop over bond angles and record 1-3 scaling
    for (auto &angle_el : reg.template get_objects<BondAngle, level>()) {
        auto angle = angle_el.second;
        if (angle.get_trajectory_index() == trajectory) {
            auto b1 = angle[0].body().id();
            auto b2 = angle[2].body().id();
            Coulomb_scaling(conf.get_index_by_id(b1),
                            conf.get_index_by_id(b2)) =
                force_field.get_value(FF_param_type::Coulomb_13scaling, 0);
            LJ_scaling(conf.get_index_by_id(b1), conf.get_index_by_id(b2)) =
                force_field.get_value(FF_param_type::LJ_13scaling, 0);
        }
    }
    // Loop over torsions and record 1-4 scaling
    for (auto &torsion_el : reg.template get_objects<Torsion, level>()) {
        auto torsion = torsion_el.second;
        if (torsion.get_trajectory_index() == trajectory) {
            auto b1 = torsion[0].body().id();
            auto b2 = torsion[3].body().id();
            Coulomb_scaling(conf.get_index_by_id(b1),
                            conf.get_index_by_id(b2)) =
                force_field.get_value(FF_param_type::Coulomb_14scaling, 0);
            LJ_scaling(conf.get_index_by_id(b1), conf.get_index_by_id(b2)) =
                force_field.get_value(FF_param_type::LJ_14scaling, 0);
        }
    }
    return {Coulomb_scaling, LJ_scaling};
}

template <level_type n, level_type level>
Matrix<Vector<real>> compute_separations(const ObjectRegistry<n> &reg,
                                         size_type trajectory,
                                         size_type snapshot) {
    auto config = reg.template get_object<Trajectory, level>(trajectory)
                      .get_snapshot(snapshot);
    auto N = config.size();
    auto separations = Matrix<Vector<real>>{N, N};
    for (size_type i = 0; i < N; ++i) {
        for (size_type j = i + 1; j < N; ++j) {
            auto point_i = config.get_position(i);
            auto point_j = config.get_position(j);
            separations(i, j) = Vector<real>{point_j - point_i};
            separations(j, i) = -separations(i, j);
        }
    }
    return separations;
}

real compute_angle(const Vector<real> &r_1, const Vector<real> &r_2,
                   const Vector<real> &r_3) {
    auto r_21_hat = (r_1 - r_2).normalize();
    auto r_23_hat = (r_3 - r_2).normalize();
    auto cos_theta = dot(r_21_hat, r_23_hat);
    auto theta = std::acos(cos_theta);
    return theta;
}

template <level_type n, level_type level>
void print_force_field(const ObjectRegistry<n> &reg,
                       const ForceField<n, level> &force_field) {
    std::cout << "\nForce field:\n" << std::endl;
    std::cout << "Raw form: \n" << force_field.get_values() << std::endl;
    std::cout << "Formatted:" << std::endl;
    std::cout << "\nForce center types:\n" << std::endl;
    auto no_fct = force_field.no_parameters(FF_param_type::charge);
    for (size_type i = 0; i != no_fct; ++i) {
        auto fct = reg.template get_object<ForceCenterType, level>(i);
        std::cout << fct << ": {Charge: "
                  << force_field.get_value(FF_param_type::charge, i)
                  << ", epsilon = "
                  << force_field.get_value(FF_param_type::epsilon, i)
                  << " kJ/mol, sigma = "
                  << force_field.get_value(FF_param_type::sigma, i) << " nm}"
                  << std::endl;
    }
    auto no_bonds = force_field.no_parameters(FF_param_type::eq_bond);
    if (no_bonds > 0) {
        std::cout << "\nBond types:\n" << std::endl;
        for (size_type i = 0; i != no_bonds; ++i) {
            auto bond_type = reg.template get_object<BondType, level>(i);
            std::cout << bond_type << ": {l0 = "
                      << force_field.get_value(FF_param_type::eq_bond, i)
                      << " nm, k = "
                      << force_field.get_value(FF_param_type::k_bond, i)
                      << " kJ/(mol*nm^2)}" << std::endl;
        }
    }
    auto no_bond_angles = force_field.no_parameters(FF_param_type::eq_angle);
    if (no_bond_angles > 0) {
        std::cout << "\nBond angle types:\n" << std::endl;
        for (size_type i = 0; i != no_bond_angles; ++i) {
            auto bond_angle_type =
                reg.template get_object<BondAngleType, level>(i);
            std::cout << bond_angle_type << ": {theta0 = "
                      << convert_units(
                             force_field.get_value(FF_param_type::eq_angle, i),
                             units::rad, units::degrees)
                      << " degrees, k = "
                      << force_field.get_value(FF_param_type::k_angle, i)
                      << " kJ/(mol*nm*rad)}" << std::endl;
        }
    }
}

} // namespace champion

#endif
