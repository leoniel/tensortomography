/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: ImproperTorsion.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:


\******************************************************************************/

#ifndef CHAMPION_IMPROPER_TORSION_HPP
#define CHAMPION_IMPROPER_TORSION_HPP

namespace champion {

/***************************** Class declarations *****************************/

// Forward declarations
template <level_type n> class ObjectRegistry;
template <level_type n, level_type level> class ImproperTorsionType;
template <level_type n, level_type level> class ForceCenter;

template <level_type n, level_type level> class ImproperTorsion {
  public:
    // Public types
    using constituent_type = ForceCenter<n, level>;
    using iterator = RegistryIterator<ImproperTorsion<n, level>, false>;
    using const_iterator = RegistryIterator<ImproperTorsion<n, level>, true>;

    template <level_type ll>
    using it_index = ObjectIndex<::champion::ImproperTorsion, n, ll>;
    template <level_type ll>
    using itt_index = ObjectIndex<ImproperTorsionType, n, ll>;
    template <level_type ll> using fc_index = ObjectIndex<ForceCenter, n, ll>;
    // Public member functions
    // Construct from ObjectRegistry, ID, torsion type and constituents
    ImproperTorsion(ObjectRegistry<n> &reg, it_index<level> ID,
                    itt_index<level + 1> itt, fc_index<level> fc1,
                    fc_index<level> fc2, fc_index<level> fc3,
                    fc_index<level> fc4)
        : reg_{&reg}, id_{ID}, improper_torsion_type_{itt}, constituents_{
                                                                fc1, fc2, fc3,
                                                                fc4} {
        std::sort(constituents_.begin() + 1, constituents_.end());
    }

    // Access

    // Index access
    static constexpr size_type size() { return 4; }
    it_index<level> id() const { return id_; }
    fc_index<level> get_index(size_type i) const { return constituents_[i]; }
    itt_index<level> get_type_index() const { return improper_torsion_type_; }
    fc_index<level> first() const { return constituents_[0]; }
    fc_index<level> second() const { return constituents_[1]; }
    fc_index<level> third() const { return constituents_[2]; }
    fc_index<level> fourth() const { return constituents_[3]; }

    // Object access
    iterator begin() { return iterator{*this}; }
    iterator end() { return iterator{*this, size()}; }
    const_iterator begin() const { return const_iterator{*this, size()}; }
    const_iterator end() const { return const_iterator{*this, size()}; }
    const_iterator cbegin() const { return const_iterator{*this}; }
    const_iterator cend() const { return const_iterator{*this, size()}; }

    const ForceCenter<n, level> &operator[](size_type i) const {
        return reg_->template get_object<ForceCenter, level>(constituents_[i]);
    }
    ForceCenter<n, level> &operator[](size_type i) {
        return const_cast<ForceCenter<n, level> &>(
            static_cast<const ImproperTorsion &>(*this)[i]);
    }

    const ImproperTorsionType<n, level + 1> &type() const {
        return reg_->template get_object<ImproperTorsionType, level + 1>(
            improper_torsion_type_);
    }

  private:
    // Private data members
    ObjectRegistry<n> *reg_;
    it_index<level> id_;
    itt_index<level> improper_torsion_type_;
    std::vector<fc_index<level>> constituents_;
};

template <level_type n, level_type level>
bool operator==(const ImproperTorsion<n, level> &it1,
                const ImproperTorsion<n, level> &it2) {
    assert(it1.size() == it2.size());
    for (size_type i = 0; i != it1.size(); ++i) {
        if (it1[i] != it2[i]) {
            return false;
        }
    }
    return true;
}

template <level_type n, level_type level>
bool operator!=(const ImproperTorsion<n, level> &it1,
                const ImproperTorsion<n, level> &it2) {
    return !(it1 == it2);
}

template <level_type n, level_type level>
std::ostream &print_brief(std::ostream &os,
                          const ImproperTorsion<n, level> &it) {
    os << "ImproperTorsion<" << level << "> #" << it.id()
       << " (ImproperTorsionType<" << level << "> #" << it.get_type_index()
       << ")";
    return os;
}

template <level_type n, level_type level>
std::ostream &print_full(std::ostream &os,
                         const ImproperTorsion<n, level> &it) {
    print_brief(os, it) << ": {";
    print_brief(os, it[0]) << ", ";
    print_brief(os, it[1]) << ", ";
    print_brief(os, it[2]) << ", ";
    print_brief(os, it[3]) << "}";
    return os;
}

template <level_type n, level_type level>
std::ostream &print_recursive(std::ostream &os,
                              const ImproperTorsion<n, level> &it) {
    os << "Bond<" << level << "> #" << it.id() << ": {";
    print_recursive(os, it[0]) << ", ";
    print_recursive(os, it[1]) << ", ";
    print_recursive(os, it[2]) << ", ";
    print_recursive(os, it[3]) << "}";
    return os;
}

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os,
                         const ImproperTorsion<n, level> &it) {
    print_full(os, it);
    return os;
}

} // namespace champion

#endif
