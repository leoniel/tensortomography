/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: Dictionary.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2018-01-17
MODIFIED BY:

DESCRIPTION:

\******************************************************************************/

#ifndef CHAMPION_DICTIONARY_HPP
#define CHAMPION_DICTIONARY_HPP

#include <cassert>
#include <fstream>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

#include "set_precision.hpp"

namespace champion {

class Dictionary {
  public:
    // Construct from string
    Dictionary(std::string str = "") : str_{str} { trim_whitespace(); }
    // Construct from ifstream
    Dictionary(std::ifstream &is) {
        std::stringstream config_stream;
        config_stream << is.rdbuf();
        str_ = config_stream.str();
        trim_whitespace();
    }
    // Construct from range of stringlike object
    template <typename str_it>
    Dictionary(const str_it &b, const str_it &e)
        : Dictionary(std::string{b, e}) {}
    // Return the underlying string
    const std::string &str() const { return str_; }
    // Perform key-value lookup
    Dictionary get_value(std::string key) const;
    // Create vector of constituent dictionaries
    std::vector<Dictionary> partition() const;
    std::string get_key() const;

  private:
    // Private member functions

    // Remove all combinations of whitespace, line breaks and comma separators
    // and replace with a single comma
    void trim_whitespace();

    // Private data members
    std::string str_;
};

Dictionary Dictionary::get_value(std::string key) const {
    // depth == 0 iff outside all {}
    size_type depth = 0;
    auto it = str_.begin();
    bool match;
    // Move through dictionary to find a match and set the iterator to that
    // position
    while (it != str_.end()) {
        if (*it == '{') {
            ++depth;
        } else if (*it == '}') {
            --depth;
        }
        if (depth == 0) {
            match = false;
            // If previous letter is not ',' or we're at the beginning of the
            // dictionary, don't try to match keyword
            if (it != str_.begin() && *(it - 1) != ',') {
                ++it;
                continue;
            }
            // Make sure the full key is matched
            bool match_for = true;
            for (size_type offset = 0; offset != key.size(); ++offset) {
                if (*(it + offset) != key[offset]) {
                    match_for = false;
                    break;
                }
            }
            if (!match_for) {
                ++it;
                continue;
            }
            // Make sure that the next character is not the continuation of a
            // keyword
            if ((*(it + key.size()) != ' ' && *(it + key.size()) != '=') &&
                *(it + key.size()) != '\n') {
                ++it;
                continue;
            }
            match = true;
            // // Print out dictionary with highligted match for debugging
            // std::string before = {str_.begin(), it};
            // std::string matchstr = {it, it + key.size()};
            // std::string after = {it + key.size(), str_.end()};
            // std::cout << before << "\033[1;31m" << matchstr << "\033[0m"
            //           << after << "\n"
            //           << std::endl;
            break;
        }
        ++it;
    }
    // Return empty dictionary if no match is found
    if (!match) {
        return Dictionary("");
    }
    // Move iterator forward to end of keyword
    it += key.size();
    // Move forward to the start of the value
    while (it != str_.end() && (*it < 'a' || *it > 'z') &&
           (*it < 'A' || *it > 'Z') && (*it < '0' || *it > '9') &&
           (*it != '_') && (*it != '.') && (*it != '-') && (*it != '+') &&
           (*it != '~') && (*it != '/') && (*it != '{')) {
        ++it;
    }
    bool remove_first_and_last = false;
    if (*it == '{') {
        remove_first_and_last = true;
    }
    assert(depth == 0);
    // Set beginning of the value string to be returned
    auto begin = it;
    while (it != str_.end()) {
        if (*it == '{') {
            ++depth;
        } else if (*it == '}') {
            --depth;
        }
        // Find the end of the value string to be returned
        if ((depth == 0) && (*it == ',')) {
            // Return a new dictionary from the value string
            if (remove_first_and_last) {
                return Dictionary(std::string(begin + 1, it - 1));
            } else {
                return Dictionary(std::string(begin, it));
            }
        }
        ++it;
    }
    // If end of dictionary was reached without finding end of value,
    // throw exception
    if (depth != 0) {
        throw std::runtime_error{
            "Format error in champion::Dictionary value:\nkey: \'" + key +
            "\', value: \'" + std::string(begin, it) + "\'"};
    }
    // If the sought value was the last piece of the dictionary, return it
    if (remove_first_and_last) {
        return Dictionary(std::string(begin + 1, it - 1));
    } else {
        return Dictionary(std::string(begin, it));
    }
}

std::vector<Dictionary> Dictionary::partition() const {
    auto vec = std::vector<Dictionary>{};
    auto depth = 0;
    auto start = str_.begin();
    if (start == str_.end()) {
        return vec;
    }
    for (auto it = str_.begin(); it != str_.end(); ++it) {
        if (*it == '{') {
            ++depth;
        } else if (*it == '}') {
            --depth;
        } else {
            if ((depth == 0) && (*it == ',')) {
                if (*start == '{') {
                    vec.push_back(Dictionary{std::string{start + 1, it - 1}});
                    start = it + 1;
                } else {
                    vec.push_back(Dictionary{std::string{start, it}});
                    start = it + 1;
                }
            }
        }
    }
    if (*start == '{') {
        vec.push_back(Dictionary{std::string{start + 1, str_.end() - 1}});
    } else {
        vec.push_back(Dictionary{std::string{start, str_.end()}});
    }
    return vec;
}

std::string Dictionary::get_key() const {
    for (auto it = str_.begin(); it != str_.end(); ++it) {
        if (*it == '=') {
            return std::string{str_.begin(), it};
        }
    }
    throw std::runtime_error{
        "champion::Dictionary::get_key(): no key was found in string: \"" +
        str_ + "\""};
}

void Dictionary::trim_whitespace() {
    // Remove comments
    str_ = std::regex_replace(str_, std::regex{R"(#.*\n)"}, "");
    str_ = std::regex_replace(str_, std::regex{R"(//.*\n)"}, "");
    str_ = std::regex_replace(str_, std::regex{R"(/\*(.|\n)*\*/)"}, "");
    // Turn all braces into curly braces
    str_ = std::regex_replace(str_, std::regex{R"(\[)"}, "{");
    str_ = std::regex_replace(str_, std::regex{R"(\()"}, "{");
    str_ = std::regex_replace(str_, std::regex{R"(\])"}, "}");
    str_ = std::regex_replace(str_, std::regex{R"(\))"}, "}");
    // Trim whitespace
    str_ = std::regex_replace(str_, std::regex{R"(\s*;\s*)"}, ",");
    str_ = std::regex_replace(str_, std::regex{R"(\s*=\s*)"}, "=");
    str_ = std::regex_replace(str_, std::regex{R"(\s*\{\s*)"}, "{");
    str_ = std::regex_replace(str_, std::regex{R"(\s*\}\s*)"}, "},");
    str_ = std::regex_replace(str_, std::regex{R"(\s*,\s*)"}, ",");
    str_ = std::regex_replace(str_, std::regex{R"(\s*,\s*)"}, ",");
    str_ = std::regex_replace(str_, std::regex{R"(\s+)"}, ",");
    str_ = std::regex_replace(str_, std::regex{R"(,,)"}, ",");
    str_ = std::regex_replace(str_, std::regex{R"(,\})"}, "}");
    if (str_[str_.size() - 1] == ',') {
        str_.erase(str_.end() - 1);
    }
}

std::ostream &operator<<(std::ostream &os, Dictionary dict) {
    std::cout << "Dictionary: ";
    std::cout << dict.str();
    return os;
}

} // namespace champion

#endif
