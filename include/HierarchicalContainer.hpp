/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017-2018 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: HierarchicalContainer.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2018-01-17
MODIFIED BY:

USAGE:

DESCRIPTION:

\******************************************************************************/

#ifndef CHAMPION_HIERARCHICAL_CONTAINER_HPP
#define CHAMPION_HIERARCHICAL_CONTAINER_HPP

#include <utility>

#include "set_precision.hpp"

namespace champion {

namespace internal {

template <level_type n, template <typename> class Container,
          template <level_type, level_type> class T, level_type... ni>
std::tuple<Container<T<n, ni>>...>
make_hierarchical_container_impl(std::integer_sequence<level_type, ni...> seq) {
    return {};
}

template <template <typename, typename...> class Container,
          template <level_type, level_type> class T, level_type n,
          typename... Args>
struct MakeHierarchicalContainer {
    template <typename TT> using ContainerType = Container<TT, Args...>;
    constexpr auto operator()() {
        return make_hierarchical_container_impl<n, ContainerType, T>(
            std::make_integer_sequence<level_type, n>());
    }
};

} // namespace internal

template <template <typename, typename...> class Container,
          template <level_type, level_type> class T, level_type n,
          typename... Args>
auto make_hierarchical_container() {
    return internal::MakeHierarchicalContainer<Container, T, n, Args...>{}();
}

template <template <typename, typename...> class Container,
          template <level_type, level_type> class T, level_type n,
          typename... Args>
using HierarchicalContainer =
    decltype(make_hierarchical_container<Container, T, n, Args...>());

} // namespace champion

#endif
