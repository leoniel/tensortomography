/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: RegistryIterator.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-27
MODIFIED BY:

DESCRIPTION: Random access iterator to constituents of objects where both the
object and its constituents are owned by an object registry.

Uses IDs stored in the object to index into the ObjectRegistry in order to
retrieve pointers to the elements.

\******************************************************************************/

#ifndef CHAMPION_REGISTRY_ITERATOR_HPP
#define CHAMPION_REGISTRY_ITERATOR_HPP

#include <iterator>

#include "choose.hpp"
#include "set_precision.hpp"

/***************************** Class declarations *****************************/

namespace champion {

template <typename T, bool is_const> class RegistryIterator {
  public:
    using ObjectType = Choose_type<is_const, const T, T>;
    using ValueType = Choose_type<is_const, const typename T::constituent_type,
                                  typename T::constituent_type>;

    using difference_type = ptrdiff_t;
    using value_type = ValueType;
    using pointer = ValueType *;
    using reference = ValueType &;
    using iterator_category = std::random_access_iterator_tag;

    // Default constructor
    RegistryIterator() : object_{nullptr}, num_constituents_{0}, index_{0} {}

    // Construct from reference to registry-owned object and constituent index
    RegistryIterator(ObjectType &obj, size_type index = 0)
        : object_{&obj}, num_constituents_{obj.size()}, index_{index} {}

    reference operator*() const;
    reference operator*();
    template <typename member> pointer operator->() const;

    reference operator[](difference_type i);

    RegistryIterator &operator+=(difference_type a) {
        index_ += a;
        return *this;
    }
    RegistryIterator &operator-=(difference_type a) { return *this += -a; }
    difference_type operator-(const RegistryIterator &it) {
        return index_ - it.index_;
    }

    // Prefix increment/decrement
    RegistryIterator &operator++();
    RegistryIterator &operator--();
    // Postfix increment/decrement
    RegistryIterator operator++(int);
    RegistryIterator operator--(int);
    bool operator==(const RegistryIterator &It) const;
    bool operator<(const RegistryIterator &It) const;

  private:
    ObjectType *object_;
    size_type num_constituents_;
    size_type index_;
};

/************************ Member Function definitions *************************/

template <typename T, bool is_const>
typename RegistryIterator<T, is_const>::reference
    RegistryIterator<T, is_const>::operator*() const {
    return (*object_)[index_];
}

template <typename T, bool is_const>
typename RegistryIterator<T, is_const>::reference
    RegistryIterator<T, is_const>::operator*() {
    return (*object_)[index_];
}

template <typename T, bool is_const>
template <typename member>
typename RegistryIterator<T, is_const>::pointer RegistryIterator<T, is_const>::
operator->() const {
    return &(*object_)[index_];
}

template <typename T, bool is_const>
typename RegistryIterator<T, is_const>::reference
    RegistryIterator<T, is_const>::
    operator[](RegistryIterator<T, is_const>::difference_type i) {
    return (*object_)[i];
}

template <typename T, bool is_const>
RegistryIterator<T, is_const> &
operator+(const RegistryIterator<T, is_const> &it_,
          typename RegistryIterator<T, is_const>::difference_type a) {
    auto it_result = it_;
    return it_ += a;
}

template <typename T, bool is_const>
RegistryIterator<T, is_const> &
operator+(typename RegistryIterator<T, is_const>::difference_type a,
          const RegistryIterator<T, is_const> &it_) {
    auto it_result = it_;
    return it_ += a;
}

template <typename T, bool is_const>
RegistryIterator<T, is_const> &
operator-(const RegistryIterator<T, is_const> &it_,
          typename RegistryIterator<T, is_const>::difference_type a) {
    auto it_result = it_;
    return it_ -= a;
}

template <typename T, bool is_const>
RegistryIterator<T, is_const> &RegistryIterator<T, is_const>::operator++() {
    *this += 1;
    return *this;
}

template <typename T, bool is_const>
RegistryIterator<T, is_const> &RegistryIterator<T, is_const>::operator--() {
    *this -= 1;
    return *this;
}

template <typename T, bool is_const>
RegistryIterator<T, is_const> RegistryIterator<T, is_const>::operator++(int) {
    auto it_ = *this;
    *this ++;
    return it_;
}

template <typename T, bool is_const>
RegistryIterator<T, is_const> RegistryIterator<T, is_const>::operator--(int) {
    auto it_ = *this;
    *this --;
    return it_;
}

template <typename T, bool is_const>
bool RegistryIterator<T, is_const>::
operator==(const RegistryIterator<T, is_const> &It) const {
    return (this->object_ == It.object_ && this->index_ == It.index_);
}

template <typename T, bool is_const>
bool RegistryIterator<T, is_const>::
operator<(const RegistryIterator<T, is_const> &It) const {
    return (this->object_ == It.object_ && this->index_ < It.index_);
}

/********************** Non-member Function definitions *≈*********************/

template <typename T, bool is_const1, bool is_const2>
bool operator!=(const RegistryIterator<T, is_const1> &It1,
                const RegistryIterator<T, is_const2> &It2) {
    return !(It1 == It2);
}

template <typename T, bool is_const1, bool is_const2>
bool operator>(const RegistryIterator<T, is_const1> &It1,
               const RegistryIterator<T, is_const1> &It2) {
    return (It2 < It1);
}

template <typename T, bool is_const1, bool is_const2>
bool operator<=(const RegistryIterator<T, is_const1> &It1,
                const RegistryIterator<T, is_const2> &It2) {
    return ((It1 < It2) || (It1 == It2));
}

template <typename T, bool is_const1, bool is_const2>
bool operator>=(const RegistryIterator<T, is_const1> &It1,
                const RegistryIterator<T, is_const2> &It2) {
    return ((It1 > It2) || (It1 == It2));
}

} // namespace champion

#endif
