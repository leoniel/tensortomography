/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  _  \ /| | // __ \  /|   \    /| |
 //  /  \_#| |  |#| |#| |  |#| |#|   \/   |#| |# \ \#| |// / \\ \|#| |\ \  |#| |
|#| |    |#| |__|#| |#| |__|#| |#| |\  /| |#| |# | |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| |_ / /#| |#| | |#| |#| |\#\ \ #| |
|#| |   __#| |  |#| |#| |  |#| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |    |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: Tree.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:

Defines two interoperating abstract base class templates:
Tree<T> which abstracts the notion of a general tree data structure with value
type T, and TreeNode<T> which abstracts the nodes of such a tree. Specifies
interface for navigating such trees.

\******************************************************************************/

#ifndef CHAMPION_TREE_HPP
#define CHAMPION_TREE_HPP

#include <iostream>
#include <map>
#include <sstream>
#include <string>

#include "NodeVisitor.hpp"
#include "TreeIterator.hpp"

namespace champion {

/***************************** Class declarations *****************************/

// Forward declarations
template <typename T> class Tree;
template <typename T> class NodeVisitor;

template <typename T> class TreeNode {
  public:
    virtual ~TreeNode() {}
    // Value of current node
    virtual T value() const = 0;
    // Which layer in the tree is this on? (root layer = 0)
    virtual size_type layer() const = 0;
    // The number of nodes below this
    virtual size_type size() const = 0;
    // How deep is the deepest tree starting at this node? (depth(leaf) = 0)
    virtual size_type depth() const = 0;
    // Navigation (returns nullptr if out of bounds) Return pointer to
    // unique parent node
    virtual const TreeNode<T> *up() const = 0;
    virtual TreeNode<T> *up() = 0;
    // Return pointer to leftmost child node
    virtual const TreeNode<T> *down() const = 0;
    virtual TreeNode<T> *down() = 0;
    // Return pointer to next node with the same parent as this
    virtual const TreeNode<T> *right() const = 0;
    virtual TreeNode<T> *right() = 0;
    // Return pointer to previous node with the same parent as this
    virtual const TreeNode<T> *left() const = 0;
    virtual TreeNode<T> *left() = 0;
    // Return vector of pointers to all nodes sharing this' parent
    virtual std::vector<const TreeNode<T> *> siblings() const = 0;
    virtual std::vector<TreeNode<T> *> siblings() = 0;
    // Return vector of pointers to all nodes that have this as a parent
    virtual std::vector<const TreeNode<T> *> children() const = 0;
    virtual std::vector<TreeNode<T> *> children() = 0;
    // Dispatch a visitor
    virtual void accept(ConstNodeVisitor<T> &visit) const = 0;
    virtual void accept(NodeVisitor<T> &visit) = 0;
    // Explore all nodes downstream of this
    void explore_downstream(ConstNodeVisitor<T> &visitor) const;
    void explore_downstream(NodeVisitor<T> &visitor);
};

template <typename T> class Tree {
  public:
    using iterator = TreeIterator<T, false>;
    using const_iterator = TreeIterator<T, true>;
    virtual ~Tree() {}
    // Number of nodes in tree
    virtual size_type size() const;
    // How deep is the tree? (depth(leaf) = 0)
    virtual size_type depth() const;
    // Get a pointer to the root node
    virtual const TreeNode<T> *root() const = 0;
    virtual TreeNode<T> *root() = 0;
    // Get a pointer to the tail node
    virtual const TreeNode<T> *tail() const = 0;
    virtual TreeNode<T> *tail() = 0;
    // Get the "index" of the node in the tree
    virtual size_type get_index(const TreeNode<T> *node) const;
    // Get the node with ordinal number index in the tree
    virtual const TreeNode<T> *get_node(size_type index) const;
    virtual TreeNode<T> *get_node(size_type index);
    // Range based access
    virtual iterator begin() = 0;
    virtual iterator end() = 0;
    virtual const_iterator begin() const = 0;
    virtual const_iterator end() const = 0;
    virtual const_iterator cbegin() const = 0;
    virtual const_iterator cend() const = 0;
    // Traverse the tree to visit all nodes, depth first
    virtual void visit_nodes(ConstNodeVisitor<T> &visitor) const = 0;
    virtual void visit_nodes(NodeVisitor<T> &visitor) = 0;
};

// TreeNode function definitions

template <typename T>
void TreeNode<T>::explore_downstream(ConstNodeVisitor<T> &visitor) const {
    accept(visitor);
    auto next = down();
    for (; next != nullptr; next = next->right()) {
        next->explore_downstream(visitor);
    }
}
template <typename T>
void TreeNode<T>::explore_downstream(NodeVisitor<T> &visitor) {
    accept(visitor);
    auto next = down();
    for (; next != nullptr; next = next->right()) {
        next->explore_downstream(visitor);
    }
}

// Tree function definitions

// Get the "index" of the node in the tree
template <typename T>
size_type Tree<T>::get_index(const TreeNode<T> *node) const {
    size_type index = 0;
    auto it = this->begin();
    while (it != this->end()) {
        ++it;
        ++index;
    }
    return index;
}

// Go to the node with ordinal number <index>
template <typename T>
const TreeNode<T> *Tree<T>::get_node(size_type index) const {
    auto it = this->begin();
    for (size_type i = 0; i != index; ++i) {
        ++it;
    }
    return &*it;
}

template <typename T> TreeNode<T> *Tree<T>::get_node(size_type index) {
    return const_cast<TreeNode<T> *>(
        static_cast<const Tree<T> *>(this)->get_node(index));
}

// Functions operating on TreeNodes

template <typename T> size_type count_leaves(const TreeNode<T> *node) {
    auto leaf_counter = LeafCounter<T>{};
    node->explore_downstream(leaf_counter);
    return leaf_counter.count();
}

// Functions operating on Trees

template <typename T> size_type Tree<T>::size() const {
    auto node_counter = NodeCounter<T>{};
    this->visit_nodes(node_counter);
    return node_counter.count();
}

template <typename T> size_type Tree<T>::depth() const {
    return root()->depth();
}

template <typename T> size_type count_leaves(const Tree<T> *tree) {
    auto leaf_counter = LeafCounter<T>{};
    tree->visit_nodes(leaf_counter);
    return leaf_counter.count();
}

template <typename T> std::vector<TreeNode<T> *> get_leaves(Tree<T> *tree) {
    auto node_recorder = NodeRecorder<T>{};
    tree->visit_nodes(node_recorder);
    return node_recorder.get_leaves();
}

template <typename T>
std::vector<const TreeNode<T> *> get_leaves(const Tree<T> *tree) {
    auto node_recorder = ConstNodeRecorder<T>{};
    tree->visit_nodes(node_recorder);
    return node_recorder.get_leaves();
}

template <typename T>
std::vector<const TreeNode<T> *> get_layer(size_type i, const Tree<T> *tree) {
    auto layer_recorder = ConstLayerRecorder<T>(i);
    tree->visit_nodes(layer_recorder);
    return layer_recorder.get_nodes();
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const Tree<T> &tree) {
    os << std::endl;
    auto width_recorder = OutputWidthVisitor<T>{};
    tree.visit_nodes(width_recorder);
    auto widths = width_recorder.widths();
    size_type el_width = 0;
    for (const auto &el : widths) {
        if (el.second > el_width) {
            el_width = el.second;
        }
    }
    auto space_width = 2;
    auto num_leaves = count_leaves(&tree);
    auto linewidth = num_leaves * el_width + (num_leaves - 1) * space_width;
    std::map<const TreeNode<T> *, size_type> origins;
    std::map<const TreeNode<T> *, size_type> positions;
    origins[tree.root()->up()] = 0;
    size_type i = 0;
    auto current_layer = get_layer(i, &tree);
    while (!current_layer.empty()) {
        auto line = std::string(linewidth, ' ');
        auto connections = std::string(linewidth, ' ');
        for (auto l_it = current_layer.begin(); l_it != current_layer.end();) {
            auto current = *l_it;
            auto parent = current->up();
            auto origin = origins[parent];
            size_type offset = 0;
            for (auto s : current->siblings()) {
                origins[s] = origin + offset;
                auto num_leaves = count_leaves(s);
                auto print_width =
                    num_leaves * el_width + (num_leaves - 1) * space_width;
                auto str = std::string(el_width, '0');
                str.replace(el_width - widths[s], widths[s],
                            std::to_string(s->value()));
                auto pos = origin + offset + print_width / 2;
                positions[s] = pos;
                line.replace(pos, el_width, str);
                if (s != tree.root()) {
                    if (pos < positions[parent]) {
                        connections[pos + 1] = '/';
                    } else if (pos > positions[parent]) {
                        connections[pos - 1] = '\\';
                    } else {
                        connections[pos] = '|';
                    }
                }
                offset += print_width + space_width;
                ++l_it;
            }
        }
        os << connections << std::endl;
        os << line << std::endl;
        current_layer = get_layer(++i, &tree);
    }
    return os;
}
} // namespace champion

#endif
