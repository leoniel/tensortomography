/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

SOURCE FILE: optimize_force_field.cpp

AUTHOR: Fabian �r�n, Rasmus Andersson 2018-05-08
MODIFIED BY:

DESCRIPTION:

\******************************************************************************/

#include <iostream>
#include <memory>
#include <vector>

#include "Dictionary.hpp"
#include "EvolutionaryAlgorithm.hpp"
#include "ObjectRegistry.hpp"
#include "VectorSlice.hpp"
#include "io.hpp"
#include "set_precision.hpp"
#include "units.hpp"

#include "OpenMM.h"

namespace champion {

// Must be set to the number of options in InteractionParameterTypes
const size_type no_interaction_types = 11;
enum InteractionParameterTypes {
    charge,
    eta,
    sigma,
    eq_bond,
    k_bond,
    eq_angle,
    k_angle,
    eq_torsion,
    k_torsion,
    eq_improper,
    k_improper
};

using Force = std::vector<real>;

class OpenMMInterface {
  public:
    // Construct from object registry and genome
    OpenMMInterface(ObjectRegistry<3> reg, Genome genome);

    virtual ~OpenMMInterface() {}

    std::vector<OpenMM::Vec3> get_forces(Individual individual);

  private:
    // Private data members
    ObjectRegistry<3> reg_;
    Genome genome_;
    std::unique_ptr<OpenMM::System> system_;
    // Vector over type indices, each containing indices into genome_ of that
    // particular type
    std::vector<std::vector<size_type>> type_indices_;
};

OpenMMInterface::OpenMMInterface(ObjectRegistry<3> reg, Genome genome)
    : reg_{reg}, genome_{genome}, system_{std::make_unique<OpenMM::System>()} {
    // Get the atoms from the object registry
    auto atoms = reg_.get_objects<Body, 0>();

    // Create atoms in system_
    for (const auto &el : atoms) {
        real mass = el.second.type().get_mass();
        system_->addParticle(mass);
    }
    // Initialize type_indices
    for (size_type ti = 0; ti < no_interaction_types; ++ti) {
        std::vector<size_type> element;
        type_indices_.push_back(element);
    }
    for (size_type gi = 0; gi < genome_.size(); ++gi) {
        for (size_type ti = 0; ti < no_interaction_types; ++ti) {
            if (genome_.get_type(gi) == ti) {
                type_indices_[ti].push_back(gi);
                break;
            }
        }
    }
}

std::vector<OpenMM::Vec3> OpenMMInterface::get_forces(Individual individual) {
    // Create OpenMM forces (interaction types)
    auto nonbonded = std::make_unique<OpenMM::NonbondedForce>();
    auto bond_stretch = std::make_unique<OpenMM::HarmonicBondForce>();
    auto bond_bend = std::make_unique<OpenMM::HarmonicAngleForce>();
    auto bond_torsion = std::make_unique<OpenMM::PeriodicTorsionForce>();
    auto bond_improper = std::make_unique<OpenMM::PeriodicTorsionForce>();

    // Get all interactions from object registry
    auto atoms = reg_.get_objects<Body, 0>();
    auto harmonic_bonds = reg_.get_objects<Bond, 0>();
    auto bond_angles = reg_.get_objects<BondAngle, 0>();
    auto torsions = reg_.get_objects<Torsion, 0>();
    auto impropers = reg_.get_objects<ImproperTorsion, 0>();

    // Add atoms to nonbondeded interactions
    for (size_type index = 0; index != atoms.size(); ++index) {
        double q = individual[type_indices_[InteractionParameterTypes::charge][index]];
        double sigma_ =
            individual[type_indices_[InteractionParameterTypes::sigma][index]];
        double eta_ = individual[type_indices_[InteractionParameterTypes::eta][index]];
        nonbonded->addParticle(q, sigma_, eta_);
    }
    // Add atoms to bonds
    size_type c = 0;
    for (auto el : harmonic_bonds) {
        size_type first = el.second[0].body().id();
        size_type second = el.second[1].body().id();
        double eq = individual[type_indices_[InteractionParameterTypes::eq_bond][c]];
        double k = individual[type_indices_[InteractionParameterTypes::k_bond][c]];
        bond_stretch->addBond(first, second, eq, k);
        c++;
    }
    // Add atoms to angles
    c = 0;
    for (auto el : bond_angles) {
        size_type first = el.second[0].body().id();
        size_type second = el.second[1].body().id();
        size_type third = el.second[2].body().id();
        double eq = individual[type_indices_[InteractionParameterTypes::eq_angle][c]];
        double k = individual[type_indices_[InteractionParameterTypes::k_angle][c]];
        bond_bend->addAngle(first, second, third, eq, k);
        c++;
    }
    // Add atoms to (proper) torsions
    c = 0;
    int periodicity = 2; /**temporary**/
    for (auto el : torsions) {
        size_type first = el.second[0].body().id();
        size_type second = el.second[1].body().id();
        size_type third = el.second[2].body().id();
        size_type fourth = el.second[3].body().id();
        double eq = individual[type_indices_[InteractionParameterTypes::eq_torsion][c]];
        double k = individual[type_indices_[InteractionParameterTypes::k_torsion][c]];
        bond_torsion->addTorsion(first, second, third, fourth, periodicity, eq,
                                k);
        c++;
    }
    // Add atoms to improper torsions
    c = 0;
    for (auto el : impropers) {
        size_type first = el.second[0].body().id();
        size_type second = el.second[1].body().id();
        size_type third = el.second[2].body().id();
        size_type fourth = el.second[3].body().id();
        double eq =
            individual[type_indices_[InteractionParameterTypes::eq_improper][c]];
        double k = individual[type_indices_[InteractionParameterTypes::k_improper][c]];
        bond_improper->addTorsion(first, second, third, fourth, periodicity, eq,
                                 k);
        c++;
    }
    // Add the created forces to the system_
    system_->addForce(nonbonded.get());
    system_->addForce(bond_stretch.get());
    system_->addForce(bond_bend.get());
    system_->addForce(bond_torsion.get());
    system_->addForce(bond_improper.get());

    // Create vector of OpenMM contexts
    auto integrator_vec = std::vector<std::shared_ptr<OpenMM::Integrator>>{};
    auto context_vec = std::vector<std::unique_ptr<OpenMM::Context>>{};
    auto ConfigMap = reg_.get_objects<Configuration, 0>();
    for (size_type i = 0; i != ConfigMap.size(); ++i) {
        auto config = ConfigMap.at(i);
        auto confpos = config.get_positions();

        std::vector<OpenMM::Vec3> oMMVec;
        std::vector<OpenMM::Vec3> Vel;

        for (size_type j = 0; j != confpos.size(); ++j) {
            auto point = confpos[j];
            auto x = point[0].value(), y = point[1].value(),
                 z = point[2].value();
            auto oMMpoint = OpenMM::Vec3(x, y, z);
            oMMVec.push_back(oMMpoint);
            Vel.push_back(OpenMM::Vec3(0, 0, 0));
        }
        auto integrator = std::make_shared<OpenMM::VerletIntegrator>(
            OpenMM::VerletIntegrator{0.0});
        auto context = std::make_unique<OpenMM::Context>(*system_, *integrator);
        context->setTime(0.0);
        context->setPositions(oMMVec);
        context->setVelocities(Vel);
        integrator_vec.push_back(integrator);
        context_vec.push_back(std::move(context));
    }
    std::vector<OpenMM::Vec3> forces;
    // Create state and compute forces. Print results
    for (auto &context : context_vec) {
        OpenMM::State state = context->getState(OpenMM::State::Forces, false);
        forces = state.getForces();

        for (auto i = 0; i != 2; ++i) {
             for (auto j = 0; j != 3; ++j) {
                 std::cout << forces[i][j] << ' ';
             }
             std::cout << std::endl;
         }
    }
    return forces;
}

} // namespace champion
