/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: Body.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:

Class template to represent individual mechanical bodies, templated on
hierarchical level. Level 0 denotes simple point particles with fixed
mass and radius and no internal structure. Higher levels represent bodies
composed by graphs of lower level force centers.

\******************************************************************************/

#ifndef CHAMPION_BODY_HPP
#define CHAMPION_BODY_HPP

#include <algorithm>
#include <utility> // For >, !=, <=, >=

#include "BodyType.hpp"
#include "ObjectIndex.hpp"
#include "RegistryIterator.hpp"
#include "set_precision.hpp"

namespace champion {

/******************** Class declarations and definitions  *********************/

// Forward declarations
template <level_type n> class ObjectRegistry;
template <level_type n, level_type level> class Trajectory;
template <level_type n, level_type level> class Body;
template <level_type n, level_type level> class ForceCenter;
template <level_type n, level_type level>
std::ostream &print_neighbors(std::ostream &os, const Body<n, level> &body);

// Primary class template Body
template <level_type n, level_type level> class Body {
  public:
    // Public types
    using constituent_type = Body<n, level - 1>;
    using iterator = RegistryIterator<Body<n, level>, false>;
    using const_iterator = RegistryIterator<Body<n, level>, true>;

    template <level_type ll>
    using body_index = ObjectIndex<::champion::Body, n, ll>;
    template <level_type ll> using bt_index = ObjectIndex<BodyType, n, ll>;
    template <level_type ll> using fc_index = ObjectIndex<ForceCenter, n, ll>;
    template <level_type ll>
    using trajectory_index = ObjectIndex<Trajectory, n, ll>;

    // Public member functions
    // Construct from object registry, ID, body type index and force center
    // indices
    Body(ObjectRegistry<n> &reg, size_type ID, bt_index<level> body_type,
         Graph<body_index<level - 1>> constituent_graph,
         size_type trajectory_id = 0, size_type birth_time = 0,
         size_type death_time = s_max);

    // Access
    size_type size() const { return graph_.size(); }
    body_index<level> get_id() const { return id_; }
    iterator begin() { return iterator{*this}; }
    iterator end() { return iterator{*this, size()}; }
    const_iterator begin() const { return const_iterator{*this}; }
    const_iterator end() const { return const_iterator{*this, size()}; }
    const_iterator cbegin() const { return const_iterator{*this}; }
    const_iterator cend() const { return const_iterator{*this, size()}; }
    const Body<n, level - 1> &operator[](size_type i) const {
        return reg_->template get_object<Body, level - 1>(graph_[i]);
    }
    Body<n, level - 1> &operator[](size_type i) {
        return const_cast<Body<n, level - 1> &>(
            (static_cast<const Body &>(*this))[i]);
    }

    template <level_type ll>
    std::enable_if_t<(ll == level - 1), std::vector<body_index<ll>>>
    get_constituent_ids() const;
    template <level_type ll>
    std::enable_if_t<(ll < level - 1), std::vector<body_index<ll>>>
    get_constituent_ids() const;

    template <level_type ll>
    std::enable_if_t<(ll < level), std::vector<Body<n, ll>>>
    get_constituents() const {
        auto constit_ids = get_constituent_ids<ll>();
        auto no_constituents = constit_ids.size();
        auto res = std::vector<Body<n, ll>>{};
        for (auto c_id : constit_ids) {
            res.push_back(reg_->get_object(c_id));
        }
        return res;
    }

    bt_index<level> get_type_id() const { return body_type_; }
    const BodyType<n, level> &type() const {
        return reg_->template get_object<BodyType, level>(body_type_);
    }
    BodyType<n, level> &type() {
        return reg_->template get_object<BodyType, level>(body_type_);
    }
    const Trajectory<n, level> &trajectory() const {
        return reg_->get_object(trajectory_);
    }
    trajectory_index<level> get_trajectory_id() const { return trajectory_; }
    static constexpr level_type get_level() { return level; }
    const Graph<body_index<level - 1>> &graph() const { return graph_; }
    size_type get_birth_time() const { return birth_time_; }
    size_type get_death_time() const { return death_time_; }
    void set_birth_time(size_type i) {
        if (i != 0 &&
            i >= reg_->template get_object<Trajectory, level - 1>(trajectory_)
                     .size()) {
            throw std::out_of_range{
                "CHAMPION::Body::set_birth_time(): Time index out of bounds."};
        }
        birth_time_ = i;
        // Set *this as the parent of all its constituent Body<n, level - 1>s
        // from the time of its creation.
        for (auto id : get_constituent_ids<level - 1>()) {
            auto &constituent = reg_->get_object(id);
            constituent.set_parent(birth_time_, id_);
        }
    }
    void set_death_time(size_type i) {
        if (i < birth_time_) {
            throw std::runtime_error{"CHAMPION::Body::set_death_time(): Death "
                                     "time must be later than birth time."};
        }
        death_time_ = i;
    }
    real get_life_time() const {
        const auto &trajectory = reg_->get_object(trajectory_);
        auto death_time = std::min(death_time_, trajectory.size() - 1);
        auto time_of_death = trajectory.get_time(death_time);
        auto time_of_birth = trajectory.get_time(birth_time_);
        return time_of_death - time_of_birth;
    }
    const auto &get_parents() const { return parents_; }
    // Get id of parent at time step i
    body_index<level + 1> get_parent_id(size_type ti) const {
        size_type j = 0;
        while (parents_[j].first < ti) {
            ++j;
        }
        return parents_[j].second;
    }
    const Body<n, level + 1> &parent(size_type ti) const {
        return reg_->get_object(get_parent_id(ti));
    }
    Body<n, level + 1> &parent(size_type ti) {
        return reg_->get_object(get_parent_id(ti));
    }
    void set_parent(size_type ti, body_index<level + 1> parent) {
        auto it = parents_.begin();
        while (it != parents_.end() && it->first < ti) {
            ++it;
        }
        parents_.insert(
            it, std::pair<size_type, body_index<level + 1>>{ti, parent});
    }

    const auto &get_force_centers() const { return force_centers_; }
    // Get id of force_center at time step i
    fc_index<level> get_force_center_id(size_type ti) const {
        size_type j = 0;
        while (force_centers_[j].first < ti) {
            ++j;
        }
        return force_centers_[j].second;
    }
    const ForceCenter<n, level> &force_center(size_type ti) const {
        return reg_->get_object(get_force_center_id(ti));
    }
    ForceCenter<n, level> &force_center(size_type ti) {
        return reg_->get_object(get_force_center_id(ti));
    }
    void set_force_center(size_type ti, fc_index<level> force_center) {
        auto it = force_centers_.begin();
        while (it != force_centers_.end() && it->first < ti) {
            ++it;
        }
        force_centers_.insert(
            it, std::pair<size_type, fc_index<level>>{ti, force_center});
    }

    // Get neighbors at timestep ti
    std::vector<body_index<level>> get_neighbors(size_type ti) const {
        auto neighbors = std::vector<body_index<level>>{};
        for (const auto &neighbor : neighbors_) {
            for (const auto &time_window : neighbor.second) {
                if (time_window.first <= ti && time_window.second >= ti) {
                    neighbors.push_back(neighbor.first);
                    break;
                }
            }
        }
        return neighbors;
    }
    // Get all neighbors including bond birth and death times
    const auto &get_neighbors() const { return neighbors_; }
    // Add a neighbor or extend the time window of the neighborhood
    void set_neighbor(body_index<level> id, size_type start_time = 0,
                      size_type end_time = s_max) {
        auto it = find_if(neighbors_.begin(), neighbors_.end(),
                          [id](const auto &neighbor) {
                              if (neighbor.first == id)
                                  return true;
                              else
                                  return false;
                          });
        if (it == neighbors_.end()) {
            neighbors_.push_back({id, {{start_time, end_time}}});
        } else {
            auto &neighbor = *it;
            auto &time_windows = neighbor.second;
            for (auto it = time_windows.begin(); it < time_windows.end();
                 ++it) {
                if (it->first <= start_time) {
                    time_windows.insert(it, {start_time, end_time});
                    break;
                }
            }
            std::sort(time_windows.begin(), time_windows.end());
            for (auto it = ++time_windows.rbegin(); it < time_windows.rend();
                 ++it) {
                if (it->second >= (it - 1)->first - 1) {
                    it->second = (it - 1)->second;
                    time_windows.erase(it.base());
                }
            }
        }
    }

  private:
    // Friends
    friend std::ostream &print_neighbors<n, level>(std::ostream &,
                                                   const Body<n, level> &);
    // Private member functions
    std::vector<body_index<level - 1>> get_constituents_impl() const;
    // Private data members
    ObjectRegistry<n> *reg_;
    body_index<level> id_;
    bt_index<level> body_type_;
    fc_index<level> force_center_;
    Graph<body_index<level - 1>> graph_;
    trajectory_index<level> trajectory_;
    size_type birth_time_;
    size_type death_time_;
    std::vector<std::pair<size_type, body_index<level + 1>>> parents_;
    std::vector<std::pair<size_type, fc_index<level>>> force_centers_;
    // Contains body index, and birth and death time of the bond
    std::vector<std::pair<body_index<level>,
                          std::vector<std::pair<size_type, size_type>>>>
        neighbors_;
};

// Specialization of Body for level = 0 (point particle)
template <level_type n> class Body<n, 0> {
  public:
    // Public types
    // Dummy iterator to fullfill Body interface
    using const_iterator = const void *;
    template <level_type ll> using bt_index = ObjectIndex<BodyType, n, ll>;
    template <level_type ll>
    using body_index = ObjectIndex<::champion::Body, n, ll>;
    template <level_type ll> using fc_index = ObjectIndex<ForceCenter, n, ll>;
    template <level_type ll>
    using trajectory_index = ObjectIndex<Trajectory, n, ll>;
    // Public member functions
    // Construct from object registry, ID, body type and trajectory index
    // (optional)
    Body(ObjectRegistry<n> &reg, size_type ID, bt_index<0> body_type,
         size_type trajectory = 0, size_type birth_time = 0,
         size_type death_time = std::numeric_limits<size_type>::max())
        : reg_{&reg}, id_{ID}, body_type_{body_type},
          trajectory_{trajectory}, parents_{}, force_centers_{}, neighbors_{} {
        set_birth_time(birth_time);
        set_death_time(death_time);
    }

    size_type size() const { return 1; }
    // Dummy operators to fullfill Body interface
    const_iterator begin() const { return nullptr; }
    const_iterator end() const { return nullptr; }
    const_iterator cbegin() const { return nullptr; }
    const_iterator cend() const { return nullptr; }
    const Body<n, -1> &operator[](size_type i) const {
        throw std::out_of_range{"Body<n, 0>s have no elements."};
    }
    body_index<0> get_id() const { return id_; }

    const BodyType<n, 0> &type() const {
        return reg_->template get_object<BodyType, 0>(body_type_);
    }
    BodyType<n, 0> &type() {
        return reg_->template get_object<BodyType, 0>(body_type_);
    }
    bt_index<0> get_type_id() const { return body_type_; }
    static constexpr level_type get_level() { return 0; }
    const Trajectory<n, 0> &trajectory() const {
        return reg_->get_object(trajectory_);
    }
    trajectory_index<0> get_trajectory_id() const { return trajectory_; }
    template <level_type ll> const Trajectory<n, ll> &trajectory() const {
        return reg_->get_object(trajectory_);
    }
    template <level_type ll> Trajectory<n, ll> &trajectory() {
        return reg_->get_object(trajectory_);
    }
    size_type get_birth_time() const { return birth_time_; }
    size_type get_death_time() const { return death_time_; }
    void set_birth_time(size_type i) {
        if (i != 0 && i >= reg_->get_object(trajectory_).size()) {
            throw std::out_of_range{
                "CHAMPION::Body::set_birth_time(): Time index out of bounds."};
        }
        birth_time_ = i;
    }
    void set_death_time(size_type i) {
        if (i < birth_time_) {
            throw std::runtime_error{"CHAMPION::Body::set_death_time(): Death "
                                     "time must be later than birth time."};
        }
        death_time_ = i;
    }
    real get_life_time() const {
        const auto &trajectory = reg_->get_object(trajectory_);
        auto death_time = std::min(death_time_, trajectory.size() - 1);
        auto time_of_death = trajectory.get_time(death_time);
        auto time_of_birth = trajectory.get_time(birth_time_);
        return time_of_death - time_of_birth;
    }
    // Get id of parent at time step i
    body_index<1> get_parent_id(size_type i) const {
        size_type j = 0;
        while (parents_[j].first < i) {
            ++j;
        }
        return parents_[j].second;
    }
    const auto &get_parents() const { return parents_; }
    const Body<n, 1> &parent(size_type i) const {
        return reg_->get_object(get_parent_id(i));
    }
    Body<n, 1> &parent(size_type i) {
        return reg_->get_object(get_parent_id(i));
    }
    void set_parent(size_type ti, body_index<1> parent) {
        auto it = parents_.begin();
        while (it != parents_.end() && it->first < ti) {
            ++it;
        }
        parents_.insert(it, std::pair<size_type, body_index<1>>{ti, parent});
    }

    fc_index<0> get_force_center_id(size_type i) const {
        size_type j = 0;
        while (force_centers_[j].first < i) {
            ++j;
        }
        return force_centers_[j].second;
    }
    const auto &get_force_centers() const { return force_centers_; }
    const ForceCenter<n, 0> &force_center(size_type i) const {
        return reg_->get_object(get_force_center_id(i));
    }
    ForceCenter<n, 0> &force_center(size_type i) {
        return reg_->get_object(get_force_center_id(i));
    }
    void set_force_center(size_type ti, body_index<0> force_center) {
        auto it = force_centers_.begin();
        while (it != force_centers_.end() && it->first < ti) {
            ++it;
        }
        force_centers_.insert(
            it, std::pair<size_type, fc_index<0>>{ti, force_center});
    }

    // Get neighbors at timestep ti
    std::vector<body_index<0>> get_neighbors(size_type ti) const {
        auto neighbors = std::vector<body_index<0>>{};
        for (const auto &neighbor : neighbors_) {
            for (const auto &time_window : neighbor.second) {
                if (time_window.first <= ti && time_window.second >= ti) {
                    neighbors.push_back(neighbor.first);
                    break;
                }
            }
        }
        return neighbors;
    }
    // Get all neighbors including bond birth and death times
    const auto &get_neighbors() const { return neighbors_; }
    // Add a neighbor or extend the time window of the neighborhood
    void set_neighbor(body_index<0> id, size_type start_time = 0,
                      size_type end_time = s_max) {
        auto it = find_if(neighbors_.begin(), neighbors_.end(),
                          [id](const auto &neighbor) {
                              if (neighbor.first == id)
                                  return true;
                              else
                                  return false;
                          });
        if (it == neighbors_.end()) {
            neighbors_.push_back({id, {{start_time, end_time}}});
        } else {
            auto &neighbor = *it;
            auto &time_windows = neighbor.second;
            for (auto it = time_windows.begin(); it < time_windows.end();
                 ++it) {
                if (it->first <= start_time) {
                    time_windows.insert(it, {start_time, end_time});
                    break;
                }
            }
            std::sort(time_windows.begin(), time_windows.end());
            for (auto it = ++time_windows.rbegin(); it < time_windows.rend();
                 ++it) {
                if (it->second >= (it - 1)->first - 1) {
                    it->second = (it - 1)->second;
                    time_windows.erase(it.base());
                }
            }
        }
    }

  private:
    // Friends
    friend std::ostream &print_neighbors<n, 0>(std::ostream &,
                                               const Body<n, 0> &);
    // Private data members
    ObjectRegistry<n> *reg_;
    body_index<0> id_;
    bt_index<0> body_type_;
    trajectory_index<0> trajectory_;
    size_type birth_time_;
    size_type death_time_;
    std::vector<std::pair<size_type, body_index<1>>> parents_;
    std::vector<std::pair<size_type, fc_index<0>>> force_centers_;
    // Contains id of neighbor and a vector of {birth, death} timestep pairs
    std::vector<
        std::pair<body_index<0>, std::vector<std::pair<size_type, size_type>>>>
        neighbors_;
}; // namespace champion

/*********************** Member function definitions **************************/

template <level_type n, level_type level>
Body<n, level>::Body(ObjectRegistry<n> &reg, size_type ID,
                     bt_index<level> body_type,
                     Graph<body_index<level - 1>> constituent_graph,
                     size_type trajectory_id, size_type birth_time,
                     size_type death_time)
    : reg_{&reg}, id_{ID},
      body_type_(body_type), force_center_{s_max}, graph_{constituent_graph},
      trajectory_{reg.get_object(constituent_graph[0]).get_trajectory_id()},
      parents_{}, force_centers_{}, neighbors_{} {
    set_birth_time(birth_time);
    set_death_time(death_time);
    constituent_graph.sort();
}

template <level_type n, level_type level>
std::vector<ObjectIndex<Body, n, level - 1>>
Body<n, level>::get_constituents_impl() const {
    auto level_minus_1_vec = std::vector<body_index<level - 1>>(graph_.size());
    for (size_type i = 0; i != graph_.size(); ++i) {
        level_minus_1_vec[i] = reg_->get_object(graph_[i]).get_id();
    }
    return level_minus_1_vec;
}

template <level_type n, level_type level>
template <level_type ll>
std::enable_if_t<(ll == level - 1), std::vector<ObjectIndex<Body, n, ll>>>
Body<n, level>::get_constituent_ids() const {
    return get_constituents_impl();
}

template <level_type n, level_type level>
template <level_type ll>
std::enable_if_t<(ll < level - 1), std::vector<ObjectIndex<Body, n, ll>>>
Body<n, level>::get_constituent_ids() const {
    auto level_minus_1_vec = get_constituents_impl();
    auto ll_vec = std::vector<body_index<ll>>{};
    for (auto &body : level_minus_1_vec) {
        auto new_constituents =
            reg_->get_object(body).template get_constituent_ids<ll>();
        ll_vec.insert(ll_vec.end(), new_constituents.begin(),
                      new_constituents.end());
    }
    return ll_vec;
}

/*************************** Non-member functions *****************************/

// Compare Body:s based on their types
template <level_type n, level_type level>
bool operator<(const Body<n, level> &b1, const Body<n, level> &b2) {
    return b1.type() < b2.type();
}

template <level_type n, level_type level>
bool operator==(const Body<n, level> &b1, const Body<n, level> &b2) {
    return b1.type() == b2.type();
}

template <level_type n, level_type level>
std::ostream &print_brief(std::ostream &os, const Body<n, level> &body) {
    os << "Body<" << level << "> #" << body.get_id() << " (BodyType<" << level
       << "> #" << body.get_type_id() << std::flush;
    if (body.type().get_name() != "") {
        os << " " << body.type().get_name() << std::flush;
    }
    os << ")" << std::flush;
    return os;
}

template <level_type n, level_type level>
std::enable_if_t<(level > 0), std::ostream &>
print_constituents(std::ostream &os, const Body<n, level> &body) {
    os << "Constituents:\n";
    auto constituents = body.template get_constituents<level - 1>();
    for (size_type i = 0; i < constituents.size(); ++i) {
        os << "\t";
        print_brief(os, constituents[i]);
        os << "\n";
    }
    return os;
}

template <level_type n, level_type level>
std::enable_if_t<(level == 0), std::ostream &>
print_constituents(std::ostream &os, const Body<n, level> &body) {
    return os;
}

template <level_type n, level_type level>
std::enable_if_t<(level < n - 1), std::ostream &>
print_parents(std::ostream &os, const Body<n, level> &body) {
    const auto parents = body.get_parents();
    if (parents.size() > 0) {
        const auto &trajectory = body.trajectory();
        os << "Parents:\n";
        for (size_type i = 0; i < parents.size(); ++i) {
            auto ti = parents[i].first;
            auto t = trajectory.get_time(ti);
            auto parent = body.parent(ti);
            os << "From time " << ti << " (" << t << " ps): ";
            print_brief(os, parent) << '\n';
        }
    }
    return os;
}

template <level_type n, level_type level>
std::enable_if_t<(level >= n - 1), std::ostream &>
print_parents(std::ostream &os, const Body<n, level> &body) {
    return os;
}

template <level_type n, level_type level>
std::ostream &print_neighbors(std::ostream &os, const Body<n, level> &body) {
    const auto &neighbors = body.get_neighbors();
    auto no_neighbors = neighbors.size();
    if (no_neighbors > 0) {
        os << "Neighbors:\n";
        for (size_type i = 0; i < no_neighbors; ++i) {
            auto neighbor = body.reg_->get_object(neighbors[i].first);
            os << '\t';
            print_brief(os, neighbor);
            os << ": in time intervals ";
            const auto &time_intervals = neighbors[i].second;
            const auto &trajectory = body.trajectory();
            for (size_type j = 0; j < time_intervals.size() - 1; ++j) {
                os << trajectory.get_time(time_intervals[j].first) << "-"
                   << trajectory.get_time(time_intervals[j].second) << " ps, ";
            }
            os << trajectory.get_time(time_intervals.back().first) << "-"
               << trajectory.get_time(time_intervals.back().second) << " ps.\n";
        }
        os << "\n";
    }
    return os;
}

template <level_type n, level_type level>
std::ostream &print_full(std::ostream &os, const Body<n, level> &body) {
    print_brief(os, body) << '\n';
    print_constituents(os, body);
    print_parents(os, body);
    print_neighbors(os, body);
    os << std::flush;
    return os;
}

template <level_type n, level_type level>
std::ostream &print_recursive(std::ostream &os, const Body<n, level> &body) {
    print_brief(os, body);
    size_type i = 0;
    auto num_constituents = body.size();
    os << ": {Constituents: {";
    for (i = 0; i != num_constituents - 1; ++i) {
        os << "[" << i << "]: ";
        print_recursive(os, body[i]) << ", ";
    }
    i = num_constituents - 1;
    os << "[" << i << "]: ";
    print_recursive(os, body[i]) << "}, Bonds: {";
    auto edges = body.graph().get_edges();
    auto num_edges = edges.size();
    if (num_edges > 0) {
        for (i = 0; i < num_edges - 1; ++i) {
            os << edges[i] << ", ";
        }
        i = num_edges - 1;
        os << edges[i];
    }
    os << "}}";
    return os;
}

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os, const Body<n, level> &body) {
    print_full(os, body);
    return os;
}

template <level_type n>
std::ostream &print_recursive(std::ostream &os, const Body<n, 0> &body) {
    return print_brief(os, body);
}

template <level_type n>
std::ostream &operator<<(std::ostream &os, const Body<n, 0> &body) {
    print_full(os, body);
    return os;
}

} // namespace champion

#endif
