/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2018 <Main Author>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: DistributedGA.hpp

AUTHOR: Rasmus Andersson

DESCRIPTION:

A class to run multiple genetic algorithms in parallel, with the possibility of
synchronizing and adaptive decision making.

\******************************************************************************/

#ifndef CHAMPION_DISTRIBUTED_GA_HPP
#define CHAMPION_DISTRIBUTED_GA_HPP

// Standard library includes
#include <iostream>
#include <stdexcept>
#include <vector>

// External library includes
#include <omp.h>

// champion includes
#include "GA.hpp"
#include "NormalDistribution.hpp"
#include "io.hpp"
#include "set_precision.hpp"

namespace champion {

/**************************** Forward declarations ****************************/

/***************************** Class declarations *****************************/

template <typename T, typename CostFunc> class DistributedGA {
  public:
    // Public member functions
    // Construct from guess distributions and settings
    DistributedGA(size_type no_GAs) : no_GAs{no_GAs} { assign_GAs(); }

    DistributedGA(
        RandomNumberGenerator &rng, size_type no_GAs,
        std::vector<double> guess_means, std::vector<double> guess_stds,
        CostFunc training_cost_function, CostFunc test_cost_function,
        std::function<void(const std::vector<real> &,
                           const std::vector<real> &)>
            print_genome = nullptr,
        std::function<void(Individual<T> &)> preprocess = nullptr,
        std::function<void(GA<T, CostFunc> &)> callback = nullptr,
        size_type compare_interval = 100, size_type print_interval = 1,
        bool print_GAs = false, size_type max_iter = 1000, real cost_tol = 1e-6,
        real cost_abs_spread_tol = 1e-6, real cost_rel_spread_tol = 1e-6,
        real gene_abs_spread_tol = 1e-6, real gene_rel_spread_tol = 1e-6,
        size_type no_individuals = 0, size_type no_elites = s_max,
        real bias = 1, real bias_increment = 0.1,
        CrossOverMode cross_over_mode = CrossOverMode::arithmetic,
        unsigned no_cross_over_pts = 2,
        MutationMode mutation_mode = MutationMode::perturb,
        real mutation_rate = 0.005, real mutation_amplitude = 1);

    DistributedGA(RandomNumberGenerator &rng, const Dictionary &dict,
                  std::vector<real> guess_means, std::vector<real> guess_stds,
                  CostFunc training_cost_function, CostFunc test_cost_function,
                  std::function<void(const std::vector<real> &,
                                     const std::vector<real> &)>
                      print_genome = nullptr,
                  std::function<void(Individual<T> &)> preprocess = nullptr,
                  std::function<void(GA<T, CostFunc> &)> callback = nullptr);

    // Public API
    std::pair<std::vector<real>, std::vector<real>> optimize();

    // Public low-level API
    void advance();
    void run_GAs();
    void check_internal_convergence();
    void check_stopping_criteria();
    bool converged() const { return converged_; }
    bool all_converged() const;
    bool any_converged() const;
    void compute_best_individuals_convergence_rate();
    void compute_global_convergence_rate();
    void compute_local_convergence_rate();
    Individual<T> best_individual() const;
    void reset(const std::vector<real> &guess_means = {},
               const std::vector<real> &guess_stds = {});
    void reset_GA(size_type i, const std::vector<real> &guess_means = {},
                  const std::vector<real> &guess_stds = {});
    void reset_all_GAs(const std::vector<real> &guess_means = {},
                       const std::vector<real> &guess_stds = {});
    void adapt_bias();
    void increase_bias();
    void decrease_bias();
    void gather_best_individuals();
    void transplant_best_individuals();
    void print_best_individuals() const;
    void print() const;

    real min_cost() const { return min_cost_; };
    void compute_min_cost();
    void compute_best_individuals_cost_distribution();
    void compute_best_individuals_gene_distributions();
    void compute_global_cost_distribution();
    void compute_global_gene_distributions();
    void best_individuals_gene_distributions();

    // Public data members (settings)

    // Printing behavior
    size_type print_interval;
    bool print_GAs;
    // Genetic operator settings
    CrossOverMode cross_over_mode;
    unsigned no_cross_over_pts;
    MutationMode mutation_mode;
    // Hyperparameters
    size_type no_GAs;
    size_type compare_interval;
    size_type no_individuals;
    size_type no_elites;
    real bias;
    real bias_increment;
    real mutation_rate;
    real mutation_amplitude;
    // Tolerances
    size_type max_iter;
    real cost_tol;
    real cost_abs_spread_tol;
    real cost_rel_spread_tol;
    real gene_abs_spread_tol;
    real gene_rel_spread_tol;

  private:
    // Private member functions
    void assign_GAs();

    // Private data members

    // Random number generator
    RandomNumberGenerator *rng_;
    // GA instantiations
    std::vector<size_type> GA_layout_;
    std::vector<GA<T, CostFunc>> GAs_;
    // Population data
    size_type no_genes_;
    std::vector<Individual<T>> best_individuals_;
    // Optimization state
    bool converged_;
    std::vector<GAConvergenceStatus> internal_convergence_;
    size_type iteration_;
    real min_cost_;
    real best_individuals_cost_mean_;
    real best_individuals_cost_std_;
    real best_individuals_rel_cost_std_;
    real global_cost_mean_;
    real global_cost_std_;
    real global_rel_cost_std_;
    std::vector<real> best_individuals_gene_means_;
    std::vector<real> best_individuals_gene_stds_;
    real best_individuals_avg_gene_std_;
    std::vector<real> best_individuals_rel_gene_stds_;
    real best_individuals_avg_rel_gene_std_;
    std::vector<real> global_gene_means_;
    std::vector<real> global_gene_stds_;
    real global_avg_gene_std_;
    std::vector<real> global_rel_gene_stds_;
    real global_avg_rel_gene_std_;
    std::vector<real> old_best_individuals_gene_stds_;
    std::vector<real> old_global_gene_stds_;
    real starting_bias_;
    real old_bias_;
    real internal_gene_spread_;
    real mutual_gene_spread_;
    real best_individuals_convergence_rate_;
    real old_best_individuals_convergence_rate_;
    real global_convergence_rate_;
    real old_global_convergence_rate_;
    real local_convergence_rate_;
    real old_local_convergence_rate_;
    // Initial distribution of genotypes
    std::vector<real> guess_means_;
    std::vector<real> guess_stds_;
    // External functions
    CostFunc training_cost_function_;
    CostFunc test_cost_function_;
    // Takes either an individual genome, or mean + variance of a population
    std::function<void(const std::vector<real> &, const std::vector<real> &)>
        print_genome_;
    std::function<void(Individual<T> &)> preprocess_;
    std::function<void(GA<T, CostFunc> &)> callback_;
};

/************************ Member function definitions *************************/

// Constructors

template <typename T, typename CostFunc>
DistributedGA<T, CostFunc>::DistributedGA(
    RandomNumberGenerator &rng, size_type no_GAs, std::vector<real> guess_means,
    std::vector<real> guess_stds, CostFunc training_cost_function,
    CostFunc test_cost_function,
    std::function<void(const std::vector<real> &, const std::vector<real> &)>
        print_genome,
    std::function<void(Individual<T> &)> preprocess,
    std::function<void(GA<T, CostFunc> &)> callback, size_type compare_interval,
    size_type print_interval, bool print_GAs, size_type max_iter, real cost_tol,
    real cost_abs_spread_tol, real cost_rel_spread_tol,
    real gene_abs_spread_tol, real gene_rel_spread_tol,
    size_type no_individuals, size_type no_elites, real bias,
    real bias_increment, CrossOverMode cross_over_mode,
    unsigned no_cross_over_pts, MutationMode mutation_mode, real mutation_rate,
    real mutation_amplitude)
    : print_interval{print_interval}, print_GAs{print_GAs},
      cross_over_mode{cross_over_mode}, no_cross_over_pts{no_cross_over_pts},
      mutation_mode{mutation_mode}, no_GAs{no_GAs},
      compare_interval{compare_interval},
      no_individuals{no_individuals}, no_elites{no_elites}, bias{bias},
      bias_increment{bias_increment}, mutation_rate{mutation_rate},
      mutation_amplitude{mutation_amplitude}, max_iter{max_iter},
      cost_tol{cost_tol}, cost_abs_spread_tol{cost_abs_spread_tol},
      cost_rel_spread_tol{cost_rel_spread_tol},
      gene_abs_spread_tol{gene_abs_spread_tol},
      gene_rel_spread_tol{gene_rel_spread_tol}, rng_{&rng},
      GA_layout_{}, GAs_{}, no_genes_{guess_means.size()},
      best_individuals_(no_GAs), converged_{false},
      internal_convergence_(no_GAs, GAConvergenceStatus::not_converged),
      iteration_{0}, min_cost_{std::numeric_limits<real>::infinity()},
      best_individuals_cost_mean_{std::numeric_limits<real>::infinity()},
      best_individuals_cost_std_{std::numeric_limits<real>::infinity()},
      best_individuals_rel_cost_std_{std::numeric_limits<real>::infinity()},
      global_cost_mean_{std::numeric_limits<real>::infinity()},
      global_cost_std_{std::numeric_limits<real>::infinity()},
      global_rel_cost_std_{std::numeric_limits<real>::infinity()},
      best_individuals_gene_means_(no_genes_, 0),
      best_individuals_gene_stds_(no_genes_, 0),
      best_individuals_avg_gene_std_{std::numeric_limits<real>::infinity()},
      best_individuals_rel_gene_stds_(no_genes_, 0),
      best_individuals_avg_rel_gene_std_{std::numeric_limits<real>::infinity()},
      global_gene_means_(no_genes_, 0), global_gene_stds_(no_genes_, 0),
      global_avg_gene_std_{std::numeric_limits<real>::infinity()},
      global_rel_gene_stds_(no_genes_, 0),
      global_avg_rel_gene_std_{std::numeric_limits<real>::infinity()},
      old_best_individuals_gene_stds_(no_genes_, 0),
      old_global_gene_stds_(no_genes_, 0),
      starting_bias_{bias}, old_bias_{bias}, internal_gene_spread_{1},
      mutual_gene_spread_{1}, best_individuals_convergence_rate_{0},
      old_best_individuals_convergence_rate_{0}, global_convergence_rate_{0},
      old_global_convergence_rate_{0}, local_convergence_rate_{0},
      old_local_convergence_rate_{0}, guess_means_{guess_means},
      guess_stds_{guess_stds}, training_cost_function_{training_cost_function},
      test_cost_function_{test_cost_function}, print_genome_{print_genome},
      preprocess_{preprocess}, callback_{callback} {
    if (no_individuals == 0) {
        no_individuals = no_genes_ * 2;
    }
    if (no_elites == s_max) {
        no_elites = std::max<size_type>(1, no_individuals / 20);
    }
    if (!print_genome_) {
        print_genome_ = print_genome_default;
    }
    assign_GAs();
    for (size_type i = 0; i < GA_layout_.size(); ++i) {
        if (GA_layout_[i] == MPI_rank) {
            GAs_.push_back(GA<T, CostFunc>{*rng_,
                                           guess_means_,
                                           guess_stds_,
                                           training_cost_function_,
                                           test_cost_function_,
                                           print_genome_,
                                           preprocess_,
                                           callback_,
                                           0,
                                           compare_interval,
                                           cost_tol,
                                           cost_abs_spread_tol,
                                           cost_rel_spread_tol,
                                           gene_abs_spread_tol,
                                           gene_rel_spread_tol,
                                           no_individuals,
                                           no_elites,
                                           bias,
                                           cross_over_mode,
                                           no_cross_over_pts,
                                           mutation_mode,
                                           mutation_rate,
                                           mutation_amplitude});
        }
    }
    compute_best_individuals_cost_distribution();
    compute_best_individuals_gene_distributions();
    compute_global_cost_distribution();
    compute_global_gene_distributions();
    compute_global_convergence_rate();
    compute_local_convergence_rate();
    compute_min_cost();
    if (MPI_rank == MPI_root && print_interval > 0) {
        std::cout << "\nPrinting Distributed GA info:" << std::endl;
        std::cout << "Starting DistributedGA with " << no_GAs
                  << " GA instantiations." << std::endl;
        std::cout << "Cost bias: " << bias << "." << std::endl;
        std::cout << "Bias increment: " << bias_increment << std::endl;
        std::cout << "Maximum number of iterations: " << max_iter << "."
                  << std::endl;
        std::cout << "Absolute cost tolerance: " << cost_tol << "."
                  << std::endl;
        std::cout << "Cost standard deviation tolerance: "
                  << cost_abs_spread_tol << "." << std::endl;
        std::cout << "Cost relative deviation tolerance: "
                  << cost_rel_spread_tol << "." << std::endl;
        std::cout << "Genetic standard deviation tolerance: "
                  << gene_abs_spread_tol << "." << std::endl;
        std::cout << "Genetic relative deviation tolerance: "
                  << gene_rel_spread_tol << "." << std::endl;
        std::cout << "Comparing GAs every " << compare_interval
                  << " GA iterations." << std::endl;

        switch (print_interval) {
        case 0:
            std::cout << "Printing DistributedGA status disabled." << std::endl;
            break;
        default:
            std::cout << "Printing status every " << print_interval
                      << " iterations.\n"
                      << std::endl;
        }
        GAs_[0].print_info();
    }
}

template <typename T, typename CostFunc>
DistributedGA<T, CostFunc>::DistributedGA(
    RandomNumberGenerator &rng, const Dictionary &dict,
    std::vector<real> guess_means, std::vector<real> guess_stds,
    CostFunc training_cost_function, CostFunc test_cost_function,
    std::function<void(const std::vector<real> &, const std::vector<real> &)>
        print_genome,
    std::function<void(Individual<T> &)> preprocess,
    std::function<void(GA<T, CostFunc> &)> callback)
    : DistributedGA(
          rng, read_value_or_default<size_type>("number_of_GAs", dict, 4),
          guess_means, guess_stds, training_cost_function, test_cost_function,
          print_genome, preprocess, callback,
          read_value_or_default<size_type>("compare_interval", dict, 1),
          read_value_or_default<size_type>("print_interval", dict, 1),
          read_bool("print_GAs", dict, false),
          read_value_or_default<size_type>("max_iter", dict, 1e4),
          read_value_or_default<real>("cost_tol", dict, 1e-6),
          read_value_or_default<real>("cost_abs_spread_tol", dict, 1e-6),
          read_value_or_default<real>("cost_rel_spread_tol", dict, 1e-6),
          read_value_or_default<real>("gene_abs_spread_tol", dict, 1e-6),
          read_value_or_default<real>("gene_rel_spread_tol", dict, 1e-6),
          read_value_or_default<size_type>("no_individuals", dict,
                                           guess_means.size() * 2),
          read_value_or_default<size_type>("no_elites", dict, 0),
          read_value_or_default<real>("bias", dict, 1),
          read_value_or_default<real>("bias_increment", dict, 0.5),
          GA<T, CostFunc>::read_cross_over_mode(dict),
          GA<T, CostFunc>::read_no_cross_over_pts(dict),
          GA<T, CostFunc>::read_mutation_mode(dict),
          read_value_or_default<real>("mutation_rate", dict, 3e-3),
          read_value_or_default<real>("mutation_amplitude", dict, 1.0)) {}

// Public API

template <typename T, typename CostFunc>
std::pair<std::vector<real>, std::vector<real>>
DistributedGA<T, CostFunc>::optimize() {
    bool print_flag = print_interval > 0 && MPI_rank == MPI_root;
    for (; iteration_ < max_iter; ++iteration_) {
        advance();
        run_GAs();
        compute_best_individuals_cost_distribution();
        compute_best_individuals_gene_distributions();
        compute_global_cost_distribution();
        compute_global_gene_distributions();
        compute_best_individuals_convergence_rate();
        compute_global_convergence_rate();
        compute_local_convergence_rate();
        compute_min_cost();
        check_internal_convergence();
        check_stopping_criteria();
        gather_best_individuals();
        if (print_flag && iteration_ % print_interval == 0) {
            print();
        }
        if (converged_) {
            if (print_flag) {
                std::cout
                    << "Reached mutual convergence of all GAs. Stopping.\n"
                    << std::endl;
                std::cout << "Final state: " << std::endl;
                print();
                std::cout << "Best individual:" << std::endl;
                print_genome_(best_individual().genome(), {});
            }
            break;
        } else if (all_converged()) {
            if (print_flag) {
                std::cout << "GAs converged internally but not mutually. "
                          << std::endl;
            }
            break;
        } else {
            adapt_bias();
        }
    }
    if (print_flag) {
        if (iteration_ >= max_iter) {
            std::cout
                << "Convergence not reached within maximum allowed iterations. "
                << std::endl;
        }
    }
    return {best_individuals_gene_means_, best_individuals_gene_stds_};
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::advance() {
    old_best_individuals_gene_stds_ = std::move(best_individuals_gene_stds_);
    best_individuals_gene_stds_ = std::vector<real>(no_genes_, -1);
    old_best_individuals_convergence_rate_ = best_individuals_convergence_rate_;
    old_global_gene_stds_ = std::move(global_gene_stds_);
    global_gene_stds_ = std::vector<real>(no_genes_, -1);
    old_global_convergence_rate_ = global_convergence_rate_;
    for (size_type i = 0; i < GAs_.size(); ++i) {
        GAs_[i].reset_iterations();
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::run_GAs() {
    for (size_type i = 0; i < GAs_.size(); ++i) {
        GAs_[i].optimize();
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::check_internal_convergence() {
    // Create vector of ints to communicate across MPI
    std::vector<int> internally_converged(no_GAs, -1);
    size_type j = 0;
    for (size_type i = 0; i < no_GAs; ++i) {
        if (GA_layout_[i] == MPI_rank) {
            internally_converged[i] =
                static_cast<int>(GAs_[j++].convergence_status());
        }
#ifdef MPI_VERSION
        MPI_Bcast(&internally_converged[i], 1, MPI_INT, GA_layout_[i],
                  MPI_COMM_WORLD);
#endif
    }
    for (size_type i = 0; i < no_GAs; ++i) {
        internal_convergence_[i] =
            static_cast<GAConvergenceStatus>(internally_converged[i]);
    }
}

template <typename T, typename CostFunc>
bool DistributedGA<T, CostFunc>::all_converged() const {
    bool all_converged = true;
    for (size_type i = 0; i < no_GAs; ++i) {
        all_converged = all_converged && (internal_convergence_[i] !=
                                          GAConvergenceStatus::not_converged);
    }
    return all_converged;
}

template <typename T, typename CostFunc>
bool DistributedGA<T, CostFunc>::any_converged() const {
    for (size_type i = 0; i < no_GAs; ++i) {
        if (internal_convergence_[i] != GAConvergenceStatus::not_converged) {
            return true;
        }
    }
    return false;
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::compute_best_individuals_convergence_rate() {
    real sum = 0;
#pragma omp parallel for reduction(+ : sum)
    for (size_type i = 0; i < no_genes_; ++i) {
        if (old_best_individuals_gene_stds_[i] != 0) {
            sum += -(best_individuals_gene_stds_[i] -
                     old_best_individuals_gene_stds_[i]) /
                   old_best_individuals_gene_stds_[i];
        }
    }
    best_individuals_convergence_rate_ = sum / no_genes_;
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::compute_global_convergence_rate() {
    real sum = 0;
#pragma omp parallel for reduction(+ : sum)
    for (size_type i = 0; i < no_genes_; ++i) {
        if (old_global_gene_stds_[i] != 0) {
            sum += -(global_gene_stds_[i] - old_global_gene_stds_[i]) /
                   old_global_gene_stds_[i];
        }
    }
    global_convergence_rate_ = sum / no_genes_;
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::compute_local_convergence_rate() {
    double sum_local = 0;
#pragma omp parallel for reduction(+ : sum_local)
    for (size_type i = 0; i < GAs_.size(); ++i) {
        sum_local += static_cast<double>(GAs_[i].convergence_rate());
    }
#ifdef MPI_VERSION
    double sum_global = 0;
    MPI_Allreduce(&sum_local, &sum_global, 1, MPI_DOUBLE, MPI_SUM,
                  MPI_COMM_WORLD);
    local_convergence_rate_ = sum_global / no_GAs;
#else
    local_convergence_rate_ = sum_local / no_GAs;
#endif
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::check_stopping_criteria() {
    if (min_cost_ < cost_tol) {
        converged_ = true;
        return;
    }
    if (best_individuals_cost_std_ < cost_abs_spread_tol ||
        global_cost_std_ < cost_abs_spread_tol) {
        converged_ = true;
        return;
    }
    if (best_individuals_rel_cost_std_ < cost_rel_spread_tol ||
        global_rel_cost_std_ < cost_rel_spread_tol) {
        converged_ = true;
        return;
    }
    if (best_individuals_avg_gene_std_ < gene_abs_spread_tol ||
        global_avg_gene_std_ < gene_abs_spread_tol) {
        converged_ = true;
        return;
    }
    if (best_individuals_avg_rel_gene_std_ < gene_rel_spread_tol ||
        global_avg_rel_gene_std_ < gene_rel_spread_tol) {
        converged_ = true;
        return;
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::adapt_bias() {
    if (global_convergence_rate_ /*+ local_convergence_rate_*/ >
        old_global_convergence_rate_ /*+ old_local_convergence_rate_*/) {
        if (bias >= old_bias_) {
            increase_bias();
        } else if (bias < old_bias_) {
            decrease_bias();
        }
    } else if (global_convergence_rate_ /*+ local_convergence_rate_*/ <
               old_global_convergence_rate_ /*+ old_local_convergence_rate_*/) {
        if (old_bias_ >= bias) {
            increase_bias();
        } else if (bias > old_bias_) {
            decrease_bias();
        }
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::increase_bias() {
    old_bias_ = bias;
    bias *= (1 + bias_increment);
    for (auto &GA : GAs_) {
        GA.bias = bias;
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::decrease_bias() {
    old_bias_ = bias;
    bias /= (1 + bias_increment);
    for (auto &GA : GAs_) {
        GA.bias = bias;
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::print() const {
    if (MPI_rank == MPI_root) {
        std::cout << "Iteration " << iteration_ << std::endl;
        std::cout << "Bias: " << std::defaultfloat << bias << std::endl;
        std::cout << "Minimum cost: " << min_cost_
                  << " (tolerance: " << cost_tol << ")" << std::endl;
        std::cout << "Cost standard deviation among best individuals: "
                  << best_individuals_cost_std_
                  << " (tolerance: " << cost_abs_spread_tol << ")" << std::endl;
        std::cout << "Cost standard deviation in global population: "
                  << global_cost_std_ << " (tolerance: " << cost_abs_spread_tol
                  << ")" << std::endl;
        std::cout << "Relative cost standard deviation among best individuals: "
                  << best_individuals_rel_cost_std_
                  << " (tolerance: " << cost_rel_spread_tol << ")" << std::endl;
        std::cout << "Relative cost standard deviation in global population: "
                  << global_rel_cost_std_
                  << " (tolerance: " << cost_rel_spread_tol << ")" << std::endl;
        std::cout << "Average gene standard deviation among best individuals: "
                  << best_individuals_avg_gene_std_
                  << " (tolerance: " << gene_abs_spread_tol << ")" << std::endl;
        std::cout << "Average gene standard deviation in global population: "
                  << global_avg_gene_std_
                  << " (tolerance: " << gene_abs_spread_tol << ")" << std::endl;
        std::cout << "Average relative gene standard deviation among best "
                     "individuals: "
                  << best_individuals_avg_rel_gene_std_
                  << " (tolerance: " << gene_rel_spread_tol << ")" << std::endl;
        std::cout << "Average relative gene standard deviation in global "
                     "population: "
                  << global_avg_rel_gene_std_
                  << " (tolerance: " << gene_rel_spread_tol << ")" << std::endl;
        std::cout << "Global convergence rate: " << global_convergence_rate_
                  << ". Best individuals convergence rate: "
                  << best_individuals_convergence_rate_
                  << ". Local convergence rate: " << local_convergence_rate_
                  << std::endl;
        std::cout << "Convergence: " << std::endl;
        for (size_type i = 0; i < no_GAs; ++i) {
            std::cout << "GA " << i << " – ";
            if (internal_convergence_[i] ==
                GAConvergenceStatus::not_converged) {
                std::cout << "not converged." << std::endl;
            } else {
                std::cout << "Converged. Reason: ";
                if (internal_convergence_[i] == GAConvergenceStatus::abs_cost) {
                    std::cout << "Absolute cost." << std::endl;
                } else if (internal_convergence_[i] ==
                           GAConvergenceStatus::abs_gene_spread) {
                    std::cout << "Absolute genetic diversity." << std::endl;
                } else if (internal_convergence_[i] ==
                           GAConvergenceStatus::rel_gene_spread) {
                    std::cout << "Relative genetic diversity." << std::endl;
                } else if (internal_convergence_[i] ==
                           GAConvergenceStatus::abs_cost_spread) {
                    std::cout << "Absolute cost spread." << std::endl;
                } else if (internal_convergence_[i] ==
                           GAConvergenceStatus::rel_cost_spread) {
                    std::cout << "Relative cost spread." << std::endl;
                }
            }
        }
        if (print_GAs) {
            for (size_type i = 0; i < MPI_size; ++i) {
                if (MPI_rank == i) {
                    for (size_type j = 0; j < GAs_.size(); ++j) {
                        std::cout << "GA " << i << "." << j
                                  << " (bias = " << GAs_[j].bias
                                  << "): " << std::endl;
                        GAs_[j].print();
                    }
                }
#ifdef MPI_VERSION
                MPI_Barrier(MPI_COMM_WORLD);
#endif
            }
        }
        std::cout << "Statistical distributions: (95% C.I.)" << std::endl;
        std::cout << "Cost distribution of best individuals: "
                  << best_individuals_cost_mean_ << " ± "
                  << best_individuals_cost_std_ << std::endl;
        // std::cout << "Cost distribution of combined populations: "
        //           << global_cost_mean_ << " ± "
        //           << global_cost_std_ << std::endl;
        // std::cout << "Genetic distribution of best individuals: " <<
        // std::endl; print_genome_(best_individuals_gene_means_,
        //               best_individuals_gene_stds_);
        std::cout << "Genetic distribution of combined population: "
                  << std::endl;
        print_genome_(global_gene_means_, global_gene_stds_);
        // std::cout << "Best individual:" << std::endl;
        // print_genome_(best_individual().genome(), {});
        print_best_individuals();
        std::cout << std::endl;
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::gather_best_individuals() {
    if (MPI_size == 1) {
        for (size_type i = 0; i < no_GAs; ++i) {
            best_individuals_[i] = GAs_[i].best_individual();
        }
    } else {
        size_type j = 0;
        for (size_type i = 0; i < no_GAs; ++i) {
            auto best_individual_doubles = std::vector<double>(no_genes_);
            auto best_individual = Individual<T>{};
            if (GA_layout_[i] == MPI_rank) {
                best_individual = GAs_[j++].best_individual();
                for (size_type k = 0; k < no_genes_; ++k) {
                    best_individual_doubles[k] = best_individual[k];
                }
            }
#ifdef MPI_VERSION
            MPI_Bcast(&best_individual_doubles[0], no_genes_, MPI_DOUBLE,
                      GA_layout_[i], MPI_COMM_WORLD);
#endif
            for (size_type k = 0; k < no_genes_; ++k) {
                best_individuals_[i][k] = best_individual_doubles[k];
            }
        }
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::transplant_best_individuals() {
    for (size_type i = 0; i < GAs_.size(); ++i) {
        for (size_type j = 0; j < no_GAs; ++j) {
            size_type k = no_individuals - 1 - j;
            GAs_[i].population_[k] = best_individuals_[j];
            GAs_[i].evaluate_training_cost(k);
            GAs_[i].evaluate_test_cost(k);
        }
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::print_best_individuals() const {
    for (size_type i = 0; i < no_genes_; ++i) {
        std::cout << i << ": {";
        for (size_type j = 0; j < no_GAs; ++j) {
            std::cout << best_individuals_[j][i] << " ";
        }
        std::cout << "}" << std::endl;
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::compute_min_cost() {
    double min_cost_local = std::numeric_limits<double>::infinity();
#pragma omp parallel for reduction(min : min_cost_local)
    for (size_type i = 0; i < GAs_.size(); ++i) {
        min_cost_local = min(min_cost_local, GAs_[i].min_cost());
    }
#ifdef MPI_VERSION
    double min_cost_global = std::numeric_limits<double>::infinity();
    MPI_Allreduce(&min_cost_local, &min_cost_global, 1, MPI_DOUBLE, MPI_MIN,
                  MPI_COMM_WORLD);
    min_cost_ = min_cost_global;
#else
    min_cost_ = min_cost_local;
#endif
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::compute_best_individuals_cost_distribution() {
    {
        double sum_local = 0;
#pragma omp parallel for reduction(+ : sum_local)
        for (size_type i = 0; i < GAs_.size(); ++i) {
            sum_local += GAs_[i].test_cost(GAs_[i].best_individual_index());
        }
#ifdef MPI_VERSION
        double sum_global = 0;
        MPI_Allreduce(&sum_local, &sum_global, 1, MPI_DOUBLE, MPI_SUM,
                      MPI_COMM_WORLD);
        best_individuals_cost_mean_ = sum_global / static_cast<double>(no_GAs);
#else
        best_individuals_cost_mean_ = sum_local / static_cast<double>(no_GAs);
#endif
    }
    {
        double sum_local = 0;
#pragma omp parallel for reduction(+ : sum_local)
        for (size_type i = 0; i < GAs_.size(); ++i) {
            sum_local +=
                std::pow(GAs_[i].test_cost(GAs_[i].best_individual_index()) -
                             best_individuals_cost_mean_,
                         2);
        }
#ifdef MPI_VERSION
        double sum_global = 0;
        MPI_Allreduce(&sum_local, &sum_global, 1, MPI_DOUBLE, MPI_SUM,
                      MPI_COMM_WORLD);
        best_individuals_cost_std_ =
            std::sqrt(sum_global / static_cast<double>(no_GAs));
#else
        best_individuals_cost_std_ =
            std::sqrt(sum_local / static_cast<double>(no_GAs));
#endif
    }
    if (best_individuals_cost_mean_ == 0) {
        best_individuals_rel_cost_std_ = 0;
    } else {
        best_individuals_rel_cost_std_ =
            best_individuals_cost_std_ / best_individuals_cost_mean_;
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::compute_best_individuals_gene_distributions() {
    {
        for (size_type i = 0; i < no_genes_; ++i) {
            double sum_local = 0;
#pragma omp parallel for reduction(+ : sum_local)
            for (size_type j = 0; j < GAs_.size(); ++j) {
                sum_local += GAs_[j].best_individual()[i];
            }
#ifdef MPI_VERSION
            double sum_global = 0;
            MPI_Allreduce(&sum_local, &sum_global, 1, MPI_DOUBLE, MPI_SUM,
                          MPI_COMM_WORLD);
            best_individuals_gene_means_[i] =
                sum_global / static_cast<double>(no_GAs);
#else
            best_individuals_gene_means_[i] =
                sum_local / static_cast<double>(no_GAs);
#endif
        }
    }
    {
        for (size_type i = 0; i < no_genes_; ++i) {
            double sum_local = 0;
#pragma omp parallel for reduction(+ : sum_local)
            for (size_type j = 0; j < GAs_.size(); ++j) {
                sum_local += std::pow(GAs_[j].best_individual()[i] -
                                          best_individuals_gene_means_[i],
                                      2);
            }
#ifdef MPI_VERSION
            double sum_global = 0;
            MPI_Allreduce(&sum_local, &sum_global, 1, MPI_DOUBLE, MPI_SUM,
                          MPI_COMM_WORLD);
            best_individuals_gene_stds_[i] =
                std::sqrt(sum_global / static_cast<double>(no_GAs));
#else
            best_individuals_gene_stds_[i] =
                std::sqrt(sum_local / static_cast<double>(no_GAs));
#endif
            if (best_individuals_gene_means_[i] == 0) {
                best_individuals_rel_gene_stds_[i] = 0;
            } else {
                best_individuals_rel_gene_stds_[i] =
                    best_individuals_gene_stds_[i] /
                    std::fabs(best_individuals_gene_means_[i]);
            }
        }
    }
    best_individuals_avg_gene_std_ = mean(best_individuals_gene_stds_);
    best_individuals_avg_rel_gene_std_ = mean(best_individuals_rel_gene_stds_);
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::compute_global_cost_distribution() {
    {
        double sum_local = 0;
#pragma omp parallel for reduction(+ : sum_local)
        for (size_type i = 0; i < GAs_.size(); ++i) {
            sum_local += GAs_[i].cost_mean();
        }
#ifdef MPI_VERSION
        double sum_global = 0;
        MPI_Allreduce(&sum_local, &sum_global, 1, MPI_DOUBLE, MPI_SUM,
                      MPI_COMM_WORLD);
        global_cost_mean_ = sum_global / static_cast<double>(no_GAs);
#else
        global_cost_mean_ = sum_local / static_cast<double>(no_GAs);
#endif
    }
    {
        double sum_local = 0;
        double no_terms = 0;
#pragma omp parallel for reduction(+ : sum_local) collapse(2)
        for (size_type i = 0; i < GAs_.size(); ++i) {
            for (size_type j = 0; j < no_individuals; ++j) {
                if (!GAs_[i].is_mutant(j)) {
                    sum_local +=
                        std::pow(GAs_[i].test_cost(j) - global_cost_mean_, 2);
                    ++no_terms;
                }
            }
        }
#ifdef MPI_VERSION
        double sum_global = 0;
        double no_terms_global = 0;
        MPI_Allreduce(&sum_local, &sum_global, 1, MPI_DOUBLE, MPI_SUM,
                      MPI_COMM_WORLD);
        MPI_Allreduce(&no_terms, &no_terms_global, 1, MPI_DOUBLE, MPI_SUM,
                      MPI_COMM_WORLD);
        global_cost_std_ = std::sqrt(sum_global / no_terms_global);
#else
        global_cost_std_ = std::sqrt(sum_local / no_terms);
#endif
    }
    if (global_cost_mean_ == 0) {
        global_rel_cost_std_ = 0;
    } else {
        global_rel_cost_std_ = global_cost_std_ / std::fabs(global_cost_mean_);
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::compute_global_gene_distributions() {
    {
        for (size_type i = 0; i < no_genes_; ++i) {
            double sum_local = 0;
#pragma omp parallel for reduction(+ : sum_local)
            for (size_type j = 0; j < GAs_.size(); ++j) {
                sum_local += GAs_[j].gene_means()[i];
            }
#ifdef MPI_VERSION
            double sum_global = 0;
            MPI_Allreduce(&sum_local, &sum_global, 1, MPI_DOUBLE, MPI_SUM,
                          MPI_COMM_WORLD);
            global_gene_means_[i] = sum_global / static_cast<double>(no_GAs);
#else
            global_gene_means_[i] = sum_local / static_cast<double>(no_GAs);
#endif
        }
    }
    {
        for (size_type i = 0; i < no_genes_; ++i) {
            double sum_local = 0;
            double no_terms = 0;
#pragma omp parallel for collapse(2) reduction(+ : sum_local, no_terms)
            for (size_type j = 0; j < GAs_.size(); ++j) {
                for (size_type k = 0; k < no_individuals; ++k) {
                    if (!GAs_[j].is_mutant(k)) {
                        sum_local +=
                            std::pow(GAs_[j][k][i] - global_gene_means_[i], 2);
                        ++no_terms;
                    }
                }
            }
#ifdef MPI_VERSION
            double sum_global = 0;
            double no_terms_global = 0;
            MPI_Allreduce(&sum_local, &sum_global, 1, MPI_DOUBLE, MPI_SUM,
                          MPI_COMM_WORLD);
            MPI_Allreduce(&no_terms, &no_terms_global, 1, MPI_DOUBLE, MPI_SUM,
                          MPI_COMM_WORLD);
            global_gene_stds_[i] = std::sqrt(sum_global / no_terms_global);
#else
            global_gene_stds_[i] = std::sqrt(sum_local / no_terms);
#endif
            if (global_gene_means_[i] == 0) {
                global_rel_gene_stds_[i] = 0;
            } else {
                global_rel_gene_stds_[i] =
                    global_gene_stds_[i] / global_gene_means_[i];
            }
        }
    }
    global_avg_gene_std_ = mean(global_gene_stds_);
    global_avg_rel_gene_std_ = mean(global_rel_gene_stds_);
}

template <typename T, typename CostFunc>
Individual<T> DistributedGA<T, CostFunc>::best_individual() const {
    // Find the best individual on each MPI rank
    size_type best_GA_index = no_GAs;
    size_type best_individual_index = no_individuals;
    double min_cost = std::numeric_limits<double>::infinity();
    for (size_type i = 0; i < GAs_.size(); ++i) {
        auto individual_index = GAs_[i].best_individual_index();
        auto cost = GAs_[i].test_cost(individual_index);
        if (cost < min_cost) {
            min_cost = cost;
            best_GA_index = i;
            best_individual_index = individual_index;
        }
    }
// Find the best individual across MPI ranks
#ifdef MPI_VERSION
    Individual<T> best_individual(no_genes_);
    std::vector<double> rank_min_costs(MPI_size,
                                       std::numeric_limits<double>::infinity());

    MPI_Allgather(&min_cost, 1, MPI_DOUBLE, &rank_min_costs[0], 1, MPI_DOUBLE,
                  MPI_COMM_WORLD);
    auto it = std::min_element(rank_min_costs.begin(), rank_min_costs.end());
    int best_rank_index = it - rank_min_costs.begin();
    if (MPI_rank == best_rank_index) {
        best_individual = GAs_[best_GA_index][best_individual_index];
    }
    MPI_Bcast(&best_individual[0], no_genes_, MPI_DOUBLE, best_rank_index,
              MPI_COMM_WORLD);
    return best_individual;
#else
    return GAs_[best_GA_index][best_individual_index];
#endif
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::reset(const std::vector<real> &guess_means,
                                       const std::vector<real> &guess_stds) {
    iteration_ = 0;
    guess_means_ = guess_means;
    guess_stds_ = guess_stds;
    // bias = starting_bias_;
    auto inf = std::numeric_limits<double>::infinity();
    min_cost_ = inf;
    best_individuals_cost_mean_ = inf;
    best_individuals_cost_std_ = inf;
    best_individuals_rel_cost_std_ = inf;
    global_cost_mean_ = inf;
    global_cost_std_ = inf;
    global_rel_cost_std_ = inf;
    reset_all_GAs(guess_means, guess_stds);
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::reset_GA(size_type i,
                                          const std::vector<real> &guess_means,
                                          const std::vector<real> &guess_stds) {
    if (GA_layout_[i] == MPI_rank) {
        size_type index =
            std::count(GA_layout_.begin(), GA_layout_.begin() + i, MPI_rank);
        GAs_[index].reset_GA(guess_means, guess_stds);
        GAs_[index].bias = bias;
    }
}

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::reset_all_GAs(
    const std::vector<real> &guess_means, const std::vector<real> &guess_stds) {
    for (size_type i = 0; i < GAs_.size(); ++i) {
        GAs_[i].reset(guess_means, guess_stds);
    }
    for (auto &GA : GAs_) {
        GA.bias = bias;
    }
}

// Private member functions

template <typename T, typename CostFunc>
void DistributedGA<T, CostFunc>::assign_GAs() {
#ifdef MPI_VERSION
    GA_layout_ = std::vector<size_type>(no_GAs, 0);
    std::vector<unsigned> no_threads_by_rank(MPI_size, 0);
    unsigned no_threads_on_rank = omp_get_max_threads();
    MPI_Gather(&no_threads_on_rank, 1, MPI_UNSIGNED, &no_threads_by_rank[0], 1,
               MPI_UNSIGNED, MPI_root, MPI_COMM_WORLD);
    if (MPI_rank == MPI_root) {
        std::vector<double> cum_rank_frac(MPI_size, 0);
        for (int i = 0; i < MPI_size; ++i) {
            cum_rank_frac[i] = (i + 1) / static_cast<double>(MPI_size);
        }
        std::vector<double> cum_GA_frac(no_GAs, 0);
        for (size_type i = 0; i < no_GAs; ++i) {
            cum_GA_frac[i] = (i + 1) / static_cast<double>(no_GAs);
        }
        size_type rank_no = 0;
        for (size_type GA_no = 0; GA_no < no_GAs; ++GA_no) {
            while (cum_GA_frac[GA_no] > cum_rank_frac[rank_no]) {
                ++rank_no;
            }
            GA_layout_[GA_no] = rank_no;
        }
    }
    MPI_Bcast(&GA_layout_[0], no_GAs, MPI_LONG_LONG, MPI_root, MPI_COMM_WORLD);
#else
    GA_layout_ = std::vector<size_type>(no_GAs, 0);
#endif
}

} // namespace champion
#endif