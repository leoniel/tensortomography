/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2019 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: DataContainer.hpp

AUTHOR: Rasmus Andersson

DESCRIPTION:

A read-only store of data available across MPI ranks. Takes arbitrary container
of values convertible to double and stores in a std::vector<double> duplicated
on all ranks.

\******************************************************************************/

#ifndef CHAMPION_DATA_CONTAINER_HPP
#define CHAMPION_DATA_CONTAINER_HPP

// Standard library includes
#include <iostream>
#include <vector>

// champion includes
#include "set_precision.hpp"
#include "stl_io.hpp"

namespace champion {

/**************************** Forward declarations ****************************/

/***************************** Class declarations *****************************/

class DataContainer {
  public:
    // Typedefs

    // Public member functions

    // Constructor
    template <typename Container>
    explicit DataContainer(const Container &container,
                           int source_rank = MPI_size)
        : container_{} {
#ifdef MPI_VERSION
        unsigned long long container_size = 0;
        if (source_rank < MPI_size) {
            if (MPI_rank == source_rank) {
                container_size = container.size();
                for (auto el : container) {
                    container_.push_back(el);
                }
            }
            MPI_Bcast(&container_size, 1, MPI_LONG_LONG, source_rank,
                      MPI_COMM_WORLD);
            MPI_Barrier(MPI_COMM_WORLD);
            if (MPI_rank != source_rank) {
                container_.resize(container_size);
            }
            MPI_Bcast(&container_[0], container_size, MPI_DOUBLE, source_rank,
                      MPI_COMM_WORLD);
        } else {
            for (auto el : container) {
                container_.push_back(el);
            }
            auto sizes = std::vector<int>(MPI_size, 0);

            int sz = container_.size();
            MPI_Gather(&sz, 1, MPI_INT, &sizes[0], 1, MPI_INT, MPI_root,
                       MPI_COMM_WORLD);
            if (MPI_rank == MPI_root) {
                for (auto szi : sizes) {
                    if (szi != sz) {
                        throw std::runtime_error{
                            "CHAMPION::DataContainer(): Non-matching sizes of "
                            "containers on different ranks. If constructor is "
                            "called without specifying source rank, the "
                            "containers must be equal across all ranks."};
                    }
                }
            }
            auto values = std::vector<double>(MPI_size * sz, 0);
            MPI_Gather(&container_[0], sz, MPI_DOUBLE, &values[0], sz,
                       MPI_DOUBLE, MPI_root, MPI_COMM_WORLD);
            for (size_type i = 1; i < MPI_size; ++i) {
                for (size_type j = 0; j < sz; ++j) {
                    if (values[i * sz + j] != values[j]) {
                        throw std::runtime_error{
                            "CHAMPION::DataContainer(): If constructor is "
                            "called without specifying source rank, the "
                            "containers must be equal across all ranks."};
                    }
                }
            }
        }
#else
        container_ = container;
#endif
    }

    const auto begin() const { return container_.begin(); }
    const auto end() const { return container_.end(); }
    const auto cbegin() const { return container_.cbegin(); }
    const auto cend() const { return container_.cend(); }

    const auto &operator[](size_type i) const { return *(this->begin() + i); }

    // Public data members

  private:
    // Private member functions

    // Private data members
    std::vector<double> container_;

    // Friends
    friend std::ostream &operator<<(std::ostream &, const DataContainer &);
};

/************************ Member function definitions *************************/

/*********************** Non-member function definitions **********************/

std::ostream &operator<<(std::ostream &os, const DataContainer &dc) {
    if (MPI_rank == MPI_root) {
        os << dc.container_;
    }
    return os;
}

} // namespace champion
#endif