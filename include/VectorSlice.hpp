#ifndef CHAMPION_VECTOR_SLICE_HPP
#define CHAMPION_VECTOR_SLICE_HPP

#include <vector>

#include "set_precision.hpp"

namespace champion {

template <typename T> class VectorSlice {
  public:
    // Public types
    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;
    using value_type = T;

    // Public member functions
    // Default constructor
    VectorSlice() : vec_{}, offset_{}, length_{} {}
    // Construct from vector, offset where the slice starts,
    // and length of the slice
    VectorSlice(std::vector<T> &vec, size_type offset, size_type length)
        : vec_(&vec), offset_{offset}, length_{length} {
        // Trim if out of bounds
        if (vec.begin() + offset + length > vec.end()) {
            length_ = vec.size() - offset;
        }
    }
    // Construct from vector, iterator to first and length
    template <typename iter>
    VectorSlice(std::vector<T> &vec, iter first, size_type length)
        : VectorSlice{vec, first - vec.begin(), length} {}
    // Construct from vector, iterator to first, and iterator 1 beyond last
    template <typename iter>
    VectorSlice(std::vector<T> &vec, iter first, iter one_beyond_last)
        : vec_{&vec}, offset_{static_cast<size_type>(first - vec.begin())},
          length_{static_cast<size_type>(one_beyond_last - first)} {}

    // Construct from reference to vector (view of entire vector)
    VectorSlice(std::vector<T> &vec)
        : VectorSlice{vec, vec.begin(), vec.end()} {}

    // // Disable copying
    // VectorSlice(const VectorSlice<T> &) = delete;
    // VectorSlice<T> &operator=(const VectorSlice<T> &) = delete;
    // // Default move semantics
    // VectorSlice(VectorSlice<T> &&) = default;
    // VectorSlice<T> &operator=(VectorSlice &&) = default;

    // Access
    size_type size() const { return length_; }
    iterator begin() { return vec_->begin() + offset_; }
    iterator end() { return vec_->begin() + offset_ + length_; }
    const_iterator begin() const { return vec_->begin() + offset_; }
    const_iterator end() const { return vec_->begin() + offset_ + length_; }
    const_iterator cbegin() const { return begin(); }
    const_iterator cend() const { return end(); }
    // Read-write access to non-const VectorSlice
    T &operator[](size_type i) { return *(begin() + i); }
    // Read-only access to const VectorSlice
    const T &operator[](size_type i) const { return *(begin() + i); }

    // Conversion operations
    operator std::vector<T>() const {
        return std::vector<T>(this->begin(), this->end());
    }

  private:
    // Private data members
    std::vector<T> *vec_;
    size_type offset_;
    size_type length_;
};

template <typename T>
std::ostream &operator<<(std::ostream &os, const VectorSlice<T> &vs) {
    os << "{";
    for (size_type i = 0; i != vs.size() - 1; ++i) {
        os << vs[i] << ", ";
    }
    os << vs[vs.size() - 1] << "}";
    return os;
}

} // namespace champion

#endif
