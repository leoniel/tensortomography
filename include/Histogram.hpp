/******************************************************************************
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

SOURCE FILE: Histogram.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2018-07-12
MODIFIED BY:

DESCRIPTION:

\******************************************************************************/

#ifndef CHAMPION_HISTOGRAM_HPP
#define CHAMPION_HISTOGRAM_HPP

#include <algorithm>
#include <vector>

namespace champion {

class Histogram {
  public:
    // Public member functions

    // Default constructor
    Histogram() : limits_{}, counts_{} {}
    // Construct from data and (optionally) number of bins
    // Chooses bins based on the data
    Histogram(const std::vector<double> &values, size_type no_bins = 100);
    // Construct from min, max, number of bins and vector of elements
    Histogram(double min, double max, size_type no_bins = 100,
              const std::vector<double> &values = {});

    size_type size() const { return counts_.size(); }
    std::vector<double> get_limits() const { return limits_; }
    std::vector<double> get_x() const {
        std::vector<double> x(this->size(), 0);
        for (size_type i = 1; i != this->size(); ++i) {
            x[i] = (x[i - 1] + x[i]) / 2;
        }
        return x;
    }
    std::vector<size_type> get_counts() const { return counts_; }
    std::vector<double> get_y() const {
        std::vector<double> y(this->size(), 0);
        auto max = *std::max_element(counts_.begin(), counts_.end());
        for (size_type i = 0; i != this->size(); ++i) {
            y[i] = counts_[i] / max;
        }
        return y;
    }

    void add_data(const std::vector<double> &data);
    void configure(double min, double max, size_type no_bins);

  private:
    // Private data members
    std::vector<double> limits_;
    std::vector<size_type> counts_;
    friend std::ostream &operator<<(std::ostream &os, const Histogram &hist);
};

Histogram::Histogram(double min, double max, size_type no_bins,
                     const std::vector<double> &values)
    : limits_{}, counts_{} {
    configure(min, max, no_bins);
    for (auto val : values) {
        if (val < min) {
            continue;
        }
        for (size_type i = 1; i != no_bins; ++i) {
            if (val < limits_[i + 1]) {
                ++counts_[i];
                break;
            }
        }
    }
}

Histogram::Histogram(const std::vector<double> &values, size_type no_bins) {
    limits_ = std::vector<double>(no_bins + 1, 0);
    counts_ = std::vector<size_type>(no_bins, 0);
    auto minmax = std::minmax_element(values.begin(), values.end());
    auto min = *minmax.first;
    auto max = *minmax.second;
    configure(min, max, no_bins);
    for (auto val : values) {
        if (val < min) {
            continue;
        }
        for (size_type i = 1; i != no_bins; ++i) {
            if (val < limits_[i + 1]) {
                ++counts_[i];
                break;
            }
        }
    }
    // auto limit = min;
    // real step = (max - min) / no_bins;
    // for (auto &lim : limits_) {
    //     lim = limit;
    //     limit += step;
    // }
}

void Histogram::configure(double min, double max, size_type no_bins) {
    limits_ = std::vector<double>(no_bins + 1, 0);
    counts_ = std::vector<size_type>(no_bins, 0);
    auto limit = min;
    auto step = (max - min) / no_bins;
    for (auto &lim : limits_) {
        lim = limit;
        limit += step;
    }
}

void Histogram::add_data(const std::vector<double> &data) {
    for (auto d : data) {
        if (d < limits_[0]) {
            continue;
        }
        for (size_type i = 0; i != this->size(); ++i) {
            if (d < limits_[i + 1]) {
                ++counts_[i];
                break;
            }
        }
    }
}

std::ostream &operator<<(std::ostream &os, const Histogram &hist) {
    auto x = std::vector<double>(hist.size(), 0);
    for (size_type i = 0; i != hist.size(); ++i) {
        x[i] = (hist.limits_[i] + hist.limits_[i + 1]) / 2;
    }
    os << "x = [";
    for (size_type i = 0; i != hist.size() - 1; ++i) {
        os << x[i] << ", ";
    }
    os << x[hist.size() - 1] << "]" << std::endl;
    os << "y = [";
    for (size_type i = 0; i != hist.size() - 1; ++i) {
        os << hist.counts_[i] << ", ";
    }
    os << hist.counts_[hist.size() - 1] << "]" << std::endl;
    return os;
}

} // namespace champion

#endif
