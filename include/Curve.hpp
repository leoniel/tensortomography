#ifndef CHAMPION_CURVE_HPP
#define CHAMPION_CURVE_HPP

#include <iostream>
#include <vector>

#include "algorithm.hpp"
#include "set_precision.hpp"
#include "stl_io.hpp"

namespace champion {

template <typename T1 = real, typename T2 = real> class Curve {
  public:
    Curve(std::vector<T1> x, std::vector<T2> y) : x_{x}, y_{y} {
        if (x.size() != y.size()) {
            throw std::runtime_error{"x and y must be of the same size."};
        }
    }
    size_type size() const { return x_.size(); }
    const auto &x() const { return x_; }
    const auto &y() const { return y_; }
    auto &x() { return x_; }
    auto &y() { return y_; }
    const auto &x(size_type i) const { return x_[i]; }
    const auto &y(size_type i) const { return y_[i]; }
    auto &x(size_type i) { return x_[i]; }
    auto &y(size_type i) { return y_[i]; }

    Curve interval(T1 lo, T2 hi) {
        if (lo > hi) {
            throw std::runtime_error{
                "CHAMPION::Curve::interval(): hi must be greater than lo."};
        }
        size_type first = 0;
        size_type last = 0;
        size_type i = 0;
        while (x_[i] < lo) {
            ++i;
        }
        first = i;
        while (x_[i] < hi) {
            ++i;
        }
        last = i;
        auto x = std::vector<T1>(last - first + 1, 0);
        auto y = std::vector<T2>(last - first + 1, 0);
        for (size_type i = 0; i < last - first + 1; ++i) {
            x[i] = x_[i + first];
            y[i] = y_[i + first];
        }
        return Curve{x, y};
    }

    Curve smooth(size_type smoothing_window =
                     std::numeric_limits<size_type>::max()) const {
        if (smoothing_window == std::numeric_limits<size_type>::max()) {
            smoothing_window = std::max<size_type>(1, this->size() * 0.05);
        }
        auto smoothed = *this;
        smoothed.y_ = moving_average(smoothed.y_, smoothing_window);
        return smoothed;
    }

  private:
    std::vector<T1> x_;
    std::vector<T2> y_;
};

template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &os, const Curve<T1, T2> &curve) {
    os << "x = " << curve.x() << std::endl;
    os << "y = " << curve.y() << std::endl;
    return os;
}

} // namespace champion
#endif