/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017-2019 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: sort.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se)
MODIFIED BY:

DESCRIPTION:

Generic quicksort implementation allowing custom swap operation (unlike the
standard library). Uses Hoare partitioning and mid container pivot selection.

\******************************************************************************/

#include <algorithm>
#include <functional>
#include <iterator>

namespace champion {

template <typename T> class StdSwap {
    void operator()(T &a, T &b) { std::swap(a, b); }
};

template <
    typename RandomIt,
    typename Cmp = std::less<typename std::iterator_traits<RandomIt>::type>,
    typename Swp = StdSwap<typename std::iterator_traits<RandomIt>::type>>
void sort(RandomIt begin, RandomIt end, Cmp compare = Cmp{}, Swp swap = Swp{}) {
    auto last = end - 1;
    if (last <= begin) {
        return;
    }
    if (last == begin + 1) {
        if (compare(*begin, *last)) {
            return;
        }
        swap(*begin, *last);
        return;
    }
    auto pivot = begin + (last - begin) / 2;
    swap(*pivot, *last);
    pivot = last;
    auto left = begin;
    auto right = last - 1;
    while (left < right) {
        while (left < last && !compare(*pivot, *left)) {
            ++left;
        }
        while (right > begin && !compare(*right, *pivot)) {
            --right;
        }
        if (left < right) {
            swap(*left++, *right--);
        }
    }
    swap(*left, *pivot);
    sort(begin, left, compare, swap);
    sort(left + 1, end, compare, swap);
}

} // namespace champion