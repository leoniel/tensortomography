#ifndef CHAMPION_MATRIX_HPP
#define CHAMPION_MATRIX_HPP

#include <cassert>
#include <cmath>
#include <functional>
#include <iomanip>
#include <limits>
#include <map>
#include <memory>
#include <numeric>
#include <sstream>
#include <utility>
#include <vector>

#include "choose.hpp"
#include "set_precision.hpp"
#include "stl_io.hpp"
#include "type_inference.hpp"

namespace champion {

/*************************** Forward declarations ****************************/

template <typename T, size_type N, size_type M> class Matrix;
template <typename T, size_type N> class SquareMatrix;
template <typename T, size_type N> class SymmetricMatrix;
template <typename T, size_type N, size_type M> class SparseMatrix;
template <typename T, size_type N> class SparseSquareMatrix;
template <typename T, size_type N> class SparseSymmetricMatrix;

template <typename T> class Vector;

/****************************** Type functions *******************************/

// Empty base class for all matrices, to enable checking whether a type is a
// matrix
struct matrix_tag {};

template <typename MatrixType> struct is_matrix {
    static constexpr bool value =
        std::is_base_of<matrix_tag, MatrixType>::value;
};

template <typename MatrixType>
constexpr bool Is_matrix = is_matrix<MatrixType>::value;

// Compile time dispatch of matrix types based on traits

template <typename element_type, size_type N, size_type M, bool is_square,
          bool is_symmetric, bool is_sparse>
struct matrix_type {
    using type = Matrix<element_type, N, M>;
};

template <typename element_type, size_type N, size_type M>
struct matrix_type<element_type, N, M, true, false, false> {
    using type = SquareMatrix<element_type, N>;
};

template <typename element_type, size_type N, size_type M>
struct matrix_type<element_type, N, M, true, true, false> {
    using type = SymmetricMatrix<element_type, N>;
};

template <typename element_type, size_type N, size_type M>
struct matrix_type<element_type, N, M, false, false, true> {
    using type = SparseMatrix<element_type, N, M>;
};

template <typename element_type, size_type N, size_type M>
struct matrix_type<element_type, N, M, true, false, true> {
    using type = SparseSquareMatrix<element_type, N>;
};

template <typename element_type, size_type N, size_type M>
struct matrix_type<element_type, N, M, true, true, true> {
    using type = SparseSymmetricMatrix<element_type, N>;
};

// Error: Symmetric but not square, disallow!
template <typename element_type, size_type N, size_type M>
struct matrix_type<element_type, N, M, false, true, false> {};
template <typename element_type, size_type N, size_type M>
struct matrix_type<element_type, N, M, false, true, true> {};

// Compile time dispatch for the result of matrix additions
template <typename Matrix1, typename Matrix2> struct matrix_sum_type {
    // Do compile-time dimensions checking if possible. Otherwise, delegate to
    // run-time checking by setting dynamically determined dimensions to 0.
    static constexpr size_type N1 = Matrix1::number_of_rows;
    static constexpr size_type M1 = Matrix1::number_of_cols;
    static constexpr size_type N2 = Matrix2::number_of_rows;
    static constexpr size_type M2 = Matrix2::number_of_cols;
    static constexpr size_type N = Choose_value<size_type, (N1 && N2), N1, 0>;
    static_assert(!N || N1 == N2);
    static constexpr size_type M = Choose_value<size_type, (M1 && M2), M1, 0>;
    static_assert(!M || M1 == M2);
    static_assert(Matrix1::is_square == Matrix2::is_square);
    static constexpr bool is_square = Matrix1::is_square;
    static constexpr bool is_symmetric =
        (Matrix1::is_symmetric && Matrix2::is_symmetric);
    static constexpr bool is_sparse =
        (Matrix1::is_sparse && Matrix2::is_sparse);
    using element_type = Sum_type<typename Matrix1::element_type,
                                  typename Matrix2::element_type>;
    using type = typename matrix_type<element_type, N, M, is_square,
                                      is_symmetric, is_sparse>::type;
};

// Compile time dispatch for the result of matrix multiplications
template <typename Matrix1, typename Matrix2> struct matrix_product_type {
    static constexpr size_type N1 = Matrix1::number_of_rows;
    static constexpr size_type M1 = Matrix1::number_of_cols;
    static constexpr size_type N2 = Matrix2::number_of_rows;
    static constexpr size_type M2 = Matrix2::number_of_cols;
    // Require M1 and M2 to match if dimensions are known at compile-time.
    // Otherwise, defer checking to run-time.
    static_assert(!(M1 && N2) || M1 == N2);
    static constexpr size_type N = N1;
    static constexpr size_type M = M2;
    static constexpr bool is_square = (M == N);
    static constexpr bool is_symmetric = false;
    static constexpr bool is_sparse =
        (Matrix1::is_sparse && Matrix2::is_sparse);
    using element_type = Product_type<typename Matrix1::element_type,
                                      typename Matrix2::element_type>;
    using type = typename matrix_type<element_type, N, M, is_square,
                                      is_symmetric, is_sparse>::type;
};

// Compile time dispatch for Matrix scaling
template <typename MatrixType, typename T> struct scaled_matrix_type {
    static constexpr size_type N = MatrixType::number_of_rows;
    static constexpr size_type M = MatrixType::number_of_cols;
    using element_type = Product_type<typename MatrixType::element_type, T>;
    static constexpr bool is_square = MatrixType::is_square;
    static constexpr bool is_symmetric = MatrixType::is_symmetric;
    static constexpr bool is_sparse = MatrixType::is_sparse;
    using type = typename matrix_type<element_type, N, M, is_square,
                                      is_symmetric, is_sparse>::type;
};

/*************************** Functions on matrices ***************************/

template <typename MatrixType>
typename MatrixType::transpose_type compute_transpose(const MatrixType &A) {
    auto A_T = typename MatrixType::transpose_type{};
    for (size_type i = 0; i != A.num_rows(); ++i) {
        for (size_type j = 0; j != A.num_cols(); ++j) {
            A_T(j, i) = A(i, j);
        }
    }
    return A_T;
}

// Submatrix, disallowed for M or N = 1 as this makes the submatrix empty
template <typename MatrixType>
std::enable_if_t<(MatrixType::number_of_rows != 1 &&
                  MatrixType::number_of_cols != 1),
                 typename MatrixType::submatrix_type>
compute_submatrix(const MatrixType &A, size_type i, size_type j) {
    typename MatrixType::submatrix_type sub;
    auto N = A.num_rows();
    auto M = A.num_cols();
    auto rows = std::vector<size_type>(N);
    std::iota(rows.begin(), rows.end(), 0);
    rows.erase(std::find(rows.begin(), rows.end(), i));
    auto cols = std::vector<size_type>(M);
    std::iota(cols.begin(), cols.end(), 0);
    cols.erase(std::find(cols.begin(), cols.end(), j));
    for (size_type ii = 0; ii != rows.size(); ++ii) {
        for (size_type jj = 0; jj != cols.size(); ++jj) {
            sub(ii, jj) = A(rows[ii], cols[jj]);
        }
    }
    return sub;
}

// Unary operators on square matrices
template <typename MatrixType>
typename std::enable_if_t<MatrixType::is_square,
                          typename MatrixType::element_type>
compute_trace(const MatrixType &A) {
    typename MatrixType::element_type sum = 0;
    for (size_type i = 0; i != A.num_rows(); ++i) {
        sum += A(i, i);
    }
    return sum;
}

// Computes the sign of a submatrix in adjugate expansion
short sign(size_type i, size_type j) { return std::pow(-1, i + j); };

template <typename MatrixType>
typename std::enable_if_t<(MatrixType::is_square &&
                           MatrixType::number_of_rows > 1),
                          typename MatrixType::element_type>
compute_determinant(const MatrixType &A) {
    typename MatrixType::element_type det = 0;
    // Expand submatrices along first row
    size_type i = 0;
    for (size_type j = 0; j != A.num_cols(); ++j) {
        det += sign(i, j) * A(i, j) *
               compute_determinant(compute_submatrix(A, i, j));
    }
    return det;
}

template <typename MatrixType>
typename std::enable_if_t<(MatrixType::is_square &&
                           MatrixType::number_of_rows == 1),
                          typename MatrixType::element_type>
compute_determinant(const MatrixType &A) {
    return A(0, 0);
}

template <typename MatrixType>
typename std::enable_if_t<(MatrixType::is_square &&
                           MatrixType::number_of_rows == 0),
                          typename MatrixType::element_type>
compute_determinant(const MatrixType &A) {
    assert(A.num_rows() == A.num_cols());
    if (A.num_rows() == 1) {
        return A(0, 0);
    } else if (A.num_rows() > 1) {
        typename MatrixType::element_type det = 0;
        // Expand submatrices along first row
        size_type i = 0;
        for (size_type j = 0; j != A.num_cols(); ++j) {
            det += sign(i, j) * A(i, j) *
                   compute_determinant(compute_submatrix(A, i, j));
        }
    } else {
        throw std::runtime_error{"CHAMPION::Matrix::compute_determinant: "
                                 "Dynamically set dimension may not be 0."};
    }
}

template <typename MatrixType>
typename std::enable_if_t<MatrixType::is_square,
                          typename MatrixType::inverse_type>
compute_adjugate(const MatrixType &A) {
    typename MatrixType::inverse_type adj;
    for (size_type i = 0; i != A.num_rows(); ++i) {
        for (size_type j = 0; j != A.num_cols(); ++j) {
            adj(j, i) =
                sign(i, j) * compute_determinant(compute_submatrix(A, i, j));
        }
    }
    return adj;
}

template <typename MatrixType>
typename std::enable_if_t<MatrixType::is_square,
                          typename MatrixType::inverse_type>
compute_inverse(const MatrixType &A) {
    auto det = compute_determinant(A);
    if (det == 0) {
        throw std::runtime_error{"CHAMPION::SquareMatrixInterface::inverse("
                                 "): Matrix is singular."};
    }
    auto res = compute_adjugate(A);
    res /= det;
    return res;
}

/******************************* Interfaces **********************************/

// Abstract interface for immutable matrices.
template <typename T> class MatrixInterface : public matrix_tag {
  public:
    virtual ~MatrixInterface() {}
    // Access

    // Dimensions
    virtual size_type num_rows() const = 0;
    virtual size_type num_cols() const = 0;

    // Element access
    virtual const T &operator()(size_type row, size_type col) const = 0;
    virtual T &operator()(size_type row, size_type col) = 0;

    // Access to whole rows and columns
    virtual std::vector<T> get_row(size_type i) const = 0;
    virtual std::vector<std::reference_wrapper<T>> get_row(size_type i) = 0;
    virtual std::vector<T> get_col(size_type i) const = 0;
    virtual std::vector<std::reference_wrapper<T>> get_col(size_type i) = 0;
};

/******************* Abstract implementation base classes ********************/

// Abstract base class for dense matrices implementing common access
// operators in terms of function get_index() which each derived class has
// to implement
template <typename T, size_type N, size_type M>
class MatrixBase : public MatrixInterface<T> {
  public:
    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;

    virtual ~MatrixBase() {}

    // Public access functions
    virtual const T &operator()(size_type row, size_type col) const override {
        if (row >= this->num_rows()) {
            throw std::out_of_range{"CHAMPION::Matrix: Row out of bounds."};
        }
        if (col >= this->num_cols()) {
            throw std::out_of_range{"CHAMPION::Matrix: Column out of bounds."};
        }
        return elements_[get_index(row, col)];
    }

    virtual T &operator()(size_type row, size_type col) override {
        return const_cast<T &>(
            (*static_cast<const MatrixBase *>(this))(row, col));
    }

    virtual std::vector<T> get_row(size_type i) const override {
        auto row = std::vector<T>(this->num_cols());
        for (size_type j = 0; j != this->num_cols(); ++j) {
            row[j] = (*this)(i, j);
        }
        return row;
    }

    virtual std::vector<std::reference_wrapper<T>>
    get_row(size_type i) override {
        auto row = std::vector<std::reference_wrapper<T>>{};
        for (size_type j = 0; j != this->num_cols(); ++j) {
            row.push_back((*this)(i, j));
        }
        return row;
    }

    virtual std::vector<T> get_col(size_type j) const override {
        auto col = std::vector<T>(this->num_rows());
        for (size_type i = 0; i != this->num_rows(); ++i) {
            col[i] = (*this)(i, j);
        }
        return col;
    }

    virtual std::vector<std::reference_wrapper<T>>
    get_col(size_type j) override {
        auto col = std::vector<std::reference_wrapper<T>>{};
        for (size_type i = 0; i != this->num_rows(); ++i) {
            col.push_back((*this)(i, j));
        }
        return col;
    }

    // Range-based access
    inline iterator begin() { return elements_.begin(); }
    inline iterator end() { return elements_.end(); }
    inline const_iterator begin() const { return elements_.begin(); }
    inline const_iterator end() const { return elements_.end(); }
    inline const_iterator cbegin() const { return elements_.cbegin(); }
    inline const_iterator cend() const { return elements_.cend(); }

  protected:
    // Protected member functions

    // Get index (implementation detail)
    virtual size_type get_index(size_type row, size_type col) const = 0;

    // Protected data members

    // Element storage
    std::vector<T> elements_;
};

// Abstract base class implementing common features of sparse matrices.
// Each derived class must implement check_bounds() as well as num_rows()
// and num_cols() from MatrixInterface
template <typename T, size_type N, size_type M>
class SparseMatrixBase : public MatrixInterface<T> {
  public:
    using iterator =
        typename std::map<std::pair<size_type, size_type>, T>::iterator;
    using const_iterator =
        typename std::map<std::pair<size_type, size_type>, T>::const_iterator;

    // Default constructor
    SparseMatrixBase(T default_value = {})
        : elements_{}, default_value_{default_value} {}

    virtual ~SparseMatrixBase() {}

    // Public access functions

    virtual const T &operator()(size_type row, size_type col) const override {
        check_bounds(row, col);
        try {
            return elements_.at({row, col});
        } catch (std::out_of_range &) {
            return default_value_;
        }
    }
    virtual T &operator()(size_type row, size_type col) override {
        check_bounds(row, col);
        try {
            return elements_.at({row, col});
        } catch (std::out_of_range &) {
            elements_[{row, col}] = default_value_;
            return elements_[{row, col}];
        }
    }

    virtual std::vector<T> get_row(size_type i) const override {
        auto row = std::vector<T>(this->num_cols());
        for (size_type j = 0; j != this->num_cols(); ++j) {
            row[j] = (*this)(i, j);
        }
        return row;
    }

    virtual std::vector<std::reference_wrapper<T>>
    get_row(size_type i) override {
        auto row = std::vector<std::reference_wrapper<T>>{};
        for (size_type j = 0; j != this->num_cols(); ++j) {
            row.push_back((*this)(i, j));
        }
        return row;
    }

    virtual std::vector<T> get_col(size_type j) const override {
        auto col = std::vector<T>(this->num_rows());
        for (size_type i = 0; i != this->num_rows(); ++i) {
            col[i] = (*this)(i, j);
        }
        return col;
    }

    virtual std::vector<std::reference_wrapper<T>>
    get_col(size_type j) override {
        auto col = std::vector<std::reference_wrapper<T>>{};
        for (size_type i = 0; i != this->num_rows(); ++i) {
            col.push_back((*this)(i, j));
        }
        return col;
    }

    // Range-based access
    inline iterator begin() { return elements_.begin(); }
    inline iterator end() { return elements_.end(); }
    inline const_iterator begin() const { return elements_.begin(); }
    inline const_iterator end() const { return elements_.end(); }
    inline const_iterator cbegin() const { return elements_.cbegin(); }
    inline const_iterator cend() const { return elements_.cend(); }

    // Modification

    // Remove any default-valued elements from storage
    // (may be created by frivolous use of read/write element access)
    virtual void prune() {
        for (auto &el : elements_) {
            if (el.second == default_value_) {
                elements_.erase(el.first);
            }
        }
    }

  protected:
    // Protected member functions
    virtual void check_bounds(size_type &i, size_type &j) const = 0;
    // Protected data members
    std::map<std::pair<size_type, size_type>, T> elements_;
    T default_value_;
};

/**************************** Concrete classes *******************************/

// Generic (dense, non-symmetric) matrix class
template <typename T, size_type N = 0, size_type M = 0>
class Matrix : public MatrixBase<T, N, M> {
  public:
    // Types
    using element_type = T;
    using matrix_type = Matrix<T, N, M>;
    using transpose_type = Matrix<T, M, N>;
    using inverse_type = void;
    static constexpr size_type sub_N =
        Choose_value<size_type, (N == 0), 0, N - 1>;
    static constexpr size_type sub_M =
        Choose_value<size_type, (M == 0), 0, M - 1>;
    using submatrix_type = Matrix<T, sub_N, sub_M>;
    // Traits
    static constexpr size_type number_of_rows = N;
    static constexpr size_type number_of_cols = M;
    static constexpr bool is_square = false;
    static constexpr bool is_symmetric = false;
    static constexpr bool is_sparse = false;

    // Construction, assignment and clean-up

    // Default constructor
    Matrix(T default_value = {}) : number_of_rows_{N}, number_of_cols_{M} {
        this->elements_ = decltype(this->elements_)(N * M, default_value);
    }

    // Copy and move semantics
    Matrix(const Matrix &) = default;
    Matrix &operator=(const Matrix &) = default;
    Matrix(Matrix &&) = default;
    Matrix &operator=(Matrix &&) = default;

    // Construct from pair of iterators, assumes row-major order
    template <typename Iter>
    Matrix(Iter begin, Iter end) : number_of_rows_{N}, number_of_cols_{M} {
        for (auto it = begin; it != end; ++it) {
            this->elements_.push_back(*it);
        }
        if (N && M && this->elements_.size() != N * M) {
            throw std::runtime_error{
                "CHAMPION::Matrix: Matrix must be initialized with the right "
                "number of elements to fill the matrix."};
        }
    }

    // Construct from initializer list, assumes row-major order
    Matrix(const std::initializer_list<T> ls)
        : number_of_rows_{N}, number_of_cols_{M} {
        this->elements_ = ls;
        if (N && M && this->elements_.size() != N * M) {
            throw std::runtime_error{
                "CHAMPION::Matrix: Matrix must be initialized with the right "
                "number of elements to fill the matrix."};
        }
    }

    // Construct from vector of elements, assumes row-major order
    Matrix(const std::vector<T> &vec) : number_of_rows_{N}, number_of_cols_{M} {
        this->elements_ = vec;
        if (N && M && this->elements_.size() != N * M) {
            throw std::runtime_error{
                "CHAMPION::Matrix: Matrix must be initialized with the right "
                "number of elements to fill the matrix."};
        }
    }
    Matrix(std::vector<T> &&vec) : number_of_rows_{N}, number_of_cols_{M} {
        this->elements_ = vec;
        if (N && M && this->elements_.size() != N * M) {
            throw std::runtime_error{
                "CHAMPION::Matrix: Matrix must be initialized with the right "
                "number of elements to fill the matrix."};
        }
    }

    // Construct from shape and vector of elements (optional)
    Matrix(size_type number_of_rows, size_type number_of_cols,
           const std::vector<T> &vec = {})
        : number_of_rows_{N}, number_of_cols_{M} {
        if (N != 0 && N != number_of_rows) {
            throw std::runtime_error{
                "CHAMPION::Matrix: Tried to construct a matrix with number of "
                "rows incompatible with static type."};
        }
        if (M != 0 && M != number_of_cols) {
            throw std::runtime_error{
                "CHAMPION::Matrix: Tried to construct a matrix with number of "
                "cols incompatible with static type."};
        }
        number_of_rows_ = number_of_rows;
        number_of_cols_ = number_of_cols;
        if (vec.size() == number_of_rows * number_of_cols) {
            this->elements_ = vec;
        } else if (vec.empty()) {
            this->elements_ =
                decltype(this->elements_)(number_of_rows * number_of_cols);
        } else {
            throw std::runtime_error{"CHAMPION::Matrix: Could not construct "
                                     "matrix from given vector. Wrong size."};
        }
    }
    Matrix(size_type number_of_rows, size_type number_of_cols,
           std::vector<T> &&vec)
        : number_of_rows_{N}, number_of_cols_{M} {
        if (N != 0 && N != number_of_rows) {
            throw std::runtime_error{
                "CHAMPION::Matrix: Tried to construct a matrix with number of "
                "rows incompatible with static type."};
        }
        if (M != 0 && M != number_of_cols) {
            throw std::runtime_error{
                "CHAMPION::Matrix: Tried to construct a matrix with number of "
                "cols incompatible with static type."};
        }
        number_of_rows_ = number_of_rows;
        number_of_cols_ = number_of_cols;
        if (vec.size() == number_of_rows * number_of_cols) {
            this->elements_ = vec;
        } else if (vec.empty()) {
            this->elements_ = (number_of_rows * number_of_cols);
        } else {
            throw std::runtime_error{"CHAMPION::Matrix: Could not construct "
                                     "matrix from given vector. Wrong size."};
        }
    }

    // Assignment from vector of elements
    Matrix &operator=(const std::vector<T> &vec) {
        if (vec.size() != this->num_rows() * this->num_cols()) {
            throw std::runtime_error{"CHAMPION::Matrix: Tried to assign a "
                                     "vector of the wrong size to a Matrix."};
        }
        this->elements_ = vec;
        return *this;
    }
    Matrix &operator=(std::vector<T> &&vec) {
        if (vec.size() != this->num_rows() * this->num_cols()) {
            throw std::runtime_error{"CHAMPION::Matrix: Tried to assign a "
                                     "vector of the wrong size to a Matrix."};
        }
        this->elements_ = vec;
        return *this;
    }

    ~Matrix() = default;

    // Construct as copy from any type of matrix object
    template <typename T2>
    Matrix(const MatrixInterface<T2> &A)
        : number_of_rows_{N}, number_of_cols_{M} {
        if (N != 0 && A.num_rows() != N) {
            throw std::runtime_error{"CHAMPION::Matrix: Dimension mismatch for "
                                     "rows in constructor."};
        }
        if (M != 0 && A.num_cols() != M) {
            throw std::runtime_error{"CHAMPION::Matrix: Dimension mismatch for "
                                     "columns in constructor."};
        }
        number_of_rows_ = A.num_rows();
        number_of_cols_ = A.num_cols();
        for (size_type i = 0; i != number_of_rows; ++i) {
            for (size_type j = 0; j != number_of_cols; ++j) {
                (*this)(i, j) = A(i, j);
            }
        }
    }

    // Construct from Vector (only valid for 3x1 matrix)
    // Implemented in Vector.hpp
    template <typename T2> explicit Matrix(const Vector<T2> &V);

    // Access

    virtual size_type num_rows() const override { return number_of_rows_; }
    virtual size_type num_cols() const override { return number_of_cols_; }

    // Modification

    auto transpose() const { return compute_transpose(*this); }

  private:
    // Private member functions
    virtual size_type get_index(size_type row, size_type col) const override {
        return row * num_cols() + col;
    }

    // Private data members
    size_type number_of_rows_;
    size_type number_of_cols_;
}; // namespace champion

// Generic dense square matrix
template <typename T, size_type N = 0>
class SquareMatrix : public MatrixBase<T, N, N> {
  public:
    // Types
    using element_type = T;
    using matrix_type = SquareMatrix<T, N>;
    using transpose_type = SquareMatrix<T, N>;
    using inverse_type = SquareMatrix<T, N>;
    using submatrix_type =
        SquareMatrix<T, Choose_value<size_type, N == 0, 0, N - 1>>;
    // Traits
    static constexpr size_type number_of_rows = N;
    static constexpr size_type number_of_cols = N;
    static constexpr bool is_square = true;
    static constexpr bool is_symmetric = false;
    static constexpr bool is_sparse = false;

    // Default constructor
    SquareMatrix(T default_value = {}) : dim_{N} {
        this->elements_ = std::vector<T>(N * N, default_value);
    }

    // Construct from initializer list, assumes row-major order
    SquareMatrix(std::initializer_list<T> ls) : dim_{N} {
        if (N != 0 && ls.size() != N * N) {
            throw std::runtime_error{"CHAMPION::SquareMatrix: Matrix must be "
                                     "initialized with the right "
                                     "number of elements to fill the matrix."};
        }
        this->elements_ = ls;
    }

    // Construct from vector of elements, assumes row-major order
    SquareMatrix(const std::vector<T> &vec) : dim_{N} {
        if (N != 0 && vec.size() != N * N) {
            throw std::runtime_error{"CHAMPION::SquareMatrix: Matrix must be "
                                     "initialized with the right "
                                     "number of elements to fill the matrix."};
        }
        this->elements_ = vec;
    }
    SquareMatrix(std::vector<T> &&vec) : dim_{N} {
        if (N && vec.size() != N * N) {
            throw std::runtime_error{"CHAMPION::SquareMatrix: Matrix must be "
                                     "initialized with the right "
                                     "number of elements to fill the matrix."};
        }
        this->elements_ = vec;
    }

    // Construct from shape and vector of elements (optional)
    SquareMatrix(size_type number_of_rows, size_type number_of_cols,
                 const std::vector<T> &vec = {})
        : dim_{N} {
        if (number_of_rows != number_of_cols) {
            throw std::runtime_error{
                "CHAMPION::SquareMatrix: Tried to construct a SquareMatrix "
                "with unequal number of rows and columns."};
        }
        if (N != 0 && number_of_rows != N) {
            throw std::runtime_error{"CHAMPION::SquareMatrix: Tried to "
                                     "construct a matrix with number of "
                                     "rows incompatible with static type."};
        }
        dim_ = number_of_rows;
        if (vec.size() == dim_ * dim_) {
            this->elements_ = vec;
        } else if (vec.empty()) {
            this->elements_ = decltype(this->elements_)(dim_ * dim_);
        } else {
            throw std::runtime_error{
                "CHAMPION::SquareMatrix: Could not construct matrix from the "
                "given vector. Wrong size."};
        }
    }
    SquareMatrix(size_type number_of_rows, size_type number_of_cols,
                 std::vector<T> &&vec)
        : dim_{N} {
        if (number_of_rows != number_of_cols) {
            throw std::runtime_error{
                "CHAMPION::SquareMatrix: Tried to construct a SquareMatrix "
                "with unequal number of rows and columns."};
        }
        if (N != 0 && number_of_rows != N) {
            throw std::runtime_error{"CHAMPION::SquareMatrix: Tried to "
                                     "construct a matrix with number of "
                                     "rows incompatible with static type."};
        }
        dim_ = number_of_rows;
        if (vec.size() == dim_ * dim_) {
            this->elements_ = vec;
        } else if (vec.empty()) {
            this->elements_ = decltype(this->elements_)(dim_ * dim_);
        } else {
            throw std::runtime_error{
                "CHAMPION::SquareMatrix: Could not construct matrix from the "
                "given vector. Wrong size."};
        }
    }

    // Copy and move semantics
    SquareMatrix(const SquareMatrix &) = default;
    SquareMatrix &operator=(const SquareMatrix &) = default;
    SquareMatrix(SquareMatrix &&) = default;
    SquareMatrix &operator=(SquareMatrix &&) = default;

    // Assignment from vector of elements
    SquareMatrix &operator=(const std::vector<T> &vec) {
        if (vec.size() != this->num_rows() * this->num_cols()) {
            throw std::runtime_error{"CHAMPION::Matrix: Tried to assign a "
                                     "vector of the wrong size to a Matrix."};
        }
        this->elements_ = vec;
        return *this;
    }
    SquareMatrix &operator=(std::vector<T> &&vec) {
        if (vec.size() != this->num_rows() * this->num_cols()) {
            throw std::runtime_error{"CHAMPION::Matrix: Tried to assign a "
                                     "vector of the wrong size to a Matrix."};
        }
        this->elements_ = vec;
        return *this;
    }

    // Construct as copy from any square type of matrix object
    SquareMatrix(const MatrixInterface<T> &A) : dim_{A.num_rows()} {
        if (A.num_rows() != A.num_cols()) {
            throw std::runtime_error{
                "CHAMPION::SquareMatrix: Tried to construct square matrix from "
                "copy of non-square matrix"};
        }
        if (N != 0 && A.num_rows() != N) {
            throw std::runtime_error{
                "CHAMPION::SquareMatrix: Dimension mismatch in constructor."};
        }
        this->elements_ = std::vector<T>(number_of_rows * number_of_cols);
        for (size_type i = 0; i != number_of_rows; ++i) {
            for (size_type j = 0; j != number_of_cols; ++j) {
                (*this)(i, j) = A(i, j);
            }
        }
    }

    virtual ~SquareMatrix() {}

    // Access
    size_type num_rows() const override { return dim_; }
    size_type num_cols() const override { return dim_; }

    // Modification

    auto transpose() const { return compute_transpose(*this); }

    auto trace() const { return compute_trace(*this); }
    auto determinant() const { return compute_determinant(*this); }
    auto inverse() const { return compute_inverse(*this); }

  private:
    // Private member functions
    virtual size_type get_index(size_type row, size_type col) const override {
        return row * num_cols() + col;
    }
    // Private data members
    size_type dim_;
};

// Symmetric matrix (dense)
template <typename T, size_type N = 0>
class SymmetricMatrix : public MatrixBase<T, N, N> {
  public:
    // Types
    using element_type = T;
    using matrix_type = SymmetricMatrix<T, N>;
    using transpose_type = SymmetricMatrix<T, N>;
    using inverse_type = SymmetricMatrix<T, N>;
    using submatrix_type =
        SquareMatrix<T, Choose_value<size_type, N == 0, 0, N - 1>>;
    // Traits
    static constexpr size_type number_of_rows = N;
    static constexpr size_type number_of_cols = N;
    static constexpr bool is_square = true;
    static constexpr bool is_symmetric = true;
    static constexpr bool is_sparse = false;

    // Default constructor
    SymmetricMatrix(T default_value = {}) : dim_{N} {
        constexpr size_type num_elements = N * (N + 1) / 2;
        this->elements_ = std::vector<T>(num_elements, default_value);
    }

    // Construct from initializer list
    //(must give all elements, including redundant ones)
    SymmetricMatrix(std::initializer_list<T> ls) : dim_{N} {
        if (N != 0 && (ls.size() != N * N)) {
            throw std::runtime_error{
                "CHAMPION::SymmetricMatrix: Matrix must be "
                "initialized with the right "
                "number of elements to fill the matrix."};
        }
        // Check if full (redundant) list was passed
        auto sroot = std::sqrt(ls.size());
        auto nn = std::round(sroot);
        // if ls.size() is square
        if (sroot == nn) {
            bool symm = true;
            for (size_type i = 0; i < nn; ++i) {
                for (size_type j = i + 1; j < nn; ++j) {
                    if (ls[i * nn + j] != ls[j * nn + i]) {
                        symm = false;
                        break;
                    }
                }
            }
            if (symm) {
                this->elements_ = {};
                dim_ = nn;
                for (size_type i = 0; i < dim_; ++i) {
                    for (size_type j = i + 1; j < dim_; ++j) {
                        this->elements_.push_back(ls[i * nn + j]);
                    }
                }
                return;
            } else
                throw std::runtime_error{
                    "CHAMPION::SymmetricMatrix: Matrix must be "
                    "initialized with the right "
                    "number of elements to fill the matrix."};
        }
    }

    // Construct from vector
    SymmetricMatrix(const std::vector<T> &vec) : dim_{N} {
        // Check that static dimension (if set) matches vector length
        if (N != 0 && (vec.size() != N * N || vec.size() != num_elements())) {
            throw std::runtime_error{
                "CHAMPION::SymmetricMatrix: Matrix must be "
                "initialized with the right "
                "number of elements to fill the matrix."};
        }
        // Check if full (redundant) vector was passed
        auto sroot = std::sqrt(vec.size());
        auto nn = std::round(sroot);
        // if vec.size() is square
        if (sroot == nn) {
            bool symm = true;
            for (size_type i = 0; i < nn; ++i) {
                for (size_type j = i + 1; j < nn; ++j) {
                    if (vec[i * nn + j] != vec[j * nn + i]) {
                        symm = false;
                        break;
                    }
                }
            }
            if (symm) {
                this->elements_ = {};
                dim_ = nn;
                for (size_type i = 0; i < dim_; ++i) {
                    for (size_type j = i + 1; j < dim_; ++j) {
                        this->elements_.push_back(vec[i * nn + j]);
                    }
                }
                return;
            }
        }
        // The number of dimensions corresponding to given vector if
        // non-redundant vector is given
        auto calc_dim = (std::sqrt(8 * vec.size() + 1) - 1) / 2;
        nn = std::round(calc_dim);
        if (calc_dim == nn) {
            this->elements_ = vec;
            dim_ = nn;
            return;
        }
        // If nothing returned yet, error!
        throw std::runtime_error{
            "CHAMPION::SymmetricMatrix: Cannot initialize SymmetricMatrix from "
            "given vector. Wrong number of elements."};
    }

    // Construct from shape and vector of elements (optional)
    SymmetricMatrix(size_type number_of_rows, size_type number_of_cols,
                    const std::vector<T> &vec = {})
        : dim_{N} {
        if (number_of_rows != number_of_cols) {
            throw std::runtime_error{
                "CHAMPION::SymmetricMatrix: Tried to construct a matrix "
                "with unequal number of rows and columns."};
        }
        if (N != 0 && number_of_rows != N) {
            throw std::runtime_error{"CHAMPION::SquareMatrix: Tried to "
                                     "construct a matrix with number of "
                                     "rows incompatible with static type."};
        }
        dim_ = number_of_rows;
        // If vector is empty, return vector of default-constructed values
        if (vec.empty()) {
            this->elements_ = decltype(this->elements_)(num_elements());
            return;
        }
        // Check if full (redundant) vector was passed
        if (vec.size() == dim_ * dim_) {
            // Check that it is symmetric
            for (size_type i = 0; i != dim_; ++i) {
                for (size_type j = i + 1; j < dim_; ++j) {
                    if (vec[i * dim_ + j] != vec[j * dim_ + i]) {
                        throw std::runtime_error{
                            "CHAMPION::SymmetricMatrix: Illegal vector passed "
                            "to constructor. Either the wrong number of "
                            "elements or violated symmetry."};
                    }
                }
            }
            this->elements_ = {};
            for (size_type i = 0; i < dim_; ++i) {
                for (size_type j = 0; j < dim_; ++j) {
                    this->elements_.push_back(vec[i * dim_ + j]);
                }
            }
            return;
        }
        // Check if non-redundant vector was passed
        if (vec.size() == num_elements()) {
            this->elements_ = vec;
            return;
        }
        // If nothing returned yet, error!
        throw std::runtime_error{
            "CHAMPION::SymmetricMatrix: Cannot initialize SymmetricMatrix from "
            "given vector. Wrong number of elements."};
    }

    // Copy and move semantics
    SymmetricMatrix(const SymmetricMatrix &) = default;
    SymmetricMatrix &operator=(const SymmetricMatrix &) = default;
    SymmetricMatrix(SymmetricMatrix &&) = default;
    SymmetricMatrix &operator=(SymmetricMatrix &&) = default;

    // Assign from vector of elements
    SymmetricMatrix &operator=(const std::vector<T> &vec) {
        // Check if full (redundant) vector was passed
        if (vec.size() == dim_ * dim_) {
            // Check that it is symmetric
            for (size_type i = 0; i != dim_; ++i) {
                for (size_type j = i + 1; j < dim_; ++j) {
                    if (vec[i * dim_ + j] != vec[j * dim_ + i]) {
                        throw std::runtime_error{
                            "CHAMPION::SymmetricMatrix: Illegal vector passed "
                            "to constructor. Either the wrong number of "
                            "elements or violated symmetry."};
                    }
                }
            }
            this->elements_ = {};
            for (size_type i = 0; i < dim_; ++i) {
                for (size_type j = 0; j < dim_; ++j) {
                    this->elements_.push_back(vec[i * dim_ + j]);
                }
            }
            return *this;
        }
        // Check if non-redundant vector was passed
        if (vec.size() == num_elements()) {
            this->elements_ = vec;
            return *this;
        }
        // If nothing returned yet, error!
        throw std::runtime_error{
            "CHAMPION::SymmetricMatrix: Cannot assign to SymmetricMatrix from "
            "given vector. Wrong number of elements."};
    }

    // Construct as copy from any symmetric type of matrix object.
    template <typename T2>
    SymmetricMatrix(const MatrixInterface<T2> &A) : dim_{A.num_rows()} {
        if (A.num_rows() != A.num_cols()) {
            throw std::runtime_error{"CHAMPION::SymmetricMatrix: Tried to "
                                     "construct symmetric matrix from "
                                     "copy of non-square matrix"};
        }
        if (N != 0 && A.num_rows() != N) {
            throw std::runtime_error{
                "CHAMPION::SquareMatrix: Dimension mismatch in constructor."};
        }
        this->elements_ = std::vector<T>(num_elements());
        for (size_type i = 0; i < number_of_rows; ++i) {
            for (size_type j = i + 1; j < number_of_cols; ++j) {
                (*this)(i, j) = A(i, j);
                if (A(j, i) != A(i, j)) {
                    throw std::runtime_error{
                        "CHAMPION::SymmetricMatrix: Tried to construct a "
                        "symmetric matrix from a copy of a non-symmetric "
                        "matrix."};
                }
            }
        }
    }

    virtual ~SymmetricMatrix() {}

    // Access
    size_type num_rows() const override { return dim_; }
    size_type num_cols() const override { return dim_; }

    // Modification

    auto transpose() const { return compute_transpose(*this); }

    auto trace() const { return compute_trace(*this); }
    auto determinant() const { return compute_determinant(*this); }
    auto inverse() const { return compute_inverse(*this); }

  private:
    // Private member functions
    size_type get_index(size_type row, size_type col) const override {
        if (row > col) {
            std::swap(row, col);
        }
        int index = 0;
        int stride = num_cols();
        for (size_type i = 0; i != row; ++i) {
            index += stride--;
            --col;
        }
        index += col;
        return index;
    }
    auto submatrix(size_type i, size_type j) const {
        return submatrix(*this, i, j);
    }
    size_type num_elements() { return dim_ * (dim_ + 1) / 2; }

    // Private data members
    size_type dim_;
};

// Sparse matrix implementation (nonsquare)
template <typename T, size_type N = 0, size_type M = 0>
class SparseMatrix : public SparseMatrixBase<T, N, M> {
  public:
    // Types
    using element_type = T;
    using matrix_type = SparseMatrix<T, N, M>;
    using transpose_type = SparseMatrix<T, M, N>;
    using inverse_type = void;
    using submatrix_type =
        SparseMatrix<T, Choose_value<size_type, (N == 0), 0, N - 1>,
                     Choose_value<size_type, (M == 0), 0, M - 1>>;
    // Traits
    static constexpr size_type number_of_rows = N;
    static constexpr size_type number_of_cols = M;
    static constexpr bool is_square = false;
    static constexpr bool is_symmetric = false;
    static constexpr bool is_sparse = true;
    // Default constructor
    SparseMatrix(T default_value = {})
        : number_of_rows_{N}, number_of_cols_{M} {
        this->default_value_ = default_value;
    }

    // Construct from shape and default value
    SparseMatrix(size_type number_of_rows, size_type number_of_cols,
                 T default_value = {})
        : number_of_rows_{N}, number_of_cols_{M} {
        this->default_value_ = default_value;
        if (N != 0 && number_of_rows != N) {
            throw std::runtime_error{
                "CHAMPION::SparseMatrix: Requested number of rows is not "
                "compatible with static type."};
        }
        if (M != 0 && number_of_cols != M) {
            throw std::runtime_error{
                "CHAMPION::SparseMatrix: Requested number of columns is not "
                "compatible with static type."};
        }
        number_of_rows_ = number_of_rows;
        number_of_cols_ = number_of_cols;
    }

    // Construct from shape, default value and map of values
    SparseMatrix(size_type number_of_rows, size_type number_of_cols,
                 T default_value,
                 std::map<std::pair<size_type, size_type>, T> elements)
        : number_of_rows_{N}, number_of_cols_{N} {
        if (N != 0 && number_of_rows != N) {
            throw std::runtime_error{"CHAMPION::SparseMatrix: "
                                     "Requested number of rows is not "
                                     "compatible with static type."};
        }
        if (N != 0 && number_of_cols != N) {
            throw std::runtime_error{"CHAMPION::SparseMatrix: "
                                     "Requested number of columns is not "
                                     "compatible with static type."};
        }
        number_of_rows_ = number_of_rows;
        number_of_cols_ = number_of_cols;
        this->default_value_ = default_value;
        for (const auto &el : elements) {
            auto i = el.first.first;
            auto j = el.first.second;
            if (i > this->num_rows() || j > this->num_cols()) {
                throw std::out_of_range{
                    "CHAMPION::SparseMatrix: Elements given to constructor "
                    "includes indices out of range."};
            }
        }
        this->elements_ = std::move(elements);
    }

    // Copy and move semantics
    SparseMatrix(const SparseMatrix &) = default;
    SparseMatrix &operator=(const SparseMatrix &) = default;
    SparseMatrix(SparseMatrix &&) = default;
    SparseMatrix &operator=(SparseMatrix &&) = default;

    // Construct as copy from any type of matrix with convertible
    // element_type
    template <typename T2>
    SparseMatrix(const MatrixInterface<T2> &A, T default_value = {})
        : number_of_rows_{N}, number_of_cols_{M} {
        if (N != 0 && A.num_rows() != N) {
            throw std::runtime_error{
                "CHAMPION::SparseMatrix: Dimension mismatch for "
                "rows in constructor."};
        }
        if (M != 0 && A.num_cols() != M) {
            throw std::runtime_error{
                "CHAMPION::SparseMatrix: Dimension mismatch for "
                "columns in constructor."};
        }
        this->elements_ = {};
        for (size_type i = 0; i != number_of_rows; ++i) {
            for (size_type j = 0; j != number_of_cols; ++j) {
                if (A(i, j) != this->default_value_) {
                    (*this)(i, j) = A(i, j);
                }
            }
        }
    }

    virtual ~SparseMatrix() {}

    // Access
    virtual size_type num_rows() const override { return number_of_rows_; }
    virtual size_type num_cols() const override { return number_of_cols_; }

    // Modification
    auto transpose() const { return compute_transpose(*this); }

  private:
    // Private member functions
    virtual void check_bounds(size_type &row, size_type &col) const override {
        if (row >= num_rows() || col >= num_cols()) {
            throw std::out_of_range{"CHAMPION::SparseMatrix: Tried to index "
                                    "elements beyond matrix dimensions."};
        }
    }

    // Private data members
    size_type number_of_rows_;
    size_type number_of_cols_;
};

template <typename T, size_type N = 0>
class SparseSquareMatrix : public SparseMatrixBase<T, N, N> {
  public:
    // Types
    using element_type = T;
    using matrix_type = SparseSquareMatrix<T, N>;
    using transpose_type = SparseSquareMatrix<T, N>;
    using inverse_type = SquareMatrix<T, N>;
    using submatrix_type =
        SparseSquareMatrix<T, Choose_value<size_type, N == 0, 0, N - 1>>;
    // Traits
    static constexpr size_type number_of_rows = N;
    static constexpr size_type number_of_cols = N;
    static constexpr bool is_square = true;
    static constexpr bool is_symmetric = false;
    static constexpr bool is_sparse = true;
    // Construct from default value (optional)
    SparseSquareMatrix(T default_value = {}) : dim_{N} {
        this->default_value_ = default_value;
    }
    // Construct from shape and default value
    SparseSquareMatrix(size_type number_of_rows, size_type number_of_cols,
                       T default_value = {})
        : dim_{N} {
        if (number_of_rows != number_of_cols) {
            throw std::runtime_error{
                "CHAMPION::SparseSquareMatrix: Tried to construct square "
                "matrix witn unequal number of rows and columns."};
        }
        if (N != 0 && number_of_rows != N) {
            throw std::runtime_error{
                "CHAMPION::SparseSquareMatrix: Requested number of rows is not "
                "compatible with static type."};
        }
        this->default_value_ = default_value;
        dim_ = number_of_rows;
    }
    // Construct from shape, default value and map of values
    SparseSquareMatrix(size_type number_of_rows, size_type number_of_cols,
                       T default_value,
                       std::map<std::pair<size_type, size_type>, T> elements)
        : dim_{N} {
        if (number_of_rows != number_of_cols) {
            throw std::runtime_error{
                "CHAMPION::SparseSquareMatrix: Tried to construct "
                "symmetric "
                "matrix witn unequal number of rows and columns."};
        }
        if (N != 0 && number_of_rows != N) {
            throw std::runtime_error{"CHAMPION::SparseSquareMatrix: "
                                     "Requested number of rows is not "
                                     "compatible with static type."};
        }
        dim_ = number_of_rows;
        this->default_value_ = default_value;
        for (const auto &el : elements) {
            auto i = el.first.first;
            auto j = el.first.second;
            if (i > this->num_rows() || j > this->num_cols()) {
                throw std::out_of_range{
                    "CHAMPION::SparseSquareMatrix: Elements given to "
                    "constructor "
                    "includes indices out of range."};
            }
        }
        this->elements_ = std::move(elements);
    }
    // Construct as copy from any type of matrix
    template <typename T2>
    SparseSquareMatrix(const MatrixInterface<T2> &A, T default_value = {})
        : dim_{A.num_rows()} {
        this->default_value_ = default_value;
        if (A.num_rows() != number_of_rows || A.num_cols() != number_of_cols) {
            throw std::runtime_error{"CHAMPION::SparseSquareMatrix: Dimension "
                                     "mismatch in constructor."};
        }
        this->elements_ = {};
        for (size_type i = 0; i != number_of_rows; ++i) {
            for (size_type j = 0; j != number_of_cols; ++j) {
                if (A(i, j) != this->default_value_) {
                    (*this)(i, j) = A(i, j);
                }
            }
        }
    }

    // Copy and move semantics
    SparseSquareMatrix(const SparseSquareMatrix &) = default;
    SparseSquareMatrix &operator=(const SparseSquareMatrix &) = default;
    SparseSquareMatrix(SparseSquareMatrix &&) = default;
    SparseSquareMatrix &operator=(SparseSquareMatrix &&) = default;

    virtual ~SparseSquareMatrix() {}

    // Public access
    size_type num_rows() const override { return dim_; }
    size_type num_cols() const override { return dim_; }

    // Modification

    auto transpose() const { return compute_transpose(*this); }

    auto trace() const { return compute_trace(*this); }
    auto determinant() const { return compute_determinant(*this); }
    auto inverse() const { return compute_inverse(*this); }

  private:
    // Private member functions
    virtual void check_bounds(size_type &row, size_type &col) const override {
        if (row >= num_rows() || col >= num_cols()) {
            throw std::out_of_range{"CHAMPION::SparseMatrix: Tried to index "
                                    "elements beyond matrix dimensions."};
        }
    }
    auto submatrix(size_type i, size_type j) const {
        return compute_submatrix(*this, i, j);
    }

    // Private data members
    size_type dim_;
};

// Sparse matrix implementation of symmetric matrices
template <typename T, size_type N = 0>
class SparseSymmetricMatrix : public SparseSquareMatrix<T, N> {
  public:
    // Types
    using element_type = T;
    using matrix_type = SparseSymmetricMatrix<T, N>;
    using transpose_type = SparseSymmetricMatrix<T, N>;
    using inverse_type = SymmetricMatrix<T, N>;
    using submatrix_type =
        SparseSquareMatrix<T, Choose_value<size_type, N == 0, 0, N - 1>>;
    // Traits
    static constexpr size_type number_of_rows = N;
    static constexpr size_type number_of_cols = N;
    static constexpr bool is_square = true;
    static constexpr bool is_symmetric = true;
    static constexpr bool is_sparse = true;
    // Default constructor
    SparseSymmetricMatrix(T default_value = 0) : dim_{N} {
        this->default_value_ = default_value;
    }
    // Construct from shape and default value
    SparseSymmetricMatrix(size_type number_of_rows, size_type number_of_cols,
                          T default_value = {})
        : dim_{N} {
        if (number_of_rows != number_of_cols) {
            throw std::runtime_error{
                "CHAMPION::SparseSymmetriceMatrix: Tried to construct "
                "symmetric "
                "matrix witn unequal number of rows and columns."};
        }
        if (N != 0 && number_of_rows != N) {
            throw std::runtime_error{"CHAMPION::SparseSymmetricMatrix: "
                                     "Requested number of rows is not "
                                     "compatible with static type."};
        }
        dim_ = number_of_rows;
        this->default_value_ = default_value;
    }
    // Construct from shape, default value and map of values
    SparseSymmetricMatrix(size_type number_of_rows, size_type number_of_cols,
                          T default_value,
                          std::map<std::pair<size_type, size_type>, T> elements)
        : dim_{N} {
        if (number_of_rows != number_of_cols) {
            throw std::runtime_error{
                "CHAMPION::SparseSymmetriceMatrix: Tried to construct "
                "symmetric "
                "matrix witn unequal number of rows and columns."};
        }
        if (N != 0 && number_of_rows != N) {
            throw std::runtime_error{"CHAMPION::SparseSymmetricMatrix: "
                                     "Requested number of rows is not "
                                     "compatible with static type."};
        }
        dim_ = number_of_rows;
        this->default_value_ = default_value;
        for (auto &el : elements) {
            auto i = el.first.first;
            auto j = el.first.second;
            if (i > j) {
                throw std::out_of_range{
                    "CHAMPION::SparseSymmetricMatrix: Map of elements given to "
                    "constructor can only contain keys with (i, j) where j >= "
                    "i."};
            }
            if (i > this->num_rows() || j > this->num_cols()) {
                throw std::out_of_range{"CHAMPION::SparseSymmetricMatrix: "
                                        "Elements given to constructor "
                                        "includes indices out of range."};
            }
        }
        this->elements_ = std::move(elements);
    }
    // Construct as copy from any type of matrix
    template <typename T2>
    SparseSymmetricMatrix(const MatrixInterface<T2> &A, T default_value = {})
        : dim_{A.num_rows()} {
        if (A.num_rows() != A.num_cols()) {
            throw std::runtime_error{"CHAMPION::SparseSymmetricMatrix: Tried "
                                     "to construct square matrix from "
                                     "copy of non-square matrix"};
        }
        if (N != 0 && A.num_rows() != N) {
            throw std::runtime_error{"CHAMPION::SparseSymmetricMatrix: "
                                     "Dimension mismatch in constructor."};
        }
        this->default_value_ = default_value;
        this->elements_ = {};
        for (size_type i = 0; i != number_of_rows; ++i) {
            for (size_type j = 0; j != number_of_cols; ++j) {
                if (A(i, j) != this->default_value_) {
                    (*this)(i, j) = A(i, j);
                    if (A(j, i) != A(i, j)) {
                        throw std::runtime_error{
                            "CHAMPION::SparseSymmetricMatrix: Tried to "
                            "initialize symmetric matrix from copy of "
                            "non-symmetric matrix."};
                    }
                }
            }
        }
    }

    // Copy and move semantics
    SparseSymmetricMatrix(const SparseSymmetricMatrix &) = default;
    SparseSymmetricMatrix &operator=(const SparseSymmetricMatrix &) = default;
    SparseSymmetricMatrix(SparseSymmetricMatrix &&) = default;
    SparseSymmetricMatrix &operator=(SparseSymmetricMatrix &&) = default;

    virtual ~SparseSymmetricMatrix() {}

    virtual size_type num_rows() const override { return dim_; }
    virtual size_type num_cols() const override { return dim_; }

    // Modification

    auto transpose() const { return compute_transpose(*this); }

    auto trace() const { return compute_trace(*this); }
    auto determinant() const { return compute_determinant(*this); }
    auto inverse() const { return compute_inverse(*this); }

  private:
    // Private member functions

    // Performs bound checking and swaps indices if i > j, in order to avoid
    // storage overhead
    virtual void check_bounds(size_type &i, size_type &j) const override {
        if (i >= num_rows() || j >= num_cols()) {
            std::cout << "Debug_info: i = " << i << ", j = " << j
                      << ", dims = (" << this->num_rows() << " x "
                      << this->num_cols() << std::endl;
            throw std::out_of_range{"CHAMPION::SparseSymmetricMatrix: Tried to "
                                    "index elements beyond matrix dimensions."};
        }
        if (i > j) {
            std::swap(i, j);
        }
    }
    auto submatrix(size_type i, size_type j) const {
        return submatrix(*this, i, j);
    }

    // Private data members
    size_type dim_;
};

/************************** Function definitions *****************************/

template <typename MatrixType>
std::enable_if_t<(Is_matrix<MatrixType>), typename MatrixType::matrix_type>
operator-(const MatrixType &A) {
    MatrixType B;
    for (size_type i = 0; i != A.num_rows(); ++i) {
        size_type j = MatrixType::is_symmetric ? i + 1 : 0;
        for (j = 0; j != A.num_cols(); ++j) {
            B(i, j) = -A(i, j);
        }
    }
    return B;
}

// Scaling

// Changes values of current matrix in-place
template <typename MatrixType, typename T>
std::enable_if_t<(Is_matrix<MatrixType> && !Is_matrix<T>), MatrixType>
operator*=(MatrixType &A, const T &x) {
    for (size_type i = 0; i != A.num_rows(); ++i) {
        size_type j = MatrixType::is_symmetric ? i + 1 : 0;
        for (j = 0; j != A.num_cols(); ++j) {
            A(i, j) *= x;
        }
    }
    return A;
}

template <typename MatrixType, typename T>
std::enable_if_t<(Is_matrix<MatrixType> && !Is_matrix<T>), MatrixType> &
operator/=(MatrixType &A, const T &x) {
    for (size_type i = 0; i != A.num_rows(); ++i) {
        size_type j = MatrixType::is_symmetric ? i : 0;
        for (; j != A.num_cols(); ++j) {
            A(i, j) /= x;
        }
    }
    return A;
}

// Returns a new matrix with element_type reflecting the result of
// elementwise multiplication
template <typename MatrixType, typename T>
typename std::enable_if_t<(Is_matrix<MatrixType> && !Is_matrix<T>),
                          scaled_matrix_type<MatrixType, T>>::type
operator*(const MatrixType &A, const T &x) {
    typename scaled_matrix_type<MatrixType, T>::type B = A;
    for (size_type i = 0; i != B.num_rows(); ++i) {
        size_type j = MatrixType::is_symmetric ? i + 1 : 0;
        for (j = 0; j != B.num_cols(); ++j) {
            B(i, j) *= x;
        }
    }
    return B;
}

template <typename MatrixType, typename T>
typename std::enable_if_t<(Is_matrix<MatrixType> && !Is_matrix<T>),
                          scaled_matrix_type<MatrixType, T>>::type
operator*(const T &x, const MatrixType &A) {
    return A * x;
}

template <typename MatrixType, typename T>
typename std::enable_if_t<(Is_matrix<MatrixType> && !Is_matrix<T>),
                          scaled_matrix_type<MatrixType, T>>::type
operator/(const MatrixType &A, const T &x) {
    return A * (1.0 / x);
}

// Matrix addition and subtraction

template <typename Matrix1, typename Matrix2>
typename std::enable_if_t<(Is_matrix<Matrix1> && Is_matrix<Matrix2>),
                          matrix_sum_type<Matrix1, Matrix2>>::type
operator+(const Matrix1 &A, const Matrix2 &B) {
    typename matrix_sum_type<Matrix1, Matrix2>::type C{};
    // Perform run-time checks if necessary
    if constexpr (C.number_of_rows == 0) {
        if (A.num_rows() != B.num_rows()) {
            throw std::runtime_error{"CHAMPION::Matrix: Dimension mismatch in "
                                     "matrix addition (rows)."};
        }
    }
    if constexpr (C.number_of_cols == 0) {
        if (A.num_cols() != B.num_cols()) {
            throw std::runtime_error{"CHAMPION::Matrix: Dimension mismatch in "
                                     "matrix addition (columns)."};
        }
    }
    for (size_type i = 0; i != C.num_rows(); ++i) {
        size_type j = C.is_symmetric ? i + 1 : 0;
        for (j = 0; j != C.num_cols(); ++j) {
            C(i, j) = A(i, j) + B(i, j);
        }
    }
    return C;
}

template <typename Matrix1, typename Matrix2>
typename std::enable_if_t<(Is_matrix<Matrix1> && Is_matrix<Matrix2>),
                          matrix_sum_type<Matrix1, Matrix2>>::type
operator-(const Matrix1 &A, const Matrix2 &B) {
    typename matrix_sum_type<Matrix1, Matrix2>::type C{};
    C = A + -B;
    return C;
}

// Matrix multiplication

template <typename Matrix1, typename Matrix2>
typename std::enable_if_t<(Is_matrix<Matrix1> && Is_matrix<Matrix2>),
                          matrix_product_type<Matrix1, Matrix2>>::type
operator*(const Matrix1 &A, const Matrix2 &B) {
    typename matrix_product_type<Matrix1, Matrix2>::type C(A.num_rows(),
                                                           B.num_cols());
    for (size_type i = 0; i != A.num_rows(); ++i) {
        size_type j = C.is_symmetric ? i + 1 : 0;
        for (j = 0; j != B.num_cols(); ++j) {
            for (size_type k = 0; k != A.num_cols(); ++k) {
                C(i, j) += A(i, k) * B(k, j);
            }
        }
    }
    return C;
}

// Logical operators
template <typename T1, typename T2>
bool operator==(const MatrixInterface<T1> &A, const MatrixInterface<T2> &B) {
    if (A.num_rows() != B.num_rows() || A.num_cols() != B.num_cols()) {
        return false;
    }
    for (size_type i = 0; i != A.num_rows(); ++i) {
        for (size_type j = 0; j != A.num_cols(); ++j) {
            if (A(i, j) != B(i, j)) {
                return false;
            }
        }
    }
    return true;
}

template <typename T1, typename T2>
bool operator!=(const MatrixInterface<T1> &A, const MatrixInterface<T2> &B) {
    return !(A == B);
}

// I/O

template <typename T>
std::ostream &operator<<(std::ostream &os, const MatrixInterface<T> &M) {
    constexpr size_type sep_width = 2;
    constexpr size_type lmargin = 1;
    constexpr size_type rmargin = 1;
    auto element_widths = Matrix<size_type>(M.num_rows(), M.num_cols());
    for (size_type i = 0; i != M.num_rows(); ++i) {
        for (size_type j = 0; j != M.num_cols(); ++j) {
            auto ss = std::stringstream{};
            ss << M(i, j);
            auto str = ss.str();
            element_widths(i, j) = str.size();
        }
    }
    auto max_col_widths = std::vector<size_type>(element_widths.num_cols(), 0);
    for (size_type j = 0; j != element_widths.num_cols(); ++j) {
        for (size_type i = 0; i != element_widths.num_rows(); ++i) {
            max_col_widths[j] =
                std::max(max_col_widths[j], element_widths(i, j));
        }
    }
    for (size_type i = 0; i != M.num_rows(); ++i) {
        os << '|' << std::string(lmargin, ' ');
        for (size_type j = 0; j < M.num_cols() - 1; ++j) {
            size_type padding = max_col_widths[j] - element_widths(i, j);
            os << std::string(padding, ' ') << M(i, j)
               << std::string(sep_width, ' ');
        }
        auto j = M.num_cols() - 1;
        size_type padding = max_col_widths[j] - element_widths(i, j);
        os << std::string(padding, ' ') << M(i, j) << std::string(rmargin, ' ')
           << "|\n";
    }
    return os;
}

} // namespace champion

#endif