/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: GraphTree.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:
Graph class template that holds data associated with each vertex (e.g. chemical
species in a chemical bonding graph). Maintains graphs in a uniquely sorted
state unless explicitly told not to by passing sort_on = false to constructor,
in which case graph.sort() can be called at a later point.
The unique sorting takes into account both vertex values and position in
graph. Supports both equality testing and lexicographical ordering of graphs.

\******************************************************************************/

#ifndef CHAMPION_GRAPH_HPP
#define CHAMPION_GRAPH_HPP

#include <algorithm> // for sort, swap, find
#include <list>
#include <map>
#include <memory>  // for smart pointers
#include <numeric> // for iota
#include <queue>
#include <vector>

#include "Edge.hpp"
#include "algorithm.hpp"
#include "set_precision.hpp"
#include "sort.hpp"

namespace champion {

/***************************** Class declarations *****************************/

// Forward declarations
template <typename vertex_type> class Graph;

template <typename vertex_type, typename... Ts>
void add_args_to_graph(Graph<vertex_type> &g, Ts... Args);

void sort_edges(std::vector<Edge> &edges);

// Exceptions

class edge_index_too_large_error : public std::runtime_error {
    std::string msg =
        "Edge indices may not be larger than vertices.size() - 1.\n";

  public:
    edge_index_too_large_error(const std::string &arg)
        : std::runtime_error(arg) {
        msg += arg;
    }
    const char *what() const throw() { return msg.c_str(); }
};

// Graph class template

template <class vertex_type> class Graph {
  public:
    using iterator = typename std::vector<vertex_type>::iterator;
    using const_iterator = typename std::vector<vertex_type>::const_iterator;

    // Default constructor
    Graph() : vertices_{}, edges_{} {}

    // Construct from single vertex
    Graph(vertex_type vertex) : vertices_{vertex}, edges_{} {}

    // Construct from vectors of vertices and edges
    Graph(std::vector<vertex_type> vertices, std::vector<Edge> edges = {},
          bool sort_on = true)
        : vertices_{vertices}, edges_{edges} {
        if (sort_on) {
            sort();
        }
    }

    // Construct from variadic initializer list of vertices and edges
    // (can be disordered but why on earth would you want that?)
    template <typename... Ts> Graph(Ts... Args, bool sort_on = true) {
        add_args_to_graph(*this, Args..., sort_on);
    }

    // Access
    size_type size() const { return vertices_.size(); }
    size_type no_edges() const { return edges_.size(); }
    iterator begin() { return vertices_.begin(); }
    iterator end() { return vertices_.end(); }
    const_iterator begin() const { return vertices_.cbegin(); }
    const_iterator end() const { return vertices_.cend(); }
    const_iterator cbegin() const { return vertices_.cbegin(); }
    const_iterator cend() const { return vertices_.cend(); }
    const vertex_type &operator[](size_type i) const { return vertices_[i]; }
    vertex_type &operator[](size_type i) { return vertices_[i]; }

    const std::vector<vertex_type> &get_vertices() const { return vertices_; }
    vertex_type get_vertex(size_type i) const { return vertices_[i]; }
    auto get_edges() const { return edges_; }
    std::vector<size_type> get_neighbor_indices(size_type i) const;
    std::vector<size_type> get_neighbor_indices(size_type i, size_type d) const;
    std::vector<vertex_type> get_neighbors(size_type i, size_type d = 1) const;

    // Get the indices of the leaf vertices, i.e. those who have exactly one
    // connection to another vertex
    std::vector<size_type> get_leaves() const;

    // Return Graph built from selected vertices v in *this
    Graph<vertex_type> get_subgraph(std::vector<vertex_type> v) const;
    Graph<vertex_type> get_subgraph(std::vector<size_type> v) const;

    // Modification
    // Composition of disjoint graphs
    Graph operator+=(const Graph &g2);

    void check_edge(Edge edge) const;

    // Helper function for comparing vertices. Return -1 if v1 < v2, 1 if v1 >
    // v2 and 0 if v1 == v2
    int compare_vertices(const vertex_type &v1, const vertex_type &v2) const {
        assert(&v1 >= &vertices_.front() && &v1 <= &vertices_.back());
        assert(&v2 >= &vertices_.front() && &v2 <= &vertices_.back());
        if (v1 < v2) {
            return -1;
        } else if (v1 > v2) {
            return 1;
        }
        auto i1 = &v1 - &vertices_.front();
        auto i2 = &v2 - &vertices_.front();
        std::vector<Edge> relabeled_edges = this->edges_;
        for (auto &edge : relabeled_edges) {
            if (edge.first == i1) {
                edge.first = i2;
            } else if (edge.first == i2) {
                edge.first = i1;
            }
            if (edge.second == i1) {
                edge.second = i2;
            } else if (edge.second == i2) {
                edge.second = i1;
            }
        }
        ::champion::sort_edges(relabeled_edges);
        if (this->edges_ < relabeled_edges) {
            return -1;
        } else if (this->edges_ > relabeled_edges) {
            return 1;
        }
        return 0;
    }

    void swap(vertex_type &v1, vertex_type &v2,
              std::vector<size_type> *key = nullptr) {
        assert(&v1 >= &vertices_.front() && &v1 <= &vertices_.back());
        assert(&v2 >= &vertices_.front() && &v2 <= &vertices_.back());
        assert(key->size() == this->size() || key->size() == 0);
        std::swap(v1, v2);
        auto i1 = &v1 - &vertices_.front();
        auto i2 = &v2 - &vertices_.front();
        if (key->size() == this->size()) {
            std::swap((*key)[i1], (*key)[i2]);
        }
        for (auto &edge : this->edges_) {
            if (edge.first == i1) {
                edge.first = i2;
            } else if (edge.first == i2) {
                edge.first = i1;
            }
            if (edge.second == i1) {
                edge.second = i2;
            } else if (edge.second == i2) {
                edge.second = i1;
            }
        }
        this->sort_edges();
    }

    std::vector<size_type> sort() {
        auto key = std::vector<size_type>(this->size());
        std::iota(key.begin(), key.end(), 0);
        // First sort by vertex values.
        ::champion::sort(this->begin(), this->end(), std::less<vertex_type &>{},
                         [this, &key](vertex_type &v1, vertex_type &v2) {
                             this->swap(v1, v2, &key);
                         });
        // Now find the permutation that yields the lowest possible value for
        // edges_.
        auto begin = this->begin();
        while (begin < this->end()) {
            auto end = std::find_if(begin, this->end(),
                                    [begin](auto &v) { return v != *begin; });
            size_type no_equivalent = end - begin;
            auto indices = std::vector<size_type>(no_equivalent, 0);
            std::iota(indices.begin(), indices.end(), begin - this->begin());
            auto best_graph = *this;
            auto best_key = key;
            auto perm = indices;
            while (std::next_permutation(perm.begin(), perm.end())) {
                auto permuted_graph = *this;
                auto trial_key = key;
                for (size_type i = 0; i < no_equivalent; ++i) {
                    auto from = indices[i];
                    auto to = perm[i];
                    trial_key[from] = key[to];
                    for (size_type e = 0; e < this->edges_.size(); ++e) {
                        if (this->edges_[e].first == from) {
                            permuted_graph.edges_[e].first = to;
                        } else if (this->edges_[e].second == from) {
                            permuted_graph.edges_[e].second = to;
                        }
                    }
                }
                permuted_graph.sort_edges();
                if (permuted_graph < best_graph) {
                    best_graph = permuted_graph;
                    best_key = trial_key;
                }
            }
            *this = best_graph;
            key = best_key;
            begin += no_equivalent;
        }
        // "Invert" the key so that it can be used in sort_consistently to
        // reorder containers consistently with the graph
        auto key_mold = key;
        std::iota(key.begin(), key.end(), 0);
        sort_consistently(key_mold, key);
        return key;
    }

    // Finds all non-reentrant paths of length length in the graph
    std::vector<std::vector<size_type>> find_chains(size_type length) const;
    // Finds all star vertices (and the arms of the stars) with num_arms
    // arms. Returns them in a vector of indices where the first is the
    // center vertex and the rest are sorted by indices
    std::vector<std::vector<size_type>> find_stars(size_type num_arms) const;

    // Find the connected components of *this
    std::vector<std::unique_ptr<Graph<vertex_type>>>
    connected_components(bool sort_graphs = true) const;

    void add_vertex(vertex_type vertex, bool sort_on);
    void add_edge(Edge edge) {
        check_edge(edge);
        edges_.push_back(edge);
        sort_edges();
    }

  private:
    // Private member functions
    std::vector<std::vector<size_type>>
    add_one(std::vector<std::vector<size_type>> beginnings,
            std::vector<std::vector<size_type>> neighbors) const;
    void remove_reverse(std::vector<std::vector<size_type>> &chains) const;

    void add_edge(int a, int b) { add_edge(Edge(a, b)); }

    Graph build_subgraph(std::vector<vertex_type> selected_vertices,
                         std::vector<size_type> selected_indices) const;

    // Sort the edges wrt the vertices
    void sort_edges();

    // Private data members
    std::vector<vertex_type> vertices_;
    std::vector<Edge> edges_;
};

/*************************** Function definitions *****************************/

// Composition of disjoint graphs
template <typename vertex_type>
Graph<vertex_type> Graph<vertex_type>::
operator+=(const Graph<vertex_type> &g2) {
    auto N = this->size();
    for (auto v : g2.vertices_) {
        vertices_.push_back(v);
    }
    for (auto e : g2.edges_) {
        edges_.push_back({e.first + N, e.second + N});
    }
    return *this;
}

template <typename vertex_type>
void Graph<vertex_type>::add_vertex(vertex_type vertex, bool sort_on) {
    vertices_.push_back(vertex);
    if (sort_on)
        sort();
}

template <typename vertex_type>
Graph<vertex_type> Graph<vertex_type>::get_subgraph(
    std::vector<vertex_type> selected_vertices) const {
    auto N = selected_vertices.size();
    auto selected_indices = std::vector<size_type>(N, 0);
    for (size_type i = 0; i != N; ++i) {
        selected_indices[i] =
            std::find(this->vertices_.begin(), this->vertices_.end(),
                      selected_vertices[i]) -
            this->vertices_.begin();
        assert(selected_indices[i] < this->vertices_.size());
    }
    return build_subgraph(selected_vertices, selected_indices);
}

template <typename vertex_type>
Graph<vertex_type> Graph<vertex_type>::get_subgraph(
    std::vector<size_type> selected_indices) const {
    auto selected_vertices = std::vector<vertex_type>();
    for (auto i : selected_indices) {
        selected_vertices.push_back(this->vertices_[i]);
    }
    return build_subgraph(selected_vertices, selected_indices);
}

template <typename vertex_type>
Graph<vertex_type> Graph<vertex_type>::build_subgraph(
    std::vector<vertex_type> selected_vertices,
    std::vector<size_type> selected_indices) const {
    // Construct new edge vector
    // Include and relabel all edge vectors between vertices included in the
    // subgraph
    auto new_edges = std::vector<std::pair<size_type, size_type>>();
    for (auto ei : this->edges_) {
        auto ei_first_it = std::find(selected_indices.begin(),
                                     selected_indices.end(), ei.first);
        auto ei_second_it = std::find(selected_indices.begin(),
                                      selected_indices.end(), ei.second);
        if (ei_first_it != selected_indices.end() &&
            ei_second_it != selected_indices.end()) {
            auto ei_first = ei_first_it - selected_indices.begin();
            auto ei_second = ei_second_it - selected_indices.begin();
            new_edges.push_back({ei_first, ei_second});
        }
    }
    // Construct and return subgraph
    return Graph<vertex_type>(selected_vertices, new_edges);
}

template <typename vertex_type>
void Graph<vertex_type>::check_edge(Edge edge) const {
    auto max = vertices_.size() - 1;
    if (edge.first > max || edge.second > max) {
        std::string err_str =
            "number of vertices_: " + std::to_string(vertices_.size()) +
            ", first: " + std::to_string(edge.first) +
            ", second: " + std::to_string(edge.second);
        throw edge_index_too_large_error(err_str);
    }
}

template <typename vertex_type>
std::vector<size_type>
Graph<vertex_type>::get_neighbor_indices(size_type i) const {
    std::vector<size_type> neighbors;
    for (auto e : edges_) {
        if (e.first == i) {
            neighbors.push_back(e.second);
        } else if (e.second == i) {
            neighbors.push_back(e.first);
        }
    }
    return neighbors;
}

template <typename vertex_type>
std::vector<size_type>
Graph<vertex_type>::get_neighbor_indices(size_type i, size_type d) const {
    auto neighbors = get_neighbor_indices(i);
    if (d == 1) {
        return neighbors;
    } else if (d > 1) {
        for (auto j : neighbors) {
            auto current_neighbors = get_neighbor_indices(j, d - 1);
            neighbors.insert(neighbors.end(), current_neighbors.begin(),
                             current_neighbors.end());
        }
    }
    remove_duplicates(neighbors);
    return neighbors;
}

template <typename vertex_type>
std::vector<vertex_type> Graph<vertex_type>::get_neighbors(size_type i,
                                                           size_type d) const {
    auto neighbor_indices = get_neighbor_indices(i, d);
    auto neighbors = std::vector<vertex_type>{};
    for (auto i : neighbor_indices) {
        neighbors.push_back(vertices_[i]);
    }
    return neighbors;
}

template <typename vertex_type>
std::vector<size_type> Graph<vertex_type>::get_leaves() const {
    auto edge_counts = std::vector<size_type>(this->size(), 0);
    for (auto e : edges_) {
        ++edge_counts[e.first];
        ++edge_counts[e.second];
    }
    auto leaves = std::vector<size_type>{};
    for (size_type i = 0; i != this->size(); ++i) {
        if (edge_counts[i] == 1) {
            leaves.push_back(i);
        }
    }
    return leaves;
}

template <typename vertex_type>
std::vector<std::vector<size_type>>
Graph<vertex_type>::find_chains(size_type length) const {
    auto N = this->size();
    auto chains = std::vector<std::vector<size_type>>(N, {0});
    for (size_type i = 0; i != N; ++i) {
        chains[i][0] = i;
    }
    auto neighbors = std::vector<std::vector<size_type>>(N);
    for (size_type i = 0; i != N; ++i) {
        neighbors[i] = get_neighbor_indices(i);
    }
    for (size_type l = 2; l <= length; ++l) {
        chains = add_one(chains, neighbors);
    }
    remove_reverse(chains);
    return chains;
}

template <typename vertex_type>
std::vector<std::vector<size_type>> Graph<vertex_type>::add_one(
    std::vector<std::vector<size_type>> beginnings,
    std::vector<std::vector<size_type>> neighbors) const {
    auto chains = std::vector<std::vector<size_type>>{};
    for (auto bi : beginnings) {
        for (auto nj : neighbors[bi[bi.size() - 1]]) {
            if (std::find(bi.begin(), bi.end(), nj) == bi.end()) {
                auto ci = bi;
                auto new_chain = ci;
                ci.push_back(nj);
                chains.push_back(ci);
            }
        }
    }
    return chains;
}

template <typename vertex_type>
void Graph<vertex_type>::remove_reverse(
    std::vector<std::vector<size_type>> &chains) const {
    for (size_type i = 0; i != chains.size(); ++i) {
        for (size_type j = chains.size() - 1; j > i; --j) {
            if (is_reverse(chains[i], chains[j])) {
                if (chains[j] < chains[j]) {
                    chains[i].swap(chains[j]);
                }
                chains.erase(chains.begin() + j);
            }
        }
    }
}

template <typename vertex_type>
std::vector<std::vector<size_type>>
Graph<vertex_type>::find_stars(size_type num_arms) const {
    auto N = this->size();
    auto neighbors = std::vector<std::vector<size_type>>(N);
    for (size_type i = 0; i != N; ++i) {
        neighbors[i] = get_neighbor_indices(i);
    }
    auto stars = std::vector<std::vector<size_type>>();
    size_type star_index = 0;
    for (size_type i = 0; i != N; ++i) {
        auto groups = pick(neighbors[i], num_arms);
        if (!groups.empty()) {
            for (auto &group : groups) {
                stars.push_back({});
                auto new_star = std::vector<size_type>{i};
                stars[star_index] = new_star;
                for (auto arm : group) {
                    stars[star_index].push_back(arm);
                }
                ++star_index;
            }
        }
    }
    return stars;
}

// Sorts edges wrt vertices and enforces that each edge index refers to an
// existing vertex
void sort_edges(std::vector<Edge> &edges) {
    // Sort each edge so that its first holds the index of the earliest
    // sorted vertex
    for (auto &edge : edges) {
        if (edge.second < edge.first) {
            std::swap(edge.first, edge.second);
        }
    }
    // Sort the edge list based on vertices (1: first, 2: second)
    std::sort(edges.begin(), edges.end(),
              [](const Edge &e1, const Edge &e2) { return e1 < e2; });
}

template <typename vertex_type> void Graph<vertex_type>::sort_edges() {
    ::champion::sort_edges(this->edges_);
}

/* Returns a vector of mutually unconnected but internally connected
component graphs. Uses breadth-first-search to explore graph.

 Requirements: Assumes that all vertices in the graph to be partitioned are
unique.
*/
template <typename vertex_type>
std::vector<std::unique_ptr<Graph<vertex_type>>>
Graph<vertex_type>::connected_components(bool sort_graphs) const {
    using Graph_ptr = std::unique_ptr<Graph<vertex_type>>;
    std::vector<Graph_ptr> components;
    std::queue<size_type> visit_queue;
    std::list<size_type> unvisited_list(vertices_.size());
    std::iota(unvisited_list.begin(), unvisited_list.end(), 0);
    while (!unvisited_list.empty()) {
        auto g = std::make_unique<Graph<vertex_type>>();
        // Seed the empty visit queue with the first unvisited vertex
        visit_queue.push(unvisited_list.front());
        // Add the seed vertex to the new component graph
        g->add_vertex(vertices_[unvisited_list.front()], false);
        // Index in the current component graph of the latest discovered
        // vertex
        size_type latest_addition = 0;
        // Index in the current component of the current vertex
        size_type vertex_no = 0;
        while (!visit_queue.empty()) {
            // Index in the global graph of the current vertex
            auto v_current = visit_queue.front();
            // Remove from queue
            visit_queue.pop();
            // Find and traverse all edges adjacent to current vertex, add
            // the found vertices and edges to the new graph and push the
            // found vertices to visit_queue
            for (const auto &e : edges_) {
                size_type new_neighbor = this->size();
                if (v_current == e.first) {
                    new_neighbor = e.second;
                } else if (v_current == e.second) {
                    new_neighbor = e.first;
                }

                if (new_neighbor < this->size()) {
                    // if new_neighbor is not already in *g, add both vertex
                    // and edge and add to visit queue
                    auto match_it =
                        std::find(g->begin(), g->end(), (*this)[new_neighbor]);
                    size_type match_index = match_it - g->begin();
                    if (match_index == g->size()) {
                        g->add_vertex(vertices_[new_neighbor], false);
                        g->add_edge(vertex_no, ++latest_addition);
                        visit_queue.push(new_neighbor);
                    } else {
                        // if new_neighbor already in *g, add only edge if
                        // not present
                        Edge match_edge;
                        if (match_index < vertex_no) {
                            match_edge = {match_index, vertex_no};
                        } else {
                            match_edge = {vertex_no, match_index};
                        }
                        if (std::find(g->edges_.begin(), g->edges_.end(),
                                      match_edge) == g->edges_.end()) {
                            g->edges_.push_back(match_edge);
                        }
                    }
                }
            }
            // Remove visited vertex from unvisited_list
            unvisited_list.erase(std::find(unvisited_list.begin(),
                                           unvisited_list.end(), v_current));
            ++vertex_no;
        }
        if (sort_graphs) {
            g->sort();
        }
        components.push_back(std::move(g));
    }

    std::sort(
        components.begin(), components.end(),
        [](const Graph_ptr &gp1, const Graph_ptr &gp2) { return *gp1 < *gp2; });
    return components;
}
int compare_edge_lists(const std::vector<Edge> &a, const std::vector<Edge> &b) {
    auto a_sz = a.size();
    auto b_sz = b.size();
    auto sz = std::min(a_sz, b_sz);
    for (size_type i = 0; i < sz; ++i) {
        if (a[i].first < b[i].first) {
            return -1;
        } else if (a[i].first > b[i].first) {
            return 1;
        }
        if (a[i].second < b[i].second) {
            return -1;
        } else if (a[i].second > b[i].second) {
            return 1;
        }
    }
    if (a_sz < b_sz) {
        return -1;
    } else if (a_sz > b_sz) {
        return 1;
    } else {
        return 0;
    }
}

bool operator==(const std::vector<Edge> &a, const std::vector<Edge> &b) {
    return compare_edge_lists(a, b) == 0;
}

bool operator!=(const std::vector<Edge> &a, const std::vector<Edge> &b) {
    return compare_edge_lists(a, b) != 0;
}

bool operator<(const std::vector<Edge> &a, const std::vector<Edge> &b) {
    return compare_edge_lists(a, b) < 0;
}

bool operator>(const std::vector<Edge> &a, const std::vector<Edge> &b) {
    return compare_edge_lists(a, b) > 0;
}

bool operator<=(const std::vector<Edge> &a, const std::vector<Edge> &b) {
    return compare_edge_lists(a, b) <= 0;
}

bool operator>=(const std::vector<Edge> &a, const std::vector<Edge> &b) {
    return compare_edge_lists(a, b) >= 0;
}

template <typename vertex_type>
int compare_graphs(const Graph<vertex_type> &g1, const Graph<vertex_type> &g2) {
    if (g1.get_vertices() < g2.get_vertices()) {
        return -1;
    } else if (g1.get_vertices() > g2.get_vertices()) {
        return 1;
    }
    if (g1.get_edges() < g2.get_edges()) {
        return -1;
    } else if (g1.get_edges() > g2.get_edges()) {
        return 1;
    }
    return 0;
}

template <typename vertex_type>
bool operator==(const Graph<vertex_type> &g1, const Graph<vertex_type> &g2) {
    return compare_graphs(g1, g2) == 0;
}

template <typename vertex_type>
bool operator!=(const Graph<vertex_type> &g1, const Graph<vertex_type> &g2) {
    return compare_graphs(g1, g2) != 0;
}

template <typename vertex_type>
bool operator<(const Graph<vertex_type> &g1, const Graph<vertex_type> &g2) {
    return compare_graphs(g1, g2) == -1;
}

template <typename vertex_type>
bool operator>(const Graph<vertex_type> &g1, const Graph<vertex_type> &g2) {
    return compare_graphs(g1, g2) == 1;
}

template <typename vertex_type>
bool operator>=(const Graph<vertex_type> &g1, const Graph<vertex_type> &g2) {
    return compare_graphs(g1, g2) >= 0;
}

template <typename vertex_type>
bool operator<=(const Graph<vertex_type> &g1, const Graph<vertex_type> &g2) {
    return compare_graphs(g1, g2) <= 0;
}

template <typename vertex_type>
std::ostream &operator<<(std::ostream &os, const Graph<vertex_type> &g) {
    os << "Graph:\n\tVertices: {";
    auto vertices = g.get_vertices();
    for (size_type i = 0; i < vertices.size() - 1; ++i) {
        os << "[" << i << "]: " << vertices[i] << ", ";
    }
    os << "[" << vertices.size() - 1 << "]: " << vertices.back() << "}\n";
    os << "\tEdges: {";
    auto edges = g.get_edges();
    if (edges.size() == 0) {
        os << "}";
    } else {
        for (size_type i = 0; i < edges.size() - 1; ++i) {
            os << "(" << edges[i].first << "," << edges[i].second << "), ";
        }
        os << "(" << edges[edges.size() - 1].first << ","
           << edges[edges.size() - 1].second << ")}\n";
    }
    return os;
}

// Primary template
template <typename vertex_type, typename T>
void add_arg_to_graph(Graph<vertex_type> &g, T Arg) {
    throw std::runtime_error{"champion::Graph: add_arg_to_graph() takes "
                             "either a vertex_type or an Edge."};
}

// Specialization for adding vertex
template <typename vertex_type>
void add_arg_to_graph(Graph<vertex_type> &g, vertex_type vertex) {
    g.add_vertex(vertex, false);
}

// Specialization for adding edge
template <typename vertex_type>
void add_arg_to_graph(Graph<vertex_type> &g, Edge edge) {
    g.add_edge(edge);
}

// Special case, only one arg remaining to add
template <typename vertex_type, typename T>
void add_args_to_graph(Graph<vertex_type> &g, T Arg, bool sort_on) {
    add_arg_to_graph(Arg);
    if (sort_on) {
        g.sort();
    }
}

// General case with more than one argument to add
template <typename vertex_type, typename T, typename... Ts>
void add_args_to_graph(Graph<vertex_type> &g, T Arg, Ts... Args, bool sort_on) {
    add_arg_to_graph(Arg);
    add_args_to_graph(Args..., sort_on);
    if (sort_on) {
        g.sort();
    }
}

} // namespace champion

#endif
