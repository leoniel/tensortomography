#include "Vector.hpp"
#include "Symbolic.hpp"

namespace champion{

    template<typename T> class VectorExtended ;
    template<>
    class VectorExtended<Symbolic> : public Vector<Symbolic> {
        public:
        Symbolic divergence() const;
           
    };

    VectorExtended<Symbolic>::divergence() {
        Symbolic divsymb;
        for (const auto &var : variables_) {
            auto pdiff = derivative(var.first);
            divsymb += pdiff;
        }
                
        return divsymb;

    }
}