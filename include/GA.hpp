/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2018 Fabian Årén

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: GA.hpp

AUTHOR: Rasmus Andersson
ORIGINAL VERSION: Fabian Årén

DESCRIPTION:

A template of functions to perform an Evolutionary Algorithm.

\******************************************************************************/

#ifndef CHAMPION_GA_HPP
#define CHAMPION_GA_HPP

#include <algorithm>
#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <stdexcept>
#include <vector>

// #include <omp.h>

#include "Dictionary.hpp"
#include "algorithm.hpp"
#include "io.hpp"
#include "partition.hpp"
#include "random.hpp"
#include "set_precision.hpp"
#include "statistics.hpp"

namespace champion {

// Forward declarations
template <typename T, typename CostFunc> class DistributedGA;

enum class CrossOverMode : unsigned { n_point, arithmetic };
enum class MutationMode : unsigned { perturb, reset };

enum class GAConvergenceStatus : unsigned {
    not_converged,
    abs_cost,
    abs_gene_spread,
    rel_gene_spread,
    abs_cost_spread,
    rel_cost_spread
};

template <typename T> class Individual {
  public:
    Individual() = default;
    Individual(size_type sz) : genome_(sz, 0) {}

    Individual(std::vector<T> genome) : genome_{genome} {}

    template <typename Iterator>
    Individual(Iterator begin, Iterator end) : genome_{} {
        for (auto it = begin; it != end; ++it) {
            genome_.push_back(*it);
        }
    }

    size_type size() const { return genome_.size(); }

    std::vector<T> &genome() { return genome_; }
    const std::vector<T> &genome() const { return genome_; }

    auto begin() { return genome_.begin(); }
    auto end() { genome_.end(); }
    auto begin() const { genome_.begin(); }
    auto end() const { genome_.end(); }
    auto cbegin() const { genome_.cbegin(); }
    auto cend() const { genome_.cend(); }

    const auto &operator[](size_type i) const { return genome_[i]; }
    auto &operator[](size_type i) { return genome_[i]; }

  private:
    std::vector<T> genome_;
};

template <typename T> class Population {
  public:
    Population() = default;
    Population(size_type sz) : population_(sz) {}
    Population(std::vector<Individual<T>> individuals)
        : population_{individuals} {}
    Population(const std::vector<std::vector<T>> individuals)
        : population_(individuals.size(), {}) {
        for (size_type i = 0; i < individuals.size(); ++i) {
            population_[i] = individuals[i];
        }
    }
    template <typename Iterator>
    Population(Iterator begin, Iterator end) : population_{} {
        for (auto it = begin; it != end; ++it) {
            population_.push_back(*it);
        }
    }

    size_type size() const { return population_.size(); }

    auto begin() { return population_.begin(); }
    auto end() { return population_.end(); }
    auto begin() const { return population_.begin(); }
    auto end() const { return population_.end(); }
    auto cbegin() const { return population_.cbegin(); }
    auto cend() const { return population_.cend(); }

    const auto &operator[](size_type i) const { return population_[i]; }
    auto &operator[](size_type i) { return population_[i]; }

  private:
    std::vector<Individual<T>> population_;
};

void print_genome_default(const std::vector<real> &means,
                          const std::vector<real> &standard_deviations) {
    bool print_stds = true;
    if (standard_deviations.empty()) {
        print_stds = false;
    }
    std::cout << "Genome: \n" << std::endl;
    auto N = means.size();
    for (size_type j = 0; j < N; ++j) {
        std::cout << j << ": " << means[j];
        if (print_stds) {
            std::cout << " ± " << standard_deviations[j] << std::endl;
        } else {
            std::cout << std::endl;
        }
    }
    std::cout << std::endl;
}

template <typename T, typename CostFunc> class GA {

  public:
    // Public member functions

    // Constructors

    // Construct from guess distributions and settings
    GA(RandomNumberGenerator &rng, std::vector<real> guess_means,
       std::vector<real> guess_standard_deviations,
       CostFunc training_cost_function, CostFunc test_cost_function,
       std::function<void(const std::vector<real> &, const std::vector<real> &)>
           print_genome = nullptr,
       std::function<void(Individual<T> &)> preprocess = nullptr,
       std::function<void(GA &)> callback = nullptr,
       size_type print_interval = 100, size_type max_iter = 1000,
       real cost_tol = 1e-3, real cost_abs_spread_tol = 1e-4,
       real cost_rel_spread_tol = 1e-2, real gene_abs_spread_tol = 1e-3,
       real gene_rel_spread_tol = 1e-2, size_type no_individuals = 0,
       size_type no_elites = 0, real bias = 1,
       CrossOverMode cross_over_mode = CrossOverMode::arithmetic,
       unsigned no_cross_over_pts = 0,
       MutationMode mutation_mode = MutationMode::perturb,
       real mutation_rate = 0.005, real mutation_amplitude = 1);

    // Construct from guess distributions, read settings from dictionary
    GA(RandomNumberGenerator &rng, const Dictionary &dict,
       std::vector<real> guess_means,
       std::vector<real> guess_standard_deviations,
       CostFunc training_cost_function, CostFunc test_cost_function,
       std::function<void(const std::vector<real> &, const std::vector<real> &)>
           print_genome = nullptr,
       std::function<void(Individual<T> &)> preprocess = nullptr,
       std::function<void(GA &)> callback = nullptr);

    // Public API
    Individual<T> optimize();

    // Access functions
    auto begin() const { return population_.begin(); }
    auto end() const { return population_.end(); }
    auto cbegin() { return population_.cbegin(); }
    auto cend() { return population_.cend(); }
    auto begin() { return population_.begin(); }
    auto end() { return population_.end(); }
    const Individual<T> &operator[](size_type i) const {
        return population_[i];
    }
    Individual<T> &operator[](size_type i) { return population_[i]; }

    Individual<T> best_individual() const;
    size_type best_individual_index() const;
    real training_cost(size_type i) const { return training_costs_[i]; }
    real test_cost(size_type i) const { return test_costs_[i]; }
    real min_cost() const { return test_costs_[best_individual_index()]; }
    bool converged() const { return static_cast<bool>(convergence_); }
    GAConvergenceStatus convergence_status() const { return convergence_; }
    real cost_mean() const { return test_cost_mean_; }
    real cost_standard_deviation() const {
        return test_cost_standard_deviation_;
    }
    const std::vector<real> &gene_means() const { return gene_means_; }
    const std::vector<real> &gene_standard_deviations() const {
        return gene_standard_deviations_;
    }
    real convergence_rate() const { return convergence_rate_; }
    bool is_mutant(size_type i) const {
        return std::find(mutants_.begin(), mutants_.end(), i) != mutants_.end();
    }

    // Public low-level API. You don't need to call any of these
    // explicitly unless you want to modify the algorithm as implemented
    // in optimize().

    // Initialize new population from guess distributions and computes
    // costs
    void initialize();
    // Move individuals to old_population_ and initialize new population
    void advance();
    // Propagate the best no_elites individuals in old_population_ into
    // the new generation
    void propagate_elites();
    // Fill up new population with parents drawn from Boltzmann
    // distribution
    void select_parents();
    // Perform genetic operator cross-over as specified by configuration
    // of GA
    void cross_over();
    // Perform genetic operator mutation as specified by configuration
    // of GA
    void mutate();
    // Return whether algorithm should be halted after this generation
    // Modify individuals by calling externally provided function
    // (defaults to empty lambda)
    void preprocess_individuals();
    // Evaluate costs by calling externally provided functions to
    // compute training and test costs
    void evaluate_training_costs();
    void evaluate_training_cost(size_type i) {
        training_costs_[i] = training_cost_function(population_[i].genome());
    }
    void evaluate_test_costs();
    void evaluate_test_cost(size_type i) {
        test_costs_[i] = test_cost_function(population_[i].genome());
    }
    void compute_cost_distribution();
    void compute_gene_distributions();
    void compute_convergence_rate();
    // Print the status of the GA run
    void print_info() const;
    void print() const;
    void run_callback();
    // Check whether any stopping criteria are fulfilled
    bool check_stopping_criteria();
    // Mode setters
    void set_cross_over_mode(unsigned mode, unsigned parameter);
    void set_mutation_mode(unsigned mode);

    void reset(const std::vector<real> &guess_means = {},
               const std::vector<real> &guess_stds = {});
    void reset_iterations();

    // Helpers for constructor from Dictionary
    static CrossOverMode read_cross_over_mode(const Dictionary &dict);
    static unsigned read_no_cross_over_pts(const Dictionary &dict);
    static MutationMode read_mutation_mode(const Dictionary &dict);

    // Public data members (settings)

    // Printing behavior
    size_type print_interval;
    // Genetic operator settings
    CrossOverMode cross_over_mode;
    unsigned no_cross_over_pts;
    MutationMode mutation_mode;
    // Hyperparameters
    size_type no_individuals;
    size_type no_elites;
    real bias;
    real mutation_rate;
    real mutation_amplitude;
    // Tolerances
    size_type max_iter;
    real cost_tol;
    real cost_abs_spread_tol;
    real cost_rel_spread_tol;
    real gene_abs_spread_tol;
    real gene_rel_spread_tol;
    // External functions
    CostFunc training_cost_function;
    CostFunc test_cost_function;
    // Takes either an individual genome, or mean + variance of a
    // population
    std::function<void(const std::vector<real> &, const std::vector<real> &)>
        print_genome;
    std::function<void(Individual<T> &)> preprocess;
    std::function<void(GA &)> callback;

  private:
    friend DistributedGA<T, CostFunc>;

    // Private member functions

    // Implementation details that make no sense to use outside the
    // class
    void n_point_cross_over();
    void arithmetic_cross_over();
    void reset_mutate();
    void perturb_mutate();

    // Private data members

    // Random number generator
    RandomNumberGenerator *rng_;
    // Population data
    Population<T> population_;
    Population<T> old_population_;
    std::vector<real> training_costs_;
    std::vector<real> test_costs_;
    size_type no_genes_;
    // Optimization state
    GAConvergenceStatus convergence_;
    size_type iteration_;
    real training_cost_mean_;
    real training_cost_standard_deviation_;
    real test_cost_mean_;
    real test_cost_standard_deviation_;
    std::vector<real> gene_means_;
    std::vector<real> gene_standard_deviations_;
    std::vector<real> old_gene_standard_deviations_;
    real convergence_rate_;
    std::vector<size_type> mutants_; // indices mutated in current iteration
    // Initial distribution of genotypes
    std::vector<real> guess_means_;
    std::vector<real> guess_standard_deviations_;
};

// Function definitions

// Constructors

template <typename T, typename CostFunc>
GA<T, CostFunc>::GA(
    RandomNumberGenerator &rng, std::vector<real> guess_means,
    std::vector<real> guess_standard_deviations,
    CostFunc training_cost_function, CostFunc test_cost_function,
    std::function<void(const std::vector<real> &, const std::vector<real> &)>
        print_genome,
    std::function<void(Individual<T> &)> preprocess,
    std::function<void(GA &)> callback, size_type print_interval,
    size_type max_iter, real cost_tol, real cost_abs_spread_tol,
    real cost_rel_spread_tol, real gene_abs_spread_tol,
    real gene_rel_spread_tol, size_type no_individuals, size_type no_elites,
    real bias, CrossOverMode cross_over_mode, unsigned no_cross_over_pts,
    MutationMode mutation_mode, real mutation_rate, real mutation_amplitude)
    : print_interval{print_interval}, cross_over_mode{cross_over_mode},
      no_cross_over_pts{no_cross_over_pts}, mutation_mode{mutation_mode},
      no_individuals{no_individuals}, no_elites{no_elites}, bias{bias},
      mutation_rate{mutation_rate}, mutation_amplitude{mutation_amplitude},
      max_iter{max_iter}, cost_tol{cost_tol},
      cost_abs_spread_tol{cost_abs_spread_tol},
      cost_rel_spread_tol{cost_rel_spread_tol},
      gene_abs_spread_tol{gene_abs_spread_tol},
      gene_rel_spread_tol{gene_rel_spread_tol},
      training_cost_function{training_cost_function},
      test_cost_function{test_cost_function}, print_genome{print_genome},
      preprocess{preprocess}, callback{callback}, rng_{&rng}, population_{},
      old_population_{}, training_costs_{},
      test_costs_{}, no_genes_{guess_means.size()},
      convergence_{GAConvergenceStatus::not_converged}, iteration_{0},
      training_cost_mean_{0}, training_cost_standard_deviation_{0},
      test_cost_mean_{0}, test_cost_standard_deviation_{0},
      gene_means_(guess_means.size(), 0),
      gene_standard_deviations_(guess_means.size(), 0),
      old_gene_standard_deviations_(guess_means.size(), 0), mutants_{},
      guess_means_{guess_means}, guess_standard_deviations_{
                                     guess_standard_deviations} {
    if (no_individuals == 0) {
        no_individuals = no_genes_ * 3;
    }
    population_ = Population<T>(no_individuals);
    for (size_type i = 0; i < no_individuals; ++i) {
        population_[i] = Individual<T>{no_genes_};
    }
    old_population_ = population_;
    training_costs_ =
        std::vector<real>(no_individuals, std::numeric_limits<real>::max());
    test_costs_ = training_costs_;

    if (no_elites == 0) {
        no_elites = std::max<size_type>(1, population_.size() / 20);
    }
    if (!this->print_genome) {
        this->print_genome = print_genome_default;
    }
    if (is_odd(no_individuals)) {
        throw std::runtime_error{
            "CHAMPION::GA(): no_individuals must be an even number."};
    }
    switch (cross_over_mode) {
    case CrossOverMode::n_point:
        break;
    case CrossOverMode::arithmetic:
        break;
    default:
        throw std::runtime_error{
            std::string{"Illegal cross-over mode "} +
            std::to_string(static_cast<unsigned>(cross_over_mode))};
    }
    initialize();
}

template <typename T, typename CostFunc>
GA<T, CostFunc>::GA(
    RandomNumberGenerator &rng, const Dictionary &dict,
    std::vector<real> guess_means, std::vector<real> guess_standard_deviations,
    CostFunc training_cost_function, CostFunc test_cost_function,
    std::function<void(const std::vector<real> &, const std::vector<real> &)>
        print_genome,
    std::function<void(Individual<T> &)> preprocess,
    std::function<void(GA &)> callback)
    : GA(rng, guess_means, guess_standard_deviations, training_cost_function,
         test_cost_function, print_genome, preprocess, callback,
         read_value_or_default("print_interval", dict, 1000),
         read_value_or_default("max_iter", dict, 1e4),
         read_value_or_default("cost_tol", dict, 1e-6),
         read_value_or_default("cost_abs_spread_tol", dict, 1e-6),
         read_value_or_default("cost_rel_spread_tol", dict, 1e-6),
         read_value_or_default("gene_abs_spread_tol", dict, 1e-6),
         read_value_or_default("gene_rel_spread_tol", dict, 1e-6),
         read_value_or_default("no_individuals", dict, guess_means.size() * 2),
         read_value_or_default("no_elites", dict,
                               std::ceil(guess_means.size() * 0.2)),
         read_value_or_default("bias", dict, 1e-2), read_cross_over_mode(dict),
         read_no_cross_over_pts(dict), read_mutation_mode(dict),
         read_value_or_default("mutation_rate", dict, 3e-3),
         read_value_or_default("mutation_amplitude", dict, 1)) {}

template <typename T, typename CostFunc> void GA<T, CostFunc>::initialize() {
    if (print_interval > 0) {
        std::cout << "Initializing GA..." << std::flush;
    }
    normal_distribution<real> draw_genotype{0, 1, *rng_};
    for (size_type gene = 0; gene < no_genes_; ++gene) {
        for (size_type individual = 0; individual < no_individuals;
             ++individual) {
            if constexpr (std::is_integral<T>::value) {
                population_[individual][gene] = std::round(
                    guess_means_[gene] +
                    guess_standard_deviations_[gene] * draw_genotype());
            } else {
                population_[individual][gene] =
                    guess_means_[gene] +
                    guess_standard_deviations_[gene] * draw_genotype();
            }
        }
    }
    // Preprocess individuals if applicable
    preprocess_individuals();
    // Compute initial costs, using the selected data points
    evaluate_training_costs();
    evaluate_test_costs();
    // Compute mean and standard deviation of costs evaluated on test
    // data
    compute_cost_distribution();
    // Compute means and standard deviations for each gene
    compute_gene_distributions();
    run_callback();
    if (print_interval > 0) {
        std::cout << "Done initializing GA.\n" << std::endl;
        print_info();
        print();
    }
}

// High level API

template <typename T, typename CostFunc>
Individual<T> GA<T, CostFunc>::optimize() {
    // Initialize clock
    auto t0 = std::chrono::high_resolution_clock::now();

    // Start optimization loop
    do {
        // Move current population to old_population_ and initialize new
        // one
        advance();
        // Propagate the no_elites lowest cost individuals to the next
        // generation
        propagate_elites();
        // Draw new parents from Boltzmann distribution
        select_parents();
        // Fill up remaining spots in the population by cross-over of
        // neighboring parents
        cross_over();
        // Apply random mutation to the new population
        mutate();
        // Preprocess genomes before evaluating
        preprocess_individuals();
        // Evaluate cost of each individual
        evaluate_training_costs();
        evaluate_test_costs();
        // Compute mean and standard deviation of costs evaluated on
        // test data
        compute_cost_distribution();
        // Compute means and standard deviations for each gene
        compute_gene_distributions();
        compute_convergence_rate();
        if (print_interval > 0 && iteration_ % print_interval == 0) {
            auto t1 = std::chrono::high_resolution_clock::now();
            std::chrono::duration<real> running_time = t1 - t0;
            auto avg_time_per_iteration =
                std::chrono::duration_cast<std::chrono::milliseconds>(
                    running_time)
                    .count() /
                static_cast<real>(iteration_ + 1);
            std::cout << "Running time: " << running_time.count() << " s."
                      << std::endl;
            std::cout << "Average time per generation: "
                      << avg_time_per_iteration << " ms." << std::endl;
            print();
        }
        run_callback();
        ++iteration_;
    } while (check_stopping_criteria() == false);
    // Return the best individual in the most recent test cost
    // evaluation
    if (print_interval > 0) {
        std::cout << "Optimization stopped after " << iteration_
                  << " iterations.\n"
                  << std::endl;
        auto t1 = std::chrono::high_resolution_clock::now();
        std::chrono::duration<real> running_time = t1 - t0;
        auto avg_time_per_iteration =
            std::chrono::duration_cast<std::chrono::milliseconds>(running_time)
                .count() /
            static_cast<real>(iteration_ + 1);
        std::cout << "Running time: " << running_time.count() << " s."
                  << std::endl;
        std::cout << "Average time per generation: " << avg_time_per_iteration
                  << " ms." << std::endl;
        std::cout << "Final results: " << std::endl;
        print();
    }
    return best_individual();
}

template <typename T, typename CostFunc>
Individual<T> GA<T, CostFunc>::best_individual() const {
    auto i = best_individual_index();
    auto best = population_[i];
    return best;
}

template <typename T, typename CostFunc>
size_type GA<T, CostFunc>::best_individual_index() const {
    auto it = std::min_element(training_costs_.begin(), training_costs_.end());
    size_type i = it - training_costs_.begin();
    return i;
}

// Low level API

template <typename T, typename CostFunc> void GA<T, CostFunc>::advance() {
    old_population_ = std::move(population_);
    population_ = (no_individuals);
    for (size_type i = 0; i < no_individuals; ++i) {
        population_[i] = Individual<T>(no_genes_);
    }
    mutants_.clear();
    old_gene_standard_deviations_ = gene_standard_deviations_;
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::propagate_elites() {
    // Find the elites and propagate to the next generation
    auto elite_indices = get_first_n_indices(training_costs_, no_elites);
    // Propagate elites to next generation
    // #pragma omp parallel for
    for (size_type i = 0; i < no_elites; ++i) {
        population_[i] = old_population_[elite_indices[i]];
    }
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::select_parents() {
    // Compute (unnormalized) Boltzmann factors
    auto normalized_costs = map_to_standard_normal(training_costs_);
    auto cmf = compute_Boltzmann_cmf(normalized_costs, bias);
    // Draw parents out of the Boltzmann distribution
    auto draw =
        uniform_real_distribution<real>{0, cmf[no_individuals - 1], *rng_};
    // #pragma omp parallel for
    for (size_type i = no_elites; i < no_individuals; ++i) {
        auto random = draw();
        for (size_type j = 0; j < no_individuals; ++j) {
            if (cmf[j] > random) {
                population_[i] = old_population_[j];
                break;
            }
        }
    }
}

template <typename T, typename CostFunc> void GA<T, CostFunc>::cross_over() {
    switch (cross_over_mode) {
    case CrossOverMode::n_point:
        return n_point_cross_over();
    case CrossOverMode::arithmetic:
        return arithmetic_cross_over();
    default:
        throw std::runtime_error{
            "CHAMPION::GA::cross_over(): Unknown cross-over mode."};
    }
}

template <typename T, typename CostFunc> void GA<T, CostFunc>::mutate() {
    switch (mutation_mode) {
    case MutationMode::perturb:
        return perturb_mutate();
    case MutationMode::reset:
        return reset_mutate();
    default:
        throw std::runtime_error{
            "CHAMPION::GA::mutate(): Unknown mutation mode."};
    }
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::preprocess_individuals() {
    if (preprocess != nullptr) {
#pragma omp parallel for
        for (size_type i = 0; i < no_individuals; ++i) {
            preprocess(population_[i]);
        }
    }
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::evaluate_training_costs() {
#pragma omp parallel for
    for (size_type i = 0; i < no_individuals; ++i) {
        const auto &individual = population_[i].genome();
        training_costs_[i] = training_cost_function(individual);
    }
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::evaluate_test_costs() {
#pragma omp parallel for
    for (size_type i = 0; i < no_individuals; ++i) {
        const auto &individual = population_[i].genome();
        test_costs_[i] = test_cost_function(individual);
    }
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::compute_cost_distribution() {
    // Create a vector of the indices of all indiviuals not mutated in
    // the present iteration (as that would mess up cost statistics)
    auto non_mutants = std::vector<size_type>(no_individuals);
    std::iota(non_mutants.begin(), non_mutants.end(), 0);
    remove_indices(mutants_, non_mutants);
    // Loop over all non-mutants and compute mean and standard deviation
    // of cost
    auto training_costs = std::vector<real>{};
    auto test_costs = std::vector<real>{};
    for (size_type i = 0; i < non_mutants.size(); ++i) {
        training_costs.push_back(training_costs_[non_mutants[i]]);
        test_costs.push_back(test_costs_[non_mutants[i]]);
    }
    training_cost_mean_ = mean(training_costs);
    test_cost_mean_ = mean(test_costs);
    training_cost_standard_deviation_ = standard_deviation(training_costs);
    test_cost_standard_deviation_ = standard_deviation(test_costs);
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::compute_gene_distributions() {
    // Loop over all non-mutants and compute mean and standard
    // deviations for all genes.
    // Create a vector of the indices of all indiviuals not mutated in the
    // present iteration (as that would mess up statistics).
    auto non_mutants = std::vector<size_type>(no_individuals);
    std::iota(non_mutants.begin(), non_mutants.end(), 0);
    remove_indices(mutants_, non_mutants);
    // Loop over all non-mutants and compare mean and standard deviation
    // for each gene
    for (size_type j = 0; j < no_genes_; ++j) {
        auto gene_values = std::vector<real>{};
        for (size_type i = 0; i < non_mutants.size(); ++i) {
            gene_values.push_back(population_[non_mutants[i]][j]);
        }
        gene_means_[j] = mean(gene_values);
        gene_standard_deviations_[j] = standard_deviation(gene_values);
    }
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::compute_convergence_rate() {
    real sum = 0;
#pragma omp parallel for reduction(+ : sum)
    for (size_type i = 0; i < no_genes_; ++i) {
        if (gene_standard_deviations_[i] != 0) {
            sum += -(gene_standard_deviations_[i] -
                     old_gene_standard_deviations_[i]) /
                   gene_standard_deviations_[i];
        }
    }
    convergence_rate_ = sum / no_genes_;
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::print_info() const {
    std::cout << "GA Info:" << std::endl;
    std::cout << "Number of inidividuals: " << no_individuals << "."
              << std::endl;
    std::cout << "Genes per individual: " << no_genes_ << "." << std::endl;
    std::cout << "Number of elites bypassing cross-over: " << no_elites << "."
              << std::endl;
    std::cout << "Cross-over mode: ";
    switch (cross_over_mode) {
    case CrossOverMode::n_point:
        std::cout << no_cross_over_pts << "-point." << std::endl;
        break;
    case CrossOverMode::arithmetic:
        std::cout << "arithmetic." << std::endl;
        break;
    }
    std::cout << "Mutation rate: " << mutation_rate << "." << std::endl;
    std::cout << "Mutation mode: ";
    switch (mutation_mode) {
    case MutationMode::perturb:
        std::cout << "perturb with " << mutation_amplitude
                  << " x initial guess variance." << std::endl;
        break;
    case MutationMode::reset:
        std::cout << "reset"
                  << "." << std::endl;
        break;
    }
    std::cout << "Cost bias: " << bias << "." << std::endl;
    std::cout << "Maximum number of iterations: " << max_iter << "."
              << std::endl;
    std::cout << "Absolute cost tolerance: " << cost_tol << "." << std::endl;
    std::cout << "Cost standard deviation tolerance: " << cost_abs_spread_tol
              << "." << std::endl;
    std::cout << "Cost relative deviation tolerance: " << cost_rel_spread_tol
              << "." << std::endl;
    std::cout << "Genetic standard deviation tolerance: " << gene_abs_spread_tol
              << "." << std::endl;
    std::cout << "Genetic relative deviation tolerance: " << gene_rel_spread_tol
              << "." << std::endl;
    switch (print_interval) {
    case 0:
        std::cout << "Printing GA status disabled." << std::endl;
        break;
    default:
        std::cout << "Printing status every " << print_interval
                  << " iterations.\n"
                  << std::endl;
    }
}

template <typename T, typename CostFunc> void GA<T, CostFunc>::print() const {
    std::cout << "Iteration " << iteration_ << std::endl;
    std::cout << "Training cost distribution: " << training_cost_mean_ << " ± "
              << 2 * training_cost_standard_deviation_ << " (2 sigma ≈ 95% CI)."
              << std::endl;
    std::cout << "Test cost distribution: " << test_cost_mean_ << " ± "
              << 2 * test_cost_standard_deviation_ << " (2 sigma ≈ 95% CI)."
              << std::endl;
    std::cout << "Genetic distribution:\n";
    print_genome(gene_means_, gene_standard_deviations_);
    std::cout << "Best individual: ("
              << "training cost: " << training_costs_[best_individual_index()]
              << ", test cost: " << test_costs_[best_individual_index()]
              << ")\n";
    const auto &best = best_individual();
    auto genome = std::vector<real>(no_genes_, 0);
    for (size_type i = 0; i < no_genes_; ++i) {
        genome[i] = best[i];
    }
    print_genome(genome, {});
}

template <typename T, typename CostFunc> void GA<T, CostFunc>::run_callback() {
    if (callback != nullptr) {
        callback(*this);
    }
}

template <typename T, typename CostFunc>
bool GA<T, CostFunc>::check_stopping_criteria() {
    bool stop = false;
    auto min_cost = min(test_costs_);
    if (min_cost < cost_tol) {
        convergence_ = GAConvergenceStatus::abs_cost;
        stop = true;
        if (print_interval > 0) {
            std::cout << "Convergence reached: Cost of best individual is "
                         "below tolerance."
                      << std::endl;
        }
    }
    auto avg_gene_std = mean(gene_standard_deviations_);
    if (avg_gene_std < gene_abs_spread_tol) {
        convergence_ = GAConvergenceStatus::abs_gene_spread;
        stop = true;
        if (print_interval > 0) {
            std::cout << "Convergence reached: Absolute genetic "
                         "diversity below "
                         "tolerance."
                      << std::endl;
        }
    }
    auto gene_rel_stds = gene_standard_deviations_;
    for (size_type i = 0; i < no_genes_; ++i) {
        if (gene_means_[i] == 0) {
            gene_rel_stds[i] = 0;
        } else {
            gene_rel_stds[i] /= std::fabs(gene_means_[i]);
        }
    }
    auto avg_gene_rel_std = mean(gene_rel_stds);
    if (avg_gene_rel_std < gene_rel_spread_tol) {
        convergence_ = GAConvergenceStatus::rel_gene_spread;
        stop = true;
        if (print_interval > 0) {
            std::cout << "Convergence reached: Relative genetic "
                         "diversity below "
                         "tolerance."
                      << std::endl;
        }
    }
    if (test_cost_standard_deviation_ < cost_abs_spread_tol) {
        convergence_ = GAConvergenceStatus::abs_cost_spread;
        stop = true;
        if (print_interval > 0) {
            std::cout << "Convergence reached: Cost standard deviation below "
                         "tolerance."
                      << std::endl;
        }
    }
    auto cost_rel_std = test_cost_standard_deviation_ / min_cost;
    if (cost_rel_std < cost_rel_spread_tol) {
        convergence_ = GAConvergenceStatus::rel_cost_spread;
        stop = true;
        if (print_interval > 0) {
            std::cout << "Convergence reached: Relative cost standard "
                         "deviation below tolerance."
                      << std::endl;
        }
    }
    if (iteration_ >= max_iter) {
        stop = true;
        if (print_interval > 0) {
            std::cout << "Stopping criteria met without convergence: Reached "
                         "maximum number of "
                         "iterations."
                      << std::endl;
        }
    }
    return stop;
}

// Implementation details

// Genetic operators

template <typename T, typename CostFunc>
void GA<T, CostFunc>::n_point_cross_over() {
    size_type no_points = no_cross_over_pts;
    // #pragma omp parallel for
    for (size_type i = no_elites; i < no_individuals - 1; i += 2) {
        // Create function object for generating random points along a
        // gene
        auto random_point =
            uniform_int_distribution<size_type>(0, no_genes_, *rng_);
        // Select cross-over points
        std::vector<size_type> points(no_points);
        for (size_type p = 0; p < no_points; ++p) {
            points[p] = random_point();
        }
        std::sort(points.begin(), points.end());
        // if no_points is odd, make the last interval to be crossed
        // over be from the last drawn point to the end of the genome
        if (no_points % 2) {
            points.push_back(no_genes_ - 1);
        }
        // Perform cross-over of parents
        for (size_type p = 0; p != points.size() / 2; ++p) {
            std::swap_ranges(population_[i].begin() + points[2 * p],
                             population_[i].begin() + points[2 * p + 1],
                             population_[i + 1].begin() + points[2 * p]);
        }
    }
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::arithmetic_cross_over() {
    // #pragma omp parallel for
    for (size_type i = no_elites; i < no_individuals - 1; i += 2) {
        auto draw_mixing = uniform_real_distribution<real>(0, 1, *rng_);
        auto a = draw_mixing();
        auto b = 1 - a;
        for (size_type j = 0; j < no_genes_; ++j) {
            auto tmp1 = population_[i][j];
            auto tmp2 = population_[i + 1][j];
            if constexpr (std::is_integral<T>::value) {
                population_[i][j] = std::round(a * tmp1 + b * tmp2);
                population_[i + 1][j] = std::round(b * tmp1 + a * tmp2);
            } else {
                population_[i][j] = a * tmp1 + b * tmp2;
                population_[i + 1][j] = b * tmp1 + a * tmp2;
            }
        }
    }
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::perturb_mutate() {
    // #pragma omp parallel for
    for (size_type i = no_elites; i < no_individuals; ++i) {
        auto draw_mutate = uniform_real_distribution<real>(0, 1, *rng_);
        auto draw_delta =
            normal_distribution<real>(0, mutation_amplitude, *rng_);
        for (size_type j = 0; j < no_genes_; ++j) {
            if (draw_mutate() < mutation_rate) {
                mutants_.push_back(i);
                if constexpr (std::is_integral<T>::value) {
                    population_[i][j] += std::round(
                        draw_delta() * guess_standard_deviations_[j]);
                } else {
                    population_[i][j] +=
                        draw_delta() * guess_standard_deviations_[j];
                }
            }
        }
    }
}

template <typename T, typename CostFunc> void GA<T, CostFunc>::reset_mutate() {
    auto draw_mutate = uniform_real_distribution<real>(0, 1, *rng_);
    auto draw_val = normal_distribution<real>(0, 1, *rng_);
    // #pragma omp parallel for collapse(2)
    for (size_type i = no_elites; i < no_individuals; ++i) {
        for (size_type j = 0; j < no_genes_; ++j) {
            if (draw_mutate() < mutation_rate) {
                mutants_.push_back(i);
                if constexpr (std::is_integral<T>::value) {
                    population_[i][j] =
                        std::round(guess_means_[j] +
                                   draw_val() * guess_standard_deviations_[j]);
                } else {
                    population_[i][j] =
                        guess_means_[j] +
                        draw_val() * guess_standard_deviations_[j];
                }
            }
        }
    }
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::reset_iterations() {
    iteration_ = 0;
}

template <typename T, typename CostFunc>
void GA<T, CostFunc>::reset(const std::vector<real> &guess_means,
                            const std::vector<real> &guess_stds) {
    if (print_interval > 0) {
        std::cout << "Resetting GA... " << std::flush;
    }
    if (!guess_means.empty()) {
        if (guess_means.size() != no_genes_) {
            throw std::runtime_error{"CHAMPION::GA::reset(): Passed wrong "
                                     "number of genes as guess means."};
        }
        guess_means_ = guess_means;
    }
    if (!guess_stds.empty()) {
        if (guess_stds.size() != no_genes_) {
            throw std::runtime_error{"CHAMPION::GA::reset(): Passed wrong "
                                     "number of genes as guess stds."};
        }
        guess_standard_deviations_ = guess_stds;
    }
    iteration_ = 0;
    convergence_ = GAConvergenceStatus::not_converged;
    normal_distribution<real> draw_genotype{0, 1, *rng_};
    for (size_type gene = 0; gene < no_genes_; ++gene) {
        for (size_type individual = 0; individual < no_individuals;
             ++individual) {
            if constexpr (std::is_integral<T>::value) {
                population_[individual][gene] = std::round(
                    guess_means_[gene] +
                    guess_standard_deviations_[gene] * draw_genotype());
            } else {
                population_[individual][gene] =
                    guess_means_[gene] +
                    guess_standard_deviations_[gene] * draw_genotype();
            }
        }
    }
    preprocess_individuals();
    evaluate_training_costs();
    evaluate_test_costs();
    compute_cost_distribution();
    compute_gene_distributions();
    run_callback();
    if (print_interval > 0) {
        std::cout << "Done." << std::endl;
    }
}

// Constructor helpers

template <typename T, typename CostFunc>
CrossOverMode GA<T, CostFunc>::read_cross_over_mode(const Dictionary &dict) {
    auto mode_str = read_string("cross_over_mode", dict);
    if (mode_str == "n_point") {
        return CrossOverMode::n_point;
    } else if (mode_str == "arithmetic") {
        return CrossOverMode::arithmetic;
    } else {
        throw std::runtime_error{std::string{"GA::get_cross_over_mode(): "
                                             "Illegal cross-over mode: "} +
                                 mode_str};
    }
}

template <typename T, typename CostFunc>
unsigned GA<T, CostFunc>::read_no_cross_over_pts(const Dictionary &dict) {
    auto cross_over_mode = read_cross_over_mode(dict);
    unsigned no_cross_over_pts = std::numeric_limits<unsigned>::max();
    switch (cross_over_mode) {
    case CrossOverMode::n_point:
        no_cross_over_pts = read_single_value("no_points", dict);
        break;
    case CrossOverMode::arithmetic:
        break;
    }
    return no_cross_over_pts;
}

template <typename T, typename CostFunc>
MutationMode GA<T, CostFunc>::read_mutation_mode(const Dictionary &dict) {
    auto mode_str = read_string("mutation_mode", dict);
    MutationMode mutation_mode;
    if (mode_str == "perturb") {
        mutation_mode = MutationMode::perturb;
    } else if (mode_str == "reset") {
        mutation_mode = MutationMode::reset;
    } else {
        throw std::runtime_error{
            std::string{"GA::read_mutation_mode(): Illegal "
                        "mutation mode: "} +
            mode_str};
    }
    return mutation_mode;
}

} // namespace champion
#endif