#ifndef CHAMPION_QUATERNION_HPP
#define CHAMPION_QUATERNION_HPP

#include <cmath>
#include <iostream>
#include <stdexcept>
#include <utility>

#include "Point.hpp"
#include "Vector.hpp"
#include "algorithm.hpp"
#include "set_precision.hpp"
#include "type_inference.hpp"

namespace champion {

// Forward declarations
template <typename T> class Rotation;

template <typename T> class Quaternion {
  public:
    using iterator = T *;
    using const_iterator = const T *;
    // Default constructor
    Quaternion() : q0{0}, q1{0}, q2{0}, q3{0} {}
    // Copy constructor
    Quaternion(const Quaternion &) = default;
    // Copy assignment
    Quaternion &operator=(const Quaternion &) = default;
    // Move constructor
    Quaternion(Quaternion &&) = default;
    // Move assignment
    Quaternion &operator=(Quaternion &&) = default;
    // Generalized copy constructor
    template <typename T2>
    Quaternion(const Quaternion<T2> &Q)
        : q0{static_cast<T>(Q.q0)}, q1{static_cast<T>(Q.q1)},
          q2{static_cast<T>(Q.q2)}, q3{static_cast<T>(Q.q3)} {}
    // Generalized copy assignment
    template <typename T2> Quaternion &operator=(const Quaternion<T2> &Q) {
        for (size_type i = 0; i != this->size(); ++i) {
            (*this)[i] = Q[i];
        }
        return *this;
    }
    // Construct from components
    Quaternion(T q0_, T q1_, T q2_, T q3_)
        : q0{q0_}, q1{q1_}, q2{q2_}, q3{q3_} {}
    // Construct from Vector<T>
    Quaternion(const Vector<T> &v) : q0{0}, q1{v.x}, q2{v.y}, q3{v.z} {}
    // Construct from Point<T>
    Quaternion(const Point<T> &p)
        : q0{0}, q1{p.x.value()}, q2{p.y.value()}, q3{p.z.value()} {}
    // Construct from vector components
    Quaternion(T x, T y, T z) : q0{0}, q1{x}, q2{y}, q3{z} {}
    // Construct from T and Vector<T>
    Quaternion(T r, Vector<T> v = {0, 0, 0})
        : q0{r}, q1{v.x}, q2{v.y}, q3{v.z} {}
    // Construct from T and Point<T>
    Quaternion(T r, Point<T> p)
        : q0{r}, q1{p.x.value()}, q2{p.y.value()}, q3{p.z.value()} {}
    // Construct from axis-angle (Vector<T>-T) representation
    Quaternion(Vector<T> axis, T angle) {
        auto axis_n(axis.normalize());
        q0 = cos(angle / 2);
        q1 = axis_n.x * sin(angle / 2);
        q2 = axis_n.y * sin(angle / 2);
        q3 = axis_n.z * sin(angle / 2);
    }
    // Construct from Rotation
    Quaternion(Rotation<T> R) : Quaternion{R.axis(), R.angle()} {}

    // Access functions
    static constexpr size_type size() { return 4; }
    // Iterators to support ranges
    iterator begin() { return &q0; }
    iterator end() { return &q3 + 1; }
    const_iterator begin() const { return &q0; }
    const_iterator end() const { return &q3 + 1; }
    const_iterator cbegin() { return &q0; }
    const_iterator cend() { return &q3 + 1; }
    // Read-write access to non-const Quaternion<T>
    T &operator[](size_type i) { return *(begin() + i); }
    // Read-only access to const Quaternion<T>
    const T &operator[](size_type i) const { return *(begin() + i); }

    // Mathematical operation member functions
    // Unary operations
    Quaternion<T> normalize() const;
    Square_type<T> squared_magnitude() const;
    T magnitude() const;
    Quaternion<T> compl_conj() const;
    Quaternion<Inverse_type<T>> inverse() const;
    Dimensionless_type<T> angle() const;

    // Binary operations with another Quaternion
    template <typename T2>
    Quaternion<Product_type<T2, T, Inverse_type<T2>>>
    conjugate(const Quaternion<T2> &Q) const;

    // Public data members
    T q0, q1, q2, q3;
};

// Forward declaration (to facilitate debugging)
template <typename T>
std::ostream &operator<<(std::ostream &os, const Quaternion<T> &Q);

// Mathematical operations on Quaternion<T>s

// Unary operators
template <typename T> Quaternion<T> operator-(const Quaternion<T> &Q) {
    Quaternion<T> res = Q;
    for (auto &q : res) {
        q = -q;
    }
    return res;
}
// Forward declaration needed for squared_magnitude()
template <typename T1, typename T2>
Quaternion<Product_type<T1, T2>> operator*(const Quaternion<T1> &Q1,
                                           const Quaternion<T2> &Q2);

template <typename T> Square_type<T> squared_magnitude(const Quaternion<T> &Q) {
    Square_type<T> res = 0;
    for (auto q : Q) {
        res += q * q;
    }
    return res;
}
template <typename T> Square_type<T> Quaternion<T>::squared_magnitude() const {
    return ::champion::squared_magnitude(*this);
}
template <typename T> T magnitude(const Quaternion<T> &Q) {
    return sqrt(squared_magnitude(Q));
}
template <typename T> T Quaternion<T>::magnitude() const {
    return ::champion::magnitude(*this);
}
// Forward declaration needed for normalize()
template <typename T1, typename T2>
Quaternion<Quotient_type<T1, T2>> operator/(const Quaternion<T1> &Q, T2 x);

template <typename T> Quaternion<T> normalize(const Quaternion<T> &Q) {
    auto mag = Q.magnitude();
    if (mag == 0) {
        return Q;
    }
    return Quaternion<T>{Q} / mag;
}
template <typename T> Quaternion<T> Quaternion<T>::normalize() const {
    return ::champion::normalize(*this);
}
template <typename T> Quaternion<T> compl_conj(const Quaternion<T> &Q) {
    auto res = Q;
    for (size_type i = 1; i != Q.size(); ++i) {
        res[i] = -res[i];
    }
    return res;
}
template <typename T> Quaternion<T> Quaternion<T>::compl_conj() const {
    return ::champion::compl_conj(*this);
}
template <typename T>
Quaternion<Inverse_type<T>> inverse(const Quaternion<T> &Q) {
    auto sqmag = Q.squared_magnitude();
    if (sqmag == 0) {
        return Q;
    }
    return Q.compl_conj() / Q.squared_magnitude();
}
template <typename T>
Quaternion<Inverse_type<T>> Quaternion<T>::inverse() const {
    return ::champion::inverse(*this);
}
template <typename T> T angle(const Quaternion<T> &Q) {
    return 2 * std::acos(Q[0]);
}
template <typename T> Dimensionless_type<T> Quaternion<T>::angle() const {
    return ::champion::angle(*this);
}

// Binary operators between Quaternions

// Logical operators
template <typename T1, typename T2>
bool operator==(const Quaternion<T1> &Q, const Quaternion<T2> &P) {
    auto Q_it = Q.begin(), P_it = P.begin();
    while (Q_it != Q.end()) {
        if (*Q_it++ != *P_it++) {
            return false;
        }
    }
    return true;
}
template <typename T1, typename T2>
bool operator!=(const Quaternion<T1> &Q, const Quaternion<T2> &P) {
    return !(Q == P);
}

template <typename T> bool operator==(const Quaternion<T> &Q, double a) {
    return (Q[0] == a && Q[1] == 0 && Q[2] == 0 && Q[3] == 0);
}

template <typename T> bool operator!=(const Quaternion<T> &Q, double a) {
    return !(Q == a);
}

template <typename T> bool operator==(double a, const Quaternion<T> &Q) {
    return (Q == a);
}

template <typename T> bool operator!=(double a, const Quaternion<T> &Q) {
    return (Q != a);
}

// Arithmetic operators

// Addition of Quaternion<T>s
template <typename T1, typename T2>
Quaternion<T1> operator+=(Quaternion<T1> &Q1, const Quaternion<T2> &Q2) {
    for (size_type i = 0; i != Q1.size(); ++i) {
        Q1[i] += Q2[i];
    }
    return Q1;
}
template <typename T1, typename T2>
Quaternion<Sum_type<T1, T2>> operator+(const Quaternion<T1> &Q1,
                                       const Quaternion<T2> &Q2) {
    Quaternion<Sum_type<T1, T2>> res;
    for (size_type i = 0; i != res.size(); ++i) {
        res[i] = Q1[i] + Q2[i];
    }
    return res;
}
// Subtraction
template <typename T1, typename T2>
Quaternion<T1> operator-=(Quaternion<T1> &Q1, const Quaternion<T2> &Q2) {
    return Q1 += -Q2;
}
template <typename T1, typename T2>
Quaternion<Sum_type<T1, T2>> operator-(const Quaternion<T1> &Q1,
                                       const Quaternion<T2> &Q2) {
    return Q1 + -Q2;
}

// Multiplication
template <typename T1, typename T2>
Quaternion<Product_type<T1, T2>> operator*(const Quaternion<T1> &Q,
                                           const Quaternion<T2> &P) {
    Quaternion<Product_type<T1, T2>> Q_res;
    Q_res[0] = Q[0] * P[0] - Q[1] * P[1] - Q[2] * P[2] - Q[3] * P[3];
    Q_res[1] = Q[0] * P[1] + Q[1] * P[0] + Q[2] * P[3] - Q[3] * P[2];
    Q_res[2] = Q[0] * P[2] + Q[2] * P[0] + Q[3] * P[1] - Q[1] * P[3];
    Q_res[3] = Q[0] * P[3] + Q[3] * P[0] + Q[1] * P[2] - Q[2] * P[1];
    return Q_res;
}
// Quaternion<T> conjugation
// Conjugation of Q1 by Q2
template <typename T1, typename T2>
Quaternion<Product_type<T2, T1, Inverse_type<T2>>>
conjugate(const Quaternion<T1> &Q1, const Quaternion<T2> &Q2) {
    return (Q2 * Q1 * Q2.inverse()) / Q2.magnitude();
}
// Conjugation of *this by Q
template <typename T1>
template <typename T2>
Quaternion<Product_type<T2, T1, Inverse_type<T2>>>
Quaternion<T1>::conjugate(const Quaternion<T2> &Q) const {
    return ::champion::conjugate(*this, Q);
}

// Binary operators between Quaternions and other types

template <typename T> Quaternion<T> &operator+=(Quaternion<T> &Q, double a) {
    Q[0] += a;
    return Q;
}
template <typename T>
Quaternion<T> operator+(const Quaternion<T> &Q, double a) {
    Quaternion<T> res = Q;
    return res += a;
}
template <typename T>
Quaternion<T> operator+(double a, const Quaternion<T> &Q) {
    return Q + a;
}

template <typename T> Quaternion<T> &operator-=(Quaternion<T> &Q, double a) {
    return Q += -a;
}
template <typename T>
Quaternion<T> operator-(const Quaternion<T> &Q, double a) {
    return Q + -a;
}

// Conjugate a Vector by a Quaternion, return Vector
template <typename T1, typename T2>
Vector<Product_type<T2, T1, Inverse_type<T2>>>
conjugate(const Vector<T1> &V, const Quaternion<T2> &Q) {
    Quaternion<T1> conj = Quaternion<T1>{V}.conjugate(Q);
    Vector<Product_type<T2, T1, Inverse_type<T2>>> res = V;
    for (size_type i = 0; i != res.size(); ++i) {
        res[i] = conj[i + 1];
    }
    return res;
}

// Conjugate a Point by a Quaternion, return Point
// Performs conjugation without regards for bounds (as it should),
// then puts the result back into periodic box
template <typename T1, typename T2>
Point<Product_type<T2, T1, Inverse_type<T2>>>
conjugate(const Point<T1> &P, const Quaternion<T2> &Q) {
    Quaternion<T1> conj = Quaternion<T1>{P}.conjugate(Q);
    Point<Product_type<T2, T1, Inverse_type<T2>>> res = P;
    for (size_type i = 0; i != res.size(); ++i) {
        res[i] = conj[i + 1];
    }
    return res;
}

// Scaling of Quaternion<T>s by numeric types
template <typename T1, typename T2>
Quaternion<T1> operator*=(Quaternion<T1> &Q, T2 r) {
    for (auto &qi : Q) {
        qi *= r;
    }
    return Q;
}
template <typename T1, typename T2>
Quaternion<Product_type<T1, T2>> operator*(const Quaternion<T1> &Q, T2 r) {
    Quaternion<Product_type<T1, T2>> res;
    for (size_type i = 0; i != res.size(); ++i) {
        res[i] = Q[i] * r;
    }
    return res;
}
template <typename T1, typename T2>
Quaternion<Product_type<T1, T2>> operator*(T1 r, const Quaternion<T2> &Q) {
    return Q * r;
}
template <typename T1, typename T2>
Quaternion<T1> operator/=(Quaternion<T1> &Q, T2 r) {
    return Q *= (1 / r);
}
template <typename T1, typename T2>
Quaternion<Quotient_type<T1, T2>> operator/(const Quaternion<T1> &Q, T2 r) {
    return Q * (1 / r);
}

// User defined literals to express quaternions as q0 + q1_i + q2_j + q3_k
Quaternion<real> operator"" _i(long double r) {
    return {0, static_cast<real>(r), 0, 0};
}
Quaternion<real> operator"" _j(long double r) {
    return {0, 0, static_cast<real>(r), 0};
}
Quaternion<real> operator"" _k(long double r) {
    return {0, 0, 0, static_cast<real>(r)};
}
Quaternion<real> operator"" _i(unsigned long long a) {
    return {0, static_cast<real>(a), 0, 0};
}
Quaternion<real> operator"" _j(unsigned long long a) {
    return {0, 0, static_cast<real>(a), 0};
}
Quaternion<real> operator"" _k(unsigned long long a) {
    return {0, 0, 0, static_cast<real>(a)};
}

// IO
template <typename T>
std::ostream &operator<<(std::ostream &os, const Quaternion<T> &Q) {
    auto Q2{Q};
    for (auto &q : Q2) {
        if (sqrt(q * q) < 1e-10)
            q = 0;
    }
    return os << Q2[0] << " + " << Q2[1] << "i + " << Q2[2] << "j + " << Q2[3]
              << "k";
}

// Construct Vector<T> from vector part of Quaternion<T>
template <typename T> Vector<T>::Vector(const Quaternion<T> &Q) {
    x = Q[1];
    y = Q[2];
    z = Q[3];
}

} // namespace champion

#endif
