/******************************************************************************
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2018 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

SOURCE FILE: Energy_Form.hpp

AUTHOR: Fabian Årén (fabian.aaren@chalmers.se) 2018-01-17
MODIFIED BY:

DESCRIPTION:

\******************************************************************************/

#ifndef CHAMPION_ENERGY_FORM_HPP
#define CHAMPION_ENERGY_FORM_HPP

#include <iostream>
#include <omp.h>
#include <fftw3.h>

#include <cmath>
#include <numeric>
#include <string>
#include <typeinfo>
#include <vector>

#include "Complex.hpp"
#include "Matrix.hpp"
#include "ObjectRegistry.hpp"
#include "Vector.hpp"

namespace champion {

template <typename T> int sgn(T val) { return (T(0) < val) - (val < T(0)); }
template <typename T> int heavyside(T val) { return 0.5 * (sgn(val)) + 1; }
template <typename T> int dirac(T val) { return (T(0) == val); }

template <level_type n, level_type level> class EnergyForm {
  public:
    // Public types

    EnergyForm(const ObjectRegistry<n> &reg,
               ObjectIndex<Trajectory, n, level> traj_id, real L,
               real r_cutoff = 9.0, size_type no_k = 10, size_type n_max = 10);

    void distance_check();
    real bond_energy(const long);
    real angle_energy(const long);
    real proper_torsion_energy(const long);
    real improper_torsion_energy(const long);
    // Non-bonded energy
    real nb_energy(const long);
    // Electro Magnetic Energy (including dipoles)
    real coloumb_potential(size_type);
    real em_energy(const long);
    real total_energy(bool = false);
    std::vector<Matrix<real>> force_list(bool = false);

    const ObjectRegistry<n> &reg_;
    ObjectIndex<Trajectory, n, level> traj_id_;
    // TODO: fixa mer dynamiskt val av vilka forces som inkluderas
    size_type no_force_types_;

    std::vector<Vector<real>> r_;
    // Displacement vectors
    Matrix<Vector<real>> r_ij_;
    // Distances
    Matrix<real> R_ij_;
    // Atomic properties*/
    std::vector<real> q_; // charge
    std::vector<real> p_; // dipole polarizability
    std::vector<real> k_bond_, k_angle_, k_proper_, k_improper_;
    std::vector<real> r_0_, theta_0_, phi_prop_0_, phi_improp_0_;
    // Non-Bonded parameters
    std::vector<real> A_, B_, C_, D_;
    // Dipole Ewald summation parameters
    std::vector<real> alpha_;
    std::vector<real> eta_;
    // Total charge
    real Q_;

    // Periodicity
    int n_name_;

    // Box length
    real L_;
    real r_cutoff_;
    // Number of atoms, bonds, angles, proper torsions, improper torsions
    size_type N_, no_bonds_, no_angles_, no_props_, no_improps_;
    // Number of wave vectors
    size_type no_k_;
    size_type no_trajectories_;
    size_type no_species_;
    size_type n_max_;

    std::vector<SymmetricMatrix<real>> distances_;
    std::vector<Vector<size_type>> n_vec_;
};

template <level_type n, level_type level>
EnergyForm<n, level>::EnergyForm(const ObjectRegistry<n> &reg,
                                 ObjectIndex<Trajectory, n, level> traj_id,
                                 real L, real r_cutoff, size_type no_k,
                                 size_type n_max)
    : reg_{&reg}, traj_id_{traj_id}, L_{L}, r_cutoff_{r_cutoff}, no_k_{no_k},
      n_max_{n_max} {

    std::cout << "initialization done" << std::endl;
}

template <level_type n, level_type level>
void EnergyForm<n, level>::distance_check() {
    distances_ = std::vector<SymmetricMatrix<real>>(no_trajectories_);
    for (size_type traj = 0; traj < no_trajectories_; ++traj) {
        auto trajectory = reg_->template get_object<Trajectory, 0>(traj);
        for (size_type t = 0; t != trajectory.size(); t += 100) {
            distances_[traj] = SymmetricMatrix<real>(N_, N_);
            auto conf = trajectory.get_snapshot(t);
            auto no_atoms = conf.size();
            for (size_type i = 0; i < N_; ++i) {
                for (size_type j = i + 1; j < N_; ++j) {
                    auto d =
                        magnitude(conf.get_position(j) - conf.get_position(i));
                    distances_[traj](i, j) = d;
                }
            }
        }
    }
}

template <level_type n, level_type level>
real EnergyForm<n, level>::bond_energy(const long t) {

    const auto &traj = reg_.get_trajectory_by_id(traj_id_);
     const auto &conf =
        traj.get_snapshot(t);
    real V_bond = 0;
    const auto &bonds = reg_.template get_objects<Bond, level>();
    for (const auto &bond_pair : bonds) {
        const auto &bond = bond_pair.second;

        const auto &i = bond.first(); // force center index
        const auto &fc_i = reg_.template get_object<ForceCenter, 0>(i);
        const auto &point_i =
            conf.get_position(conf.get_index_by_id(fc_i.body().get_id()));

        const auto &j = bond.second(); // force center index
        const auto &fc_j = reg_.template get_object<ForceCenter, 0>(j);
        const auto &point_j =
            conf.get_position(conf.get_index_by_id(fc_j.body().get_id()));

        const auto &type_i = fc_i.body().type();
        const auto &type_j = fc_j.body().type();

        const auto &r_ij = Vector(point_i - point_j);
        const auto &R_ij = magnitude(r_ij);

        V_bond += k_bond_(type_i,type_j) * (R_ij - r_0_(type_i,type_j)) * (R_ij - r_0_(type_i,type_j));
    }

    std::cout << "bond done" << std::endl;
    return V_bond;
}
template <level_type n, level_type level>
real EnergyForm<n, level>::angle_energy(const long t) {
    const auto &traj = reg_.get_trajectory_by_id(traj_id_);
    const auto &conf = traj.get_snapshot(t);
    real V_angle = 0;
    const auto &angles = reg_.template get_objects<BondAngle, level>();
    for (const auto &angle_pair : angles) {
        const auto &angle = angle_pair.second;

        const auto &i = angle.first(); // force center index
        const auto &fc_i = reg_.template get_object<ForceCenter, 0>(i);
        const auto &point_i =
            conf.get_position(conf.get_index_by_id(fc_i.body().get_id()));

        const auto &j = angle.second(); // force center index
        const auto &fc_j = reg_.template get_object<ForceCenter, 0>(j);
        const auto &point_j =
            conf.get_position(conf.get_index_by_id(fc_j.body().get_id()));

        const auto &k = angle.third(); // force center index
        const auto &fc_k = reg_.template get_object<ForceCenter, 0>(k);
        const auto &point_k =
            conf.get_position(conf.get_index_by_id(fc_k.body().get_id()));

        const auto &type_i = fc_i.body().type();
        const auto &type_j = fc_j.body().type();
        const auto &type_k = fc_k.body().type();

        const auto &r_ij = Vector(point_i - point_j);
        const auto &R_ij = magnitude(r_ij);

        const auto &r_jk = Vector(point_j - point_k);
        const auto &R_jk = magnitude(r_jk);

        auto theta = acos(r_ij.dot(r_jk)) / (R_ij * R_jk);
        V_angle += k_angle_(type_i, type_j, type_k) *
                   pow((theta - theta_0_(type_i, type_j, type_k)), 2);
    }
    std::cout << "angle done" << std::endl;
    return V_angle;
}
template <level_type n, level_type level>
real EnergyForm<n, level>::proper_torsion_energy(const long t) {

    const auto &traj = reg_.get_trajectory_by_id(traj_id_);
    const auto &conf = traj.get_snapshot(t);
    const auto &props = reg_.template get_objects<Torsion, level>();
    real V_proper = 0;
    for (const auto &props_pair : props) {
        const auto &props = props_pair.second;

        const auto &i = props.first(); // force center index
        const auto &fc_i = reg_.template get_object<ForceCenter, 0>(i);
        const auto &point_i =
            conf.get_position(conf.get_index_by_id(fc_i.body().get_id()));

        const auto &j = props.second(); // force center index
        const auto &fc_j = reg_.template get_object<ForceCenter, 0>(j);
        const auto &point_j =
            conf.get_position(conf.get_index_by_id(fc_j.body().get_id()));

        const auto &k = props.third(); // force center index
        const auto &fc_k = reg_.template get_object<ForceCenter, 0>(k);
        const auto &point_k =
            conf.get_position(conf.get_index_by_id(fc_k.body().get_id()));

        const auto &l = props.fourth(); // force center index
        const auto &fc_l = reg_.template get_object<ForceCenter, 0>(l);
        const auto &point_l =
            conf.get_position(conf.get_index_by_id(fc_l.body().get_id()));

        const auto &type_i = fc_i.body().type();
        const auto &type_j = fc_j.body().type();
        const auto &type_k = fc_k.body().type();
        const auto &type_l = fc_l.body().type();

        const auto &r_ij = Vector(point_i - point_j);
        const auto &R_ij = magnitude(r_ij);

        const auto &r_jk = Vector(point_j - point_k);
        const auto &R_jk = magnitude(r_jk);

        const auto &r_lk = Vector(point_l - point_k);
        const auto &R_lk = magnitude(r_lk);

        auto m = r_ij.cross(r_jk);
        auto nn = r_lk.cross(r_jk);

        auto cosphi_ijkl = m.dot(nn);
        auto sinphi_ijkl =
            (nn.dot(r_ij) *
             R_jk); // denominator removed since it dissapears analytically.
        auto phi_prop = -atan(sinphi_ijkl / cosphi_ijkl);

        V_proper += k_proper_(type_i, type_j, type_k, type_l) *
                    (1 + cos(n_name_ * phi_prop -
                             phi_prop_0_(type_i, type_j, type_k, type_l)));
    }
    std::cout << "proper done" << std::endl;
    return V_proper;
}
template <level_type n, level_type level>
real EnergyForm<n, level>::improper_torsion_energy(const long t) {

    const auto &traj = reg_.get_trajectory_by_id(traj_id_);
    const auto &conf = traj.get_snapshot(t);
    const auto &improps = reg_.template get_objects<ImproperTorsion, level>();
    real V_improp;
    
    for (const auto &improps_pair : improps) {
        const auto &improps = improps_pair.second;

        const auto &i = improps.first(); // force center index
        const auto &fc_i = reg_.template get_object<ForceCenter, 0>(i);
        const auto &point_i =
            conf.get_position(conf.get_index_by_id(fc_i.body().get_id()));

        const auto &j = improps.second(); // force center index
        const auto &fc_j = reg_.template get_object<ForceCenter, 0>(j);
        const auto &point_j =
            conf.get_position(conf.get_index_by_id(fc_j.body().get_id()));

        const auto &k = improps.third(); // force center index
        const auto &fc_k = reg_.template get_object<ForceCenter, 0>(k);
        const auto &point_k =
            conf.get_position(conf.get_index_by_id(fc_k.body().get_id()));

        const auto &l = improps.fourth(); // force center index
        const auto &fc_l = reg_.template get_object<ForceCenter, 0>(l);
        const auto &point_l =
            conf.get_position(conf.get_index_by_id(fc_l.body().get_id()));

        const auto &type_i = fc_i.body().type();
        const auto &type_j = fc_j.body().type();
        const auto &type_k = fc_k.body().type();
        const auto &type_l = fc_l.body().type();

        const auto &r_ij = Vector(point_i - point_j);
        const auto &R_ij = magnitude(r_ij);

        const auto &r_jk = Vector(point_j - point_k);
        const auto &R_jk = magnitude(r_jk);

        const auto &r_lk = Vector(point_l - point_k);
        const auto &R_lk = magnitude(r_lk);

        auto m = r_ij.cross(r_jk);
        auto nn = r_lk.cross(r_jk);

        auto cosphi_ijkl = m.dot(nn);
        auto sinphi_ijkl =
            (nn.dot(r_ij) *
             R_jk); // denominator removed since it dissapears analytically.
        auto phi_improp = -atan(sinphi_ijkl / cosphi_ijkl);

        V_improp +=
            k_improper_(type_i, type_j, type_k, type_l) *
            pow((phi_improp - phi_improp_0_(type_i, type_j, type_k, type_l)),
                2);
    }
    std::cout << "improper done" << std::endl;
    return V_improp;
}
// TODO: make effective algorithm that doesn't double count
// Non-bonded energy
template <level_type n, level_type level>
real EnergyForm<n, level>::nb_energy(const long t) {
    // TODO: fix periodic long range interaction
    const auto &traj = reg_.get_trajectory_by_id(traj_id_);
    const auto &conf = traj.get_snapshot(t);
    const auto &bodies = reg_.template get_objects<Body, level>();
    real V_nb = 0;

    for (const auto &body_pair_i : bodies) {
        const auto &body_i = body_pair_i.second;
        const auto &point_i =
            conf.get_position(conf.get_index_by_id(body_i.get_id()));

        const auto &type_i = body_i.type();

        for (const auto &body_pair_j : bodies) {
            const auto &body_j = body_pair_j.second;
            const auto &point_j =
                conf.get_position(conf.get_index_by_id(body_j.get_id()));

            const auto &r_ij = Vector(point_i, point_j);
            const auto &R_ij = magnitude(r_ij);

            const auto &type_j = body_j.type();

            V_nb +=
                A_(type_i, type_j) * exp(-B_(type_i, type_j) * R_ij) -
                C_(type_i, type_j) * pow(R_ij, -6) +
                D_(type_i, type_j) * pow(12 / (B_(type_i, type_j) * R_ij), 12);
        }
    }

    std::cout << "nb done" << std::endl;
    return V_nb;
}

// TODO:
/*
real EnergyForm::coloumb_energy(){

}
*/
/*template <level_type n, level_type level>
real EnergyForm<n, level>::coloumb_potential(size_type i) {
    real phi_i(0);
    for (size_type j = 0; j < N_; ++j) {
        real phi_multiplier(0);
        for (auto n_vec_elem : n_vec_) {
            auto temp = eta_(type_i) * magnitude(r_ij_(i, j) + n_vec_elem * L_);
            phi_multiplier +=
                erfc(temp) / magnitude(r_ij_(i, j) + n_vec_elem * L_);
        }
        for (auto n_vec_elem : n_vec_) { // not necesarily the same n_vec_
            auto k_vec = 2 * pi * n_vec_elem / L_;
            auto k = magnitude(k_vec);
            phi_multiplier += Re((4 * pi / pow(L_, 3)) *
                                 exp(-k * k / (4 * eta_(type_i) * eta_(type_i))) /
                                 (k * k) * compl_exp(k_vec.dot(r_ij_(i, j))));
        }
        std::cout << j << std::endl;
        phi_i += q_(type_j) * phi_multiplier;
    }
    phi_i -= pi / (L_ * L_ * L_ * eta_(type_i) * eta_(type_i)) * Q_ +
             2 * eta_(type_i) / sqrt(pi) * q_(type_i);

    return phi_i;
}*/
// Electro-Magnetic Energy with induced dipole contribution
// Implementation based on:
// Notes on Ewald summation for charges and dipolesThomas L. BeckDepartment
// of ChemistryUniversity of CincinnatiCincinnati, OH 45221-0172email:
// becktl@email.uc.eduFebruary 17, 2010
template <level_type n, level_type level>
real EnergyForm<n, level>::em_energy(const long t) {

    real U = 0;

    std::vector<Vector<real>> d;
    const auto &traj = reg_.get_trajectory_by_id(traj_id_);
    const auto &conf = traj.get_snapshot(t);
    const auto &bodies = reg_.template get_objects<Body, level>();

    /*for (const auto &body_pair_i : bodies) {
        const auto &body_i = body_pair_i.second;

        const auto &type_i = body_i.type();
        d.push_back(p_(type_i) *
                    Vector<real>(coloumb_potential(i)
                                     .gradient({x + std::to_string(i),
                                                y + std::to_string(i),
                                                z + std::to_string(i)})
                                     .begin())); // induced dipole moment
    }*/
    real U_sr = 0;
    real U_lr = 0;
    for (const auto &body_pair_i : bodies) {
        const auto &body_i = body_pair_i.second;
        const auto &point_i =
            conf.get_position(conf.get_index_by_id(body_i.get_id()));

        const auto &type_i = body_i.type();

        for (const auto &body_pair_j : bodies) {
            const auto &body_j = body_pair_j.second;
            const auto &point_j =
                conf.get_position(conf.get_index_by_id(body_j.get_id()));

                const auto &type_j = body_j.type();

            const auto &r_ij = Vector(point_i, point_j);
            const auto &R_ij = magnitude(r_ij);

            for (auto n_vec_elem : n_vec_) {
                const auto &sumrnL = r_ij + n_vec_elem * L_;
                const auto &abssumrnL = magnitude(sumrnL);
                const auto &squaredsumrnL = squared_magnitude(sumrnL);
                U_sr += q_(type_i) * q_(type_j) *
                        std::erfc(eta_(type_i) * abssumrnL) /
                        abssumrnL; // monopile-monopole interaction
                auto delerfc = 2 * abssumrnL *
                               (-2 * exp(-eta_(type_i) * eta_(type_i) * squaredsumrnL) *
                                    eta_(type_i) / (sqrt(pi) * squaredsumrnL) -
                                std::erfc(eta_(type_i) * abssumrnL) /
                                    (squaredsumrnL * abssumrnL)) *
                               (-1 / 2 + heavyside(sumrnL));
                U_sr +=
                    -q_(type_i) * d(type_j).dot(delerfc); // monopole-dipole interaction
                U_sr += q_(type_j) * d(type_i).dot(delerfc); // dipole-monopole
                                                   // interaction
                U_sr +=
                    d(type_i) * d(type_j) *
                    (2 * abssumrnL * dirac(sumrnL) *
                         (2 / sqrt(pi) * eta_(type_i) *
                              exp(-eta_(type_i) * eta_(type_i) * squaredsumrnL) /
                              squaredsumrnL -
                          std::erfc(eta_(type_i) * abssumrnL) / pow(abssumrnL, 3)) +
                     4 *
                         ((squaredsumrnL * 2 *
                           exp(-eta_(type_i) * eta_(type_i) * squaredsumrnL) * eta_(type_i) *
                           (3 + 2 * eta_(type_i) * eta_(type_i) * squaredsumrnL)) /
                              (sqrt(pi) * squaredsumrnL * squaredsumrnL) +
                          3 * std::erfc(eta_(type_i) * abssumrnL) /
                              (squaredsumrnL * squaredsumrnL * abssumrnL)) *
                         (-1 / 2 + heavyside(sumrnL)) *
                         (-1 / 2 + heavyside(sumrnL)) +
                     (-2 * epx(-eta_(type_i) * eta_(type_i) * squaredsumrnL) * eta_(type_i) /
                          (sqrt(pi) * squaredsumrnL) -
                      std::erfc(eta_(type_i) * abssumrnL) /
                          (squaredsumrnL * abssumrnL)) *
                         (-1 / 2 + heavyside(sumrnL)) *
                         (-1 / 2 +
                          heavyside(sumrnL))); // tbc dipole-dipole interaction
            }
        }
    }
    const auto & k_space = conf.get_positions();
    for (const auto &body_pair_i : bodies) {
        const auto &body_i = body_pair_i.second;
        const auto &point_i =
            conf.get_position(conf.get_index_by_id(body_i.get_id()));

        const auto &type_i = body_i.type();

        for (const auto &body_pair_j : bodies) {
            const auto &body_j = body_pair_j.second;
            const auto &point_j =
                conf.get_position(conf.get_index_by_id(body_j.get_id()));

            const auto &r_ij = Vector(point_i, point_j);
            const auto &R_ij = magnitude(r_ij);

            const auto &type_j = body_j.type();
            U_lr += q_(type_i) * q_(type_j) *
                    exp(-k_space_elem.squared_magnitude() / pow(eta_(type_i), 2)) /
                    k_space_elem.squared_magnitude() *
                    compl_exp(
                        k_space_elem.dot(r_ij)); // monopole-monopole interaction
            auto dellr =
                std::complex(k_space_elem.*compl_exp(k_space_elem.dot(r_ij)));
            U_lr += -q_(type_i) * d(type_j).dot(dellr); // monopole-dipole interaction
            U_lr += q_(type_j) * d(type_i).dot(dellr);  // dipole-monopole interaction
            U_lr +=
                d(type_i) * d(type_j) *
                (-magnitude(k_space_elem) *
                 compl_exp(k_space_elem.dot(r_ij))); // dipole-dipole interaction
        }
    }
}
/* for (size_type i = 0; i < N_; ++i)
{
    std::cout << i << std::endl;
    std::initializer_list<std::string> deriv_var = {"x" + std::to_string(i),
"y" + std::to_string(i), "z" + std::to_string(i)}; for (size_type j = 0; j <
N_; ++j)
    {

        if (i == j)
        {
            break;
        }
        real U_multiplier(0);
        for (auto n_vec_elem : n_vec_)
        {
            U_multiplier += erfc(eta_(type_i) * magnitude(r_ij_(i, j) +
n_vec_elem * L_)) / magnitude(r_ij_(i, j) + n_vec_elem * L_);
        }
        for (auto n_vec_elem : n_vec_)
        {
            auto k_vec = 2 * pi * n_vec_elem / L_;
            auto k = magnitude(k_vec);
            U_multiplier += Re((4 * pi / pow(L_, 3)) * exp(-k * k / (4 *
eta_(type_i) * eta_(type_i))) / (k * k) * compl_exp(k_vec.dot(r_ij_(i, j))));
        }
        U += q_(type_i) * q_(type_j) * U_multiplier - q_(type_i) *
d(type_j).dot(Vector<real>(U_multiplier.gradient(deriv_var).begin())) - q_(type_j) *
d(type_i).dot(Vector<real>(U_multiplier.gradient(deriv_var).begin())) -
d(type_i).dot((divergence(d(type_j), deriv_var)) *
(Vector<real>(U_multiplier.gradient(deriv_var).begin())));
    }

    U += -eta_(type_i) / sqrt(pi) * (q_(type_i) * q_(type_i)) -
         2 * pow(eta_(type_i), 3) / (3 * sqrt(pi)) *
             (pow(sqrt((d(type_i)).dot(d(type_i))), 2)) +
         1 / 2 * (pow(sqrt(d(type_i).dot(d(type_i))), 2) / alpha_(type_i)) -
         pi / (2 * pow(L_, 3) * pow(eta_(type_i), 2));
}

U += Q_ * Q_;
std::cout << "polarization done" << std::endl;
return U;*/



template <level_type n, level_type level>
real EnergyForm<n, level>::total_energy(bool Coloumb_only) {

    real E;

    E = this->bond_energy() + this->angle_energy() +
        this->proper_torsion_energy() + this->improper_torsion_energy() +
        this->nb_energy();
    if (Coloumb_only) {
        // E += EnergyForm<n, level>::coloumb_energy();
        // TODO:
        std::cout << "Not finished yet [TBA]" << std::endl;
        return 1;
    } else {
        E += this->em_energy();
    }

    return E;
}

/*template <level_type n, level_type level>
std::vector<Matrix<real>> EnergyForm<n, level>::force_list(bool Coloumb_only) {
    real E = total_energy(Coloumb_only);

    std::vector<Matrix<real>> F;

    for (size_type i = 0; i < N_; ++i) {
        F.push_back(E.gradient({x + std::to_string(i), y + std::to_string(i),
                                z + std::to_string(i)}));
        std::cout << "i: " << i << std::endl;
    }
    return F;
}*/
} // namespace champion

#endif
