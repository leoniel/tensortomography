#ifndef CHAMPION_SYMBOLIC_HPP
#define CHAMPION_SYMBOLIC_HPP

#include <memory>
#include <sstream>
#include <string>

#include <cln/cln.h>
#include <ginac/ginac.h>

#include "Matrix.hpp"
#include "Vector.hpp"
#include "set_precision.hpp"

namespace champion
{

class Symbolic
{
public:
    // Default constructor
    Symbolic(real a = 0) : variables_{}, expr_{a} {}
    // Copy constructor
    Symbolic(const Symbolic &) = default;
    // Copy assignment
    Symbolic &operator=(const Symbolic &) = default;
    // Move constructor
    Symbolic(Symbolic &&) = default;
    // Move assignment
    Symbolic &operator=(Symbolic &&) = default;
    // Create a new independent variable
    Symbolic(std::string name) : variables_{}, expr_{}
    {
        if (name == "")
        {
            throw std::runtime_error{
                "CHAMPION::Symbolic: Independent symbolic variable names must "
                "consist of one or more characters."};
        }
        variables_[name] = std::make_shared<GiNaC::symbol>(name);
        expr_ = GiNaC::ex{*variables_[name]};
        ++no_variables_;
    }

    // Access
    size_type num_variables() const { return variables_.size(); }
    std::vector<std::string> get_variables() const
    {
        std::vector<std::string> vars;
        for (const auto &var : variables_)
        {
            vars.push_back(var.first);
        }
        return vars;
    }
    std::string str() const
    {
        std::stringstream ss;
        ss << expr_;
        return ss.str();
    }

    // Logical operators
    bool operator==(const Symbolic &S) const { return expr_ == S.expr_; }
    bool operator<(const Symbolic &S) const { return expr_ < S.expr_; }
    bool operator>(const Symbolic &S) const { return expr_ > S.expr_; }

    // Evaluation
    template <typename... Args>
    Symbolic evaluate(const std::string &variable, real value,
                      Args... args) const;

    // Derivatives
    Symbolic derivative(const std::string &variable,
                        unsigned short order = 1) const;
    //Matrix<Symbolic> gradient() const;
    Matrix<Symbolic> gradient(std::initializer_list<std::string> = {}) const;
    Symbolic laplacian() const;
    Symbolic diff_operator() const;
    SquareMatrix<Symbolic> hessian() const;

    // Mathematical operators
    Symbolic operator-() const;
    Symbolic &operator+=(const Symbolic &);
    Symbolic &operator*=(const Symbolic &);
    Symbolic &operator/=(const Symbolic &);

    // Change type
    double
    symb_to_double()
    {
        if (GiNaC::is_a<GiNaC::numeric>(expr_))
        {
            double d = GiNaC::ex_to<GiNaC::numeric>(expr_).to_double();
            return d;
        }
        else
        {
            std::cout << "Not a numeric" << std::endl;
            return 0;
        }
    }

private:
    // Friends
    template <typename... Args>
    friend Symbolic evaluate(const Symbolic &, const std::string &, real,
                             Args... args);
    friend Symbolic pow(const Symbolic &, const Symbolic &);
    friend Symbolic exp(const Symbolic &);
    friend Symbolic log(const Symbolic &);
    friend Symbolic sin(const Symbolic &);
    friend Symbolic cos(const Symbolic &);
    friend Symbolic tan(const Symbolic &);
    friend Symbolic asin(const Symbolic &);
    friend Symbolic acos(const Symbolic &);
    friend Symbolic atan(const Symbolic &);
    friend Symbolic sinh(const Symbolic &);
    friend Symbolic cosh(const Symbolic &);
    friend Symbolic tanh(const Symbolic &);
    friend Symbolic asinh(const Symbolic &);
    friend Symbolic acosh(const Symbolic &);
    friend Symbolic atanh(const Symbolic &);
    friend Symbolic sqrt(const Symbolic &);
    friend Symbolic abs(const Symbolic &);
    friend std::ostream &operator<<(std::ostream &, const Symbolic &);
    friend Symbolic divergence(const Vector<Symbolic> &, std::initializer_list<std::string>);

    // Private member functions
    void add_variables(const Symbolic &S)
    {
        for (auto &Si : S.variables_)
        {
            if (!variables_[Si.first])
            {
                variables_[Si.first] = Si.second;
            }
            else
            {
                if (variables_[Si.first] != Si.second)
                {
                    throw std::runtime_error{
                        "CHAMPION::Symbolic: Different variables in the same "
                        "Symbolic cannot have the same names."};
                }
            }
        }
    }

    // Private data members
    std::map<std::string, std::shared_ptr<GiNaC::symbol>> variables_;
    GiNaC::ex expr_;

    // Static data members
    static size_type no_variables_;
};

// Logical operators

bool operator!=(const Symbolic &S1, const Symbolic &S2) { return !(S1 == S2); }
bool operator>=(const Symbolic &S1, const Symbolic &S2) { return !(S1 < S2); }
bool operator<=(const Symbolic &S1, const Symbolic &S2) { return !(S1 > S2); }

// Evaluation

Symbolic &evaluate(Symbolic &S) { return S; }
const Symbolic &evaluate(const Symbolic &S) { return S; }

template <typename... Args>
Symbolic evaluate(const Symbolic &S, const std::string &variable, real value,
                  Args... args)
{
    auto evaluated = S;
    auto it = std::find_if(
        S.variables_.begin(), S.variables_.end(),
        [&variable](const auto &el) { return el.first == variable; });
    if (it == S.variables_.end())
    {
        return evaluate(evaluated, args...);
    }
    evaluated.expr_ =
        evaluated.expr_.subs(*evaluated.variables_.at(variable) == value);
    evaluated.variables_.erase(variable);
    return evaluate(evaluated, args...);
}

template <typename... Args>
Symbolic Symbolic::evaluate(const std::string &variable, real value,
                            Args... args) const
{
    return ::champion::evaluate(*this, variable, value, args...);
}

// Derivatives
Symbolic Symbolic::derivative(const std::string &variable,
                              unsigned short order) const
{
    auto S = *this;
    // Find iterator to variable
    auto it = std::find_if(
        S.variables_.begin(), S.variables_.end(),
        [&variable](const auto &el) { return el.first == variable; });
    // Return 0 if *this not dependent on variable
    if (it == S.variables_.end())
    {
        return Symbolic{0};
    }
    const auto &var = *it->second;
    S.expr_ = S.expr_.diff(var, order);
    auto S_str = S.str();
    // Remove variables on which the differentiated expression no longer
    // depends.
    for (const auto &var : variables_)
    {
        if (S_str.find(var.first) == std::string::npos)
        {
            S.variables_.erase(var.first);
        }
    }
    return S;
}
Matrix<Symbolic> Symbolic::gradient(std::initializer_list<std::string> in_list) const
{

    std::vector<Symbolic> gradvec;
    if (in_list.size() == 0)
    {
        for (const auto &var : variables_)
        {
            auto pdiff = derivative(var.first);
            gradvec.push_back(pdiff);
        }
    }
    else
    {
        for (auto var : in_list)
        {
            auto pdiff = derivative(var);
            gradvec.push_back(pdiff);
        }
    }

    auto res = Matrix<Symbolic>{gradvec.size(), 1, gradvec};

    return res;
}

Symbolic Symbolic::laplacian() const
{
    Symbolic L = 0;
    for (const auto &var : variables_)
    {
        L += derivative(var.first, 2);
    }
    return L;
}

Symbolic Symbolic::diff_operator() const
{
    Symbolic L = 0;
    for (const auto &var : variables_)
    {
        L += derivative(var.first, 1);
    }
    return L;
}

SquareMatrix<Symbolic> Symbolic::hessian() const
{
    auto N = variables_.size();
    auto H = SquareMatrix<Symbolic>(N, N);
    auto vars = std::vector<std::string>{};
    for (auto &var : variables_)
    {
        vars.push_back(var.first);
    }
    for (size_type i = 0; i != N; ++i)
    {
        for (size_type j = 0; j != N; ++j)
        {
            H(i, j) = derivative(vars[i]).derivative(vars[j]);
        }
    }
    return H;
}

// Unary operators

Symbolic Symbolic::operator-() const
{
    auto res = *this;
    res.expr_ = -res.expr_;
    return res;
}
Symbolic pow(const Symbolic &S1, const Symbolic &S2)
{
    auto res = S1;
    res.expr_ = GiNaC::pow(res.expr_, S2.expr_);
    return res;
}
Symbolic exp(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::exp(res.expr_);
    return res;
}
Symbolic log(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::log(res.expr_);
    return res;
}
Symbolic sin(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::sin(res.expr_);
    return res;
}
Symbolic cos(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::cos(res.expr_);
    return res;
}
Symbolic tan(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::tan(res.expr_);
    return res;
}
Symbolic asin(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::asin(res.expr_);
    return res;
}
Symbolic acos(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::acos(res.expr_);
    return res;
}
Symbolic atan(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::atan(res.expr_);
    return res;
}
Symbolic sinh(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::sinh(res.expr_);
    return res;
}
Symbolic cosh(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::cosh(res.expr_);
    return res;
}
Symbolic tanh(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::tanh(res.expr_);
    return res;
}
Symbolic asinh(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::asinh(res.expr_);
    return res;
}
Symbolic acosh(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::acosh(res.expr_);
    return res;
}
Symbolic atanh(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::atanh(res.expr_);
    return res;
}
Symbolic sqrt(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::sqrt(res.expr_);
    return res;
}
Symbolic abs(const Symbolic &S)
{
    auto res = S;
    res.expr_ = GiNaC::abs(res.expr_);
    return res;
}

Symbolic fabs(const Symbolic &S) { return abs(S); }

// Binary operators between Symbols

Symbolic &Symbolic::operator+=(const Symbolic &S)
{
    add_variables(S);
    expr_ += S.expr_;
    return *this;
}

Symbolic operator+(const Symbolic &S1, const Symbolic &S2)
{
    auto res = S1;
    res += S2;
    return res;
}

Symbolic &operator-=(Symbolic &S1, const Symbolic &S2) { return S1 += -S2; }

Symbolic operator-(const Symbolic &S1, const Symbolic &S2)
{
    auto res = S1;
    return res -= S2;
}

Symbolic &Symbolic::operator*=(const Symbolic &S)
{
    add_variables(S);
    expr_ *= S.expr_;
    return *this;
}

Symbolic operator*(const Symbolic &S1, const Symbolic &S2)
{
    auto res = S1;
    res *= S2;
    return res;
}

Symbolic &Symbolic::operator/=(const Symbolic &S)
{
    add_variables(S);
    expr_ /= S.expr_;
    return *this;
}

Symbolic operator/(const Symbolic &S1, const Symbolic &S2)
{
    auto res = S1;
    return res /= S2;
}

// Functions on containers of Symbolics
template <typename MatrixType, typename... Args>
typename std::enable_if<Is_matrix<MatrixType>, MatrixType>::type
evaluate(const MatrixType &A, Args... args)
{
    auto res = A;
    for (auto &element : res)
    {
        element = element.evaluate(args...);
    }
    return res;
}

template <template <typename, typename...> class Container, typename T,
          typename... Dims, typename... Args>
Container<Symbolic> evaluate(const Container<T, Dims...> &C, Args... args)
{
    auto res = C;
    for (auto &element : res)
    {
        element = element.evaluate(args...);
    }
    return res;
}

template <template <typename> class Container, typename T>
Container<Symbolic> derivative(const Container<T> &C,
                               const std::string &variable,
                               unsigned short order = 1)
{
    auto res = Container<Symbolic>{};
    for (const auto &element : C)
    {
        res.push_back(element.derivative(variable, order));
    }
    return res;
}

// I/O
std::ostream &operator<<(std::ostream &os, const Symbolic &S)
{
    os << S.expr_;
    return os;
}

// Set static variables
size_type Symbolic::no_variables_{0};

Symbolic divergence(const Vector<Symbolic> &invec, std::initializer_list<std::string> in_list = {})
{
    Symbolic divsymb;
    if (in_list.size() == 0)
    {
        std::map<std::string, std::shared_ptr<GiNaC::symbol>> Variables_;

        for (auto elem : invec)
        {
            for (auto var : elem.variables_)
            {
                Variables_.emplace(var.first, var.second); //champion::Union sometime in the future
            }
        }

        for (size_type i = 0; i < invec.size(); ++i)
        {
            auto it = Variables_.begin();
            std::advance(it, i);
            auto var_used = it->first;
            auto pdiff_ = invec[i].derivative(var_used);
            divsymb += pdiff_;
        }
    }
    else
    {

        for (size_type i = 0; i < invec.size(); ++i)
        {
            auto pdiff_ = invec[i].derivative(in_list.begin()[i]);
            divsymb += pdiff_;
        }
    }
    return divsymb;
}

} // namespace champion

#endif