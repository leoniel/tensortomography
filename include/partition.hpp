#ifndef CHAMPION_PARTITION_HPP
#define CHAMPION_PARTITION_HPP

#include <cmath>
#include <vector>

#include <omp.h>

#include "set_precision.hpp"

namespace champion {

template <typename T>
std::vector<T> map_to_standard_normal(const std::vector<T> &vec) {
    auto res = vec;
    auto mu = mean(vec);
    auto sigma = standard_deviation(vec);
    for (auto &el : res) {
        el -= mu;
        el /= sigma;
    }
    return res;
}

template <typename T> T compute_Boltzmann_factor(T energy, T beta) {
    return std::exp(-beta * energy);
}

template <typename T>
std::vector<T> compute_Boltzmann_pmf(const std::vector<T> &energies, T beta,
                                     bool normalize = true) {
    auto pmf = energies;
    T sum = 0;
#pragma omp parallel for reduction(+ : sum)
    for (size_type i = 0; i < energies.size(); ++i) {
        sum += pmf[i] = compute_Boltzmann_factor(energies[i], beta);
    }
    if (normalize) {
#pragma omp parallel for
        for (size_type i = 0; i < pmf.size(); ++i) {
            pmf[i] /= sum;
        }
    }
    return pmf;
}

template <typename T> std::vector<T> cmf_from_pmf(const std::vector<T> &pmf) {
    auto cmf = pmf;
#pragma omp parallel for
    for (size_type i = 0; i < pmf.size(); ++i) {
        auto &el = cmf[i] = 0;
#pragma omp parallel for reduction(+ : el)
        for (size_type j = 0; j <= i; ++j) {
            el += pmf[j];
        }
    }
    return cmf;
}

template <typename T>
std::vector<T> compute_Boltzmann_cmf(const std::vector<T> &energies, T beta,
                                     bool normalize = true) {
    auto pmf = energies;
    if (normalize) {
        pmf = compute_Boltzmann_pmf(energies, beta);
    } else {
        pmf = compute_Boltzmann_pmf(energies, beta, false);
    }
    auto cmf = cmf_from_pmf(pmf);
    return cmf;
}

} // namespace champion

#endif