/******************************************************************************
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

SOURCE FILE: statistics.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2018-07-12
MODIFIED BY:

DESCRIPTION: Collection of functions related to statistics

\******************************************************************************/

#ifndef CHAMPION_STATISTICS_HPP
#define CHAMPION_STATISTICS_HPP

#include <algorithm>
#include <cmath>
#include <iterator>
#include <utility>
#include <vector>

#include "concatenate.hpp"
#include "set_precision.hpp"
#include "type_inference.hpp"
#include "type_traits.hpp"

namespace champion {

/********************************* Mean **************************************/

// Primary implementation template

template <typename T, typename... Args>
void mean_impl(T &sum, size_type &no_els, Args... args) {
    return;
}

// Implementation forward declarations

template <typename T, typename Number, typename... Args>
std::enable_if_t<Is_number<Number>, void> mean_impl(T &sum, size_type &no_els,
                                                    Number x, Args... args);

template <typename T, typename Container, typename... Args>
std::enable_if_t<Is_container<Container>, void>
mean_impl(T &sum, size_type &no_els, const Container &container, Args... args);

template <typename T, typename Iter, typename... Args>
std::enable_if_t<Is_iterator<Iter>, void>
mean_impl(T &sum, size_type &no_els, Iter begin, Iter end, Args... args);

// Implementation definitions

template <typename T, typename Number, typename... Args>
std::enable_if_t<Is_number<Number>, void> mean_impl(T &sum, size_type &no_els,
                                                    Number x, Args... args) {
    sum += x;
    ++no_els;
    mean_impl(sum, no_els, args...);
}

template <typename T, typename Container, typename... Args>
std::enable_if_t<Is_container<Container>, void>
mean_impl(T &sum, size_type &no_els, const Container &container, Args... args) {
    for (const auto &el : container) {
        sum += el;
        ++no_els;
    }
    mean_impl(sum, no_els, args...);
}

template <typename T, typename Iter, typename... Args>
std::enable_if_t<Is_iterator<Iter>, void>
mean_impl(T &sum, size_type &no_els, Iter begin, Iter end, Args... args) {
    for (auto it = begin; it != end; ++it) {
        sum += *it;
        ++no_els;
    }
    mean_impl(sum, no_els, args...);
}

// APIs

template <typename T, typename Iter, typename... Args>
typename std::enable_if_t<Is_iterator<Iter>, T> mean(Iter begin, Iter end,
                                                     Args... args) {
    T sum = 0;
    size_type no_els = 0;
    mean_impl(sum, no_els, begin, end, args...);
    if (no_els == 0) {
        return 0;
    }
    return sum / (T)no_els;
}

template <typename Iter, typename... Args>
typename std::enable_if_t<Is_iterator<Iter>, real> mean(Iter begin, Iter end,
                                                        Args... args) {
    return mean<real>(begin, end, args...);
}

template <typename T, typename Container, typename... Args>
typename std::enable_if_t<Is_container<Container>, T>
mean(const Container &container, Args... args) {
    return mean<T>(container.begin(), container.end(), args...);
}

template <typename Container, typename... Args>
typename std::enable_if_t<Is_container<Container>, real>
mean(const Container &container, Args... args) {
    return mean<real>(container, args...);
}

template <typename T, typename Number, typename... Args>
typename std::enable_if_t<Is_number<Number>, T> mean(Number x, Args... args) {
    T sum = 0;
    size_type no_els = 0;
    mean_impl(sum, no_els, x, args...);
    if (no_els == 0) {
        return 0;
    }
    return sum / (T)no_els;
}

template <typename Number, typename... Args>
typename std::enable_if_t<Is_number<Number>, real> mean(Number x,
                                                        Args... args) {
    return mean<real>(x, args...);
}

/******************************* Variance ************************************/

// Primary implementation template
template <typename T, typename... Args>
void var_impl(T &sum, size_type &no_els, const T &mean, Args... args) {
    return;
}

// Implementation forward declarations

template <typename T, typename Number, typename... Args>
std::enable_if_t<Is_number<Number>, void>
var_impl(T &sum, size_type &no_els, const T &mean, Number number, Args... args);

template <typename T, typename Container, typename... Args>
std::enable_if_t<Is_container<Container>, void>
var_impl(T &sum, size_type &no_els, const T &mean, const Container &container,
         Args... args);

template <typename T, typename Iterator, typename... Args>
std::enable_if_t<Is_iterator<Iterator>, void>
var_impl(T &sum, size_type &no_els, const T &mean, Iterator begin, Iterator end,
         Args... args);

// Implementation definitions

template <typename T, typename Number, typename... Args>
std::enable_if_t<Is_number<Number>, void> var_impl(T &sum, size_type &no_els,
                                                   const T &mean, Number number,
                                                   Args... args) {
    auto diff = number - mean;
    auto sq = diff * diff;
    sum += sq;
    ++no_els;
    var_impl<T>(sum, no_els, mean, args...);
}

template <typename T, typename Container, typename... Args>
std::enable_if_t<Is_container<Container>, void>
var_impl(T &sum, size_type &no_els, const T &mean, const Container &container,
         Args... args) {
    for (const auto &el : container) {
        auto diff = el - mean;
        auto sq = diff * diff;
        sum += sq;
        ++no_els;
    }
    var_impl<T>(sum, no_els, mean, args...);
}

template <typename T, typename Iterator, typename... Args>
std::enable_if_t<Is_iterator<Iterator>, void>
var_impl(T &sum, size_type &no_els, const T &mean, Iterator begin, Iterator end,
         Args... args) {
    for (auto it = begin; it != end; ++it) {
        auto diff = *it - mean;
        auto sq = diff * diff;
        sum += sq;
        ++no_els;
    }
    var_impl<T>(sum, no_els, mean, args...);
}

// API
template <typename T = real, typename... Args> T variance(Args... args) {
    T avg = mean<T>(args...);
    T sum = 0;
    size_type no_els = 0;
    var_impl<T>(sum, no_els, avg, args...);
    if (no_els == 0) {
        return 0;
    }
    auto var = sum / (T)no_els;
    return var;
}

/************************** Standard deviation *******************************/

template <typename T = real, typename... Args>
T standard_deviation(Args... args) {
    auto var = variance(args...);
    auto stddev = std::sqrt(var);
    return stddev;
}

/******************************* Median **************************************/

template <typename T, typename... Args> T median(Args... args) {
    auto vec = concatenate<T>(args...);
    std::sort(vec.begin(), vec.end());
    auto N = vec.size();
    if ((N % 2) == 0) {
        return (vec[N / 2 - 1] + vec[N / 2]) / 2;
    } else {
        return vec[N / 2];
    }
}

template <typename... Args> real median(Args... args) {
    return median<real>(args...);
}

/********************************** Min ***************************************/

template <typename T = real, typename... Args> T min(Args... args) {
    auto vec = concatenate<T>(args...);
    std::sort(vec.begin(), vec.end());
    return vec[0];
}

/********************************** Max ***************************************/

template <typename T, typename... Args> T max(Args... args) {
    auto vec = concatenate<T>(args...);
    std::sort(vec.begin(), vec.end());
    return vec[vec.size() - 1];
}

template <typename... Args> real max(Args... args) {
    return max<real>(args...);
}

/******************************** Min/Max *************************************/

template <typename T, typename... Args> std::pair<T, T> minmax(Args... args) {
    auto vec = concatenate<T>(args...);
    std::sort(vec.begin(), vec.end());
    return {vec[0], vec[vec.size() - 1]};
}

template <typename... Args> std::pair<real, real> minmax(Args... args) {
    return minmax<real>(args...);
}

} // namespace champion

#endif
