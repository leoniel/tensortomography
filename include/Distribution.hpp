#ifndef CHAMPION_DISTRIBUTION_H
#define CHAMPION_DISTRIBUTION_H

#include <iostream>
#include <vector>

#include "set_precision.hpp"

namespace champion {

template <typename T1, typename T2> struct Distribution {
  public:
    // Default constructor
    inline Distribution() : x{}, y{} {}

    // Construct from x and y vectors
    inline Distribution(const std::vector<T1> &x_, const std::vector<T2> &y_)
        : x{x_}, y{y_} {}

    size_type size() const { return x.size(); }

    std::vector<T1> x;
    std::vector<T2> y;
};

template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &os, const Distribution<T1, T2> &dist) {
    os << "x = {" << dist.x[0];
    for (size_type i = 1; i != dist.size(); ++i) {
        os << ", " << dist.x[i];
    }
    os << "}" << std::endl;
    os << "y = {" << dist.y[0];
    for (size_type i = 1; i != dist.size(); ++i) {
        os << ", " << dist.y[i];
    }
    os << "}" << std::endl;

    return os;
}

} // namespace champion

#endif // DISTRIBUTION_H
