#ifndef CHAMPION_ALGORITHM_HPP
#define CHAMPION_ALGORITHM_HPP

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstddef>
#include <iostream>
#include <limits>
#include <list>
#include <map>
#include <numeric>
#include <queue>
#include <stdexcept>
#include <utility>
#include <vector>

#include "Distribution.hpp"
#include "Edge.hpp"
#include "Point.hpp"
#include "Vector.hpp"
#include "set_precision.hpp"

namespace champion {

// Forward declarations
template <typename T1, typename T2> class Curve;

template <typename T1, typename T2> T1 mod(T1 i, T2 j) {
    if (j <= 0) {
        throw std::runtime_error{
            "CHAMPION::mod(): second argument must be positive."};
    }
    if (std::round(i) != i || std::round(j) != j) {
        throw std::runtime_error{
            "CHAMPION::mod(): both arguments must be integer-valued."};
    }
    if (i < 0) {
        T1 multiple = std::ceil(i / j);
        i -= multiple * j;
    } else if (i >= 0) {
        T1 multiple = std::floor(i / j);
        i -= multiple * j;
    } else {
        throw std::runtime_error{"CHAMPION::mod(): data types passed must be "
                                 "either non-negative or negative."};
    }
    return i;
}

template <typename T> bool is_even(T value) { return (mod(value, 2) == 0); }
template <typename T> bool is_odd(T value) { return !is_even(value); }

template <typename T>
bool is_equal(const std::vector<T> &v1, const std::vector<T> &v2) {
    auto N = v1.size();
    if (v2.size() != N) {
        return false;
    }
    for (size_type i = 0; i != N; ++i) {
        if (v1[i] != v2[i]) {
            return false;
        }
    }
    return true;
}

// Smooths vector vec by calculating moving average with interval steps
// before and after
template <typename T>
std::vector<T> moving_average(const std::vector<T> &vec, size_type interval) {
    auto size = vec.size();
    std::vector<T> result(size);
#pragma omp parallel for
    for (int i = 0; i < size; ++i) {
        T sum = 0;
        for (int j = i - interval; j < i + interval; ++j) {
            if (j < 0)
                sum += vec[0];
            else if (j >= size)
                sum += vec[size - 1];
            else
                sum += vec[j];
        }
        result[i] = sum / (2 * interval + 1);
    }
    return result;
}

// Smooths vector by averaging bin_size adjacent datapoints
template <typename T>
std::vector<T> pool_data(const std::vector<T> &vec, size_type bin_size) {
    auto n = vec.size() / bin_size;
    if (bin_size * n != vec.size()) {
        ++n;
    }
    auto res = std::vector<T>(n);
    auto in_it = vec.begin();
    auto out_it = res.begin();
    while (in_it < vec.end()) {
        for (size_type i = 0; i < bin_size; ++i) {
            *out_it += *in_it++;
        }
        *out_it /= static_cast<T>(bin_size);
        ++out_it;
    }
    return res;
}

template <typename T1, typename T2>
Curve<T1, T2> pool_data(const Curve<T1, T2> &curve, size_type bin_size) {
    auto x = pool_data(curve.x(), bin_size);
    auto y = pool_data(curve.y(), bin_size);
    return {x, y};
}

// Calculates first derivative vector of vector vec using central
// differencing. Independent variable vector is optional and has default
// stepsize 1.
template <typename T> std::vector<T> diff(const std::vector<T> &y) {
    std::vector<int> x(y.size());
    std::iota(x.begin(), x.end(), 0);
    return diff(y, x);
}

template <typename T1, typename T2>
std::vector<T1> diff(const std::vector<T1> &y, const std::vector<T2> &x) {
    auto size = y.size();
    std::vector<T1> result(size);
    result[0] = (y[1] - y[0]) / (x[1] - x[0]);
    for (size_type i = 1; i != size - 1; ++i) {
        result[i] = (y[i + 1] - y[i - 1]) / (x[i + 1] - x[i - 1]);
    }
    result[size - 1] =
        (y[size - 1] - y[size - 2]) / (x[size - 1] - x[size - 2]);

    return result;
}

template <typename T1, typename T2>
T1 diff(const std::vector<T1> &y, const std::vector<T2> &x, std::size_t i) {
    auto size = y.size();
    if (i > 0 && i < size - 1) {
        return (y[i + 1] - y[i - 1]) / (x[i + 1] - x[i - 1]);
    } else if (i == 0)
        return (y[1] - y[0]) / (x[1] - x[0]);
    else if (i == size - 1)
        return (y[size - 1] - y[size - 2]) / (x[size - 1] - y[size - 2]);
    else {
        std::cout << "Something went wrong" << std::endl;
        exit(1);
    }
}

// Calculates second derivative vector of vector vec using central
// differencing. Independent variable vector is optional and has default
// stepsize 1.
template <typename T> std::vector<float> diff2(const std::vector<T> &y) {
    std::vector<int> x(y.size());
    std::iota(x.begin(), x.end(), 0);
    return diff2(y, x);
}

template <typename T1, typename T2>
std::vector<float> diff2(const std::vector<T1> &y, const std::vector<T2> &x) {
    int size = y.size();
    std::vector<float> result(size);
    for (size_type i = 1; i != size - 1; ++i) {
        result[i] = ((y[i + 1] - y[i]) / (x[i + 1] - x[i]) -
                     (y[i] - y[i - 1]) / (x[i] - x[i - 1])) /
                    ((x[i + 1] - x[i - 1]) / 2);
    }
    result[0] = result[1];
    result[size - 1] = result[size - 2];
    return result;
}

template <typename T> void update_average(T &avg, const T &val, std::size_t n) {
    avg = avg * (n - 1) / n + val / n;
}

// Return a histogram (std::vector containing number of occurrences of
// values in each bin)
template <class container>
Distribution<real, unsigned>
histogram(const container &c, size_type num_bins,
          real min = std::numeric_limits<real>::max(),
          real max = std::numeric_limits<real>::max()) {
    // Return empty Distribution if the container passed is empty
    if (c.begin() == c.end())
        return Distribution<real, unsigned>();
    std::vector<real> x(num_bins);
    std::vector<unsigned> hist(num_bins);
    // If min and/or max are unset, use defaults
    // Default min = 0
    if (min == std::numeric_limits<real>::max())
        min = 0;
    // Default max = max(c)
    if (max == std::numeric_limits<real>::max())
        max = static_cast<real>(*std::max_element(c.begin(), c.end()));
    auto delta = (max - min) / num_bins;
    for (size_type i = 0; i != num_bins; ++i) {
        x[i] = delta * (i + 0.5);
    }
    for (auto it = c.begin(); it != c.end(); ++it) {
        real ceil = min;
        for (size_type i = 0; i != num_bins; ++i) {
            ceil += delta;
            if (*it <= ceil) {
                ++hist[i];
                break;
            }
        }
    }
    return Distribution<real, unsigned>(x, hist);
}

real squared_magnitude(real x) { return x * x; }

template <template <typename> class C, typename T>
T squared_magnitude(const C<T> &x) {
    T result = 0;
    for (auto xi : x) {
        result += xi * xi;
    }
    return result;
}

template <typename T> real magnitude(const T &x) {
    return std::sqrt(squared_magnitude(x));
}

template <typename T> size_type size(const T &x) {
    size_type sz = 0;
    for (auto xi : x) {
        (void)xi; // Does nothing (to suppress unused variable warning)
        ++sz;
    }
    return sz;
}

template <typename container> real sum_squares(const container &C) {
    real result = 0;
    for (const auto &Ci : C) {
        result += squared_magnitude(Ci);
    }
    return result;
}

template <typename container> real sum_magnitudes(const container &C) {
    real result = 0;
    for (const auto &Ci : C) {
        result += magnitude(Ci);
    }
    return result;
}

template <typename container> real mean_square(const container &C) {
    return sum_squares(C) / C.size();
}

template <typename container> real root_mean_square(const container &C) {
    return std::sqrt(mean_square(C));
}

template <typename container> real mean_magnitude(const container &C) {
    return sum_magnitudes(C) / C.size();
}

std::vector<size_type> sample(size_type no_points, size_type no_samples) {
    std::random_device rd{};
    std::mt19937 gen{rd()};
    auto draw = std::uniform_int_distribution<size_type>{0, no_points - 1};
    auto data_points = std::vector<size_type>(no_samples, 0);
    auto eval_fraction = no_samples / static_cast<real>(no_points);
    if (eval_fraction == 1) {
        // Choose all available data points
        std::iota(data_points.begin(), data_points.end(), 0);
    } else if (eval_fraction < 0.5) {
        // Choose which datapoints to evaluate
#pragma omp parallel for
        // Data race could theoretically cause duplicates, but shouldn't be
        // too much of a problem for sparse sampling of training data
        for (size_type i = 0; i < data_points.size(); ++i) {
            size_type new_pt = 0;
            do {
                new_pt = draw(gen);
            } while (std::find(data_points.begin(), data_points.end(),
                               new_pt) != data_points.end());
            data_points[i] = new_pt;
        }
    } else {
        // Choose which datapoints not to evaluate
        std::vector<size_type> excluded((1 - eval_fraction) * no_points);
#pragma omp parallel for
        for (size_type i = 0; i < excluded.size(); ++i) {
            unsigned long new_pt = 0;
            do {
                new_pt = draw(gen);
            } while (std::find(excluded.begin(), excluded.end(), new_pt) !=
                     excluded.end());
            excluded[i] = new_pt;
        }
        size_type index = 0;
        // Fill up datapoints vector
#pragma omp parallel for
        for (size_type i = 0; i < no_points; ++i) {
            if (std::find(excluded.begin(), excluded.end(), i) ==
                excluded.end()) {
                data_points[index++] = i;
            }
        }
    }
    return data_points;
}

template <typename T, class Predicate>
std::vector<std::vector<size_type>>
get_equivalent_indices(const std::vector<T> &vec, Predicate equivalent) {
    auto unique_elements = std::vector<const T *>{};
    auto equivalent_indices = std::vector<std::vector<size_type>>{};
    for (size_type i = 0; i != vec.size(); ++i) {
        auto it = unique_elements.begin();
        for (; it != unique_elements.end(); ++it) {
            if (equivalent(**it, vec[i])) {
                break;
            }
        }
        if (it == unique_elements.end()) {
            unique_elements.push_back(&vec[i]);
            equivalent_indices.push_back({i});
        } else {
            equivalent_indices[it - unique_elements.begin()].push_back(i);
        }
    }
    return equivalent_indices;
}

// Reorder the elements of a single subscriptable container, in the order
// specified by key, so that key[i] is the index of the element that will be
// placed on position i in the reordered container
template <class C> void reorder(const std::vector<size_type> &key, C &cont) {
    std::vector<typename C::iterator::value_type> copy{cont.begin(),
                                                       cont.end()};
    size_type N = key.size();
    assert(cont.size() == N);
    for (size_type i = 0; i != N; ++i) {
        cont[i] = copy[key[i]];
    }
}

// Reorder the elements of an arbitrary number of subscriptable containers,
// in the order specified by key, so that key[i] is the index of the element
// that will be placed on position i in the reordered container.
// Reorders the first container apart from key by calling the non-variadic
// function template reorder, and calls itself recursively until reaching
// the base case for which the non-variadic version is also called.
template <typename C1, typename... Ci>
void reorder(std::vector<size_type> key, C1 &container, Ci &... containers) {
    reorder(key, container);
    reorder(key, containers...);
}

// Sort an arbitrary number of STL compliant containers consistently, i.e.
// with the same permutation, determined by the first container,applied to
// all containers. Returns the sorting key, where key is a
// std::vector<size_type> and key[i] is the index of the element that is
// placed on position i upon sorting. Uses stable_sort so that equal valued
// elements in the first container are not shuffled
template <typename C1, typename... Ci>
std::vector<size_type> sort_consistently(C1 &container, Ci &... containers) {
    auto N = container.size();
    auto key = std::vector<size_type>(N, N);
    std::iota(key.begin(), key.end(), 0);
    std::stable_sort(key.begin(), key.end(),
                     [&container](size_type i1, size_type i2) {
                         return container[i1] < container[i2];
                     });
    reorder(key, container, containers...);
    return key;
}

// Relabel the vertex indices in a std::vector of champion::Edge:s,
// and sort them
void relabel_edges(const std::vector<size_type> &key,
                   std::vector<Edge> &edges) {
    auto N = edges.size();
    auto new_edges = std::vector<Edge>(N, {N, N});
    for (size_type i = 0; i != N; ++i) {
        auto replace = key[i];
        for (size_type j = 0; j != N; ++j) {
            if (edges[j].first == replace) {
                new_edges[j].first = i;
            }
            if (edges[j].second == replace) {
                new_edges[j].second = i;
            }
        }
    }
    // Replace the original edge vector with the relabeled one
    edges = std::move(new_edges);
    // Sort edges internally so that first < second
    for (auto &ei : edges) {
        if (ei.first > ei.second) {
            std::swap(ei.first, ei.second);
        }
    }
    // Sort vector of edges
    std::sort(edges.begin(), edges.end());
}

template <typename C> bool is_reverse(const C &c1, const C &c2) {
    if (c1.size() != c2.size()) {
        return false;
    }
    auto it_2 = c2.rbegin();
    for (auto it_1 = c1.begin(); it_1 != c1.end(); ++it_1) {
        if (*it_1 != *it_2++) {
            return false;
        }
    }
    return true;
}

// Return a vector of vectors of all the ways in which one element can be
// removed from each of the input vector
template <typename T>
std::vector<std::vector<T>> remove_one(const std::vector<std::vector<T>> &vec) {
    auto result = std::vector<std::vector<T>>();
    auto res_index = 0;
    for (const auto &v : vec) {
        for (size_type i = 0; i != v.size(); ++i) {
            result.push_back({});
            for (size_type j = 0; j != v.size(); ++j) {
                if (i != j) {
                    result[res_index].push_back(v[j]);
                }
            }
            ++res_index;
        }
    }
    return result;
}

// Special case of remove_one acting on only a single vector of elements
template <typename T>
std::vector<std::vector<T>> remove_one(const std::vector<T> &vec) {
    remove_one(std::vector<std::vector<T>>{{vec}});
}

// Return all distinct (unordered) sets of n individuals from vec
template <typename T>
std::vector<std::vector<T>> pick(const std::vector<T> &vec, size_type n) {
    if (vec.size() < n) {
        return {};
    }
    if (vec.size() == n) {
        return {vec};
    }
    auto result = std::vector<std::vector<T>>{{vec}};
    for (size_type i = 0; i < vec.size() - n; ++i) {
        result = remove_one(result);
    }
    for (auto &res_i : result) {
        std::sort(res_i.begin(), res_i.end());
    }
    std::unique(result.begin(), result.end());
    return result;
}

// Remove the elements at the indices given in the first argument of the
// vector given as second argument
template <typename T>
void remove_indices(const std::vector<size_type> &indices,
                    std::vector<T> &vec) {
    if (indices.empty()) {
        return;
    }
    auto it = vec.end();
    while (it != vec.begin()) {
        --it;
        if (std::find(indices.begin(), indices.end(), it - vec.begin()) !=
            indices.end()) {
            vec.erase(it);
        }
    }
}

template <typename T> std::vector<T> &remove_duplicates(std::vector<T> &vec) {
    if (vec.size() <= 1) {
        return vec;
    }
    auto remove_list = std::vector<size_type>{};
    for (auto it = vec.begin() + 1; it != vec.end(); ++it) {
        for (auto itt = vec.begin(); itt != it; ++itt) {
            if (*it == *itt) {
                remove_list.push_back(itt - vec.begin());
            }
        }
    }
    remove_indices(remove_list, vec);
    return vec;
}

// Given 1) a set of independent choices where for each choice a number of
// alternatives exists, and 2) the set of current options, give the next
// combination of options (where next is defined by analogy to a number
// system with potentially different base for each digit ). The first
// argument encodes the current combination of options by giving the index
// (starting from 0) selected for each choice. The second argument gives the
// number of available options for each choice. Returns whether overflow
// occurred in this number system.
bool next_index_combination(std::vector<size_type> &index_vec,
                            const std::vector<size_type> &dim_vec) {
    size_type N = index_vec.size();
    assert(N == dim_vec.size());
    bool overflow_flag = true;
    for (int i = N - 1; i >= 0; --i) {
        ++index_vec[i];
        if (index_vec[i] == dim_vec[i]) {
            index_vec[i] = 0;
        } else {
            break;
            overflow_flag = false;
        }
    }
    return overflow_flag;
}

std::vector<real> normalize_probabilities(const std::vector<real> &vec) {
    auto sum = std::accumulate(vec.begin(), vec.end(), 0.0);
    auto P = vec;
    for (auto &P_i : P) {
        P_i /= sum;
    }
    return P;
}

// Return sorted vector of the indices of the first (by "<") n elements
template <typename T>
std::vector<size_type> get_first_n_indices(const std::vector<T> &vec,
                                           size_type n) {
    if (n == 0) {
        return {};
    }
    // Initialize vector of indices with the first n indices
    std::vector<size_type> result(n);
    // Sort the first n indices wrt vector elements
    std::iota(result.begin(), result.end(), 0);
    std::sort(result.begin(), result.end(),
              [&vec](size_type i, size_type j) { return vec[i] < vec[j]; });
    // Loop through the remaining vector elements and replace result indices
    // as needed. Keep the vector of the first n indices sorted
    for (size_type i = n; i < vec.size(); ++i) {
        // Check if index i qualifies for the current index vector
        if (vec[i] < vec[result[n - 1]]) {
            // Loop through current indices to see where to put i
            size_type j = 0;
            while (vec[i] >= vec[result[j]]) {
                ++j;
            }
            // move the indices greater than the current one and insert it
            for (size_type k = n - 1; k > j; --k) {
                result[k] = result[k - 1];
            }
            result[j] = i;
        }
    }
    return result;
}

// Same as above but with conditions on the indices and elements
template <typename T, typename Predicate>
std::vector<size_type> get_first_n_indices(const std::vector<T> &vec,
                                           size_type n,
                                           const Predicate &condition) {
    auto filtered_vec = vec;
    auto index_vec = std::vector<size_type>(vec.size(), 0);
    std::iota(index_vec.begin(), index_vec.end(), 0);
    auto remove_list = std::vector<size_type>{};
    for (size_type i = 0; i != vec.size(); ++i) {
        if (!condition(filtered_vec[i], i)) {
            remove_list.push_back(i);
        }
    }
    remove_indices(remove_list, filtered_vec);
    remove_indices(remove_list, index_vec);

    auto first_n = get_first_n_indices(filtered_vec, n);
    for (size_type i = 0; i != first_n.size(); ++i) {
        first_n[i] = index_vec[first_n[i]];
    }
    return first_n;
}

template <typename T, typename Compare = std::less<T>>
std::vector<T> get_first_n(const std::vector<T> &vec, size_type n,
                           Compare compare) {
    std::vector<T> result(vec.begin(), vec.begin() + n);
    // Initialize iterator to maximum element in result vector
    auto max_el = std::max_element(result.begin(), result.end());
    for (auto it = vec.begin() + n + 1; it < vec.end(); ++it) {
        if (compare(*it, *max_el)) {
            std::swap(*it, *max_el);
            max_el = std::max_element(result.begin(), result.end());
        }
    }
    return std::move(result);
}

template <typename T> real direction_cosine(Vector<T> v1, Vector<T> v2) {
    v1 = v1.normalize();
    v2 = v2.normalize();
    auto dir_cos = dot(v1, v2);
    return dir_cos;
}

} // namespace champion

#endif
