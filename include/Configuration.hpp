/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                            ANALYSIS         TOOLKIT                          |
|                                                                              |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: ForceCenterType.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:

\******************************************************************************/

#ifndef CHAMPION_CONFIGURATION_HPP
#define CHAMPION_CONFIGURATION_HPP

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdarg>
#include <deque>
#include <functional>
#include <iostream>
#include <memory>
#include <type_traits>
#include <vector>

#include "Body.hpp"
#include "BodyType.hpp"
#include "ForceCenter.hpp"
#include "ForceCenterType.hpp"
#include "Matrix.hpp"
#include "ObjectIndex.hpp"
#include "Point.hpp"
#include "Rotation.hpp"
#include "Stoichiometry.hpp"
#include "Transformation.hpp"
#include "Vector.hpp"
#include "dynamics.hpp"
#include "io.hpp"
#include "permutation.hpp"
#include "random.hpp"
#include "set_precision.hpp"
#include "stl_io.hpp"

namespace champion {

// Forward declarations
template <level_type n> class ObjectRegistry;
template <level_type n, level_type level> class Configuration;

template <level_type ll, level_type n, level_type level>
std::enable_if_t<(ll >= 0 && ll < n), const Configuration<n, ll> *>
get_conf_ptr_from_reg(const Configuration<n, level> &C, ObjectRegistry<n> &reg);

template <level_type ll, level_type n, level_type level>
std::enable_if_t<(ll < 0 || ll >= n), const Configuration<n, ll> *>
get_conf_ptr_from_reg(const Configuration<n, level> &C, ObjectRegistry<n> &reg);

template <template <level_type, level_type> class T, level_type n,
          level_type level>
std::enable_if_t<(level < n), std::ostream &>
print_all(std::ostream &os, const ObjectRegistry<n> &reg);

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os, const Configuration<n, level> &c);

template <level_type n, level_type level> class map_to_lower_level_impl {
  public:
    map_to_lower_level_impl(Configuration<n, level> &C) : C_{C} {}
    void operator()() {
        // Create bidirectional mapping between this and corresponding
        // Configuration<level - 1>
        auto Cl_ptr = C_.template get_representation<level - 1>();
        // Return immediately if no lower representation
        if (!Cl_ptr) {
            return;
        }
        auto &Cl = *Cl_ptr;
        Cl.higher_level_bodies_ = std::vector<size_type>(Cl.size());
        C_.lower_level_bodies_ = std::vector<std::vector<size_type>>(C_.size());
#pragma omp parallel for schedule(dynamic)
        for (size_type i = 0; i < C_.size(); ++i) {
            C_.lower_level_bodies_[i] = {};
            const auto &body = C_.reg_->get_object(C_.bodies_[i]);
            for (size_type j = 0; j != body.size(); ++j) {
                auto constituent = Cl.get_index_by_id(body[j].get_id());
                Cl.higher_level_bodies_[constituent] = i;
                C_.lower_level_bodies_[i].push_back(constituent);
            }
        }
    }

  private:
    Configuration<n, level> &C_;
};

template <level_type n> class map_to_lower_level_impl<n, 0> {
  public:
    map_to_lower_level_impl(Configuration<n, 0> &C) {}
    void operator()() {}
};

// Class declaration
template <level_type n, level_type level> class Configuration {
  public:
    // Public types
    template <level_type ll> using bt_index = ObjectIndex<BodyType, n, ll>;
    template <level_type ll>
    using conf_index = ObjectIndex<::champion::Configuration, n, ll>;
    template <level_type ll> using body_index = ObjectIndex<Body, n, ll>;
    template <level_type ll> using fc_index = ObjectIndex<ForceCenter, n, ll>;
    template <level_type ll>
    using fct_index = ObjectIndex<ForceCenterType, n, ll>;

    // Public member functions

    // Construct from object registry, ID, bodies and positions
    Configuration(ObjectRegistry<n> &reg, conf_index<level> ID,
                  std::vector<body_index<level>> bodies,
                  std::vector<Point<real>> positions);

    // Construct from lower level Configuration and Body<level>s.
    // Computes center of mass positions for the Body<level>s based on the
    // positions of their constituents
    Configuration(Configuration<n, level - 1> &C,
                  const std::vector<body_index<level>> &bodies);

    // Construct from lower level Configuration by structure discovery of
    // known BodyType<level>s
    Configuration(Configuration<n, level - 1> &C);

    // Access
    size_type size() const { return bodies_.size(); }
    conf_index<level> get_id() const { return id_; }
    size_type get_index_by_id(body_index<level> id) const {
        for (size_type i = 0; i != bodies_.size(); ++i) {
            if (bodies_[i] == id) {
                return i;
            }
        }
        throw std::runtime_error{std::string{"No body<"} +
                                 std::to_string(level) + "> with id " +
                                 std::to_string(id) + " in Configuration"};
    }
    std::vector<size_type> get_lower_level_indices(size_type i) {
        return lower_level_bodies_[i];
    }
    size_type get_higher_level_index(size_type i) {
        return higher_level_bodies_[i];
    }
    const Body<n, level> &get_body(size_type i) const {
        return reg_->get_object(bodies_[i]);
    }
    Body<n, level> &get_body(size_type i) {
        return const_cast<Body<n, level> &>(
            static_cast<const Configuration<n, level> &>(*this).get_body(i));
    }
    ObjectIndex<Body, n, level> get_body_id(size_type i) const {
        return bodies_[i];
    }
    const std::vector<ObjectIndex<Body, n, level>> &get_body_ids() const {
        return bodies_;
    }
    Point<real> get_position(size_type i) const {
        if (i >= positions_.size()) {
            throw std::runtime_error{
                std::string{"CHAMPION::Configuration::get_position(): Out of "
                            "bounds (i = "} +
                std::to_string(i) + ", size of Configuration: " +
                std::to_string(positions_.size())};
        }
        return positions_[i];
    }
    const std::vector<Point<real>> &get_positions() const { return positions_; }

    template <level_type l>
    const Configuration<n, l> *get_representation() const {
        return get_conf_ptr_from_reg<l>(*this, *reg_);
    }
    template <level_type l> Configuration<n, l> *get_representation() {
        return const_cast<Configuration<n, l> *>(
            static_cast<const Configuration *>(this)->get_representation<l>());
    }

    // Modification

    Point<real> compute_center_of_mass(size_type i) {
        std::vector<real> masses;
        std::vector<Point<real>> coordinates;
        auto C_ptr = get_representation<level - 1>();
        if (C_ptr != nullptr) {
            for (auto j : lower_level_bodies_[i]) {
                masses.push_back(C_ptr->get_body(j).type().get_mass());
                coordinates.push_back(C_ptr->get_position(j));
            }
        }
        // Compute relative vectors from first coordinate, to avoid problems
        // with periodic boundary conditions
        std::vector<Vector<real>> rel_coords;
        for (size_type j = 0; j != coordinates.size(); ++j) {
            rel_coords[j] = coordinates[j] - coordinates[0];
        }
        // Compute com Vector<real> in translated coordinate system
        Vector<real> com_rel = {0, 0, 0};
        real total_mass = 0;
        for (size_type j = 0; j != coordinates.size(); ++j) {
            com_rel += masses[j] * rel_coords[j];
            total_mass += masses[j];
        }
        com_rel /= total_mass;
        // Finally, translate back and apply boundary conditions
        return Point<real>{coordinates[0] + com_rel};
    }

    // Translate body #i by V
    void translate(size_type i, Vector<real> V) {
        assert(i < this->size());
        // Apply translation on this level
        positions_[i] += V;
        // Apply translation recursively to lower level representations
        if constexpr (level > 0) {
            auto C_ptr_l = get_representation<level - 1>();
            if (C_ptr_l != nullptr) {
                for (auto ll_i : lower_level_bodies_[i]) {
                    C_ptr_l->translate(ll_i, V);
                }
            }
        }
        // Update recursively higher level centers-of-mass
        if constexpr (level < n - 1) {
            auto C_ptr_h = get_representation<level + 1>();
            if (C_ptr_h != nullptr) {
                C_ptr_h->compute_center_of_mass(higher_level_bodies_[i]);
            }
        }
    }
    // Rotate body #i by R about its center of mass
    void rotate(size_type i, Rotation<real> R) { rotate(i, R, positions_[i]); }
    // Rotate body #i by R about Vector<real> center
    void rotate(size_type i, Rotation<real> R, Vector<real> center) {
        assert(i < this->size());
        // Apply rotation recursively to lower level representations
        if constexpr (level > 0) {
            auto C_ptr_l = get_representation<level - 1>();
            for (auto &c_j : lower_level_bodies_[i]) {
                C_ptr_l->rotate(c_j, R, center);
            }
        }
        // Update recursively higher level centers-of-mass if needed
        if constexpr (level < n - 1) {
            if (center != Vector<real>{positions_[i]}) {
                auto C_ptr_h = get_representation<level + 1>();
                if (C_ptr_h != nullptr) {
                    C_ptr_h->compute_center_of_mass(higher_level_bodies_[i]);
                }
            }
        }
    }
    void set_position(size_type i, Point<real> p) {
        auto displacement = Vector<real>{p - positions_[i]};
        translate(i, displacement);
    }

    void set_positions(std::vector<Point<real>> positions) {
        if (positions.size() != this->size()) {
            throw std::runtime_error{
                "Configuration::set_positions(): Wrong number of positions "
                "given, set_positions() cannot change the number of atoms"};
        }
        for (size_type i = 0; i != this->size(); ++i) {
            set_position(i, positions[i]);
        }
    }

  private:
    // Friends
    friend class Configuration<n, level - 1>;
    friend class Configuration<n, level + 1>;
    friend class map_to_lower_level_impl<n, level>;
    friend class map_to_lower_level_impl<n, level + 1>;
    // Private member functions
    void map_to_lower_level() { return map_to_lower_level_impl{*this}(); }
    // Private data members
    ObjectRegistry<n> *reg_;
    conf_index<level> id_;
    std::vector<body_index<level>> bodies_;
    // Indices into the corresponding Configuration at other levels
    std::vector<std::vector<size_type>> lower_level_bodies_;
    std::vector<size_type> higher_level_bodies_;
    std::vector<Point<real>> positions_;
};

// Member function definitions

// Construct from object registry,ID, bodies and positions
// (lower level Configuration must exist in object registry)
template <level_type n, level_type level>
Configuration<n, level>::Configuration(ObjectRegistry<n> &reg,
                                       conf_index<level> ID,
                                       std::vector<body_index<level>> bodies,
                                       std::vector<Point<real>> positions)
    : reg_{&reg}, id_{ID},
      bodies_(bodies), lower_level_bodies_{}, higher_level_bodies_{},
      positions_(positions) {
    auto N = bodies_.size();
    assert(positions_.size() == N);
    map_to_lower_level();
}

// Construct from lower level Configuration and Body<level>s
// Computes center of mass positions for the Body<level>s based on the
// positions of their constituents
template <level_type n, level_type level>
Configuration<n, level>::Configuration(
    Configuration<n, level - 1> &C,
    const std::vector<body_index<level>> &bodies)
    : reg_{C.reg_}, id_{C.id_}, bodies_{bodies}, lower_level_bodies_{},
      higher_level_bodies_{}, positions_(bodies.size()) {
#pragma omp parallel for schedule(dynamic)
    for (size_type i = 0; i < bodies.size(); ++i) {
        auto &body = reg_->get_object(bodies[i]);
        auto constituent_bodies =
            body.template get_constituent_ids<level - 1>();
        auto N = constituent_bodies.size();
        auto masses = std::vector<real>(N, 0.0);
        auto coordinates = std::vector<Point<real>>(N);
        for (size_type i = 0; i != N; ++i) {
            masses[i] =
                reg_->get_object(constituent_bodies[i]).type().get_mass();
            coordinates[i] =
                C.get_position(C.get_index_by_id(constituent_bodies[i]));
        }
        Point<real> com = center_of_mass(masses, coordinates);
        positions_[i] = com;
    }
    map_to_lower_level();
}

// Construct from lower level Configuration by structure discovery
template <level_type n, level_type level>
Configuration<n, level>::Configuration(Configuration<n, level - 1> &C)
    : reg_{C.reg_}, id_{C.id_}, bodies_{}, lower_level_bodies_{},
      higher_level_bodies_{}, positions_{} {
    std::cout << "Atomic configuration:\n" << C << std::endl;
    // Compute all pairwise distances between lower level bodies in C
    auto distances = SymmetricMatrix<real>(C.size(), C.size());
    for (size_type i = 0; i != C.size(); ++i) {
        for (size_type j = i + 1; j != C.size(); ++j) {
            distances(i, j) = distance(C.positions_[i], C.positions_[j]);
        }
    }
    // Create list of indices to bodies not yet identified
    std::vector<size_type> unidentified(C.size());
    std::iota(unidentified.begin(), unidentified.end(), 0);
    auto body_graphs = std::vector<Graph<Body<n, level - 1>>>{};
    auto FCTs = std::vector<fct_index<level - 1>>(C.size());
    while (!unidentified.empty()) {
        auto body_i = unidentified.front();
        // Create a queue of candidate graphs to build until valid one is found.
        // Vertices consist of index of the current body, id of the postulated
        // fct and a flag for whether its neighborhood has been explored.
        using body_fct_tuple =
            std::tuple<size_type, fct_index<level - 1>, bool>;
        auto graphs = std::deque<Graph<body_fct_tuple>>{};
        auto completed_graphs = std::vector<Graph<body_fct_tuple>>{};
        // Create a vector to hold (and own) the tuples in the created graphs
        auto body_fct_tuples = std::vector<std::unique_ptr<body_fct_tuple>>{};
        // Find the body type index of current body
        auto body_type_index = C.get_body(body_i).type().get_id();
        // Find candidate force center types based on body type and initialize
        // candidate graphs
        for (const auto &fct_el :
             reg_->template get_objects<ForceCenterType, level - 1>()) {
            auto fct_i = fct_el.first;
            auto fct = fct_el.second;
            if (fct.species().get_id() == body_type_index) {
                body_fct_tuples.push_back(
                    std::make_unique<body_fct_tuple>(body_i, fct_i, false));
                auto graph =
                    Graph<body_fct_tuple>{body_fct_tuples.back().get()};
                graphs.push_back(std::move(graph));
            }
        }
        // Extend and fork the first candidate graph until completed or
        // eliminated as a candidate, then move on to the next one
        bool eliminated = false, completed = false;
        auto graph = graphs.front();
        while (!graphs.empty()) {
            // Reset eliminated flag
            eliminated = completed = false;
            while (!eliminated && !completed) {
                graph = graphs.front();
                // The graph selected from graphs is to be either extended,
                // discarded or moved to completed_graphs, so remove it from the
                // queue.
                graphs.pop_front();
                // Try to add nearest neighbors to all unvisited vertices.
                // Fork the graph whenever a neighbor can be of more than
                // one possible fct.

                // Provisionally set completed to true
                completed = true;
                // Extend the graph with neighbors of all unvisisted
                // vertices.
                for (size_type i = 0; i != graph.size(); ++i) {
                    auto &vertex_i = *graph[i];
                    bool &visited = std::get<2>(vertex_i);
                    if (!visited) {
                        // Will visit now, so set visited to true
                        visited = true;
                        // Find neighbor FCTs to this vertex
                        auto fct_id_i = std::get<1>(vertex_i);
                        auto fct = reg_->get_object(fct_id_i);
                        auto fct_neighbors = fct.get_bonded_neighbor_types();
                        // Get the neighbors hitherto discovered in the
                        // graph
                        auto graph_neighbor_vertices = graph.get_neighbors(i);
                        auto graph_neighbors = std::vector<size_type>{};
                        for (const auto &neighbor : graph_neighbor_vertices) {
                            graph_neighbors.push_back(std::get<0>(*neighbor));
                        }
                        // For each graph neighbor, remove one element of
                        // fct_neighbors of the same force center type
                        for (const auto &vertex_j : graph_neighbor_vertices) {
                            auto fct_id_j = std::get<1>(*vertex_j);
                            bool found_fct_match = false;
                            for (auto it = fct_neighbors.begin();
                                 it != fct_neighbors.end(); ++it) {
                                if (*it == fct_id_j) {
                                    graph_neighbors.push_back(
                                        std::get<0>(*vertex_j));
                                    found_fct_match = true;
                                    fct_neighbors.erase(it);
                                    break;
                                }
                            }
                            if (!found_fct_match) {
                                throw std::runtime_error{
                                    "CHAMPION::Configuration: error in "
                                    "structure discovery."};
                            }
                            assert(found_fct_match);
                        }
                        if (fct_neighbors.empty()) {
                            continue;
                        }
                        // If any vertices get new added neighbors, the graph is
                        // not completed.
                        completed = false;
                        // Create list of BodyType indices of the
                        // hypothesized neighbors
                        auto FCT_neighbor_BTs =
                            std::vector<bt_index<level - 1>>{};
                        auto FCT_neighbor_no_BTs =
                            std::map<bt_index<level - 1>, unsigned short>{};
                        for (auto &neighbor : fct_neighbors) {
                            auto bt =
                                reg_->get_object(neighbor).species().get_id();
                            FCT_neighbor_BTs.push_back(bt);
                            ++FCT_neighbor_no_BTs[bt];
                        }
                        // Find the neighboring atoms to add to the graph
                        auto distances_i =
                            distances.get_row(std::get<0>(vertex_i));
                        // Lambda to run some tests on each candidate neighbor
                        // to make sure it is eligible as neighbor for vertex_i
                        auto eligible_neighbor = [&C, &unidentified, &graph,
                                                  &graph_neighbors, &vertex_i,
                                                  &fct_neighbors](
                                                     real d, size_type i,
                                                     bt_index<level - 1> bt) {
                            // Eliminate if wrong BodyType<level - 1>
                            if (C.get_body(i).type().get_id() != bt) {
                                return false;
                            }
                            // Eliminate if this is the Body<level - 1>
                            // we're seeking neighbors for
                            if (std::get<0>(vertex_i) == i) {
                                return false;
                            }
                            // Eliminate if already identified as part of
                            // another Body<level>
                            if (std::find(unidentified.begin(),
                                          unidentified.end(),
                                          i) == unidentified.end()) {
                                return false;
                            }
                            // If this Body<level - 1> is already part of
                            // the Graph, eliminate if it is already visited or
                            // of a ForceCenterType<level - 1> not neighboring
                            // this Body<level - 1>
                            auto match_it = std::find_if(
                                graph.begin(), graph.end(),
                                [&graph, &i](auto &vertex_ptr) {
                                    return std::get<0>(*vertex_ptr) == i;
                                });
                            if (match_it != graph.end()) {
                                if (std::get<2>(**match_it)) {
                                    return false;
                                }
                                if (std::find_if(fct_neighbors.begin(),
                                                 fct_neighbors.end(),
                                                 [&match_it](auto &fct) {
                                                     return std::get<1>(
                                                                **match_it) ==
                                                            fct;
                                                 }) == fct_neighbors.end()) {
                                    return false;
                                }
                            }
                            // If this Body<level - 1> is already found to
                            // be a neighbor, don't add it again
                            if (std::find(graph_neighbors.begin(),
                                          graph_neighbors.end(),
                                          i) != graph_neighbors.end()) {
                                return false;
                            }
                            // If all above tests pass, the Body<level - 1>
                            // is eligible
                            return true;
                        };
                        auto config_nearest_neighbors =
                            std::vector<size_type>{};
                        for (const auto &bt_el : FCT_neighbor_no_BTs) {
                            auto BT_i_neighbors = get_first_n_indices(
                                distances_i, bt_el.second,
                                [&eligible_neighbor, &bt_el](real d,
                                                             size_type i) {
                                    return eligible_neighbor(d, i, bt_el.first);
                                });
                            config_nearest_neighbors.insert(
                                config_nearest_neighbors.end(),
                                BT_i_neighbors.begin(), BT_i_neighbors.end());
                        }
                        sort_consistently(FCT_neighbor_BTs, fct_neighbors);
                        // Identify all valid permutations in which they can
                        // be added. Find all groups of FCTs of the same
                        // central species.
                        auto bt_sets = std::vector<std::vector<size_type>>{{0}};
                        auto bt_n = 0;
                        for (size_type k = 1; k < FCT_neighbor_BTs.size();
                             ++k) {
                            if (FCT_neighbor_BTs[k] ==
                                FCT_neighbor_BTs[k - 1]) {
                                bt_sets[bt_n].push_back(k);
                            } else {
                                bt_sets.push_back({});
                                ++bt_n;
                                bt_sets[bt_n] = {k};
                            }
                        }
                        // Construct all valid permutations
                        auto indices = std::vector<size_type>(
                            config_nearest_neighbors.size());
                        std::iota(indices.begin(), indices.end(), 0);
                        auto permutations = get_permutations(indices, bt_sets);
                        // Remove duplicates
                        auto remove_list = std::vector<size_type>{};
                        for (size_type i = 1; i < permutations.size(); ++i) {
                            for (size_type j = 0; j < i; ++j) {
                                bool matches = true;
                                for (size_type k = 0; k != indices.size();
                                     ++k) {
                                    if (fct_neighbors[permutations[i][k]] !=
                                        fct_neighbors[permutations[j][k]]) {
                                        matches = false;
                                        break;
                                    }
                                }
                                if (matches) {
                                    remove_list.push_back(i);
                                }
                            }
                        }
                        remove_indices(remove_list, permutations);
                        for (auto &permutation : permutations) {
                            // Copy current graph before extending
                            auto new_graph = graph;
                            for (size_type k = 0; k != permutation.size();
                                 ++k) {
                                // Create new tuple
                                body_fct_tuples.push_back(std::make_unique<
                                                          body_fct_tuple>(
                                    config_nearest_neighbors[permutation[k]],
                                    fct_neighbors[k], false));
                                auto *new_tuple =
                                    body_fct_tuples[body_fct_tuples.size() - 1]
                                        .get();
                                // Check if tuple already exists in graph
                                size_type already_included = 0;
                                for (size_type i = 0; i != new_graph.size();
                                     ++i) {
                                    if (*new_graph[i] == *new_tuple) {
                                        already_included = i;
                                        break;
                                    }
                                }
                                // Add new tuple to graph w/o resorting
                                if (!already_included) {
                                    new_graph.add_vertex(new_tuple, false);
                                    auto new_vertex_index =
                                        new_graph.size() - 1;
                                    // Add bond to vertex i
                                    new_graph.add_edge({i, new_vertex_index});
                                } else {
                                    // See if bond already exists. If not, add
                                    // it.
                                    auto new_bond = Edge{i, already_included};
                                    if (i > already_included) {
                                        std::swap(new_bond.first,
                                                  new_bond.second);
                                    }
                                    const auto &edges = new_graph.get_edges();
                                    if (std::find(edges.begin(), edges.end(),
                                                  new_bond) == edges.end()) {
                                        new_graph.add_edge(new_bond);
                                    }
                                }
                            }
                            // Sort once all new vertices have been added
                            new_graph.sort();
                            graphs.push_front(std::move(new_graph));
                        }
                        break;
                    }
                }
                if (completed) {
                    // If completed graph does not match any chemical
                    // graphs, it should be eliminated
                    auto FCTs = std::vector<fct_index<level - 1> *>{};
                    for (auto &vertex : graph) {
                        FCTs.push_back(&std::get<1>(*vertex));
                    }
                    auto fct_graph =
                        Graph<fct_index<level - 1>>{FCTs, graph.get_edges()};
                    fct_graph.sort();
                    auto found_match = false;
                    for (auto bt_el :
                         reg_->template get_objects<BodyType, level>()) {
                        auto bt = bt_el.second;
                        if (bt.graph() == fct_graph) {
                            found_match = true;
                            break;
                        }
                    }
                    if (!found_match) {
                        eliminated = true;
                        completed = false;
                    }
                }
            }
            if (completed) {
                // Copy the graph into completed graphs
                completed_graphs.push_back(graph);
            }
        }
        // Among the completed candidate graphs, select the one with
        // shortest average bond length
        if (completed_graphs.empty()) {
            throw std::runtime_error{
                "Could not match structure to given chemical graphs"};
        }
        real min_avg_bond_length = std::numeric_limits<real>::max();
        size_type chosen_graph_i = 0;
        for (size_type graph_i = 0; graph_i != completed_graphs.size();
             ++graph_i) {
            const auto &graph = completed_graphs[graph_i];
            const auto &edges = graph.get_edges();
            real total_bond_length = 0.0;
            for (size_type edge_i = 0; edge_i != edges.size(); ++edge_i) {
                auto edge = edges[edge_i];
                auto v1 = *graph[edge.first];
                auto v2 = *graph[edge.second];
                auto index_1 = std::get<0>(v1);
                auto index_2 = std::get<0>(v2);
                auto bond_length = distances(index_1, index_2);
                total_bond_length += bond_length;
            }
            auto avg_bond_length = total_bond_length / edges.size();
            if (avg_bond_length < min_avg_bond_length) {
                min_avg_bond_length = avg_bond_length;
                chosen_graph_i = graph_i;
            }
        }
        auto chosen_graph = completed_graphs[chosen_graph_i];
        // Remember FCTs and remove elements from unidentified
        for (auto vertex : chosen_graph) {
            auto atom_index = std::get<0>(*vertex);
            FCTs[atom_index] = std::get<1>(*vertex);
            auto it =
                std::find(unidentified.begin(), unidentified.end(), atom_index);
            std::cout << "Found it? " << (it != unidentified.end())
                      << std::endl;
            std::cout << "*vertex: " << *vertex << std::endl;
            std::cout << "Unidentified: " << unidentified << std::endl;
            std::cout << "Chosen graph: " << chosen_graph << std::endl;
            unidentified.erase(it);
        }
        // Create Graph of Body<level-1>s based on graph to use when
        // constructing ForceCenter<level - 1>s
        auto vertices = std::vector<Body<n, level - 1> *>{};
        for (auto el_ptr : chosen_graph) {
            auto C_index = std::get<0>(*el_ptr);
            auto body_ptr = &C.get_body(C_index);
            vertices.push_back(body_ptr);
        }
        auto edges = graph.get_edges();
        auto body_graph = Graph<Body<n, level - 1>>{vertices, edges};
        body_graphs.push_back(std::move(body_graph));
        // Remove all graphs and body_fct_tuples
        graphs = {};
        body_fct_tuples.clear();
    }

    // Create a list of graphs of FCTs, to use in constructing Body<level>s
    auto FCT_graphs = std::vector<Graph<fct_index<level - 1>>>{};

    // Construct graph trees of bodies in the same order as the bodies in C.
    // Also, populate FCT_graphs
    auto graph_trees = std::vector<GraphTree<Body<n, level - 1>>>(C.size());
    for (const auto &graph : body_graphs) {
        auto FCT_vertices = std::vector<fct_index<level - 1> *>(graph.size());
        for (size_type i = 0; i != graph.size(); ++i) {
            auto vertex = graph[i];
            auto id = vertex->get_id();
            auto index = C.get_index_by_id(id);
            auto graph_tree = GraphTree<Body<n, level - 1>>{graph, i};
            FCT_vertices[i] = &FCTs[index];
            graph_trees[index] = std::move(graph_tree);
        }
        auto FCT_graph =
            Graph<fct_index<level - 1>>{FCT_vertices, graph.get_edges()};
        FCT_graphs.push_back(std::move(FCT_graph));
    }
    // Identify BodyType<level>s
    auto body_types = std::vector<bt_index<level>>{};
    for (const auto &FCT_graph : FCT_graphs) {
        bool found_match = false;
        for (const auto &el : reg_->template get_objects<BodyType, level>()) {
            auto id = el.first;
            auto bt = el.second;
            if (bt.graph() == FCT_graph) {
                body_types.push_back(id);
                found_match = true;
                break;
            }
        }
        if (!found_match) {
            throw std::runtime_error{"Identified structure did not match any "
                                     "given chemical graphs."};
        }
    }

    // Construct the ForceCenter<level - 1>s
    auto force_centers = std::vector<fc_index<level - 1>>{};
    for (size_type i = 0; i != C.size(); ++i) {
        force_centers.push_back(reg_->template make_force_center<level - 1>(
            FCTs[i], graph_trees[i]));
    }

    // Construct the Body<level>s
    for (size_type i = 0; i != body_graphs.size(); ++i) {
        auto fc_indices = std::vector<fc_index<level - 1>>{};
        auto &bg = body_graphs[i];
        for (auto vertex : bg) {
            auto body = *vertex;
            auto id = body.get_id();
            auto index = C.get_index_by_id(id);
            fc_indices.push_back(force_centers[index]);
        }
        bodies_.push_back(
            reg_->template make_body<level>(body_types[i], fc_indices));
    }

    // Compute the positions of the Body<level>s
    for (auto bi : bodies_) {
        auto body_i = reg_->template get_object(bi);
        auto masses = std::vector<real>{};
        auto constituent_positions = std::vector<Point<real>>{};
        for (auto &constituent : body_i) {
            masses.push_back(constituent.body().type().get_mass());
            auto C_index = C.get_index_by_id(constituent.get_id());
            constituent_positions.push_back(C.get_position(C_index));
        }
        auto N = masses.size();
        assert(N == constituent_positions.size());
        real total_mass = 0;
        for (size_type i = 0; i != N; ++i) {
            total_mass += masses[i];
        }
        // Initialize center-of-mass position with correct bounds
        auto com_position = constituent_positions[0] * 0;
        for (size_type i = 0; i != N; ++i) {
            com_position += (masses[i] / total_mass) * constituent_positions[i];
        }
        positions_.push_back(com_position);
    }
    map_to_lower_level();
}

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os, const Configuration<n, level> &c) {
    os << "Configuration<" << level << ">"
       << ": {";
    for (size_type i = 0; i != c.size() - 1; ++i) {
        os << "\n[" << i << "]: " << c.get_body(i) << ": " << c.get_position(i)
           << ", ";
    }
    auto i = c.size() - 1;
    os << "\n[" << i << "]: " << c.get_body(i) << ": " << c.get_position(i)
       << "}";
    return os;
}

template <level_type n, level_type level>
void print_xyz(const Configuration<n, level> &C, std::ostream &os = std::cout) {
    auto atomic_config = *C.template get_representation<0>();
    auto positions = atomic_config.get_positions();
    os << atomic_config.size() << std::endl;
    os << "Periodic box: x = (" << Angstrom(positions[0].x.low()) << " A, "
       << Angstrom(positions[0].x.high()) << " A), y = ("
       << Angstrom(positions[0].y.low()) << " A, "
       << Angstrom(positions[0].y.high()) << " A), z = ("
       << Angstrom(positions[0].z.low()) << " A, "
       << Angstrom(positions[0].z.high()) << " A)" << std::endl;
    for (size_type i = 0; i != atomic_config.size(); ++i) {
        os << atomic_config.get_body(i).type().name() << ' '
           << Angstrom(positions[i].x.value()) << ' '
           << Angstrom(positions[i].y.value()) << ' '
           << Angstrom(positions[i].z.value()) << std::endl;
    }
}

template <level_type ll, level_type n, level_type level>
std::enable_if_t<(ll >= 0 && ll < n), const Configuration<n, ll> *>
get_conf_ptr_from_reg(const Configuration<n, level> &C,
                      ObjectRegistry<n> &reg) {
    auto l_id = ObjectIndex<Configuration, n, ll>{C.get_id()};
    try {
        return &reg.get_object(l_id);
    } catch (std::out_of_range) {
        return nullptr;
    }
}
template <level_type ll, level_type n, level_type level>
std::enable_if_t<(ll < 0 || ll >= n), const Configuration<n, ll> *>
get_conf_ptr_from_reg(const Configuration<n, level> &C,
                      ObjectRegistry<n> &reg) {
    return nullptr;
}

// // Generate a randomized starting configuration specified by stoichiometry
// and
// // density (SFINAE disabled for level = 0, why would you want to optimize an
// // atomic geometry anyway?)
// template <level_type n, level_type level>
// Configuration<n, level>
// randomize_configuration(ObjectRegistry<n> &reg,
//                         const Stoichiometry<n, level> &S, real density,
//                         unsigned max_atoms, size_type relax = 5,
//                         real cutoff_coeff = 2, bool perturb_structures =
//                         true, std::ostream &os = std::cout, bool animate =
//                         false) {
//     // Initialize random distributions
//     auto rand_fraction = uniform_real_distribution(0, 1);
//     // Calculate mass per formula unit
//     real formula_mass = 0;
//     for (size_type i = 0; i != S.size(); ++i) {
//         formula_mass += S[i].get_mass() * S.relative_amount(i);
//     }
//     // Calculate volume per formula unit
//     real formula_vol = formula_mass / density;
//     // Calculate number of atoms per formula unit
//     size_type num_atoms = 0;
//     for (size_type i = 0; i != S.size(); ++i) {
//         num_atoms += S.relative_amount(i) * count_atoms(S[i]);
//     }
//     // Calculate how many formula units should be created
//     int num_formulae = max_atoms / num_atoms;
//     if (num_formulae < 1) {
//         std::cerr << "Number of atoms must be greater than the number of
//         atoms "
//                      "in one formula unit. max_atoms = "
//                   << max_atoms << ", num_atoms = " << num_atoms << std::endl;
//         exit(1);
//     }
//     // Calculate size of box to achieve target density
//     real box_volume = num_formulae * formula_vol;
//     // Calculate box side length and create simulation box
//     real box_length = std::pow(box_volume, 1.0 / 3.0);
//     std::pair<real, real> bounds{0, box_length};
//     // Create random coordinate generator
//     auto rand_real = Rand_real{0, 1};
//     // Create random coordinate perturbation generator
//     auto perturb_gen = Normal_distributed{0, 0.1_A};
//     auto random_perturbation = std::function<real()>(perturb_gen);
//     // Calculate the number of Body<n, level> to generate
//     size_type no_bodies = 0;
//     for (size_type i = 0; i != S.size(); ++i) {
//         no_bodies += S.relative_amount(i);
//     }
//     no_bodies *= num_formulae;
//     // Construct Bodies
//     std::vector<ObjectIndex<Body, n, level>> bodies;
//     for (size_type i = 0; i != S.size(); ++i) {
//         for (size_type j = 0; j != S.relative_amount(i) * num_formulae; ++j)
//         {
//             bodies.push_back(reg.make_body(S.get_index(i)));
//         }
//     }
//     // Generate random coordinates and orientations
//     std::vector<Point<real>> positions;
//     std::vector<Rotation<real>> orientations;
//     for (size_type i = 0; i != no_bodies; ++i) {
//         positions.push_back(Point<real>{bounds, rand_real() * box_length,
//                                         rand_real() * box_length,
//                                         rand_real() * box_length});
//         orientations.push_back(Rotation<real>(
//             {rand_real(), rand_real(), rand_real()}, rand_real() * 2 * pi));
//     }
//     // Compute original atomic positions
//     size_type no_atoms = 0;
//     // Nested vectors of atoms belonging to each Body<level> body_i
//     auto atoms = std::vector<std::vector<ObjectIndex<Body, n,
//     0>>>(no_bodies);
//     // Map: atom_index -> position
//     auto atomic_positions =
//         std::map<ObjectIndex<Body, n, 0>, Point<real>>(no_bodies);
//     for (size_type i = 0; i != no_bodies; ++i) {
//         auto body = reg.get_object(bodies[i]);
//         auto constituent_atoms = body.template get_constituents<0>();
//         auto prototype = body.prototype();
//         // auto origin = positions[i];
//         // auto orientation = orientations[i];
//         auto no_constituents = constituent_atoms.size();
//         assert(no_constituents == prototype.size());
//         for (size_type j = 0; j != no_constituents; ++j) {
//             ++no_atoms;
//             atoms[i].push_back(constituent_atoms[j]);
//             auto pos = (positions[i] + prototype[j])
//                            .rotate(orientations[i], positions[i]);
//             atomic_positions[i].push_back(pos);
//         }
//     }
//     // Create flattened vector of body indices
//     auto atoms_flattened = std::vector<ObjectIndex<Body, n, 0>>{};
//     for (const auto &body : atoms) {
//         for (const auto &atom : body) {
//             atoms_flattened.push_back(atom);
//         }
//     }
//     // Relax the geometry by rotating and translating Body<level>s with
//     // overlapping atoms wrt vdW radii
//     if (relax) {
//         // Define lambda to compute the contribution to the cost function of
//         // each pair of atoms
//         auto cost_contribution = [&reg, cutoff_coeff, &atoms_flattened,
//                                   &atomic_positions](size_type i, size_type
//                                   j) {
//             auto radius_i =
//             reg.get_object(atoms_flattened[i]).type().radius(); auto radius_j
//             = reg.get_object(atoms_flattened[j]).type().radius(); auto
//             sum_radii = radius_i + radius_j; auto pos_i =
//             atomic_positions[atoms_flattened[i]]; auto pos_j =
//             atomic_positions[atoms_flattened[j]]; auto d = distance(pos_i,
//             pos_j); auto x = d / (cutoff_coeff * sum_radii); if (x >= 1) {
//                 return 0.0;
//             } else {
//                 return pow(x - 1, 2);
//             }
//         };
//         // Compute all atomic distances
//         auto distances = SymmetricMatrix<real>(no_atoms, no_atoms);
//         for (size_type i = 0; i < no_atoms; ++i) {
//             for (size_type j = i + 1; j < no_atoms; ++j) {
//                 distances(i, j) =
//                     distance(atomic_positions[i], atomic_positions[j]);
//             }
//         }
//         // Compute cutoff value for considering atomic distances in cost
//         // computations.
//         // Cutoff should be set to twice the largest radius among atoms
//         real cutoff = 0;
//         for (auto &atom : atoms_flattened) {
//             auto radius = reg.get_object(atom).type().radius();
//             if (radius > cutoff) {
//                 cutoff = radius;
//             }
//         }
//         cutoff *= 2;
//         // Create matrix to keep track of which bodies are within each
//         other's
//         // purview
//         SparseSymmetricMatrix<bool> neighboring_bodies{no_bodies + 1,
//         no_bodies,
//                                                        false};
//         for (size_type body_i = 0; body_i < no_bodies; ++body_i) {
//             for (size_type body_j = body_i + 1; body_j < no_bodies; ++body_j)
//             {
//                 for (size_type atom_i = 0; atom_i < atoms[body_i].size();
//                      ++atom_i) {
//                     for (size_type atom_j = 0; atom_j < atoms[body_j].size();
//                          ++atom_j) {
//                         if (distances(atom_i, atom_j) < cutoff) {
//                             neighboring_bodies(body_i, body_j) = true;
//                             break;
//                         }
//                     }
//                     if (neighboring_bodies(body_i, body_j)) {
//                         break;
//                     }
//                 }
//             }
//         }
//     }

//     // Geometry relaxation loop
//     for (size_type body_i = 0; body_i != no_bodies; ++body_i) {
//         // Create analytical symbols for nondependent transformation
//         // variables
//         GiNaC::symbol Tx{"Tx"}, Ty{"Ty"}, Tz{"Tz"};
//         GiNaC::symbol Rx{"Rx"}, Ry{"Ry"}, Rz{"Rz"};
//         auto symbols =
//         std::vector<std::reference_wrapper<GiNaC::symbol>>{
//             Tx, Ty, Tz, Rx, Ry, Rz};
//         // Translation of body_i
//         auto T = Vector<GiNaC::ex>{Tx, Ty, Tz};
//         // Rotation of body_i
//         auto R = Rotation<GiNaC::ex>{Rx, Ry, Rz};
//         // Origin of the rotation (center-of-mass of body_i)
//         auto O = Vector<GiNaC::ex>{};
//         for (size_type xi = 0; xi != O.size(); ++xi) {
//             O[xi] = positions[body_i][xi].value();
//         }
//         // Create analytical expressions for the positions to be
//         rotated
//         // and translated
//         auto position_expressions = std::vector<GiNaC::ex>{};
//         for (size_type atom_i = 0; atom_i != atoms[body_i].size();
//              ++atom_i) {
//             auto pos_ex =
//                 atomic_positions[atoms[body_i][atom_i]].rotate(R, O)
//                 + T;
//             position_expressions.push_back(std::move(pos_ex));
//         }
//         // Loop over neighboring bodies and compute cost
//         contributions GiNaC::ex cost = 0; for (const auto &atom_i :
//         atoms[body_i]) {
//             for (const auto &neighbor : bodies) {
//                 if (neighboring_bodies(body_i, neighbor)) {
//                     for (const auto &atom_j :
//                     reg->get_object(neighbor)) {
//                         cost += cost_contribution(atom_i, atom_j);
//                     }
//                 }
//             }
//         }
//         // auto cost_gradient = derivative(cost, symbols, 1);
//         // auto cost_hessian = derivative(cost_gradient, symbols, 1);
//     }
// }
// auto ID = reg.make_configuration(atoms, atomic_positions);
// make_higher_level_configurations<n, lmin, lmax>(reg, ID);
// auto config = reg.get_object<Configuration, n, level>(ID);

// if (animate) {
//     print_xyz(config, os);
// }
// if (perturb_structures) {
//     for (size_type i = 0; i != config.size(); ++i) {
//         config.perturb_structure(i, random_perturbation);
//     }
//     if (animate) {
//         print_xyz(config, os);
//     }
// }

// if (relax) {
//     auto num_loops = relax * no_bodies;
//     // Get atomic representation
//     auto atom_config = config.template get_representation<0>();
//     // Store atomic radii in std::vector
//     std::vector<real> radii;
//     auto rep0 = config.template get_representation<0>();
//     for (size_type i = 0; i != rep0.size(); ++i) {
//         radii.push_back(rep0.get_body(i).type().get_radius());
//     }
//     // Loop over bodies to relax their configuration
//     // Create random Body selector
//     auto random_body = Rand_int{0, static_cast<int>(config.size()) -
//     1}; std::cout << "Starting geometry optimization" << std::endl;
//     for (size_type m = 0; m != num_loops; ++m) {
//         auto config0 = config;
//         auto i = m % no_bodies;
//         // auto i = random_body();
//         std::cout << "\nOptimization loop " << m + 1 << "/" <<
//         num_loops
//                   << ". Optimizing body #" << i << ": \n"
//                   << config.get_body(i)
//                   << "\nInitial configuration: Position = "
//                   << config.get_position(i)
//                   << ", Orientation = " << config.get_orientation(i)
//                   << ", Initial cost = " << cost_func.cost()
//                   << "\nOptimize translation..." << std::endl;
//         // Construct cost as a function of translation of Body<n,
//         level >
//             // #i
//             std::function<real(Point<real>)> cost_translation =
//             [&config, &cost_func, i](Point<real> P) {
//                 // Store the original position of body #i in config
//                 auto P0 = config.get_position(i);
//                 // Evaluate the cost by translating body #i in-place
//                 in
//                 // config and constructing the atomic representation
//                 auto cost = cost_func(config.translate(i, P)
//                                           .template
//                                           get_representation<0>()
//                                           .get_positions());
//                 // Undo the translation of config
//                 config.set_position(i, P0);
//                 // Return computed cost value
//                 return cost;
//             };
//         // Construct gradient optimizer
//         auto P0 = Point<real>{bounds, 0, 0, 0};
//         auto P_opt = ConjugateGradient<Point<real>, Vector<real>>(
//             P0, cost_translation, trans_crit, trans_ls_crit);
//         // Run optimization
//         auto P = P_opt.optimize();
//         // Translate body #i
//         config.translate(i, P);
//         if (animate) {
//             print_xyz(config, os);
//         }
//         std::cout << "...Done. Optimized translation vector: "
//                   << Vector<real>(P)
//                   << "\nNew position: " << config.get_position(i)
//                   << std::endl;
//         if (config.get_body(i).size() > 1) {
//             std::cout << "Optimize orientation..." << std::endl;
//             // Find optimal rotation of body #i
//             // Create design function
//             std::function<real(Rotation<real>)> cost_rotation =
//                 [&config, &cost_func, i](const Rotation<real> &R) {
//                     // Store the original orientation of body #i in
//                     // config
//                     auto R0 = config.get_orientation(i);
//                     auto cost =
//                         // Evaluate the cost by rotating body #i
//                         // in-place in config and constructing the
//                         // atomic representation
//                         cost_func(config.rotate(i, R)
//                                       .template
//                                       get_representation<0>()
//                                       .get_positions());
//                     // Undo the rotation of config body #i
//                     config.set_orientation(i, R0);
//                     // Return computed cost value
//                     return cost;
//                 };
//             // Construct conjugate gradient optimizer
//             Rotation<real> R0 = {0, 0, 0};
//             auto R_opt = ConjugateGradient<Rotation<real>,
//             Rotation<real>>(
//                 R0, cost_rotation, rot_crit, rot_ls_crit);
//             // Run optimization
//             auto R = R_opt.optimize();
//             // Rotate body #i
//             config.rotate(i, R);
//             std::cout << "...Done. Optimized rotation: " << R
//                       << "\nNew orientation: " <<
//                       config.get_orientation(i)
//                       << std::endl;

//             if (animate) {
//                 print_xyz(config, os);
//             }
//         }
//     }
// }
// if (!animate || animate) {
//     print_xyz(config, os);
// }
// // Return Configuration
// return std::move(config);

} // namespace champion

#endif
