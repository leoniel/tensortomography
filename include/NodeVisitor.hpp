/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  _  \ /| | // __ \  /|   \    /| |
 //  /  \_#| |  |#| |#| |  |#| |#|   \/   |#| |# \ \#| |// / \\ \|#| |\ \  |#| |
|#| |    |#| |__|#| |#| |__|#| |#| |\  /| |#| |# | |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| |_ / /#| |#| | |#| |#| |\#\ \ #| |
|#| |   __#| |  |#| |#| |  |#| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |    |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: Tree.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:

\******************************************************************************/

#ifndef CHAMPION_NODE_VISITOR
#define CHAMPION_NODE_VISITOR

#include <algorithm>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

#include "set_precision.hpp"

namespace champion {

/**************************** Abstract base classes ***************************/

// Forward declarations
template <typename T> class TreeNode;

template <typename T> class NodeVisitor {
  public:
    virtual ~NodeVisitor() {}
    virtual void visit(TreeNode<T> *tree) = 0;
};

template <typename T> class ConstNodeVisitor {
  public:
    virtual ~ConstNodeVisitor() {}
    virtual void visit(const TreeNode<T> *tree) = 0;
};

/************************** Concrete implementations **************************/

template <typename T> class NodeRecorder : public NodeVisitor<T> {
  public:
    NodeRecorder() : nodes_{} {}
    virtual ~NodeRecorder() {}
    virtual void visit(TreeNode<T> *node) override { nodes_.push_back(node); }
    virtual std::vector<TreeNode<T> *> get_nodes() { return nodes_; }
    virtual std::vector<TreeNode<T> *> get_leaves() {
        auto leaves = nodes_;
        auto it = leaves.begin();
        while (it != leaves.end()) {
            if ((*it)->down()) {
                leaves.erase(it);
            } else {
                ++it;
            }
        }
        return leaves;
    }

  protected:
    std::vector<TreeNode<T> *> nodes_;
};

template <typename T> class ConstNodeRecorder : public ConstNodeVisitor<T> {
  public:
    ConstNodeRecorder() : nodes_{} {}
    virtual ~ConstNodeRecorder() {}
    virtual void visit(const TreeNode<T> *node) override {
        nodes_.push_back(node);
    }
    virtual std::vector<const TreeNode<T> *> get_nodes() { return nodes_; }
    virtual std::vector<const TreeNode<T> *> get_leaves() {
        auto leaves = nodes_;
        auto it = leaves.begin();
        while (it != leaves.end()) {
            if ((*it)->down()) {
                leaves.erase(it);
            } else {
                ++it;
            }
        }
        return leaves;
    }

  protected:
    std::vector<const TreeNode<T> *> nodes_;
};

template <typename T> class NodeCounter : public ConstNodeVisitor<T> {
  public:
    NodeCounter() : count_{0} {}
    virtual ~NodeCounter() {}
    void visit(const TreeNode<T> *tree) override { ++count_; }
    size_type count() { return count_; }

  protected:
    size_type count_;
};

template <typename T> class LeafCounter : public NodeCounter<T> {
  public:
    virtual ~LeafCounter() {}
    void visit(const TreeNode<T> *node) override {
        if (!node->down()) {
            ++this->count_;
        }
    }
};

template <typename T> class NodesInLayerCounter : public NodeCounter<T> {
  public:
    // Construct from layer index
    NodesInLayerCounter(size_type i) : layer_{i} {}
    virtual ~NodesInLayerCounter();
    void visit(const TreeNode<T> *node) override {
        if (node->layer() == layer_) {
            ++this->count_;
        }
    }

  private:
    size_type layer_;
};

template <typename T> class ConstLayerRecorder : public ConstNodeRecorder<T> {
  public:
    // Construct from layer index
    ConstLayerRecorder(size_type i) : layer_{i} {}
    virtual ~ConstLayerRecorder() {}
    void visit(const TreeNode<T> *node) override {
        if (node->layer() == layer_) {
            this->nodes_.push_back(node);
        }
    }

  private:
    size_type layer_;
};

template <typename T> class OutputWidthVisitor : public ConstNodeVisitor<T> {
  public:
    OutputWidthVisitor() : widths_{} {}
    void visit(const TreeNode<T> *node) override {
        std::ostringstream os;
        size_type pos = 0;
        os << node->value();
        auto p = os.tellp();
        if (p == 0) {
            std::cerr << "ostringstream::tellp() failed." << std::endl;
            exit(1);
        } else {
            pos = p;
        }
        widths_[node] = pos;
    }
    std::map<const TreeNode<T> *, size_type> widths() { return widths_; }

  private:
    std::map<const TreeNode<T> *, size_type> widths_;
};
} // namespace champion

#endif
