/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  _  \ /| | // __ \  /|   \    /| |
 //  /  \_#| |  |#| |#| |  |#| |#|   \/   |#| |# \ \#| |// / \\ \|#| |\ \  |#| |
|#| |    |#| |__|#| |#| |__|#| |#| |\  /| |#| |# | |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| |_ / /#| |#| | |#| |#| |\#\ \ #| |
|#| |   __#| |  |#| |#| |  |#| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |    |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: NormalDistribution.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:

\******************************************************************************/

#ifndef CHAMPION_NORMAL_DISTRIBUTION_HPP
#define CHAMPION_NORMAL_DISTRIBUTION_HPP

#include <iostream>

#include "set_precision.hpp"

namespace champion {

class NormalDistribution {
  public:
    // Construct from mean and standard deviation
    NormalDistribution(real mu = 0, real sigma = 0) : mu_{mu}, sigma_{sigma} {}

    // Access
    real get_mean() const { return mu_; }
    real get_standard_deviation() const { return sigma_; }
    real get_variance() const { return sigma_ * sigma_; }

    // Modification
    void set_mean(real mu) { mu_ = mu; }
    void set_standard_deviation(real sigma) { sigma_ = sigma; }

  private:
    real mu_;
    real sigma_;
};

NormalDistribution operator-(const NormalDistribution &G) {
    return NormalDistribution{-G.get_mean(), G.get_standard_deviation()};
}

NormalDistribution &operator+=(NormalDistribution &G1,
                               const NormalDistribution &G2) {
    G1.set_mean(G1.get_mean() + G2.get_mean());
    G1.set_standard_deviation(std::sqrt(G1.get_variance() + G2.get_variance()));
    return G1;
}

NormalDistribution &operator-=(NormalDistribution &G1,
                               const NormalDistribution &G2) {
    return G1 += -G2;
}

NormalDistribution operator+(const NormalDistribution &G1,
                              const NormalDistribution &G2) {
    auto G = G1;
    return G += G2;
}

NormalDistribution operator-(const NormalDistribution &G1,
                              const NormalDistribution &G2) {
    return G1 + -G2;
}

NormalDistribution &operator*=(NormalDistribution &G, real a) {
    G.set_mean(G.get_mean() * a);
    G.set_standard_deviation(G.get_standard_deviation() * std::sqrt(a));
    return G;
}

NormalDistribution &operator/=(NormalDistribution &G, real a) {
    return G *= 1/a;
}

NormalDistribution operator*(const NormalDistribution &G, real a) {
    auto G2 = G;
    return G2 *= a;
}

NormalDistribution operator*(real a, const NormalDistribution &G) {
    return G * a;
}

std::ostream &operator<<(std::ostream &os, const NormalDistribution &G) {
    os << G.get_mean() << " ± " << G.get_standard_deviation();
    return os;
}

} // namespace champion

#endif
