#ifndef CHAMPION_OPTIMIZER_HPP
#define CHAMPION_OPTIMIZER_HPP

#include <algorithm>
#include <cassert>
#include <cmath>
#include <functional>
#include <limits>
#include <string>

#include "Function.hpp"
#include "Rotation.hpp"
#include "algorithm.hpp"
#include "set_precision.hpp"

namespace champion {

struct StopCriteria {
    // Constructor
    StopCriteria(unsigned max_iter = 1000, real delta = 1e-6,
                 real max_step = 0.1, real abs_tol = 1e-6, real rel_tol = 1e-6,
                 real grad_tol = 1e-6)
        : max_iter_{max_iter}, delta_{delta}, max_step_{max_step},
          abs_tol_{abs_tol}, rel_tol_{rel_tol}, grad_tol_{grad_tol} {}

    // Public data members
    unsigned max_iter_;
    real delta_;
    real max_step_;
    real abs_tol_;
    real rel_tol_;
    real grad_tol_;
};

// Forward declarations
template <typename T> class Optimizer;
template <typename T> class NumericalOptimizer;
template <typename T1, typename T2> class GradientOptimizer;
class LineSearch;

template <typename T>
std::ostream &operator<<(std::ostream &os, const NumericalOptimizer<T> &opt);

template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &os,
                         const GradientOptimizer<T1, T2> &opt);

template <typename T1>
std::ostream &operator<<(std::ostream &os, const LineSearch &opt);

template <typename T> class Optimizer {
  public:
    virtual T optimize() = 0;
    virtual ~Optimizer() {}
};

template <typename T> class NumericalOptimizer {
  public:
    // Public member functions
    virtual T optimize() = 0;
    virtual ~NumericalOptimizer() {}

    inline void update_errors(real cost, real old_cost) {
        abs_err_ = cost - old_cost;
        rel_err_ = abs_err_ / cost;
    }

    friend std::ostream &operator<<<>(std::ostream &os,
                                      const NumericalOptimizer<T> &opt);

  protected:
    // Protected member functions
    NumericalOptimizer(T x0, const std::function<real(T)> &cost_func,
                       StopCriteria crit = StopCriteria(), bool print = true)
        : print_{print}, iter_{0}, abs_err_{std::numeric_limits<real>::max()},
          rel_err_{std::numeric_limits<real>::max()}, x_{x0}, cost{cost_func},
          cost_{cost(x_)}, cost_old_{0}, max_iter_{crit.max_iter_},
          max_step_{crit.max_step_}, delta_{crit.delta_},
          abs_tol_{crit.abs_tol_}, rel_tol_{crit.rel_tol_} {}

    // Protected data members
    bool print_;
    unsigned iter_;
    real abs_err_;
    real rel_err_;
    T x_;
    const std::function<real(T)> &cost;
    real cost_, cost_old_;
    unsigned max_iter_;
    real max_step_;
    real delta_;
    real abs_tol_;
    real rel_tol_;
};

template <typename T>
std::ostream &operator<<(std::ostream &os, const NumericalOptimizer<T> &opt) {
    os << "Number of iterations: " << opt.iter_ + 1
       << "\nCurrent x = " << opt.x_ << "\nCost value: " << opt.cost_
       << "\nAbsolute error: " << opt.abs_err_
       << "\nRelative error: " << opt.rel_err_;
    return os;
}

class LineSearch : public NumericalOptimizer<real> {
  public:
    // Public member functions
    LineSearch(real x0, std::function<real(real)> &cost,
               StopCriteria crit = StopCriteria())
        : NumericalOptimizer(x0, cost, crit), step_{max_step_},
          d1_{std::numeric_limits<real>::max()}, grad_tol_{crit.grad_tol_} {}
    virtual ~LineSearch() {}

    friend std::ostream &operator<<(std::ostream &os, const LineSearch &opt);

    virtual real optimize() override;

    inline real derivative(real x) {
        auto diff = (cost(x + delta_) - cost(x));
        // Use positive differencing in downward sloping direction so that a
        // downhill step of length delta_ is guaranteed to decrease cost
        if (diff > 0) {
            diff = (cost(x) - cost(x - delta_));
            if (diff < 0) { // If sign is not same for left and right difference
                // lower delta to better approximate limit
                delta_ /= 10;
                return derivative(x);
            }
        }
        return diff / delta_;
    }

  private:
    // Private member functions
    void choose_next();
    // Private data members
    real step_;
    real d1_;
    real grad_tol_;
};

std::ostream &operator<<(std::ostream &os, const LineSearch &ls) {
    os << "LineSearch: " << std::endl;
    const NumericalOptimizer<real> &base_ref = ls;
    os << base_ref << "\nDerivative: " << ls.d1_;
    return os;
}

void LineSearch::choose_next() {
    auto D0 = derivative(x_);
    auto sgn = D0 < 0 ? -1 : 1;
    auto step = -sgn * step_;
    while (cost(x_ + step) > cost_) {
        step /= 2;
        if (fabs(step) < delta_) {
            step = -sgn * delta_;
            break;
        }
    }
    auto x_old = x_;
    auto new_cost = cost(x_ + step);
    size_type numsteps = 0;
    while (new_cost < cost_) {
        ++numsteps;
        x_ += step;
        cost_ = new_cost;
        new_cost = cost(x_ + step);
    }
    // Choose a new value for step_, assuring it is between delta_ and max_step_
    step_ = std::max(std::min(max_step_, std::fabs(x_ - x_old)), delta_);
    auto D1 = derivative(x_);
    if (D1 == D0) {
        return;
    }
    // Use D1 and D0 to try to inter-/extrapolate location of minimum
    auto dx = (x_old - x_) * D1 / (D1 - D0);
    auto cost_trial = cost(x_ + dx);
    if (cost_trial < cost_) { // Interpolation succeeded
        x_ += dx;
        cost_ = cost_trial;
        d1_ = derivative(x_);
        step_ = std::min(max_step_, step_ + std::fabs(dx));
    } else {
        d1_ = D1;
    }
}

real LineSearch::optimize() {
    size_type abs_conv = 0;
    size_type rel_conv = 0;
    cost_ = cost(x_);
    for (iter_ = 0; iter_ != max_iter_; ++iter_) {
        assert(step_ <= max_step_);
        cost_old_ = cost_;
        choose_next();
        update_errors(cost_, cost_old_);
        if (std::fabs(d1_) < grad_tol_) {
            return x_;
        }
        if (std::fabs(rel_err_) < rel_tol_) {
            if (++rel_conv == 2) {
                return x_;
            }
        } else {
            rel_conv = 0;
        }
        if (std::fabs(abs_err_) < abs_tol_) {
            if (++abs_conv == 2) {
                return x_;
            }
        } else {
            abs_conv = 0;
        }
    }
    return x_;
}

template <typename T1, typename T2>
class GradientOptimizer : public NumericalOptimizer<T1> {
  public:
    // Public member functions
    virtual ~GradientOptimizer() {}
    virtual T1 optimize() override;
    virtual T2 gradient() const;
    virtual T2 choose_direction(unsigned i, T2 new_grad, T2 old_grad) = 0;

    friend std::ostream &operator<<<>(std::ostream &os,
                                      const GradientOptimizer<T1, T2> &opt);

  protected:
    // Protected member functions
    GradientOptimizer(T1 x0, const std::function<real(T1)> &cost_func,
                      StopCriteria crit = StopCriteria(),
                      StopCriteria ls_crit = StopCriteria(), bool print = true)
        : NumericalOptimizer<T1>(x0, cost_func, crit, print), grad_{gradient()},
          grad_tol_{crit.grad_tol_}, ls_crit_{ls_crit} {
        assert(this->cost_ == this->cost(this->x_));
    }

    // Protected data members
    T2 grad_;
    real grad_tol_;
    StopCriteria ls_crit_; // Stop criteria for line search instantiations
};

// Assumes that cost_ contains updated cost value
template <typename T1, typename T2>
T2 GradientOptimizer<T1, T2>::gradient() const {
    T2 grad;
    assert(this->cost_ == this->cost(this->x_));
    // auto orientation = random_orientation();
    for (size_type i = 0; i != grad.size(); ++i) {
        auto test = 0 * grad;
        test[i] = 1;
        // test = test.rotate(orientation);
        auto c1 = this->cost(this->x_ + this->delta_ * test);
        if (this->cost_ != this->cost(this->x_)) {
            throw std::runtime_error{
                std::string{"champion::GradientOptimizer: Stored cost value "
                            "does not match evaluated value. Stored value: "} +
                std::to_string(this->cost_) +
                ", evaluated value: " + std::to_string(this->cost(this->x_))};
        }
        auto diff = c1 - this->cost_;
        auto derivative = diff / this->delta_;
        grad[i] = derivative;
    }
    assert(this->cost_ == this->cost(this->x_));
    // grad = grad.rotate(-orientation);
    return grad;
}

template <typename T1, typename T2> T1 GradientOptimizer<T1, T2>::optimize() {
    size_type conv_count = 0;
    this->cost_ = this->cost(this->x_);
    if (this->print_) {
        std::cout << "GradientOptimizer: initial cost: "
                  << std::setprecision(std::numeric_limits<real>::digits10)
                  << this->cost(this->x_);
    }
    this->cost_old_ = this->cost_;
    auto grad_old = grad_;
    std::string message = "reached maximum number of iterations";
    for (this->iter_ = 0; this->iter_ != this->max_iter_; ++this->iter_) {
        // Compute gradient
        grad_ = gradient();
        assert(this->cost_ == this->cost(this->x_));
        // Halt if gradient is less than grad_tol_
        if (magnitude(grad_) < this->grad_tol_) {
            message = "gradient magnitude below tolerance";
            break;
        }
        // Choose search direction
        auto dir = choose_direction(this->iter_, grad_, grad_old);
        // Construct a line search in gradient direction
        auto ls_cost = std::function<real(real)>{[this, dir](real alpha) {
            return this->cost(this->x_ + alpha * dir);
        }};
        auto ls = LineSearch(0, ls_cost, ls_crit_);
        // Perform line search and update this->x_ and cost_
        auto alpha = ls.optimize();
        this->cost_old_ = this->cost_;
        this->x_ += alpha * dir;
        this->cost_ = this->cost(this->x_);
        this->update_errors(this->cost_, this->cost_old_);
        // Halt if (delta cost < abs_tol_ + rel_tol_ * |cost|) for second
        // consecutive time
        if (fabs(this->cost_old_ - this->cost_) <
            this->abs_tol_ + this->rel_tol_ * fabs(this->cost_)) {
            if (++conv_count == 2) {
                message = "residual below tolerance";
                break;
            }
        } else {
            conv_count = 0;
        }
        // Remember old gradient value
        grad_old = grad_;
    }
    if (this->print_) {
        std::cout << ", final cost: "
                  << std::setprecision(std::numeric_limits<real>::digits10)
                  << this->cost(this->x_) << " after " << this->iter_
                  << " iterations. Reason: " << message << std::endl;
        std::cout << this->x_ << std::endl;
    }
    return this->x_;
}

template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &os,
                         const GradientOptimizer<T1, T2> &opt) {
    os << "GradientOptimizer: " << std::endl;
    const Optimizer<T1> &base_ref = opt;
    os << base_ref << "\nGradient: " << opt.grad_;
    return os;
}

template <typename T1, typename T2>
class SteepestDescent : public GradientOptimizer<T1, T2> {
  public:
    // Public member functions
    SteepestDescent(T1 x0, std::function<real(T1)> &cost,
                    StopCriteria crit = StopCriteria(),
                    StopCriteria ls_crit = StopCriteria(), bool print = true)
        : GradientOptimizer<T1, T2>(x0, cost, crit, ls_crit, print) {}

  protected:
    // Protected member functions
    virtual T2 choose_direction(unsigned i, T2 new_grad, T2 old_grad) override;
};

template <typename T1, typename T2>
T2 SteepestDescent<T1, T2>::choose_direction(unsigned i, T2 new_grad,
                                             T2 old_grad) {
    return -new_grad.normalize();
}

template <typename T1, typename T2>
class ConjugateGradient : public GradientOptimizer<T1, T2> {
  public:
    // Public member functions
    ConjugateGradient(T1 x0, std::function<real(T1)> &cost,
                      StopCriteria crit = StopCriteria(),
                      StopCriteria ls_crit = StopCriteria(), bool print = true)
        : GradientOptimizer<T1, T2>(x0, cost, crit, ls_crit, print) {}

  protected:
    virtual T2 choose_direction(unsigned i, T2 new_grad, T2 old_grad) override;
};

template <typename T1, typename T2>
T2 ConjugateGradient<T1, T2>::choose_direction(unsigned i, T2 new_grad,
                                               T2 old_grad) {
    // Reset direction every n iterations to avoid loss of conjugacy
    if (i % new_grad.size() == 0) {
        return -new_grad.normalize();
    }
    return (-new_grad + old_grad * (squared_magnitude(new_grad) /
                                    squared_magnitude(old_grad)))
        .normalize();
}
} // namespace champion

#endif
