#ifndef CHAMPION_TYPE_INFERENCE_HPP
#define CHAMPION_TYPE_INFERENCE_HPP

#include <utility>

namespace champion {

// Debugging helper function to print out deduced types at compile time.
// Declared but not defined => compilation error that prints the type
// Usage:
//  ShowType<type>;
//  ShowType<decltype(var)>;
// Method suggested by Diego Assencio:
// https://diego.assencio.com/?index=05e466a9a1e6dfc958769f80bf986f65
template <typename T> class Show_type;

// Addition

// Base case: The result of adding two generic types
template <typename T1, typename T2, typename... Types> struct sum_type {
    using type = decltype(std::declval<T1>() + std::declval<T2>());
};

// Convenience wrapper
template <typename T1, typename T2, typename... Types>
using Sum_type = typename sum_type<T1, T2, Types...>::type;

// Generic case: The result of adding three or more (recursive)
template <typename T1, typename T2, typename T3, typename... Types>
struct sum_type<T1, T2, T3, Types...> {
    using type = Sum_type<T1, Sum_type<T2, T3, Types...>>;
};

// Multiplication

// Base case: The result of multiplying two generic types
template <typename T1, typename T2, typename... Types> struct product_type {
    using type = decltype(std::declval<T1>() * std::declval<T2>());
};

// Convenience wrapper
template <typename T1, typename T2, typename... Types>
using Product_type = typename product_type<T1, T2, Types...>::type;

// Generic case: The result of multiplying three or more (recursive)
template <typename T1, typename T2, typename T3, typename... Types>
struct product_type<T1, T2, T3, Types...> {
    using type = Product_type<T1, Product_type<T2, T3, Types...>>;
};

// Powers
template <typename T, int exp_num, int exp_denom = 1> struct power_type {
    using type = decltype(pow(std::declval<T>(), exp_num / exp_denom));
};

// Convenience wrapper
template <typename T, int exp_num, int exp_denom = 1>
using Power_type = typename power_type<T, exp_num, exp_denom>::type;

// Used to remove dimensions from dimensioned quantity
template <typename T> struct dimensionless_type {
    using type = decltype(std::declval<T>() / std::declval<T>());
};

// Convenience wrapper
template <typename T>
using Dimensionless_type = typename dimensionless_type<T>::type;

// Special cases convenience wrapper
template <typename T> using Inverse_type = Power_type<T, -1>;
template <typename T> using Square_type = Product_type<T, T>;
template <typename T> using Sqrt_type = Power_type<T, 1, 2>;
template <typename T1, typename T2>
using Quotient_type = Product_type<T1, Inverse_type<T2>>;

} // namespace champion
#endif