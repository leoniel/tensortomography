/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2018 Fabian Årén

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: EvolutionaryAlgorithm.hpp

AUTHOR: Fabian Årén 2018-03-28
MODIFIED BY: Rasmus Andersson 2018-05-09

DESCRIPTION:

A template of functions to perform an Evolutionary Algorithm.


\******************************************************************************/

#ifndef CHAMPION_EVOLUTIONARYALGORITHM_HPP
#define CHAMPION_EVOLUTIONARYALGORITHM_HPP

#include <algorithm>
#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <numeric>
#include <random>
#include <stdexcept>
#include <vector>

#include <mpi.h>

#include "CostFunction.hpp"
#include "Dictionary.hpp"
#include "Optimizer.hpp"
#include "VectorSlice.hpp"
#include "VectorSliceCollection.hpp"
#include "algorithm.hpp"
#include "io.hpp"
#include "set_precision.hpp"
#include "statistics.hpp"

using namespace std::chrono;

// Access global MPI variables
extern int MPI_root, MPI_rank, MPI_size;

namespace champion {

using normal_distribution = std::pair<double, double>;

enum cross_over_modes { n_point, arithmetic };
enum mutation_modes { perturb, reset };

class Genome {
  public:
    // Construct from values and types
    Genome(std::vector<normal_distribution> values,
           std::vector<size_type> types)
        : values_{values}, types_{types} {}
    // Construct from values (disregard types)
    Genome(std::vector<normal_distribution> values)
        : values_{values}, types_{} {}

    size_type size() const { return values_.size(); }
    double get_mean(size_type i) const { return values_[i].first; }
    double get_standard_deviation(size_type i) const {
        return values_[i].second;
    }
    double get_variance(size_type i) const {
        return std::pow(values_[i].second, 2);
    }
    int get_type(size_type i) const { return types_[i]; }

  private:
    std::vector<normal_distribution> values_;
    std::vector<size_type> types_;
};

// Forward declaration
std::ostream &operator<<(std::ostream &os, const Genome &g);

using Population = std::vector<double>;
using Individual = std::vector<double>;

template <class cost_function>
class EvolutionaryAlgorithm : public Optimizer<Individual> {

  public:
    // Public member functions

    // Construct from genome and optimization settings
    EvolutionaryAlgorithm(
        const Genome &genome, cost_function &training_cost_function,
        cost_function &testing_cost_function,
        std::function<void(const Individual &, const Individual &,
                           const Individual &)>
            print_func,
        size_type no_evals = 100, size_type max_iter = 1e4,
        size_type print_interval = 1e3, double abs_tol = 1e-6,
        size_type no_individuals = 1000, size_type no_parents = 100,
        size_type no_elites = 5, double partition_beta = 0.05,
        unsigned cross_over_mode = 0, unsigned cross_over_arg = 2,
        unsigned mutation_mode = 0, double mutation_rate = 0.1,
        double mutation_amplitude = 1e-1, double mutation_decay = 1e-2,
        bool print_debug = false);

    // Construct from dictionary, genome and cost function
    EvolutionaryAlgorithm(
        const Dictionary &dict, const Genome &genome,
        cost_function &training_cost_function,
        cost_function &testing_cost_function,
        std::function<void(const Individual &, const Individual &,
                           const Individual &)>
            print_func);

    virtual ~EvolutionaryAlgorithm() {}

    virtual std::vector<double> optimize();

    VectorSlice<double> get_individual(size_type i) {
        auto individual =
            VectorSlice<double>{population_, no_genes_ * i, no_genes_};
        return individual;
    }

  private:
    // Private member functions
    void initialize();
    std::vector<unsigned long> choose_datapoints(unsigned long no_points) const;
    void evaluate_costs(size_type start_at,
                        std::vector<unsigned long> training_data_pts);
    void save_elites(Population &individuals, const std::vector<double> &costs);
    void select_parents(Population &individuals,
                        const std::vector<double> &costs);
    void cross_over(size_type start_at);
    void n_point_cross_over(size_type start_at);
    void arithmetic_cross_over(size_type start_at);
    void mutate(size_type iteration, size_type start_at);
    unsigned read_cross_over_mode(const Dictionary &dict) const;
    unsigned read_cross_over_arg(const Dictionary &dict) const;
    unsigned read_mutation_mode(const Dictionary &dict) const;

    // Private data members
    Genome genome_;
    cost_function training_cost_function_;
    cost_function testing_cost_function_;
    std::function<void(const Individual &, const Individual &,
                       const Individual &)>
        print_function_;
    size_type no_evals_;
    unsigned long training_data_size_;
    unsigned long testing_data_size_;
    Population population_;
    Population parents_;
    std::vector<double> costs_;
    size_type max_iter_;
    size_type print_interval_;
    double abs_tol_;
    size_type no_genes_;
    size_type tot_no_individuals_;
    size_type no_individuals_;
    size_type no_parents_;
    size_type no_elites_;
    double partition_beta_;
    size_type cross_over_mode_;
    size_type cross_over_arg_;
    size_type mutation_mode_;
    double mutation_rate_;
    double mutation_amplitude_;
    double mutation_decay_;
    bool print_debug_;
    std::vector<size_type> training_data_pts_;
    std::vector<size_type> testing_data_pts_;
};

template <class cost_function>
EvolutionaryAlgorithm<cost_function>::EvolutionaryAlgorithm(
    const Dictionary &dict, const Genome &genome,
    cost_function &training_cost_function, cost_function &testing_cost_function,
    std::function<void(const Individual &, const Individual &,
                       const Individual &)>
        print_func)
    : EvolutionaryAlgorithm(
          genome, training_cost_function, testing_cost_function, print_func,
          read_single_value("no_evaluations", dict),
          read_single_value("max_iter", dict),
          read_single_value("print_interval", dict),
          read_single_value("abs_tol", dict),
          read_single_value("no_individuals", dict),
          read_single_value("no_parents", dict),
          read_single_value("no_elites", dict),
          read_single_value("partition_beta", dict), read_cross_over_mode(dict),
          read_cross_over_arg(dict), read_mutation_mode(dict),
          read_single_value("mutation_rate", dict),
          read_single_value("mutation_amplitude", dict),
          read_single_value("mutation_decay", dict),
          read_bool("print_debug", dict, false)) {
    std::string mutation_mode = read_string("mutation_mode", dict);
    if (mutation_mode == "perturb") {
        mutation_mode_ = mutation_modes::perturb;
    } else if (mutation_mode == "reset") {
        mutation_mode_ = mutation_modes::reset;
    } else {
        throw std::runtime_error{
            std::string("EvolutionaryAlgorithm: Illegal mutation mode ") +
            mutation_mode};
    }
    std::string cross_over_mode = read_string("cross_over_mode", dict);
    if (cross_over_mode == "n_point") {
        cross_over_mode_ = cross_over_modes::n_point;
        cross_over_arg_ = read_single_value("no_points", dict);
    } else if (cross_over_mode == "arithmetic") {
        cross_over_mode_ = cross_over_modes::arithmetic;
        auto mixing_mode = read_string("mixing", dict);
        if (mixing_mode == "random") {
            cross_over_mode_ = 1;
        } else if (mixing_mode == "equal" || mixing_mode == "") {
            cross_over_mode_ = 0;
        } else {
            throw std::runtime_error{
                std::string(
                    "EvolutionaryAlgorithm: Illegal arithmetic mixing mode ") +
                mixing_mode};
        }
    } else {
        throw std::runtime_error{std::string("Illegal cross-over mode ") +
                                 cross_over_mode};
    }
}

template <class cost_function>
EvolutionaryAlgorithm<cost_function>::EvolutionaryAlgorithm(
    const Genome &genome, cost_function &training_cost_function,
    cost_function &testing_cost_function,
    std::function<void(const Individual &, const Individual &,
                       const Individual &)>
        print_func,
    size_type no_evals, size_type max_iter, size_type print_interval,
    double abs_tol, size_type no_individuals, size_type no_parents,
    size_type no_elites, double partition_beta, unsigned cross_over_mode,
    unsigned cross_over_arg, unsigned mutation_mode, double mutation_rate,
    double mutation_amplitude, double mutation_decay, bool print_debug)
    : genome_{genome}, training_cost_function_{training_cost_function},
      testing_cost_function_{testing_cost_function},
      print_function_{print_func}, no_evals_{no_evals},
      training_data_size_{training_cost_function.size()},
      testing_data_size_{testing_cost_function.size()}, population_{},
      parents_(no_parents * genome.size(), 0), costs_{}, max_iter_{max_iter},
      print_interval_{print_interval}, abs_tol_{abs_tol},
      no_genes_{genome.size()}, tot_no_individuals_{no_individuals},
      no_individuals_{0}, no_parents_{no_parents}, no_elites_{no_elites},
      partition_beta_{partition_beta}, cross_over_mode_{cross_over_mode},
      cross_over_arg_{cross_over_arg}, mutation_mode_{mutation_mode},
      mutation_rate_{mutation_rate}, mutation_amplitude_{mutation_amplitude},
      mutation_decay_{mutation_decay}, print_debug_{print_debug},
      training_data_pts_{}, testing_data_pts_{} {
    if (MPI_rank == MPI_root) {
        std::cout << "\nEvolutionaryAlgorithm:\n"
                  << "Number of parameters: " << genome_.size()
                  << "\nCross-over mode: ";
        switch (cross_over_mode_) {
        case cross_over_modes::n_point:
            std::cout << cross_over_arg_ << " point cross-over" << std::endl;
            break;
        case cross_over_modes::arithmetic:
            std::cout << "arithmetic, mixing_mode: ";
            switch (cross_over_arg_) {
            case 0:
                std::cout << "equal mixing of parents" << std::endl;
                break;
            default:
                std::cout << "random mixing of parents" << std::endl;
            }
            break;
        default:
            throw std::runtime_error{std::string{"Illegal cross-over mode "} +
                                     std::to_string(cross_over_mode_)};
        }

        std::cout
            << "Number of training data points to use in cost evaluation each "
               "iteration: "
            << no_evals_ << "\n"
            << "Number of individuals per generation: " << no_individuals
            << "\n"
            << "Number of parents per generation: " << no_parents << "\n"
            << "Elitism is applied to the " << no_elites_
            << " best individuals of each generation\n"
            << "Maximum number of iterations: " << max_iter << "\n"
            << "Absolute tolerance: " << abs_tol << "\n"
            << "Initial mutation amplitude as a fraction of starting guess "
               "variance: "
            << mutation_amplitude << "\n"
            << "Mutation amplitude decay: " << mutation_decay << "\n"
            << "Printing results every " << print_interval << " iterations\n"
            << std::endl;
    }
    no_individuals_ = no_individuals / MPI_size;
    int rest = no_individuals % MPI_size;
    if (MPI_rank < rest) {
        ++no_individuals_;
    }
    population_ = Population(no_individuals_ * no_genes_);
    costs_ = std::vector<double>(no_individuals_);
    initialize();
}

template <class cost_function>
void EvolutionaryAlgorithm<cost_function>::initialize() {
    if (MPI_rank == MPI_root) {
        std::cout << "Initializing EvolutionaryAlgorithm" << std::endl;
    }
    std::random_device rd{};
    std::mt19937 gen{rd()};
    for (size_type gene = 0; gene != no_genes_; ++gene) {
        std::normal_distribution<double> draw{
            genome_.get_mean(gene), genome_.get_standard_deviation(gene)};
        for (size_type individual = 0; individual != no_individuals_;
             ++individual) {
            population_[individual * no_genes_ + gene] = draw(gen);
        }
    }
    // Choose which data points to use in initial cost evaluation and broadcast
    // selections to all processors
    if (MPI_rank == MPI_root) {
        training_data_pts_ = choose_datapoints(training_data_size_);
    }
    if (MPI_rank != MPI_root) {
        training_data_pts_.resize(no_evals_);
    }
    MPI_Bcast(&training_data_pts_[0], no_evals_, MPI_UNSIGNED_LONG, MPI_root,
              MPI_COMM_WORLD);
    // Compute initial costs, using the selected data points

    evaluate_costs(0, training_data_pts_);
    if (MPI_rank == MPI_root) {
        std::cout << "Done initializing EvolutionaryAlgorithm" << std::endl;
    }
}

template <class cost_function>
std::vector<unsigned long>
EvolutionaryAlgorithm<cost_function>::choose_datapoints(
    size_type no_points) const {
    std::random_device rd{};
    std::mt19937 gen{rd()};
    auto draw = std::uniform_int_distribution<unsigned long>{0, no_points - 1};
    auto data_points = std::vector<unsigned long>(no_evals_, 0);
    auto eval_fraction = no_evals_ / (double)no_points;
    if (eval_fraction == 1) {
        // Choose all available data points
        std::iota(data_points.begin(), data_points.end(), 0);
    } else if (eval_fraction < 0.5) {
        // Choose which datapoints to evaluate
        for (auto &pt : data_points) {
            unsigned long new_pt = 0;
            do {
                new_pt = draw(gen);
            } while (std::find(data_points.begin(), data_points.end(),
                               new_pt) != data_points.end());
            pt = new_pt;
        }
    } else {
        // Choose which datapoints not to evaluate
        std::vector<unsigned long> excluded((1 - eval_fraction) * no_points);
        for (auto &pt : excluded) {
            unsigned long new_pt = 0;
            do {
                new_pt = draw(gen);
            } while (std::find(excluded.begin(), excluded.end(), new_pt) !=
                     excluded.end());
            pt = new_pt;
        }
        size_type index = 0;
        // Fill up datapoints vector
        for (size_type i = 0; i != no_points; ++i) {
            if (std::find(excluded.begin(), excluded.end(), i) ==
                excluded.end()) {
                data_points[index++] = i;
            }
        }
    }
    return data_points;
}

template <class cost_function>
void EvolutionaryAlgorithm<cost_function>::evaluate_costs(
    size_type start_at, std::vector<unsigned long> training_data_pts) {
    for (size_type i = start_at; i != no_individuals_; ++i) {
        auto individual = get_individual(i);
        costs_[i] = training_cost_function_(individual, training_data_pts);
    }
}

template <class cost_function>
Individual EvolutionaryAlgorithm<cost_function>::optimize() {
    // Declare variables for use in message passing

    // Vector of individuals from all processors
    Population all_individuals(tot_no_individuals_ * no_genes_, 0);
    // Vector of the cost values of individuals from all processors
    std::vector<double> all_costs(tot_no_individuals_, 0);

    // Number of individuals from each processor
    int recvcounts_indv[MPI_size];
    // Number of genes (doubles) from each processor
    int genecounts_indv[MPI_size];

    // Addresses where each processor's values for individuals and costs start
    // after MPI_gatherv
    int offsets_indv[MPI_size];
    int offsets_cost[MPI_size];

    // Lowest cost value so far over all processors
    int winner_index;
    double min_cost = std::numeric_limits<double>::max();
    double old_min_cost = std::numeric_limits<double>::max();
    double min_cost_in = std::numeric_limits<double>::max();
    double old_min_cost_in = std::numeric_limits<double>::max();
    double min_cost_out = std::numeric_limits<double>::max();
    double old_min_cost_out = std::numeric_limits<double>::max();

    // Initialize clock
    high_resolution_clock::time_point t1 = high_resolution_clock::now();

    // Variables to keep track of changes over optimization
    Individual old_top_individual(no_genes_, 0);

    // Start optimization loop
    size_type iteration;
    for (iteration = 0; iteration != max_iter_; ++iteration) {
        // Gather individuals and their cost values on MPI_root
        // Gathers no_individuals_ from each processor, save in recvcounts_indv
        MPI_Gather(&no_individuals_, 1, MPI_INT, &recvcounts_indv[0], 1,
                   MPI_INT, MPI_root, MPI_COMM_WORLD);

        if (MPI_rank == MPI_root) {
            // Create genecounts_indv as a scaled version of recvcounts_indv to
            // account for the number of genes rather than the number of
            // individuals
            for (int i = 0; i != MPI_size; ++i) {
                genecounts_indv[i] = no_genes_ * recvcounts_indv[i];
            }
            // Compute offsets for receiving individuals and costs
            offsets_indv[0] = 0;
            offsets_cost[0] = 0;
            for (auto i = 1; i != MPI_size; ++i) {
                offsets_indv[i] = offsets_indv[i - 1] + genecounts_indv[i - 1];
                offsets_cost[i] = offsets_cost[i - 1] + recvcounts_indv[i - 1];
            }
        }

        // Receive all individuals from all processes
        MPI_Gatherv(&population_[0], no_individuals_ * no_genes_, MPI_DOUBLE,
                    &all_individuals[0], genecounts_indv, offsets_indv,
                    MPI_DOUBLE, MPI_root, MPI_COMM_WORLD);

        // Receive all cost values
        MPI_Gatherv(&costs_[0], no_individuals_, MPI_DOUBLE, &all_costs[0],
                    recvcounts_indv, offsets_cost, MPI_DOUBLE, MPI_root,
                    MPI_COMM_WORLD);
        if (MPI_rank == MPI_root) {
            // Find the individual with lowest cost
            for (auto &el : all_costs) {
                if (std::isnan(el)) {
                    el = std::numeric_limits<double>::max();
                }
            }
            winner_index =
                std::min_element(all_costs.begin(), all_costs.end()) -
                all_costs.begin();
            if (std::isnan(all_costs[winner_index])) {
                std::cout << "all_costs: \n" << all_costs << std::endl;
                throw std::runtime_error(
                    "winner_index: min_element returned NaN");
            }
            min_cost = all_costs[winner_index];
            auto parameter_avgs = std::vector<double>(no_genes_, 0);
            auto parameter_spreads = std::vector<double>(no_genes_, 0);
            auto sliced_individuals =
                VectorSliceCollection<double>{all_individuals, no_genes_};
            for (size_type i = 0; i != no_genes_; ++i) {
                auto values = std::vector<double>{};
                for (auto &individual : sliced_individuals) {
                    values.push_back(individual[i]);
                }
                parameter_avgs[i] = mean<double>(values);
                parameter_spreads[i] = standard_deviation<double>(values);
            }

            if (iteration % print_interval_ == 0 ||
                iteration == max_iter_ - 1) {
                high_resolution_clock::time_point t2 =
                    high_resolution_clock::now();
                auto duration = duration_cast<seconds>(t2 - t1).count();
                auto top_individual = sliced_individuals[winner_index];
                min_cost_in = training_cost_function_(
                    top_individual, training_data_pts_, print_debug_);
                testing_data_pts_ = choose_datapoints(testing_data_size_);
                min_cost_out = testing_cost_function_(
                    top_individual, testing_data_pts_, print_debug_);
                std::cout << "\nNumber of iterations: " << iteration << "/"
                          << max_iter_ << ", Time = " << duration
                          << " s."
                          // << "\nCost values: " << adjusted_costs
                          << "\nMean cost: " << mean(all_costs)
                          << "\nCost standard deviation: "
                          << standard_deviation(all_costs)
                          << "\nMinimum cost evaluated on stochastic training "
                             "sample: "
                          << min_cost_in
                          << "\nMinimum cost evaluated on stochastic testing "
                             "sample: "
                          << min_cost_out << ". Relative error:\n"
                          << "\nDelta cost (within training sample) = "
                          << min_cost - old_min_cost
                          << "\nDelta cost (out-of-training sample) = "
                          << min_cost_out - old_min_cost_out << "\n"
                          << "Current best individual: " << std::endl;
                print_function_(top_individual, parameter_avgs,
                                parameter_spreads);
                std::cout << std::endl;
                old_min_cost = min_cost;
                old_min_cost_in = min_cost_in;
                old_min_cost_out = min_cost_out;
            }
        }

        // Broadcast minimum cost value to all processors
        MPI_Bcast(&min_cost, 1, MPI_DOUBLE, MPI_root, MPI_COMM_WORLD);

        // Check convergence and break if converged
        if (min_cost < abs_tol_) {
            break;
        }
        // If not converged, select new parents out of all existing
        // individuals
        if (MPI_rank == MPI_root) {
            select_parents(all_individuals, all_costs);
        }
        // Broadcast the selected parents to all processes
        MPI_Bcast(&parents_[0], no_parents_ * no_genes_, MPI_DOUBLE, MPI_root,
                  MPI_COMM_WORLD);
        // Draw the data points to be used for next training and testing
        // step
        if (MPI_rank == MPI_root) {
            training_data_pts_ = choose_datapoints(training_data_size_);
        }
        if (MPI_rank != MPI_root) {
            training_data_pts_.resize(no_evals_);
        }
        MPI_Bcast(&training_data_pts_[0], no_evals_, MPI_UNSIGNED_LONG,
                  MPI_root, MPI_COMM_WORLD);
        // Apply elitism, perform cross-over and mutations, and evaluate
        // costs
        if (MPI_rank == MPI_root) {
            save_elites(all_individuals, all_costs);
            auto start_index = no_elites_;
            cross_over(start_index);
            mutate(iteration, start_index);
            evaluate_costs(0 /*start_index*/, training_data_pts_);
        } else {
            cross_over(0);
            mutate(iteration, 0);
            evaluate_costs(0, training_data_pts_);
        }
    }
    if (MPI_rank == MPI_root) {
        auto population_view =
            VectorSliceCollection<double>{all_individuals, no_genes_};
        auto top_individual = Individual{population_view[winner_index]};
        high_resolution_clock::time_point t2 = high_resolution_clock::now();
        auto duration = duration_cast<seconds>(t2 - t1).count();
        auto diff = 0;
        for (size_type i = 0; i != top_individual.size(); ++i) {
            diff += std::pow(top_individual[i] - old_top_individual[i], 2);
        }
        diff = std::sqrt(diff);
        std::cout << "Optimization finished after " << iteration
                  << " iterations due to ";
        if (min_cost < abs_tol_) {
            std::cout << "meeting convergence criteria" << std::endl;
        } else {
            std::cout << "reaching maximum number of iterations" << std::endl;
        }
        std::cout << "Time = " << duration << " s.\n" << std::endl;

        return top_individual;
    } else {
        return {};
    }
}

template <class cost_function>
void EvolutionaryAlgorithm<cost_function>::save_elites(
    Population &all_individuals, const std::vector<double> &all_costs) {
    // Apply elitism
    if (no_elites_ > no_individuals_) {
        throw std::runtime_error{"Number of elites may not be greater than the "
                                 "number of individuals allocated (by load "
                                 "balancing) to MPI rank 0."};
    }
    auto elite_indices = get_first_n_indices(all_costs, no_elites_);
    auto population_view =
        VectorSliceCollection<double>{all_individuals, no_genes_};
    auto indv_it = population_.begin();
    // Loop over elite individuals
    for (auto i : elite_indices) {
        // Overwrite the first no_elites_ individuals in individuals_
        for (auto replace_it = population_view[i].begin();
             replace_it < population_view[i].end();) {
            *indv_it++ = *replace_it++;
        }
    }
    // Save the cost values of the elite individuals locally
    for (size_type i = 0; i != no_elites_; ++i) {
        costs_[i] = all_costs[elite_indices[i]];
    }
}

template <class cost_function>
void EvolutionaryAlgorithm<cost_function>::select_parents(
    Population &all_individuals, const std::vector<double> &all_costs) {
    // Draw stochastically from Boltzmann (softmax) distribution to choose
    // parents based on cost values as energy
    // Compute Boltzmann distribution
    auto P_select = partition_function(all_costs, partition_beta_);
    // Compute cumulative distribution
    // Initialize to disallowed probability value
    std::vector<double> cum_prob(tot_no_individuals_, -1.0);

    std::partial_sum(P_select.begin(), P_select.end(), cum_prob.begin(),
                     std::plus<double>());
    // Initialize random number generator
    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::uniform_real_distribution<double> distribution{0, 1};
    auto parent_it = parents_.begin();
    for (size_type i = 0; i < no_parents_; ++i) {
        auto draw = distribution(gen);
        size_type index = 0;
        while (draw > cum_prob[index]) {
            ++index;
        }
        auto population_view =
            VectorSliceCollection<double>{all_individuals, no_genes_};
        for (auto replace_it = population_view[index].begin();
             replace_it != population_view[index].end();) {
            *parent_it++ = *replace_it++;
        }
        // std::cout << "Selecting individual " << index << " for parenthood "
        //           << std::endl;
    }
}

template <class cost_function>
void EvolutionaryAlgorithm<cost_function>::cross_over(size_type start_at) {
    switch (cross_over_mode_) {
    case cross_over_modes::n_point:
        return n_point_cross_over(start_at);
    case cross_over_modes::arithmetic:
        return arithmetic_cross_over(start_at);
    }
}

template <class cost_function>
void EvolutionaryAlgorithm<cost_function>::n_point_cross_over(
    size_type start_at) {
    size_type no_points = cross_over_arg_;
    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::uniform_int_distribution<int> draw_parent(0, no_parents_ - 1);
    std::uniform_int_distribution<int> draw_point(0, no_genes_ - 1);
    auto parent_view = VectorSliceCollection<double>{parents_, no_genes_};
    auto indv_view = VectorSliceCollection<double>{population_, no_genes_};
    for (auto indv_it = indv_view[start_at].begin();
         indv_it < population_.end();) {
        // Select parents
        auto a = draw_parent(gen);
        auto b = draw_parent(gen);
        Individual parent_a = parent_view[a];
        Individual parent_b = parent_view[b];
        // Select cross-over points
        std::vector<size_type> points{};
        for (size_type p = 0; p != no_points; ++p) {
            points.push_back(draw_point(gen));
        }
        std::sort(points.begin(), points.end());
        // if no_points is odd, make the last interval to be crossed over
        // be from the last drawn point to the end of the genome
        if (no_points % 2) {
            points.push_back(no_genes_ - 1);
        }
        // Perform cross-over of parents
        for (size_type p = 0; p != points.size() / 2; ++p) {
            std::swap_ranges(parent_a.begin() + points[2 * p],
                             parent_a.begin() + points[2 * p + 1],
                             parent_b.begin() + points[2 * p]);
        }
        // Overwrite individuals
        for (auto parent_it = parent_a.begin(); parent_it != parent_a.end();) {
            *indv_it++ = *parent_it++;
        }
        // Check if we've overwritten the last individual in population_
        if (indv_it == population_.end()) {
            break;
        }
        for (auto parent_it = parent_b.begin(); parent_it != parent_b.end();) {
            *indv_it++ = *parent_it++;
        }
    }
}

template <class cost_function>
void EvolutionaryAlgorithm<cost_function>::arithmetic_cross_over(
    size_type start_at) {
    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::uniform_int_distribution<int> draw_parent(0, no_parents_ - 1);
    std::uniform_real_distribution<double> draw_mixing(0, 1);
    bool mix_parents_randomly = cross_over_arg_;
    auto parent_view = VectorSliceCollection<double>{parents_, no_genes_};
    auto indv_view = VectorSliceCollection<double>{population_, no_genes_};
    for (auto indv_it = indv_view[start_at].begin();
         indv_it < population_.end();) {
        // Select parents
        auto a = draw_parent(gen);
        auto b = draw_parent(gen);
        Individual parent_a = parent_view[a];
        Individual parent_b = parent_view[b];
        // std::cout << "\nSelected parents for cross-over:\n"
        //           << a << ": " << parent_a << "\n"
        //           << b << ": " << parent_b << std::endl;
        double mix = 0.5;
        // Select mixing coefficient
        if (mix_parents_randomly) {
            mix = draw_mixing(gen);
        }
        // std::cout << "Mixing coefficient: " << mix << std::endl;
        // Perform cross-over operation
        for (size_type i = 0; i != no_genes_; ++i) {
            *indv_it++ = mix * parent_a[i] + (1 - mix) * parent_b[i];
        }
        // std::cout << " Resulting genotype: "
        //           << VectorSlice<double>{population_, indv_it - no_genes_,
        //                                  indv_it}
        //           << std::endl;
    }
}

template <class cost_function>
void EvolutionaryAlgorithm<cost_function>::mutate(size_type iteration,
                                                  size_type start_at) {
    double e = mutation_amplitude_ * std::exp(-mutation_decay_ * iteration);
    if (MPI_rank == MPI_root && mutation_mode_ == mutation_modes::perturb &&
        ((iteration % print_interval_ == 0) || iteration == 0)) {
        std::cout << "Iteration " << iteration
                  << ": mutation fraction of initial variance: " << e
                  << std::endl;
    }

    // Initialize rng
    std::random_device rd{};
    std::mt19937 gen{rd()};
    auto draw_prob = std::uniform_real_distribution<double>{0, 1};
    auto distributions = std::vector<std::normal_distribution<double>>{};
    for (size_type i = 0; i != no_genes_; ++i) {
        distributions.push_back(std::normal_distribution<double>{
            0, genome_.get_standard_deviation(i)});
    }
    auto population_view =
        VectorSliceCollection<double>{population_, no_genes_};
    // std::cout << "\nIndividuals before mutation: " << std::endl;
    // for (auto indv : population_view) {
    //     std::cout << indv << std::endl;
    // }
    for (size_type indv = start_at; indv != population_view.size(); ++indv) {
        for (size_type i = 0; i != no_genes_; ++i) {
            if (draw_prob(gen) < mutation_rate_) {
                if (mutation_mode_ == mutation_modes::perturb) {
                    // Compute scaling factor for mutations
                    population_view[indv][i] += e * distributions[i](gen);
                } else if (mutation_mode_ == mutation_modes::reset) {
                    population_view[indv][i] = distributions[i](gen);
                }
            }
        }
    }
    // std::cout << "\nIndividuals after mutation: " << std::endl;
    // for (auto indv : population_view) {
    //     std::cout << indv << std::endl;
    // }
}

std::ostream &operator<<(std::ostream &os, const Genome &g) {
    os << "Genome: \n";
    auto type_names =
        std::vector<std::string>{"Charge",
                                 "Epsilon",
                                 "Sigma",
                                 "Equilibrium bond length",
                                 "Bond force constant",
                                 "Equilibrium bond angle",
                                 "Angle force constant",
                                 "Proper torsion periodicity",
                                 "Proper torsion phase",
                                 "Proper torsion force constant",
                                 "Improper torsion periodicity",
                                 "Improper torsion phase",
                                 "Improper torsion force constant"};
    for (size_type i = 0; i != g.size(); ++i) {
        os << "[" << i << "] " << type_names[g.get_type(i)]
           << ": mean = " << g.get_mean(i)
           << ", standard deviation = " << g.get_standard_deviation(i) << "\n";
    }
    os << std::endl;
    return os;
}

template <class cost_function>
unsigned EvolutionaryAlgorithm<cost_function>::read_cross_over_mode(
    const Dictionary &dict) const {
    auto mode_str = read_string("cross_over_mode", dict);
    if (mode_str == "n_point") {
        return cross_over_modes::n_point;
    } else if (mode_str == "arithmetic") {
        return cross_over_modes::arithmetic;
    } else {
        throw std::runtime_error{
            std::string{"EvolutionaryAlgorithm::get_cross_over_mode(): "
                        "Illegal cross-over mode: "} +
            mode_str};
    }
}

template <class cost_function>
unsigned EvolutionaryAlgorithm<cost_function>::read_cross_over_arg(
    const Dictionary &dict) const {
    unsigned cross_over_mode = read_cross_over_mode(dict);
    unsigned cross_over_arg = std::numeric_limits<unsigned>::max();
    switch (cross_over_mode) {
    case cross_over_modes::n_point:
        cross_over_arg = read_single_value("no_points", dict);
        break;
    case cross_over_modes::arithmetic:
        std::string mixing_mode = read_string("mixing", dict);
        if (mixing_mode == "equal") {
            cross_over_arg = 0;
        } else if (mixing_mode == "random") {
            cross_over_arg = 1;
        } else {
            throw std::runtime_error{
                std::string{"EvolutionaryAlgorithm::read_cross_over_arg(): "
                            "Illegal mixing mode for arithmetic cross-over: "} +
                mixing_mode};
        }
        break;
    }
    return cross_over_arg;
}

template <class cost_function>
unsigned EvolutionaryAlgorithm<cost_function>::read_mutation_mode(
    const Dictionary &dict) const {
    auto mode_str = read_string("mutation_mode", dict);
    unsigned mutation_mode = std::numeric_limits<unsigned>::max();
    if (mode_str == "perturb") {
        mutation_mode = mutation_modes::perturb;
    } else if (mode_str == "reset") {
        mutation_mode = mutation_modes::reset;
    } else {
        throw std::runtime_error{
            std::string{"EvolutionaryAlgorithm::read_mutation_mode(): Illegal "
                        "mutation mode: "} +
            mode_str};
    }
    return mutation_mode;
}

} // namespace champion
#endif
