/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  _  \ /| | // __ \  /|   \    /| |
 //  /  \_#| |  |#| |#| |  |#| |#|   \/   |#| | \\ \#| |// / \\ \|#| |\ \  |#| |
|#| |    |#| |__|#| |#| |__|#| |#| |\  /| |#| | || |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| |_|/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   __#| |  |#| |#| |  |#| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |    |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: UnstructuredTree.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2018-11-08
MODIFIED BY:

DESCRIPTION:

General unstructured tree data structure.
Implements a concrete class template UnstructuredTree, templated on the type of
vertex, implementing the Tree interface, and a concrete class
UnstructuredTreeNode, implementing the TreeNode interface.

\******************************************************************************/

#ifndef CHAMPION_UNSTRUCTURED_TREE_HPP
#define CHAMPION_UNSTRUCTURED_TREE_HPP

#include <algorithm>
#include <cassert>
#include <limits>
#include <utility>
#include <vector>

#include "Tree.hpp"
#include "set_precision.hpp"

namespace champion {

/***************************** Class declarations *****************************/

// Forward declarations
template <typename T> class UnstructuredTree;

template <typename T> class UnstructuredTreeNode : public TreeNode<T> {
  public:
    // Construct from value (optional, otherwise default-constructed, and parent
    // (default nullptr)
    UnstructuredTreeNode(T value = T{}, UnstructuredTreeNode *parent = nullptr);

    // Functions implementing Tree interface

    // Value of current node
    size_type value() const override;
    // Which layer in the tree is this on? (root layer = 0)
    size_type layer() const override;
    // The number of nodes below this
    size_type size() const override;
    // How deep is the deepest tree starting at this node? (depth(leaf) = 0)
    size_type depth() const override;
    // Recursively compute, and set, the depth value for all nodes below this
    size_type compute_depths();
    // Recursively compute, and set, the size of all nodes below this
    // (i.e. # nodes below it)
    size_type compute_sizes();
    // Navigation (returns nullptr if out of bounds)
    // Return pointer to unique parent node
    const TreeNode<size_type> *up() const override;
    TreeNode<size_type> *up() override;
    // Return pointer to leftmost child node
    const TreeNode<size_type> *down() const override;
    TreeNode<size_type> *down() override;
    // Return pointer to next node with the same parent as this
    const TreeNode<size_type> *right() const override;
    TreeNode<size_type> *right() override;
    // Return pointer to previous node with the same parent as this
    const TreeNode<size_type> *left() const override;
    TreeNode<size_type> *left() override;
    // Return vector of pointers to all nodes sharing this' parent
    std::vector<const TreeNode<size_type> *> siblings() const override;
    std::vector<TreeNode<size_type> *> siblings() override;
    // Return vector of pointers to all nodes that have this as a parent
    std::vector<const TreeNode<size_type> *> children() const override;
    std::vector<TreeNode<size_type> *> children() override;
    // Dispatch a visitor
    void accept(ConstNodeVisitor<size_type> &visit) const override;
    void accept(NodeVisitor<size_type> &visit) override;

  private:
    // Friends
    template <typename T> friend class UnstructuredTree;

    // Private data members
    size_type layer_;
    size_type size_;
    size_type depth_;
    T value_;
    UnstructuredTreeNode *parent_;
    std::vector<UnstructuredTreeNode *> children_;
};

template <typename T> class UnstructuredTree : public Tree<T> {
  public:
    using iterator = TreeIterator<T, false>;
    using const_iterator = TreeIterator<T, true>;
    ~UnstructuredTree();
    // Default constructor
    UnstructuredTree() : root_{nullptr}, tail_{nullptr}, size_{0} {}
    // Construct from root value
    UnstructuredTree(T root_value);
    // Copy constructor
    UnstructuredTree(const UnstructuredTree<T> &UT);
    // Move constructor
    UnstructuredTree(UnstructuredTree<T> &&UT);
    // Copy assignment
    UnstructuredTree<T> &operator=(const UnstructuredTree<T> &UT);
    // Move assignment
    UnstructuredTree<T> &operator=(UnstructuredTree<T> &&UT);
    // Swap member function
    void swap(UnstructuredTree<T> &UT);
    // Create root for the tree (only allowed if it does not yet exist)
    void set_root(T root_value);

    // Range based access
    iterator begin() override;
    iterator end() override;
    const_iterator begin() const override;
    const_iterator end() const override;
    const_iterator cbegin() const override;
    const_iterator cend() const override;
    // Return a handle to the root node of the tree
    const TreeNode<T> *root() const override;
    TreeNode<T> *root() override;
    const TreeNode<T> *tail() const override;
    TreeNode<T> *tail() override;
    // Visit all nodes in tree, traverse depth first
    void visit_nodes(ConstNodeVisitor<T> &visitor) const override;
    void visit_nodes(NodeVisitor<T> &visitor) override;

    // Node creation and deletion
    TreeNode *add_node(TreeNode<T> *parent, T value);
    void delete_node(TreeNode<T> *node);

  private:
    // Private helper functions
    // Release all allocated memory
    void release_memory();

    // Data members
    UnstructuredTreeNode *root_;
    UnstructuredTreeNode *tail_;
    size_type size_;
};

/********************** Non-member function declarations **********************/

// Functions operating on UnstructuredTreeNodes

// Non-member swap function template
template <typename T>
void swap(UnstructuredTree<T> &UT1, UnstructuredTree<T> &UT2) {
    UT1.swap(UT2);
}

template <typename T>
bool operator==(const UnstructuredTree<T> &t1, const UnstructuredTree<T> &t2);
template <typename T>
bool operator!=(const UnstructuredTree<T> &t1, const UnstructuredTree<T> &t2);

/**************************** Function definitions ****************************/

// UnstructuredTreeNode member functions

template <typename T>
UnstructuredTreeNode<T>::UnstructuredTreeNode(T value,
                                              UnstructuredTreeNode *parent)
    : layer_{0}, value_{value}, parent_{parent}, children_{} {
    auto next = this->parent_;
    while (next) {
        next = next->parent_;
        ++layer_;
    }
}
template <typename T> size_type UnstructuredTreeNode<T>::layer() const {
    return layer_;
}
template <typename T> size_type UnstructuredTreeNode<T>::value() const {
    return value_;
}
template <typename T> size_type UnstructuredTreeNode<T>::size() const {
    return size_;
}
template <typename T> size_type UnstructuredTreeNode<T>::depth() const {
    return depth_;
}
template <typename T> size_type UnstructuredTreeNode<T>::compute_depths() {
    depth_ = 0;
    if (children_.empty()) {
        return depth_;
    }
    for (auto n : children_) {
        depth_ = std::max(depth_, n->compute_depths());
    }
    // Depth is one more than the depth of the deepest child node
    return ++depth_;
}

template <typename T> size_type UnstructuredTreeNode<T>::compute_sizes() {
    size_ = 1;
    for (auto n : children_) {
        size_ += n->compute_sizes();
    }
    return size_;
}

template <typename T>
typename UnstructuredTree<T>::iterator UnstructuredTree<T>::begin() {
    return iterator{root()};
}
template <typename T>
typename UnstructuredTree<T>::iterator UnstructuredTree<T>::end() {
    auto tail_ = tail();
    return iterator{++tail_};
}
template <typename T>
typename UnstructuredTree<T>::const_iterator
UnstructuredTree<T>::begin() const {
    return const_iterator{root()};
}
template <typename T>
typename UnstructuredTree<T>::const_iterator UnstructuredTree<T>::end() const {
    auto tail_ = tail();
    return const_iterator{++tail_};
}
template <typename T>
typename UnstructuredTree<T>::const_iterator
UnstructuredTree<T>::cbegin() const {
    return const_iterator{root()};
}
template <typename T>
typename UnstructuredTree<T>::const_iterator UnstructuredTree<T>::cend() const {
    auto tail_ = tail();
    return const_iterator{++tail_};
}

template <typename T> const TreeNode<T> *UnstructuredTreeNode<T>::up() const {
    return parent_;
}
TreeNode<T> *UnstructuredTreeNode<T>::up() { return parent_; }

template <typename T> const TreeNode<T> *UnstructuredTreeNode<T>::down() const {
    if (children_.empty()) {
        return nullptr;
    } else {
        return *children_.begin();
    }
}

template <typename T> TreeNode<T> *UnstructuredTreeNode<T>::down() {
    if (children_.empty()) {
        return nullptr;
    } else {
        return *children_.begin();
    }
}

template <typename T>
const TreeNode<T> *UnstructuredTreeNode<T>::right() const {
    if (!parent_) {
        return nullptr;
    }
    auto next_it =
        std::find(parent_->children_.begin(), parent_->children_.end(), this) +
        1;
    if (next_it == parent_->children_.end()) {
        return nullptr;
    } else {
        return *next_it;
    }
}

template <typename T> TreeNode<T> *UnstructuredTreeNode<T>::right() {
    if (!parent_) {
        return nullptr;
    }
    auto next_it =
        std::find(parent_->children_.begin(), parent_->children_.end(), this) +
        1;
    if (next_it == parent_->children_.end()) {
        return nullptr;
    } else {
        return *next_it;
    }
}

template <typename T> const TreeNode<T> *UnstructuredTreeNode<T>::left() const {
    auto current_it =
        std::find(parent_->children_.begin(), parent_->children_.end(), this);
    if (current_it == parent_->children_.begin()) {
        return nullptr;
    } else {
        return *(current_it - 1);
    }
}

template <typename T> TreeNode<T> *UnstructuredTreeNode<T>::left() {
    auto current_it =
        std::find(parent_->children_.begin(), parent_->children_.end(), this);
    if (current_it == parent_->children_.begin()) {
        return nullptr;
    } else {
        return *(current_it - 1);
    }
}

template <typename T>
std::vector<const TreeNode<T> *> UnstructuredTreeNode<T>::siblings() const {
    std::vector<const TreeNode<T> *> siblings{};
    if (parent_ == nullptr) {
        siblings.push_back(dynamic_cast<const TreeNode<T> *>(this));
        return siblings;
    }
    for (auto &c : parent_->children_) {
        siblings.push_back(c);
    }
    return siblings;
}

template <typename T>
std::vector<TreeNode<T> *> UnstructuredTreeNode<T>::siblings() {
    std::vector<TreeNode<T> *> siblings{};
    if (parent_ == nullptr) {
        siblings.push_back(dynamic_cast<TreeNode<T> *>(this));
        return siblings;
    }
    for (auto &c : parent_->children_) {
        siblings.push_back(c);
    }
    return siblings;
}

template <typename T>
std::vector<const TreeNode<T> *> UnstructuredTreeNode<T>::children() const {
    std::vector<const TreeNode<T> *> children;
    for (auto &c : children_) {
        children.push_back(c);
    }
    return children;
}

template <typename T>
std::vector<TreeNode<T> *> UnstructuredTreeNode<T>::children() {
    std::vector<TreeNode<T> *> children;
    for (auto &c : children_) {
        children.push_back(c);
    }
    return children;
}

template <typename T>
void UnstructuredTreeNode<T>::accept(ConstNodeVisitor<T> &visit) const {
    visit.visit(this);
}

template <typename T>
void UnstructuredTreeNode<T>::accept(NodeVisitor<T> &visit) {
    visit.visit(this);
}

// UnstructuredTree member functions

// Default constructor (no nodes)
template <typename T>
UnstructuredTree<T>::UnstructuredTree()
    : root_{nullptr}, tail_{nullptr}, size_{0} {}

// Construct from root value
template <typename T>
UnstructuredTree<T>::UnstructuredTree(T root_value)
    : root_{nullptr}, tail_{nullptr}, size_{1} {
    root_ = new UnstructuredTreeNode<T>{root_value, nullptr};
    tail_ = root_;
}

// Destructor
template <typename T> UnstructuredTree<T>::~UnstructuredTree() {
    release_memory();
}

// Copy constructor
template <typename T>
UnstructuredTree<T>::UnstructuredTree(const UnstructuredTree<T> &UT)
    : UnstructuredTree(UT.root_->value()) {}

// Copy assignment
template <typename T>
UnstructuredTree<T> &UnstructuredTree<T>::
operator=(const UnstructuredTree<T> &UT) {
    auto UT_copy = UnstructuredTree<T>{UT};
    this->swap(UT_copy);
    return *this;
}

// Move constructor
template <typename T>
UnstructuredTree<T>::UnstructuredTree(UnstructuredTree<T> &&UT)
    : root_{UT.root_}, size_{UT.size_} {
    UT.graph_ = {};
    UT.root_ = {};
}

// Move assignment
template <typename T>
UnstructuredTree<T> &UnstructuredTree<T>::operator=(UnstructuredTree<T> &&UT) {
    this->swap(UT);
    return *this;
}

template <typename T> void UnstructuredTree<T>::swap(UnstructuredTree<T> &UT) {
    using std::swap;
    swap(root_, UT.root_);
    swap(tail_, UT.tail_);
    swap(size_, UT.size_);
}

template <typename T> void set_root(T root_value) {
    if (root_ == nullptr) {
        root_ = new UnstructuredTreeNode<T>{root_value, nullptr};
        tail_ = root_;
        size_ = 1;
    } else {
        throw std::runtime_error{"champion::UnstructuredTree: Cannot set root "
                                 "if it already exists."};
    }
}

template <typename T> const TreeNode<T> *UnstructuredTree<T>::root() const {
    return root_;
}
template <typename T> TreeNode<T> *UnstructuredTree<T>::root() { return root_; }

template <typename T> const TreeNode<T> *UnstructuredTree<T>::tail() const {
    return tail_;
}
template <typename T> TreeNode<T> *UnstructuredTree<T>::tail() { return tail_; }

template <typename T>
void UnstructuredTree<T>::visit_nodes(ConstNodeVisitor<T> &visitor) const {
    root_->explore_downstream(visitor);
}

template <typename T>
void UnstructuredTree<T>::visit_nodes(NodeVisitor<T> &visitor) {
    root_->explore_downstream(visitor);
}

template <typename T>
TreeNode<T> *UnstructuredTree<T>::add_node(TreeNode<T> *parent, T value) {
    // Make sure the parent node belongs to the tree
    bool exists = false;
    for (auto it = this->begin(); it != this->end(); ++it) {
        if (it == parent) {
            exists = true;
            break;
        }
    }
    if (!exists) {
        throw std::runtime_error{"champion::UnstructuredTree::add_node(): "
                                 "Parent node given is not part of the tree."};
    }
    // Create the new node and add it as the rightmost child of the parent
    auto node = new UnstructuredTreeNode<T>{value, parent};
    parent->children_.push_back(node);
    // Update tail_ if the new node is the last one
    auto it = node;
    if ((++it).get() == nullptr) {
        tail_ = node;
    }
    // Update sizes and depths
    root_->compute_sizes();
    root_->compute_depths();
}

template <typename T>
void UnstructuredTree<T>::delete_node(TreeNode<T> *node) {}

template <typename T> void UnstructuredTree<T>::release_memory() {
    if (root_ == nullptr) {
        size_ = 0;
        return;
    }
    auto recorder = NodeRecorder<T>{};
    visit_nodes(recorder);
    for (auto node : recorder.nodes()) {
        delete (node);
    }
    root_ = nullptr;
    size_ = 0;
}

// Non-member functions

template <typename T>
bool operator==(const UnstructuredTree<T> &t1, const UnstructuredTree<T> &t2) {
    return (compare_trees(t1, t2) == 0);
}

template <typename T>
bool operator!=(const UnstructuredTree<T> &t1, const UnstructuredTree<T> &t2) {
    return (compare_trees(t1, t2) != 0);
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const UnstructuredTree<T> &UT) {
    os << "UnstructuredTree: " << dynamic_cast<const Tree<T> &>(UT) << "\n}";
    return os;
}

} // namespace champion

#endif
