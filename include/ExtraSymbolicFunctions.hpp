/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                            ANALYSIS         TOOLKIT                          |
|                                                                              |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: ExtraSymbolicFunctions.hpp

AUTHOR: Fabian Årén (fabian.aaren@chalmers.se) 2019-08-14
MODIFIED BY:

DESCRIPTION:
Some extra mathematical functions for working with champion::Symbolic.

\******************************************************************************/

#ifndef CHAMPION_ERF_HPP
#define CHAMPION_ERF_HPP

#include <iostream>

#include <cmath>
#include <numeric>
#include <typeinfo>
#include <vector>

#include "Complex.hpp"
#include "Matrix.hpp"
#include "ObjectRegistry.hpp"
#include "Symbolic.hpp"
#include "Vector.hpp"

namespace champion
{

class ERF
{
public:
    //Default constructor:
    ERF(Symbolic z = 0) : z_{z}, added_{0}, multiply_{1} {};
    // Copy constructor
    ERF(const ERF &) = default;
    // Copy assignment
    ERF &operator=(const ERF &) = default;
    // Move constructor
    ERF(ERF &&) = default;
    // Move assignment
    ERF &operator=(ERF &&) = default;
    // Create a new independent variable

    //Public functions
    // Derivatives
    Symbolic derivative(const std::string &variable,
                        unsigned short order = 1) const;

    template <typename... Args>
    Symbolic evaluate(const std::string &variable, real value,
                      Args... args) const;

    // Mathematical operators
    ERF operator-() const;
    ERF &operator+=(const Symbolic &);
    ERF &operator*=(const Symbolic &);
    ERF &operator/=(const Symbolic &);

private:
    Symbolic z_;
    Symbolic added_;
    Symbolic multiply_;
};

// Derivatives
Symbolic
ERF::derivative(const std::string &variable,
                unsigned short order) const
{

    Symbolic u;
    u = 2 / sqrt(pi) * z_.derivative(variable, order) * exp(z_ * z_);
    return u;
}
template <typename... Args>
Symbolic ERF::evaluate(const std::string &variable, real value, Args... args) const
{
    auto add_val = added_.evaluate(variable, value, args...).symb_to_double();
    auto multiply_val = multiply_.evaluate(variable, value, args...).symb_to_double();
    auto eval_val = z_.evaluate(variable, value, args...).symb_to_double();

    return add_val + multiply_val * std::erf(eval_val);
}

// Binary operators between ERF and Symbols

ERF &ERF::operator+=(const Symbolic &S)
{
    added_ += S;
    return *this;
}

ERF operator+(const ERF &E, const Symbolic &S)
{
    auto res = E;
    res += S;
    return res;
}

ERF &operator-=(ERF &E, const Symbolic &S) { return E += -S; }

ERF operator-(const ERF &E, const Symbolic &S)
{
    auto res = E;
    return res -= S;
}

ERF &ERF::operator*=(const Symbolic &S)
{
    added_ *= S;
    multiply_ *= S;
    return *this;
}

ERF operator*(const ERF &E, const Symbolic &S)
{
    auto res = E;
    res *= S;
    return res;
}

ERF &ERF::operator/=(const Symbolic &S)
{
    added_ /= S;
    multiply_ /= S;
    return *this;
}

ERF operator/(const ERF &E, const Symbolic &S)
{
    auto res = E;
    return res /= S;
}

// Comuted version of binary operators

ERF operator+(const Symbolic &S, const ERF &E)
{
    auto res = E;
    res += S;
    return res;
}

ERF &operator-=(const Symbolic &S, ERF &E) { return -E += S; }

ERF operator-(const Symbolic &S, const ERF &E)
{
    auto res = E;
    return res -= S;
}

ERF operator*(const Symbolic &S, const ERF &E)
{
    auto res = E;
    res *= S;
    return res;
}

ERF operator/(const Symbolic &S, const ERF &E)
{
    auto res = E;
    return res /= S;
}

class ERFC
{
public:
    //Default constructor:
    ERFC(Symbolic z = 0) : z_{z}, added_{0}, multiply_{1} {};
    // Copy constructor
    ERFC(const ERFC &) = default;
    // Copy assignment
    ERFC &operator=(const ERFC &) = default;
    // Move constructor
    ERFC(ERFC &&) = default;
    // Move assignment
    ERFC &operator=(ERFC &&) = default;
    // Create a new independent variable

    //Public functions
    // Derivatives
    Symbolic derivative(const std::string &variable,
                        unsigned short order = 1) const;

    template <typename... Args>
    Symbolic evaluate(const std::string &variable, real value,
                      Args... args) const;

    // Mathematical operators
    ERFC operator-() const;
    ERFC &operator+=(const Symbolic &);
    ERFC &operator*=(const Symbolic &);
    ERFC &operator/=(const Symbolic &);

private:
    Symbolic z_;
    Symbolic added_;
    Symbolic multiply_;
};

// Derivatives
Symbolic
ERFC::derivative(const std::string &variable,
                 unsigned short order) const
{

    Symbolic u;
    u = -2 / sqrt(pi) * z_.derivative(variable, order) * exp(z_ * z_);
    return u;
}
template <typename... Args>
Symbolic ERFC::evaluate(const std::string &variable, real value, Args... args) const
{
    auto add_val = added_.evaluate(variable, value, args...).symb_to_double();
    auto multiply_val = multiply_.evaluate(variable, value, args...).symb_to_double();
    auto eval_val = z_.evaluate(variable, value, args...).symb_to_double();

    return add_val + multiply_val * std::erfc(eval_val);
}

// Binary operators between ERF and Symbols

ERFC &ERFC::operator+=(const Symbolic &S)
{
    added_ += S;
    return *this;
}

ERFC operator+(const ERFC &E, const Symbolic &S)
{
    auto res = E;
    res += S;
    return res;
}

ERFC &operator-=(ERFC &E, const Symbolic &S) { return E += -S; }

ERFC operator-(const ERFC &E, const Symbolic &S)
{
    auto res = E;
    return res -= S;
}

ERFC &ERFC::operator*=(const Symbolic &S)
{
    added_ *= S;
    multiply_ *= S;
    return *this;
}

ERFC operator*(const ERFC &E, const Symbolic &S)
{
    auto res = E;
    res *= S;
    return res;
}

ERFC &ERFC::operator/=(const Symbolic &S)
{
    added_ /= S;
    multiply_ /= S;
    return *this;
}

ERFC operator/(const ERFC &E, const Symbolic &S)
{
    auto res = E;
    return res /= S;
}

// Comuted version of binary operators

ERFC operator+(const Symbolic &S, const ERFC &E)
{
    auto res = E;
    res += S;
    return res;
}

ERFC &operator-=(const Symbolic &S, ERFC &E) { return -E += S; }

ERFC operator-(const Symbolic &S, const ERFC &E)
{
    auto res = E;
    return res -= S;
}

ERFC operator*(const Symbolic &S, const ERFC &E)
{
    auto res = E;
    res *= S;
    return res;
}

ERFC operator/(const Symbolic &S, const ERFC &E)
{
    auto res = E;
    return res /= S;
}
/*
class DiracDelta
{
public:
    //Default constructor:
    DiracDelta(Symbolic z = 0) : z_{z} {};
    // Copy constructor
    DiracDelta(const DiracDelta &) = default;
    // Copy assignment
    DiracDelta &operator=(const DiracDelta &) = default;
    // Move constructor
    DiracDelta(DiracDelta &&) = default;
    // Move assignment
    DiracDelta &operator=(DiracDelta &&) = default;
    // Create a new independent variable

    //Public functions
    // Derivatives
    real derivative(const std::string &variable,
                    unsigned short order = 1) const;

    template <typename... Args>
    real evaluate(const std::string &variable, real value,
                  Args... args) const;

private:
    Symbolic z_;
};

real DiracDelta::derivative(const std::string &variable,
                            unsigned short order) const
{
    std::cout << "No don't derive the dirac delta function, it is not well behaved" << std::endl;
    return 0; //assumes for now that it returns 0
}

template <typename... Args>
real DiracDelta::evaluate(const std::string &variable, real value, Args... args) const
{
    auto eval_val = z_.evaluate(variable, value, args...);

    if (eval_val == 0)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

class HeavysideTheta
{
public:
    //Default constructor:
    HeavysideTheta(Symbolic z = 0) : z_{z} {};
    // Copy constructor
    HeavysideTheta(const HeavysideTheta &) = default;
    // Copy assignment
    HeavysideTheta &operator=(const HeavysideTheta &) = default;
    // Move constructor
    HeavysideTheta(HeavysideTheta &&) = default;
    // Move assignment
    HeavysideTheta &operator=(HeavysideTheta &&) = default;
    // Create a new independent variable

    //Public functions
    // Derivatives
    DiracDelta derivative(const std::string &variable,
                          unsigned short order = 1) const;

    template <typename... Args>
    real evaluate(const std::string &variable, real value,
                  Args... args) const;

private:
    Symbolic z_;
};

template <typename... Args>
real HeavysideTheta::evaluate(const std::string &variable, real value, Args... args) const
{
    auto eval_val = z_.evaluate(variable, value, args...);

    if (eval_val < 0)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

DiracDelta HeavysideTheta::derivative(const std::string &variable,
                                      unsigned short order) const
{
    DiracDelta return_object = DiracDelta(variable);
    return return_object;
}
*/
} // namespace champion

#endif