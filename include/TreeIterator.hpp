/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: TreeIterator.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-27
MODIFIED BY:

DESCRIPTION:

\******************************************************************************/

#include <iterator>

#include "choose.hpp"

/***************************** Class declarations *****************************/

namespace champion {

// Forward declarations
template <typename T> class TreeNode;

template <typename T, bool is_const = true> class TreeIterator {
  public:
    using ValueType = Choose_type<is_const, const TreeNode<T>, TreeNode<T>>;

    using difference_type = ptrdiff_t;
    using value_type = ValueType;
    using pointer = ValueType *;
    using reference = ValueType &;

    // Construct from pointer to TreeNode
    TreeIterator(pointer p, pointer last = nullptr) : it_{p}, last_{last} {}

    reference operator*() const { return *it_; }
    template <typename member> pointer operator->() const { return it_; }
    // Prefix increment/decrement
    TreeIterator &operator++();
    TreeIterator &operator--();
    // Postfix increment/decrement
    TreeIterator operator++(int);
    TreeIterator operator--(int);
    bool operator==(const TreeIterator &It) const;

    // Get raw pointer to ValueType
    pointer get() { return it_; }

  private:
    pointer it_;
    pointer last_;
};

/**************************** Function definitions ****************************/

template <typename T, bool is_const>
TreeIterator<T, is_const> &TreeIterator<T, is_const>::operator++() {
    auto next = it_->down();
    if (next) {
        it_ = next;
        return *this;
    }
    next = it_->right();
    if (next) {
        it_ = next;
        return *this;
    }
    auto tmp = it_;
    do {
        tmp = tmp->up();
        if (tmp) {
            next = tmp->right();
            if (next) {
                it_ = next;
                return *this;
            }
        }
    } while (tmp);
    it_ = nullptr;
    return *this;
}

template <typename T, bool is_const>
TreeIterator<T, is_const> &TreeIterator<T, is_const>::operator--() {
    if (it_ == nullptr) {
        it_ = last_;
        return *this;
    }
    auto next = it_->left();
    auto tmp = next;
    if (next) {
        do {
            tmp = next->down();
            auto tmp2 = tmp;
            while (tmp2) {
                next = tmp2;
                tmp2 = tmp2->right();
            }
        } while (tmp);
        it_ = next;
        return *this;
    }
    it_ = it_->up();
    return *this;
}

template <typename T, bool is_const>
TreeIterator<T, is_const> TreeIterator<T, is_const>::operator++(int) {
    TreeIterator<T, is_const> It{*this};
    ++(*this);
    return It;
}

template <typename T, bool is_const>
TreeIterator<T, is_const> TreeIterator<T, is_const>::operator--(int) {
    TreeIterator It{*this};
    --(*this);
    return It;
}

template <typename T, bool is_const>
bool TreeIterator<T, is_const>::
operator==(const TreeIterator<T, is_const> &It) const {
    return this->it_ == It.it_;
}

template <typename T, bool is_const>
bool operator!=(const TreeIterator<T, is_const> &It1,
                const TreeIterator<T, is_const> &It2) {
    return !(It1 == It2);
}

} // namespace champion
