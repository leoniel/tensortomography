#ifndef CHAMPION_UNITS_HPP
#define CHAMPION_UNITS_HPP

#include <cmath>
#include <stdexcept>
#include <string>

#include "set_precision.hpp"

namespace champion {

namespace units {
// Constants
const double N_A = 6.022140857e23;
const double pi = 3.14159265359;
// Length units
const double nm = 1;
const double A = 0.1;
const double pm = 1e-3;
const double um = 1e3;
const double mm = 1e6;
const double cm = 1e7;
const double m = 1e9;
const double bohr = 0.0529177;
// Time units
const double ps = 1;
const double fs = 1e-3;
const double ns = 1e3;
const double us = 1e6;
const double ms = 1e9;
const double s = 1e12;
const double atu = 2.418884326505e-5;
// Mass units
const double amu = 1;
const double me = 0.00054858;
const double g = N_A;
const double kg = N_A * 1e3;
// Energy units
const double eV = 96.4869;
const double keV = 1e3 * eV;
const double meV = 1e-3 * eV;
const double J = kg * m * m / (s * s);
const double kJ = 1e3 * J;
const double Hartree = 27.21139 * eV;
// Force units
const double N = kg * m / (s * s);
// Pressure units
const double Pa = N / (m * m);
const double bar = Pa * 1e5;
// Temperature units
const double K = 1;
// Dimensionless units
const double mol = 6.022140857e23;
const double rad = 1;
const double degrees = pi / 180;
// Misc. units
const double C = 1 / (1.60217662e-19);
const double F = s * s * C * C / (m * m * m * kg);
// Dependent constants
const double epsilon0 = 8.854187817e-12 * std::pow(m, -3) * std::pow(kg, -1) *
                        std::pow(C, 2) * std::pow(s, 2);
} // namespace units

double convert_units(double value, double from, double to) {
    return value * from / to;
}

// Length unit literals

// Default length unit: nanometer
constexpr long double operator""_nm(long double nm) { return nm; }

// Angstrom
constexpr long double operator""_A(long double A) {
    // 1 A = 0.1 nm
    return 0.1 * A;
}
// Convert nm value to Angstrom
constexpr real Angstrom(real nm) { return 10 * nm; }

// Bohr radii
constexpr long double operator""_bohr(long double bohr) {
    return 0.0529177 * bohr;
}

// picometer
constexpr long double operator""_pm(long double pm) { return 1e-3 * pm; }

// meters
constexpr long double operator""_m(long double m) { return 1e9 * m; }

real convert_length_units(real value, std::string unit) {
    if (unit == "_nm" || unit == "") {
        return value;
    } else if (unit == "_A" || unit == "_Angstrom") {
        return value * 1e-1;
    } else if (unit == "_pm") {
        return value * 1e-3;
    } else if (unit == "_bohr") {
        return value * 0.0529177;
    } else if (unit == "_m") {
        return value * 1e9;
    } else {
        throw std::runtime_error{"champion::units: No such length unit: \"" +
                                 unit + "\""};
    }
}

// mass unit literals

// Default mass unit: amu (Daltons)
constexpr long double operator""_amu(long double amu) { return amu; }
// D for Daltons
constexpr long double operator""_D(long double D) { return D; }
// g/mol is also the same unit
constexpr long double operator""_g_p_mol(long double gpm) { return gpm; }
// Electron mass
constexpr long double operator""_me(long double me) { return 0.00054858 * me; }
// kg
constexpr long double operator""_kg(long double kg) {
    return 6.022140857e26 * kg;
}
// g
constexpr long double operator""_g(long double g) { return 6.022140857e23 * g; }

real convert_mass_units(real value, std::string unit) {
    if (unit == "_amu" || unit == "_g/mol" || unit == "_D" || unit == "") {
        return value;
    } else if (unit == "_m_e" || unit == "_me") {
        return value * 0.00054858;
    } else if (unit == "_kg") {
        return value * 6.022140857e26;
    } else {
        throw std::runtime_error{"champion::units: No such mass unit: \"" +
                                 unit + "\""};
    }
}

// Time unit literals

// Default time unit: picoseconds
constexpr long double operator""_ps(long double ps) { return ps; }
// femtoseconds
constexpr long double operator""_fs(long double fs) { return 1e-3 * fs; }
// nanoseconds
constexpr long double operator""_ns(long double ns) { return 1e3 * ns; }
// atomic time units
constexpr long double operator""_atu(long double atu) {
    return 2.418884326505e-8 * atu;
}

real convert_time_units(real value, std::string unit) {
    if (unit == "_ps" || unit == "_picoseconds" || unit == "") {
        return value;
    }
    if (unit == "_fs" || unit == "_femtoseconds") {
        return value * 1e-3;
    }
    if (unit == "_ns" || unit == "_nanoseconds") {
        return value * 1e3;
    }
    if (unit == "_atu" || unit == "_au") {
        return value * 2.418884326505e-5;
    } else if (unit == "_s" || unit == "_seconds") {
        return value * 1e12;
    } else {
        throw std::runtime_error{"champion::units: No such time unit: \"" +
                                 unit + "\""};
    }
}

// Energy unit literals

// Default energy unit: kJ/mol (consequence of length, mass, time units)
constexpr long double operator""_kJ_p_mol(long double kjpm) { return kjpm; }
// Hartree
constexpr long double operator""_H(long double H) { return 2625.5 * H; }
// electron volts
constexpr long double operator""_eV(long double eV) { return 96.4869 * eV; }
// Joules
constexpr long double operator""_J(long double J) { return 6.022140857e20 * J; }

real convert_energy_units(real value, std::string unit) {
    if (unit == "_kJ/mol") {
        return value;
    }
    if (unit == "_H" || unit == "_Hartree" || unit == "_au") {
        return value * 2625.5;
    } else if (unit == "_eV" || unit == "_electronvolts") {
        return value * 96.4869;
    } else if (unit == "_J" || unit == "_Joules") {
        return value * 6.022140857e20;
    } else {
        throw std::runtime_error{"champion::units: No such energy unit: \"" +
                                 unit + "\""};
    }
}

// Angle unit literals

// Default angle unit: radians
constexpr long double operator""_rad(long double rad) { return rad; }

// degrees
constexpr long double operator""_deg(long double deg) {
    return ((2 * pi) / 360.0) * deg;
}
constexpr real degrees(real radians) { return radians * 360.0 / (2 * pi); }

real convert_angle_units(real value, std::string unit) {
    if (unit == "_rad" || unit == "") {
        return value;
    } else if (unit == "_deg" || unit == "_degrees") {
        return value * pi / 180;
    } else {
        throw std::runtime_error{"champion::units: No such angle unit: \"" +
                                 unit + "\""};
    }
}

// Force unit literals

real convert_force_units(real value, std::string unit) {
    if (unit == "_kJ/mol/nm" || unit == "_kJ/(mol*nm)" || unit == "") {
        return value;
    }
    if (unit == "_H/bohr" || unit == "_hartree/bohr" || unit == "_au") {
        return value * units::Hartree / units::bohr;
    } else if (unit == "_eV/A") {
        return value * units::eV / units::A;
    } else if (unit == "_eV/nm") {
        return value * units::eV / units::nm;
    } else if (unit == "_J" || unit == "_Joules") {
        return value * 6.022140857e20;
    } else {
        throw std::runtime_error{"champion::units: No such force unit: \"" +
                                 unit + "\""};
    }
}

// Convert pressure units

real convert_pressure_units(real value, std::string unit) {
    if (unit == "_kJ/mol/nm^3" || unit == "_kJ/(mol*nm^3)" || unit == "") {
        return value;
    } else if (unit == "_Pa") {
        return convert_units(value, units::Pa, 1);
    } else if (unit == "_bar") {
        return convert_units(value, units::bar, 1);
    } else {
        throw std::runtime_error{"champion::units: No such pressure unit: \"" +
                                 unit + "\""};
    }
}

// Density unit literals

// Default density unit (consequence of length, mass, time units)
constexpr long double operator""_amu_p_nm3(long double apn3) { return apn3; }
// grams per cubic centimeter
constexpr long double operator""_g_p_cm3(long double gpc3) {
    return 602.2140857 * gpc3;
}
// kg per m^3
constexpr long double operator""_kg_p_m3(long double kpm3) {
    return 0.6022140857 * kpm3;
}
real convert_density_units(real value, std::string unit) {
    if (unit == "_amu/nm^3" || unit == "") {
        return value;
    } else if (unit == "_g/cm^3") {
        return value * 602.2140857;
    } else if (unit == "_kg/m^3") {
        return value * 0.6022140857;
    } else {
        throw std::runtime_error{"champion::units: No such density unit: \"" +
                                 unit + "\""};
    }
}

// Velocity units
real convert_velocity_units(real value, std::string unit) {
    if (unit == "_nm/ps" || unit == "") {
        return value;
    } else if (unit == "_bohr/atu") {
        return value * units::bohr / units::atu;
    } else if (unit == "_A/ps") {
        return value * units::A / units::ps;
    } else if (unit == "m/s") {
        return value * units::m / units::s;
    } else {
        throw std::runtime_error{"champion::units: No such velocity unit: \"" +
                                 unit + "\""};
    }
}

real convert_linear_force_constant_units(real value, std::string unit) {
    if (unit == "_kJ/mol/nm^2" || unit == "_kJ/(mol*nm^2)" || unit == "") {
        return value;
    } else {
        throw std::runtime_error{
            "champion::units: No such linear force constant unit: \"" + unit +
            "\""};
    }
}

real convert_angular_force_constant_units(real value, std::string unit) {
    if (unit == "_kJ/(mol*nm*rad)" || unit == "") {
        return value;
    } else {
        throw std::runtime_error{
            "champion::units: No such angular force constant unit: \"" + unit +
            "\""};
    }
}

real convert_charge_units(real value, std::string unit) {
    if (unit == "_e0" || unit == "") {
        return value;
    } else {
        throw std::runtime_error{
            "champion::units: No such angluar force constant unit: \"" + unit +
            "\""};
    }
}

} // namespace champion

#endif
