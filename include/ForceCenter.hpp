/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2018 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: ForceCenter.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2018-01-15
MODIFIED BY:

DESCRIPTION:

ForceCenter<level>


\******************************************************************************/

#ifndef CHAMPION_FORCE_CENTER_HPP
#define CHAMPION_FORCE_CENTER_HPP

#include "GraphTree.hpp"
#include "ObjectIndex.hpp"
#include "set_precision.hpp"

namespace champion {

/***************************** Class declarations *****************************/

// Forward declarations
template <level_type n> class ObjectRegistry;
template <level_type n, level_type level> class Body;
template <level_type n, level_type level> class ForceCenterType;
template <level_type n, level_type level> class Trajectory;

template <level_type n, level_type level> class ForceCenter {
  public:
    // Public types
    template <level_type ll> using body_index = ObjectIndex<Body, n, ll>;
    template <level_type ll>
    using fc_index = ObjectIndex<::champion::ForceCenter, n, ll>;
    template <level_type ll>
    using fct_index = ObjectIndex<ForceCenterType, n, ll>;
    template <level_type ll>
    using trajectory_index = ObjectIndex<Trajectory, n, ll>;

    // Public member functions
    ForceCenter(ObjectRegistry<n> &reg, size_type ID, fct_index<level> type,
                GraphTree<body_index<level>> graph_tree)
        : reg_{&reg}, id_{ID}, type_{type}, tree_{graph_tree},
          trajectory_{
              reg.get_object(graph_tree.graph()[0]).get_trajectory_id()} {}

    fc_index<level> get_id() const { return id_; }
    fct_index<level> get_type_id() { return type_; }
    // Get a reference to ForceCenterType
    const ForceCenterType<n, level> &type() const {
        return reg_->template get_object<ForceCenterType, level>(type_);
    }
    ForceCenterType<n, level> &type() {
        return reg_->template get_object<ForceCenterType, level>(type_);
    }
    // Get a reference to the body at the root of the graph tree
    body_index<level> get_body_id() const {
        return body_index<level>{tree_.graph()[tree_.root()->value()]};
    }
    const Body<n, level> &body() const {
        return reg_->get_object(get_body_id());
    }
    Body<n, level> &body() { return reg_->get_object(get_body_id()); }
    // Get const reference to graph tree
    const GraphTree<body_index<level>> &tree() const { return tree_; }
    // Get the trajectory index
    trajectory_index<level> get_trajectory_id() const { return trajectory_; }
    template <level_type ll> const Trajectory<n, ll> &trajectory() const {
        return reg_->get_object(trajectory_);
    }
    template <level_type ll> Trajectory<n, ll> &trajectory() {
        return reg_->get_object(trajectory_);
    }

  private:
    // Private data members
    ObjectRegistry<n> *reg_;
    fc_index<level> id_;
    fct_index<level> type_;
    GraphTree<body_index<level>> tree_;
    trajectory_index<level> trajectory_;
};

/*************************** Function definitions *****************************/

template <level_type n, level_type level>
bool operator<(const ForceCenter<n, level> &FC1,
               const ForceCenter<n, level> &FC2) {
    return FC1.tree() < FC2.tree();
}

template <level_type n, level_type level>
bool operator>(const ForceCenter<n, level> &FC1,
               const ForceCenter<n, level> &FC2) {
    return FC1.tree() > FC2.tree();
}

template <level_type n, level_type level>
bool operator==(const ForceCenter<n, level> &FC1,
                const ForceCenter<n, level> &FC2) {
    return FC1.tree() == FC2.tree();
}

template <level_type n, level_type level>
bool operator<=(const ForceCenter<n, level> &FC1,
                const ForceCenter<n, level> &FC2) {
    return FC1.tree() <= FC2.tree();
}

template <level_type n, level_type level>
bool operator>=(const ForceCenter<n, level> &FC1,
                const ForceCenter<n, level> &FC2) {
    return FC1.tree() >= FC2.tree();
}

template <level_type n, level_type level>
std::ostream &print_brief(std::ostream &os, const ForceCenter<n, level> &fc) {
    os << "ForceCenter<" << level << "> #" << fc.get_id();
    if (fc.body().type().get_name() != "") {
        os << " (" << fc.body().type().get_name() << ")";
    }
    return os;
}

template <level_type n, level_type level>
std::ostream &print_full(std::ostream &os, const ForceCenter<n, level> &fc) {
    print_brief(os, fc) << ": {";
    // print_brief(os, fc.tree())
    os << "}";
    return os;
}

template <level_type n, level_type level>
std::ostream &print_recursive(std::ostream &os,
                              const ForceCenter<n, level> &fc) {
    print_brief(os, fc) << ": {";
    print_recursive(os, fc.tree()) << "}";
    return os;
}

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os, const ForceCenter<n, level> &fc) {
    print_full(os, fc);
    return os;
}

} // namespace champion

#endif
