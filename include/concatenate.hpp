/******************************************************************************
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

SOURCE FILE: statistics.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2019-04-04
MODIFIED BY:

DESCRIPTION: Variadic function to concatenate compatible elements into a single
vector of chosen type.

\******************************************************************************/

#ifndef CHAMPION_CONCATENATE_HPP
#define CHAMPION_CONCATENATE_HPP

#include <iterator>
#include <vector>

#include "set_precision.hpp"
#include "type_inference.hpp"
#include "type_traits.hpp"

namespace champion {

// Primary template for implementation
template <typename T, typename... Args>
void concatenate_impl(std::vector<T> &vec, Args... args) {
    return;
}

// Forward declarations for implementations

template <typename T, typename Element, typename... Args>
std::enable_if_t<!(Is_container<Element> || Is_iterator<Element>), void>
concatenate_impl(std::vector<T> &vec, Element el, Args... args);

template <typename T, typename Iterator, typename... Args>
std::enable_if_t<Is_iterator<Iterator>, void>
concatenate_impl(std::vector<T> &vec, Iterator begin, Iterator end,
                 Args... args);

template <typename T, typename Container, typename... Args>
std::enable_if_t<Is_container<Container>, void>
concatenate_impl(std::vector<T> &vec, const Container &container, Args... args);

// Implementation definitions

template <typename T, typename Element, typename... Args>
std::enable_if_t<!(Is_container<Element> || Is_iterator<Element>), void>
concatenate_impl(std::vector<T> &vec, Element el, Args... args) {
    vec.push_back((T)el);
    concatenate_impl(vec, args...);
}

template <typename T, typename Iterator, typename... Args>
std::enable_if_t<Is_iterator<Iterator>, void>
concatenate_impl(std::vector<T> &vec, Iterator begin, Iterator end,
                 Args... args) {
    for (auto it = begin; it != end; ++it) {
        vec.push_back((T)(*it));
    }
    concatenate_impl(vec, args...);
}

template <typename T, typename Container, typename... Args>
std::enable_if_t<Is_container<Container>, void>
concatenate_impl(std::vector<T> &vec, const Container &container,
                 Args... args) {
    for (const auto &el : container) {
        vec.push_back((T)el);
    }
    concatenate_impl(vec, args...);
}

// User interface

template <typename T, typename... Args>
std::vector<T> concatenate(Args... args) {
    auto vec = std::vector<T>{};
    concatenate_impl<T>(vec, args...);
    return vec;
}

} // namespace champion

#endif
