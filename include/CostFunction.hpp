#ifndef CHAMPION_COST_FUNCTION_HPP
#define CHAMPION_COST_FUNCTION_HPP

#include <vector>

namespace champion {

template <typename T> class CostFunction {
  public:
    virtual T operator()(const std::vector<T> &) = 0;
    virtual bool empty() const = 0;
};

} // namespace champion

#endif
