#ifndef CHAMPION_RDF_HPP
#define CHAMPION_RDF_HPP

#include <vector>

#include <omp.h>

#include "Matrix.hpp"
#include "ObjectRegistry.hpp"
#include "algorithm.hpp"
#include "set_precision.hpp"
#include "statistics.hpp"

namespace champion {

template <level_type n, level_type level> class RDF {
  public:
    // Typedefs
    using trajectory_index = ObjectIndex<Trajectory, n, level>;
    using body_index = ObjectIndex<Body, n, level>;
    using bt_index = ObjectIndex<BodyType, n, level>;

    // Public member functions

    // Special member functions
    RDF() = default;
    RDF(const RDF &) = default;
    RDF &operator=(const RDF &) = default;
    RDF(RDF &&) = default;
    RDF &operator=(RDF &&) = default;
    ~RDF() = default;

    // Construct from vector of distances, cut-off and number of bins
    RDF(const std::vector<real> &distances, real cutoff = 10.0_A,
        size_type no_bins = 100)
        : limits_(no_bins), amplitudes_(no_bins, 0) {
        real bin_width = cutoff / static_cast<real>(no_bins);
        real lim = 0;
        for (auto &limit : limits_) {
            limit = (lim += bin_width);
        }
        auto counts = std::vector<size_type>(no_bins, 0);
        for (const auto &d : distances) {
            auto bin = static_cast<size_type>(d / bin_width);
            if (bin < no_bins) {
                ++counts[bin];
            }
        }
        for (size_type i = 0; i != no_bins; ++i) {
            auto r = (i + 0.5) * bin_width;
            amplitudes_[i] = static_cast<real>(counts[i]) / (r * r);
        }
        auto a_max = max(amplitudes_);
        for (auto &ai : amplitudes_) {
            ai /= a_max;
        }
    }

    // Construct from limits_ and amplitudes_
    RDF(std::vector<real> limits, std::vector<real> amplitudes)
        : limits_{limits}, amplitudes_{amplitudes} {
        if (limits_.size() != amplitudes_.size()) {
            throw std::runtime_error{
                "CHAMPION::RDF::RDF(): sizes of arguments to ctor must match."};
        }
        auto offset = (limits_[1] - limits_[0]) / 2;
        for (size_type i = 0; i < size(); ++i) {
            auto r = limits_[i] - offset;
            amplitudes_[i] = amplitudes_[i] / (4 * pi * r * r);
        }
    }

    // Construct from object registry, body types, cut_off and number of bins
    RDF(ObjectRegistry<n> &reg, bt_index bt1, bt_index bt2,
        real cutoff = 10.0_A, size_type no_bins = 100)
        : RDF(get_distances(reg, bt1, bt2), cutoff, no_bins) {}

    size_type size() const { return amplitudes_.size(); }

    const std::vector<real> &limits() const { return limits_; }
    std::vector<real> &limits() { return limits_; }
    const std::vector<real> &amplitudes() const { return amplitudes_; }
    std::vector<real> &amplitudes() { return amplitudes_; }

    real get_x_value(size_type i) const {
        if (i == 0) {
            return limits_[0] / 2;
        } else {
            return (limits_[i] + limits_[i - 1]) / 2;
        }
    }
    real get_onset() const {
        auto it = amplitudes_.begin();
        while (it != amplitudes_.end() && *it == 0) {
            ++it;
        }
        if (it == amplitudes_.end()) {
            return -1;
        } else {
            auto index = it - amplitudes_.begin();
            return get_x_value(index);
        }
    }

    RDF smooth(size_type smoothing_window =
                   std::numeric_limits<size_type>::max()) const {
        if (smoothing_window == std::numeric_limits<size_type>::max()) {
            smoothing_window = std::max<size_type>(1, this->size() * 0.05);
        }
        auto smoothed = *this;
        smoothed.amplitudes_ =
            moving_average(smoothed.amplitudes_, smoothing_window);
        return smoothed;
    }

    std::pair<real, real> get_highest_peak() const {
        auto peak = std::pair<real, real>{0, 0};
        for (size_type i = 0; i < this->size(); ++i) {
            if (amplitudes_[i] > peak.second) {
                peak.second = amplitudes_[i];
                peak.first = limits_[i];
            }
        }
        auto half_step = (limits_[1] - limits_[0]) / 2.0;
        peak.first -= half_step;
        return peak;
    }

    real get_first_max(size_type smoothing_window =
                           std::numeric_limits<size_type>::max()) const {
        if (smoothing_window == std::numeric_limits<size_type>::max()) {
            smoothing_window = std::max<size_type>(1, this->size() * 0.05);
        }
        auto rdf = *this;
        if (smoothing_window > 1) {
            rdf = rdf.smooth(smoothing_window);
        }
        for (size_type i = 1; i < rdf.size(); ++i) {
            if (rdf.amplitudes_[i - 1] < rdf.amplitudes_[i] &&
                rdf.amplitudes_[i + 1] < rdf.amplitudes_[i]) {
                return get_x_value(i);
            }
        }
        return -1;
    }

    real get_first_min(size_type smoothing_window =
                           std::numeric_limits<size_type>::max()) const {
        if (smoothing_window == std::numeric_limits<size_type>::max()) {
            smoothing_window = std::max<size_type>(1, this->size() * 0.05);
        }
        auto rdf = *this;
        if (smoothing_window > 1) {
            rdf = rdf.smooth(smoothing_window);
        }
        for (size_type i = 1; i < rdf.size(); ++i) {
            if (rdf.amplitudes_[i - 1] > rdf.amplitudes_[i] &&
                rdf.amplitudes_[i + 1] > rdf.amplitudes_[i]) {
                return get_x_value(i);
            }
        }
        return std::numeric_limits<double>::max();
    }

  private:
    // Private data members
    std::vector<real> limits_;
    std::vector<real> amplitudes_;

    // Private member functions
    std::vector<real> get_distances(const ObjectRegistry<n> &reg, bt_index bt1,
                                    bt_index bt2) const {
        auto distances = std::vector<real>{};
        const auto &trajectories =
            reg.template get_objects<Trajectory, level>();
        for (const auto &traj_el : trajectories) {
            const auto &traj = traj_el.second;
            for (size_type ti = 0; ti != traj.size(); ++ti) {
                auto conf = traj.get_snapshot(ti);
                //#pragma omp parallel for
                for (size_type i = 0; i < conf.size(); ++i) {
                    for (size_type j = i + 1; j < conf.size(); ++j) {
                        if (!((conf.get_body(i).type().id() == bt1 &&
                               conf.get_body(j).type().id() == bt2) ||
                              (conf.get_body(i).type().id() == bt2 &&
                               conf.get_body(j).type().id() == bt1))) {
                            continue;
                        }
                        distances.push_back(distance(conf.get_position(i),
                                                     conf.get_position(j)));
                    }
                }
            }
        }
        return distances;
    }
};

template <level_type n, level_type level>
SymmetricMatrix<RDF<n, level>>
compute_RDFs(const ObjectRegistry<n> &reg, real cutoff = 15.0_A,
             size_type no_bins = 150, size_type timestep_delta = 1) {
    const auto &body_types = reg.template get_objects<BodyType, level>();
    auto N = body_types.size();
    auto RDFs = SymmetricMatrix<RDF<n, level>>(N, N);
    const auto &trajectories = reg.template get_objects<Trajectory, level>();
    real bin_width = cutoff / static_cast<real>(no_bins);
    auto limits_ = std::vector<real>(no_bins);
    real lim = 0;
    for (auto &limit : limits_) {
        limit = (lim += bin_width);
    }
    auto indices = std::vector<std::pair<size_type, size_type>>{};
    for (size_type i = 0; i < N; ++i) {
        for (size_type j = i; j < N; ++j) {
            indices.push_back({i, j});
        }
    }
#pragma omp parallel for schedule(dynamic)
    for (size_type k = 0; k < indices.size(); ++k) {
        real normalization = 0.0;
        size_type i = indices[k].first;
        size_type j = indices[k].second;
        auto counts = std::vector<real>(no_bins, 0);
        for (const auto &traj_el : trajectories) {
            const auto &traj = traj_el.second;
            for (size_type ti = 0; ti < traj.size(); ti += timestep_delta) {
                const auto &conf = traj.get_snapshot(ti);
                for (size_type bi = 0; bi < conf.size(); ++bi) {
                    auto bti = conf.get_body(bi).type().get_id();
                    if (bti != i) {
                        continue;
                    }
                    ++normalization;
                    auto Pi = conf.get_position(bi);
                    for (size_type bj = bi + 1; bj < conf.size(); ++bj) {
                        auto btj = conf.get_body(bj).type().get_id();
                        if (btj != j) {
                            continue;
                        }
                        auto Pj = conf.get_position(bj);
                        auto d = distance(Pi, Pj);
                        auto bin = static_cast<size_type>(d / bin_width);
                        if (bin < no_bins) {
                            ++counts[bin];
                        }
                    }
                }
            }
        }
        for (auto &c : counts) {
            c /= normalization;
        }
        RDFs(i, j) = RDF<n, level>{limits_, counts};
    }
    return RDFs;
}

template <level_type n, level_type level>
bool operator==(const RDF<n, level> &RDF1, const RDF<n, level> &RDF2) {
    return RDF1.limits() == RDF2.limits() &&
           RDF1.amplitudes() == RDF2.amplitudes();
}

template <level_type n, level_type level>
bool operator!=(const RDF<n, level> &RDF1, const RDF<n, level> &RDF2) {
    return !(RDF1 == RDF2);
}

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os, const RDF<n, level> &RDF) {
    os << RDF.limits() << std::endl;
    os << RDF.amplitudes() << std::endl;
    return os;
}

} // namespace champion

#endif