/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017-2019 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: discover_molecular_structure.hpp

AUTHOR: Rasmus Andersson

DESCRIPTION:

Discover molecules from atomic xyz trajectories, by subjecting candidate bonds
to 3 tests:
    1. closer than (multiple of) sum of van der Waals radii throughout
        trajectory (radius_multiplier),
    2. average distance close to first peak in pairwise RDF (RDF_max_dev),
    3. more than a minimum angle separated from all shorter bonds
        (bond_angle_limit).

The settings can optionally be given in a dictionary with the
following keywords controlling behavior:
    * radius_multiplier
    * RDF_max_dev
    * bond_angle_limit

\******************************************************************************/

#ifndef CHAMPION_DISCOVER_MOLECULAR_STRUCTURE_HPP
#define CHAMPION_DISCOVER_MOLECULAR_STRUCTURE_HPP

#include <numeric>
#include <optional>
#include <vector>

#include <mpi.h>

#include "Matrix.hpp"
#include "ObjectRegistry.hpp"
#include "RDF.hpp"
#include "Vector.hpp"
#include "fft.hpp"
#include "set_precision.hpp"

namespace champion {

namespace structure_discovery {

using timestep = ::champion::size_type;
using timewindow = std::pair<timestep, timestep>;

struct bond {
    size_type i, j;
    size_type t_form;
    size_type t_break;
    real mean_distance;
};

bool operator==(const bond &b1, const bond &b2) {
    return b1.i == b2.i && b1.j == b2.j;
}

std::ostream &operator<<(std::ostream &os, const bond &bnd) {
    return os << "bond: {i: " << bnd.i << ", j: " << bnd.j
              << ", t_form: " << bnd.t_form << ", t_break: " << bnd.t_break
              << ", mean_distance: " << bnd.mean_distance << "}";
}

} // namespace structure_discovery

std::vector<structure_discovery::bond>
create_bonds(size_type i, size_type j,
             const std::vector<structure_discovery::timestep> &timesteps,
             const std::vector<real> &distances, real d_eq, real d_tol) {
    // Compute main frequency of vibration in time window
    real delta_t = 1.0; // In timestep units (absolute values don't matter here)
    auto osc_freq =
        get_main_frequency(distances.begin(), distances.end(), delta_t);
    size_type period = 1 / osc_freq;
    // Bin the distance time series in period-long bins to determine during
    // which bins the atoms are bonded
    auto no_timesteps = timesteps.size();
    auto quote = no_timesteps / period;
    auto rest = no_timesteps % period;
    auto no_bins = (rest == 0) ? quote : quote + 1;
    auto binned_distances = std::vector<real>(no_bins);
    for (size_type i = 0; i < no_bins; ++i) {
        real d_sum = 0.0;
        size_type no_terms = 0;
        for (size_type j = 0; j < period && i * period + j < no_timesteps;
             ++j) {
            d_sum += distances[i * period + j];
            ++no_terms;
        }
        binned_distances[i] = d_sum / static_cast<real>(no_terms);
    }
    auto bond_windows = std::vector<structure_discovery::timewindow>{};
    size_type window_opened = s_max;
    bool window_open = false;
    for (size_type i = 0; i < binned_distances.size(); ++i) {
        if (binned_distances[i] > d_eq * (1 - d_tol) &&
            binned_distances[i] < d_eq * (1 + d_tol)) {
            if (!window_open) {
                window_open = true;
                window_opened = i;
            }
        } else if (window_open) {
            window_open = false;
            bond_windows.push_back({window_opened * period, i * period - 1});
        }
    }
    if (window_open) {
        bond_windows.push_back({window_opened * period, distances.size() - 1});
    }
    auto bonds = std::vector<structure_discovery::bond>{};
    for (auto &bond_window : bond_windows) {
        auto local_maxima = std::vector<real>{};
        auto max_ts = std::vector<size_type>{};
        for (size_type i = bond_window.first + 1; i < bond_window.second - 1;
             ++i) {
            if (distances[i] > distances[i - 1] &&
                distances[i] >= distances[i + 1]) {
                local_maxima.push_back(distances[i]);
                max_ts.push_back(i);
            }
        }
        if (local_maxima.empty()) {
            continue;
        }
        auto highest_max = max(local_maxima);
        auto i_left = max_ts.front();
        auto i_right = max_ts.back();
        while (i_left > 0 && distances[i_left - 1] < highest_max) {
            --i_left;
        }
        bond_window.first = i_left;
        while (i_right < no_timesteps - 1 &&
               distances[i_right + 1] < highest_max) {
            ++i_right;
        }
        bond_window.second = i_right;
        auto mean_distance = mean(distances.begin() + bond_window.first,
                                  distances.begin() + bond_window.second);
        bonds.push_back({i, j, timesteps[bond_window.first],
                         timesteps[bond_window.second], mean_distance});
    }
    return bonds;
}

template <level_type n>
void discover_molecular_structure(
    ObjectRegistry<n> &reg, const SymmetricMatrix<real> &RDF_peak_distances,
    real radius_multiplier = 1, real RDF_max_dev = 0.15,
    real angle_limit = pi / 3) {
    for (const auto &traj_el : reg.template get_objects<Trajectory, 0>()) {
        auto traj_id = traj_el.first;
        const auto &trajectory = traj_el.second;
        size_type no_atoms = trajectory.no_bodies();

        auto connections =
            SparseSymmetricMatrix<std::vector<structure_discovery::bond>>{
                no_atoms, no_atoms, std::vector<structure_discovery::bond>{}};
        // Helper data structure to roll out indices in omp parallel for:
        auto indices = std::vector<std::pair<size_type, size_type>>{};
        for (size_type i = 0; i < no_atoms; ++i) {
            for (size_type j = i + 1; j < no_atoms; ++j) {
                indices.push_back({i, j});
            }
        }
        auto connections_vec =
            std::vector<std::vector<structure_discovery::bond>>(indices.size());
#pragma omp parallel for schedule(dynamic)
        for (size_type k = 0; k < indices.size(); ++k) {
            if (omp_get_thread_num() == 0) {
                std::cout << "\r" << 100 * k / indices.size() << "%"
                          << std::flush;
            }
            auto i = indices[k].first;
            auto j = indices[k].second;
            auto no_timesteps = trajectory.size();
            const auto &atom_i =
                reg.get_object(trajectory.get_snapshot(0).get_body_id(i));
            const auto &atom_j =
                reg.get_object(trajectory.get_snapshot(0).get_body_id(j));
            const auto &species_i = atom_i.type();
            const auto &species_j = atom_j.type();
            auto RDF_max =
                RDF_peak_distances(species_i.get_id(), species_j.get_id());
            auto combined_radii =
                species_i.get_radius() + species_j.get_radius();
            if (RDF_max > radius_multiplier * combined_radii) {
                continue;
            }
            auto time_steps = std::vector<structure_discovery::timestep>{};
            auto distances = std::vector<real>{};
            bool record_window = false;
            for (structure_discovery::timestep ti = 0; ti < no_timesteps;
                 ++ti) {
                const auto &snapshot = trajectory.get_snapshot(ti);
                const auto &Pi = snapshot.get_position(i);
                const auto &Pj = snapshot.get_position(j);
                auto d = distance(Pi, Pj);
                if (d < radius_multiplier * combined_radii) {
                    record_window = true;
                    time_steps.push_back(ti);
                    distances.push_back(d);
                } else if (record_window) {
                    // Close the window and narrow it to an integral number
                    // of vibrational cycles about the RDF peak
                    record_window = false;
                    auto new_bonds = create_bonds(i, j, time_steps, distances,
                                                  RDF_max, RDF_max_dev);
                    for (auto &bond : new_bonds) {
                        connections_vec[k].push_back(std::move(bond));
                    }
                    time_steps.clear();
                    distances.clear();
                }
            }
            if (record_window) {
                auto new_bonds = create_bonds(i, j, time_steps, distances,
                                              RDF_max, RDF_max_dev);
                for (auto &bond : new_bonds) {
                    connections_vec[k].push_back(std::move(bond));
                }
            }
        }
        for (size_type k = 0; k < indices.size(); ++k) {
            if (!connections_vec[k].empty()) {
                auto i = indices[k].first;
                auto j = indices[k].second;
                connections(i, j) = std::move(connections_vec[k]);
            }
        }
        std::cout << std::endl;
        // Eliminate spurious connections that are mediated through other
        // bonds (e.g. between hydrogens bonded to the same carbon) by
        // checking that the bond vector from a central atom to a neighbor
        // has at least a pi/angle_limit angle with the bond vectors of all
        // closer neighbors.
        for (size_type i = 0; i < no_atoms; ++i) {
            std::cout << "\ri = " << i << "/" << no_atoms << std::flush;
            auto bonds = std::vector<structure_discovery::bond>{};
            for (size_type j = 0; j < no_atoms; ++j) {
                const auto &j_bonds = connections(i, j);
                for (const auto &bond_k : j_bonds) {
                    bonds.push_back(bond_k);
                }
            }
            std::sort(bonds.begin(), bonds.end(),
                      [](const auto &b1, const auto &b2) {
                          return b1.mean_distance < b2.mean_distance;
                      });
            for (size_type bi = 1; bi < bonds.size(); ++bi) {
                for (size_type bj = 0; bj < bi; ++bj) {
                    // Check for time window overlap of bonds
                    if (bonds[bj].t_break >= bonds[bi].t_form &&
                        bonds[bj].t_form <= bonds[bi].t_break) {
                        size_type index_i = i;
                        size_type index_j, index_k;
                        if (bonds[bi].i == i) {
                            index_j = bonds[bi].j;
                        } else if (bonds[bi].j == i) {
                            index_j = bonds[bi].i;
                        } else {
                            throw std::runtime_error{
                                "CHAMPION::discover_structure(): Error in "
                                "indices in bond struct."};
                        }
                        if (bonds[bj].i == i) {
                            index_k = bonds[bj].j;
                        } else if (bonds[bj].j == i) {
                            index_k = bonds[bj].i;
                        } else {
                            throw std::runtime_error{
                                "CHAMPION::discover_structure(): Error in "
                                "indices in bond struct."};
                        }
                        if (index_j == index_k) {
                            continue;
                        }
                        real mean_angle = 0.0;
                        for (size_type t = bonds[bi].t_form;
                             t <= bonds[bi].t_break; ++t) {
                            const auto &snapshot = trajectory.get_snapshot(t);
                            const auto &Pi = snapshot.get_position(index_i);
                            const auto &Pj = snapshot.get_position(index_j);
                            const auto &Pk = snapshot.get_position(index_k);
                            auto vec_ij_hat = Vector<real>{Pj - Pi}.normalize();
                            auto vec_ik_hat = Vector<real>(Pk - Pi).normalize();
                            mean_angle +=
                                std::acos(dot(vec_ij_hat, vec_ik_hat));
                        }
                        mean_angle /= static_cast<real>(bonds[bi].t_break -
                                                        bonds[bi].t_form);
                        if (mean_angle < angle_limit) {
                            auto &ij_bonds = connections(index_i, index_j);
                            auto indices_to_remove = std::vector<size_type>{};
                            for (size_type i = 0; i < ij_bonds.size(); ++i) {
                                if (ij_bonds[i] == bonds[bi]) {
                                    indices_to_remove.push_back(i);
                                }
                            }
                            remove_indices(indices_to_remove, ij_bonds);
                            ;
                        }
                    }
                }
            }
        }
        std::cout << std::endl;
        for (structure_discovery::timestep ti = 0; ti < trajectory.size();) {
            std::cout << "\rti = " << ti << "/" << trajectory.size()
                      << std::flush;
            // Find all bonds alive at ti and record the next change in the
            // global bond graph
            structure_discovery::timestep next_structure_change = s_max;
            auto bonds = std::vector<Edge>{};
            auto atoms = trajectory.get_snapshot(ti).get_body_ids();
            for (size_type i = 0; i < no_atoms; ++i) {
                for (size_type j = i + 1; j < no_atoms; ++j) {
                    for (const auto &conn : connections(i, j)) {
                        if (conn.t_form <= ti && conn.t_break >= ti) {
                            bonds.push_back({i, j});
                            next_structure_change = std::min(
                                conn.t_break + 1, next_structure_change);
                        }
                    }
                }
            }
            auto graph = Graph{atoms, bonds, false};
            // Create and life-extend all discovered structures
            auto components =
                graph.connected_components(false); // Sorted in factory
            for (const auto &cmp_ptr : components) {
                const auto &cmp = *cmp_ptr;
                auto body = reg.template make_body<1>(
                    cmp, static_cast<size_type>(traj_id), ti,
                    next_structure_change);
            }
            ti = next_structure_change;
        }
    }
}

} // namespace champion

#endif