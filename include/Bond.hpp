/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: Bond.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:


\******************************************************************************/

#ifndef CHAMPION_BOND_HPP
#define CHAMPION_BOND_HPP

#include "ObjectIndex.hpp"
#include "RegistryIterator.hpp"

namespace champion {

/***************************** Class declarations *****************************/

// Forward declarations
template <level_type n> class ObjectRegistry;
template <level_type n, level_type level> class BondType;
template <level_type n, level_type level> class ForceCenter;
template <level_type n, level_type level> class Trajectory;

template <level_type n, level_type level> class Bond {
  public:
    // Public types
    using constituent_type = ForceCenter<n, level>;
    using iterator = RegistryIterator<Bond<n, level>, false>;
    using const_iterator = RegistryIterator<Bond<n, level>, true>;

    template <level_type ll>
    using trajectory_index = ObjectIndex<Trajectory, n, ll>;
    template <level_type ll>
    using bnd_index = ObjectIndex<::champion::Bond, n, ll>;
    template <level_type ll> using bt_index = ObjectIndex<BondType, n, ll>;
    template <level_type ll> using fc_index = ObjectIndex<ForceCenter, n, ll>;
    // Public member functions
    // Construct from ObjectRegistry, ID, bond type, constituents and trajectory
    // index
    Bond(ObjectRegistry<n> &reg, bnd_index<level> ID, bt_index<level> bt,
         fc_index<level> fc1, fc_index<level> fc2, size_type traj_index = 0)
        : reg_{&reg}, id_{ID}, bond_type_{bt}, constituents_{fc1, fc2},
          trajectory_{traj_index} {
        if (constituents_[1] < constituents_[0]) {
            std::swap(constituents_[0], constituents_[1]);
        }
    }

    // Access

    // Index access
    static constexpr size_type size() { return 2; }
    bnd_index<level> id() const { return id_; }
    fc_index<level> get_index(size_type i) const { return constituents_[i]; }
    bt_index<level> get_type_index() const { return bond_type_; }
    fc_index<level> first() const { return constituents_[0]; }
    fc_index<level> second() const { return constituents_[1]; }
    trajectory_index<level> get_trajectory_index() const {
        return trajectory_;
    };
    // Object access
    iterator begin() { return iterator{*this}; }
    iterator end() { return iterator{*this, size()}; }
    const_iterator begin() const { return const_iterator{*this, size()}; }
    const_iterator end() const { return const_iterator{*this, size()}; }
    const_iterator cbegin() const { return const_iterator{*this}; }
    const_iterator cend() const { return const_iterator{*this, size()}; }

    const ForceCenter<n, level> &operator[](size_type i) const {
        return reg_->template get_object<ForceCenter, level>(constituents_[i]);
    }
    ForceCenter<n, level> &operator[](size_type i) {
        return const_cast<ForceCenter<n, level> &>(
            static_cast<const Bond &>(*this)[i]);
    }

    const BondType<n, level> &type() const {
        return reg_->template get_object<BondType, level>(bond_type_);
    }

  private:
    // Private data members
    ObjectRegistry<n> *reg_;
    bnd_index<level> id_;
    bt_index<level> bond_type_;
    std::vector<fc_index<level>> constituents_;
    trajectory_index<level> trajectory_;
    // Time indices into the trajectory where the bond was born and died
    std::pair<size_type, size_type> birth_and_death_;
    // Bond distance for each time step between birth and death
    std::vector<real> bond_distance_;
};

template <level_type n, level_type level>
bool operator==(const Bond<n, level> &bnd1, const Bond<n, level> &bnd2) {
    assert(bnd1.size() == bnd2.size());
    for (size_type i = 0; i != bnd1.size(); ++i) {
        if (bnd1[i] != bnd2[i]) {
            return false;
        }
    }
    return true;
}

template <level_type n, level_type level>
bool operator!=(const Bond<n, level> &bnd1, const Bond<n, level> &bnd2) {
    return !(bnd1 == bnd2);
}

template <level_type n, level_type level>
std::ostream &print_brief(std::ostream &os, const Bond<n, level> &bnd) {
    os << "Bond<" << level << "> #" << bnd.id() << " (BondType<" << level
       << "> #" << bnd.get_type_index() << ")";
    return os;
}

template <level_type n, level_type level>
std::ostream &print_full(std::ostream &os, const Bond<n, level> &bnd) {
    print_brief(os, bnd) << ": {";
    print_brief(os, bnd[0]) << ", ";
    print_brief(os, bnd[1]) << "}";
    return os;
}

template <level_type n, level_type level>
std::ostream &print_recursive(std::ostream &os, const Bond<n, level> &bnd) {
    os << "Bond<" << level << "> #" << bnd.id() << ": {";
    print_recursive(os, bnd[0]) << ", ";
    print_recursive(os, bnd[1]) << "}";
    return os;
}

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os, const Bond<n, level> &bnd) {
    print_full(os, bnd);
    return os;
}

} // namespace champion

#endif
