/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2018 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: Trajectory.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2018-01-15
MODIFIED BY:

DESCRIPTION:

\******************************************************************************/

#ifndef CHAMPION_TRAJECTORY_HPP
#define CHAMPION_TRAJECTORY_HPP

#include <iostream>
#include <vector>

#include "Configuration.hpp"
#include "ObjectIndex.hpp"
#include "Point.hpp"
#include "Rotation.hpp"
#include "Vector.hpp"
#include "VectorSlice.hpp"
#include "set_precision.hpp"

namespace champion {

/***************************** Class declarations *****************************/

// Forward declarations
template <level_type n> class ObjectRegistry;

template <level_type n, level_type level> class Trajectory {
  public:
    // Public types
    template <level_type ll>
    using traj_index = ObjectIndex<::champion::Trajectory, n, ll>;
    template <level_type ll>
    using config_index = ObjectIndex<Configuration, n, ll>;
    template <level_type ll> using body_index = ObjectIndex<Body, n, ll>;

    // Public member functions
    // Default constructor
    Trajectory()
        : reg_{nullptr}, id_{0}, store_velocities_{false},
          store_angular_velocities_{false}, store_forces_{false},
          store_torques_{false}, times_{}, snapshots_{},
          velocities_{}, forces_{} {}
    // Construct empty Trajectory from object registry and id
    Trajectory(ObjectRegistry<n> &reg, size_type ID)
        : reg_{&reg}, id_{ID}, store_velocities_{false},
          store_angular_velocities_{false}, store_forces_{false},
          store_torques_{false}, times_{}, snapshots_{},
          velocities_{}, forces_{} {}
    // Construct from ObjectRegistry, ID, vector of time steps, vector of
    // Configuration indices and optionally velocities and forces
    Trajectory(ObjectRegistry<n> &reg, size_type ID,
               std::vector<real> time_steps,
               std::vector<config_index<level>> configurations,
               std::vector<std::vector<Vector<real>>> velocities = {},
               std::vector<std::vector<Vector<real>>> angular_velocities = {},
               std::vector<std::vector<Vector<real>>> forces = {},
               std::vector<std::vector<Vector<real>>> torques = {})
        : reg_{&reg}, id_{ID}, store_velocities_{false}, store_forces_{false},
          times_(time_steps), snapshots_(configurations),
          velocities_{velocities}, angular_velocities_{angular_velocities},
          forces_{forces}, torques_{torques} {
        if (velocities.size() != 0) {
            store_velocities_ = true;
        }
        if (angular_velocities.size() != 0) {
            store_velocities_ = true;
        }
        if (forces.size() != 0) {
            store_forces_ = true;
        }
        if (torques.size() != 0) {
            store_torques_ = true;
        }
        if (store_velocities_ && velocities_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of velocities"};
        }
        if (store_angular_velocities_ &&
            angular_velocities_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of angular_velocities"};
        }
        if (store_forces_ && forces_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of forces"};
        }
        if (store_torques_ && torques_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of torques"};
        }
    }
    // Construct from ObjectRegistry, ID, constant time step, vector of
    // Configuration indices and optionally velocities and forces
    Trajectory(ObjectRegistry<n> &reg, size_type ID, real time_step,
               std::vector<config_index<level>> configurations,
               std::vector<std::vector<Vector<real>>> velocities = {},
               std::vector<std::vector<Vector<real>>> angular_velocities = {},
               std::vector<std::vector<Vector<real>>> forces = {},
               std::vector<std::vector<Vector<real>>> torques = {})
        : reg_{&reg}, id_{ID}, store_velocities_{false},
          store_angular_velocities_{false}, store_forces_{false},
          store_torques_{false}, times_{},
          snapshots_(configurations), velocities_{velocities},
          angular_velocities_{velocities}, forces_{forces}, torques_{torques} {
        if (velocities.size() != 0) {
            store_velocities_ = true;
        }
        if (angular_velocities.size() != 0) {
            store_angular_velocities_ = true;
        }
        if (forces.size() != 0) {
            store_forces_ = true;
        }
        if (torques.size() != 0) {
            store_torques_ = true;
        }
        if (store_velocities_ && velocities_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of velocities"};
        }
        if (store_angular_velocities_ &&
            angular_velocities_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of angular_velocities"};
        }
        if (store_forces_ && forces_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of forces"};
        }
        if (store_torques_ && torques_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of torques"};
        }
        real time = 0;
        for (size_type i = 0; i != configurations.size(); ++i) {
            times_.push_back(time);
            time += time_step;
        }
    }
    // Construct from ObjectRegistry, ID, single starting Configuration and
    // optionally velocities and forces
    Trajectory(ObjectRegistry<n> &reg, size_type ID,
               config_index<level> configuration,
               std::vector<std::vector<Vector<real>>> velocities = {},
               std::vector<std::vector<Vector<real>>> angular_velocities = {},
               std::vector<std::vector<Vector<real>>> forces = {},
               std::vector<std::vector<Vector<real>>> torques = {})
        : reg_{&reg}, id_{ID}, store_velocities_{false},
          store_angular_velocities_{false}, store_forces_{false},
          store_torques_{false}, times_{},
          snapshots_(configuration), velocities_{velocities},
          angular_velocities_{angular_velocities}, torques_{torques} {
        if (velocities.size() != 0) {
            store_velocities_ = true;
        }
        if (angular_velocities.size() != 0) {
            store_angular_velocities_ = true;
        }
        if (forces.size() != 0) {
            store_forces_ = true;
        }
        if (torques.size() != 0) {
            store_torques_ = true;
        }
        if (store_velocities_ && velocities_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of velocities"};
        }
        if (store_angular_velocities_ &&
            angular_velocities_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of angular_velocities"};
        }
        if (store_forces_ && forces_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of forces"};
        }
        if (store_torques_ && torques_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of torques"};
        }
    }
    // Construct from lower level Trajectory
    Trajectory(Trajectory<n, level - 1> &traj)
        : reg_{traj.reg_}, id_{traj.id_}, store_orientations_{false},
          store_internal_coordinates_{false}, store_velocities_{false},
          store_angular_velocities_{false}, store_forces_{false},
          store_torques_{false}, times_{traj.times_}, snapshots_{},
          velocities_{}, angular_velocities_{}, forces_{}, torques_{} {
        // Get all the Body<n, level> indices belonging to this trajectory from
        // the registry
        const auto &all_bodies = reg_->template get_objects<Body, level>();
        auto bodies = std::vector<ObjectIndex<Body, n, level>>{};
        for (const auto &el : all_bodies) {
            auto id = el.first;
            const auto &body = el.second;
            if (body.get_trajectory_id() == this->get_id()) {
                bodies.push_back(id);
            }
        }
        for (size_type ti = 0; ti < this->size(); ++ti) {
            auto current_bodies = std::vector<ObjectIndex<Body, n, level>>{};
            for (auto body_id : bodies) {
                const auto &body = reg_->get_object(body_id);
                if (body.get_birth_time() <= ti &&
                    body.get_death_time() >= ti) {
                    current_bodies.push_back(body_id);
                }
            }
            auto config =
                reg_->make_configuration(traj.get_snapshot(ti), current_bodies);
            snapshots_.push_back(config);
        }
    }

    // Access functions
    traj_index<level> get_id() const { return id_; }
    size_type size() const { return times_.size(); }
    size_type no_bodies(size_type ti = 0) const {
        return get_snapshot(ti).size();
    }
    bool stores_velocities() const { return store_velocities_; }
    bool stores_forces() const { return store_forces_; }
    bool stores_angular_velocities() const { return store_angular_velocities_; }
    bool stores_torques() const { return store_torques_; }
    // Get bounds of the periodic box
    std::vector<std::pair<real, real>> get_boundary(size_type i) const {
        return get_snapshot(i).get_boundary();
    }
    // Get a reference to a Configuration
    const Configuration<n, level> &get_snapshot(size_type i) const {
        return reg_->template get_object<Configuration, level>(snapshots_[i]);
    }
    Configuration<n, level> &get_snapshot(size_type i) {
        return reg_->template get_object<Configuration, level>(snapshots_[i]);
    }
    // Get a reference to the positions of a snapshot Configuration
    std::vector<Point<real>> get_positions(size_type i) const {
        return get_snapshot(i).get_positions();
    }
    // Get a reference to the orientations of a snapshot Configuration
    std::vector<Rotation<real>> get_orientations(size_type i) const {
        return get_snapshot(i).get_orientations();
    }
    // Get the velocities at timestep i
    std::vector<Vector<real>> get_velocities(size_type i) const {
        if (!store_velocities_) {
            throw std::runtime_error{
                "CHAMPION::Trajectory::get_velocities(): The trajectory does "
                "not store velocities."};
        }
        return velocities_[i];
    }
    // Get the angular velocities at timestep i
    std::vector<Vector<real>> get_angular_velocities(size_type i) const {
        return angular_velocities_[i];
    }
    // Get a reference to the forces at timestep i
    std::vector<Vector<real>> get_forces(size_type i) const {
        return forces_[i];
    }
    // Get a reference to the torques at timestep i
    std::vector<Vector<real>> get_torques(size_type i) const {
        return torques_[i];
    }

    // Get the time value at time i
    real get_time(size_type i) const {
        if (i == s_max) {
            return times_.back();
        } else {
            return times_[i];
        }
    }

    // Modification functions
    size_type set_timestep(size_type step, real time,
                           ObjectIndex<Configuration, n, level> snapshot,
                           std::vector<Vector<real>> velocities = {},
                           std::vector<Vector<real>> angular_velocities = {},
                           std::vector<Vector<real>> forces = {},
                           std::vector<Vector<real>> torques = {}) {
        std::string err_str = "Trajectory::set_timestep: ";
        // Check that the timestep exists or is the next to be added
        if (step > this->size()) {
            err_str += "step must refer to an existing time step or the next "
                       "to be added";
        }
        // Check that all vectors have the right length
        bool is_error = false;
        if (reg_->get_object(snapshot).size() != no_bodies(step)) {
            is_error = true;
            err_str += "positions.size() does not match number of atoms in "
                       "trajectory\n";
        }
        if (store_velocities_ && velocities.size() != no_bodies(step)) {
            is_error = true;
            err_str += "velocities.size() does not match number of atoms in "
                       "trajectory\n";
        } else if (!store_velocities_ && velocities.size() != 0) {
            if (step == 0 && times_.size() <= 1) {
                store_velocities_ = true;
            } else {
                is_error = true;
                err_str += "Trajectory does not store velocities but non-empty "
                           "velocity vector was given\n";
            }
        }
        if (store_angular_velocities_ &&
            angular_velocities.size() != no_bodies(step)) {
            is_error = true;
            err_str +=
                "angular_velocities.size() does not match number of atoms in "
                "trajectory\n";
        } else if (!store_angular_velocities_ &&
                   angular_velocities.size() != 0) {
            if (step == 0 && times_.size() <= 1) {
                store_angular_velocities_ = true;
            } else {
                is_error = true;
                err_str += "Trajectory does not store angular velocities but "
                           "non-empty "
                           "angular velocity vector was given\n";
            }
        }
        if (store_forces_ && forces.size() != no_bodies(step)) {
            is_error = true;
            err_str +=
                "forces.size() does not match number of atoms in trajectory\n";
        } else if (!store_forces_ && forces.size() != 0) {
            if (step == 0 && times_.size() <= 1) {
                store_forces_ = true;
            } else {
                is_error = true;
                err_str += "Trajectory does not store forces but non-empty "
                           "force vector was given\n";
            }
        }
        if (store_torques_ && torques.size() != no_bodies(step)) {
            is_error = true;
            err_str +=
                "torques.size() does not match number of atoms in trajectory\n";
        } else if (!store_torques_ && torques.size() != 0) {
            if (step == 0 && times_.size() <= 1) {
                store_torques_ = true;
            } else {
                is_error = true;
                err_str += "Trajectory does not store torques but non-empty "
                           "torque vector was given\n";
            }
        }

        if (is_error) {
            throw std::runtime_error{err_str};
        }

        // Add timestep to the end if step is the next timestep
        if (step == this->size()) {
            times_.push_back(time);
            snapshots_.push_back(snapshot);
            velocities_.insert(velocities_.end(), velocities.begin(),
                               velocities.end());
            angular_velocities_.insert(angular_velocities_.end(),
                                       angular_velocities.begin(),
                                       angular_velocities.end());
            forces_.insert(forces_.end(), forces.begin(), forces.end());
            torques_.insert(torques_.end(), torques.begin(), torques.end());
        } else {
            // Else update the existing data for step
            // Update time
            times_[step] = time;
            // Update Configuration
            reg_->template get_object(snapshots_[step])
                .set_positions(reg_->get_object(snapshot).get_positions());
            reg_->template get_object(snapshots_[step])
                .set_orientations(
                    reg_->get_object(snapshot).get_orientations());
            // Overwrite velocities
            auto write_it = velocities_.begin() + step * no_bodies(step);
            for (auto vel_it = velocities.begin(); vel_it != velocities.end();
                 ++vel_it) {
                *write_it = *vel_it;
                ++write_it;
            }
            // Overwrite angular velocities
            write_it = angular_velocities_.begin() + step * no_bodies(step);
            for (auto vel_it = angular_velocities.begin();
                 vel_it != angular_velocities.end(); ++vel_it) {
                *write_it = *vel_it;
                ++write_it;
            }
            // Overwrite forces
            write_it = forces_.begin() + step * no_bodies(step);
            for (auto F_it = forces.begin(); F_it != forces.end(); ++F_it) {
                *write_it = *F_it;
                ++write_it;
            }
            // Overwrite torques
            write_it = torques_.begin() + step * no_bodies(step);
            for (auto F_it = torques.begin(); F_it != torques.end(); ++F_it) {
                *write_it = *F_it;
                ++write_it;
            }
        }
        return step;
    }

    size_type set_timestep(size_type step, real time,
                           std::vector<Point<real>> positions,
                           std::vector<Rotation<real>> orientations,
                           std::vector<Vector<real>> velocities = {},
                           std::vector<Vector<real>> angular_velocities = {},
                           std::vector<Vector<real>> forces = {},
                           std::vector<Vector<real>> torques = {}) {
        auto bodies = reg_->template get_object<Configuration, level>(
                              snapshots_[snapshots_.size() - 1])
                          .get_body_indices();
        auto conf = reg_->template make_configuration<level>(bodies, positions,
                                                             orientations);
        return set_timestep(step, time, conf, velocities, angular_velocities,
                            forces, torques);
    }

    // Add next time step
    size_type add_timestep(real time,
                           ObjectIndex<Configuration, n, level> snapshot,
                           std::vector<Vector<real>> velocities = {},
                           std::vector<Vector<real>> angular_velocities = {},
                           std::vector<Vector<real>> forces = {},
                           std::vector<Vector<real>> torques = {}) {
        auto timestep_index = this->size();
        set_timestep(timestep_index, time, snapshot, velocities,
                     angular_velocities, forces);
        return timestep_index;
    }

    size_type add_timestep(real time, std::vector<Point<real>> positions,
                           std::vector<Rotation<real>> orientations,
                           std::vector<Vector<real>> velocities = {},
                           std::vector<Vector<real>> angular_velocities = {},
                           std::vector<Vector<real>> forces = {},
                           std::vector<Vector<real>> torques = {}) {
        auto timestep_index = this->size();
        set_timestep(timestep_index, time, positions, orientations, velocities,
                     angular_velocities, forces, torques);
        return timestep_index;
    }

    // Compute velocities by finite differences of consecutive snapshots.
    void compute_velocities() {
        store_velocities_ = true;
        velocities_.resize(this->size());
        // Use central differencing for inner timesteps
        for (size_type ti = 1; ti != this->size() - 1; ++ti) {
            auto snapshot_0 = this->get_snapshot(ti - 1);
            auto snapshot_1 = this->get_snapshot(ti);
            auto snapshot_2 = this->get_snapshot(ti + 1);
            velocities_[ti] = std::vector<Vector<real>>(snapshot_1.size());
            for (size_type i = 0; i != snapshot_1.size(); ++i) {
                auto body_ID = snapshot_1.get_body_id(i);
                size_type s0_index, s2_index;
                try {
                    s0_index = snapshot_0.get_index_by_id(body_ID);
                } catch (const std::exception &) {
                    s0_index = s_max;
                }
                try {
                    s2_index = snapshot_2.get_index_by_id(body_ID);
                } catch (const std::exception &) {
                    s2_index = s_max;
                }
                if (s0_index != s_max && s2_index != s_max) {
                    auto P0 = snapshot_0.get_position(s0_index);
                    auto P1 = snapshot_1.get_position(i);
                    auto P2 = snapshot_2.get_position(s2_index);
                    auto displacement = Point<real>{};
                    // This will fail if box dimensions have changed between
                    // timesteps. If so, harmonize bounds before computing
                    // displacement.
                    try {
                        displacement = P2 - P0;
                    } catch (std::exception &) {
                        for (size_type xi = 0; xi != 3; ++xi) {
                            P0[xi].set_bounds(P1[xi].bounds());
                            P2[xi].set_bounds(P1[xi].bounds());
                        }
                        displacement = P2 - P0;
                    }
                    real delta_t = times_[ti + 1] - times_[ti - 1];
                    velocities_[ti][i] = Vector{displacement} / delta_t;
                } else if (s0_index == s_max && s2_index != s_max) {
                    auto P1 = snapshot_1.get_position(i);
                    auto P2 = snapshot_2.get_position(s2_index);
                    auto displacement = Point<real>{};
                    // This will fail if box dimensions have changed between
                    // timesteps. If so, harmonize bounds before computing
                    // displacement.
                    try {
                        displacement = P2 - P1;
                    } catch (std::exception &) {
                        for (size_type xi = 0; xi != 3; ++xi) {
                            P2[xi].set_bounds(P1[xi].bounds());
                        }
                        displacement = P2 - P1;
                    }
                    real delta_t = times_[ti + 1] - times_[ti];
                    velocities_[ti][i] = Vector{displacement} / delta_t;
                } else if (s0_index != s_max && s2_index == s_max) {
                    auto P0 = snapshot_0.get_position(s0_index);
                    auto P1 = snapshot_1.get_position(i);
                    auto displacement = Point<real>{};
                    // This will fail if box dimensions have changed between
                    // timesteps. If so, harmonize bounds before computing
                    // displacement.
                    try {
                        displacement = P1 - P0;
                    } catch (std::exception &) {
                        for (size_type xi = 0; xi != 3; ++xi) {
                            P0[xi].set_bounds(P1[xi].bounds());
                        }
                        displacement = P1 - P0;
                    }
                    real delta_t = times_[ti] - times_[ti - 1];
                    velocities_[ti][i] = Vector{displacement} / delta_t;
                } else {
                    velocities_[ti][i] = {0, 0, 0};
                }
            }
        }
        // Compute velocities for first and last timesteps using one-sided
        // differencing
        {
            auto snapshot_0 = this->get_snapshot(0);
            auto snapshot_1 = this->get_snapshot(1);
            velocities_[0] = std::vector<Vector<real>>(snapshot_0.size());
            for (size_type i = 0; i != snapshot_0.size(); ++i) {
                auto body_ID = snapshot_0.get_body_id(i);
                size_type s1_index;
                try {
                    s1_index = snapshot_1.get_index_by_id(body_ID);
                } catch (const std::exception &) {
                    s1_index = s_max;
                }
                if (s1_index == s_max) {
                    velocities_[0][i] = {0, 0, 0};
                } else {
                    auto P0 = snapshot_0.get_position(i);
                    auto P1 = snapshot_1.get_position(s1_index);
                    auto displacement = Point<real>{};
                    try {
                        displacement = P1 - P0;
                    } catch (std::exception &) {
                        for (size_type xi = 0; xi != 3; ++xi) {
                            P1[xi].set_bounds(P0[xi].bounds());
                        }
                        displacement = P1 - P0;
                    }
                    velocities_[0][i] =
                        Vector{displacement / (times_[1] - times_[0])};
                }
            }
        }
        {
            auto snapshot_0 = this->get_snapshot(this->size() - 2);
            auto snapshot_1 = this->get_snapshot(this->size() - 1);
            velocities_[this->size() - 1] =
                std::vector<Vector<real>>(snapshot_1.size());
            for (size_type i = 0; i != snapshot_1.size(); ++i) {
                auto body_ID = snapshot_1.get_body_id(i);
                size_type s0_index;
                try {
                    s0_index = snapshot_0.get_index_by_id(body_ID);
                } catch (const std::exception &) {
                    s0_index = s_max;
                }
                if (s0_index == s_max) {
                    velocities_.back()[i] = {0, 0, 0};
                } else {
                    auto P0 = snapshot_0.get_position(s0_index);
                    auto P1 = snapshot_1.get_position(i);
                    auto displacement = Point<real>{};
                    try {
                        displacement = P1 - P0;
                    } catch (std::exception &) {
                        for (size_type xi = 0; xi != 3; ++xi) {
                            P0[xi].set_bounds(P1[xi].bounds());
                        }
                        displacement = P1 - P0;
                    }
                    velocities_.back()[i] =
                        Vector{displacement / (times_[this->size() - 1] -
                                               times_[this->size() - 2])};
                }
            }
        }
    }

    // io functions
    void print_timestep(size_type time_index, std::ostream &os) {
        os << no_bodies(time_index) << std::endl;
        const auto &configuration =
            reg_->template get_object<Configuration, level>(
                snapshots_[time_index]);
        const auto &positions = configuration.get_positions();
        const auto &body_indices = configuration.get_body_indices();
        auto offset = time_index * no_bodies(time_index);
        VectorSlice<Vector<real>> velocities, forces;
        if (store_velocities_) {
            velocities = VectorSlice<Vector<real>>{velocities_, offset,
                                                   no_bodies(time_index)};
        }
        if (store_forces_) {
            forces_ = VectorSlice<Vector<real>>{forces_, offset,
                                                no_bodies(time_index)};
        }
        os << "Timestep " << time_index << ", t = " << times_[time_index]
           << ", Periodic box: x = (" << positions[0].x.low() << ", "
           << positions[0].x.high() << "), y = (" << positions[0].y.low()
           << ", " << positions[0].y.high() << "), z = ("
           << positions[0].z.low() << ", " << positions[0].z.high() << ")"
           << std::endl;
        os << "x y z ";
        if (store_velocities_) {
            os << "v_x v_y v_z";
        }
        if (store_forces_) {
            os << "F_x F_y F_z";
        }
        os << std::endl;
        for (size_type i = 0; i != no_bodies(time_index); ++i) {
            os << reg_->get_object(body_indices[i]).type().id() << " "
               << positions[i].x.value() << " " << positions[i].y.value() << " "
               << positions[i].z.value() << " ";
            if (store_velocities_) {
                os << velocities[i].x << " " << velocities[i].y << " "
                   << velocities[i].z << " ";
            }
            if (store_forces_) {
                os << forces[i].x << " " << forces[i].y << " " << forces[i].z;
            }
            os << std::endl;
        }
    }

    void print_xyz(size_type time_index, std::ostream &os) {
        ::champion::print_xyz(get_snapshot(time_index), os);
    }

    void print_xyz(std::ostream &os) {
        for (size_type i = 0; i != this->size(); ++i) {
            this->print_xyz(get_snapshot(i), os);
        }
    }

  private:
    // Friends
    friend class Trajectory<n, level + 1>;
    // Private data members
    ObjectRegistry<n> *reg_;
    traj_index<level> id_;
    bool store_orientations_, store_internal_coordinates_;
    bool store_velocities_, store_angular_velocities_;
    bool store_forces_, store_torques_;
    std::vector<real> times_;
    std::vector<config_index<level>> snapshots_;
    std::vector<std::vector<Vector<real>>> velocities_;
    std::vector<std::vector<Vector<real>>> angular_velocities_;
    std::vector<std::vector<Vector<real>>> forces_;
    std::vector<std::vector<Vector<real>>> torques_;
};

// Specialization for level = 0
template <level_type n> class Trajectory<n, 0> {
  public:
    // Public types
    template <level_type ll>
    using traj_index = ObjectIndex<::champion::Trajectory, n, ll>;
    template <level_type ll>
    using config_index = ObjectIndex<Configuration, n, ll>;
    template <level_type ll> using body_index = ObjectIndex<Body, n, ll>;

    // Public member functions
    // Default constructor
    Trajectory()
        : reg_{nullptr}, id_{0}, store_velocities_{false}, store_forces_{false},
          times_{}, snapshots_{}, velocities_{}, forces_{} {}
    // Construct empty Trajectory from object registry and id
    Trajectory(ObjectRegistry<n> &reg, size_type ID) : reg_{&reg}, id_{ID} {}
    // Construct from ObjectRegistry, ID, vector of time steps, vector of
    // Configuration indices and optionally velocities and forces
    Trajectory(ObjectRegistry<n> &reg, size_type ID,
               std::vector<real> time_steps,
               std::vector<config_index<0>> configurations,
               std::vector<std::vector<Vector<real>>> velocities = {},
               std::vector<std::vector<Vector<real>>> forces = {})
        : reg_{&reg}, id_{ID}, store_velocities_{false}, store_forces_{false},
          times_(time_steps),
          snapshots_(configurations), velocities_{velocities}, forces_{forces} {
        if (velocities.size() != 0) {
            store_velocities_ = true;
        }
        if (forces.size() != 0) {
            store_forces_ = true;
        }
        if (store_velocities_ && velocities_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of velocities"};
        }
        if (store_forces_ && forces_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of forces"};
        }
    }
    // Construct from ObjectRegistry, ID, constant time step, vector of
    // Configuration indices and optionally velocities and forces
    Trajectory(ObjectRegistry<n> &reg, size_type ID, real time_step,
               std::vector<config_index<0>> configurations,
               std::vector<std::vector<Vector<real>>> velocities = {},
               std::vector<std::vector<Vector<real>>> forces = {})
        : reg_{&reg}, id_{ID}, store_velocities_{false},
          store_forces_{false}, times_{},
          snapshots_(configurations), velocities_{velocities}, forces_{forces} {
        if (velocities.size() != 0) {
            store_velocities_ = true;
        }
        if (forces.size() != 0) {
            store_forces_ = true;
        }
        if (store_velocities_ && velocities_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of velocities"};
        }
        if (store_forces_ && forces_.size() != snapshots_.size()) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of forces"};
        }
        real time = 0;
        for (size_type i = 0; i != configurations.size(); ++i) {
            times_.push_back(time);
            time += time_step;
        }
    }
    // Construct from ObjectRegistry, ID, single starting Configuration and
    // optionally velocities and forces
    Trajectory(ObjectRegistry<n> &reg, size_type ID,
               config_index<0> configuration,
               std::vector<std::vector<Vector<real>>> velocities = {},
               std::vector<std::vector<Vector<real>>> forces = {})
        : reg_{&reg}, id_{ID}, store_velocities_{false}, store_forces_{false},
          times_{}, snapshots_{std::vector<config_index<0>>{configuration}},
          velocities_{velocities}, forces_{forces} {
        if (velocities.size() != 0) {
            store_velocities_ = true;
        }
        if (forces.size() != 0) {
            store_forces_ = true;
        }
        if (store_velocities_ && velocities_.size() != 1) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of velocities"};
        }
        if (store_forces_ && forces_.size() != 1) {
            throw std::runtime_error{
                "Trajectory constructor: Wrong number of forces"};
        }
    }
    // Access functions
    traj_index<0> get_id() const { return id_; }
    size_type size() const { return times_.size(); }
    size_type no_bodies(size_type ti = 0) const {
        return get_snapshot(ti).size();
    }
    bool stores_velocities() const { return store_velocities_; }
    bool stores_forces() const { return store_forces_; }
    // Get bounds of the periodic box
    std::vector<std::pair<real, real>> get_boundary(size_type i) const {
        return get_snapshot(i).get_boundary();
    }
    // Get a reference to a Configuration
    const Configuration<n, 0> &get_snapshot(size_type i) const {
        return reg_->template get_object<Configuration, 0>(snapshots_[i]);
    }
    Configuration<n, 0> &get_snapshot(size_type i) {
        return reg_->template get_object<Configuration, 0>(snapshots_[i]);
    }
    // Get a reference to the positions of a snapshot Configuration
    std::vector<Point<real>> get_positions(size_type i) const {
        return get_snapshot(i).get_positions();
    }
    // Get the velocities at timestep i
    std::vector<Vector<real>> get_velocities(size_type i) const {
        return velocities_[i];
    }
    // Get a reference to the forces at timestep i
    std::vector<Vector<real>> get_forces(size_type i) const {
        return forces_[i];
    }
    // Get the time value at time i
    real get_time(size_type i) const {
        if (i == s_max) {
            return times_.back();
        } else {
            return times_[i];
        }
    }

    // Modification functions
    size_type set_timestep(size_type step, real time,
                           ObjectIndex<Configuration, n, 0> snapshot,
                           std::vector<Vector<real>> velocities = {},
                           std::vector<Vector<real>> forces = {}) {
        std::string err_str = "Trajectory::set_timestep: ";
        // Check that the timestep exists or is the next to be added
        if (step > this->size()) {
            err_str += "step must refer to an existing time step or the next "
                       "to be added";
        }
        // Check that all vectors have the right length
        bool is_error = false;
        if (reg_->get_object(snapshot).size() != no_bodies(step)) {
            is_error = true;
            err_str += "positions.size() does not match number of atoms in "
                       "trajectory\n";
        }
        if (store_velocities_ && velocities.size() != no_bodies(step)) {
            is_error = true;
            err_str += "velocities.size() does not match number of atoms in "
                       "trajectory\n";
        } else if (!store_velocities_ && velocities.size() != 0) {
            if (step == 0 && times_.size() <= 1) {
                store_velocities_ = true;
            } else {
                is_error = true;
                err_str += "Trajectory does not store velocities but non-empty "
                           "velocity vector was given\n";
            }
        }
        if (store_forces_ && forces.size() != no_bodies(step)) {
            is_error = true;
            err_str +=
                "forces.size() does not match number of atoms in trajectory\n";
        } else if (!store_forces_ && forces.size() != 0) {
            if (step == 0 && times_.size() <= 1) {
                store_forces_ = true;
            } else {
                is_error = true;
                err_str += "Trajectory does not store forces but non-empty "
                           "velocity vector was given\n";
            }
        }
        if (is_error) {
            throw std::runtime_error{err_str};
        }

        // Add timestep to the end if step is the next timestep
        if (step == this->size()) {
            times_.push_back(time);
            snapshots_.push_back(snapshot);
            velocities_.push_back(velocities);
            forces_.push_back(forces);
        } else {
            // Else update the existing data for step
            // Update time
            times_[step] = time;
            // Update Configuration
            reg_->template get_object(snapshots_[step])
                .set_positions(reg_->get_object(snapshot).get_positions());
            // Overwrite velocities
            auto write_it = velocities_[step].begin() + step * no_bodies(step);
            for (auto vel_it = velocities.begin(); vel_it != velocities.end();
                 ++vel_it) {
                *write_it = *vel_it;
                ++write_it;
            }
            // Overwrite forces
            write_it = forces_[step].begin() + step * no_bodies(step);
            for (auto F_it = forces.begin(); F_it != forces.end(); ++F_it) {
                *write_it = *F_it;
                ++write_it;
            }
        }
        return step;
    }

    size_type set_timestep(size_type step, real time,
                           std::vector<Point<real>> positions,
                           std::vector<Vector<real>> velocities = {},
                           std::vector<Vector<real>> forces = {}) {
        auto bodies = reg_->template get_object<Configuration, 0>(
                              snapshots_[snapshots_.size() - 1])
                          .get_body_indices();
        auto conf = reg_->template make_configuration<0>(bodies, positions);
        return set_timestep(step, time, conf, velocities, forces);
    }

    // Add next time step
    size_type add_timestep(real time, ObjectIndex<Configuration, n, 0> snapshot,
                           std::vector<Vector<real>> velocities = {},
                           std::vector<Vector<real>> forces = {}) {
        auto timestep_index = this->size();
        set_timestep(timestep_index, time, snapshot, velocities, forces);
        return timestep_index;
    }

    size_type add_timestep(real time, std::vector<Point<real>> positions,
                           std::vector<Vector<real>> velocities = {},
                           std::vector<Vector<real>> forces = {}) {
        auto timestep_index = this->size();
        set_timestep(timestep_index, time, positions, velocities, forces);
        return timestep_index;
    }

    // Compute velocities by finite differences of consecutive snapshots.
    // Assumes unchanging population of bodies.
    void compute_velocities() {
        store_velocities_ = true;
        velocities_.resize(this->size());
        // Use central differencing for inner timesteps
        for (size_type ti = 1; ti != this->size() - 1; ++ti) {
            auto snapshot_0 = this->get_positions(ti - 1);
            auto snapshot_1 = this->get_positions(ti);
            auto snapshot_2 = this->get_positions(ti + 1);
            velocities_[ti] = std::vector<Vector<real>>(snapshot_1.size());
            for (size_type i = 0; i != snapshot_1.size(); ++i) {
                auto P0 = snapshot_0[i];
                auto P1 = snapshot_1[i];
                auto P2 = snapshot_2[i];
                auto displacement = Point<real>{};
                // This will fail if box dimensions have changed between
                // timesteps. If so, harmonize bounds before computing
                // displacement.
                try {
                    displacement = P2 - P0;
                } catch (std::runtime_error &) {
                    for (size_type xi = 0; xi != 3; ++xi) {
                        P0[xi].set_bounds(P1[xi].bounds());
                        P2[xi].set_bounds(P1[xi].bounds());
                    }
                    displacement = P2 - P0;
                }
                real delta_t = times_[ti + 1] - times_[ti - 1];
                velocities_[ti][i] = Vector{displacement} / delta_t;
            }
        }
        // Compute velocities for first and last timesteps using one-sided
        // differencing
        {
            auto snapshot_0 = this->get_positions(0);
            auto snapshot_1 = this->get_positions(1);
            velocities_[0] = std::vector<Vector<real>>(snapshot_0.size());
            for (size_type i = 0; i != snapshot_0.size(); ++i) {
                auto P0 = snapshot_0[i];
                auto P1 = snapshot_1[i];
                auto displacement = Point<real>{};
                try {
                    displacement = P1 - P0;
                } catch (std::runtime_error &) {
                    for (size_type xi = 0; xi != 3; ++xi) {
                        P1[xi].set_bounds(P0[xi].bounds());
                    }
                    displacement = P1 - P0;
                }
                velocities_[0][i] =
                    Vector{displacement / (times_[1] - times_[0])};
            }
        }
        {
            auto snapshot_0 = this->get_positions(this->size() - 2);
            auto snapshot_1 = this->get_positions(this->size() - 1);
            velocities_[this->size() - 1] =
                std::vector<Vector<real>>(snapshot_0.size());
            for (size_type i = 0; i != snapshot_0.size(); ++i) {
                auto P0 = snapshot_0[i];
                auto P1 = snapshot_1[i];
                auto displacement = Point<real>{};
                try {
                    displacement = P1 - P0;
                } catch (std::runtime_error &) {
                    for (size_type xi = 0; xi != 3; ++xi) {
                        P1[xi].set_bounds(P0[xi].bounds());
                    }
                    displacement = P1 - P0;
                }
                velocities_[this->size() - 1][i] =
                    Vector{displacement / (times_[this->size() - 1] -
                                           times_[this->size() - 2])};
            }
        }
    }

    // io functions
    void print_timestep(size_type time_index, std::ostream &os) {
        os << no_bodies(time_index) << std::endl;
        const auto &configuration =
            reg_->template get_object<Configuration, 0>(snapshots_[time_index]);
        const auto &positions = configuration.get_positions();
        const auto &body_indices = configuration.get_body_indices();
        auto offset = time_index * no_bodies(time_index);
        VectorSlice<Vector<real>> velocities, forces;
        if (store_velocities_) {
            velocities = VectorSlice<Vector<real>>{velocities_, offset,
                                                   no_bodies(time_index)};
        }
        if (store_forces_) {
            forces = VectorSlice<Vector<real>>{forces_, offset,
                                               no_bodies(time_index)};
        }
        os << "Timestep " << time_index << ", t = " << times_[time_index]
           << ", Periodic box: x = (" << positions[0].x.low() << ", "
           << positions[0].x.high() << "), y = (" << positions[0].y.low()
           << ", " << positions[0].y.high() << "), z = ("
           << positions[0].z.low() << ", " << positions[0].z.high() << ")"
           << std::endl;
        os << "x y z ";
        if (store_velocities_) {
            os << "v_x v_y v_z";
        }
        if (store_forces_) {
            os << "F_x F_y F_z";
        }
        os << std::endl;
        for (size_type i = 0; i != no_bodies(time_index); ++i) {
            os << reg_->get_object(body_indices[i]).type().id() << " "
               << positions[i].x.value() << " " << positions[i].y.value() << " "
               << positions[i].z.value() << " ";
            if (store_velocities_) {
                os << velocities[i].x << " " << velocities[i].y << " "
                   << velocities[i].z << " ";
            }
            if (store_forces_) {
                os << forces[i].x << " " << forces[i].y << " " << forces[i].z;
            }
            os << std::endl;
        }
    }

    void print_xyz(size_type time_index, std::ostream &os) {
        ::champion::print_xyz(get_snapshot(time_index), os);
    }

    void print_xyz(std::ostream &os) {
        for (size_type i = 0; i != this->size(); ++i) {
            this->print_xyz(i, os);
        }
    }

  private:
    // Friends
    friend class Trajectory<n, 1>;
    // Private data members
    ObjectRegistry<n> *reg_;
    traj_index<0> id_;
    bool store_velocities_, store_forces_;
    std::vector<real> times_;
    std::vector<config_index<0>> snapshots_;
    std::vector<std::vector<Vector<real>>> velocities_;
    std::vector<std::vector<Vector<real>>> forces_;
};

} // namespace champion

#endif
