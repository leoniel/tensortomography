#ifndef CHAMPION_FFT_HPP
#define CHAMPION_FFT_HPP

#include <cmath>
#include <complex>
#include <vector>

#include "fftw3.h"

#include "Curve.hpp"
#include "set_precision.hpp"

namespace champion {

struct Peak {
    real x0;
    real y0;
    real FWHM;
};

std::ostream &operator<<(std::ostream &os, const Peak &peak) {
    os << "{x0 = " << peak.x0 << ", y0 = " << peak.y0
       << ", FWHM = " << peak.FWHM << "}";
    return os;
}

template <typename It>
Curve<real, real> power_spectrum(It begin, It end, real delta_x = 1.0) {
    double *in;
    fftw_complex *out;
    int N = end - begin;
    int M = N / 2 + 1;
    fftw_plan p;
#pragma omp critical
    {
        in = static_cast<double *>(fftw_malloc(N * sizeof(double)));
        auto it = begin;
        for (size_type i = 0; i < N; ++i) {
            in[i] = *it++;
        }
        out =
            static_cast<fftw_complex *>(fftw_malloc(M * sizeof(fftw_complex)));
        p = fftw_plan_dft_r2c_1d(N, in, out, FFTW_ESTIMATE);
    }
    fftw_execute(p);
    auto Y = std::vector<real>(M, 0);
    auto X = Y;
    for (size_type i = 0; i < M; ++i) {
        Y[i] = std::sqrt(out[i][0] * out[i][0] + out[i][1] * out[i][1]);
    }
    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);
    for (size_type i = 0; i < M; ++i) {
        X[i] = static_cast<real>(i) / (N * delta_x);
    }
    return Curve{X, Y};
}

template <typename T>
std::vector<std::vector<std::complex<double>>>
real_to_resiproc_fft(const T &in_data) {
    fftw_complex *in, *out;
    fftw_plan p;
    int n0 = in_data.size();
    int n1 = 3;
    in = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * n0 * n1);
    out = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * n0 * n1);
    int i, j = 0;
    for (const auto &val : in_data) {
        for (const auto &vall : val) {
            in[i * n0 + j][0] = vall;
            in[i * n0 + j][1] = 0;
            ++j;
        }
        ++i;
    }
    p = fftw_plan_dft_2d(n0, n1, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

    fftw_execute(p);
    std::vector<std::vector<std::complex<double>>> return_data;
    for (size_type i = 0; i != n0; ++i) {
        std::vector<std::complex<double>> temp;
        for (size_type j = 0; j != n1; ++j) {
            std::complex<double> complex;
            complex.real(out[i * n0 + j][0]);
            complex.imag(out[i * n0 + j][1]);
            temp.push_back(complex);
        }
        return_data.push_back(temp);
    }
    fftw_destroy_plan(p);
    fftw_free(in);
    fftw_free(out);

    return return_data;
}

// Frequency of the main peak in the signal whose time series is provided
// between iterators begin and end with constant time step delta_t
template <typename It> real get_main_frequency(It begin, It end, real delta_t) {
    auto spectrum = power_spectrum(begin, end, delta_t);
    if (spectrum.size() > 0) {
        auto max_it =
            std::max_element(spectrum.y().begin() + 1, spectrum.y().end());
        auto index = max_it - spectrum.y().begin();
        return spectrum.x(index);
    }
    return 0;
}

// A peak is defined as greater than the data points both to the left and to
// the right and greater than any data points between points of half-maximum
// to the left and to the right.
std::vector<Peak> get_peaks(const Curve<real, real> &spectrum) {
    auto peaks = std::vector<Peak>{};
    for (size_type i = 1; i < spectrum.size() - 1; ++i) {
        if (spectrum.y(i) > spectrum.y(i - 1) &&
            spectrum.y(i) > spectrum.y(i + 1)) {
            Peak peak;
            peak.x0 = spectrum.x(i);
            peak.y0 = spectrum.y(i);
            auto half_max = peak.y0 / 2;
            auto x_l = peak.x0;
            size_type j = i;
            for (; j >= 0; --j) {
                if (spectrum.y(j) < half_max) {
                    x_l = spectrum.x(j);
                    break;
                }
            }
            if (j == 0) {
                break;
            }
            auto x_r = peak.x0;
            for (j = i; j < spectrum.size(); ++j) {
                if (spectrum.y(j) < half_max) {
                    x_r = spectrum.x(j);
                    break;
                }
            }
            if (j == spectrum.size() - 1) {
                break;
            }
            peak.FWHM = x_r - x_l;
            peaks.push_back(std::move(peak));
        }
    }
    return peaks;
}

// Ratio of largest to second largest peak heights of a spectrum
real peak_dominance(const std::vector<Peak> &peaks) {
    if (peaks.size() == 0) {
        return 0;
    } else if (peaks.size() == 1) {
        return std::numeric_limits<real>::infinity();
    }
    real first = 0, second = 0;
    for (const auto &peak : peaks) {
        auto peak_area = peak.y0 * peak.FWHM;
        if (peak_area > first) {
            second = first;
            first = peak_area;
        } else if (peak_area > second) {
            second = peak_area;
        }
    }
    return first / second;
}

} // namespace champion

#endif