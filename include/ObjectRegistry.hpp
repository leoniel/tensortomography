/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: ObjectRegistry.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2018-01-17
MODIFIED BY:

DESCRIPTION:

Registry object keeping account of all objects in a session. Is the owner
of all objects (Body:s, BodyType:s, Trajectory:s, Configuration:s etc.) and can
return raw pointers to them. Includes member functions to create body types,
bodies and force centers and autodiscover the bonds, bond angles, proper and
improper dihedrals in the registry and their types.

Future plans:

Ability to offload storage of object data to database.
If ObjectRegistry is queried on something which is not currently in memory it
will first query the database to recreate the object and then return a pointer
to the newly re-instantiated object.

\******************************************************************************/

#ifndef CHAMPION_OBJECT_REGISTRY
#define CHAMPION_OBJECT_REGISTRY

#include <cassert>
#include <limits>
#include <memory>
#include <stdexcept>
#include <string>
#include <tuple>
#include <type_traits>
#include <typeinfo>
#include <utility>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <sqlite3.h>

#include "Body.hpp"
#include "BodyType.hpp"
#include "Bond.hpp"
#include "BondAngle.hpp"
#include "BondAngleType.hpp"
#include "BondType.hpp"
#include "Configuration.hpp"
#include "HierarchicalContainer.hpp"
#include "HierarchicalMap.hpp"
#include "HierarchicalTuple.hpp"
#include "ImproperTorsion.hpp"
#include "ImproperTorsionType.hpp"
#include "Incrementer.hpp"
#include "ObjectIndex.hpp"
#include "Point.hpp"
#include "Torsion.hpp"
#include "TorsionType.hpp"
#include "Trajectory.hpp"
#include "Vector.hpp"
#include "VectorSlice.hpp"
#include "algorithm.hpp"
#include "set_precision.hpp"

namespace champion {

// Forward declarations
template <typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &c);

template <level_type n> class ObjectRegistry {
  public:
    template <level_type level>
    using bt_index = ObjectIndex<BodyType, n, level>;
    template <level_type level> using body_index = ObjectIndex<Body, n, level>;
    template <level_type level>
    using fct_index = ObjectIndex<ForceCenterType, n, level>;
    template <level_type level>
    using fc_index = ObjectIndex<ForceCenter, n, level>;
    template <level_type level>
    using bond_type_index = ObjectIndex<BondType, n, level>;
    template <level_type level> using bond_index = ObjectIndex<Bond, n, level>;
    template <level_type level>
    using bat_index = ObjectIndex<BondAngleType, n, level>;
    template <level_type level>
    using ba_index = ObjectIndex<BondAngle, n, level>;
    template <level_type level>
    using torsion_index = ObjectIndex<Torsion, n, level>;
    template <level_type level>
    using it_index = ObjectIndex<ImproperTorsion, n, level>;

    // Default constructor
    ObjectRegistry(std::string db_path_string = "")

        : ID_{0}, objects_{}, trajectory_counter_{}, configuration_counter_{},
          body_type_counter_(n), force_center_type_counter_(n - 1),
          body_counter_(n), force_center_counter_(n - 1),
          bond_type_counter_(n - 1), bond_angle_type_counter_(n - 1),
          torsion_type_counter_(n - 1), improper_torsion_type_counter_(n - 1),
          bond_counter_(n - 1), bond_angle_counter_(n - 1),
          torsion_counter_(n - 1),
          improper_torsion_counter_(n - 1), db_{nullptr} {
        if (db_path_string != "") {
            auto db_path = boost::filesystem::path{db_path_string};
            auto err = sqlite3_open(db_path.c_str(), &db_);
            if (err) {
                throw std::runtime_error{"CHAMPION::ObjectRegistry: Error "
                                         "opening database file."};
            }
            initialize_db();
        }
    }

    ~ObjectRegistry() {
        if (db_) {
            sqlite3_close(db_);
        }
    }

    // Access

    // Get map of all elements of the specified type
    // Return const reference to map of T<n, level>s
    template <template <level_type, level_type> class T, level_type level>
    const std::map<size_type, T<n, level>> &get_objects() const {
        auto &h_map = std::get<HierarchicalMap<n, T>>(objects_);
        auto &object_map = std::get<level>(h_map);
        return object_map;
    }
    // Return mutable reference to map of T<n, level>s
    template <template <level_type, level_type> class T, level_type level>
    std::map<size_type, T<n, level>> &get_objects() {
        return const_cast<std::map<size_type, T<n, level>> &>(
            static_cast<const ObjectRegistry &>(*this).get_objects<T, level>());
    }
    // Get the number of a certain type of object
    template <template <level_type, level_type> class T, level_type level>
    size_type no_objects() const {
        return get_objects<T, level>().size();
    }

    // Get a single object based on its ID
    // Return const reference to object with ID index in map of T<n, level>
    template <template <level_type, level_type> class T, level_type level>
    const T<n, level> &get_object(size_type index) const {
        const T<n, level> *obj_ptr{};
        try {
            obj_ptr = &get_objects<T, level>().at(index);
        } catch (std::out_of_range &e) {
            throw std::runtime_error{
                std::string("ObjectRegistry.hpp:get_object<") +
                typeid(T<n, level>).name() + ">: No element with index " +
                std::to_string(index) + e.what()};
        }
        return *obj_ptr;
    }
    template <template <level_type, level_type> class T, level_type level>
    const T<n, level> &get_object(ObjectIndex<T, n, level> index) const {
        return get_objects<T, level>().at(index);
    }
    // Return mutable reference to object with ID index in map of T<n,
    // level>
    template <template <level_type, level_type> class T, level_type level>
    T<n, level> &get_object(size_type index) {
        return const_cast<T<n, level> &>(
            static_cast<const ObjectRegistry<n> &>(*this)
                .template get_object<T, level>(index));
    }
    template <template <level_type, level_type> class T, level_type level>
    T<n, level> &get_object(ObjectIndex<T, n, level> index) {
        return get_objects<T, level>().at(index);
    }
    // Get single object based on its name
    // Return const reference to object with name name in map of T<n, level>
    template <template <level_type, level_type> class T, level_type level>
    const T<n, level> &get_object(std::string name) const {
        auto map = get_objects<T, level>();
        // Return the first instance that matches the name
        for (auto &entry : map) {
            // Get the value from the key-value pair
            auto &el = entry.second;
            if (el.name() == name) {
                return el;
            }
        }
        throw std::runtime_error(
            "ObjectRegistry::get_object(std::string): No entry with name " +
            name);
    }
    // Return mutable reference to object with name name in map of T<n,
    // level>
    template <template <level_type, level_type> class T, level_type level>
    T<n, level> &get_object(std::string name) {
        return const_cast<T<n, level> &>(
            static_cast<const ObjectRegistry<n> &>(*this)
                .template get_object<T, level>(name));
    }

    // Modification

    // Construct a BodyType<n, level> consisting of a single
    // BodyType<n, level - 1>. Avoids having to provide connectivity graph
    // and geometry for point particle.
    template <level_type level>
    std::enable_if_t<(level >= 1), ObjectIndex<BodyType, n, level>>
        make_body_type(ObjectIndex<BodyType, n, level - 1> species_ID,
                       std::string name);

    // Create BodyType<n, level> by forwarding arguments to suitable
    // constructor
    template <level_type level, typename... Ts>
    ObjectIndex<BodyType, n, level> make_body_type(Ts... args);

    // Create a Body<n, level> after creating the requisite lower level
    // bodies
    template <level_type level>
    std::enable_if_t<(level >= 1), ObjectIndex<Body, n, level>>
    make_body(ObjectIndex<BodyType, n, level> body_type_index,
              size_type trajectory_index = 0, size_type birth_time = 0,
              size_type death_time = s_max);

    // Create a Body<n, level> from graph of constituent Body<n, level - 1>s
    template <level_type level>
    std::enable_if_t<(level >= 1), ObjectIndex<Body, n, level>>
        make_body(Graph<ObjectIndex<Body, n, level - 1>> graph,
                  size_type trajectory_index = 0, size_type birth_time = 0,
                  size_type death_time = s_max);

    // Create a Body<n, level> by forwarding to suitable constructor
    template <level_type level, typename... Ts>
    ObjectIndex<Body, n, level> make_body(Ts... args);

    template <level_type level, typename... Ts>
    ObjectIndex<ForceCenterType, n, level> make_force_center_type(Ts... args);

    // Create a ForceCenter<n, level> by forwarding arguments to appropriate
    // constructor
    template <level_type level, typename... Ts>
    ObjectIndex<ForceCenter, n, level> make_force_center(Ts... args);

    // Create equivalent configuration from higher level configuration
    template <level_type target_level, level_type source_level>
    std::enable_if_t<(target_level <= source_level),
                     ObjectIndex<Configuration, n, target_level>>
    make_configuration(
        ObjectIndex<Configuration, n, source_level> source_index);

    template <level_type level>
    ObjectIndex<Configuration, n, level>
        make_configuration(ObjectIndex<Configuration, n, level - 1> Ci);

    // Create a randomized configuration and add to registry
    template <level_type level>
    ObjectIndex<Configuration, n, level> make_randomized_configuration(
        const Stoichiometry<n, level> &S, real density, unsigned max_atoms,
        size_type relax = 5, bool perturb_structures = true,
        std::ostream &os = std::cout, bool animate = false);

    // Create a configuration by forwarding arguments to appropriate
    // constructor
    template <level_type level, typename... Ts>
    ObjectIndex<Configuration, n, level> make_configuration(Ts... args);

    template <level_type level, level_type ll, typename... Ts>
    std::enable_if_t<(ll != level), ObjectIndex<Configuration, n, level>>
    make_configuration(ObjectIndex<Configuration, n, ll> id, Ts... args);

    template <level_type level>
    ObjectIndex<Configuration, n, level> make_configuration(
        Configuration<n, level - 1> &config,
        const std::vector<ObjectIndex<Body, n, level>> &bodies);

    // Create a trajectory
    template <level_type level>
    ObjectIndex<Trajectory, n, level>
        make_trajectory(Trajectory<n, level - 1> &traj);

    template <level_type level, typename... Ts>
    ObjectIndex<Trajectory, n, level> make_trajectory(Ts... args);

    ObjectIndex<Trajectory, n, 0> add_CPMD_trajectories(
        Stoichiometry<n, 1> stoichiometry, size_type no_trajectories,
        std::vector<unsigned> units_per_trajectory,
        std::vector<unsigned> trajectory_lengths,
        std::vector<double> box_lengths, std::vector<double> timestep_lengths,
        std::vector<double> coordinates, std::vector<double> velocities,
        std::vector<double> forces);

    void find_force_center_types(size_type max_depth = s_max);
    void find_interaction_types();
    void find_bond_types();
    void find_bond_angle_types();
    void find_torsion_types();
    void find_improper_torsion_types();

    void find_force_centers(size_type max_depth = s_max);
    void find_interactions();
    void find_bonds();
    void find_bond_angles();
    void find_torsions();
    void find_improper_torsions();

    // Database API
    void initialize_db();
    void save_registry() const;

  private:
    // Private member functions
    template <level_type level>
    std::vector<ObjectIndex<ForceCenterType, n, level>>
        make_force_center_types(ObjectIndex<BodyType, n, level + 1> bt,
                                size_type max_depth);
    template <level_type level>
    std::vector<ObjectIndex<ForceCenter, n, level>>
        make_force_centers(ObjectIndex<Body, n, level + 1> bt,
                           size_type max_depth);

    template <level_type level>
    std::enable_if_t<(level < n - 1), void>
    find_force_center_types_impl(size_type max_depth = s_max);
    template <level_type level>
    std::enable_if_t<(level >= n - 1), void>
    find_force_center_types_impl(size_type max_depth = s_max);
    template <level_type level>
    std::enable_if_t<(level < n - 1), void>
    find_force_centers_impl(size_type max_depth = s_max);
    template <level_type level>
    std::enable_if_t<(level >= n - 1), void>
    find_force_centers_impl(size_type max_depth = s_max);

    template <level_type level>
    std::enable_if_t<(level < n - 1), void> find_bond_types_impl();
    template <level_type level>
    std::enable_if_t<(level >= n - 1), void> find_bond_types_impl();
    template <level_type level>
    std::enable_if_t<(level < n - 1), void> find_bond_angle_types_impl();
    template <level_type level>
    std::enable_if_t<(level >= n - 1), void> find_bond_angle_types_impl();
    template <level_type level>
    std::enable_if_t<(level < n - 1), void> find_torsion_types_impl();
    template <level_type level>
    std::enable_if_t<(level >= n - 1), void> find_torsion_types_impl();
    template <level_type level>
    std::enable_if_t<(level < n - 1), void> find_improper_torsion_types_impl();
    template <level_type level>
    std::enable_if_t<(level >= n - 1), void> find_improper_torsion_types_impl();
    template <level_type level>
    std::enable_if_t<(level < n - 1), void> find_bonds_impl();
    template <level_type level>
    std::enable_if_t<(level >= n - 1), void> find_bonds_impl();
    template <level_type level>
    std::enable_if_t<(level < n - 1), void> find_bond_angles_impl();
    template <level_type level>
    std::enable_if_t<(level >= n - 1), void> find_bond_angles_impl();
    template <level_type level>
    std::enable_if_t<(level < n - 1), void> find_torsions_impl();
    template <level_type level>
    std::enable_if_t<(level >= n - 1), void> find_torsions_impl();
    template <level_type level>
    std::enable_if_t<(level < n - 1), void> find_improper_torsions_impl();
    template <level_type level>
    std::enable_if_t<(level >= n - 1), void> find_improper_torsions_impl();

    // Private data members
    level_type ID_;
    std::tuple<HierarchicalMap<n, Trajectory>,
               HierarchicalMap<n, Configuration>, HierarchicalMap<n, BodyType>,
               HierarchicalMap<n, Body>, HierarchicalMap<n, ForceCenterType>,
               HierarchicalMap<n, ForceCenter>, HierarchicalMap<n, BondType>,
               HierarchicalMap<n, BondAngleType>,
               HierarchicalMap<n, TorsionType>,
               HierarchicalMap<n, ImproperTorsionType>,
               HierarchicalMap<n, Bond>, HierarchicalMap<n, BondAngle>,
               HierarchicalMap<n, Torsion>, HierarchicalMap<n, ImproperTorsion>>
        objects_;
    Incrementer trajectory_counter_;
    Incrementer configuration_counter_;
    std::vector<Incrementer> body_type_counter_;
    std::vector<Incrementer> force_center_type_counter_;
    std::vector<Incrementer> body_counter_;
    std::vector<Incrementer> force_center_counter_;
    std::vector<Incrementer> bond_type_counter_;
    std::vector<Incrementer> bond_angle_type_counter_;
    std::vector<Incrementer> torsion_type_counter_;
    std::vector<Incrementer> improper_torsion_type_counter_;
    std::vector<Incrementer> bond_counter_;
    std::vector<Incrementer> bond_angle_counter_;
    std::vector<Incrementer> torsion_counter_;
    std::vector<Incrementer> improper_torsion_counter_;
    sqlite3 *db_;
};

// Construct a BodyType<n, level> consisting of a single BodyType<n, level -
// 1> Avoids having to provide connectivity graph and geometry for point
// particle
template <level_type n>
template <level_type level>
std::enable_if_t<(level >= 1), ObjectIndex<BodyType, n, level>>
ObjectRegistry<n>::make_body_type(
    ObjectIndex<BodyType, n, level - 1> species_ID, std::string name) {
    return make_body_type<level>(Graph{{species_ID}, {}}, {{0, 0, 0}}, name);
}

template <level_type n>
template <level_type level, typename... Args>
ObjectIndex<BodyType, n, level>
ObjectRegistry<n>::make_body_type(Args... args) {
    size_type bt_index = body_type_counter_[level]++;
    get_objects<BodyType, level>().emplace(
        bt_index, BodyType<n, level>(*this, bt_index, args...));
    return ObjectIndex<BodyType, n, level>(bt_index);
}

// Create a body of a specified type after creating constituent lower level
// bodies
template <level_type n>
template <level_type level>
std::enable_if_t<(level >= 1), ObjectIndex<Body, n, level>>
ObjectRegistry<n>::make_body(ObjectIndex<BodyType, n, level> body_type_index,
                             size_type trajectory_index, size_type birth_time,
                             size_type death_time) {
    const auto &bt = get_object(body_type_index);
    auto body_ids = std::vector<ObjectIndex<Body, n, level - 1>>{};
    for (const auto &ll_bt : bt) {
        auto bt_id = ll_bt.get_id();
        auto body_id =
            make_body(bt_id, trajectory_index, birth_time, death_time);
        body_ids.push_back(body_id);
    } // namespace champion
    auto edges = bt.graph().get_edges();
    auto graph = Graph{body_ids, edges};
    get_objects<Body, n, level>().emplace(
        body_counter_[level],
        Body<n, level>(*this, body_counter_[level], body_type_index,
                       trajectory_index, birth_time, death_time));
    return ObjectIndex<Body, n, level>(body_counter_[level]++);
} // namespace champion

// Create Body<n, level> based on a graph of lower level bodies
template <level_type n>
template <level_type level>
std::enable_if_t<(level >= 1), ObjectIndex<Body, n, level>>
ObjectRegistry<n>::make_body(Graph<ObjectIndex<Body, n, level - 1>> graph,
                             size_type trajectory, size_type birth_time,
                             size_type death_time) {
    // Sort the graph
    graph.sort();
    // Check if this is just a life extension of an existing body
    auto &existing_bodies = get_objects<Body, level>();
    for (auto &body_el : existing_bodies) {
        auto &body = body_el.second;
        if (graph == body.graph()) {
            if (body.get_birth_time() > birth_time) {
                body.set_birth_time(birth_time);
            }
            if (body.get_death_time() < death_time) {
                body.set_death_time(death_time);
            }
            return body.get_id();
        }
    }
    // Add bonds to Body<n, level - 1>s
    auto bonds = graph.find_chains(2);
    size_type i = 0;
    for (const auto &bond : bonds) {
        auto &body_1 = get_object(graph[bond[0]]);
        auto &body_2 = get_object(graph[bond[1]]);
        body_1.set_neighbor(body_2.get_id(), birth_time, death_time);
        body_2.set_neighbor(body_1.get_id(), birth_time, death_time);
    }
    auto N = graph.size();
    auto body_types =
        std::vector<ObjectIndex<BodyType, n, level - 1>>(N, {s_max});
    for (size_type i = 0; i < N; ++i) {
        body_types[i] = get_object(graph[i]).get_type_id();
    }
    auto bt_graph = Graph{body_types, graph.get_edges()};
    auto bt = ObjectIndex<BodyType, n, level>{s_max};
    for (const auto &bt_el : get_objects<BodyType, level>()) {
        if (bt_el.second.graph() == bt_graph) {
            bt = bt_el.first;
            break;
        }
    }
    if (bt == s_max) {
        // Create new BodyType<n, level>
        bt = make_body_type<level>(bt_graph);
    }
    // Create and return new Body<n, level>
    return make_body<level>(bt, graph, trajectory, birth_time, death_time);
}

// Create a Body<n, level> by forwarding to suitable constructor
template <level_type n>
template <level_type level, typename... Ts>
ObjectIndex<Body, n, level> ObjectRegistry<n>::make_body(Ts... args) {
    get_objects<Body, level>().emplace(
        body_counter_[level],
        Body<n, level>(*this, body_counter_[level], args...));
    return ObjectIndex<Body, n, level>(body_counter_[level]++);
}

template <level_type n>
template <level_type level>
std::vector<ObjectIndex<ForceCenterType, n, level>>
ObjectRegistry<n>::make_force_center_types(
    ObjectIndex<BodyType, n, level + 1> bt, size_type max_depth) {
    // Create a vector of ForceCenterType indices
    const auto &bt_graph = get_object(bt).graph();
    auto vertices = std::vector<ObjectIndex<ForceCenterType, n, level>>(
        bt_graph.size(), s_max);
    for (size_type i = 0; i != bt_graph.size(); ++i) {
        // Create a new graph tree and compare with the trees of existing
        // force center types in the object registry. If a match is found,
        // assign the matching force center type to the present vertex.
        auto tree =
            GraphTree<ObjectIndex<BodyType, n, level>>(bt_graph, i, max_depth);
        for (const auto &el : get_objects<ForceCenterType, level>()) {
            if (tree == el.second.tree()) {
                vertices[i] = {el.first};
                break;
            }
        }
        // If no match among existing force center types, create a new one
        if (vertices[i] == s_max) {
            vertices[i] = make_force_center_type<level>(tree);
        }
    }
    return vertices;
}

// Create force centers for each constituent in a particular body.
// Also create ForceCenterType:s if needed.
template <level_type n>
template <level_type level>
std::vector<ObjectIndex<ForceCenter, n, level>>
ObjectRegistry<n>::make_force_centers(ObjectIndex<Body, n, level + 1> body_ID,
                                      size_type max_depth) {
    // Create a vector of ForceCenter indices
    const auto &body = get_object(body_ID);
    const auto &body_graph = body.graph();
    auto bt_vertices =
        std::vector<ObjectIndex<BodyType, n, level>>(body.size(), s_max);
    for (size_type i = 0; i < body.size(); ++i) {
        bt_vertices[i] = body[i].get_type_id();
    }
    auto fcs =
        std::vector<ObjectIndex<ForceCenter, n, level>>(body.size(), s_max);
    auto bt_graph = Graph{bt_vertices, body_graph.get_edges(), false};
    for (size_type i = 0; i < bt_graph.size(); ++i) {
        auto bt_tree = GraphTree{bt_graph, i, max_depth};
        auto body_tree = GraphTree{body_graph, i, max_depth};
        auto fct = ObjectIndex<ForceCenterType, n, level>{s_max};
        for (const auto &fct_el : get_objects<ForceCenterType, level>()) {
            const auto &fct_j = fct_el.second;
            auto fct_j_id = fct_el.first;
            if (bt_tree == fct_j.tree()) {
                fct = fct_j_id;
                break;
            }
        }
        if (fct == s_max) {
            fct = make_force_center_type<level>(bt_tree);
        }
        fcs[i] = make_force_center<level>(fct, body_tree);
    }
    return fcs;
}

template <level_type n>
template <level_type level, typename... Args>
ObjectIndex<ForceCenterType, n, level>
ObjectRegistry<n>::make_force_center_type(Args... args) {
    size_type fct_index = force_center_type_counter_[level]++;
    get_objects<ForceCenterType, level>().emplace(
        fct_index, ForceCenterType<n, level>(*this, fct_index, args...));
    return ObjectIndex<ForceCenterType, n, level>(fct_index);
}

template <level_type n>
template <level_type level, typename... Ts>
ObjectIndex<ForceCenter, n, level>
ObjectRegistry<n>::make_force_center(Ts... args) {
    auto index =
        ObjectIndex<ForceCenter, n, level>(force_center_counter_[level]++);
    get_objects<ForceCenter, level>().emplace(
        index, ForceCenter<n, level>{*this, index, args...});
    return index;
}

// Create equivalent configuration from higher level configuration
template <level_type n>
template <level_type target_level, level_type source_level>
std::enable_if_t<(target_level <= source_level),
                 ObjectIndex<Configuration, n, target_level>>
ObjectRegistry<n>::make_configuration(
    ObjectIndex<Configuration, n, source_level> source_index) {
    get_objects<Configuration, target_level>().emplace(
        source_index,
        get_object(source_index).template get_representation<target_level>());
    return ObjectIndex<Configuration, n, target_level>{source_index};
}

// Create higher level configuration by structure discovery from lower level
template <level_type n>
template <level_type level>
ObjectIndex<Configuration, n, level> ObjectRegistry<n>::make_configuration(
    ObjectIndex<Configuration, n, level - 1> Ci) {
    auto index = ObjectIndex<Configuration, n, level>(Ci);
    get_objects<Configuration, level>().emplace(
        index, Configuration<n, level>{get_object(Ci)});
    return index;
}

// Create a new configuration and add to registry
template <level_type n>
template <level_type level, typename... Ts>
ObjectIndex<Configuration, n, level>
ObjectRegistry<n>::make_configuration(Ts... args) {
    auto index = ObjectIndex<Configuration, n, level>(configuration_counter_++);
    get_objects<Configuration, level>().emplace(
        index, Configuration<n, level>(*this, index, args...));
    return index;
}

template <level_type n>
template <level_type level, level_type ll, typename... Ts>
std::enable_if_t<(ll != level), ObjectIndex<Configuration, n, level>>
ObjectRegistry<n>::make_configuration(ObjectIndex<Configuration, n, ll> id,
                                      Ts... args) {
    get_objects<Configuration, level>().emplace(
        id, Configuration<n, level>(*this, id, args...));
    return ObjectIndex<Configuration, n, level>(id);
}

template <level_type n>
template <level_type level>
ObjectIndex<Configuration, n, level>
ObjectRegistry<n>::make_randomized_configuration(
    const Stoichiometry<n, level> &S, real density, unsigned max_atoms,
    size_type relax, bool perturb_structures, std::ostream &os, bool animate) {
    get_objects<Configuration, level>().emplace(
        configuration_counter_,
        randomize_configuration(
            *this, ObjectIndex<Configuration, n, level>(configuration_counter_),
            S, density, max_atoms, relax, perturb_structures, os, animate));
    return ObjectIndex<Configuration, n, level>(configuration_counter_++);
}

template <level_type n>
template <level_type level>
ObjectIndex<Configuration, n, level> ObjectRegistry<n>::make_configuration(
    Configuration<n, level - 1> &config,
    const std::vector<ObjectIndex<Body, n, level>> &bodies) {
    get_objects<Configuration, level>().emplace(
        config.get_id(), Configuration<n, level>(config, bodies));
    return ObjectIndex<Configuration, n, level>(config.get_id());
}
// Create a new trajectory and add to registry
template <level_type n>
template <level_type level>
ObjectIndex<Trajectory, n, level>
ObjectRegistry<n>::make_trajectory(Trajectory<n, level - 1> &traj) {
    get_objects<Trajectory, level>().emplace(traj.get_id(),
                                             Trajectory<n, level>(traj));
    return ObjectIndex<Trajectory, n, level>(traj.get_id());
}

// Create a new trajectory and add to registry
template <level_type n>
template <level_type level, typename... Ts>
ObjectIndex<Trajectory, n, level>
ObjectRegistry<n>::make_trajectory(Ts... args) {
    get_objects<Trajectory, level>().emplace(
        trajectory_counter_,
        Trajectory<n, level>(*this, trajectory_counter_, args...));
    return ObjectIndex<Trajectory, n, level>(trajectory_counter_++);
}

template <level_type n>
void ObjectRegistry<n>::find_force_center_types(size_type s_max) {
    find_force_center_types_impl<0>(s_max);
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level < n - 1), void>
ObjectRegistry<n>::find_force_center_types_impl(size_type max_depth) {
    for (const auto &el : get_objects<BodyType, level + 1>()) {
        const auto &bt_id = el.first;
        make_force_center_types<level>(bt_id, max_depth);
    }
    find_force_center_types_impl<level + 1>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level >= n - 1), void>
ObjectRegistry<n>::find_force_center_types_impl(size_type max_depth) {
    return;
}

template <level_type n>
void ObjectRegistry<n>::find_force_centers(size_type max_depth) {
    find_force_center_types(max_depth);
    find_force_centers_impl<0>(max_depth);
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level < n - 1), void>
ObjectRegistry<n>::find_force_centers_impl(size_type max_depth) {
    for (const auto &el : get_objects<Body, level + 1>()) {
        const auto &body_id = el.first;
        make_force_centers<level>(body_id, max_depth);
    }
    find_force_centers_impl<level + 1>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level >= n - 1), void>
ObjectRegistry<n>::find_force_centers_impl(size_type max_depth) {
    return;
}

template <level_type n> void ObjectRegistry<n>::find_interaction_types() {
    find_bond_types();
    find_bond_angle_types();
    find_torsion_types();
    find_improper_torsion_types();
}

template <level_type n> void ObjectRegistry<n>::find_interactions() {
    find_bonds();
    find_bond_angles();
    find_torsions();
    find_improper_torsions();
}

template <level_type n> void ObjectRegistry<n>::find_bond_types() {
    find_bond_types_impl<0>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level < n - 1), void>
ObjectRegistry<n>::find_bond_types_impl() {
    for (auto bt : get_objects<BodyType, level + 1>()) {
        auto graph = bt.second.graph();
        auto fcts = graph.get_vertices();
        auto chains = graph.find_chains(2);
        for (auto c : chains) {
            // Create new bond type
            auto new_bondtype = BondType<n, level>(
                *this,
                ObjectIndex<BondType, n, level>(bond_type_counter_[level]),
                ObjectIndex<ForceCenterType, n, level>(
                    bt.second[c[0]].get_id()),
                ObjectIndex<ForceCenterType, n, level>(
                    bt.second[c[1]].get_id()));
            // Loop through existing bond types to see if the current one
            // already exists in object registry
            bool exists = false;
            for (auto el : get_objects<BondType, level>()) {
                if (el.second == new_bondtype) {
                    exists = true;
                    break;
                }
            }
            // If no match was found, create a new bond type and add to
            // the registry
            if (!exists) {
                get_objects<BondType, level>().emplace(
                    bond_type_counter_[level]++, std::move(new_bondtype));
            }
        }
    }
    find_bond_types_impl<level + 1>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level >= n - 1), void>
ObjectRegistry<n>::find_bond_types_impl() {
    return;
}

template <level_type n> void ObjectRegistry<n>::find_bond_angle_types() {
    find_bond_angle_types_impl<0>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level < n - 1), void>
ObjectRegistry<n>::find_bond_angle_types_impl() {
    for (auto bt : get_objects<BodyType, level + 1>()) {
        auto graph = bt.second.graph();
        auto fcts = graph.get_vertices();
        auto chains = graph.find_chains(3);
        for (auto c : chains) {
            // Create new bond angle type
            auto new_bond_angle_type =
                BondAngleType<n, level>(*this,
                                        ObjectIndex<BondAngleType, n, level>(
                                            bond_angle_type_counter_[level]),
                                        ObjectIndex<ForceCenterType, n, level>(
                                            bt.second[c[0]].get_id()),
                                        ObjectIndex<ForceCenterType, n, level>(
                                            bt.second[c[1]].get_id()),
                                        ObjectIndex<ForceCenterType, n, level>(
                                            bt.second[c[2]].get_id()));
            // Loop through existing bond angle types to see if the current
            // one already exists in object registry
            bool exists = false;
            for (auto el : get_objects<BondAngleType, level>()) {
                if (el.second == new_bond_angle_type) {
                    exists = true;
                    break;
                }
            }
            // If no match was found, create a new bond angle type and add
            // to the registry
            if (!exists) {
                get_objects<BondAngleType, level>().emplace(
                    bond_angle_type_counter_[level]++,
                    std::move(new_bond_angle_type));
            }
        }
    }
    find_bond_angle_types_impl<level + 1>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level >= n - 1), void>
ObjectRegistry<n>::find_bond_angle_types_impl() {
    return;
}

template <level_type n> void ObjectRegistry<n>::find_torsion_types() {
    find_torsion_types_impl<0>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level < n - 1), void>
ObjectRegistry<n>::find_torsion_types_impl() {
    for (auto bt : get_objects<BodyType, level + 1>()) {
        auto graph = bt.second.graph();
        auto fcts = graph.get_vertices();
        auto chains = graph.find_chains(4);
        for (auto c : chains) {
            // Create new torsion type
            auto new_torsion_type =
                TorsionType<n, level>(*this,
                                      ObjectIndex<TorsionType, n, level>(
                                          torsion_type_counter_[level]),
                                      ObjectIndex<ForceCenterType, n, level>(
                                          bt.second[c[0]].get_id()),
                                      ObjectIndex<ForceCenterType, n, level>(
                                          bt.second[c[1]].get_id()),
                                      ObjectIndex<ForceCenterType, n, level>(
                                          bt.second[c[2]].get_id()),
                                      ObjectIndex<ForceCenterType, n, level>(
                                          bt.second[c[3]].get_id()));
            // Loop through existing torsion types to see if the current one
            // already exists in object registry
            bool exists = false;
            for (auto el : get_objects<TorsionType, level>()) {
                if (el.second == new_torsion_type) {
                    exists = true;
                    break;
                }
            }
            // If no match was found, create a new torsion type and add to
            // the registry
            if (!exists) {
                get_objects<TorsionType, level>().emplace(
                    torsion_type_counter_[level]++,
                    std::move(new_torsion_type));
            }
        }
    }
    find_torsion_types_impl<level + 1>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level >= n - 1), void>
ObjectRegistry<n>::find_torsion_types_impl() {
    return;
}

template <level_type n> void ObjectRegistry<n>::find_improper_torsion_types() {
    find_improper_torsion_types_impl<0>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level < n - 1), void>
ObjectRegistry<n>::find_improper_torsion_types_impl() {
    for (auto bt : get_objects<BodyType, level + 1>()) {
        auto graph = bt.second.graph();
        auto fcts = graph.get_vertices();
        auto stars = graph.find_stars(3);
        for (auto s : stars) {
            // Create new improper torsion type
            auto new_improper_torsion_type = ImproperTorsionType<n, level>(
                *this,
                ObjectIndex<ImproperTorsionType, n, level>(
                    improper_torsion_type_counter_[level]),
                ObjectIndex<ForceCenterType, n, level>(
                    bt.second[s[0]].get_id()),
                ObjectIndex<ForceCenterType, n, level>(
                    bt.second[s[1]].get_id()),
                ObjectIndex<ForceCenterType, n, level>(
                    bt.second[s[2]].get_id()),
                ObjectIndex<ForceCenterType, n, level>(
                    bt.second[s[3]].get_id()));
            // Loop through existing improper torsion types to see if the
            // current one already exists in object registry
            bool exists = false;
            for (auto el : get_objects<ImproperTorsionType, level>()) {
                if (el.second == new_improper_torsion_type) {
                    exists = true;
                    break;
                }
            }
            // If no match was found, create a new improper torsion type and
            // add to the registry
            if (!exists) {
                get_objects<ImproperTorsionType, level>().emplace(
                    improper_torsion_type_counter_[level]++,
                    std::move(new_improper_torsion_type));
            }
        }
    }
    find_improper_torsion_types_impl<level + 1>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level >= n - 1), void>
ObjectRegistry<n>::find_improper_torsion_types_impl() {
    return;
}

template <level_type n> void ObjectRegistry<n>::find_bonds() {
    find_bonds_impl<0>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level < n - 1), void> ObjectRegistry<n>::find_bonds_impl() {
    // Loop over body types
    for (const auto &body_type_el : get_objects<BodyType, level + 1>()) {
        const auto &body_type = body_type_el.second;
        // Find the body type's bond types and store which vertex indices
        // they refer to
        auto chains = body_type.graph().find_chains(2);
        auto bond_types = std::vector<size_type>(
            chains.size(), std::numeric_limits<size_type>::max());
        for (size_type ci = 0; ci != chains.size(); ++ci) {
            // Create new bond type to compare with existing ones
            auto new_bondtype = BondType<n, level>(
                *this,
                ObjectIndex<BondType, n, level>(bond_type_counter_[level]),
                ObjectIndex<ForceCenterType, n, level>(
                    body_type[chains[ci][0]].get_id()),
                ObjectIndex<ForceCenterType, n, level>(
                    body_type[chains[ci][1]].get_id()));
            // Loop through existing bond types to see if the current
            // one already exists in object registry
            for (auto el : get_objects<BondType, level>()) {
                if (el.second == new_bondtype) {
                    bond_types[ci] = el.second.get_id();
                    break;
                }
            }
            // If no match was found, create a new bond type and add to
            // the registry
            if (bond_types[ci] == std::numeric_limits<size_type>::max()) {
                bond_types[ci] = bond_type_counter_[level]++;
                get_objects<BondType, level>().emplace(bond_types[ci],
                                                       std::move(new_bondtype));
            }
        }
        auto unique_types = std::vector<ObjectIndex<BondType, n, level>>{};
        for (auto t : bond_types) {
            if (std::find(unique_types.begin(), unique_types.end(), t) ==
                unique_types.end()) {
                unique_types.push_back(t);
            }
        }
        // Loop over each bond type present in the body type and add all
        // individual bond exemplars
        for (const auto &ui : unique_types) {
            // Loop over bodies to find those of the right type
            for (const auto &body_el : get_objects<Body, level + 1>()) {
                const auto &body = body_el.second;
                // Move on if body is of the wrong type
                if (body.type().get_id() != body_type.get_id()) {
                    continue;
                }
                for (size_type i = 0; i != chains.size(); ++i) {
                    if (bond_types[i] == ui) {
                        // Create new Bond and add to registry
                        size_type traj = body.get_trajectory_id();
                        get_objects<Bond, level>().emplace(
                            bond_counter_[level],
                            Bond<n, level>(
                                *this,
                                ObjectIndex<Bond, n, level>(
                                    bond_counter_[level]),
                                ui,
                                get_object(body[chains[i][0]].get_id())
                                    .get_force_center_id(0),
                                get_object(body[chains[i][1]].get_id())
                                    .get_force_center_id(0),
                                traj)); // TODO: not 0 in
                                        // get_force_center_id()
                        ++bond_counter_[level];
                    }
                }
            }
        }
    }
    find_bonds_impl<level + 1>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level >= n - 1), void> ObjectRegistry<n>::find_bonds_impl() {
    return;
}

template <level_type n> void ObjectRegistry<n>::find_bond_angles() {
    find_bond_angles_impl<0>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level < n - 1), void>
ObjectRegistry<n>::find_bond_angles_impl() {
    // Loop over body types
    for (auto body_type_el : get_objects<BodyType, level + 1>()) {
        auto body_type = body_type_el.second;
        // Find the body type's bond angle types and store which vertex
        // indices they refer to
        auto chains = body_type.graph().find_chains(3);
        auto angle_types = std::vector<size_type>(
            chains.size(), std::numeric_limits<size_type>::max());
        for (size_type ci = 0; ci != chains.size(); ++ci) {
            // Create new bond angle type to compare with existing ones
            auto new_angletype =
                BondAngleType<n, level>(*this,
                                        ObjectIndex<BondAngleType, n, level>(
                                            bond_angle_type_counter_[level]),
                                        ObjectIndex<ForceCenterType, n, level>(
                                            body_type[chains[ci][0]].get_id()),
                                        ObjectIndex<ForceCenterType, n, level>(
                                            body_type[chains[ci][1]].get_id()),
                                        ObjectIndex<ForceCenterType, n, level>(
                                            body_type[chains[ci][2]].get_id()));
            // Loop through existing bond angle types to see if the current
            // one already exists in object registry
            for (auto el : get_objects<BondAngleType, level>()) {
                if (el.second == new_angletype) {
                    angle_types[ci] = el.second.get_id();
                    break;
                }
            }
            // If no match was found, create a new bond angle type and add
            // to the registry
            if (angle_types[ci] == std::numeric_limits<size_type>::max()) {
                angle_types[ci] = bond_angle_type_counter_[level]++;
                get_objects<BondAngleType, level>().emplace(
                    angle_types[ci], std::move(new_angletype));
            }
        }
        auto unique_types = std::vector<ObjectIndex<BondAngleType, n, level>>{};
        for (auto t : angle_types) {
            if (std::find(unique_types.begin(), unique_types.end(), t) ==
                unique_types.end()) {
                unique_types.push_back(t);
            }
        }
        // Loop over each bond angle type present in the body type and add
        // all individual bond angle exemplars
        for (auto ui : unique_types) {
            // Loop over bodies to find those of the right type
            for (auto body_el : get_objects<Body, level + 1>()) {
                auto body = body_el.second;
                // Move on if body is of the wrong type
                if (body.type().get_id() != body_type.get_id()) {
                    continue;
                }
                for (size_type i = 0; i != chains.size(); ++i) {
                    if (angle_types[i] == ui) {
                        // Create new Bond Angle and add to registry
                        size_type traj = body.get_trajectory_id();
                        get_objects<BondAngle, level>().emplace(
                            bond_angle_counter_[level],
                            BondAngle<n, level>(
                                *this,
                                ObjectIndex<BondAngle, n, level>(
                                    bond_angle_counter_[level]),
                                ui,
                                get_object(body[chains[i][0]].get_id())
                                    .get_force_center_id(0),
                                get_object(body[chains[i][1]].get_id())
                                    .get_force_center_id(0),
                                get_object(body[chains[i][2]].get_id())
                                    .get_force_center_id(0),
                                traj));
                        ++bond_angle_counter_[level];
                    }
                }
            }
        }
    }
    find_bond_angles_impl<level + 1>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level >= n - 1), void>
ObjectRegistry<n>::find_bond_angles_impl() {
    return;
}

template <level_type n> void ObjectRegistry<n>::find_torsions() {
    find_torsions_impl<0>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level < n - 1), void>
ObjectRegistry<n>::find_torsions_impl() {
    // Loop over body types
    for (auto body_type_el : get_objects<BodyType, level + 1>()) {
        auto body_type = body_type_el.second;
        // Find the body type's torsion types and store which vertex indices
        // they refer to
        auto chains = body_type.graph().find_chains(4);
        auto torsion_types = std::vector<size_type>(
            chains.size(), std::numeric_limits<size_type>::max());
        for (size_type ci = 0; ci != chains.size(); ++ci) {
            // Create new torsion type to compare with existing ones
            auto new_torsiontype =
                TorsionType<n, level>(*this,
                                      ObjectIndex<TorsionType, n, level>(
                                          torsion_type_counter_[level]),
                                      ObjectIndex<ForceCenterType, n, level>(
                                          body_type[chains[ci][0]].get_id()),
                                      ObjectIndex<ForceCenterType, n, level>(
                                          body_type[chains[ci][1]].get_id()),
                                      ObjectIndex<ForceCenterType, n, level>(
                                          body_type[chains[ci][2]].get_id()),
                                      ObjectIndex<ForceCenterType, n, level>(
                                          body_type[chains[ci][3]].get_id()));
            // Loop through existing torsion types to see if the current
            // one already exists in object registry
            for (auto el : get_objects<TorsionType, level>()) {
                if (el.second == new_torsiontype) {
                    torsion_types[ci] = el.second.get_id();
                    break;
                }
            }
            // If no match was found, create a new torsion type and add to
            // the registry
            if (torsion_types[ci] == std::numeric_limits<size_type>::max()) {
                torsion_types[ci] = torsion_type_counter_[level]++;
                get_objects<TorsionType, level>().emplace(
                    torsion_types[ci], std::move(new_torsiontype));
            }
        }
        auto unique_types = std::vector<ObjectIndex<TorsionType, n, level>>{};
        for (auto t : torsion_types) {
            if (std::find(unique_types.begin(), unique_types.end(), t) ==
                unique_types.end()) {
                unique_types.push_back(t);
            }
        }
        // Loop over each torsion type present in the body type and add all
        // individual torsion exemplars
        for (auto ui : unique_types) {
            // Loop over bodies to find those of the right type
            for (auto body_el : get_objects<Body, level + 1>()) {
                auto body = body_el.second;
                // Move on if body is of the wrong type
                if (body.type().get_id() != body_type.get_id()) {
                    continue;
                }
                for (size_type i = 0; i != chains.size(); ++i) {
                    if (torsion_types[i] == ui) {
                        // Create new Torsion and add to registry
                        size_type traj = body.get_trajectory_id();
                        get_objects<Torsion, level>().emplace(
                            torsion_counter_[level],
                            Torsion<n, level>(
                                *this,
                                ObjectIndex<Torsion, n, level>(
                                    torsion_counter_[level]),
                                ui,
                                get_object(body[chains[i][0]].get_id())
                                    .get_force_center_id(0),
                                get_object(body[chains[i][1]].get_id())
                                    .get_force_center_id(0),
                                get_object(body[chains[i][2]].get_id())
                                    .get_force_center_id(0),
                                get_object(body[chains[i][3]].get_id())
                                    .get_force_center_id(i),
                                traj));
                        ++bond_angle_counter_[level];
                    }
                }
            }
        }
    }
    find_torsions_impl<level + 1>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level >= n - 1), void>
ObjectRegistry<n>::find_torsions_impl() {
    return;
}

// start

template <level_type n> void ObjectRegistry<n>::find_improper_torsions() {
    find_improper_torsions_impl<0>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level < n - 1), void>
ObjectRegistry<n>::find_improper_torsions_impl() {
    auto body_map = get_objects<Body, level + 1>();
    auto body_types = std::vector<ObjectIndex<BodyType, n, level + 1>>{};
    // Loop over bodies
    for (auto body_el : body_map) {
        // Get the body type and check if it has already been considered
        auto body_type = body_el.second.type();
        auto type_index = body_el.second.get_type_id();
        assert(type_index == body_type.get_id());
        // If body type has not been considered, find its relevant improper
        // torsion types and the indices they refer to. Then add it to list
        // of considered types
        if (std::find(body_types.begin(), body_types.end(), type_index) ==
            body_types.end()) {
            body_types.push_back(type_index);
            // Find the body type's improper torsion types and store which
            // vertex indices they refer to
            auto stars = body_type.graph().find_stars(3);
            for (auto s : stars) {
                // Create new improper torsion type to compare with existing
                // ones
                auto new_it_type = ImproperTorsionType<n, level>(
                    *this,
                    ObjectIndex<ImproperTorsionType, n, level>(
                        improper_torsion_type_counter_[level]),
                    ObjectIndex<ForceCenterType, n, level>(
                        body_type[s[0]].get_id()),
                    ObjectIndex<ForceCenterType, n, level>(
                        body_type[s[1]].get_id()),
                    ObjectIndex<ForceCenterType, n, level>(
                        body_type[s[2]].get_id()),
                    ObjectIndex<ForceCenterType, n, level>(
                        body_type[s[3]].get_id()));

                // Loop through existing improper torsion types to see if
                // the current one already exists in object registry
                size_type it_type_index = std::numeric_limits<size_type>::max();
                for (auto el : get_objects<ImproperTorsionType, level>()) {
                    if (el.second == new_it_type) {
                        it_type_index = el.second.get_id();
                        break;
                    }
                }
                // If no match was found, create a new improper torsion type
                // and add to the registry
                if (it_type_index == std::numeric_limits<size_type>::max()) {
                    it_type_index = improper_torsion_type_counter_[level]++;
                    get_objects<ImproperTorsionType, level>().emplace(
                        it_type_index, std::move(new_it_type));
                }
                // Find all the bodies of the current body type and add
                // improper torsion exemplars to the object registry
                for (auto el : body_map) {
                    if (el.second.get_type_id() == type_index) {
                        // Create new ImproperTorsion and add to registry
                        get_objects<ImproperTorsion, level>().emplace(
                            improper_torsion_counter_[level],
                            ImproperTorsion<n, level>(
                                *this,
                                ObjectIndex<ImproperTorsion, n, level>(
                                    improper_torsion_counter_[level]),
                                it_type_index,
                                get_object(el.second[s[0]].get_id())
                                    .get_force_center_id(0),
                                get_object(el.second[s[1]].get_id())
                                    .get_force_center_id(0),
                                get_object(el.second[s[2]].get_id())
                                    .get_force_center_id(0),
                                get_object(el.second[s[3]].get_id())
                                    .get_force_center_id(0)));
                        ++improper_torsion_counter_[level];
                    }
                }
            }
        }
    }
    find_improper_torsions_impl<level + 1>();
}

template <level_type n>
template <level_type level>
std::enable_if_t<(level >= n - 1), void>
ObjectRegistry<n>::find_improper_torsions_impl() {
    return;
}

template <level_type n>
ObjectIndex<Trajectory, n, 0> ObjectRegistry<n>::add_CPMD_trajectories(
    Stoichiometry<n, 1> stoichiometry, size_type no_trajectories,
    std::vector<unsigned> units_per_trajectory,
    std::vector<unsigned> trajectory_lengths, std::vector<double> box_lengths,
    std::vector<double> timestep_lengths, std::vector<double> coordinates,
    std::vector<double> velocities, std::vector<double> forces) {
    // Compute the number of atoms in a stoichiometric unit
    auto atoms_per_formula = stoichiometry.bodies_per_formula_unit();
    // Store trajectories in CHAMPION-native data structures
    std::vector<Point<real>> point_coordinates;
    std::vector<Vector<real>> vector_velocities;
    std::vector<Vector<real>> vector_forces;
    size_type index = 0;
    for (size_type i = 0; i != no_trajectories; ++i) {
        for (size_type j = 0; j != trajectory_lengths[i]; ++j) {
            for (size_type k = 0;
                 k != units_per_trajectory[i] * atoms_per_formula; ++k) {
                point_coordinates.push_back(Point<real>{
                    std::pair<real, real>{0, box_lengths[i]},
                    coordinates[3 * index], coordinates[3 * index + 1],
                    coordinates[3 * index + 2]});
                vector_velocities.push_back({velocities[3 * index],
                                             velocities[3 * index + 1],
                                             velocities[3 * index + 2]});
                vector_forces.push_back({forces[3 * index],
                                         forces[3 * index + 1],
                                         forces[3 * index + 2]});

                ++index;
            }
        }
    }
    // Create molecules and atoms
    for (size_type traj = 0; traj != no_trajectories; ++traj) {
        for (int i = 0; i != stoichiometry.size(); ++i) {
            for (int j = 0; j != stoichiometry.relative_amount(i) *
                                     units_per_trajectory[traj];
                 ++j) {
                make_body(stoichiometry.get_index(i), traj);
            }
        }
    }
    size_type start_index = 0;
    // Reorder the elements of the trajectories to CHAMPION's order
    for (int traj = 0; traj != no_trajectories; ++traj) {
        auto N = units_per_trajectory[traj] * atoms_per_formula;
        // Get atom indices and put in a vector
        auto atoms = std::vector<ObjectIndex<Body, n, 0>>{};
        for (size_type i = 0; i != N; ++i) {
            atoms.push_back({start_index + i});
        }
        auto trajectory_0 = make_trajectory<0>();
        auto trajectory_1 = make_trajectory<1>(trajectory_0);
        auto order = std::vector<size_type>(N);
        std::iota(order.begin(), order.end(), start_index);
        // Find the reordering to obtain CPMD's order from CHAMPION's
        std::stable_sort(order.begin(), order.end(),
                         [&atoms, this](size_type i, size_type j) {
                             return get_object<Body, 0>(i).type().get_mass() <
                                    get_object<Body, 0>(j).type().get_mass();
                         });
        real time = 0;
        // Revert the sorting and reorder the trajectories consistently
        // across all time steps
        for (size_type i = 0; i < trajectory_lengths[traj]; ++i) {
            auto key = order;
            auto offset = start_index * trajectory_lengths[traj] + i * N;
            auto point_slice =
                VectorSlice<Point<real>>(point_coordinates, offset, N);
            auto vel_slice =
                VectorSlice<Vector<real>>(vector_velocities, offset, N);
            auto force_slice =
                VectorSlice<Vector<real>>(vector_forces, offset, N);
            sort_consistently(key, point_slice, vel_slice, force_slice);
            auto atom_index_vec = std::vector<ObjectIndex<Body, n, 0>>(N);
            for (size_type j = 0; j != N; ++j) {
                atom_index_vec[j] = start_index + j;
            }
            // Create std::vector of champion positions
            auto points_i = std::vector<Point<real>>(point_slice.begin(),
                                                     point_slice.end());
            // Create Configuration
            auto config_i = make_configuration<0>(atoms, points_i);
            // Create std::vectors of champion velocities and forces
            auto vels_i =
                std::vector<Vector<real>>(vel_slice.begin(), vel_slice.end());
            auto forces_i = std::vector<Vector<real>>(force_slice.begin(),
                                                      force_slice.end());
            // Increment time
            time += timestep_lengths[traj];
            // Add the current timestep to the trajectory
            get_object(trajectory_0)
                .add_timestep(time, config_i, vels_i, forces_i);

            // Create level 1 representation
            std::vector<ObjectIndex<Body, n, 1>> molecules;
            std::vector<Point<real>> positions;
            std::vector<Rotation<real>> orientations;
            auto body_1_map = get_objects<Body, 1>();
            // Loop over body<1>s of the current trajectory
            for (const auto &el : body_1_map) {
                const auto body = el.second;
                if (body.get_trajectory_index() == trajectory_1) {
                    // Add molecule
                    molecules.push_back(body.get_id());
                    // Get constituent atoms
                    std::vector<ObjectIndex<Body, n, 0>> constituent_bodies{};
                    for (const auto &fc : body) {
                        constituent_bodies.push_back(fc.body().get_id());
                    }

                    // Compute center-of-mass position
                    std::vector<Point<real>> atomic_positions{};
                    std::vector<real> masses{};
                    for (const auto &constituent : constituent_bodies) {
                        auto index =
                            get_object(config_i).get_index_by_id(constituent);
                        atomic_positions.push_back(
                            get_object(config_i).get_position(index));
                        masses.push_back(
                            get_object(constituent).type().get_mass());
                    }
                    // Transform to coordinate system centered at the first
                    // atom
                    std::vector<Vector<real>> rel_positions{};
                    for (const auto &point : atomic_positions) {
                        rel_positions.push_back(
                            Vector<real>{point - atomic_positions[0]});
                    }
                    // Compute center-of-mass in translated, unbounded
                    // coordinate system
                    auto com_translated = Vector<real>{0, 0, 0};
                    real mass = 0;
                    for (size_type i = 0; i != rel_positions.size(); ++i) {
                        com_translated += rel_positions[i] * masses[i];
                        mass += masses[i];
                    }
                    com_translated /= mass;
                    // Transform back into the periodic box
                    Point<real> com = atomic_positions[0] + com_translated;
                    // Add position
                    positions.push_back(com);

                    // Compute orientation
                    // auto prot = body.type().prototype();
                    // auto geom = Geometry{atomic_positions, com};
                    // auto transformation = prot.match_orientation(geom);
                    // auto rotation = transformation.rotation();
                    // orientations.push_back(rotation);
                    orientations.push_back({});
                }
            }
            // Create level 1 Configuration
            auto conf_1i =
                make_configuration<1>(molecules, positions, orientations);
            get_object(trajectory_1).add_timestep(time, conf_1i);
        }
        start_index += N;
    }
    // Return the index of the last added trajectory
    return trajectory_counter_ - 1;
}

template <level_type n> void ObjectRegistry<n>::initialize_db() {
    auto prepare = [*this](const std::string &stmt_str) {
        auto stmt_cstr = stmt_str.c_str();
        const char *tail;
        sqlite3_stmt *stmt;
        auto res =
            sqlite3_prepare_v2(db_, stmt_cstr, stmt_str.size(), &stmt, &tail);
        if (res != SQLITE_OK) {
            throw std::runtime_error{
                (boost::format(
                     "CHAMPION::ObjectRegistry::initialize_db(): Error "
                     "preparing SQL statement for SQlite3 database "
                     "(SQLite error code: %1%).") %
                 res)
                    .str()};
        }
        return stmt;
    };
    auto step = [](sqlite3_stmt *stmt) {
        auto res = sqlite3_step(stmt);
        if (res != SQLITE_DONE) {
            throw std::runtime_error{
                (boost::format("CHAMPION::ObjectRegistry::initialize_db()"
                               ": Error stepping SQL "
                               "statement for SQlite3 database (SQLite error "
                               "code: %1%).") %
                 res)
                    .str()};
        }
        return stmt;
    };
    auto finalize = [](sqlite3_stmt *stmt) {
        auto res = sqlite3_finalize(stmt);
        if (res != SQLITE_OK) {
            throw std::runtime_error{
                (boost::format("CHAMPION::ObjectRegistry::initialize_db()"
                               ": Error finalizing SQL "
                               "statement for SQlite3 database (SQLite error "
                               "code: %1%).") %
                 res)
                    .str()};
        }
    };
    {
        auto stmt_str = std::string{
            R"SQL(
                CREATE TABLE IF NOT EXISTS registries
                (
                    ID INTEGER PRIMARY KEY ASC AUTOINCREMENT
                );
            )SQL"};
        auto stmt = prepare(stmt_str);
        step(stmt);
        finalize(stmt);
    }
    {
        auto stmt_str = std::string{
            R"SQL(
                INSERT INTO registries DEFAULT VALUES;
            )SQL"};
        auto stmt = prepare(stmt_str);
        step(stmt);
        ID_ = sqlite3_column_int(stmt, 0);
    }
    {
        auto stmt_str = std::string{
            R"SQL(
            CREATE TABLE IF NOT EXISTS graph_types
            (
                name TEXT NOT NULL PRIMARY KEY
            );
        )SQL"};
        auto stmt = prepare(stmt_str);
        step(stmt);
        finalize(stmt);
    }
    auto graph_types = std::vector<std::string>{"BodyType", "Body"};
    for (const auto &type : graph_types) {
        auto stmt_str = (boost::format("INSERT OR REPLACE INTO "
                                       "graph_types(name) VALUES ('%1%');") %
                         type)
                            .str();
        auto stmt = prepare(stmt_str);
        step(stmt);
        finalize(stmt);
    }
    {
        auto stmt_str = std::string{
            R"SQL(
            CREATE TABLE IF NOT EXISTS graphs
            (
                ID INTEGER PRIMARY KEY ASC AUTOINCREMENT,
                type TEXT NOT NULL REFERENCES graph_types(name)
                    ON DELETE CASCADE ON UPDATE CASCADE,
                registry INTEGER NOT NULL REFERENCES registries(ID)
                    ON DELETE CASCADE ON UPDATE CASCADE,
                level INTEGER NOT NULL
            );
            )SQL"};
        auto stmt = prepare(stmt_str);
        step(stmt);
        finalize(stmt);
    }
    {
        auto stmt_str = std::string{
            R"SQL(
            CREATE TABLE IF NOT EXISTS graph_edges
            (
                graph INTEGER NOT NULL REFERENCES graphs(ID)
                    ON DELETE CASCADE ON UPDATE CASCADE,
                vertex_1 INTEGER NOT NULL,
                vertex_2 INTEGER NOT NULL
            );
        )SQL"};
        auto stmt = prepare(stmt_str);
        step(stmt);
        finalize(stmt);
    }
    {
        auto stmt_str = std::string{
            R"SQL(
            CREATE TABLE IF NOT EXISTS body_types
            (
                registry INTEGER NOT NULL REFERENCES registries(ID)
                    ON DELETE CASCADE ON UPDATE CASCADE, 
                level INTEGER NOT NULL, 
                ID INTEGER NOT NULL, 
                name TEXT,
                mass REAL NOT NULL,
                radius REAL,
                graph INTEGER REFERENCES graphs(ID) 
                    ON DELETE SET NULL ON UPDATE NO ACTION,
                UNIQUE (registry, level, ID)
            );
        )SQL"};
        auto stmt = prepare(stmt_str);
        step(stmt);
        finalize(stmt);
    }
}

template <level_type n> void ObjectRegistry<n>::save_registry() const {}

template <template <level_type, level_type> class T, level_type n,
          level_type level = 0>
std::enable_if_t<(level < n), std::ostream &>
print_all(std::ostream &os, const ObjectRegistry<n> &reg) {
    auto objects = reg.template get_objects<T, level>();
    for (const auto &oi : objects) {
        os << oi << std::endl;
    }
    print_all<T, n, level + 1>(os, reg);
    return os;
}

template <template <level_type, level_type> class T, level_type n,
          level_type level = 0>
std::enable_if_t<(level >= n), std::ostream &>
print_all(std::ostream &os, const ObjectRegistry<n> &reg) {
    return os;
}

template <level_type n>
std::ostream &operator<<(std::ostream &os, const ObjectRegistry<n> &reg) {
    os << "ObjectRegistry<" << n << ">: \n" << std::endl;
    print_all<BodyType, n, 0>(os, reg);
    print_all<ForceCenterType, n, 0>(os, reg);
    print_all<BondType, n, 0>(os, reg);
    print_all<BondAngleType, n, 0>(os, reg);
    print_all<TorsionType, n, 0>(os, reg);
    print_all<ImproperTorsionType, n, 0>(os, reg);
    print_all<Body, n, 0>(os, reg);
    print_all<ForceCenter, n, 0>(os, reg);
    print_all<Bond, n, 0>(os, reg);
    print_all<BondAngle, n, 0>(os, reg);
    print_all<Torsion, n, 0>(os, reg);
    print_all<ImproperTorsion, n, 0>(os, reg);
    // print_all<Configuration, n, 0>(os, reg);
    // print_all<Trajectory, n, 0>(os, reg);
    return os;
}

} // namespace champion

#endif
