#ifndef CHAMPION_VECTOR_SLICE_COLLECTION_HPP
#define CHAMPION_VECTOR_SLICE_COLLECTION_HPP

#include <iostream>
#include <vector>

#include "set_precision.hpp"
#include "VectorSlice.hpp"

namespace champion {

template <typename T> class VectorSliceCollection {
  public:
    // Public types
    using iterator = typename std::vector<VectorSlice<T>>::iterator;
    using const_iterator = typename std::vector<VectorSlice<T>>::const_iterator;
    // Public member functions
    // Default constructor
    VectorSliceCollection() : slices_{} {}
    // Construct from vector and slice length
    VectorSliceCollection(std::vector<T> &vec, size_type slice_length)
        : slices_(vec.size() / slice_length) {
        if (vec.size() % slice_length != 0) {
            slices_.push_back({});
        }
	size_type offset = 0;
	// Construct slices
	for (auto &vi : slices_) {
	    vi = VectorSlice<T>{vec, offset, slice_length};
	    offset += slice_length;
	}
    }
    // Access
    size_type size() const { return slices_.size(); }
    iterator begin() { return slices_.begin(); }
    iterator end() { return slices_.end(); }
    const_iterator begin() const { return slices_.begin(); }
    const_iterator end() const { return slices_.end(); }
    const_iterator cbegin() const { return slices_.begin(); }
    const_iterator cend() const { return slices_.end(); }
    
    VectorSlice<T> &operator[](size_type i) {
	return *(begin() + i);
    }
    const VectorSlice<T> &operator[](size_type i) const {
	return *(begin() + i);
    }

  private:
    // Private data members
    std::vector<VectorSlice<T>> slices_;
};

template <typename T>
std::ostream &operator<<(std::ostream &os, VectorSliceCollection<T> vsc) {
    os << "VectorSliceCollection: \n";
    for (size_type i = 0; i != vsc.size(); ++i) {
	os << "Slice " << i << ": " << vsc[i] << std::endl;
    }
    return os;
}

} // namespace champion

#endif
