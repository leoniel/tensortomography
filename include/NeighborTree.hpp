#ifndef CHAMPION_NEIGHBOR_TREE_HPP
#define CHAMPION_NEIGHBOR_TREE_HPP

#include <algorithm>
#include <cassert>
#include <limits>
#include <numeric>
#include <vector>

#include "set_precision.hpp"

namespace champion {

// Forward declarations
template <typename vertex_type> class Graph;

template <typename vertex_type> class NeighborTree {
  public:
    // Construct from Graph, index of seed vertex and depth (default unlimited)
    NeighborTree(Graph<vertex_type> g, size_type i,
                 size_type depth = std::numeric_limits<size_type>::max())
        : g_{g}, tree_{{i}} {
        explore(depth);
    }

    size_type size() { return tree_.size(); }
    
    // Explore graph <depth> further steps from current tree and update tree_
    // with
    // the newly discovered vertices. Return actual additional depth traversed.
    // Default behavior is to traverse unlimited depth until all vertices are
    // discovered
    size_type explore(size_type depth = std::numeric_limits<size_type>::max());

    // Return a handle to the i:th vertex of the NeighborTree:s Graph object
    const vertex_type &get_vertex(size_type i) {
        assert(i < g_.size());
        return *g_.get_vertex(i);
    }

    // Return a std::vector containing vertex indices sorted by depth and
    // vertices
    std::vector<size_type>
    list_tree(size_type depth = std::numeric_limits<size_type>::max());

    // Return a vector containing the neighbors at distance <i>, or empty if the
    // fully discovered tree is not that deep
    std::vector<size_type> get_neighbors(size_type i);

  private:
    // Private data members
    Graph<vertex_type> g_;
    std::vector<std::vector<size_type>> tree_;
};

template <typename vertex_type>
std::vector<size_type> NeighborTree<vertex_type>::get_neighbors(size_type i) {
    auto N = tree_.size();
    if (i < N) {
        return tree_[i];
    } else {
        auto levels = explore(i - N + 1);
        if (i < N + levels) {
            return tree_[i];
        } else
            return {};
    }
}

template <typename vertex_type>
std::vector<size_type> NeighborTree<vertex_type>::list_tree(size_type depth) {
    std::vector<size_type> found_vertices;
    for (size_type i = 0; i != depth; ++i) {
        auto next_shell = get_neighbors(i);
	if (next_shell.empty()) {
	    break;
	}
	for (auto vi : next_shell) {
	    found_vertices.push_back(vi);
	}
    }
    return found_vertices;
}

template <typename vertex_type>
size_type NeighborTree<vertex_type>::explore(size_type depth) {
    if (depth == 0) {
        return 0;
    }
    // Create a list of all vertices
    auto N = g_.size();
    std::vector<size_type> unvisited(N);
    std::iota(unvisited.begin(), unvisited.end(), 0);
    // Remove already visited ones from the list
    for (const auto &shell : tree_) {
        for (auto ni : shell) {
            auto match_it = std::find(unvisited.begin(), unvisited.end(), ni);
            unvisited.erase(match_it);
        }
    }
    auto edges = g_.get_edges();
    size_type traversed = 0; // Keep track of actual depth traversed
    // Explore to requested depth or until all vertices have been visited
    while (!unvisited.empty() && depth--) {
        ++traversed;
        auto prev_shell = tree_[tree_.size() - 1];
        std::vector<size_type> next_shell;
        for (auto i : prev_shell) {
            for (auto e : edges) {
                if (e.first == i) {
                    auto match_it =
                        std::find(unvisited.begin(), unvisited.end(), e.second);
                    if (match_it != unvisited.end()) {
                        unvisited.erase(match_it);
                        next_shell.push_back(e.second);
                    }
                } else if (e.second == i) {
                    auto match_it =
                        std::find(unvisited.begin(), unvisited.end(), e.first);
                    if (match_it != unvisited.end()) {
                        unvisited.erase(match_it);
                        next_shell.push_back(e.first);
                    }
                }
            }
        }
        // Sort shell wrt vertices
        std::sort(next_shell.begin(), next_shell.end(),
                  [&](size_type a, size_type b) {
                      return *g_.get_vertex(a) < *g_.get_vertex(b);
                  });
        // Add shell to neighbor tree
        tree_.push_back(next_shell);
    }
    return traversed;
}

// Logical operators on NeighborTrees

template <typename vertex_type>
bool operator<(NeighborTree<vertex_type> &NT1, NeighborTree<vertex_type> &NT2) {
    size_type i = 0;
    size_type N = 0;
    do {
        auto NT1i = NT1.get_neighbors(i);
        auto NT2i = NT2.get_neighbors(i);
        auto N1 = NT1i.size();
        auto N2 = NT2i.size();
        N = std::min(N1, N2);
        for (size_type j = 0; j != N; ++j) {
            auto v1 = NT1.get_vertex(NT1i[j]);
            auto v2 = NT2.get_vertex(NT2i[j]);
            if (v1 < v2) {
                return true;
            } else if (v2 < v1) {
                return false;
            }
        }
        if (N1 < N2) {
            return true;
        } else if (N2 < N1) {
            return false;
        }
        ++i;
    } while (N);
    return false;
}

// const version for use in std::sort()
template <typename vertex_type>
bool operator<(const NeighborTree<vertex_type> &NT1,
               const NeighborTree<vertex_type> &NT2) {
    auto NT1_cpy = NT1;
    auto NT2_cpy = NT2;
    return NT1_cpy < NT2_cpy;
}

template <typename vertex_type>
bool operator>(const NeighborTree<vertex_type> &NT1,
               const NeighborTree<vertex_type> &NT2) {
    return NT2 < NT1;
}

template <typename vertex_type>
bool operator==(const NeighborTree<vertex_type> &NT1,
                const NeighborTree<vertex_type> &NT2) {
    return !(NT2 < NT1) && !(NT1 < NT2);
}

template <typename vertex_type>
bool operator!=(const NeighborTree<vertex_type> &NT1,
                const NeighborTree<vertex_type> &NT2) {
    return NT1 < NT2 || NT2 < NT1;
}

template <typename vertex_type>
bool operator<=(const NeighborTree<vertex_type> &NT1,
                const NeighborTree<vertex_type> &NT2) {
    return !(NT2 < NT1);
}

template <typename vertex_type>
bool operator>=(const NeighborTree<vertex_type> &NT1,
                const NeighborTree<vertex_type> &NT2) {
    return !(NT1 < NT2);
}
}

#endif
