#ifndef CHAMPION_RANDOM_HPP
#define CHAMPION_RANDOM_HPP

#include <algorithm>
#include <random>

#include <omp.h>

#include "Matrix.hpp"
#include "set_precision.hpp"

namespace champion {

class RandomNumberGenerator {
  public:
    RandomNumberGenerator() : RNGs_{} {
        // Initialize random device (one per MPI rank)
        std::random_device rd{};
        unsigned N = MPI_size;
        // Create and broadcast vector of the number of threads
        auto M = std::vector<unsigned>(N);
        unsigned M_i = omp_get_max_threads();
        std::vector<unsigned> seeds(M_i);
#ifdef MPI_VERSION
        MPI_Gather(&M_i, 1, MPI_UNSIGNED, &M[0], 1, MPI_UNSIGNED, MPI_root,
                   MPI_COMM_WORLD);
#else
        M[0] = M_i;
#endif
        // Generate a distinct random seed for each possible OMP thread on each
        // MPI rank
        if (MPI_rank == MPI_root) {
            std::vector<unsigned> all_seeds;
            for (unsigned i = 0; i < N; ++i) {
                for (unsigned j = 0; j < M[i]; ++j) {
                    unsigned seed = rd();
                    while (std::find(all_seeds.begin(), all_seeds.end(),
                                     seed) != all_seeds.end()) {
                        seed = rd();
                    }
                    all_seeds.push_back(seed);
                }
            }

#ifdef MPI_VERSION
            // Distribute the random seeds across MPI ranks
            unsigned start_index = 0;
            for (size_type i = 0; i < N; ++i) {
                MPI_Send(&all_seeds[start_index], M[i], MPI_UNSIGNED, i, 0,
                         MPI_COMM_WORLD);
                start_index += M[i];
            }
#else
            seeds = all_seeds;
#endif
        }

#ifdef MPI_VERSION
        MPI_Recv(&seeds[0], M_i, MPI_UNSIGNED, MPI_root, 0, MPI_COMM_WORLD,
                 MPI_STATUS_IGNORE);
#endif
        RNGs_.resize(M_i);
        for (size_type i = 0; i < M_i; ++i) {
            RNGs_[i] = std::mt19937{seeds[i]};
        }
    }

    size_type size() const { return RNGs_.size(); }
    std::mt19937 &operator()() { return RNGs_[omp_get_thread_num()]; }

  private:
    std::vector<std::mt19937> RNGs_;
};

template <typename T> class uniform_int_distribution {
  public:
    uniform_int_distribution(T low, T high, RandomNumberGenerator &rng)
        : rng_{&rng},
          distributions_(rng.size(),
                         std::uniform_int_distribution<T>{low, high}) {}

    T operator()() { return distributions_[omp_get_thread_num()]((*rng_)()); }

  private:
    RandomNumberGenerator *rng_;
    std::vector<std::uniform_int_distribution<T>> distributions_;
};

template <typename T> class uniform_real_distribution {
  public:
    uniform_real_distribution(T low, T high, RandomNumberGenerator &rng)
        : rng_{&rng},
          distributions_(rng.size(),
                         std::uniform_real_distribution<T>{low, high}) {}

    T operator()() { return distributions_[omp_get_thread_num()]((*rng_)()); }

  private:
    RandomNumberGenerator *rng_;
    std::vector<std::uniform_real_distribution<T>> distributions_;
};

template <typename T> class normal_distribution {
  public:
    normal_distribution(T mean, T standard_deviation,
                        RandomNumberGenerator &rng)
        : rng_{&rng},
          distributions_(rng.size(), std::normal_distribution<T>{
                                         mean, standard_deviation}) {}

    T operator()() { return distributions_[omp_get_thread_num()]((*rng_)()); }

  private:
    RandomNumberGenerator *rng_;
    std::vector<std::normal_distribution<T>> distributions_;
};

} // namespace champion

#endif