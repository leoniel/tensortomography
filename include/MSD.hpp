/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: MeanSquaredDisplacement.hpp

AUTHOR: Rasmus Andersson
MODIFIED BY:

DESCRIPTION: Class for mean squared displacements and diffusivities

\******************************************************************************/

#ifndef CHAMPION_MeanSquaredDisplacement_HPP
#define CHAMPION_MeanSquaredDisplacement_HPP

#include <algorithm>
#include <utility>

#include "Histogram.hpp"
#include "NormalDistribution.hpp"
#include "ObjectRegistry.hpp"
#include "set_precision.hpp"
#include "statistics.hpp"

namespace champion {

template <level_type n, level_type level> class MeanSquaredDisplacement {
  public:
    // Public member functions

    // Default constructor
    MeanSquaredDisplacement();
    // Construct from object registry, body type, time_span, number of time bins
    // and trajectories (optional)
    MeanSquaredDisplacement(const ObjectRegistry<n> &reg,
                            ObjectIndex<BodyType, n, level> bt, real time_span,
                            size_type no_time_bins,
                            std::vector<size_type> trajectories = {});
    // Initialize (or reset)
    void init_MSD(const ObjectRegistry<n> &reg,
                  ObjectIndex<BodyType, n, level> bt, real time_span,
                  size_type no_time_bins,
                  std::vector<size_type> trajectories = {});

    // Access functions

    // Get pair with {dt, MeanSquaredDisplacement(dt)}
    std::pair<std::vector<real>, std::vector<NormalDistribution>>
    get_MSD() const {
        return {dt_vec_, MeanSquaredDisplacement_};
    }
    // Get diffusivity {mean, standard error}
    NormalDistribution get_diffusivity();

  private:
    // Private functions
    void compute_diffusivity();

    // Private data members
    std::vector<real> dt_vec_;
    std::vector<NormalDistribution> MeanSquaredDisplacement_;
    NormalDistribution D_;
};

template <level_type n, level_type level>
MeanSquaredDisplacement<n, level>::MeanSquaredDisplacement()
    : dt_vec_{}, MeanSquaredDisplacement_{}, D_{} {}

template <level_type n, level_type level>
MeanSquaredDisplacement<n, level>::MeanSquaredDisplacement(
    const ObjectRegistry<n> &reg, ObjectIndex<BodyType, n, level> bt,
    real time_span, size_type no_time_bins, std::vector<size_type> trajectories)
    : dt_vec_{}, MeanSquaredDisplacement_{}, D_{} {
    init_MSD(reg, bt, time_span, no_time_bins, trajectories);
}

template <level_type n, level_type level>
void MeanSquaredDisplacement<n, level>::init_MSD(
    const ObjectRegistry<n> &reg, ObjectIndex<BodyType, n, level> bt,
    real time_span, size_type no_time_bins,
    std::vector<size_type> trajectories) {
    dt_vec_ = std::vector<real>(no_time_bins, 0);
    real dt = 0.0;
    real time_step = time_span / no_time_bins;
    for (size_type i = 0; i != no_time_bins; ++i) {
        dt += time_step;
        dt_vec_[i] = dt;
    }
    // Create vector of squared displacements, SD(t)
    auto squared_displacements = std::vector<std::vector<real>>(no_time_bins);
    // If no trajectory indices were given, use all trajectories
    if (trajectories.size() == 0) {
        for (auto &traj_el : reg.template get_objects<Trajectory, level>()) {
            trajectories.push_back(traj_el.first);
        }
    }
    // Loop over bodies
    auto body_map = reg.template get_objects<Body, level>();
    for (auto &el : body_map) {
        auto body = el.second;
        // Check if body is of the right type and in an included trajectory
        if (body.type().id() == bt &&
            std::find(trajectories.begin(), trajectories.end(),
                      body.get_trajectory_index()) != trajectories.end()) {
            auto trajectory = reg.template get_object<Trajectory, level>(
                body.get_trajectory_index());
            auto no_time_steps = trajectory.size();
            // Collect the worldline of the current body
            auto body_trajectory = std::vector<Point>(no_time_steps, {0, 0, 0});
            auto times = std::vector<real>(no_time_steps, 0);
            for (size_type t = 0; t != no_time_steps; ++t) {
                times[t] = trajectory.get_time(t);
                auto snapshot = trajectory.get_snapshot(t);
                auto i = snapshot.get_index_by_id(el.first);
                auto pos = snapshot.get_position(i);
                body_trajectory[t] = pos;
            }
            // Compute squared displacements and add to vector
            // Loop over starting times
            for (size_type ti = 0; ti != trajectory.size(); ++ti) {
                // Loop to starting time + time span
                auto tj = ti;
                real dt = 0;
                while (dt < time_span && tj < times.size()) {
                    auto it = std::find_if(dt_vec_.begin(), dt_vec_.end(),
                                           [dt](double x) { return x >= dt; });
                    size_type index = it - dt_vec_.begin();
                    auto squared_displacement = squared_magnitude(
                        body_trajectory[tj] - body_trajectory[ti]);
                    squared_displacements[index].push_back(
                        squared_displacement);
                    ++tj;
                    dt = times[tj] - times[ti];
                }
            }
        }
    }
    // Compute mean squared displacements and their standard deviations
    MeanSquaredDisplacement_ =
        std::vector<NormalDistribution>(no_time_bins, {0, 0});
    for (size_type i = 0; i != no_time_bins; ++i) {
        auto mu = mean(squared_displacements[i]);
        auto sigma = standard_deviation(squared_displacements[i], mu);
        MeanSquaredDisplacement_[i] = {mu, sigma};
    }
}

template <level_type n, level_type level>
NormalDistribution MeanSquaredDisplacement<n, level>::get_diffusivity() {
    if (D_.get_mean() != 0) {
        return D_;
    } else {
        compute_diffusivity();
    }
    return D_;
}

template <level_type n, level_type level>
void MeanSquaredDisplacement<n, level>::compute_diffusivity() {
    auto last = MeanSquaredDisplacement_.size() - 1;
    auto diff_MeanSquaredDisplacement =
        MeanSquaredDisplacement_[last] - MeanSquaredDisplacement_[last - 1];
    auto diff_t = dt_vec_[last] - dt_vec_[last - 1];
    auto derivative = diff_MeanSquaredDisplacement - diff_t;
    D_ = 0.5 * derivative;
}

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os,
                         const MeanSquaredDisplacement<n, level> &MSD) {
    auto MSD_data = MSD.get_MSD();
    auto t = MSD_data.first;
    auto values = MSD_data.second;
    os << "t = [";
    for (size_type i = 0; i != t.size() - 1; ++i) {
        os << t[i] << " ";
    }
    os << t[t.size() - 1] << "];\nMSD = [";
    for (size_type i = 0; i != values.size() - 1; ++i) {
        os << values[i].get_mean() << " ";
    }
    os << values[values.size() - 1].get_mean() << "];\nstd = [";
    for (size_type i = 0; i != values.size() - 1; ++i) {
        os << values[i].get_standard_deviation() << " ";
    }
    os << values[values.size() - 1].get_mean() << "];";
    return os;
}

} // namespace champion

#endif
