#ifndef CHAMPION_RANDOMNESS_H
#define CHAMPION_RANDOMNESS_H

#include <ctime>
#include <functional>
#include <random>
#include <utility>

#include "set_precision.hpp"

namespace champion {

// Functor design based on Bjarne Stroustrup: The C++ programming language, 4th
// ed. §40.7

class Rand_int {
  public:
    Rand_int(int low, int high)
        : r(std::bind(
              std::uniform_int_distribution<>(low, high),
              std::default_random_engine{static_cast<unsigned int>(time(0))})) {
    }
    Rand_int(std::pair<int, int> lohi = {0, 1})
        : r(std::bind(
              std::uniform_int_distribution<>(lohi.first, lohi.second),
              std::default_random_engine{static_cast<unsigned int>(time(0))})) {
    }

    real operator()() const { return r(); }

  private:
    std::function<int()> r;
};

class Rand_real {
  public:
    Rand_real(real low = 0, real high = 1)
        : r(std::bind(
              std::uniform_real_distribution<>(low, high),
              std::default_random_engine{static_cast<unsigned int>(time(0))})) {
    }
    Rand_real(std::pair<real, real> lohi = {0, 1})
        : r(std::bind(
              std::uniform_real_distribution<>(lohi.first, lohi.second),
              std::default_random_engine{static_cast<unsigned int>(time(0))})) {
    }

    real operator()() const { return r(); }

  private:
    std::function<real()> r;
};

class Normal_distributed {
  public:
    Normal_distributed(real mu, real sigma)
        : r(std::bind(
              std::normal_distribution<>(mu, sigma),
              std::default_random_engine{static_cast<unsigned int>(time(0))})) {
    }

    real operator()() const { return r(); }

  private:
    std::function<real()> r;
};
}

#endif
