#ifndef CHAMPION_STL_IO_HPP
#define CHAMPION_STL_IO_HPP

#include <deque>
#include <list>
#include <map>
#include <ostream>
#include <queue>
#include <tuple>
#include <utility>
#include <vector>

// STL Output

namespace champion {

template <typename T, unsigned N, unsigned Last> struct PrintTuple {
    static void print(std::ostream &os, const T &value) {
        os << std::get<N>(value) << ", ";
        PrintTuple<T, N + 1, Last>::print(os, value);
    }
};

template <typename T, unsigned N> struct PrintTuple<T, N, N> {
    static void print(std::ostream &os, const T &value) {
        os << std::get<N>(value);
    }
};

template <typename... Ts>
std::ostream &operator<<(std::ostream &os, const std::tuple<Ts...> &c) {
    os << "[";
    PrintTuple<std::tuple<Ts...>, 0, sizeof...(Ts) - 1>::print(os, c);
    os << "]";
    return os;
}

template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &os, const std::pair<T1, T2> &p) {
    os << "[" << p.first << ", " << p.second << "]";
    return os;
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const std::deque<T> &c) {
    os << "[";
    for (auto it = &c.front(); it != &c.back(); ++it) {
        os << *it << ", ";
    }
    os << c.back() << "]";
    return os;
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const std::queue<T> &c) {
    os << "[";
    for (auto it = &c.front(); it != &c.back(); ++it) {
        os << *it << ", ";
    }
    os << c.back() << "]";
    return os;
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const std::list<T> &c) {
    os << "[";
    for (auto it = c.begin(); it != --c.end(); ++it) {
        os << *it << ", ";
    }
    os << c.back() << "]";
    return os;
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &c) {
    os << "[";
    if (c.size() == 0) {
        os << "]";
        return os;
    }
    for (auto it = c.begin(); it < c.end() - 1; ++it) {
        os << *it << ", ";
    }
    os << c.back() << "]";
    return os;
}

template <typename T1, typename T2>
std::ostream &operator<<(std::ostream &os, const std::map<T1, T2> &c) {
    os << "[";
    if (c.size() == 0) {
        os << "]";
        return os;
    }
    for (auto it = c.begin(); it != --c.end(); ++it) {
        os << it->second << ", ";
    }
    os << (--c.end())->second << "]";
    return os;
}

} // namespace champion
#endif