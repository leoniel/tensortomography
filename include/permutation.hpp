#ifndef CHAMPION_PERMUTATION_HPP
#define CHAMPION_PERMUTATION_HPP

#include <algorithm>
#include <vector>

#include "algorithm.hpp"
#include "set_precision.hpp"

namespace champion {

// Return vector of all permutations of <vec> where each element of <indices>
// denotes the indices to be mutually permuted. Implemented with the assumption
// that the different sets of elements to be permuted are disjunct.
template <typename T>
std::vector<std::vector<T>>
get_permutations(const std::vector<T> &vec,
                 std::vector<std::vector<size_type>> indices) {
    // Make sure the index lists are sorted
    for (auto index_list : indices) {
        std::sort(index_list.begin(), index_list.end());
    }
    auto permutations = std::vector<std::vector<T>>{vec};
    for (auto &set : indices) {
        auto new_permutations = decltype(permutations){};
        size_type no_perms = 0;
        for (auto &permutation : permutations) {
            new_permutations.push_back(permutation);
            ++no_perms;
            auto set_cpy = set;
            while (std::next_permutation(set_cpy.begin(), set_cpy.end())) {
                new_permutations.push_back(permutation);
                ++no_perms;
                for (size_type i = 0; i != set.size(); ++i) {
                    new_permutations[no_perms - 1][set[i]] =
                        permutation[set_cpy[i]];
                }
            }
        }
        permutations = std::move(new_permutations);
    }
    return permutations;
}

template <typename T, class Predicate>
std::vector<std::vector<T>> permute_equivalent(const std::vector<T> &vec,
                                               const Predicate &equivalent) {
    auto equivalent_indices = get_equivalent_indices(vec, equivalent);
    return get_permutations(vec, equivalent_indices);
}

} // namespace champion
#endif
