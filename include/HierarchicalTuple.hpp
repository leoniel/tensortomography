/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017-2018 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: HierarchicalMap.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2018-01-17
MODIFIED BY:

USAGE:

    auto bt = make_hierarchical_tuple<level_type n, template<level_type> class
T>(); HierarchicalMap<level_type n, template<level_type> class T> bt;

DESCRIPTION:

Returns a tuple of std::map<size_type, T<level_type>> containing
unique_ptrs to objects of type T<level_type>, templated on hierarchical level,
for objects from level 0 to n-1.

Example:

auto ht = make_hierarchical_tuple<3, Body>();
    or
HierarchicalMap<3, Body> bt;


both create and return an empty

std::tuple<std::vector<Body<0>>,
           std::vector<Body<1>>,
           std::vector<Body<2>>>

If a hierarchical tuple is created as:

auto ht = make_hierarchical_tuple<i, T>();

, raw pointer access to the j:th T of level i is given by

auto htij = std::get<i>(ht)[j].get()

\******************************************************************************/

#ifndef CHAMPION_HIERARCHICAL_TUPLE
#define CHAMPION_HIERARCHICAL_TUPLE

#include <tuple>
#include <utility>

#include "set_precision.hpp"

namespace champion {

template <level_type n, template <level_type, level_type> class T,
          level_type... Levels>
std::tuple<T<n, Levels>...>
make_hierarchical_tuple_impl(std::integer_sequence<level_type, Levels...>) {
    return {};
}

template <level_type n, template <level_type, level_type> class T>
auto make_hierarchical_tuple() {
    return make_hierarchical_tuple_impl<n, T>(
        std::make_integer_sequence<level_type, n>{});
};

template <level_type n, template <level_type, level_type> class T>
using HierarchicalTuple = decltype(make_hierarchical_tuple<n, T>());

} // namespace champion

#endif
