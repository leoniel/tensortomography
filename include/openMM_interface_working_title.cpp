#include <iostream>
#include <memory>
#include <string>

#include "ObjectRegistry.hpp"
#include "set_precision.hpp"
#include "Dictionary.hpp"
#include "VectorSlice.hpp"
#include "io.hpp"
#include "set_precision.hpp"
#include "units.hpp"


#include "OpenMM.h"



using namespace champion;

int main(int argc, char *argv[]){

    auto Map = reg.get_objects<Body, 0>();

    OpenMM::System&                system       = *new OpenMM::System();
    OpenMM::Integrator&            integrator   = *new OpenMM::VerletIntegrator(0.0);

    for(auto index : Map ){
        double mass = index.second.type().get_mass();
        system.addParticle(mass);
    }

    OpenMM::NonbondedForce&          nonbond     = *new OpenMM::NonbondedForce();
    OpenMM::HarmonicBondForce&       bondStretch = *new OpenMM::HarmonicBondForce();
    OpenMM::HarmonicAngleForce&      bondBend    = *new OpenMM::HarmonicAngleForce();
    OpenMM::PeriodicTorsionForce&    bondTorsion = *new OpenMM::PeriodicTorsionForce();
    OpenMM::PeriodicTorsionForce&    bondImproper= *new OpenMM::PeriodicTorsionForce();

    reg.find_bonds();
    reg.find_bond_angles();
    reg.find_torsions();
    reg.find_improper_torsions();

    double k = 1;
    double equilibrium = 1;

    auto HarmonicBonds = reg.get_objects<Bond,0>();
    auto AngleBonds = reg.get_objects<BondAngle,0>();
    auto Torsions = reg.get_objects<Torsion,0>();
    auto Improper = reg.get_objects<ImproperTorsion, 0>();

    double charge = 1;
    double sigma = 1;
    double epsilon = 1;
    int msize = Map.size();
    for(auto index = 0; index != msize ;++index){

        nonbond.addParticle(charge, sigma, epsilon);

    }

    for(auto index : HarmonicBonds){
        size_type first = index.second[0].body().id();
        size_type second = index.second[1].body().id();
        bondStretch.addBond( first, second, equilibrium, k);
    }

    for(auto index : AngleBonds){
        size_type first = index.second[0].body().id();
        size_type second = index.second[1].body().id();
        size_type third = index.second[2].body().id();
        bondBend.addAngle( first, second, third, equilibrium, k);
    }

    int periodicity = 1;
    for(auto index : Torsions){
        size_type first = index.second[0].body().id();
        size_type second = index.second[1].body().id();
        size_type third = index.second[2].body().id();
        size_type fourth = index.second[3].body().id();
        bondTorsion.addTorsion( first, second, third, fourth, periodicity, equilibrium, k);
    }

    for(auto index : Improper){
        size_type first = index.second[0].body().id();
        size_type second = index.second[1].body().id();
        size_type third = index.second[2].body().id();
        size_type fourth = index.second[3].body().id();
        bondImproper.addTorsion( first, second, third, fourth, periodicity, equilibrium, k);
    }

    system.addForce(&nonbond);
    system.addForce(&bondStretch);
    system.addForce(&bondBend);
    system.addForce(&bondTorsion);
    system.addForce(&bondImproper);

    OpenMM::Context& context = *new OpenMM::Context(system, integrator);
    auto ConfigMap = reg.get_objects<Configuration,0>();
    for(size_type i=0; i!= ConfigMap.size();++i){
        std::cout<<"Hej"<<std::endl;
        auto config = ConfigMap.at(i);
        std::cout<<"found the error"<<std::endl;
        auto confpos = config.get_positions();

        std::vector<OpenMM::Vec3> oMMVec;
        std::vector<OpenMM::Vec3> Vel;

        for(size_type j = 0; j!=confpos.size();++j){
            auto point = confpos[j];
            auto x = point[0].value(), y = point[1].value(), z = point[2].value();
            auto oMMpoint = OpenMM::Vec3(x,y,z);
            oMMVec.push_back(oMMpoint);
            Vel.push_back(OpenMM::Vec3(0,0,0));

        }

        context.setPositions(oMMVec);
        context.setVelocities(Vel);
    }

    context.setTime(0.0);

    OpenMM::State state = context.getState(OpenMM::State::Forces,false);
    std::vector<OpenMM::Vec3> forcess = state.getForces();

    for(auto i = 0; i!=2;++i){
        for(auto j = 0; j!=3;++j){
            std::cout<<forcess[i][j]<<' ';
        } std::cout<<std::endl;
    }
    return 0;
}
