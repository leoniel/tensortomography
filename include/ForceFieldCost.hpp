/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2018 Rasmus Andersson and Fabian Årén

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: ForceField<n, level>Cost.hpp

AUTHOR: Rasmus Andersson & Fabian Årén
MODIFIED BY: Rasmus Andersson 2018-08

DESCRIPTION:

A template of functions to preform an Evolutionary Algorithm.


\******************************************************************************/

#ifndef CHAMPION_FORCE_FIELDCOST_HPP
#define CHAMPION_FORCE_FIELDCOST_HPP

#include <list>
#include <vector>

#include "CostFunction.hpp"
#include "Vector.hpp"
#include "VectorSlice.hpp"
#include "algorithm.hpp"
#include "forces.hpp"
#include "statistics.hpp"

namespace champion {

template <level_type n, level_type level> class ForceFieldCost {
  public:
    // Constructor
    ForceFieldCost(const ObjectRegistry<n> &reg,
                   std::vector<size_type> trajectory_indices,
                   std::vector<size_type> param_types, double force_tol,
                   ForceField<n, level> ref_force_field = {})
        : reg_{&reg}, trajectory_indices_{trajectory_indices}, time_indices_{},
          no_snapshots_{0}, force_field_{reg, param_types},
          ref_force_field_{ref_force_field}, force_tol_{force_tol},
          ref_forces_{}, trial_forces_{} {
        auto no_trajectories = trajectory_indices.size();
        for (size_type i = 0; i != no_trajectories; ++i) {
            no_snapshots_ +=
                reg_->template get_object<Trajectory, 0>(trajectory_indices[i])
                    .size();
        }
    }

    size_type size() const { return no_snapshots_; }

    // Call operator (evaluate costs)
    real operator()(VectorSlice<double> force_field,
                    std::vector<size_type> snapshots = {},
                    bool print_debug = false) {

        // Set trial force field to passed parameter values
        force_field_.set_values(force_field);
        auto values = force_field_.get_values();
        // Update given vector slice values to fulfill force field requirements
        for (size_type i = 0; i != force_field.size(); ++i) {
            force_field[i] = values[i];
        }

        // If no snapshot indices were sent, do a full cost calculation
        if (snapshots.size() == 0) {
            snapshots.resize(this->size());
            std::iota(snapshots.begin(), snapshots.end(), 0);
        }

        // Convert the vector of flattened indices into (trajectory, time) pairs
        auto trajectories = reg_->template get_objects<Trajectory, 0>();
        auto no_trajectories = trajectory_indices_.size();
        time_indices_ = {};
        for (size_type i = 0; i != no_trajectories; ++i) {
            time_indices_.push_back({});
        }
        // Make sure the snapshots are sorted as the algorithm depends on it
        std::sort(snapshots.begin(), snapshots.end());
        size_type index = 0;
        size_type t = 0;
        for (size_type ii = 0; ii != no_trajectories; ++ii) {
            size_type i = trajectory_indices_[ii];
            for (size_type j = 0; j != trajectories[i].size(); ++j) {
                if (snapshots[index] == t) {
                    time_indices_[ii].push_back(j);
                    ++index;
                }
                ++t;
            }
        }

        // Build vector of reference and trial forces for selected trajectories
        // and timesteps

        ref_forces_ = {};
        if (ref_force_field_.size() > 0) {

            for (size_type ii = 0; ii != no_trajectories; ++ii) {
                auto i = trajectory_indices_[ii];
                for (size_type jj = 0; jj != time_indices_[ii].size(); ++jj) {
                    auto j = time_indices_[ii][jj];
                    const auto &forces = compute_forces(
                        *reg_, i, j, ref_force_field_, force_tol_, print_debug);
                    ref_forces_.insert(ref_forces_.end(), forces.begin(),
                                       forces.end());
                }
            }
        } else {
            for (size_type ii = 0; ii != no_trajectories; ++ii) {
                auto i = trajectory_indices_[ii];
                for (size_type jj = 0; jj != time_indices_[ii].size(); ++jj) {
                    auto j = time_indices_[ii][jj];
                    const auto &forces = trajectories[i].get_forces(j);
                    ref_forces_.insert(ref_forces_.end(), forces.begin(),
                                       forces.end());
                }
            }
        }

        trial_forces_ = {};

        for (size_type ii = 0; ii != no_trajectories; ++ii) {
            auto i = trajectory_indices_[ii];
            for (size_type jj = 0; jj != time_indices_[ii].size(); ++jj) {
                auto j = time_indices_[ii][jj];
                const auto &forces = compute_forces(*reg_, i, j, force_field_,
                                                    force_tol_, print_debug);
                trial_forces_.insert(trial_forces_.end(), forces.begin(),
                                     forces.end());
            }
        }

        auto N = ref_forces_.size();
        std::vector<double> dir_costs(N, 0);
        std::vector<double> mag_costs(N, 0);
        std::list<double> vec_costs{};
        for (size_type i = 0; i != N; ++i) {
            dir_costs[i] = std::tan(
                units::pi / 4 *
                (1 - direction_cosine(ref_forces_[i], trial_forces_[i])));
            mag_costs[i] =
                std::pow(std::log(squared_magnitude(ref_forces_[i]) /
                                  squared_magnitude(trial_forces_[i])),
                         2);
            auto vec_cost =
                std::sqrt(squared_magnitude(ref_forces_[i] - trial_forces_[i]) /
                          squared_magnitude(ref_forces_[i]));
            auto it = vec_costs.begin();
            while (*it < vec_cost && it != vec_costs.end()) {
                ++it;
            }
            vec_costs.emplace(it, vec_cost);
        }
        auto mean_dir_cost = mean(dir_costs);
        auto mean_mag_cost = mean(mag_costs);
        auto cost = mean_dir_cost + mean_mag_cost;
        min_err_ = vec_costs.front();
        max_err_ = vec_costs.back();
        mean_err_ = 0;
        bool N_even = (N % 2 == 0);
        auto half_it = vec_costs.begin();
        for (size_type i = 0; i < N / 2; ++i) {
            ++half_it;
        }
        if (N_even) {
            median_err_ = (*half_it + *(half_it++)) / 2;
        } else {
            median_err_ = *half_it;
        }
        for (auto it = vec_costs.begin(); it != vec_costs.end(); ++it) {
            mean_err_ += *it / N;
        }

        return cost;
    }

    double min_error() const { return min_err_; }
    double max_error() const { return max_err_; }
    double mean_error() const { return mean_err_; }
    double median_error() const { return median_err_; }

  private:
    // Private data members
    const ObjectRegistry<n> *reg_;
    std::vector<size_type> trajectory_indices_;
    std::vector<std::vector<size_type>> time_indices_;
    size_type no_snapshots_;
    ForceField<n, level> force_field_;
    ForceField<n, level> ref_force_field_;
    double force_tol_;
    double min_err_;
    double mean_err_;
    double median_err_;
    double max_err_;
    std::vector<Vector<real>> ref_forces_;
    std::vector<Vector<real>> trial_forces_;
};

} // namespace champion

#endif
