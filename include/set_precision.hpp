/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  _  \ /| | // __ \  /|   \    /| |
 //  /  \_#| |  |#| |#| |  |#| |#|   \/   |#| |# \ \#| |// / \\ \|#| |\ \  |#| |
|#| |    |#| |__|#| |#| |__|#| |#| |\  /| |#| |# | |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| |_ / /#| |#| | |#| |#| |\#\ \ #| |
|#| |   __#| |  |#| |#| |  |#| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |    |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: set_precision.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:

Sets compile time settings for numerical precision and representation of types
used across the CHAMPION library.

\******************************************************************************/

#ifndef CHAMPION_SET_PRECISION_HPP
#define CHAMPION_SET_PRECISION_HPP

#include <cstddef>
#include <limits>

namespace champion {

// Set precision for numerical computations
using real = double;

// Convenience alias for std::size_t (because it is ugly as s**t)
using size_type = std::size_t;

// Set type for hierarchical levels
using level_type = short;

// Global constants
constexpr long double pi = 3.14159265359;

// Convenience aliases
auto s_max = std::numeric_limits<size_type>::max();
auto infinity = std::numeric_limits<real>::infinity();
auto epsilon = std::numeric_limits<real>::lowest();

struct defaults {
    real delta_theta = 1e-3;
} defaults;

int MPI_root = 0;
int MPI_rank = 0;
int MPI_size = 1;

void champion_initialize(int argc, char *argv[]) {
#ifdef MPI_VERSION
    // Initialize MPI
    int initialized = 0;
    MPI_Initialized(&initialized);
    if (!initialized) {
        MPI_Init(&argc, &argv);
        MPI_Comm_size(MPI_COMM_WORLD, &MPI_size);
        MPI_Comm_rank(MPI_COMM_WORLD, &MPI_rank);
    }
#endif
}

void champion_finalize() {
#ifdef MPI_VERSION
    int initialized = 0;
    MPI_Initialized(&initialized);
    if (initialized) {
        MPI_Finalize();
    }
#endif
}

} // namespace champion

#endif
