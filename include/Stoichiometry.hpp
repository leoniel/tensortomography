#ifndef CHAMPION_STOICHIOMETRY_HPP
#define CHAMPION_STOICHIOMETRY_HPP

#include <memory>
#include <numeric>
#include <string>
#include <valarray>
#include <vector>

#include "ObjectIndex.hpp"
#include "set_precision.hpp"

namespace champion {

// Forward declarations
template <level_type n> class ObjectRegistry;
template <level_type n, level_type level> class BodyType;

template <level_type n, level_type level> class Stoichiometry {
  public:
    // Public member functions
    // Construct from object registry, species and relative number densities
    Stoichiometry(const ObjectRegistry<n> &reg,
                  std::vector<ObjectIndex<BodyType, n, level>> species,
                  std::valarray<size_type> relative_amount)
        : reg_{&reg}, species_{species}, relative_amount_{relative_amount} {}
    size_type size() const { return species_.size(); }
    const BodyType<n, level> &operator[](size_type i) const {
        return reg_->get_object(species_[i]);
    }
    size_type relative_amount(size_type i) const { return relative_amount_[i]; }
    ObjectIndex<BodyType, n, level> get_index(size_type i) const {
        return species_[i];
    }
    size_type bodies_per_formula_unit() const {
        size_type sum = 0;
        for (size_type i = 0; i != size(); ++i) {
            sum += relative_amount_[i] *
                   reg_->template get_object(species_[i]).count_atoms();
        }
        return sum;
    }

  private:
    // Private data members
    const ObjectRegistry<n> *reg_;
    const std::vector<ObjectIndex<BodyType, n, level>> species_;
    const std::valarray<size_type> relative_amount_;
};

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os, const Stoichiometry<n, level> &S) {
    os << "Stoichiometry: {";
    for (size_type i = 0; i != S.size() - 1; ++i) {
        os << S.relative_amount(i) << " ";
        print_brief(os, S[i]) << ", ";
    }
    auto i = S.size() - 1;
    os << S.relative_amount(i) << " ";
    print_brief(os, S[i]) << "}";
    return os;
}

// Compute the number of formula units and check that they match
// an integral number of stoichiometric units
template <level_type n, level_type level>
size_type compute_no_units(size_type no_atoms,
                           const Stoichiometry<n, level> &S) {
    size_type atoms_per_formula = S.bodies_per_formula_unit();
    size_type no_units = no_atoms / atoms_per_formula;
    if (no_units * atoms_per_formula != no_atoms) {
        throw std::runtime_error{
            "Stoichiometry: " + std::to_string(no_atoms) +
            " is not an integral number of formula units of the "
            "given stoichiometry "};
    }
    return no_units;
}

} // namespace champion

#endif
