#ifndef CHAMPION_BOX_H
#define CHAMPION_BOX_H

#include <cassert>
#include <iostream>
#include <utility>

#include "set_precision.hpp"

namespace champion {

// Forward declarations
struct Box;
std::ostream &operator<<(std::ostream &os, const Box &b);

// A cubic box with periodic boundary conditions in which Points live
struct Box {
    using limits = std::numeric_limits<real>;
    using pair = std::pair<real, real>;
    using iterator = pair *;
    using const_iterator = const pair *;
    // Default constructor (infinite box)
    Box()
        : x_bounds{limits::lowest(), limits::max()},
          y_bounds{limits::lowest(), limits::max()}, z_bounds{limits::lowest(),
                                                              limits::max()} {}
    // Construct from components
    Box(real x_min, real x_max, real y_min, real y_max, real z_min, real z_max)
        : x_bounds{x_min, x_max}, y_bounds{y_min, y_max}, z_bounds{z_min,
                                                                   z_max} {}
    // Construct from pairs of limits
    Box(pair xb, pair yb, pair zb) : x_bounds{xb}, y_bounds{yb}, z_bounds{zb} {}

    // Access
    inline size_type size() const { return 3; }
    inline iterator begin() { return &x_bounds; }
    inline iterator end() { return &z_bounds + 1; }
    inline const_iterator cbegin() const { return &x_bounds; }
    inline const_iterator cend() const { return &z_bounds + 1; }
    inline const_iterator begin() const { return cbegin(); }
    inline const_iterator end() const { return cend(); }
    inline pair &operator[](size_type i) { return *(begin() + i); }
    inline pair operator[](size_type i) const { return *(cbegin() + i); }

    // Public data members
    pair x_bounds;
    pair y_bounds;
    pair z_bounds;
};

bool operator==(const Box &b1, const Box &b2) {
    for (size_type i = 0; i != b1.size(); ++i) {
        if (b1[i] != b2[i]) {
            return false;
        }
    }
    return true;
}
bool operator!=(const Box &b1, const Box &b2) { return b1 == b2 ? 0 : 1; }

std::ostream &operator<<(std::ostream &os, const Box &b) {
    os << "Box: {x = {" << b.x_bounds.first << ", " << b.x_bounds.second
       << "}, "
       << ", y = {" << b.y_bounds.first << ", " << b.y_bounds.second << "}, "
       << ", z = " << b.z_bounds.first << ", " << b.z_bounds.second << "}";
    return os;
}
}

#endif
