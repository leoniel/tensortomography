#ifndef CALCULUS_H
#define CALCULUS_H

#include <vector>

#include "set_precision.hpp"

namespace champion {

// Differentiates object stored in [beg_y, end_y) wrt the object stored in
// [beg_x, beg_x + end_y - beg_y) and returns a vector of the type of elements
// in [beg_y, end_y)
  template <class RanIt1, class RanIt2, class T = typename std::iterator_traits<RanIt1>::value_type>
std::vector<T> derivative(RanIt1 beg_y, RanIt1 end_y, RanIt2 beg_x) {
    size_type size = end_y - beg_y;
    auto res = std::vector<T>(size);
    res[0] = (beg_y[1] - beg_y[0]) / (beg_x[1] - beg_x[0]);
    res[size - 1] = (beg_y[size - 1] - beg_y[size - 2]) /
                    (beg_x[size - 1] - beg_x[size - 2]);
    for (size_type i = 1; i != size - 1; ++i) {
        res[i] = (beg_y[i + 1] - beg_y[i - 1]) / (beg_x[i + 1] - beg_x[i - 1]);
    }
    return res;
}

  template <class y_type, class x_type>
  inline std::vector<y_type> derivative(std::vector<y_type> y, std::vector<x_type> x) {
    return derivative(y.begin(), y.end(), x.begin());
  }
  
}

#endif
