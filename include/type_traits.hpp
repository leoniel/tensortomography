#ifndef CHAMPION_TYPE_TRAITS_HPP
#define CHAMPION_TYPE_TRAITS_HPP

#include <iterator>
#include <type_traits>

#include "choose.hpp"

namespace champion {

// Primary template (instantiated if the below definition is SFINAE disabled)
template <typename T, typename Enable = void> struct is_iterator {
    static constexpr bool value = false;
};

// Specialization enabled if T is an iterator type
template <typename T>
struct is_iterator<
    T, typename std::enable_if<(
           std::is_base_of<
               std::input_iterator_tag,
               typename std::iterator_traits<T>::iterator_category>::value ||
           std::is_same<std::output_iterator_tag,
                        typename std::iterator_traits<T>::iterator_category>::
               value)>::type> {
    static constexpr bool value = true;
};

// Convenience wrapper
template <typename T> constexpr bool Is_iterator = is_iterator<T>::value;

// Primary template (instantiated if the below definition is SFINAE disabled)
template <typename T, typename _ = void>
struct is_container : std::false_type {};

// Specialization enabled if T is a container
template <typename T>
struct is_container<T,
                    Choose_type<false,
                                std::void_t<decltype(std::declval<T>().begin()),
                                            decltype(std::declval<T>().end())>,
                                void>> : std::true_type {};

// Convenience wrapper
template <typename T> constexpr bool Is_container = is_container<T>::value;

template <typename T> using is_number = std::is_arithmetic<T>;
template <typename T> constexpr bool Is_number = std::is_arithmetic<T>::value;

} // namespace champion

#endif