#ifndef CHAMPION_IO_HPP
#define CHAMPION_IO_HPP

#include <iostream>
#include <limits>
#include <list>
#include <memory>
#include <queue>
#include <sstream>
#include <vector>

#include "Dictionary.hpp"
#include "ObjectRegistry.hpp"
#include "Stoichiometry.hpp"
// #include "Trajectory.hpp"
#include "set_precision.hpp"
#include "statistics.hpp"
#include "stl_io.hpp"
#include "units.hpp"

namespace champion {

// Forward declarations
template <level_type n, level_type level> class Body;
template <level_type n, level_type level> class Configuration;

template <typename... Ts>
std::ostream &operator<<(std::ostream &os, const std::tuple<Ts...> &c);

template <typename T>
std::ostream &print_brief(std::ostream &os, const std::vector<T> &c) {
    return os << c;
}

template <typename T>
std::ostream &print_full(std::ostream &os, const std::vector<T> &c) {
    return os << c;
}

template <typename T>
std::ostream &print_recursive(std::ostream &os, const std::vector<T> &c) {
    return os << c;
}

template <typename T> std::ostream &print_brief(std::ostream &os, const T &c) {
    return os << c;
}

template <typename T> std::ostream &print_full(std::ostream &os, const T &c) {
    return os << c;
}

template <typename T>
std::ostream &print_recursive(std::ostream &os, const T &c) {
    return os << c;
}

// Input

std::string read_string(std::string key, const Dictionary &dict) {
    return dict.get_value(key).str();
}

bool read_bool(std::string key, const Dictionary &dict,
               bool default_val = false) {
    auto boolstr = read_string(key, dict);
    if (boolstr == "true") {
        return true;
    }
    if (boolstr == "false") {
        return false;
    }
    if (boolstr == "") {
        return default_val;
    }
    throw std::runtime_error{
        "champion::read_bool(): Dictionary entry with key " + key +
        " has non-boolean value."};
}

int read_int(std::string key, const Dictionary &dict) {
    return std::stoi(dict.get_value(key).str());
}

size_type read_size_type(std::string key, const Dictionary &dict) {
    return std::stoull(dict.get_value(key).str());
}

template <typename T>
std::vector<T> read_vector(const std::string &key, const Dictionary &dict) {
    auto vec = std::vector<T>{};
    auto entry = dict.get_value(key);
    auto values = entry.partition();
    for (auto value_dict : values) {
        auto val_str = value_dict.str();
        auto stream = std::stringstream{val_str};
        auto value = T{};
        stream >> value;
        vec.push_back(value);
    }
    return vec;
}

template <typename T>
T read_value_or_default(
    const std::string &key, const Dictionary &dict, T default_value = 0,
    std::function<real(real, std::string)> convert_func =
        [](real value, std::string str) { return value; }) {
    auto entry = dict.get_value(key).str();
    if (entry == "") {
        return default_value;
    }
    auto stream = std::stringstream{entry};
    real value = 0;
    std::string unit;
    stream >> value >> unit;
    value = convert_func(value, unit);
    return static_cast<T>(value);
}

real read_single_value(
    const std::string &key, const Dictionary &dict,
    std::function<real(real, std::string)> convert_func =
        [](real value, std::string str) { return value; }) {
    auto entry = dict.get_value(key).str();
    if (entry == "") {
        throw std::runtime_error{
            std::string{"CHAMPION::read_single_value: no match found for \""} +
            key + "\" in dictionary."};
    }
    auto stream = std::stringstream{entry};
    real value = 0;
    std::string unit;
    stream >> value >> unit;
    value = convert_func(value, unit);
    return value;
}

real read_single_value(
    const std::string &s,
    std::function<real(real, std::string)> convert_func =
        [](real value, std::string str) { return value; }) {
    auto stream = std::stringstream{s};
    real value;
    std::string unit;
    stream >> value >> unit;
    value = convert_func(value, unit);
    return value;
}

template <level_type n>
void read_body_types(ObjectRegistry<n> &reg, const Dictionary &dict) {
    // Create all the element types specified in config
    auto element_map = std::map<std::string, ObjectIndex<BodyType, n, 0>>{};
    for (auto e : dict.get_value("elements").partition()) {
        auto name = e.get_key();
        auto mass_str = e.get_value(name).get_value("mass").str();
        auto mass_stream = std::stringstream{mass_str};
        real mass = 0;
        std::string mass_unit;
        mass_stream >> mass;
        mass_stream >> mass_unit;
        mass = convert_mass_units(mass, mass_unit);
        auto r_str = e.get_value(name).get_value("radius").str();
        auto r_stream = std::stringstream{r_str};
        real radius = 0;
        std::string r_unit;
        r_stream >> radius;
        r_stream >> r_unit;
        radius = convert_length_units(radius, r_unit);
        auto index = reg.template make_body_type<0>(name, mass, radius);
        element_map[name] = index;
    }
    // Create molecule types
    for (auto m : dict.get_value("molecules").partition()) {
        // Get name
        auto name = m.get_key();
        // Get elements
        auto elements = m.get_value(name).get_value("elements").partition();
        auto species = std::vector<ObjectIndex<BodyType, n, 0>>{};
        for (auto e : elements) {
            species.push_back(element_map[e.str()]);
        }
        // Get bond graph
        auto bonds = m.get_value(name).get_value("bonds").partition();
        auto edges = std::vector<Edge>{};
        for (auto b : bonds) {
            auto edge = b.partition();
            edges.push_back(
                {std::stoul(edge[0].str()), std::stoul(edge[1].str())});
        }
        auto graph = Graph{species, edges};
        // Get prototype structure
        auto structure = m.get_value(name).get_value("structure");
        auto unit = structure.get_value("unit");
        auto xyz = structure.get_value("xyz").partition();
        auto coordinates = std::vector<Vector<real>>{};
        for (auto it = xyz.begin(); it != xyz.end();) {
            // If coordinates in xyz format, the first entry might be a label
            try {
                std::stold(it->str());
            } catch (std::invalid_argument &) {
                ++it;
            }
            // Read in values and per-value units
            const size_type numdim = 3;
            auto values = std::vector<real>(numdim);
            auto units = std::vector<std::string>(numdim);
            for (size_type i = 0; i != numdim; ++i) {
                auto stream = std::stringstream{it++->str()};
                stream >> values[i] >> units[i];
            }
            // Convert the numerical values to champion default length unit (nm)
            // If no per-value unit was specified, use the default unit for the
            // structure, as defined by keyword unit
            for (size_type i = 0; i != numdim; ++i) {
                if (units[i] == "") {
                    units[i] = unit.str();
                    values[i] = convert_length_units(values[i], units[i]);
                }
            }
            // Add new point to coordinate vector
            coordinates.push_back({values[0], values[1], values[2]});
        }
        if (coordinates.size() == 0) {
            coordinates = std::vector<Vector<real>>(species.size(), {0, 0, 0});
        }
        // Create the new molecule type and add to registry
        reg.template make_body_type<1>(graph, coordinates, name);
    }
}

std::ifstream open_file(const std::string &path) {
    std::ifstream input_file;
    input_file.open(path, std::ios::in);
    if (!input_file) {
        throw std::runtime_error(std::string("No such file ") + path);
    }
    return input_file;
}

std::string file_to_string(std::ifstream &file) {
    std::stringstream ss;
    ss << file.rdbuf();
    return ss.str();
}

std::string file_to_string(const std::string &path) {
    auto file = open_file(path);
    return file_to_string(file);
}

void skip_xyz_step(std::istream &input_stream) {
    std::string line;
    std::getline(input_stream, line);
    std::stringstream ss;
    ss << line;
    size_type no_lines;
    ss >> no_lines;
    if (ss.fail()) {
        throw std::runtime_error{std::string{"Format error in xyz file "} +
                                 ". Must start with the number of atoms.\n"};
    } else {
        for (size_type i = 0; i < no_lines + 1; ++i) {
            input_stream.ignore(std::numeric_limits<std::streamsize>::max(),
                                '\n');
        }
    }
}

template <level_type n>
ObjectIndex<Configuration, n, 0>
read_xyz(ObjectRegistry<n> &reg, std::istream &input_stream,
         const Dictionary &read_xyz_dict,
         std::vector<ObjectIndex<Body, n, 0>> *atoms = nullptr) {
    std::stringstream ss;
    // For convenient reading line-by-line and word-by-word
    std::string line, word;
    // Read in the number of atoms in geometry
    size_type no_atoms = 0;
    std::getline(input_stream, line);
    ss.str(line);
    ss >> no_atoms;
    ss.clear();
    if (input_stream.fail()) {
        throw std::runtime_error{std::string{"Format error in xyz file "} +
                                 ". Must start with the number of atoms.\n"};
    }
    if (atoms != nullptr && !atoms->empty() && atoms->size() != no_atoms) {
        throw std::runtime_error{
            "CHAMPION::read_xyz(): List of atoms must be either empty or the "
            "same length as the list of coordinates in the .xyz file."};
    }
    // Get the size of the periodic box
    bool dimensions_set = false;
    std::pair<real, real> x_bounds, y_bounds, z_bounds;
    // First, try to read it from the second line of the xyz file
    std::getline(input_stream, line);
    ss.str(line);
    ss >> word;
    ss.clear();
    if (word == "Periodic") {
        // Skip past "box:", "x", "=" and read in "(<x_low>"
        ss >> word >> word >> word >> word;
        ss.clear();
        // Remove the parenthesis
        word = std::string{word.begin() + 1, word.end()};
        real x_low = std::stod(word);
        real x_high = 0;
        // Skip past "A, " and read in "<x_high>"
        ss >> word >> x_high;
        ss.clear();
        // Set x_bounds
        x_bounds = {x_low * units::A, x_high * units::A};
        // Skip past "A),", "y", "=", and read in "(<y_low>"
        ss >> word >> word >> word >> word;
        ss.clear();
        // Remove the parenthesis
        word = std::string{word.begin() + 1, word.end()};
        real y_low = std::stod(word);
        real y_high = 0;
        // Skip past "A, " and read in "<y_high>"
        ss >> word >> y_high;
        ss.clear();
        // Set y_bounds
        y_bounds = {y_low * units::A, y_high * units::A};
        // Skip past "A),", "z", "=", and read in "(<z_low>"
        ss >> word >> word >> word >> word;
        ss.clear();
        // Remove the parenthesis
        word = std::string{word.begin() + 1, word.end()};
        real z_low = std::stod(word);
        real z_high = 0;
        // Skip past "A, " and read in "<z_high>"
        ss >> word >> z_high;
        ss.clear();
        // Set y_bounds
        z_bounds = {z_low * units::A, z_high * units::A};
        dimensions_set = true;
    } else {
        ss.str(line);
        // Read in box dimensions from read_xyz dict if given
        auto box_dim_dict = read_xyz_dict.get_value("box_dimensions");
        if (box_dim_dict.str() != "") {
            auto dims = box_dim_dict.partition();
            double x_min, x_max, y_min, y_max, z_min, z_max;
            std::vector<Dictionary> x_dims, y_dims, z_dims;
            switch (dims.size()) {
            case 1:
                x_max = read_single_value(dims[0].str(), convert_length_units);
                x_bounds = {0, x_max};
                y_bounds = {0, x_max};
                z_bounds = {0, x_max};
                dimensions_set = true;
                break;
            case 2:
                x_min = read_single_value(dims[0].str(), convert_length_units);
                x_max = read_single_value(dims[1].str(), convert_length_units);
                x_bounds = {x_min, x_max};
                y_bounds = {x_min, x_max};
                z_bounds = {x_min, x_max};
                dimensions_set = true;
                break;
            case 3:
                x_dims = dims[0].partition();
                y_dims = dims[1].partition();
                z_dims = dims[2].partition();
                if (x_dims.size() == 1 && y_dims.size() == 1 &&
                    z_dims.size() == 1) {
                    x_max =
                        read_single_value(dims[0].str(), convert_length_units);
                    y_max =
                        read_single_value(dims[1].str(), convert_length_units);
                    z_max =
                        read_single_value(dims[2].str(), convert_length_units);
                    x_bounds = {0, x_max};
                    y_bounds = {0, y_max};
                    z_bounds = {0, z_max};
                    dimensions_set = true;
                } else if (x_dims.size() == 2 && y_dims.size() == 2 &&
                           z_dims.size() == 2) {
                    x_max = read_single_value(x_dims[0].str(),
                                              convert_length_units);
                    x_max = read_single_value(x_dims[1].str(),
                                              convert_length_units);
                    y_max = read_single_value(y_dims[0].str(),
                                              convert_length_units);
                    y_max = read_single_value(y_dims[1].str(),
                                              convert_length_units);
                    z_max = read_single_value(z_dims[0].str(),
                                              convert_length_units);
                    z_max = read_single_value(z_dims[1].str(),
                                              convert_length_units);
                    x_bounds = {x_min, x_max};
                    y_bounds = {y_min, y_max};
                    z_bounds = {z_min, z_max};
                    dimensions_set = true;
                } else {
                    throw std::runtime_error{
                        std::string{
                            "CHAMPION::read_xyz(): Inconsistent format for "
                            "box bounds: "} +
                        box_dim_dict.str()};
                }
                break;
            default:
                throw std::runtime_error{
                    std::string{"CHAMPION::read_xyz(): Erroneous format for "
                                "box bounds: "} +
                    box_dim_dict.str()};
            }
        } else {
            // Keep box "infinite" for now, make educated guess later
            x_bounds = {std::numeric_limits<real>::lowest(),
                        std::numeric_limits<real>::max()};
            y_bounds = {std::numeric_limits<real>::lowest(),
                        std::numeric_limits<real>::max()};
            z_bounds = {std::numeric_limits<real>::lowest(),
                        std::numeric_limits<real>::max()};
        }
    }
    // Create vectors for storing geometry
    bool create_atoms = (atoms == nullptr || atoms->empty());
    bool return_atoms = (atoms != nullptr);
    auto atom_vec = std::vector<ObjectIndex<Body, n, 0>>{};
    std::vector<Point<real>> coordinates(no_atoms);
    {
        // Read in atoms and coordinates
        auto lines = std::vector<std::string>(no_atoms, "");
        for (size_type i = 0; i < no_atoms; ++i) {
            std::getline(input_stream, lines[i]);
        }
        auto names = std::vector<std::string>(no_atoms, "");
        auto x = std::vector<real>(no_atoms, 0);
        auto y = std::vector<real>(no_atoms, 0);
        auto z = std::vector<real>(no_atoms, 0);
#pragma omp parallel for
        for (size_type i = 0; i < no_atoms; ++i) {
            std::istringstream is{lines[i]};
            is >> names[i] >> x[i] >> y[i] >> z[i];
        }
        for (size_type i = 0; i < no_atoms; ++i) {
            if (create_atoms) {
                bool found_type = false;
                for (const auto &el : reg.template get_objects<BodyType, 0>()) {
                    const auto &atom_type = el.second;
                    if (atom_type.get_name() == names[i]) {
                        found_type = true;
                        auto atom =
                            reg.template make_body<0>(atom_type.get_id());
                        atom_vec.push_back(atom);
                        break;
                    }
                }
                if (!found_type) {
                    throw std::runtime_error{
                        std::string{
                            "CHAMPION::read_xyz(): Unknown atom type \""} +
                        names[i] + "\"."};
                }
            } else {
                if (names[i] != reg.get_object((*atoms)[i]).type().get_name()) {
                    throw std::runtime_error{"CHAMPION::read_xyz(): .xyz atom "
                                             "type name does not match "
                                             "type of existing atom."};
                }
            }
        }
        for (size_type i = 0; i < no_atoms; ++i) {
            coordinates[i] =
                Point<real>{x_bounds,        y_bounds,        z_bounds,
                            x[i] * units::A, y[i] * units::A, z[i] * units::A};
        }
    }
    if (!dimensions_set) {
        // Make educated guess on the box dimensions based on the coordinates
        std::vector<real> x_coords, y_coords, z_coords;
        for (size_type i = 0; i != no_atoms; ++i) {
            x_coords.push_back(coordinates[i].x.value());
            y_coords.push_back(coordinates[i].y.value());
            z_coords.push_back(coordinates[i].z.value());
        }
        std::sort(x_coords.begin(), x_coords.end());
        std::sort(y_coords.begin(), y_coords.end());
        std::sort(z_coords.begin(), z_coords.end());
        std::vector<real> x_diffs, y_diffs, z_diffs;
        for (size_type i = 0; i != no_atoms - 1; ++i) {
            x_diffs.push_back(x_coords[i + 1] - x_coords[i]);
            y_diffs.push_back(y_coords[i + 1] - y_coords[i]);
            z_diffs.push_back(z_coords[i + 1] - z_coords[i]);
        }
        auto delta_x = mean(x_diffs) / 2;
        auto delta_y = mean(y_diffs) / 2;
        auto delta_z = mean(z_diffs) / 2;
        real x_min, x_max, y_min, y_max, z_min, z_max;
        x_min = x_coords[0] - delta_x;
        x_max = x_coords[no_atoms - 1] + delta_x;
        y_min = y_coords[0] - delta_y;
        y_max = y_coords[no_atoms - 1] + delta_y;
        z_min = z_coords[0] - delta_z;
        z_max = z_coords[no_atoms - 1] + delta_z;
        x_bounds = {x_min, x_max};
        y_bounds = {y_min, y_max};
        z_bounds = {z_min, z_max};
        // Update the bounds of all coordinates
        for (size_type i = 0; i != no_atoms; ++i) {
            coordinates[i].set_bounds(x_bounds, y_bounds, z_bounds);
        }
    }
    auto configuration_index = ObjectIndex<Configuration, n, 0>{};
    if (create_atoms && return_atoms) {
        *atoms = std::move(atom_vec);
        configuration_index =
            reg.template make_configuration<0>(*atoms, coordinates);
    } else if (create_atoms && !return_atoms) {
        configuration_index = reg.template make_configuration<0>(
            std::move(atom_vec), coordinates);
    } else {
        configuration_index =
            reg.template make_configuration<0>(*atoms, coordinates);
    }
    return configuration_index;
}

template <level_type n>
ObjectIndex<Configuration, n, 0>
read_xyz(ObjectRegistry<n> &reg, const std::string &xyz_path,
         const Dictionary &config_dict,
         std::vector<ObjectIndex<Body, n, 0>> *atoms = nullptr) {
    // Read in input xyz file
    auto input_file = open_file(xyz_path);
    return read_xyz(reg, input_file, config_dict, atoms);
}

template <level_type n>
ObjectIndex<Trajectory, n, 0>
read_xyz_trajectory(ObjectRegistry<n> &reg, std::istream &input_file,
                    const Dictionary &config_dict, size_type skip = 0,
                    size_type max_timesteps = s_max,
                    std::vector<ObjectIndex<Body, n, 0>> *atoms = nullptr,
                    Trajectory<n, 0> *trajectory = nullptr) {
    for (size_type i = 0; i < skip; ++i) {
        skip_xyz_step(input_file);
    }
    auto configs = std::vector<ObjectIndex<Configuration, n, 0>>{};
    std::vector<real> time_steps{};
    real t = 0;
    real delta_t = read_value_or_default<real>("timestep", config_dict, 1.0,
                                               convert_time_units);
    size_type step_i = 0;
    // If no atom vector given, initialize one for read_xyz_trajectory to fill
    // to avoid new atoms being created in registry for each timestep
    auto atom_vec = std::vector<ObjectIndex<Body, n, 0>>{};
    if (atoms != nullptr) {
        atom_vec = std::move(*atoms);
    }
    while (!input_file.eof() && ++step_i <= max_timesteps) {
        try {
            configs.push_back(
                read_xyz(reg, input_file, config_dict, &atom_vec));
            time_steps.push_back(t);
            t += delta_t;
        } catch (std::runtime_error &e) {
            break;
        }
    }
    if (atoms != nullptr) {
        *atoms = std::move(atom_vec);
    }
    if (trajectory == nullptr) {
        auto trajectory_index =
            reg.template make_trajectory<0>(time_steps, configs);
        return trajectory_index;
    } else {
        for (size_type i = 0; i != time_steps.size(); ++i) {
            trajectory->add_timestep(time_steps[i], configs[i]);
        }
        return trajectory->get_id();
    }
}

template <level_type n>
ObjectIndex<Trajectory, n, 0> read_xyz_trajectory(
    ObjectRegistry<n> &reg, const std::string &xyz_path,
    const Dictionary &config_dict, size_type skip = 0,
    size_type max_steps = s_max,
    std::vector<ObjectIndex<Body, n, 0>> *atoms = nullptr,
    Trajectory<n, 0> *trajectory = nullptr,
    size_type max_timesteps = std::numeric_limits<size_type>::max()) {
    auto input_file = open_file(xyz_path);
    return read_xyz_trajectory(reg, input_file, config_dict, skip, max_steps,
                               atoms, trajectory);
}

// template <level_type n>
// ObjectIndex<Trajectory, n, 0>
// read_xyz_trajectory(ObjectRegistry<n> &reg, std::istream &input_file,
//                     const Dictionary &config_dict, size_type skip = 0,
//                     size_type max_timesteps = s_max) {
//     return read_xyz_trajectory(reg, input_file, config_dict, skip,
//                                max_timesteps, nullptr, nullptr);
// }

// template <level_type n>
// ObjectIndex<Trajectory, n, 0>
// read_xyz_trajectory(ObjectRegistry<n> &reg, const std::string &xyz_path,
//                     const Dictionary &config_dict, size_type max_timesteps) {
//     std::vector<ObjectIndex<Body, n, 0>> *atoms = nullptr;
//     Trajectory<n, 0> *trajectory = nullptr;
//     return read_xyz_trajectory(reg, xyz_path, config_dict, atoms, trajectory,
//                                max_timesteps);
// }

template <level_type n, level_type level>
Stoichiometry<n, level> read_stoichiometry(ObjectRegistry<n> &reg,
                                           const Dictionary &dict) {
    auto S_vec = dict.get_value("stoichiometry").partition();
    size_type num_species = S_vec.size() / 2;
    auto species = std::vector<ObjectIndex<BodyType, n, level>>(num_species);
    auto rel_amount = std::valarray<size_type>(num_species);
    for (auto it = S_vec.begin(); it != S_vec.end();) {
        auto index = (it - S_vec.begin()) / 2;
        rel_amount[index] = std::stold(it++->str());
        auto name = it++->str();
        bool found_match = false;
        for (auto bt : reg.template get_objects<BodyType, level>()) {
            if (bt.second.name() == name) {
                species[index] = bt.first;
                found_match = true;
                break;
            }
        }
        if (!found_match) {
            throw std::runtime_error{"No matching BodyType<1> with name " +
                                     name + " in ObjectRegistry."};
        }
    }
    return Stoichiometry<n, level>(reg, species, rel_amount);
}

void read_CPMD_trajectory(const Dictionary &dict,
                          std::vector<double> &coordinates,
                          std::vector<double> &velocities,
                          std::vector<double> &forces, double &box_length,
                          double &timestep_length, unsigned &no_atoms,
                          unsigned &no_timesteps) {
    auto key = dict.get_key();
    auto atom_dict = dict.get_value(key).get_value("atoms");
    auto atom_format = atom_dict.get_value("format").str();
    auto atoms_path = atom_dict.get_value("path").str();
    // Open file to read atoms
    std::ifstream atom_file;
    atom_file.open(atoms_path, std::ios::in);
    if (!atom_file) {
        throw std::runtime_error(std::string("No such file \"") + atoms_path +
                                 "\"");
    }
    // Store the element names of the atoms in order
    std::vector<std::string> atom_str_vec;
    // Make sure the format is recognized and appropriate
    if (atom_format == "CPMD_out") {
        std::string word;
        // Read on until the word "ATOMS" is found,
        // and then read 7 more steps to get to the start of atoms
        while (atom_file >> word) {
            if (word == "ATOMS") {
                for (size_type i = 0; i != 7; ++i) {
                    atom_file >> word;
                }
                break;
            }
        }
        // Read in all entries in table, but save only element names
        size_type nr;
        std::string type;
        real x, y, z;
        size_type mbl;
        atom_file >> nr;
        // Read only the first time step
        for (size_type i = 1; i == nr; ++i) {
            atom_file >> type >> x >> y >> z >> mbl >> nr;
            atom_str_vec.push_back(type);
        }
    } else {
        throw std::runtime_error{
            "read_CPMD_trajectory: Uncompatible format for reading elements " +
            atom_format};
    }
    // Compute the number of atoms
    no_atoms = atom_str_vec.size();
    // Read trajectory information
    // Get dictionary for the trajectory
    auto trajectory_dict = dict.get_value(key).get_value("trajectory");
    // Read the size of the simulation box
    box_length =
        read_single_value("box_length", trajectory_dict, convert_length_units);
    // Read the time step length of the simulation
    timestep_length =
        read_single_value("timestep", trajectory_dict, convert_time_units);

    auto traj_path = trajectory_dict.get_value("path").str();
    std::ifstream traj_file;
    traj_file.open(traj_path, std::ios::in);
    if (!traj_file) {
        throw std::runtime_error(std::string("No such file ") + traj_path);
    }
    auto traj_format = trajectory_dict.get_value("format").str();
    if (traj_format == "CPMD_FTRAJECTORY") {
        no_timesteps = 0;
        std::string str = "";
        double x, y, z;
        while (traj_file >> str) {
            traj_file.unget();
            for (size_type i = 0; i != no_atoms; ++i) {
                traj_file >> str;
                if (str[0] == '<') {
                    traj_file >> str >> str >> str >> str;
                }
                traj_file >> x >> y >> z;
                coordinates.push_back(convert_units(x, units::bohr, 1));
                coordinates.push_back(convert_units(y, units::bohr, 1));
                coordinates.push_back(convert_units(z, units::bohr, 1));
                traj_file >> x >> y >> z;
                velocities.push_back(
                    convert_units(x, units::bohr / units::atu, 1));
                velocities.push_back(
                    convert_units(y, units::bohr / units::atu, 1));
                velocities.push_back(
                    convert_units(z, units::bohr / units::atu, 1));
                traj_file >> x >> y >> z;
                forces.push_back(
                    convert_units(x, units::Hartree / units::bohr, 1));
                forces.push_back(
                    convert_units(y, units::Hartree / units::bohr, 1));
                forces.push_back(
                    convert_units(z, units::Hartree / units::bohr, 1));
            }
            ++no_timesteps;
        }
    } else {
        throw std::runtime_error{"Unknown trajectory format " + traj_format};
    }
}



} // namespace champion

#endif
