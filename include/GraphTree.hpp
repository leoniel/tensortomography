/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  _  \ /| | // __ \  /|   \    /| |
 //  /  \_#| |  |#| |#| |  |#| |#|   \/   |#| | \\ \#| |// / \\ \|#| |\ \  |#| |
|#| |    |#| |__|#| |#| |__|#| |#| |\  /| |#| | || |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| |_|/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   __#| |  |#| |#| |  |#| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |    |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: GraphTree.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:

Unstructured tree data structure designed to store Graphs in a sorted manner,
in terms of a tree starting at a particular chosen vertex in the Graph.
Implements a concrete class template GraphTree, templated on the type of vertex,
implementing the Tree interface, and a concrete class GraphTreeNode,
implementing the TreeNode interface. The value() function required by the
TreeNode interface returns the index in the Graph of the corresponding
GraphTreeNode.

\******************************************************************************/

#ifndef CHAMPION_GRAPH_TREE_HPP
#define CHAMPION_GRAPH_TREE_HPP

#include <algorithm>
#include <cassert>
#include <limits>
#include <utility>
#include <vector>

#include "Tree.hpp"
#include "set_precision.hpp"

namespace champion {

/***************************** Class declarations *****************************/

// Forward declarations
template <typename vertex_type> class Graph;
template <typename vertex_type> class GraphTree;

class GraphTreeNode : public TreeNode<size_type> {
  public:
    template <typename vertex_type> friend class GraphTree;
    // Construct from index and parent (default nullptr)
    GraphTreeNode(size_type index, GraphTreeNode *parent = nullptr);
    // Value of current node
    size_type value() const override;
    // Which layer in the tree is this on? (root layer = 0)
    size_type layer() const override;
    // The number of nodes below this
    size_type size() const override;
    // How deep is the deepest tree starting at this node? (depth(leaf) = 0)
    size_type depth() const override;
    // Recursively compute, and set, the depth value for all nodes below this
    size_type compute_depths();
    // Recursively compute, and set, the size of all nodes below this
    // (i.e. # nodes below it)
    size_type compute_sizes();
    // Navigation (returns nullptr if out of bounds)
    // Return pointer to unique parent node
    const TreeNode<size_type> *up() const override;
    TreeNode<size_type> *up() override;
    // Return pointer to leftmost child node
    const TreeNode<size_type> *down() const override;
    TreeNode<size_type> *down() override;
    // Return pointer to next node with the same parent as this
    const TreeNode<size_type> *right() const override;
    TreeNode<size_type> *right() override;
    // Return pointer to previous node with the same parent as this
    const TreeNode<size_type> *left() const override;
    TreeNode<size_type> *left() override;
    // Return vector of pointers to all nodes sharing this' parent
    std::vector<const TreeNode<size_type> *> siblings() const override;
    std::vector<TreeNode<size_type> *> siblings() override;
    // Return vector of pointers to all nodes that have this as a parent
    std::vector<const TreeNode<size_type> *> children() const override;
    std::vector<TreeNode<size_type> *> children() override;
    // Dispatch a visitor
    void accept(ConstNodeVisitor<size_type> &visit) const override;
    void accept(NodeVisitor<size_type> &visit) override;

  private:
    size_type layer_;
    size_type size_;
    size_type depth_;
    size_type index_;
    GraphTreeNode *parent_;
    std::vector<GraphTreeNode *> children_;
};

template <typename vertex_type> class GraphTree : public Tree<size_type> {
  public:
    using iterator = TreeIterator<size_type, false>;
    using const_iterator = TreeIterator<size_type, true>;
    ~GraphTree();
    // Default constructor
    GraphTree() : graph_{}, root_{nullptr}, tail_{nullptr}, size_{0} {}
    // Construct from Graph, seed index and maximum depth to be explored
    // (default unlimited)
    GraphTree(const Graph<vertex_type> &g, size_type i,
              size_type max_depth = std::numeric_limits<size_type>::max());
    // Copy constructor
    GraphTree(const GraphTree<vertex_type> &GT);
    // Move constructor
    GraphTree(GraphTree<vertex_type> &&GT);
    // Copy assignment
    GraphTree<vertex_type> &operator=(const GraphTree<vertex_type> &GT);
    // Move assignment
    GraphTree<vertex_type> &operator=(GraphTree<vertex_type> &&GT);
    // Swap member function
    void swap(GraphTree<vertex_type> &GT);
    // Range based access
    iterator begin() override;
    iterator end() override;
    const_iterator begin() const override;
    const_iterator end() const override;
    const_iterator cbegin() const override;
    const_iterator cend() const override;
    // Return a handle to the root node of the tree
    const TreeNode<size_type> *root() const override;
    TreeNode<size_type> *root() override;
    const TreeNode<size_type> *tail() const override;
    TreeNode<size_type> *tail() override;
    // Return a handle to the graph
    const Graph<vertex_type> &graph() const;
    // Visit all nodes in tree, traverse depth first
    void visit_nodes(ConstNodeVisitor<size_type> &visitor) const override;
    void visit_nodes(NodeVisitor<size_type> &visitor) override;
    std::vector<const GraphTreeNode *> get_backbone() const;
    std::vector<std::vector<GraphTree<vertex_type>>>
    get_sidetrees(std::vector<const GraphTreeNode *> backbone = {}) const;

  private:
    // Private helper functions
    // Release all allocated memory
    void release_memory();

    // Data members
    Graph<vertex_type> graph_;
    GraphTreeNode *root_;
    GraphTreeNode *tail_;
    size_type size_;
};

/********************** Non-member function declarations **********************/

// Functions operating on GraphTreeNodes

/**
Returns -1 if n1 < n2, +1 if n2 < n1 and 0 if they are equal
Compares:
    1) Depth of trees starting at n1 and n2 (deeper compares lower)
    2) Number of downstream nodes (more compares lower)
    3) The value of the vertices corresponding to each node
*/
template <typename vertex_type>
int compare_nodes(const GraphTreeNode &n1, const GraphTreeNode &n2,
                  const Graph<vertex_type> &g1, const Graph<vertex_type> &g2);

// GraphTreeNode has no overloaded logical operators, since the information
// contained in GraphTreeNodes is not sufficient to compare them
// (must know also the vertex values, which are stored in the GraphTree)

// Functions operating on GraphTrees

// Non-member swap function template
template <typename vertex_type>
void swap(GraphTree<vertex_type> &GT1, GraphTree<vertex_type> &GT2) {
    GT1.swap(GT2);
}

/**
Returns -1 if t1 < t2, +1 if t2 < t1 and 0 if they are equal
Assumes tree is sorted so that the leftmost downward path is the deepest
(This is guaranteed by the constructor so should always be the case)
Compares:
   1) Depths (deeper compares lower)
   2) Sizes (i.e. number of nodes, more compares lower)
   3) Vertex values along deepest path (backbone)
   4) If still no difference, creates a 2D array with element [i][j] containing
      the jth side tree adjacent to the ith vertex of the backbone. For each
      backbone vertex, sort the adjacent side trees.
      For each backbone vertex, compare:
           a) the number of adjacent sidetrees
           b) for each sidetree, recursively call this function
   5) If no difference has been found in recursive execution of this function,
      return 0.
*/
template <typename vertex_type>
int compare_trees(const GraphTree<vertex_type> &t1,
                  const GraphTree<vertex_type> &t2);

// Logical operators (implemented in terms of compare_trees)
template <typename vertex_type>
bool operator<(const GraphTree<vertex_type> &t1,
               const GraphTree<vertex_type> &t2);
template <typename vertex_type>
bool operator>(const GraphTree<vertex_type> &t1,
               const GraphTree<vertex_type> &t2);
template <typename vertex_type>
bool operator<=(const GraphTree<vertex_type> &t1,
                const GraphTree<vertex_type> &t2);
template <typename vertex_type>
bool operator>=(const GraphTree<vertex_type> &t1,
                const GraphTree<vertex_type> &t2);
template <typename vertex_type>
bool operator==(const GraphTree<vertex_type> &t1,
                const GraphTree<vertex_type> &t2);
template <typename vertex_type>
bool operator!=(const GraphTree<vertex_type> &t1,
                const GraphTree<vertex_type> &t2);

template <typename vertex_type>
std::vector<std::shared_ptr<GraphTree<vertex_type>>>
find_unique_graphtrees(const Graph<vertex_type> &g, size_type max_depth);

/**************************** Function definitions ****************************/

// GraphTreeNode member functions

GraphTreeNode::GraphTreeNode(size_type index, GraphTreeNode *parent)
    : layer_{0}, index_{index}, parent_{parent}, children_{} {
    auto next = this->parent_;
    while (next) {
        next = next->parent_;
        ++layer_;
    }
}

size_type GraphTreeNode::layer() const { return layer_; }
size_type GraphTreeNode::value() const { return index_; }
size_type GraphTreeNode::size() const { return size_; }
size_type GraphTreeNode::depth() const { return depth_; }

size_type GraphTreeNode::compute_depths() {
    depth_ = 0;
    if (children_.empty()) {
        return depth_;
    }
    for (auto n : children_) {
        depth_ = std::max(depth_, n->compute_depths());
    }
    // Depth is one more than the depth of the deepest child node
    return ++depth_;
}

size_type GraphTreeNode::compute_sizes() {
    size_ = 1;
    for (auto n : children_) {
        size_ += n->compute_sizes();
    }
    return size_;
}

template <typename vertex_type>
typename GraphTree<vertex_type>::iterator GraphTree<vertex_type>::begin() {
    return iterator{root()};
}
template <typename vertex_type>
typename GraphTree<vertex_type>::iterator GraphTree<vertex_type>::end() {
    auto tail_ = tail();
    return iterator{++tail_};
}
template <typename vertex_type>
typename GraphTree<vertex_type>::const_iterator
GraphTree<vertex_type>::begin() const {
    return const_iterator{root()};
}
template <typename vertex_type>
typename GraphTree<vertex_type>::const_iterator
GraphTree<vertex_type>::end() const {
    auto tail_ = tail();
    return const_iterator{++tail_};
}
template <typename vertex_type>
typename GraphTree<vertex_type>::const_iterator
GraphTree<vertex_type>::cbegin() const {
    return const_iterator{root()};
}
template <typename vertex_type>
typename GraphTree<vertex_type>::const_iterator
GraphTree<vertex_type>::cend() const {
    auto tail_ = tail();
    return const_iterator{++tail_};
}

const TreeNode<size_type> *GraphTreeNode::up() const { return parent_; }
TreeNode<size_type> *GraphTreeNode::up() { return parent_; }

const TreeNode<size_type> *GraphTreeNode::down() const {
    if (children_.empty()) {
        return nullptr;
    } else {
        return *children_.begin();
    }
}
TreeNode<size_type> *GraphTreeNode::down() {
    if (children_.empty()) {
        return nullptr;
    } else {
        return *children_.begin();
    }
}

const TreeNode<size_type> *GraphTreeNode::right() const {
    if (!parent_) {
        return nullptr;
    }
    auto next_it =
        std::find(parent_->children_.begin(), parent_->children_.end(), this) +
        1;
    if (next_it == parent_->children_.end()) {
        return nullptr;
    } else {
        return *next_it;
    }
}
TreeNode<size_type> *GraphTreeNode::right() {
    if (!parent_) {
        return nullptr;
    }
    auto next_it =
        std::find(parent_->children_.begin(), parent_->children_.end(), this) +
        1;
    if (next_it == parent_->children_.end()) {
        return nullptr;
    } else {
        return *next_it;
    }
}

const TreeNode<size_type> *GraphTreeNode::left() const {
    auto current_it =
        std::find(parent_->children_.begin(), parent_->children_.end(), this);
    if (current_it == parent_->children_.begin()) {
        return nullptr;
    } else {
        return *(current_it - 1);
    }
}
TreeNode<size_type> *GraphTreeNode::left() {
    auto current_it =
        std::find(parent_->children_.begin(), parent_->children_.end(), this);
    if (current_it == parent_->children_.begin()) {
        return nullptr;
    } else {
        return *(current_it - 1);
    }
}

std::vector<const TreeNode<size_type> *> GraphTreeNode::siblings() const {
    std::vector<const TreeNode<size_type> *> siblings{};
    if (parent_ == nullptr) {
        siblings.push_back(dynamic_cast<const TreeNode<size_type> *>(this));
        return siblings;
    }
    for (auto &c : parent_->children_) {
        siblings.push_back(c);
    }
    return siblings;
}
std::vector<TreeNode<size_type> *> GraphTreeNode::siblings() {
    std::vector<TreeNode<size_type> *> siblings{};
    if (parent_ == nullptr) {
        siblings.push_back(dynamic_cast<TreeNode<size_type> *>(this));
        return siblings;
    }
    for (auto &c : parent_->children_) {
        siblings.push_back(c);
    }
    return siblings;
}

std::vector<const TreeNode<size_type> *> GraphTreeNode::children() const {
    std::vector<const TreeNode<size_type> *> children;
    for (auto &c : children_) {
        children.push_back(c);
    }
    return children;
}
std::vector<TreeNode<size_type> *> GraphTreeNode::children() {
    std::vector<TreeNode<size_type> *> children;
    for (auto &c : children_) {
        children.push_back(c);
    }
    return children;
}

void GraphTreeNode::accept(ConstNodeVisitor<size_type> &visit) const {
    visit.visit(this);
}
void GraphTreeNode::accept(NodeVisitor<size_type> &visit) { visit.visit(this); }

// GraphTree member functions

template <typename vertex_type>
GraphTree<vertex_type>::GraphTree(const Graph<vertex_type> &g, size_type i,
                                  size_type max_depth)
    : graph_{g} {
    size_type depth = 0;
    // Initialize tree by constructing root node
    root_ = new GraphTreeNode{i, nullptr};
    auto all_nodes = std::vector<GraphTreeNode *>{root_};
    auto current_layer = std::vector<GraphTreeNode *>{root_};
    auto next_layer = std::vector<GraphTreeNode *>{};
    // Keep track of nodes already included in tree
    auto discovered = std::vector<size_type>{root_->index_};
    while (!current_layer.empty() && depth < max_depth) {
        for (auto current : current_layer) {
            // Find and sort neighbors
            auto neighbors = g.get_neighbor_indices(current->index_);
            for (auto ni : neighbors) {
                // Construct a new child node for each neighbor not previously
                // discovered
                if (std::find(discovered.begin(), discovered.end(), ni) ==
                    discovered.end()) {
                    auto new_child = new GraphTreeNode{ni, current};
                    all_nodes.push_back(new_child);
                    discovered.push_back(new_child->index_);
                    current->children_.push_back(new_child);
                    next_layer.push_back(new_child);
                }
            }
        }
        current_layer = next_layer;
        next_layer = {};
    }
    size_ = size();
    root_->compute_sizes();
    root_->compute_depths();
    // Sort each list of children so that the tree is kept in a standardized,
    // sorted state with the deepest path the one travelled by recursive
    // down() calls (i.e. the leftmost path is the deepest)
    for (auto n : all_nodes) {
        std::sort(n->children_.begin(), n->children_.end(),
                  [this](const GraphTreeNode *n1p, const GraphTreeNode *n2p) {
                      return (compare_nodes(*n1p, *n2p, this->graph_,
                                            this->graph_) < 0);
                  });
    }
    // Cannot use end() yet, because tail_ has not been set (part of invariant)
    iterator it = begin();
    while (it != iterator(nullptr)) {
        ++it;
    }
    tail_ = dynamic_cast<GraphTreeNode *>((--it).get());
}

// Destructor
template <typename vertex_type> GraphTree<vertex_type>::~GraphTree() {
    release_memory();
}

// Copy constructor
template <typename vertex_type>
GraphTree<vertex_type>::GraphTree(const GraphTree<vertex_type> &GT)
    : GraphTree(GT.graph_, GT.root_->value()) {}

// Copy assignment
template <typename vertex_type>
GraphTree<vertex_type> &GraphTree<vertex_type>::
operator=(const GraphTree<vertex_type> &GT) {
    auto GT_copy = GraphTree<vertex_type>{GT};
    this->swap(GT_copy);
    return *this;
}

// Move constructor
template <typename vertex_type>
GraphTree<vertex_type>::GraphTree(GraphTree<vertex_type> &&GT)
    : graph_{GT.graph_}, root_{GT.root_}, size_{GT.size_} {
    GT.graph_ = {};
    GT.root_ = {};
}

// Move assignment
template <typename vertex_type>
GraphTree<vertex_type> &GraphTree<vertex_type>::
operator=(GraphTree<vertex_type> &&GT) {
    this->swap(GT);
    return *this;
}

template <typename vertex_type>
void GraphTree<vertex_type>::swap(GraphTree<vertex_type> &GT) {
    using std::swap;
    swap(graph_, GT.graph_);
    swap(root_, GT.root_);
    swap(tail_, GT.tail_);
    swap(size_, GT.size_);
}

template <typename vertex_type>
const TreeNode<size_type> *GraphTree<vertex_type>::root() const {
    return root_;
}
template <typename vertex_type>
TreeNode<size_type> *GraphTree<vertex_type>::root() {
    return root_;
}

template <typename vertex_type>
const TreeNode<size_type> *GraphTree<vertex_type>::tail() const {
    return tail_;
}
template <typename vertex_type>
TreeNode<size_type> *GraphTree<vertex_type>::tail() {
    return tail_;
}

template <typename vertex_type>
const Graph<vertex_type> &GraphTree<vertex_type>::graph() const {
    return graph_;
}

template <typename vertex_type>
void GraphTree<vertex_type>::visit_nodes(
    ConstNodeVisitor<size_type> &visitor) const {
    root_->explore_downstream(visitor);
}

template <typename vertex_type>
void GraphTree<vertex_type>::visit_nodes(NodeVisitor<size_type> &visitor) {
    root_->explore_downstream(visitor);
}

template <typename vertex_type>
std::vector<const GraphTreeNode *>
GraphTree<vertex_type>::get_backbone() const {
    auto backbone = std::vector<const GraphTreeNode *>{root_};
    auto current = root();
    while (current->down() != nullptr) {
        current = current->down();
        backbone.push_back(dynamic_cast<const GraphTreeNode *>(current));
    }
    return backbone;
}

template <typename vertex_type>
std::vector<std::vector<GraphTree<vertex_type>>>
GraphTree<vertex_type>::get_sidetrees(
    std::vector<const GraphTreeNode *> backbone) const {
    if (backbone.empty()) {
        backbone = get_backbone();
    }
    auto side_trees = std::vector<std::vector<GraphTree<vertex_type>>>(size_);
    for (size_type i = 0; i != backbone.size(); ++i) {
        auto Ni = backbone[i]->children_.size();
        if (Ni) { // If not on last backbone vertex
            auto st_vertices = std::vector<vertex_type *>();
            // For each child not in the backbone (leftmost child of backbone
            // vertex is guaranteed to be in the backbone)
            for (size_type j = 1; j != Ni; ++j) {
                // Construct side_trees[i][j] by exploring downstream nodes
                auto root = backbone[i]->children_[j];
                auto vis = ConstNodeRecorder<size_type>();
                root->explore_downstream(vis);
                auto nodes = vis.get_nodes();
                auto indices = std::vector<size_type>();
                for (auto n : nodes) {
                    indices.push_back(n->value());
                }
                auto stg = graph_.get_subgraph(indices);
                side_trees[i].push_back(GraphTree<vertex_type>{stg, 0});
            }
            std::sort(side_trees[i].begin(), side_trees[i].end());
        }
    }
    return side_trees;
}

template <typename vertex_type> void GraphTree<vertex_type>::release_memory() {
    if (root_ == nullptr) {
        size_ = 0;
        return;
    }
    auto recorder = NodeRecorder<size_type>{};
    visit_nodes(recorder);
    for (auto node : recorder.get_nodes()) {
        delete (node);
    }
    root_ = nullptr;
    size_ = 0;
}

// Non-member functions

template <typename vertex_type>
int compare_nodes(const GraphTreeNode &n1, const GraphTreeNode &n2,
                  const Graph<vertex_type> &g1, const Graph<vertex_type> &g2) {
    if (n2.depth() < n1.depth()) {
        return -1;
    }
    if (n1.depth() < n2.depth()) {
        return 1;
    }
    if (n2.size() < n1.size()) {
        return -1;
    }
    if (n1.size() < n2.size()) {
        return 1;
    }
    if (g1[n1.value()] < g2[n2.value()]) {
        return -1;
    }
    if (g2[n2.value()] < g1[n1.value()]) {
        return 1;
    }
    return 0;
}

template <typename vertex_type>
int compare_trees(const GraphTree<vertex_type> &t1,
                  const GraphTree<vertex_type> &t2) {
    // Compare depths
    if (t2.depth() < t1.depth()) {
        return -1;
    }
    if (t1.depth() < t2.depth()) {
        return 1;
    }
    // Compare tree sizes (i.e. number of nodes)
    if (t2.size() < t1.size()) {
        return -1;
    }
    if (t1.size() < t2.size()) {
        return 1;
    }
    // Compare backbone lengths
    auto backbone1 = t1.get_backbone();
    auto backbone2 = t2.get_backbone();
    if (backbone2.size() < backbone1.size()) {
        return -1;
    }
    if (backbone1.size() < backbone2.size()) {
        return 1;
    }
    // Compare vertices in backbones
    for (size_type i = 0; i != backbone1.size(); ++i) {
        auto res =
            compare_nodes(*backbone1[i], *backbone2[i], t1.graph(), t2.graph());
        switch (res) {
        case -1:
            return -1;
        case 1:
            return 1;
        }
    }
    // Construct sorted arrays of side trees
    auto side_trees1 = t1.get_sidetrees(backbone1);
    auto side_trees2 = t2.get_sidetrees(backbone2);
    assert(side_trees1.size() == side_trees2.size());
    for (size_type i = 0; i != side_trees1.size(); ++i) {
        // For each backbone vertex, compare number of adjacent side chains
        if (side_trees2[i].size() < side_trees1[i].size()) {
            return -1;
        }
        if (side_trees1[i].size() < side_trees2[i].size()) {
            return 1;
        }
        // Recursively compare each side chain adjacent to backbone vertex i
        for (size_type j = 0; j != side_trees1[i].size(); ++j) {
            auto cmp = compare_trees(side_trees1[i][j], side_trees2[i][j]);
            if (cmp < 0) {
                return -1;
            }
            if (cmp > 0) {
                return 1;
            }
        }
    }
    // If no difference has been found, return 0
    return 0;
}

template <typename vertex_type>
bool operator<(const GraphTree<vertex_type> &t1,
               const GraphTree<vertex_type> &t2) {
    return (compare_trees(t1, t2) < 0);
}

template <typename vertex_type>
bool operator>(const GraphTree<vertex_type> &t1,
               const GraphTree<vertex_type> &t2) {
    return (compare_trees(t1, t2) > 0);
}

template <typename vertex_type>
bool operator<=(const GraphTree<vertex_type> &t1,
                const GraphTree<vertex_type> &t2) {
    return (compare_trees(t1, t2) <= 0);
}

template <typename vertex_type>
bool operator>=(const GraphTree<vertex_type> &t1,
                const GraphTree<vertex_type> &t2) {
    return (compare_trees(t1, t2) >= 0);
}

template <typename vertex_type>
bool operator==(const GraphTree<vertex_type> &t1,
                const GraphTree<vertex_type> &t2) {
    return (compare_trees(t1, t2) == 0);
}
template <typename vertex_type>
bool operator!=(const GraphTree<vertex_type> &t1,
                const GraphTree<vertex_type> &t2) {
    return (compare_trees(t1, t2) != 0);
}

template <typename vertex_type>
std::vector<std::shared_ptr<GraphTree<vertex_type>>>
find_unique_graphtrees(const Graph<vertex_type> &g, size_type max_depth) {
    auto unique_trees = std::vector<std::shared_ptr<GraphTree<vertex_type>>>{};
    for (size_type i = 0; i != g.size(); ++i) {
        auto tree = GraphTree<vertex_type>(g, i, max_depth);
        if (std::find(unique_trees.begin(), unique_trees.end(), tree) ==
            unique_trees.end()) {
            unique_trees.push_back(
                std::make_shared<GraphTree<vertex_type>>(std::move(tree)));
        }
    }
    return unique_trees;
}

template <typename vertex_type>
std::vector<std::shared_ptr<GraphTree<vertex_type>>>
get_graphtree_vertices(const Graph<vertex_type> &g, size_type max_depth) {
    auto vertices = std::vector<std::shared_ptr<GraphTree<vertex_type>>>{};
    auto unique_vertices =
        std::vector<std::shared_ptr<GraphTree<vertex_type>>>{};
    for (size_type i = 0; i != g.size(); ++i) {
        auto tree = GraphTree<vertex_type>(g, i, max_depth);
        auto it = std::find_if(
            vertices.begin(), vertices.end(),
            [&tree](
                typename std::vector<std::shared_ptr<GraphTree<vertex_type>>>::
                    const_iterator it_) { return (**it_ == tree); });
        if (it != vertices.end()) {
        }
    }
}

template <typename vertex_type>
std::ostream &operator<<(std::ostream &os, const GraphTree<vertex_type> &gt) {
    os << "GraphTree: {" << gt.graph() << "}, "
       << dynamic_cast<const Tree<size_type> &>(gt) << "\n}";
    return os;
}

} // namespace champion

#endif
