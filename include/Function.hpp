#ifndef CHAMPION_FUNCTION_HPP
#define CHAMPION_FUNCTION_HPP

#include <functional>
#include <utility> // For std::swap

#include "set_precision.hpp"

namespace champion {

template <typename arg_type, typename grad_type> class Function {
  public:
    // Public member functions

    // Construct from reference data, argument, callable, advance function and
    // step size
    Function(const arg_type &x_ref, const arg_type &x0,
             std::function<real(arg_type)> func,
             std::function<arg_type(grad_type, real)> advance,
             real delta = defaults.delta_theta)
        : ref_{x_ref}, x_{x0}, func_{func}, advance_{advance}, delta_{delta} {}

    virtual ~Function() {}

    // Access
    inline real delta() { return delta_; }

    real operator()(const arg_type &arg) const { return func_(ref_, arg); }
    real operator()() const { return func_(ref_, x_); }

    // Advance
    // Advance from x_
    arg_type advance(grad_type dir, real step) const {
        return advance_(dir, step);
    }
    // Advance from custom x
    arg_type advance(arg_type x, grad_type dir, real step) {
	std::swap(x, x_);
	auto result = advance_(dir, step);
	std::swap(x, x_);
	return result;
    }
    
    // Gradient
    grad_type gradient(arg_type x) const {
	auto func_val = (*this)(x);
        grad_type grad;
        for (size_type i = 0; i != grad.size(); ++i) {
            grad_type dir; // Assuming default construction to zero value
            dir[i] = 1;    // Advance only in direction i
            grad[i] = ((*this)(advance(x, dir, delta_)) - func_val) / delta_;
        }
        return grad;
    }

    grad_type gradient() const {
	return gradient(x_);
    }

    // Directional derivative
    real directional_derivative(grad_type dir) const {
        return ((*this)(advance(dir, delta_) - (*this)())) / delta_;
    }

    inline real directional_derivative() = 0;

  protected:
    // Protected data members
    arg_type x_;   // Current argument
    arg_type ref_; // Reference data
    std::function<real(arg_type)> func_;
    std::function<arg_type(grad_type, real)> advance_;
    real delta_;
};
}

#endif
