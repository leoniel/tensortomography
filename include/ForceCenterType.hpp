/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: ForceCenterType.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:

ForceCenterType<level>

Class template to represent a type of mechanical body characterized by its
chemical species (BodyType<level>) as well as its place within the Graph of the
BodyType<level + 1> of which it is part. Templated on hierarchical level.
Represented by a GraphTree<BodyType<level>> with root node at the chosen
BodyType<level>.

\******************************************************************************/

#ifndef CHAMPION_FORCE_CENTER_TYPE_HPP
#define CHAMPION_FORCE_CENTER_TYPE_HPP

#include "Graph.hpp"
#include "GraphTree.hpp"
#include "ObjectIndex.hpp"
#include "set_precision.hpp"

namespace champion {

/***************************** Class declarations *****************************/

// Forward declarations
template <level_type n> class ObjectRegistry;
template <level_type n, level_type level> class BodyType;

template <level_type n, level_type level> class ForceCenterType {
  public:
    // Public types
    template <level_type ll> using bt_index = ObjectIndex<BodyType, n, ll>;
    template <level_type ll>
    using fct_index = ObjectIndex<::champion::ForceCenterType, n, ll>;
    // Public member functions
    // Construct from ObjectRegistry, ID and GraphTree
    ForceCenterType(ObjectRegistry<n> &reg, fct_index<level> ID,
                    GraphTree<bt_index<level>> graph_tree)
        : reg_{&reg}, id_{ID}, tree_{graph_tree} {}

    fct_index<level> get_id() const { return id_; }
    // Get chemical species of the force center type
    bt_index<level> get_species_id() const {
        return tree_.graph()[tree_.root()->value()];
    }
    const BodyType<n, level> &species() const {
        return reg_->get_object(get_species_id());
    }
    BodyType<n, level> &species() { return reg_->get_object(get_species_id()); }
    // Get const reference to graph tree
    const GraphTree<bt_index<level>> &tree() const { return tree_; }

    std::vector<fct_index<level>> get_bonded_neighbor_types() const {
        auto res = std::vector<fct_index<level>>{};
        auto graph = tree_.graph();
        auto indices = graph.get_neighbor_indices(tree_.root()->value());
        auto FCT_els =
            reg_->template get_objects<::champion::ForceCenterType, level>();
        for (auto index : indices) {
            auto tree = GraphTree<bt_index<level>>{graph, index};
            fct_index<level> fct_id = std::numeric_limits<size_type>::max();
            for (const auto &el : FCT_els) {
                if (el.second.tree() == tree) {
                    fct_id = el.first;
                    break;
                }
            }
            res.push_back(fct_id);
        }
        return res;
    }

  private:
    // Private data members
    ObjectRegistry<n> *reg_;
    fct_index<level> id_;
    GraphTree<bt_index<level>> tree_;
};

template <level_type n, level_type level>
bool operator<(const ForceCenterType<n, level> &FCT1,
               const ForceCenterType<n, level> &FCT2) {
    return FCT1.tree() < FCT2.tree();
}

template <level_type n, level_type level>
bool operator>(const ForceCenterType<n, level> &FCT1,
               const ForceCenterType<n, level> &FCT2) {
    return FCT1.tree() > FCT2.tree();
}

template <level_type n, level_type level>
bool operator==(const ForceCenterType<n, level> &FCT1,
                const ForceCenterType<n, level> &FCT2) {
    return FCT1.tree() == FCT2.tree();
}

template <level_type n, level_type level>
bool operator!=(const ForceCenterType<n, level> &FCT1,
                const ForceCenterType<n, level> &FCT2) {
    return !(FCT1 == FCT2);
}

template <level_type n, level_type level>
bool operator<=(const ForceCenterType<n, level> &FCT1,
                const ForceCenterType<n, level> &FCT2) {
    return FCT1.tree() <= FCT2.tree();
}

template <level_type n, level_type level>
bool operator>=(const ForceCenterType<n, level> &FCT1,
                const ForceCenterType<n, level> &FCT2) {
    return FCT1.tree() >= FCT2.tree();
}

template <level_type n, level_type level>
std::ostream &print_brief(std::ostream &os,
                          const ForceCenterType<n, level> &fct) {
    os << "ForceCenterType<" << level << "> #" << fct.get_id();
    if (fct.species().get_name() != "") {
        os << " (" << fct.species().get_name() << ")";
    }
    return os;
}

template <level_type n, level_type level>
std::ostream &print_full(std::ostream &os,
                         const ForceCenterType<n, level> &fct) {
    print_brief(os, fct) << ": {";
    try {
        print_brief(os, fct.tree()) << "}";
    } catch (const std::exception &e) {
        std::cerr << "Error while printing graph tree: " << e.what()
                  << std::endl;
    }
    return os;
}

template <level_type n, level_type level>
std::ostream &print_recursive(std::ostream &os,
                              const ForceCenterType<n, level> &fct) {
    print_brief(os, fct) << ": {";
    print_recursive(os, fct.tree()) << "}";
    return os;
}

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os,
                         const ForceCenterType<n, level> &fct) {
    print_full(os, fct);
    return os;
}

} // namespace champion

#endif
