#ifndef CHAMPION_TRANFORMATION_HPP
#define CHAMPION_TRANFORMATION_HPP

#include <iostream>

#include "Point.hpp"
#include "Rotation.hpp"
#include "Vector.hpp"
#include "set_precision.hpp"

namespace champion {

// Forward declarations
template <typename T> class Transformation;
template <typename T>
std::ostream &operator<<(std::ostream &, const Transformation<T> &);

// Geometrical transformation composed of a translation and a rotation
template <typename T> class Transformation {
  public:
    using value_type = T;
    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;

    // Public member functions

    // Constructors
    // Default constructor (identity transformation)
    Transformation() : transformation_(6, 0) {}
    // Copy constructor
    Transformation(const Transformation &) = default;
    // Copy assignment
    Transformation &operator=(const Transformation &) = default;
    // Move constructor
    Transformation(Transformation &&) = default;
    // Move assignment
    Transformation &operator=(Transformation &&) = default;
    // Generalized copy constructor
    template <typename T2>
    Transformation(const Transformation<T2> &t) : transformation_{} {
        for (const auto &ti : t) {
            transformation_.push_back(static_cast<T>(ti));
        }
    }
    // Generalized copy assignment
    template <typename T2>
    Transformation &operator=(const Transformation<T2> &t) {
        transformation_ = {};
        for (const auto &ti : t) {
            transformation_.push_back(static_cast<T>(ti));
        }
        return *this;
    }
    // Construct from Vector and Rotation
    Transformation(Vector<T> translation, Rotation<T> rotation)
        : transformation_{} {
        for (auto ti : translation)
            transformation_.push_back(ti);
        for (auto ri : rotation)
            transformation_.push_back(ri);
    }
    // Construct from Point and Rotation
    Transformation(Point<T> translation, Rotation<T> rotation)
        : transformation_{} {
        for (auto ti : translation)
            transformation_.push_back(ti.value());
        for (auto ri : rotation)
            transformation_.push_back(ri);
    }

    // Construct from components
    Transformation(T Tx, T Ty, T Tz, T Rx, T Ry, T Rz)
        : transformation_{Tx, Ty, Tz, Rx, Ry, Rz} {}

    // Component access
    size_type size() const { return 6; } // 3 transl cmpnts + 3 rot cmpnts
    Vector<T> translation() const {
        return Vector<T>(transformation_[0], transformation_[1],
                         transformation_[2]);
    }
    Rotation<T> rotation() const {
        return Rotation<T>{Vector<T>{transformation_[3], transformation_[4],
                                     transformation_[5]}};
    }
    iterator begin() { return transformation_.begin(); }
    iterator end() { return transformation_.end(); }
    const_iterator begin() const { return transformation_.begin(); }
    const_iterator end() const { return transformation_.end(); }
    const_iterator cbegin() { return transformation_.begin(); }
    const_iterator cend() { return transformation_.end(); }
    value_type &operator[](size_type i) { return *(begin() + i); }
    const value_type operator[](size_type i) const { return *(begin() + i); }

  private:
    // Friends
    template <typename T2> friend class Transformation;
    // Private data members
    std::vector<T> transformation_;
};

// Mathematical operations

// Unary operators

template <typename T> Transformation<T> operator-(const Transformation<T> &t) {
    return Transformation<T>{-t.translation(), -t.rotation()};
}

// Binary operators

// Addition and subtraction
template <typename T1, typename T2>
Transformation<T1> operator+=(Transformation<T1> &t1,
                              const Transformation<T2> &t2) {
    Vector<T1> t = t1.translation() + t2.translation();
    Rotation<T1> r = t1.rotation() + t2.rotation();
    return t1 = Transformation<T1>(t, r);
}
template <typename T1, typename T2>
Transformation<Sum_type<T1, T2>> operator+(const Transformation<T1> &t1,
                                           const Transformation<T2> &t2) {
    Transformation<Sum_type<T1, T2>> t = t1;
    return t += t2;
}
template <typename T1, typename T2>
Transformation<Sum_type<T1, T2>> operator-=(Transformation<T1> &t1,
                                            const Transformation<T2> &t2) {
    return t1 += -t2;
}
template <typename T1, typename T2>
Transformation<Sum_type<T1, T2>> operator-(const Transformation<T1> &t1,
                                           const Transformation<T2> &t2) {
    return t1 + -t2;
}

// Scaling
template <typename T1, typename T2>
Transformation<T1> operator*=(Transformation<T1> &t, const T2 &a) {
    for (auto &ti : t) {
        ti *= a;
    }
    return t;
}

template <typename T1, typename T2>
Transformation<Product_type<T1, T2>> operator*(const Transformation<T1> &t,
                                               const T2 &r) {
    Transformation<Product_type<T1, T2>> t_res = t;
    return t_res *= r;
}

template <typename T1, typename T2>
Transformation<Product_type<T1, T2>> operator*(const T2 &r,
                                               const Transformation<T1> &t) {
    return t * r;
}

template <typename T1>
template <typename T2>
Vector<Sum_type<Product_type<T2, T1, Inverse_type<T2>>, T2>>
Vector<T1>::transform(const Transformation<T2> &transf) const {
    auto res = *this;
    res = res.rotate(transf.rotation()) + transf.translation();
    return res;
}

// Output
template <typename T>
std::ostream &operator<<(std::ostream &os, const Transformation<T> &t) {
    os << "Transformation: {Translation: " << t.translation()
       << ", Rotation: " << t.rotation() << "}";
    return os;
}

} // namespace champion

#endif