/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: Torsion.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:

DESCRIPTION:


\******************************************************************************/

#ifndef CHAMPION_TORSION_HPP
#define CHAMPION_TORSION_HPP

#include "RegistryIterator.hpp"

namespace champion {

/***************************** Class declarations *****************************/

// Forward declarations
template <level_type n> class ObjectRegistry;
template <level_type n, level_type level> class TorsionType;
template <level_type n, level_type level> class ForceCenter;

template <level_type n, level_type level> class Torsion {
  public:
    // Public types
    using constituent_type = ForceCenter<n, level>;
    using iterator = RegistryIterator<Torsion<n, level>, false>;
    using const_iterator = RegistryIterator<Torsion<n, level>, true>;

    template <level_type ll>
    using trajectory_index = ObjectIndex<Trajectory, n, ll>;
    template <level_type ll>
    using t_index = ObjectIndex<::champion::Torsion, n, ll>;
    template <level_type ll> using tt_index = ObjectIndex<TorsionType, n, ll>;
    template <level_type ll> using fc_index = ObjectIndex<ForceCenter, n, ll>;
    // Public member functions
    // Construct from ObjectRegistry, ID, torsion type, constituents and
    // trajectory index
    Torsion(ObjectRegistry<n> &reg, t_index<level> ID, tt_index<level> tt,
            fc_index<level> fc1, fc_index<level> fc2, fc_index<level> fc3,
            fc_index<level> fc4, size_type traj_index = 0)
        : reg_{&reg}, id_{ID}, torsion_type_{tt},
          constituents_{fc1, fc2, fc3, fc4}, trajectory_{traj_index} {
        if (constituents_[3] < constituents_[0]) {
            std::swap(constituents_[0], constituents_[3]);
            std::swap(constituents_[1], constituents_[2]);
        }
    }

    // Access

    // Index access
    static constexpr size_type size() { return 4; }
    t_index<level> id() const { return id_; }
    fc_index<level> get_index(size_type i) const { return constituents_[i]; }
    tt_index<level> get_type_index() const { return torsion_type_; }
    fc_index<level> first() const { return constituents_[0]; }
    fc_index<level> second() const { return constituents_[1]; }
    fc_index<level> third() const { return constituents_[2]; }
    fc_index<level> fourth() const { return constituents_[3]; }
    trajectory_index<level> get_trajectory_index() const { return trajectory_; }

    // Object access
    iterator begin() { return iterator{*this}; }
    iterator end() { return iterator{*this, size()}; }
    const_iterator begin() const { return const_iterator{*this, size()}; }
    const_iterator end() const { return const_iterator{*this, size()}; }
    const_iterator cbegin() const { return const_iterator{*this}; }
    const_iterator cend() const { return const_iterator{*this, size()}; }

    const ForceCenter<n, level> &operator[](size_type i) const {
        return reg_->template get_object<ForceCenter, level>(constituents_[i]);
    }
    ForceCenter<n, level> &operator[](size_type i) {
        return const_cast<ForceCenter<n, level> &>(
            static_cast<const Torsion &>(*this)[i]);
    }

    const TorsionType<n, level> &type() const {
        return reg_->template get_object<TorsionType, level>(torsion_type_);
    }

  private:
    // Private data members
    ObjectRegistry<n> *reg_;
    t_index<level> id_;
    tt_index<level> torsion_type_;
    std::vector<fc_index<level>> constituents_;
    trajectory_index<level> trajectory_;
};

template <level_type n, level_type level>
bool operator==(const Torsion<n, level> &t1, const Torsion<n, level> &t2) {
    assert(t1.size() == t2.size());
    for (size_type i = 0; i != t1.size(); ++i) {
        if (t1[i] != t2[i]) {
            return false;
        }
    }
    return true;
}

template <level_type n, level_type level>
bool operator!=(const Torsion<n, level> &t1, const Torsion<n, level> &t2) {
    return !(t1 == t2);
}

template <level_type n, level_type level>
std::ostream &print_brief(std::ostream &os, const Torsion<n, level> &t) {
    os << "Torsion<" << level << "> #" << t.id() << " (TorsionType<" << level
       << "> #" << t.get_type_index() << ")";
    return os;
}

template <level_type n, level_type level>
std::ostream &print_full(std::ostream &os, const Torsion<n, level> &t) {
    print_brief(os, t) << ": {";
    print_brief(os, t[0]) << ", ";
    print_brief(os, t[1]) << ", ";
    print_brief(os, t[2]) << ", ";
    print_brief(os, t[3]) << "}";
    return os;
}

template <level_type n, level_type level>
std::ostream &print_recursive(std::ostream &os, const Torsion<n, level> &t) {
    os << "Bond<" << level << "> #" << t.id() << ": {";
    print_recursive(os, t[0]) << ", ";
    print_recursive(os, t[1]) << ", ";
    print_recursive(os, t[2]) << ", ";
    print_recursive(os, t[3]) << "}";
    return os;
}

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os, const Torsion<n, level> &t) {
    print_full(os, t);
    return os;
}

} // namespace champion

#endif
