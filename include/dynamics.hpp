#ifndef CHAMPION_DYNAMICS_HPP
#define CHAMPION_DYNAMICS_HPP

#include "Matrix.hpp"
#include "Point.hpp"
#include "Rotation.hpp"
#include "Vector.hpp"
#include "set_precision.hpp"

namespace champion {

template <typename T>
Vector<T> center_of_mass(const std::vector<real> &masses,
                         const std::vector<Vector<T>> &coordinates) {
    if (masses.size() != coordinates.size()) {
        throw std::runtime_error{
            "CHAMPION::center_of_mass: vectors must be of the same size."};
    }
    real total_mass = std::accumulate(masses.begin(), masses.end(), 0.0);
    Vector<T> weighted_sum = {0, 0, 0};
    for (size_type i = 0; i != coordinates.size(); ++i) {
        weighted_sum += masses[i] * coordinates[i];
    }
    auto center = weighted_sum / total_mass;
    return center;
}

template <typename T>
Point<T> center_of_mass(const std::vector<real> &masses,
                        const std::vector<Point<T>> &coordinates) {
    auto origin = coordinates[0];
    auto shifted_coords = std::vector<Vector<T>>(coordinates.size());
    for (size_type i = 0; i != coordinates.size(); ++i) {
        shifted_coords[i] = Vector{coordinates[i] - origin};
    }
    Vector<T> shifted_center = center_of_mass(masses, shifted_coords);
    auto center = origin + shifted_center;
    return center;
}

template <typename T>
SymmetricMatrix<T, 3>
matrix_of_inertia(const std::vector<real> &masses,
                  const std::vector<Vector<T>> &rel_coordinates) {
    if (masses.size() != rel_coordinates.size()) {
        throw std::runtime_error{
            "CHAMPION::matrix_of_inertia(): "
            "Size mismatch between mass and coordinate vectors."};
    }
    auto I = SymmetricMatrix<T, 3>(0);
    for (size_type i = 0; i != rel_coordinates.size(); ++i) {
        for (size_type xi = 0; xi < 3; ++xi) {
            for (size_type offset = 1; offset <= 2; ++offset) {
                auto xj = (xi + offset) % 3;
                I(xi, xi) +=
                    masses[i] * rel_coordinates[i][xj] * rel_coordinates[i][xj];
            }
        }
        for (size_type xi = 0; xi < 3; ++xi) {
            for (size_type xj = xi + 1; xj < 3; ++xj) {
                I(xi, xj) -=
                    masses[i] * rel_coordinates[i][xi] * rel_coordinates[i][xj];
            }
        }
    }
    return I;
}

template <typename T>
void extrapolate_dynamics(const std::vector<real> &masses,
                          const std::vector<Vector<T>> &coordinates,
                          const std::vector<Vector<T>> &velocities, real dt,
                          Vector<T> &translation, Rotation<T> &rotation) {
    auto N = masses.size();
    if (coordinates.size() != N || velocities.size() != N) {
        throw std::runtime_error{
            "CHAMPION::extrapolate_dynamics(): vectors must be the same size."};
    }
    auto total_mass = std::accumulate(masses.begin(), masses.end(), 0.0);
    auto center = center_of_mass(masses, coordinates);
    auto rel_coordinates = coordinates;
    for (auto &c : rel_coordinates) {
        c -= center;
    }
    auto p = Vector<T>{0.0, 0.0, 0.0};
    for (size_type i = 0; i != N; ++i) {
        p += masses[i] * velocities[i];
    }
    auto v = p / total_mass;
    auto rot_velocities = velocities;
    for (auto &vi : rot_velocities) {
        vi -= v;
    }
    auto L = Vector<T>{0.0, 0.0, 0.0};
    for (size_type i = 0; i != N; ++i) {
        L += masses[i] * cross(rel_coordinates[i], rot_velocities[i]);
    }
    auto omega = Matrix<T, 3, 1>{0, 0, 0};
    auto I = matrix_of_inertia(masses, rel_coordinates);
    if (I.determinant() != 0) {
        auto I_inv = I.inverse();
        omega = I_inv * Matrix<T, 3, 1>{L};
    }
    translation = v * dt;
    rotation = dt * Rotation<T>{omega.begin()};
}

template <typename T>
void extrapolate_dynamics(const std::vector<real> &masses,
                          const std::vector<Vector<T>> &coordinates,
                          const std::vector<Vector<T>> &velocities, real dt,
                          std::vector<Vector<T>> &coordinates_out,
                          std::vector<Vector<T>> &velocities_out) {
    auto N = masses.size();
    if (coordinates.size() != N || velocities.size() != N) {
        throw std::runtime_error{
            "CHAMPION::extrapolate_dynamics(): vectors must be the same size."};
    }
    auto total_mass = std::accumulate(masses.begin(), masses.end(), 0.0);
    auto center = center_of_mass(masses, coordinates);
    auto rel_coordinates = coordinates;
    for (auto &c : rel_coordinates) {
        c -= center;
    }
    auto p = Vector<T>{0.0, 0.0, 0.0};
    for (size_type i = 0; i != N; ++i) {
        p += masses[i] * velocities[i];
    }
    auto v = p / total_mass;
    auto rot_velocities = velocities;
    for (auto &vi : rot_velocities) {
        vi -= v;
    }
    auto L = Vector<T>{0.0, 0.0, 0.0};
    for (size_type i = 0; i != N; ++i) {
        L += masses[i] * cross(rel_coordinates[i], rot_velocities[i]);
    }
    auto omega = Matrix<T, 3, 1>{0, 0, 0};
    auto I = matrix_of_inertia(masses, rel_coordinates);
    if (I.determinant() != 0) {
        auto I_inv = I.inverse();
        omega = I_inv * Matrix<T, 3, 1>{L};
    }
    Vector<T> translation = v * dt;
    Rotation<T> rotation = dt * Rotation<T>{omega.begin()};
    coordinates_out = coordinates;
    velocities_out = velocities;
    for (size_type i = 0; i != masses.size(); ++i) {
        coordinates_out[i] =
            coordinates[i].rotate(rotation, center) + translation;
        velocities_out[i] = v + rot_velocities[i].rotate(rotation);
    }
}

template <typename T>
void extrapolate_dynamics(const std::vector<real> &masses,
                          const std::vector<Point<T>> &coordinates,
                          const std::vector<Vector<T>> &velocities, real dt,
                          std::vector<Point<T>> &coordinates_out,
                          std::vector<Vector<T>> &velocities_out) {
    auto N = masses.size();
    // Build coordinate Vector std::vector in a shifted coordinate system to
    // avoid difficulties with boundary conditions
    auto vec_coords = std::vector<Vector<T>>(N);
    for (size_type i = 0; i != N; ++i) {
        vec_coords[i] = Vector<T>{coordinates[i] - coordinates[0]};
    }
    std::vector<Vector<T>> vec_coords_out;
    extrapolate_dynamics(masses, vec_coords, velocities, dt, vec_coords_out,
                         velocities_out);
    // Shift back output coordinates to original coordinate system with boundary
    // conditions
    coordinates_out = {};
    for (size_type i = 0; i != N; ++i) {
        coordinates_out.push_back(coordinates[0] + vec_coords_out[i]);
    }
}

// template <typename T>
// Geometry<T> extrapolate_dynamics(const std::vector<real> &masses,
//                                  const Geometry<T> &g0, const Geometry<T>
//                                  &g1, real dt01 = 1, real dt12 = 1) {
//     auto T01 = g0.match_orientation(g1);
//     auto I0 = g0.matrix_of_inertia(masses);
//     auto I1 = g1.matrix_of_inertia(masses);
//     auto I01 = (I0 + I1) / 2;
//     auto omega01 = T01.rotation() / dt01;
//     auto L = I01 * Matrix<T, 3, 1>{omega01.begin(), omega01.end()};
//     auto R12_matrix = I01.inverse() * L * dt12;
//     auto R12 = Rotation<T>{R12_matrix.begin()};
//     auto g2 = g1.rotate(R12) + T01.translation();
//     return g2;
// }

} // namespace champion

#endif