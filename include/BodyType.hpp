/******************************************************************************\
    _____  __     __   ________  ___    __  ______   __   _____    ____      __
  //  _  \/| |   /| |//  ____  \/|  \ //  |/|  __ \ /| | // __ \  /|   \    /| |
 //  /_#\_#| |  |#| |#| |###/| |#|   \/   |#| |#/\ \#| |// /#/\ \|#| |\ \  |#| |
|#| |  \/##| |__|#| |#| |__|#| |#| |\  /| |#| ||#| |#| |#| | |#| |#| |#\ \ |#| |
|#| |    |#|  ____  |#|  ____  |#| | \/#| |#| ||#/ /#| |#| | |#| |#| |\#\ \ #| |
|#| |   _|#| |###/| |#| |###/| |#| |  |#| |#|  ___/|#| |#| | |#| |#| | \#\ \#| |
|#\  \_/  #| |  |#| |#| |  |#| |#| |  |#| |#| |##/ |#| |#| \_|/ /|#| |  \#\ \| |
 \#\_____/#|_|  |#|_|#|_|  |#|_|#|_|  |#|_|#|_|    |#|_|\/\____/ |#| |   \#\___|
  \/####/|#/#/  |#/#/|/#/  |/#/|/#/   |/#/|#/#/    |/#/  \/###/  |#/#/    \/###/
================================================================================
 ______________________________________________________________________________
|       Chalmers      Atomic,      Molecular,      Polymeric   &   Ionic       |
|                                                                              |
|                          ANALYSIS         TOOLKIT                            |
|______________________________________________________________________________|

********************************** MIT LICENSE *********************************

Copyright (c) 2017 Rasmus Andersson

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*********************************** SYNOPSIS ***********************************

HEADER: BodyType.hpp

AUTHOR: Rasmus Andersson (rasmus.andersson@chalmers.se) 2017-10-20
MODIFIED BY:
nnn
DESCRIPTION:

Class template to represent species of  mechanical bodies, templated on
hierarchical level. Level 0 denotes simple point particle species with fixed
mass and radius and no internal structure. Higher levels represent body types
composed by graphs of lower level force center types.

\******************************************************************************/

#ifndef CHAMPION_BODY_TYPE_HPP
#define CHAMPION_BODY_TYPE_HPP

#include <exception>
#include <memory>
#include <string>
#include <type_traits>
#include <typeinfo>
#include <utility>

#include "Geometry.hpp"
#include "Graph.hpp"
#include "ObjectIndex.hpp"
#include "RegistryIterator.hpp"
#include "Vector.hpp"
#include "set_precision.hpp"

namespace champion {

/******************** Class declarations and definitions  *********************/

// Forward declarations
template <level_type n> class ObjectRegistry;

// Class template BodyType
template <level_type n, level_type level> class BodyType {
  public:
    // Public types
    using constituent_type = BodyType<n, level - 1>;
    using iterator = RegistryIterator<BodyType<n, level>, false>;
    using const_iterator = RegistryIterator<BodyType<n, level>, true>;

    template <level_type ll>
    using bt_index = ObjectIndex<::champion::BodyType, n, ll>;

    // Public member functions
    // Construct from object registry, ID, ID of constituents
    // (in object registry), bonds, prototype geometry and name (optional)
    BodyType(ObjectRegistry<n> &reg, size_type ID,
             Graph<bt_index<level - 1>> graph, Geometry<real> prototype = {},
             std::string bt_name = "");

    // Access
    size_type size() const { return graph_.size(); }
    size_type count_atoms() const {
        size_type count = 0;
        for (auto b : *this) {
            count += b.species().count_atoms();
        }
        return count;
    }
    bt_index<level> get_id() const { return id_; }
    std::string compute_name() const;
    std::string get_name() const { return name_; }
    iterator begin() { return iterator{*this}; }
    iterator end() { return iterator{*this, size()}; }
    const_iterator begin() const { return const_iterator{*this}; }
    const_iterator end() const { return const_iterator{*this, size()}; }
    const_iterator cbegin() const { return const_iterator{*this}; }
    const_iterator cend() const { return const_iterator{*this, size()}; }

    const BodyType<n, level - 1> &operator[](size_type i) const {
        return reg_->template get_object<BodyType, level - 1>(graph_[i]);
    }
    BodyType<n, level - 1> &operator[](size_type i) {
        return const_cast<BodyType<n, level - 1> &>(
            static_cast<const BodyType &>(*this)[i]);
    }

    template <level_type ll>
    std::enable_if_t<(ll == level - 1), std::vector<bt_index<ll>>>
    get_constituent_ids() const;
    template <level_type ll>
    std::enable_if_t<(ll < level - 1), std::vector<bt_index<ll>>>
    get_constituent_ids() const;

    static constexpr level_type get_level() { return level; }
    real get_mass() const { return mass_; }
    Vector<real> get_center_of_mass() const { return prototype_.get_center(); }
    const Graph<bt_index<level - 1>> &graph() const { return graph_; }
    Geometry<real> get_prototype() const { return prototype_; }
    void set_prototype(Geometry<real> prot) {
        if (prot.size() != this->size()) {
            throw std::runtime_error{"CHAMPION::BodyType::set_prototype: Wrong "
                                     "number of coordinates."};
        } else {
            prototype_ = prot;
        }
    }

  private:
    // Private member functions
    void construct_graph(std::vector<Edge> bonds);
    std::vector<bt_index<level - 1>> get_constituents_impl() const;
    void compute_mass_and_com();

    // Private data members
    ObjectRegistry<n> *reg_;
    bt_index<level> id_;
    std::string name_;
    real mass_;
    Graph<bt_index<level - 1>> graph_;
    Geometry<real> prototype_;
};

// Specialization of BodyType for level = 0 (point particle type)
template <level_type n> class BodyType<n, 0> {
  public:
    // Public types
    using bt_index = ObjectIndex<::champion::BodyType, n, 0>;
    // Public member functions
    // Construct from object registry, ID, name, mass and radius
    BodyType(ObjectRegistry<n> &reg, size_type ID, std::string bt_name,
             real mass, real radius)
        : reg_{&reg}, id_{ID}, name_{bt_name}, mass_{mass}, radius_{radius} {}

    constexpr size_type size() const { return 1; }
    constexpr size_type count_atoms() const { return 1; }
    bt_index get_id() const { return id_; }
    std::string get_name() const { return name_; }

    static constexpr level_type get_level() { return 0; }
    real get_mass() const { return mass_; }
    real get_radius() const { return radius_; }

  private:
    // Private data members
    ObjectRegistry<n> *reg_;
    bt_index id_;
    std::string name_;
    real mass_;
    real radius_;
};

/********************** Member function definitions ***************************/

template <level_type n, level_type level>
BodyType<n, level>::BodyType(ObjectRegistry<n> &reg, size_type ID,
                             Graph<bt_index<level - 1>> graph,
                             Geometry<real> prototype, std::string bt_name)
    : reg_{&reg}, id_{ID}, name_{bt_name}, mass_{0},
      graph_(graph), prototype_{} {
    if (prototype.size() != 0) {
        auto key = graph.sort();
        set_prototype(prototype);
        sort_consistently(key, prototype_);
    }
    compute_mass_and_com();
    if (bt_name == "") {
        name_ = compute_name();
    }
}

template <level_type n, level_type level>
std::vector<ObjectIndex<BodyType, n, level - 1>>
BodyType<n, level>::get_constituents_impl() const {
    auto level_minus_1_vec = std::vector<bt_index<level - 1>>(graph_.size());
    for (size_type i = 0; i != graph_.size(); ++i) {
        level_minus_1_vec[i] = reg_->get_object(graph_[i]).get_id();
    }
    return level_minus_1_vec;
}

template <level_type n, level_type level>
template <level_type ll>
std::enable_if_t<(ll == level - 1), std::vector<ObjectIndex<BodyType, n, ll>>>
BodyType<n, level>::get_constituent_ids() const {
    return get_constituents_impl();
}

template <level_type n, level_type level>
template <level_type ll>
std::enable_if_t<(ll < level - 1), std::vector<ObjectIndex<BodyType, n, ll>>>
BodyType<n, level>::get_constituent_ids() const {
    auto level_minus_1_vec = get_constituents_impl();
    auto ll_vec = std::vector<ObjectIndex<::champion::BodyType, n, ll>>{};
    for (auto &bt : level_minus_1_vec) {
        auto new_constituents =
            reg_->get_object(bt).template get_constituent_ids<ll>();
        ll_vec.insert(ll_vec.end(), new_constituents.begin(),
                      new_constituents.end());
    }
    return ll_vec;
}

template <level_type n, level_type level>
void BodyType<n, level>::compute_mass_and_com() {
    // Calculate total mass and center of mass
    auto com = Vector<real>{0, 0, 0};
    auto constituents = get_constituent_ids<level - 1>();
    auto N = constituents.size();
    if (prototype_.size() != 0) {
        assert(prototype_.size() == N);
    }
    for (size_type i = 0; i != constituents.size(); ++i) {
        auto mi = reg_->get_object(constituents[i]).get_mass();
        mass_ += mi;
        if (prototype_.size() != 0) {
            com += mi * prototype_[i];
        }
    }
    prototype_.set_center(com / mass_);
}

template <level_type n, level_type level>
std::string BodyType<n, level>::compute_name() const {
    auto constituent_names = std::vector<std::string>{};
    auto constituent_masses = std::vector<real>{};
    auto constituent_counts = std::vector<size_type>{};
    for (const auto &c : *this) {
        auto name = c.get_name();
        auto it =
            std::find(constituent_names.begin(), constituent_names.end(), name);
        if (it == constituent_names.end()) {
            constituent_names.push_back(name);
            constituent_masses.push_back(c.get_mass());
            constituent_counts.push_back(1);
        } else {
            auto index = it - constituent_names.begin();
            ++constituent_counts[index];
        }
    }
    sort_consistently(constituent_masses, constituent_names,
                      constituent_counts);
    std::stringstream ss;
    for (size_type i = 0; i < constituent_names.size(); ++i) {
        ss << constituent_names[i];
        if (constituent_counts[i] > 1) {
            ss << constituent_counts[i];
        }
        if (i < constituent_names.size() - 1) {
            ss << "_";
        }
    }
    std::string name = "";
    ss >> name;
    return name;
}

/************************** Non-member functions ******************************/

// Compare BodyType:s based on their graphs
template <level_type n, level_type level>
bool operator<(const BodyType<n, level> &b1, const BodyType<n, level> &b2) {
    return b1.graph() < b2.graph();
}

// Compare BodyType<0>:s (point particle types) based on name
template <level_type n>
bool operator<(const BodyType<n, 0> &b1, const BodyType<n, 0> &b2) {
    return b1.get_name() < b2.get_name();
}

template <level_type n, level_type level>
size_type count_atoms(const BodyType<n, level> &bt, size_type n0 = 0) {
    size_type num_atoms = n0;
    for (const auto &bti : bt) {
        num_atoms += count_atoms(bti.species());
    }
    return num_atoms;
}

template <level_type n>
size_type count_atoms(const BodyType<n, 0> &bt, size_type n0 = 0) {
    return n0 + 1;
}

template <level_type n>
size_type count_atoms(const BodyType<n, 0> bt, size_type n0) {
    return n0 + 1;
}

template <level_type n, level_type level>
std::ostream &print_brief(std::ostream &os, const BodyType<n, level> &bt) {
    os << "BodyType<" << level << "> #" << bt.get_id();
    if (bt.get_name() != "") {
        os << ": " << bt.get_name();
    }
    return os;
}

template <level_type n, level_type level>
std::ostream &operator<<(std::ostream &os, const BodyType<n, level> &bt) {
    print_full(os, bt);
    return os;
}

template <level_type n, level_type level>
std::ostream &print_full(std::ostream &os, const BodyType<n, level> &bt) {
    print_brief(os, bt);
    size_type i = 0;
    auto num_constituents = bt.size();
    os << ": {Constituents: {";
    for (i = 0; i != num_constituents - 1; ++i) {
        os << "[" << i << "]: ";
        print_brief(os, bt[i]) << ", ";
    }
    i = num_constituents - 1;
    os << "[" << i << "]: ";
    print_brief(os, bt[i]) << "}, Bonds: {";
    auto edges = bt.graph().get_edges();
    auto num_edges = edges.size();
    if (num_edges > 0) {
        for (i = 0; i < num_edges - 1; ++i) {
            os << edges[i] << ", ";
        }
        i = num_edges - 1;
        os << edges[i];
    }
    os << "}}";
    return os;
}

template <level_type n, level_type level>
std::ostream &print_recursive(std::ostream &os, const BodyType<n, level> &bt) {
    print_brief(os, bt);
    size_type i = 0;
    auto num_constituents = bt.size();
    os << ": {Constituents: {";
    for (i = 0; i != num_constituents - 1; ++i) {
        os << "[" << i << "]: ";
        print_recursive(os, bt[i]) << ", ";
    }
    i = num_constituents - 1;
    os << "[" << i << "]: ";
    print_recursive(os, bt[i]) << "}, Bonds: {";
    auto edges = bt.graph().get_edges();
    auto num_edges = edges.size();
    if (num_edges > 0) {
        for (i = 0; i < num_edges - 1; ++i) {
            os << edges[i] << ", ";
        }
        i = num_edges - 1;
        os << edges[i];
    }
    os << "}}";
    return os;
}

template <level_type n>
std::ostream &operator<<(std::ostream &os, const BodyType<n, 0> &bt) {
    os << "BodyType<0> #" << bt.get_id() << " " << bt.get_name();
    return os;
}

} // namespace champion
#endif
