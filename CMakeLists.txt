cmake_minimum_required(VERSION 3.0)

project(tensortomography LANGUAGES C)

add_executable(tensortomography main.c)

install(TARGETS tensortomography RUNTIME DESTINATION bin)
