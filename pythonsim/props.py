import matplotlib.pyplot as plt
import math
import numpy as np
import csv
import time
import os
import threading as thr
import time
from math import floor,ceil
from scipy import signal as sps
from scipy import fftpack as spf
from scipy import linalg as spl
from scipy import ndimage as spn
from abc import ABC,abstractmethod
from numpy import linalg as lg

class material:
    def __init__(self):
        return
    def set_property(self,*,density=0,refractive=1,permitt=1,permeab=1):
        self.rho=density
        self.refractive=refractive
        self.permitt=permitt
        self.permeab=permeab
        
class tissue(material):
    
