import matplotlib.pyplot as plt
import matplotlib as mpl
import math
import numpy as np
import csv
import time
import os
import pickle
import uncertainties as uc
import multiprocessing
import time
import voxelgeo
#import props
from math import floor,ceil
from scipy import signal as sps
from scipy import fftpack as spf
from scipy import linalg as spl
from scipy import ndimage as spn
from abc import ABC,abstractmethod
from numpy import linalg as lg
from scipy import constants as con
from scipy import integrate as spi
from scipy.spatial.transform import Rotation as R
from scipy import stats as spst
from scipy import special as spec
from scipy import math as spm
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from numpy import pi,sin,cos,tan

mpl.rcParams['font.size']=14. #Large font size because my sight sucks
mpl.rcParams['lines.linewidth']=4. #Default size is a bit too small for big plots
mpl.rcParams['lines.markersize']=8. #Default size is a bit too small for big plots
