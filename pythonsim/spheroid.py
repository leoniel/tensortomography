import matplotlib.pyplot as plt
import matplotlib as mpl
import math
import numpy as np
import csv
import time
import os
import pickle
import uncertainties as uc
import multiprocessing
import time
import voxelgeo
#import props
from math import floor,ceil
from scipy import signal as sps
from scipy import fftpack as spf
from scipy import linalg as spl
from scipy import ndimage as spn
from abc import ABC,abstractmethod
from numpy import linalg as lg
from scipy import constants as con
from scipy import integrate as spi
from scipy.spatial.transform import Rotation as R
from scipy import stats as spst
from scipy import special as spec
from scipy import math as spm
from scipy import optimize as spo
from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from numpy import pi,sin,cos,tan,sqrt

mpl.rcParams['font.size']=14. #Large font size because my sight sucks
mpl.rcParams['lines.linewidth']=4. #Default size is a bit too small for big plots
mpl.rcParams['lines.markersize']=8. #Default size is a bit too small for big plots

ASYM=1
NUM=5#radial intergration interval division
NUM2=100 #Asymmetric polar division
NUM_ANG=40#polar/azimuthal integration interval divisino
DIV_AZIM=4
ETA_MIN=-1+0.005 #Azimuthal ra
ETA_MAX=1-0.005
DIV_POL=3
PHI_MIN=0.001
PHI_MAX=2*pi-0.001
NUM_CORES=12 #number of computer cores.



def sigTH(m,n,c,th,ct='prolate'):
    s,ds = d_r_driver(m,n,c,np.cos(th),ct,0,0)
    sig = m*s/np.sin(th)
    chi = -np.sin(th)*ds
    return sig,chi

def distrib_jobs(m,num):
    jobs = [[] for i in range(num)]
    k=range(m)
    j=0
    while True:
        for i in range(num):
            jobs[i].append(k[j])
            j+=1
            if j==m:
                break
        if j==m:
            break
    return jobs

def alpha_r(m,r,c):
    al = (c**2)*(2*m+r+2)*(2*m+r+1)/((2*m +2*r + 5)*(2*m + 2*r + 3))
    return al

def beta_r(m,r,c):
    be = (m+r)*(m+r+1)+ (c**2)*(2*((m+r)*(m+r+1))-2*m**2-1)/((2*m+2*r-1)*(2*m+2*r+3))
    return be

def gamma_r(m,r,c):
    ga = (c**2)*(r*(r-1))/((2*m+2*r-3)*(2*m+2*r-1))
    return ga

def d_r_driver(mr,nr,c,theta,ct='prolate'):
    out = np.zeros((2,mr,nr))*1j
    for m in range(mr):
        for n in range(nr):
            out[:,m,n]=retS(m,n+m,c,cos(theta),ct)
            if any(out[:,m,n]!=out[:,m,n]):
                print(m)
                print(n)
                raise ValueError
            out[0,m,n]*=m/sin(theta)
            out[1,m,n]*=-sin(theta)
    return out

def dr_outf(mr,nr,c,eta,ct='prolate'):
    out = np.zeros((2,mr,nr))*1j
    for m in range(mr):
        for n in range(nr):
            out[:,m,n]=np.sum(d_r(m,m+n,10,c,ct,0,0))
    return out

def d_r(m,n,rt,c,ct='prolate',zer=0,normit=1):
    dr = np.zeros(rt)*1j
    div = np.ones(rt)*1j
    oe = int((m-n)&1)
    dr[oe]=1
    norm=2*spec.factorial(oe+2*m)/((2*oe+2*m+1)*spec.factorial(oe))
    if m>0:
        div[0]=1/(m+m**2)
    div[1]=(1/((1+m)*(2+m)))
    if zer==0:
        div[1]=1
        div[0]=1
    for r in range(oe+2,rt,2):
        num_bet=beta_r(m,r,c)
        if ct=='prolate':
            la = spec.pro_cv(m,n,abs(c))
        else:
            la=spec.obl_cv(m,n,c)/spec.pro_cv(m,n,0)
        den_la = gamma_r(m,r,c)-la
        
        frac=0
        for i in np.arange(15,-0.5,-1):
            num_bet=beta_r(m,r+2*(i+1),c)
            den_la-=gamma_r(m,r+2*(i+1),c)+la-frac
            frac=num_bet/den_la

        dr[r]=dr[r-2]*frac/((-alpha_r(m,r-2,c)))
        div[r]=1/((r + m)*(r+m+1))
        if zer==0:
            div[r]=1
        norm+=(((dr[r]**2)*2*(1/(2*r+2*m+1))))*(spm.factorial(r+2*m)/spm.factorial(r))
    if not(n==0):
        normfactor = (-1**((m-n-oe)/2))*spm.factorial(n+m+oe)/((2**(n-m))*spm.factorial((n+m+oe)/2)*spm.factorial((n-m-oe)/2))+0j
    else:
        normfactor=1+0j
        
    #print(normfactor)
    norms=0+ 0j
    for r in range(oe,rt,2):
        norms+=(((dr[r])*(-1**((r-oe)/2))*(spm.factorial(r+2*m+oe))))/((2**(r))*spm.factorial((r+2*m+oe)/2)*spm.factorial((r-oe)/2))
    #print(norms)
    drn = norms/normfactor
    r=4+oe 

    dr=dr/(drn)
    norms=0
    for r in range(oe,rt,2):
        norms+=(((dr[r])*(-1**((r-oe)/2))*(spm.factorial(r+2*m+oe))))/((2**(r))*spm.factorial((r+2*m+oe)/2)*spm.factorial((r-oe)/2))
    #print(norms/normfactor)
       
    #print(dr[oe::2])
    #print(alpha_r(m,r,c)*dr[r+2] + (beta_r(m,r,c)-spec.pro_cv(m,n,abs(c)))*dr[r] + gamma_r(m,r,c)*dr[r-2],m,n)
    if normit==0:        

        return dr[oe::2]
    else:
        dr[oe::2]=dr[oe::2]/(norm*div[oe::2])

        return dr[oe::2]

def d_r_rad(m,n,rt,c,ct='prolate'):
    #c=min(c,100)
    m=int(m)
    n=int(n)
    dr = np.zeros(rt)+0j
    oe = ((m-n)&1)
    dr[oe]=1
    frac=np.zeros(2)+0j
    for r in range(2+oe,rt,2):
        num_bet=beta_r(m,r,c)
        if ct=='prolate':
            la = spec.pro_cv(m,n,abs(c))
        else:
            la=spec.obl_cv(m,n,abs(c))
        den_la = gamma_r(m,r,c)-la
        frac=0+0j
        for i in np.arange(15,-0.5,-1):
            num_bet=beta_r(m,r+2*(i+1),c)
            den_la-=gamma_r(m,r+2*(i+1),c)+la-frac
            frac=num_bet/den_la
        dr[r]=((dr[r-2]*frac/((-alpha_r(m,r-2,c))))*(1/(2*r+2*m+1)))*(spec.factorial(r+2*m)/(spec.factorial(r)))
    if not(n==0):
        normfactor = (-1**((m-n-oe)/2))*spm.factorial(n+m+oe)/((2**(n-m))*spm.factorial((n+m+oe)/2)*spm.factorial((n-m-oe)/2))+0j
    else:
        normfactor=1+0j
    norms=0+0j
    for r in range(oe,rt,2):
        norms+=(((dr[r])*(-1**((r-oe)/2))*(spm.factorial(r+2*m+oe))))/((2**(r))*spm.factorial((r+2*m+oe)/2)*spm.factorial((r-oe)/2))
    #print(norms)
    drn = norms/normfactor
    dr=dr/drn

    return dr
    


def d_r_alt(m,n,rt,c,ct='prolate'):
    dr = np.zeros(rt)*1j
    oe = int((m-n)&1)
    
    ind = int((m-n)&1)
    dr[0:2]=1
    frac=np.zeros(2)+0j
    for r in range(2,rt):
        num_bet=beta_r(m,r,c)
        if ct=='prolate':
            la = spec.pro_cv(m,n,abs(c))
        else:
            la=spec.obl_cv(m,n,c)
        
        den_la = gamma_r(m,r,c)-la
        frac[ind]=0
        for i in np.arange(15,-0.5,-1):
            num_bet=beta_r(m,r+2*(i+1),c)
            den_la-=gamma_r(m,r+2*(i+1),c)+la-frac[ind]
            frac[ind]=num_bet/den_la
        dr[r]=dr[r-2]*frac[ind]/((-alpha_r(m,r-2,c)))
    if not(n==0):
        normfactor = (-1**((m-n-oe)/2))*spm.factorial(n+m+oe)/((2**(n-m))*spm.factorial((n+m+oe)/2)*spm.factorial((n-m-oe)/2))
    else:
        normfactor=1
    norms=0
    for r in range(oe,rt,2):
        norms+=(((dr[r])*(-1**((r-oe)/2))*(spm.factorial(r+2*m+oe))))/((2**(r))*spm.factorial((r+2*m+oe)/2)*spm.factorial((r-oe)/2))
    #print(norms)
    drn = norms/normfactor
    dr=dr/drn
    return dr

def d_coef(m,n,t_in,c,ct='prolate'):
    
    dr = d_r_alt(m,n,t_in+5,c,ct)+0j
    N=np.zeros((9,t_in))
    for t in range(t_in):
        if m>0:
            N[:,t] = 2*spm.factorial(t+2*abs(m)-2)/((2*t+2*abs(m)-1)*spm.factorial(t))
        else:
            N[:,t] = 2*(t+1)*(t+2)/(2*t+3)
    coefs=np.zeros((9,t_in))+0j
    if m > 0:
        for t in range(t_in):
            if ((n-m+t)&1):
                coefs[0,t]=0
            else:
                coefs[0,t]=dr[t]*(t+2*m)*(t+2*m-1)/(2*t+2*m+1)
                if t>0:
                    coefs[0,t]-=dr[t-2]*t*(t-1)/(2*t+2*m+3)
            
            if ((n-m+t)&1):
                coefs[1,t]=0
            else:
                coefs[1,t]=np.sum(dr[t::2])*(2*t+2*m-1)
            
            if not((n-m+t)&1):
                coefs[2,t]=0
            else:
                coefs[2,t]=((t+2*m)*(t+2*m-1)/(2*t+2*m+1))*(dr[t+1]*((t+2*m+1)/(2*t+2*m+3)) + ((t)/(2*t+2*m-1))*dr[t-1])
                if t>2:
                    coefs[2,t]-=(t*(t-1)/(2*t+2*m-3))*(dr[t-1]*((t+2*m-1)/(2*t+2*m-1)) + ((t-2)/(2*t+2*m-5))*dr[t-3])
            
            if not((n-m+t)&1):
                coefs[3,t]=0
            else:
                coefs[3,t]=t*dr[t-1] + (2*t+2*m-1)*np.sum(dr[t+1::2])
            
            
            if ((n-m+t)&1):
                coefs[4,t]=0
            else:
                coefs[4,t]=(((t+2*m-1)*(t+2*m)*(t+2*m+2)*(t+2*m+2))/((2*t+2*m+1)*(2*t+2*m+3)))*((dr[t]/(2*t+2*m+1))-(dr[t+2]/(2*t+2*m+5)))
                if t>0:
                    coefs[4,t]-=((t*(t+2*m-1)*(t+2*m)*(t-1))/((2*t+2*m-3)*(2*t+2*m+1)))*((dr[t-2]/(2*t+2*m-3))-(dr[t]/(2*t+2*m+1)))
                if t>3:
                    coefs[4,t]+=((t*(t-1)*(t-2)*(t-3))/((2*t+2*m-3)*(2*t+2*m-5)))*((dr[t-4]/(2*t+2*m-7))-(dr[t-2]/(2*t+2*m-3)))
            
            if not((n-m+t)&1):
                coefs[5,t]=0
            else:
                coefs[5,t]=(((t+2*m-1)*(t+2*m)*(t+2*m+1)*(t+2*m+2)*(t+2*m+3))/((2*t+2*m+1)*(2*t+2*m+3)*(2*t+2*m+5)))*((dr[t+1]/(2*t+2*m+3))-(dr[t+3]/(2*t+2*m+3)))
                if t>0:
                    coefs[5,t]-=((t*(t+2*m-1)*(t-2*m)*(t+2*m)*(t+2*m-1))/((2*t+2*m-3)*(2*t+2*m+1)*(2*t+2*m+3)))*((dr[t-1]/(2*t+2*m-1))-(dr[t+1]/(2*t+2*m+3)))
                if t>2:
                    coefs[5,t]-=((t*(t-1)*(t-2)*(t+2*m-1)*(t+4*m-1))/((2*t+2*m-5)*(2*t+2*m-3)*(2*t+2*m+1)))*((dr[t-3]/(2*t+2*m-5))-(dr[t-1]/(2*t+2*m-1)))
                if t>4:
                    coefs[5,t]+=((t*(t-1)*(t-2)*(t-3)*(t-4))/((2*t+2*m-3)*(2*t+2*m-5)*(2*t+2*m-7)))*((dr[t-5]/(2*t+2*m-9))-(dr[t-3]/(2*t+2*m-5)))
                    
            if ((n-m+t)&1):
                coefs[6,t]=0
            else:
                coefs[6,t]=-t*(t+m-1)*dr[t-1] + m*(2*t+2*m-1)*np.sum(dr[t+1::2])
                
            if ((n-m+t)&1):
                coefs[7,t]=0
            else:
                coefs[7,t]=-((t*(t-1)*(t+2*m-2))/((2*t+2*m+3)))*dr[t-2]
                if t>0:
                    coefs[7,t]-=((t*(t-1)*(2*t+2*m-1)+(t+2*m-1)*(t+2*m))/(2*(2*t+2*m+1)))*dr[t]
                if t>3:
                    coefs[7,t]+= m*(2*t+2*m-1)*np.sum(dr[t+2::2])
                    
            if not((n-m+t)&1):
                coefs[8,t]=0
            else:
                coefs[8,t]=((t+2*m)*(t+2*m-1)/(2*t+2*m+1))*(dr[t+1]*((t+2*m+1)*(t+m+2)/(2*t+2*m+3)) - ((t*(m+t-1))/(2*t+2*m-1))*dr[t-1])
                if t>2:
                    coefs[8,t]-=(t*(t-1)/(2*t+2*m-3))*(dr[t-1]*((t+m)*(t+2*m-1)/(2*t+2*m-1)) + ((t+m-3)*(t-2)/(2*t+2*m-5))*dr[t-3])
    else:
        for t in range(t_in):
            if not((n-m+t)&1):
                coefs[2,t]=0
            else:
                coefs[2,t]=(1/(2*t+1))*(dr[t+1]*((t+1)/(2*t+3)) + ((t)/(2*max(t,1)-1))*dr[t-1])-(1/(2*t+5))*(dr[t+3]*((t+3)/(2*t+7)) + ((t+3)/(2*t+3))*dr[t+1])
                
            if not((n-m+t)&1):
                coefs[4,t]=0
            else:
                coefs[4,t]=(((t+3)*(t+4)*(t+5))/((2*t+5)*(2*t+7)))*(dr[t+1]/((2*t+5)*(2*t+3)) - dr[t+3]/((2*t+5)*(2*t+9)) + dr[t +5]/((2*t+9)*(2*t+11)))
                if t>2:
                    coefs[4,t]+=((3*t*(t+3))/((2*t+1)*(2*t+5)))*(dr[t-1]/((2*t-1)*(2*t+1)) + dr[t+3]/((2*t+5)*(2*t+7)) + dr[t +1]/((2*t+1)*(2*t+5)))+(((t)*(t-1)*(t-2))/((2*t-1)*(2*t-1)))*(dr[t-3]/((2*t-5)*(2*t-3)) - dr[t-1]/((2*t-3)*(2*t+1)) + dr[t +1]/((2*t+1)*(2*t+3)))
                
            
            if not((n-m+t)&1):
                coefs[5,t]=0
            else:
                coefs[5,t]=dr[t+1]
                
            if not((n-m+t)&1):
                coefs[8,t]=0
            else:
                coefs[8,t]=(1/(2*t+1))*(dr[t+1]*((t+2)*(t+1)/(2*t+3))- ((t*(t-1))/(2*max(t,1)-1))*dr[t-1])-(1/(2*t+5))*(dr[t+3]*((t+3)*(t+4)/(2*t+7)) - ((t+2)*(t+1)/(2*t+3))*dr[t+1])
    return coefs/N
        
        
class spheroid:
    def __init__(self,axis=5e-7,ctype='prolate',ecc=np.sqrt(0.75)):
        self.ecc=ecc
        if ctype=='prolate':
            self.lims = np.array(((-1,1),(1,np.inf),(0,2*np.pi)))
            self.con = -1
            self.axis=axis
            self.xi_0 = 1/ecc
        else:
            self.lims = np.array(((-1,1),(0,np.inf),(0,2*np.pi)))
            self.con=1
            self.axis=axis
            self.xi_0 = np.sqrt(1/(ecc**2)-1)
    def x(self,exphi):
        return self.axis*((1-exphi[0]**2)**(0.5))*((exphi[1]**2 + self.con)**(0.5))*np.cos(exphi[2])
    def y(self,exphi):
        return self.axis*((1-exphi[0]**2)**(0.5))*((exphi[1]**2 + self.con)**(0.5))*np.sin(exphi[2])
    def z(self,exphi):
        return self.axis*exphi[0]*exphi[1]
    def r(self,exphi):
        return np.array((self.x(exphi),self.y(exphi),self.z(exphi)))
    def get_phi(self,r):
        return np.arctan2(np.real(r)[1],np.real(r)[0])
    def get_xi(self,r):
        return (1/(2*self.axis))*(np.sqrt(r[0]**2 + r[1]**2 +(r[2]+self.axis)**2) + np.sqrt(r[0]**2 + r[1]**2 + (r[2]-self.axis)**2))
    def get_eta(self,r):
        return (1/(2*self.axis))*(np.sqrt(r[0]**2 + r[1]**2 +(r[2]+self.axis)**2) - np.sqrt(r[0]**2 + r[1]**2 + (r[2]-self.axis)**2))
    def get_exphi(self,r):
        return np.array((self.get_eta(r),self.get_xi(r),self.get_phi(r)))
    def get_exphi2(self,r,phi):
        return np.array((self.get_eta(r),self.get_xi(r),phi))
    def scaleFactors(self,xk):
        eta=xk[0]
        xi=xk[1]
        phi=xk[2]
        return self.axis*np.array((np.sqrt((1+self.con*eta**2)/(xi**2+self.con*eta**2)),np.power((xi**2+self.con*eta**2)/(xi**2-1),-0.5),np.power((xi**2-1 )*(1+self.con*eta**2),-0.5)))


    
def radialFun(m,n,c,xi,t=1,ct='prolate'):
    rt=15
    oec=oefun(m,n)
    if ct=='prolate':
        const=-1
    else:
        const=1
    pre=(1/(np.sum(d_r_rad(m,n,rt,c,ct))))*(((xi**2+const*1)/xi**2)**(m/2))    
    mfun=np.meshgrid(np.ones(np.size(xi)),np.arange(m,m+rt,1))[1]
    
    oemfun=np.meshgrid(np.ones(np.size(xi)),(1j**(oec+n-m+np.arange(0,rt,1)))*d_r_rad(m,n,rt,c,ct))[1]

    if t==1:    
        R= pre*(np.sum(oemfun*np.array(np.nan_to_num(spec.spherical_jn(mfun,c*xi))),0))
        dR= pre*(np.sum(oemfun*np.array(np.nan_to_num(spec.spherical_jn(mfun,c*xi,derivative=True))),0))
    else:
        R= pre*(np.sum(oemfun*np.array(np.nan_to_num(spec.spherical_jn(mfun,c*xi)+1j*spec.spherical_yn(mfun,c*xi))),0))
        dR= pre*(np.sum(oemfun*np.array(np.nan_to_num(spec.spherical_jn(mfun,c*xi,derivative=True))+1j*spec.spherical_yn(mfun,c*xi,derivative=True)),0))
    R=np.nan_to_num(R)

    dR=np.nan_to_num(dR)
    return R,dR

def oefun(m,n):
    return (m-n)&1
    
def radialFun2(m,n,c,xi,t=1,ct='prolate'):
  
    oec=oefun(m,n)
    rt=4
    if ct=='prolate':
        const=-1
    else:
        const=1
    pre=(1/(np.sum(d_r_rad(m,n,rt,c,ct))))*(((xi**2+const*1)/xi**2)**(m/2))    
    mfun=np.meshgrid(np.ones(np.size(xi)),np.arange(m,m+rt,1))[1]
    
    oemfun=np.meshgrid(np.ones(np.size(xi)),(1j**(oec+n-m+np.arange(0,rt,1)))*d_r_rad(m,n,rt,c,ct))[1]
    
    if t==1:    
        R= pre*(np.sum(oemfun*np.array(np.nan_to_num(spec.spherical_jn(mfun,c*xi))),0))
        dR= pre*(np.sum(oemfun*np.array(np.nan_to_num(spec.spherical_jn(mfun,c*xi,derivative=True))),0))
    else:
        R= pre*(np.sum(oemfun*np.array(np.nan_to_num(spec.spherical_jn(mfun,c*xi)+1j*spec.spherical_yn(mfun,c*xi))),0))
        dR= pre*(np.sum(oemfun*np.array(np.nan_to_num(spec.spherical_jn(mfun,c*xi,derivative=True))+1j*spec.spherical_yn(mfun,c*xi,derivative=True)),0))
    R=np.nan_to_num(R)
    if np.amax(abs(R))>100:
        print(pre*oemfun)
        print(spec.spherical_yn(mfun,c*xi))
        print(R)
        print(c)
        print(R)
        print(xi)
        print(pre)
        print(m,rt)
        raise ValueError
        print(np.sum(d_r_rad(m,n,rt,c,ct)))
        print(oemfun*np.array(np.nan_to_num(spec.spherical_jn(mfun,c*xi)+1j*spec.spherical_yn(mfun,c*xi))))
        print(oemfun)
        print(spec.spherical_yn(mfun,c*xi))    
        print(m)
        #print(n)

    dR=np.nan_to_num(dR)
    return R,dR

def rflim(m,n,c,xi,t=1,ct='prolate'):
    R = (1/(c*xi))*np.exp(1j*(c*xi-(n+1)/(2)*pi))
    return R
    
def RdR(m,n,c,xi,t=1,ct='prolate'):
    R,dR=radialFun2(m,n,c,xi,t,ct)
    return R,dR

def pmIntegral(func,vec):
    if not(len(vec)==2):
        raise ValueError("Phi/Eta must be a tuple (START,END) for angle integral")
            
    t = np.linspace(vec[0],vec[1],NUM_ANG)
    x = func(t)
    return spi.simps(y=x, x=t)/np.abs(vec[1]-vec[0])


def retS(m,n,c=5,eta=0,ctype='prolate'):
    S=np.zeros(15-m)+0j
    ds=np.zeros(15-m)+0j
    S,ds = np.array(spec.clpmn(m,15+m,eta))[:,m,m+((m-n)&1)::2]
    S*=(d_r(m,n,16,c,ctype,normit=0))
    
    ds*=(d_r(m,n,16,c,ctype,normit=0))
    return np.sum(S),np.sum(ds)
    np.sign(sin(m*np.mean(phi)))

def spheroidScalarWave(spheroid,m,n,cin, eta,phi,xi,ctype='prolate',rtype=1):
    
    R,dr = radialFun2(m,n,cin,xi,rtype,ctype)
    c=min(abs(cin),100)
    
    if ctype=='prolate':
        S,ds=retS(m,n,cin,np.mean(eta),ctype)
        if abs(S)>10:
            print(S)
            raise ValueError
        if abs(ds)>10:
            print(ds)
            raise ValueError
        if abs(dss)>10:
            print(dss)
            raise ValueError

        #S,ds,dss=pmIntegral(lambda x: retS(m,n,c,x,ctype),eta)
    else:
        S,ds=retS(m,n,cin,np.mean(eta),ctype)
        #S,ds,dss=pmIntegral(lambda x: retS(m,n,c,x,ctype),eta)
    if DIV_POL>1:
        sinI = pmIntegral(lambda x: (sin(m*x)),phi)
        cosI = pmIntegral(lambda x: (cos(m*x)),phi)
    else:
        sinI=sin(m*np.mean(phi))
        cosI=cos(m*np.mean(phi))
    return m*S*R*cosI,m*S*R*sinI,-ds*R*cosI,-m*S*R*cosI,-ds*R*sinI,-m*S*dr*sinI,-ds*dr*cosI,m*ds*dr*cosI,-ds*dr*sinI, -m*m*S*R*cosI, dss*R*cosI,-m*m*S*R*sinI,dss*R*sinI
    
def spheroidScalarWave2(sfs,vin=[0,0,2],spheroid=0,m=0,n=0,cin=0, ctype='prolate',rtype=1,p_in=0,lamb=lambda x:x,index=0):
    eta,xi,phi=vin     
    R,dr = radialFun2(m,n,cin,np.array([xi]),rtype,ctype)

    
    if ctype=='prolate':
        S,ds=retS(m,n,cin,np.mean(eta),ctype)   
        #S,ds,dss=pmIntegral(lambda x: retS(m,n,c,x,ctype),eta)
    else:
        S,ds=retS(m,n,cin,np.mean(eta),ctype)
        #S,ds,dss=pmIntegral(lambda x: retS(m,n,c,x,ctype),eta)

    if False:
        sinI = pmIntegral(lambda x: (sin(m*x)),phi+(pi/DIV_POL,-pi/DIV_POL))
        cosI = pmIntegral(lambda x: (cos(m*x)),phi+(pi/DIV_POL,-pi/DIV_POL))
    else:
        sinI=sin(m*np.mean(phi))
        cosI=cos(m*np.mean(phi))
    sf=np.array((-sin(np.arccos(eta))*sfs[0],sfs[1],sfs[2]))
    if p_in==1:
        return lamb((spheroid.get_exphi2(spheroid.r((np.mean(eta),xi,np.mean(phi)))*S*R*sinI,np.mean(phi))*sf)[index])#,-ds*R*cosI,-m*S*R*cosI,-ds*R*sinI,-m*S*dr*sinI,-ds*dr*cosI,m*ds*dr*cosI,-ds*dr*sinI, -m*m*S*R*cosI, dss*R*cosI,-m*m*S*R*sinI,dss*R*sinI
    else:
        return lamb(spheroid.get_exphi2(spheroid.r((np.mean(eta),xi,np.mean(phi)))*S*R*cosI,np.mean(phi))*sf)[index]
    
def Mfun(vin,spheroid,k,l,m,n,hs,ctype='prolate',rtype=1,p_in=0,index=4,la=lambda x:x):
    eta,xi,phi=vin
    cin=k*l
    ll=1
    h=np.array((1/hs[0],1/hs[1],1/hs[2]))
    lr=-1*np.ones(3)
    out=np.zeros(3)+0j
    sinf = np.array((-sin(np.arccos(eta)),1,1))
    for i in range(3):
        if index==4:
            if p_in==0:
                
                f=lambda xk:spheroidScalarWave2(h,xk,spheroid,m,n,cin,ctype,rtype,0,lambda a: np.real(a),i)  
                g=lambda xk:spheroidScalarWave2(h,xk,spheroid,m,n,cin,ctype,rtype,0,lambda a: np.imag(a),i)           
                gr = spo.approx_fprime([eta,xi,phi],f,[0.01,spheroid.xi_0*0.01,0.01]) + 1j*spo.approx_fprime([eta,xi,phi],g,[0.01,spheroid.xi_0*0.01,0.01])
                gr*=sinf
                kk=1/k
            else:
                f=lambda xk:spheroidScalarWave2(h,xk,spheroid,m,n,cin,ctype,rtype,1,lambda a: np.real(a),i)
                g=lambda xk:spheroidScalarWave2(h,xk,spheroid,m,n,cin,ctype,rtype,1,lambda a: np.imag(a),i)
                gr = spo.approx_fprime([eta,xi,phi],f,[0.01,spheroid.xi_0*0.01,0.01]) + 1j*spo.approx_fprime([eta,xi,phi],g,[0.01,spheroid.xi_0*0.01,0.01])
                gr*=sinf
                kk=1/k
            for j in range(3):
                if i!=j:
                    for kp in range(3):
                        if (kp!=j) and (kp!=i):
                            lr[j]*=-1
                            out[j]+=lr[j]*(ll*gr[kp]/(h[kp]*h[i]))

        else:       
            if p_in==0:
                f=lambda xk:spheroidScalarWave2(h,xk,spheroid,m,n,cin,ctype,rtype,0,lambda a: np.real(a),i)
                g=lambda xk:spheroidScalarWave2(h,xk,spheroid,m,n,cin,ctype,rtype,0,lambda a: np.imag(a),i)
                gr = spo.approx_fprime([eta,xi,phi],f,[0.01,spheroid.xi_0*0.01,0.01]) + 1j*spo.approx_fprime([eta,xi,phi],g,[0.01,spheroid.xi_0*0.01,0.01])
                kk=1/k
                gr*=sinf
            else:
                f=lambda xk:spheroidScalarWave2(h,xk,spheroid,m,n,cin,ctype,rtype,1,lambda a: np.real(a),i)
                g=lambda xk:spheroidScalarWave2(h,xk,spheroid,m,n,cin,ctype,rtype,1,lambda a: np.imag(a),i)
                gr = spo.approx_fprime([eta,xi,phi],f,[0.01,spheroid.xi_0*0.01,0.01]) + 1j*spo.approx_fprime([eta,xi,phi],g,[0.01,spheroid.xi_0*0.01,0.01])
                kk=1/k
                gr*=sinf
            for j in range(3):
                if i!=j:
                    for kp in range(3):
                        if (kp!=j) and (kp!=i):
                            lr[j]*=-1
                            out[j]+=lr[j]*la((h[j])*(ll*gr[kp]/(h[kp]*h[i])))

    if index==4:
        
        print("M!")
        print(eta)
        print(out)
        return out
    else:
        return la(out[index])
        
def Nfun(vin,spheroid,k,l,m,n,h,ctype='prolate',rtype=1,p_in=0):
    eta,xi,phi=vin
    kk=1/k
    lr=-1*np.ones(3)
    ll=1
    
    N=np.zeros(3)*1j
    for i in range(3):
        if p_in==0:
            f=lambda xk:Mfun(xk,spheroid,k,l,m,n,h,ctype,rtype,0,i,lambda a:np.real(a))
            g=lambda xk:Mfun(xk,spheroid,k,l,m,n,h,ctype,rtype,0,i,lambda a:np.imag(a))
            gr = spo.approx_fprime([eta,xi,phi],f,[0.01,spheroid.xi_0*0.01,0.01]) + 1j*spo.approx_fprime([eta,xi,phi],g,[0.01,spheroid.xi_0*0.01,0.01])
            
        else:
            f=lambda xk:Mfun(xk,spheroid,k,l,m,n,h,ctype,rtype,1,i,lambda a:np.real(a))
            g=lambda xk:Mfun(xk,spheroid,k,l,m,n,h,ctype,rtype,1,i,lambda a:np.imag(a))
            gr = spo.approx_fprime([eta,xi,phi],f,[0.01,spheroid.xi_0*0.01,0.01]) + 1j*spo.approx_fprime([eta,xi,phi],g,[0.01,spheroid.xi_0*0.01,0.01])
        for j in range(3):
            if i!=j:
                for kp in range(3):
                    if (kp!=j) and (kp!=i):
                        lr[j]*=-1
                        N[j]+=lr[j]*kk*(gr[kp])*h[kp]*h[i]

    print("N!")
    print(eta)
    print(N)
    return ll*N
def spheroidVectorWave(spheroid,k,l,m,n,eta,phi,xi,h,ctype='prolate',rtype=1):
    #psi,me1,me2,mo1,mo2,ne2,ne1,no2,no1,ne31,ne32,no31,no32 = spheroidScalarWave(spheroid,m,n,k*l,eta,phi,xi,ctype,rtype)
    xk=[np.mean(eta),spheroid.xi_0,np.mean(phi)]
    ll=np.ones((3,len(xi)))
    Me = (Mfun(xk,spheroid,k,l,m,n,h,ctype,rtype,0))[:,]@ll
    Mo = (Mfun(xk,spheroid,k,l,m,n,h,ctype,rtype,1))[:,]@ll    
    Ne=(Nfun(xk,spheroid,k,l,m,n,h,ctype,rtype,0))[:,]@ll
    No=(Nfun(xk,spheroid,k,l,m,n,h,ctype,rtype,1))[:,]@ll

    #Me = (1j**(n+1))*np.array((xi*(psi*hj[2,1]+h[2]*me1)/(h[1]*h[2]),xi*(h[2]*me2/(h[0]*h[2])),np.zeros(len(me1))))
    #Mo = (1j**(n+1))*np.array((xi*(me1*hj[2,1] + h[2]*mo1)/(h[1]*h[2]),xi*(h[2]*mo2*(h[0]*h[2])),np.zeros(len(me1))))
    #No = kk * (1j**(n))*np.array((no1*(h[0]*h[1]*h[1]*h[2]),no2*(h[0]*h[0]*h[1]*h[2]),no31*(h[1]*h[2]*h[1]*h[2])+no32*(h[0]*h[2]*h[0]*h[2])))
    #Ne = kk *(1j**(n))*np.array((ne1*(h[0]*h[1]*h[1]*h[2]),ne2*(h[0]*h[0]*h[1]*h[2]),ne31*(h[1]*h[2]*h[1]*h[0])+ne32*(h[0]*h[2]*h[0]*h[1])))

    return Me,No,Mo,Ne

def get_fg(msize,nsize,spheroid,kin,l,eta,phi,xi,zeta,Hin,ctype='prolate'):    
    
    k=kin[0]
    H=Hin[0]
    sfactor=np.sin(zeta)
    if zeta==0:
        sfactor=1
    rf = np.zeros((msize,nsize))*1j
    rf2 = np.copy(rf)
    if not(ctype=='prolate'):
        k=-1j*k
    Pp=np.zeros((msize,nsize))
    P=np.ones((msize,nsize))+0j
    dP=np.ones((msize,nsize))+0j
    m=msize
    n=nsize
    oe = int((msize-nsize)&1)               
    sph,dsph=spec.lpmn(msize-1,msize+nsize-1,cos(zeta))
    if zeta==0:
        P[1]=1
        dP[1]=1
        P[0]=0
        dP[0]=0
        P[2:]=0
        dP[2:]=0
    for m in range(msize):
        for n in range(nsize):
            if ((m==1)&(zeta==0)):
                rf[m,n] = 2*np.sum(d_r(m,n+m,nsize,k*l,ctype,0))
                rf2[m,n] = 2*np.sum(d_r(m,n+m,nsize,k*l,ctype,0))
            else:
                oe=int((n)&1)
                moe=int(not((m)&1))
              #  print(oe)
                rf[m,n] = np.sum(((-1)**m)*sph[m,m+oe:m+nsize:2]*d_r(m,n+m,nsize,k*l,ctype,1))*4*m/(spm.factorial(2+oe)**(2))
                rf2[m,n] = -sfactor*np.sum(((-1)**m)*dsph[m,m+oe:m+nsize:2]*d_r(m,m+n,nsize,k*l,ctype,1))*2*(2-int(m==0))/(spm.factorial(2+oe)**(2))
                if zeta==0:
                    rf[m,n]=0
                    rf2[m,n]=0
    g=dP*rf2*sfactor
    f=P*rf/sfactor
    print(g)
    print(f)
    if zeta==0:
        print("ZETA = 0")
  #  print(f[0])
  #  print(g[0])
   # print(P[0])
  #  print(dP[0])
  #  print(rf[0])
 #   print(rf2[0])
#    print(sph[0])
#    print(dsph[0])
    return f,g

def solver_engine_alt(ms,ns,ui,uo,uox,vi,vo,vox,xi,xo,xox,yi,yo,yox,f,g,hb):
    iv = 1j**np.arange(0,ns,1)
    no=np.ones((ms,1))
    iv=iv[np.newaxis]
    iv2 = 1j**np.arange(0,ms,1)
    iv2=iv2[np.newaxis]
    iv = (iv.transpose()@iv2).transpose()
    et_lhs=np.zeros((ms,ns,4))*1j
    et_rhs=np.zeros((ms,ns,2))*1j
    ph_lhs=np.zeros((ms,ns,4))*1j
    ph_rhs=np.zeros((ms,ns,2))*1j
    h_et_lhs=np.zeros((ms,ns,4))*1j
    h_et_rhs=np.zeros((ms,ns,2))*1j
    h_ph_lhs=np.zeros((ms,ns,4))*1j
    h_ph_rhs=np.zeros((ms,ns,2))*1j
    et_lhs[:,:,0]=iv*vo[:,:,0]
    et_lhs[:,:,1]=iv*uo[:,:,0]
    et_lhs[:,:,2]=-iv*vi[:,:,0]
    et_lhs[:,:,3]=-iv*ui[:,:,0]
    et_rhs[:,:,0]=-iv*f*vox[:,:,0]
    et_rhs[:,:,1]=-iv*g*uox[:,:,0]
    ph_lhs[:,:,0]=iv*yo[:,:,0]
    ph_lhs[:,:,1]=iv*xo[:,:,0]
    ph_lhs[:,:,2]=-iv*yi[:,:,0]
    ph_lhs[:,:,3]=-iv*xi[:,:,0]
    ph_rhs[:,:,0]=-iv*f*yox[:,:,0]
    ph_rhs[:,:,1]=-iv*g*xox[:,:,0]
    h_et_lhs[:,:,0]=iv*uo[:,:,1]
    h_et_lhs[:,:,1]=iv*vo[:,:,1]
    h_et_lhs[:,:,2]=-iv*ui[:,:,1]
    h_et_lhs[:,:,3]=-iv*vi[:,:,1]
    h_et_rhs[:,:,0]=-iv*f*uox[:,:,1]
    h_et_rhs[:,:,1]=-iv*g*vox[:,:,1]
    h_ph_lhs[:,:,0]=iv*xo[:,:,1]
    h_ph_lhs[:,:,1]=iv*yo[:,:,1]
    h_ph_lhs[:,:,2]=-iv*xi[:,:,1]
    h_ph_lhs[:,:,3]=-iv*yi[:,:,1]
    h_ph_rhs[:,:,0]=-iv*f*xox[:,:,1]
    h_ph_rhs[:,:,1]=-iv*g*yox[:,:,1]
    et_rhs=np.sum(et_rhs,(2))
    ph_rhs=np.sum(ph_rhs,(2))
    h_et_rhs=np.sum(h_et_rhs,(2))
    h_ph_rhs=np.sum(h_ph_rhs,(2))
    llist = np.zeros((ms,4,ns*4))*0j
    rlist=np.zeros((ms,4,ns))*0j
    print(rlist.shape)
   # print(llist.shape)
    l1=np.zeros((ms,ns,4))+0j
    for m in range(ms):
        for n in range(ns):
            l1[m,n]=np.array(np.linalg.lstsq([et_lhs[m,n],ph_lhs[m,n],h_et_lhs[m,n],h_ph_lhs[m,n]],[et_rhs[m,n],ph_rhs[m,n],h_et_rhs[m,n],h_ph_rhs[m,n]]))[0]
    print(l1[1,2])

    return l1


def solver_engine(ms,ns,ts,ui,uo,uox,vi,vo,vox,xi,xo,xox,yi,yo,yox,f,g,hb):
    iv = 1j**np.arange(0,ns,1)
    iv = iv[:,np.newaxis]@np.ones((1,ts))
    no=np.ones((ms,1))
    iv=iv[np.newaxis]
    iv2 = 1j**np.arange(0,ms,1)
    iv2=iv2[np.newaxis]
    iv = (iv.transpose()@iv2).transpose()
    hb=hb[1]/hb[0]
    et_lhs=np.zeros((ms,ns,ts,4))*1j
    et_rhs=np.zeros((ms,ns,ts,2))*1j
    ph_lhs=np.zeros((ms,ns,ts,4))*1j
    ph_rhs=np.zeros((ms,ns,ts,2))*1j
    h_et_lhs=np.zeros((ms,ns,ts,4))*1j
    h_et_rhs=np.zeros((ms,ns,ts,2))*1j
    h_ph_lhs=np.zeros((ms,ns,ts,4))*1j
    h_ph_rhs=np.zeros((ms,ns,ts,2))*1j
    et_lhs[:,:,:,0]=iv*vo
    et_lhs[:,:,:,1]=iv*uo
    et_lhs[:,:,:,2]=-iv*vi
    et_lhs[:,:,:,3]=-iv*ui
    et_rhs[:,:,:,0]=-iv*f*vox
    et_rhs[:,:,:,1]=-iv*g*uox
    ph_lhs[:,:,:,0]=iv*yo
    ph_lhs[:,:,:,1]=iv*xo
    ph_lhs[:,:,:,2]=-iv*yi
    ph_lhs[:,:,:,3]=-iv*xi
    ph_rhs[:,:,:,0]=-iv*f*yox
    ph_rhs[:,:,:,1]=-iv*g*xox
    h_et_lhs[:,:,:,0]=iv*uo
    h_et_lhs[:,:,:,1]=iv*vo
    h_et_lhs[:,:,:,2]=-iv*ui*hb
    h_et_lhs[:,:,:,3]=-iv*vi*hb
    h_et_rhs[:,:,:,0]=-iv*f*uox
    h_et_rhs[:,:,:,1]=-iv*g*vox
    h_ph_lhs[:,:,:,0]=iv*xo
    h_ph_lhs[:,:,:,1]=iv*yo
    h_ph_lhs[:,:,:,2]=-iv*xi*hb
    h_ph_lhs[:,:,:,3]=-iv*yi*hb
    h_ph_rhs[:,:,:,0]=-iv*f*xox
    h_ph_rhs[:,:,:,1]=-iv*g*yox
    et_rhs=np.sum(et_rhs,(1,3))
    ph_lhs=np.sum(ph_lhs,1)
    ph_rhs=np.sum(ph_rhs,(1,3))
    h_et_lhs=np.sum(h_et_lhs,1)
    h_et_rhs=np.sum(h_et_rhs,(1,3))
    h_ph_lhs=np.sum(h_ph_lhs,1)
    h_ph_rhs=np.sum(h_ph_rhs,(1,3))
    et_lhs=np.sum(et_lhs,1)
    l1=np.zeros((ms,ts,4))+0j
    for m in range(ms):
        for t in range(ts):
            l1[m,t]=np.linalg.solve([et_lhs[m,t],ph_lhs[m,t],h_et_lhs[m,t],h_ph_lhs[m,t]],[et_rhs[m,t],ph_rhs[m,t],h_et_rhs[m,t],h_ph_rhs[m,t]])

    return l1

def job_runner(L,in_list):
    l_te,l_tm,f,g,spheroid,hin,k,l,msize,nsize,eta,phi,xi,sf,ctype,rtype=in_list
    EMLt = EMiter(np.nan_to_num(l_te),np.nan_to_num(l_tm),f,g,spheroid,hin,k,l,msize,nsize,eta,phi,xi,sf,ctype,rtype)
    return EMLt,eta,phi

def EMiter_ASYM(l_te,l_tm,f,g,spheroid,hin,k,l,m,nsize,eta,phi,xi,sf,ctype='prolate',rtype=1):
    k=abs(k)
    th = np.linspace(0,np.pi,NUM)
    Tii = np.zeros((2,NUM,2),dtype=complex)
    for n in range(nsize):
        Tii[0,:,0]+=sigTH(m,n+m,k[0]*l,th,ctype)[0]*l_te[m,n,0] + sigTH(m,n+m,k[1]*l,th,ctype)[1]*l_te[m,n,1]
        Tii[0,:,1]+=sigTH(m,n+m,k[0]*l,th,ctype)[1]*l_te[m,n,0] + sigTH(m,n+m,k[0]*l,th,ctype)[1]*l_te[m,n,1]
        Tii[1,:,0]+=sigTH(m,n+m,k[0]*l,th,ctype)[1]*l_te[m,n,0] + sigTH(m,n+m,k[0]*l,th,ctype)[1]*l_tm[m,n,1]
        Tii[0,:,1]+=sigTH(m,n+m,k[0]*l,th,ctype)[0]*l_te[m,n,0] + sigTH(m,n+m,k[1]*l,th,ctype)[1]*l_tm[m,n,1]
                
    return Tii


def EMiter(l_te,l_tm,f,g,spheroid,hin,k,l,msize,nsize,eta,phi,xi,sf,ctype='prolate',rtype=1):
    xn = spheroid.xi_0
    EMLt = np.zeros((12,3,NUM),dtype=complex)
    xis1 = np.array(((xi<xn), (xi<xn),(xi<xn)),dtype=np.complex128)
    xis1x = np.array(((xi>=xn), (xi>=xn),(xi>(xi+1))),dtype=np.complex128)
    xis2 = np.array(((xi>=xn),(xi>=xn),(xi>=xn)),dtype=np.complex128)
    if zet==0:
        msize=1
    for m in range(msize):
        print(m)
        if zet==0:
            m=1
        for n in range(nsize):
            
            #term1,term2,term3,term4 = (spheroidVectorWave(spheroid,k[0],l,m,n+m,eta,phi,np.concatenate((xi[xi<xn],np.flip(xi[xi>=xn]))),sf,'prolate',1))
            term1,term2,term3,term4 = (spheroidVectorWave(spheroid,k[0],l,m,n+m,eta,phi,xi,sf,ctype,1))

    #        term1,term2,term3,term4 = (spheroidVectorWave(spheroid,k[0],l,m,n+m,(0.999,1),(0,0.0001),xi,sf,ctype,1))
            EMLt[0] += (1j**(n))*(term2*f[m,n]*1j+ term1*g[m,n])*xis2
            EMLt[1] +=hin[0]*(1j**(n))*(term3*f[m,n]- 1j*term4*g[m,n])*xis2
            EMLt[2] += (1j**(n))*(term3*f[m,n]- 1j*term4*g[m,n])*xis2
            EMLt[3] +=-hin[0]*(1j**(n))*(term2*g[m,n]+ 1j*term1*f[m,n])*xis2
           # if abs(np.sum(EMLt[0,:,-1]**2,0))>5:
                #print(EMLt[0,:,-1])
                #print(m)
                #print(n)
                #print(term1)
                #print(term2)
                #print(term3)
                #print(term4)
                ##raise ValueError
            
            term1,term2,term3,term4 = (spheroidVectorWave(spheroid,k[1],l,m,n+m,eta,phi,xi,sf,ctype,1))
            EMLt[4] += (1j**(n))*(term2*l_te[m,n,3]*1j+ term1*l_te[m,n,2])*xis1
            EMLt[5] +=hin[1]*(1j**(n))*(term3*l_te[m,n,3]- 1j*term4*l_te[m,n,2])*xis1
            EMLt[6] += (1j**(n))*(term3*l_tm[m,n,2]- 1j*term4*l_tm[m,n,3])*xis1
            EMLt[7] +=-hin[1]*(1j**(n))*(term2*l_tm[m,n,3]+ 1j*term1*l_tm[m,n,2])*xis1

            term1,term2,term3,term4 = (spheroidVectorWave(spheroid,k[0],l,m,n+m,eta,phi,xi,sf,ctype,3))

            EMLt[8] += (1j**(n))*(term2*l_te[m,n,1]*1j+ term1*l_te[m,n,0])*xis2
            EMLt[9] +=hin[0]*(1j**(n))*(term3*l_te[m,n,0]- 1j*term4*l_te[m,n,1])*xis2
            EMLt[10] += (1j**(n))*(term3*l_tm[m,n,0]- 1j*term4*l_tm[m,n,1])*xis2
            EMLt[11] +=-hin[0]*(1j**(n))*(term2*l_tm[m,n,1]+ 1j*term1*l_tm[m,n,0])*xis2
    return EMLt

#def wave_solver_alt(msize,nsize,spheroid,k,l,eta,phi,xi0,zeta,hin,xis,ctype='prolate'):
    #global EML
    #c=k*l
    #U_in = np.zeros((msize,nsize,2))         +0j      
    #V_in = np.zeros((msize,nsize,2))       +0j
    #U_out = np.zeros((msize,nsize,2))      +0j
    #V_out = np.zeros((msize,nsize,2))      +0j
    #X_in = np.zeros((msize,nsize,2))         +0j      
    #Y_in = np.zeros((msize,nsize,2))       +0j
    #X_out = np.zeros((msize,nsize,2))      +0j
    #Y_out = np.zeros((msize,nsize,2))      +0j
    #U_outx = np.zeros((msize,nsize,2))         +0j      
    #V_outx = np.zeros((msize,nsize,2))       +0j
    #X_outx = np.zeros((msize,nsize,2))      +0j
    #Y_outx = np.zeros((msize,nsize,2))      +0j
    #l_te = np.zeros((msize,nsize,4))+0j
    #l_tm = np.zeros((msize,nsize,4))+0j
    #f=np.zeros((msize,nsize))
    #g=np.zeros((msize,nsize))
    #ph1=np.mean(phi,0)
    #eth=np.mean(eta,0)
    #sf=spheroid.scaleFactors(np.mean(eta),np.mean(phi),np.array(xi_0))+0j
    #co=0
    #for m in range(msize):
        #for n in range(nsize):
            #term1, term2, term3, term4 = (spheroidVectorWave(spheroid,k[0],l,m,n+m,ph1,eth,xi_0,sf,'prolate',1))
                   #term1, term2, term3, term4 = (spheroidVectorWave(spheroid,k[0],l,m,n+m,(0.999,1),(0,0.0001),xi_0,sf,'prolate',1))
            #U_outx[m,n,0] = term1[0]
            #X_outx[m,n,0] = term1[1]            
            #V_outx[m,n,0] = term2[0]*1j
            #Y_outx[m,n,0] = term2[1]*1j            
            #U_outx[m,n,1] = hin[0]*term3[0]
            #X_outx[m,n,1] = hin[0]*term3[1]
            #V_outx[m,n,1] = -hin[0]*term4[0]*1j
            #Y_outx[m,n,1] = -hin[0]*term4[1]*1j
            #term1,term2,term3,term4 = (spheroidVectorWave(spheroid,k[1],l,m,n+m,ph1,eth,xi_0,sf,'prolate',1))
            #U_in[m,n,0] = term1[0] 
            #X_in[m,n,0] = term1[1]
            #V_in[m,n,0] = term2[0]*1j

            #Y_in[m,n,0] = term2[1]*1j 
            #U_in[m,n,1] = hin[1]*term3[0]
            #X_in[m,n,1] = hin[1]*term3[1]
            #V_in[m,n,1] = -hin[1]*term4[0]*1j
            #Y_in[m,n,1] = -hin[1]*term4[1]*1j
            #term1,term2,term3,term4 = (spheroidVectorWave(spheroid,k[0],l,m,n+m,ph1,eth,xi_0,sf,'prolate',3))
            #U_out[m,n,0] = term1[0]

            #X_out[m,n,0] = term1[1]            

            #V_out[m,n,0] = term2[0]*1j          

            #Y_out[m,n,0] = term2[1]*1j
            #U_out[m,n,1] = hin[0]*term3[0]
            #X_out[m,n,1] = hin[0]*term3[1]
            #V_out[m,n,1] = -hin[0]*term4[0]*1j
            #Y_out[m,n,1] = -hin[0]*term4[1]*1j         
    #f,g= get_fg(msize,nsize,spheroid,k,l,ph1,eth,xi0+1e-9,zeta,hin,ctype='prolate')  
    #l_te = solver_engine_alt(msize,nsize,U_in,U_out,U_outx,V_in,V_out,V_outx,X_in,X_out,X_outx,Y_in,Y_out,Y_outx,f,g,hin)
    #for m in range(msize):
        #for n in range(nsize):
            #term1, term2, term3, term4 = (spheroidVectorWave(spheroid,k[0],l,m,n+m,ph1,eth,xi_0,sf,'prolate',1))
            #U_outx[m,n,0] =-term4[0]*1j
            #X_outx[m,n,0] = -term4[1]*1j            
            #V_outx[m,n,0] = term3[0]
            #Y_outx[m,n,0] = term3[1]            
            #U_outx[m,n,1] = -hin[0]*term2[0]
            #X_outx[m,n,1] = -hin[0]*term2[1]
            #V_outx[m,n,1] = -hin[0]*term1[0]*1j
            #Y_outx[m,n,1] = -hin[0]*term1[1]*1j
            #term1,term2,term3,term4 = (spheroidVectorWave(spheroid,k[1],l,m,n+m,ph1,eth,xi_0,sf,'prolate',1))
            #U_in[m,n,0] = -term4[0]*1j
            #X_in[m,n,0] = -term4[1]*1j
            #V_in[m,n,0] = term3[0]

            #Y_in[m,n,0] = term3[1] 
            #U_in[m,n,1] = -hin[1]*term2[0]
            #X_in[m,n,1] = -hin[1]*term2[1]
            #V_in[m,n,1] = -hin[1]*term1[0]*1j
            #Y_in[m,n,1] = -hin[1]*term1[1]*1j
            #term1,term2,term3,term4 = (spheroidVectorWave(spheroid,k[0],l,m,n+m,ph1,eth,xi_0,sf,'prolate',3))
            #U_out[m,n,0] = -term4[0]*1j

            #X_out[m,n,0] = -term4[1]*1j            

            #V_out[m,n,0] = term3[0]          

            #Y_out[m,n,0] = term3[1]
            #U_out[m,n,1] = -hin[0]*term2[0]
            #X_out[m,n,1] = -hin[0]*term2[1]
            #V_out[m,n,1] = -hin[0]*term1[0]*1j
            #Y_out[m,n,1] = -hin[0]*term1[1]*1j    
    #l_tm = solver_engine_alt(msize,nsize,U_in,U_out,U_outx,V_in,V_out,V_outx,X_in,X_out,X_outx,Y_in,Y_out,Y_outx,f,g,hin)
    #cv = np.zeros((msize,nsize)) + 0j
    #xi=xis
    #ind = np.sum(np.array((xi<xi0),dtype=np.uint32)==1)
    #start = time.time()
    #print("Working...")
    #etph=[]
    #if ASYM==0:
        #count=0
        #start = time.time()
        #LEN=len(eta)*len(phi)
        #for eeta in eta:
            #for phhi in phi:
                #count=count+1
                #sf = spheroid.scaleFactors(np.mean(eeta),np.mean(phhi),np.array(xi_0))+0j
                #etph.append((l_te,l_tm,f,g,spheroid,hin,k,l,msize,nsize,eeta,phhi,xi,sf,ctype,1))

        #with multiprocessing.Pool(processes=4) as pool:
            #EMLt=(((pool.starmap(job_runner,zip(range(LEN),etph)))))

        #print("Done!")
        #end = time.time()
        #print("Elapsed time: " + str(end - start))
        #print("Time per run: " + str((end - start)/(count)))
        #return EMLt
    #else:
        #count = 0
        #dro=np.zeros((2,msize,nsize,NUM))*1j
        #for th in np.linspace(1e-6,pi-1e-6,NUM):
            #dro[:,:,:,count] = d_r_driver(msize,nsize,(k[0])*l,cos(th),ctype)
            #count+=1
        #TH = np.linspace(1e-6,pi-1e-6,NUM)
        #l_te=l_te[:,:nsize,:,np.newaxis]
        #l_tm=l_tm[:,:nsize,:,np.newaxis]
        #lv = l_te@np.ones((1,NUM))
        #lu = l_tm@np.ones((1,NUM))
        #print(dro.shape)
        #print(lv.shape)
        #Tii = np.zeros((2,NUM,2))*1j
        #mp = np.meshgrid(np.zeros(nsize),np.arange(0,msize,1)*phi,np.zeros(NUM))[1]
        #Tii[0,:,0]=np.sum((dro[0]*lv[:,:,0,:] + dro[1]*lv[:,:,1,:])*cos(mp),(0,1))
        #Tii[0,:,1]=np.sum((dro[1]*lv[:,:,0,:] + dro[0]*lv[:,:,1,:])*sin(mp),(0,1))
        #Tii[1,:,0]=np.sum((dro[1]*lu[:,:,0,:] + dro[0]*lu[:,:,1,:])*sin(mp),(0,1))
        #Tii[1,:,1]=np.sum((dro[0]*lu[:,:,0,:] + dro[1]*lu[:,:,1,:])*cos(mp),(0,1))
        #print("Done!")
        #end = time.time()
        #print("Elapsed time: " + str(end - start))
        #return Tii

def wave_solver(msize,nsize,spheroid,k,l,eta,phi,xi0,zeta,hin,xis,ctype='prolate'):
    global EML
    tsize=35
    c=k*l
    R_in = np.zeros((msize,nsize))         +0j      
    dr_in = np.zeros((msize,nsize))       +0j
    R_inx = np.zeros((msize,nsize))         +0j      
    dr_inx = np.zeros((msize,nsize))       +0j
    R_out = np.zeros((msize,nsize))      +0j
    dr_out = np.zeros((msize,nsize))      +0j
    Ri = np.zeros((msize,nsize))       +0j
    dri = np.zeros((msize,nsize))       +0j
    lambdas_in=np.zeros((msize,nsize))       +0j
    lambdas_out=np.zeros((msize,nsize))       +0j
    coefs_in = np.zeros((msize,nsize,9,tsize))+0j
    coefs_out = np.zeros_like(coefs_in)+0j
    for m in range(msize):
        for n in range(nsize):
            
            coefs_in[m,n]=d_coef(m,n+m,tsize,c[1])
            

            coefs_out[m,n]=d_coef(m,n+m,tsize,c[0])
            if ctype=='prolate':
                rr= RdR(m,n+m,k[1]*l,np.array(xi0),1,ctype)
                R_in[m,n] =rr[0]
                dr_in[m,n] =rr[1]
                rr = RdR(m,n+m,k[0]*l,np.array(xi0),3,ctype)
                R_out[m,n] =rr[0]
                dr_out[m,n] =rr[1]
                rr= RdR(m,n+m,k[0]*l,np.array(xi0),1,ctype)
                R_inx[m,n] =rr[0]
                dr_inx[m,n] =rr[1]
                cst=1
                lambdas_in[m,n]=spec.pro_cv(m,n+m,np.abs(c[1]))
                lambdas_out[m,n]=spec.pro_cv(m,n+m,np.abs(c[0]))
            else:
                rr= RdR(m,n+m,k[1]*l,np.array(xi0),1,ctype)
                R_in[m,n] =rr[0]
                dr_in[m,n] =rr[1]
                rr = RdR(m,n+m,k[0]*l,np.array(xi0),3,ctype)
                R_out[m,n] =rr[0]
                dr_out[m,n] =rr[1]
                rr= RdR(m,n+m,k[0]*l,np.array(xi0),1,ctype)
                R_inx[m,n] =rr[0]
                dr_inx[m,n] =rr[1]
                cst=-1
                lambdas_in[m,n]=spec.obl_cv(m,n+m,np.abs(c[1]))
                lambdas_out[m,n]=spec.obl_cv(m,n+m,np.abs(c[0]))
                c*=-1j
    

    R_in = R_in[:,:,np.newaxis]@np.ones((1,tsize))
    R_out = R_out[:,:,np.newaxis]@np.ones((1,tsize))
    dr_out = dr_out[:,:,np.newaxis]@np.ones((1,tsize))
    
    dr_in = dr_in[:,:,np.newaxis]@np.ones((1,tsize))

    R_inx = R_inx[:,:,np.newaxis]@np.ones((1,tsize))
    dr_inx = dr_inx[:,:,np.newaxis]@np.ones((1,tsize))
    mz = ((np.arange(0,msize,1)[:,np.newaxis]@np.ones((1,nsize))))[:,:,np.newaxis]@np.ones((1,tsize))
    U_in = mz*xi0*R_in*(((xi0**2-1)**2)*coefs_in[:,:,1] + 2*(xi0**2 - 1)*coefs_in[:,:,0] + coefs_in[:,:,4])
    U_out= mz*xi0*R_out*(((xi0**2-1)**2)*coefs_out[:,:,1] + 2*(xi0**2 - 1)*coefs_out[:,:,0] + coefs_out[:,:,4])
    V_in = (1j/c[1])*(((mz**2)/(xi0**2-1))*R_in*(((xi0**2-1)**2)*coefs_in[:,:,3]+2*(xi0**2-1)*coefs_in[:,:,2]+coefs_in[:,:,5]) - R_in*(lambdas_in[:,:,np.newaxis]@np.ones((1,tsize))-(c[1]*xi0)**2+(mz**2)/(xi0**2 - 1))*((xi0**2 - 1)*coefs_in[:,:,2] + coefs_in[:,:,5])+xi0*(xi0**2-1)*dr_in*(2*coefs_in[:,:,2]+(xi0**2-1)*coefs_in[:,:,6]+coefs_in[:,:,8])+R_in*(((xi0**2 - 1)**2)*coefs_in[:,:,6]+(3*xi0**2-1)*coefs_in[:,:,8]))
        
    X_in = xi0*R_in*coefs_in[:,:,6]-dr_in*coefs_in[:,:,2]
    
    Y_in = mz*((1j/c[1])*((1/(xi0**2-1))*R_in*(coefs_in[:,:,0]+coefs_in[:,:,7])+(R_in+xi0*(dr_in))*coefs_in[:,:,1]))
    
    V_out = (1j/c[0])*(((mz**2)/(xi0**2-1))*R_out*(((xi0**2-1)**2)*coefs_out[:,:,3]+2*(xi0**2-1)*coefs_out[:,:,2]+coefs_out[:,:,5]) - R_out*(lambdas_out[:,:,np.newaxis]@np.ones((1,tsize))-(c[0]*xi0)**2+(mz**2)/(xi0**2 - 1))*((xi0**2 - 1)*coefs_out[:,:,2] + coefs_out[:,:,5])+xi0*(xi0**2-1)*dr_out*(2*coefs_out[:,:,2]+(xi0**2-1)*coefs_out[:,:,6]+coefs_out[:,:,7])+R_out*(((xi0**2 - 1)**2)*coefs_out[:,:,6]+(3*xi0**2-1)*coefs_out[:,:,8]))
    
    X_out = xi0*R_out*coefs_out[:,:,6]-dr_out*coefs_out[:,:,2]
    
    Y_out = mz*((1j/c[0])*((1/(xi0**2-1))*R_out*(coefs_out[:,:,0]+coefs_out[:,:,7])+(R_out+xi0*(dr_out))*coefs_out[:,:,1]))
    
    V_outx = (1j/c[0])*(((mz**2)/(xi0**2-1))*R_inx*(((xi0**2-1)**2)*coefs_out[:,:,3]+2*(xi0**2-1)*coefs_out[:,:,2]+coefs_out[:,:,5]) - R_inx*(lambdas_out[:,:,np.newaxis]@np.ones((1,tsize))-(c[0]*xi0)**2+(mz**2)/(xi0**2 - 1))*((xi0**2 - 1)*coefs_out[:,:,2] + coefs_out[:,:,5])+xi0*(xi0**2-1)*dr_in*(2*coefs_out[:,:,2]+(xi0**2-1)*coefs_out[:,:,6]+coefs_out[:,:,7])+R_inx*(((xi0**2 - 1)**2)*coefs_out[:,:,6]+(3*xi0**2-1)*coefs_out[:,:,8]))

    X_outx = xi0*R_inx*coefs_out[:,:,6]-dr_inx*coefs_out[:,:,2]
    

    Y_outx = mz*((1j/c[0])*((1/(xi0**2-1))*R_inx*(coefs_out[:,:,0]+coefs_out[:,:,7])+(R_inx+xi0*(dr_inx))*coefs_out[:,:,1]))
    U_outx= mz*xi0*R_inx*(((xi0**2-1)**2)*coefs_out[:,:,1] + 2*(xi0**2 - 1)*coefs_out[:,:,0] + coefs_out[:,:,4])
    

    #U_out[0]=0
    #U_in[0]=0
    #U_outx[0]=0
    #Y_out[0]=0
    #Y_in[0]=0
    #Y_outx[0]=0
    np.nan_to_num(V_in,copy=False)
    np.nan_to_num(Y_in,copy=False)
    np.nan_to_num(X_in,copy=False)
    np.nan_to_num(U_in,copy=False)
    np.nan_to_num(V_out,copy=False)
    np.nan_to_num(Y_out,copy=False)
    np.nan_to_num(X_out,copy=False)
    np.nan_to_num(U_out,copy=False)
    np.nan_to_num(V_outx,copy=False)
    np.nan_to_num(Y_outx,copy=False)
    np.nan_to_num(X_outx,copy=False)
    np.nan_to_num(U_outx,copy=False)    

    
    #May need to add minus constants for oblate system
    f,g = get_fg(msize,nsize,spheroid,k,l,eta,phi,xi0,zeta,hin,ctype)  
    np.nan_to_num(g,copy=False)
    np.nan_to_num(f,copy=False)
    f = f[:,:,np.newaxis]@np.ones((1,tsize))
    g = g[:,:,np.newaxis]@np.ones((1,tsize))

    l_te = solver_engine(msize,nsize,tsize,U_in,U_out,U_outx,V_in,V_out,V_outx,X_in,X_out,X_outx,Y_in,Y_out,Y_outx,f,g,hin)
    l_tm = solver_engine(msize,nsize,tsize,V_in,V_out,V_outx,U_in,U_out,U_outx,Y_in,Y_out,Y_outx,X_in,X_out,X_outx,f,g,hin)
    cv = np.zeros((msize,nsize)) + 0j
    xi=xis
    ind = np.sum(np.array((xi<xi0),dtype=np.uint32)==1)
    f=f[:,:,0]
    g=g[:,:,0]
    start = time.time()
    print("Working...")
    if ASYM==0:
        count=0
        start = time.time()
        count=0
        start = time.time()
        LEN=len(eta)*len(phi)
        etph=[]
        for eeta in eta:
            for phhi in phi:
                count+=1
                sf =(1/(spheroid.scaleFactors([np.mean(eeta),np.array(xi_0),np.mean(phhi)])))+0j
                etph.append((l_te,l_tm,f,g,spheroid,hin,k,l,msize,nsize,eeta,phhi,xi,sf,ctype,1))

        with multiprocessing.Pool(processes=NUM_CORES) as pool:
            EMLt=(((pool.starmap(job_runner,zip(range(LEN),etph))))) 
        print("Done!")
        end = time.time()
        print("Elapsed time: " + str(end - start))
        print("Time per run: " + str((end - start)/(count)))
        return EMLt
    else:
        count = 0
        dro=np.zeros((2,msize,nsize,NUM2))*1j
        for th in np.linspace(1e-6,pi-1e-6,NUM2):
            dro[:,:,:,count] = d_r_driver(msize,nsize,(k[0])*l,th,ctype)
            count+=1
        TH = np.linspace(1e-6,pi-1e-6,NUM2)
        l_te=l_te[:,:nsize,:,np.newaxis]
        l_tm=l_tm[:,:nsize,:,np.newaxis]
        lv = l_te@np.ones((1,NUM2))
        lu = l_tm@np.ones((1,NUM2))
        print(dro.shape)
        print(lv.shape)
        Tii = np.zeros((2,NUM2,2))*1j
        mp = np.meshgrid(np.zeros(nsize),np.arange(0,msize,1)*phi,np.zeros(NUM2))[1]
        print(mp)
        #for n in range(NUM2):
            #Tii[0,n,0]=np.sum((dro[0,:,:,n]*lv[:,:,0,n] + dro[1,:,:,n]*lv[:,:,1,n])*cos(mp[:,:,n]),(0,1))
            #Tii[0,n,1]=np.sum((dro[1,:,:,n]*lv[:,:,0,n] + dro[0,:,:,n]*lv[:,:,1,n])*sin(mp[:,:,n]),(0,1))
            #Tii[1,n,0]=np.sum((dro[1,:,:,n]*lu[:,:,0,n] + dro[0,:,:,n]*lu[:,:,1,n])*sin(mp[:,:,n]),(0,1))
            #Tii[1,n,1]=np.sum((dro[0,:,:,n]*lu[:,:,0,n] + dro[1,:,:,n]*lu[:,:,1,n])*cos(mp[:,:,n]),(0,1))
            #print(lv[1,:,0,n])
        
        Tii[0,:,0]=np.sum((dro[0]*lv[:,:,0,:] + dro[1]*lv[:,:,1,:])*cos(mp),(0,1))
        Tii[0,:,1]=np.sum((dro[1]*lv[:,:,0,:] + dro[0]*lv[:,:,1,:])*sin(mp),(0,1))
        Tii[1,:,0]=np.sum((dro[1]*lu[:,:,0,:] + dro[0]*lu[:,:,1,:])*sin(mp),(0,1))
        Tii[1,:,1]=np.sum((dro[0]*lu[:,:,0,:] + dro[1]*lu[:,:,1,:])*cos(mp),(0,1))
        print("Done!")
        end = time.time()
        print("Elapsed time: " + str(end - start))
        return Tii
    
def ax_in(sc,pol,ze):
    #xax = lambda x,i,nul:(i[1]*sin(ze) + (i[0]+(x))*(cos(ze))+nul*(1-cos(ze)))*(x>nul) + nul*(x<=nul)
    #yax = lambda x,i,nul:(i[1]*cos(ze)+(i[0]+(x))*sin(ze)-nul*np.sin(ze))*(x>nul)
    #zax = lambda x,i,nul:i[2]
    xax = lambda x,i,nul:(i[1]*sin(ze) + (i[0]+(x))*(cos(ze))+nul*(1-cos(ze)))*(x>nul) + nul*(x<=nul)
    yax = lambda x,i,nul:(i[1]*cos(ze) + (i[0]+(x))*sin(ze)-nul*np.sin(ze))*(x>nul)
    zax = lambda x,i,nul:(i[2] )*(x>nul)
    return xax,yax,zax
def ax_sc(sc,pol,zep):   
    ze=-sc
    pol=-pol
    xax = lambda x,i,nul:(i[1]*sin(ze) + (i[0]+(x))*(cos(ze))+nul*(1-cos(ze)))*(x>nul) + nul*(x<=nul)
    yax = lambda x,i,nul:(i[1]*cos(ze)+(i[0]+cos(pol)*(x))*sin(ze)-nul*np.sin(ze)*cos(pol))*(x>nul)
    zax = lambda x,i,nul:(i[2]+((x))*sin(ze)*sin(pol)-nul*np.sin(ze)*sin(pol))*(x>nul)
    return xax,yax,zax

def ax_t(frac,sc,pol,zep): 
    ze = -frac*sc
    xax = lambda x,i,nul:(i[1]*sin(ze) + (i[0]+(x))*(cos(ze))+nul*(1-cos(ze)))*(x<=nul) + nul*(x>nul)
    yax = lambda x,i,nul:(i[1]*cos(ze)+(i[0]+cos(pol)*(x))*sin(ze)-nul*np.sin(ze)*cos(pol))*(x<=nul)
    zax = lambda x,i,nul:(i[2]+((x))*sin(ze)*sin(pol)-nul*np.sin(ze)*sin(pol))*(x<=nul)
    return xax,yax,zax

def ax_in_alt(sc,pol,ze):
    #xax = lambda x,i,nul:(i[1]*sin(ze) + (i[0]+(x))*(cos(ze))+nul*(1-cos(ze)))*(x>nul) + nul*(x<=nul)
    #yax = lambda x,i,nul:(i[1]*cos(ze)+(i[0]+(x))*sin(ze)-nul*np.sin(ze))*(x>nul)
    #zax = lambda x,i,nul:i[2]
    xax = lambda x,i,nul:(i[1]*sin(ze) + (i[0]+(x))*(cos(ze))+nul*(1-cos(ze)))*(x>nul) + nul*(x<=nul)
    yax = lambda x,i,nul:(i[1]*cos(ze) + (i[0]+(x))*sin(ze)-nul*np.sin(ze))*(x>nul)
    zax = lambda x,i,nul:(i[2] )*(x>nul)
    sl = lambda x,nul,i: abs(np.amax(((i[0]**2+i[1]**2+i[2]**2))[(abs(x-nul)<3*nul/len(x))]))
    return xax,yax,zax,sl

def ax_sc_alt(sc,pol,zep):   
    ze=-sc
    pol=-pol
    xax = lambda x,i,nul:( ((x))*(cos(ze))+nul*(1-cos(ze)))*(x>nul) + nul*(x<=nul)
    yax = lambda x,i,nul:((cos(pol)*(x))*sin(ze)-nul*np.sin(ze)*cos(pol))*(x>nul)
    zax = lambda x,i,nul:(((x))*sin(ze)*sin(pol)-nul*np.sin(ze)*sin(pol))*(x>nul)
    sl = lambda x,nul,i: abs(np.amax(((i[0]**2+i[1]**2+i[2]**2))[(abs(x-nul)<3*nul/len(x))]))
    return xax,yax,zax,sl

def ax_t_alt(frac,sc,pol,zep): 
    ze = -sc
    xax = lambda x,i,nul:( ((x))*(cos(ze))+nul*(1-cos(ze)))*(x<=nul) + nul*(x>nul)
    yax = lambda x,i,nul:((cos(pol)*(x))*sin(ze)-nul*np.sin(ze)*cos(pol))*(x<=nul)
    zax = lambda x,i,nul:(((x))*sin(ze)*sin(pol)-nul*np.sin(ze)*sin(pol))*(x<=nul)
    sl = lambda x,nul,i: abs(np.amax((((i[0]**2+i[1]**2+i[2]**2)))[(abs(x-nul)<3*nul/len(x))]))
    return xax,yax,zax,sl

def mags(emlt,phi,eta,xr):
    emt = np.copy(emlt)
    n=0
    for n in range(12):
        if True:
            #emt[n][0]=0
            #emt[n][1]=0
            #emt[n][2]=np.sign(emlt[n][1] + emlt[n][0])*np.sqrt(emlt[n][1]**2 + emlt[n][0]**2)*np.sqrt(xr**2-1)*np.sqrt(-eta**2+1)
            
            emt[n][0]=emlt[n][2]*eta
            emt[n][1]=emlt[n][1]*np.sqrt(xr**2-1)*np.sqrt(-eta**2+1)
            emt[n][2]=emlt[n][0]*np.sqrt(xr**2-1)*np.sqrt(-eta**2+1)
        #if (n//2&1):
            ##emt[n][0]=emlt[n][2]*eta
            ##emt[n][2]=0
            ##emt[n][1]=np.sign(emlt[n][1] + emlt[n][0])*np.sqrt(emlt[n][1]**2 + emlt[n][0]**2)*np.sqrt(xr**2-1)*np.sqrt(-eta**2+1)
            
            #emt[n][0]=emlt[n][2]*eta
            #emt[n][1]=emlt[n][1]*np.sqrt(xr**2-1)*np.sqrt(-eta**2+1)
            #emt[n][2]=emlt[n][0]*np.sqrt(xr**2-1)*np.sqrt(-eta**2+1)
        #if not(n==0 or n==2):
            #emt[n][0]=emlt[n][2]*eta
            #emt[n][1]=emlt[n][0]*np.sqrt(xr**2-1)*np.sqrt(-eta**2+1)
            #emt[n][2]=emlt[n][1]*np.sqrt(xr**2-1)*np.sqrt(-eta**2+1)
    #for n in range(3):
        #emt[4*n][0]=emlt[4*n][2]
        #emt[4*n][1]=emlt[4*n][0]
        #emt[4*n][2]=emlt[4*n][1]
        #emt[4*n+1][0]=emlt[4*n+1][0]
        #emt[4*n+1][1]=emlt[4*n+1][1]
        #emt[4*n+1][2]=emlt[4*n+1][2]
        #emt[4*n+2][0]=emlt[4*n+2][0]
        #emt[4*n+2][1]=emlt[4*n+2][1]
        #emt[4*n+2][2]=emlt[4*n+2][2]
        #emt[4*n+3][0]=emlt[4*n+3][2]
        #emt[4*n+3][1]=emlt[4*n+3][0]
        #emt[4*n+3][2]=emlt[4*n+3][1]
    return emt

 #E and H fields

#def wave_generator(l_te,l_tm):


#KEY:
#EML 0, 4, 8 = ETE incident, transmitted, scattered
#EML 1, 5, 9 = HTE incident, transmitted, scattered
#EML 2, 6, 10 = ETM incident, transmitted, scattered
#EML 3, 7, 11 = HTM incident, transmitted, scattered
#const=-1
#m=1
#n=2

#xi=5
#rt=30
#c=2+0j
#ct='prolate'
#print((1/np.sum(d_r_rad(m,n,rt,c,"prolate")))*(((2**2+const*1)/2**2)**(1/2)))
#mfun=np.meshgrid(np.ones(np.size(xi)),np.arange(m,m+rt,1))[1]
#oec=oefun(m,n)
#oemfun=np.meshgrid(np.ones(np.size(xi)),(1j**(oec+n-m+np.arange(0,rt,1)))*d_r_rad(m,n,rt,c,ct))[1]
#print(np.sum(d_r_rad(m,n,rt,c,"prolate")))
#print(mfun)
#print(oemfun)
#print(spec.spherical_yn(mfun,c*xi))
#print(radialFun2(m,n,c,xi,3))
#raise ValueError
ctype='prolate'
wvl = 5
k0=5/(wvl)
w=k0*con.c
Hb=np.array((1.05,1.3 + 1j*(4*np.pi*1e-15/w)))
k=np.zeros(2,dtype=complex)
k=k0*Hb
sphr = spheroid(axis=5,ctype=ctype)
#raise ValueError
Xr = np.linspace(0.1*sphr.axis,3*sphr.axis,NUM)
zet=0.0

xis=sphr.get_xi(np.array([Xr*np.cos(zet),Xr*np.sin(zet),np.zeros(NUM)]))
xi_0=sphr.xi_0
aai = np.sum(xis<xi_0)
xis[aai] = xi_0
print(xis)
print(xi_0)
e,t=np.meshgrid(np.linspace(-0.9999,0.9999,300),np.linspace(0,np.pi*2,300))
Xsr =sphr.x(np.array((e,np.ones((300,300))*xi_0,t)))
Ysr =sphr.y(np.array((e,np.ones((300,300))*xi_0,t)))

#for m in range(10):
    #for n in range(m,20-m):
        #ss = np.array(retS(m,n))[(m-n)&1]
        #cc = np.array(spec.clpmn(m,n,0))[(m-n)&1,m,n]
        #print(m,n,ss,cc)
        #print(ss/cc)
#raise ValueError

zz1,zz2=np.meshgrid(np.linspace(-0.9999,0.99999,300),xi_0*np.ones(300))
Zsr=sphr.axis*(zz1*zz2)

phr = np.linspace(PHI_MIN,PHI_MAX,DIV_POL+2)
etr = np.linspace(np.arccos(ETA_MIN),np.arccos(ETA_MAX),DIV_AZIM+2)
eeta=[]
phhi=[]
for ky in range(DIV_AZIM):
    eeta.append((cos(etr[ky+1]),cos(etr[ky+1])))
for ky in range(DIV_POL):
    phhi.append((phr[ky+1],phr[ky+1]))

xis=sphr.get_xi(np.array([Xr*np.cos(zet),Xr*np.sin(zet),np.zeros(NUM)]))
#fig1=plt.figure()
#ax1=fig1.add_subplot()
ai=np.zeros((100,3))
ai2=np.zeros((100,3))
ind=np.zeros((100,3))
for i in range(100):
    ai[i] = sphr.scaleFactors((-0.99 + 0.018*i,2,3.14))
    ai2[i] = sphr.scaleFactors((-0.95 ,2+ 0.018*i,3.14))
    ind[i]=i
#ax1.plot(ind,ai)
#ax1.plot(ind,ai2)
load=0
plot_now=True
if ASYM == 0:
    if load==0:
        EMLL = (wave_solver(3,6,sphr,k,sphr.axis,eeta,phhi,xi_0,zet,Hb,xis,ctype))
        text=['ETE0','ETE1','ETE2','HTE0','HTE1','HTE2','ETM0','ETM1','ETM2','HTM0','HTM1','HTM2']
        dat = open('data2.dat','wb')
        pickle.dump(EMLL,dat)
    else:
        dat = open('data2.dat','rb')
        EMLL=pickle.load(dat)
    axes=[]
    altplot=True
    first=True
    print(np.shape(EMLL))
    if altplot:
        fig=plt.figure()
        ax1=fig.add_subplot(111,projection='3d')
        fig2=plt.figure()
        ax2=fig2.add_subplot(211)
        ax3=fig2.add_subplot(212)
        fig3=plt.figure()
        ax4=fig3.add_subplot(321,projection='polar')
        ax5=fig3.add_subplot(322,projection='polar')
        ax6=fig3.add_subplot(323,projection='polar')
        ax7=fig3.add_subplot(324,projection='polar')
        ax8=fig3.add_subplot(325,projection='polar')
        ax9=fig3.add_subplot(326,projection='polar')
        alt_list=np.zeros((DIV_AZIM*DIV_POL,12))*1j
        bl=0
        coou=-1
    for EMLt,eta,phi in EMLL:
        axx=0
        coou=coou+1
        dashstyles=np.array([[3,1],[6,1,2,1],[3,1,2,1],[5,3],[5,3,1,3],[5,1,3,1,1,1],[2,1,2,2],[4,1,2,1,2,1]])

        if not(altplot):        
            fig=plt.figure()
            ax1=(fig.add_subplot(111,projection='3d'))
        axes.append(ax1)
        aai = np.sum(xis<sphr.xi_0)
        Xr+=0
       # print(aai)
      #  print(eta)
        ets = -np.mean(eta)
        phs = np.mean(phi)
        scangle=np.arccos(ets)
            
        x_nul=Xr[aai]
        LMBD=lambda x: (np.real(x))
        EMLt = (EMLt)
        EMLt = mags(np.real(EMLt),phs,-ets,xis) + 1j*mags(np.imag(EMLt),phs,-ets,xis)    

        if not(altplot):
            EMLT=np.real(EMLT)
            XSC,YSC,ZSC = ax_sc(scangle,phs,zet)
            ax1.plot(XSC(Xr,(EMLt[8])[:],x_nul),YSC(Xr,(EMLt[8])[:],x_nul),ZSC(Xr,(EMLt[8])[:],x_nul),dashes=dashstyles[4],label='Scattered TE wave',color=(0.9,0.1,0.1))
            ax1.plot(XSC(Xr,(EMLt[10])[:],x_nul),YSC(Xr,(EMLt[10])[:],x_nul),ZSC(Xr,(EMLt[10])[:],x_nul),dashes=dashstyles[4],label='Scattered TM wave',color=(0.2,0.8,0.8))
            #ax1.plot(Xr*(np.cos(zet))+np.sin(zet)*(EMLt[8])[:,0],(EMLt[8])[:,0]*np.cos(zet) - Xr*(np.sin(zet))+x_nul*np.tan(zet),(EMLt[8])[:,1],dashes=dashstyles[2],label='Scattered TE wave',color=(0.9,0.1,0.1))
            #ax1.plot(Xr*(np.cos(zet))+np.sin(zet)*(EMLt[10])[:,0],(EMLt[10])[:,0]*np.cos(zet)- Xr*(np.sin(zet))+x_nul*np.tan(zet),(EMLt[10])[:,1],dashes=dashstyles[4],label='Scattered TM wave',color=(0.2,0.8,0.8))

            XIN,YIN,ZIN=ax_in(scangle,phs,zet)
            ax1.plot(XIN(Xr,(EMLt[0]),x_nul),YIN(Xr,(EMLt[0]),x_nul),ZIN(Xr,(EMLt[0]),x_nul),dashes=dashstyles[3],label='Incident TE wave',color=(0.55,0.1,0.1))
            ax1.plot(XIN(Xr,(EMLt[2]),x_nul),YIN(Xr,(EMLt[2]),x_nul),ZIN(Xr,(EMLt[2]),x_nul),dashes=dashstyles[3],label='Incident TM wave',color=(0.15,0.6,0.6))
            
            XSC2,YSC2,ZSC2 = ax_t(np.abs(Hb[0]/Hb[1]),scangle,phs,zet)
            ax1.plot(XSC2(Xr,(EMLt[4])[:],x_nul),YSC2(Xr,(EMLt[4])[:],x_nul),ZSC2(Xr,(EMLt[4])[:],x_nul),dashes=dashstyles[0],label='Transmitted TE wave',color=(0.9,0.1,0.1))
            ax1.plot(XSC2(Xr,(EMLt[6])[:],x_nul),YSC2(Xr,(EMLt[6])[:],x_nul),ZSC2(Xr,(EMLt[6])[:],x_nul),dashes=dashstyles[0],label='Transmitted TM wave',color=(0.2,0.8,0.8))

            #ax.plot(Xr*np.cos(zet),Xr*np.sin(zet)-x_nul*np.sin(zet)/np.cos(zet),Xr*0,dashes=dashstyles[7],alpha=0.8)
            #ax.plot(Xr*np.cos(zet),-Xr*np.sin(zet)+x_nul*np.sin(zet)/np.cos(zet),Xr*0,dashes=dashstyles[7],alpha=0.8)
            ax1.plot((x_nul,x_nul),(0,0),(0,0),'o',markersize=24)
            ax1.set_title("$\eta$ = " + "{0:.3g}".format(eta[0]) + "~" + "{0:.3g}".format(eta[1]) + ", $\phi$ = " + "{0:.3g}".format(phi[0]) + "~" + "{0:.3g}".format(phi[1]))
        #  ax.plot_surface(Zsr,Xsr,Ysr,linewidth=0,alpha=0.2)
            ax1.legend()
            #ax1.set_xlim((0.5*x_nul,1.5*x_nul))
            #ax1.set_zlim((-0.5*x_nul,0.5*x_nul))   
            #ax1.set_ylim((-0.5*x_nul,0.5*x_nul))
        else:
           # print(np.mean(EMLt[0],1))
            #print(np.mean(EMLt[2],1))
            alt_list[coou,0]=ets
            alt_list[coou,1]=phs
            XIN,YIN,ZIN,sl=ax_in_alt(scangle,phs,zet)
            bl+=(sl(Xr,x_nul,(np.abs(EMLt[0])))+sl(Xr,x_nul,(np.abs(EMLt[2]))))*(4*pi/(DIV_AZIM*DIV_POL))
            XSC,YSC,ZSC,sl = ax_sc_alt(scangle,phs,zet)
            #ax1.plot(XSC(Xr,(EMLt[8]+EMLt[10])[:],x_nul),YSC(Xr,(EMLt[8]+EMLt[10])[:],x_nul),ZSC(Xr,(EMLt[8]+EMLt[10])[:],x_nul),color=(1-np.clip(sl(Xr,x_nul,EMLt[8]+EMLt[10])/bl,0,1),0.8,0.8))
            zeroone = np.zeros_like(EMLt[8])
            zerotwo=np.copy(zeroone)            
            zeroone[1]+=1
            zerotwo[0]+=1
            zeroone[2]+=1
            zerothree=np.copy(zeroone)
            print(EMLt[0,:,-1])
            print(EMLt[1,:,-1])
            #raise ValueError
            alt_list[coou,2]=np.sqrt(sl(Xr,x_nul,np.abs(EMLt[8]*zerotwo)))
            alt_list[coou,3]=np.sqrt(sl(Xr,x_nul,np.abs(EMLt[10]*zeroone)))  


            alt_list[coou,4]=np.sqrt(sl(Xr,x_nul,np.abs(EMLt[8])*zeroone))
            alt_list[coou,5]=np.sqrt(sl(Xr,x_nul,np.abs(EMLt[10])*zerotwo))
            #ax1.plot(Xr*(np.cos(zet))+np.sin(zet)*(EMLt[8])[:,0],(EMLt[8])[:,0]*np.cos(zet) - Xr*(np.sin(zet))+x_nul*np.tan(zet),(EMLt[8])[:,1],dashes=dashstyles[2],label='Scattered TE wave',color=(0.9,0.1,0.1))
            #ax1.plot(Xr*(np.cos(zet))+np.sin(zet)*(EMLt[10])[:,0],(EMLt[10])[:,0]*np.cos(zet)- Xr*(np.sin(zet))+x_nul*np.tan(zet),(EMLt[10])[:,1],dashes=dashstyles[4],label='Scattered TM wave',color=(0.2,0.8,0.8))
        #    print(sl(Xr,nul,abs(EMLt[8])+abs(EMLt[10]))/bl)
#           
            XSC2,YSC2,ZSC2,tl = ax_t_alt(np.abs(Hb[0]/Hb[1]),scangle,phs,zet)            
            alt_list[coou,6]=np.sqrt(tl(Xr,x_nul,np.abs(EMLt[4]*zerotwo)))
            alt_list[coou,7]=np.sqrt(tl(Xr,x_nul,np.abs(EMLt[6]*zeroone)))  
            alt_list[coou,8]=np.sqrt(tl(Xr,x_nul,np.abs(EMLt[4])*zeroone))
            alt_list[coou,9]=np.sqrt(tl(Xr,x_nul,np.abs(EMLt[6])*zerotwo))
            alt_list[coou,10]=np.sqrt(sl(Xr,x_nul,np.abs(EMLt[0]*zerothree)))
            alt_list[coou,11]=np.sqrt(sl(Xr,x_nul,np.abs(EMLt[2]*zerothree)))
            

            #ax1.plot(XSC2(Xr,(EMLt[4]+EMLt[6])[:],x_nul),YSC2(Xr,(EMLt[4]+EMLt[6])[:],x_nul),ZSC2(Xr,(EMLt[4]+EMLt[6])[:],x_nul),color=(0.9,0.1,1-np.clip(tl(Xr,x_nul,EMLt[4]+EMLt[6])/bl,0,1)))
         #   print(tl(Xr,x_nul,abs(EMLt[4])+abs(EMLt[6]))/bl)
#            ax1.plot(XSC2(Xr,(EMLt[6])[:],x_nul),YSC2(Xr,(EMLt[6])[:],x_nul),ZSC2(Xr,(EMLt[6])[:],x_nul),dashes=dashstyles[0],label='Transmitted TM wave',color=(0.2,0.8,0.8))

            #ax.plot(Xr*np.cos(zet),Xr*np.sin(zet)-x_nul*np.sin(zet)/np.cos(zet),Xr*0,dashes=dashstyles[7],alpha=0.8)
            #ax.plot(Xr*np.cos(zet),-Xr*np.sin(zet)+x_nul*np.sin(zet)/np.cos(zet),Xr*0,dashes=dashstyles[7],alpha=0.8)
            
            ax1.set_title("Diffuse scattering and transmission")
        #  ax.plot_surface(Zsr,Xsr,Ysr,linewidth=0,alpha=0.2)


            if False:
                print(phs)
                ax1.plot((x_nul,x_nul),(0,0),(0,0),'o',markersize=24)
                ax1.plot((x_nul,x_nul),(0,0),(0,0),'-',color=(0.2,0.8,0.8),label='Scattered ray')
                ax1.plot((x_nul,x_nul),(0,0),(0,0),'-',color=(0.9,0.1,0.1),label='Transmitted ray')
                ax1.plot(XIN(Xr,(EMLt[0]),x_nul),YIN(Xr,(EMLt[0]),x_nul),ZIN(Xr,(EMLt[0]),x_nul),label='Incident TE wave',color=(0.55,0.1,0.1))
                ax1.plot(XIN(Xr,(EMLt[2]),x_nul),YIN(Xr,(EMLt[2]),x_nul),ZIN(Xr,(EMLt[2]),x_nul),label='Incident TM wave',color=(0.1,0.1,0.9))
                ax1.legend()
                first=False
            #ax1.set_xlim((0.5*x_nul,1.5*x_nul))
            #ax1.set_zlim((-0.5*x_nul,0.5*x_nul))   
            #ax1.set_ylim((-0.5*x_nul,0.5*x_nul))
    if altplot:
        alt_list[:,2:12] = np.abs(alt_list[:,2:12])
        ax2.plot(alt_list[:,0]+0.055,alt_list[:,2:4],'+')
        ax3.plot(alt_list[:,1]+0.05,alt_list[:,2:4],'+')
        ax2.plot(alt_list[:,0]+0.005,alt_list[:,4:6],'o')
        ax3.plot(alt_list[:,1],alt_list[:,4:6],'o')
        ax2.plot(alt_list[:,0]-0.055,alt_list[:,6:8],'^')
        ax3.plot(alt_list[:,1]-0.05,alt_list[:,6:8],'^')
        ax2.plot(alt_list[:,0]-0.035,alt_list[:,8:10],'v')
        ax3.plot(alt_list[:,1]-0.03,alt_list[:,8:10],'v')
        ax2.plot(alt_list[:,0]+0.035,alt_list[:,10:12],'s')
        ax3.plot(alt_list[:,1]+0.03,alt_list[:,10:12],'s')
        tempi=np.zeros((DIV_AZIM,10))
        temp=np.zeros((10))
        for kl in range(10):
            for n in range(DIV_AZIM):
                print(np.abs(alt_list[n::DIV_AZIM,2+kl]))
                print(np.linspace(0,2*pi,DIV_POL+1))
                tempi[n,kl]=(np.trapz(np.abs((np.concatenate(([0],np.concatenate((alt_list[n::DIV_AZIM,2+kl],[0])))))),(np.linspace(0,2*pi,DIV_POL+2))))
        for n in range(10):
            temp[n]=np.trapz(abs((np.concatenate(([0],np.concatenate((tempi[:,n],[0])))))),np.linspace(-1,1,DIV_AZIM+2))
        print(alt_list[::DIV_AZIM,1])
        print(temp/(2*pi))
        coll = np.zeros((3,DIV_AZIM*DIV_POL))
        alt_list[:,2:8]=np.clip(alt_list[:,2:8],1e-6,1-1e-6)
        coll[2]=abs(np.sqrt(alt_list[:,2] ))
        coll[0]=abs(np.sqrt(1-(alt_list[:,2] )))
        
        ax4.scatter(alt_list[:,1],1-np.cos(alt_list[:,0]),c=abs(coll.transpose()),label='S TE (total)')
        ax4.legend()
        coll[2]=abs(np.sqrt(alt_list[:,3] ))
        coll[0]=abs(np.sqrt(1-(alt_list[:,3] )))
       # print(coll[0])
       # print(coll[2])
        ax5.scatter(alt_list[:,1],1-np.cos(alt_list[:,0]),c=abs(coll.transpose()),label='S TM (total)')
        ax5.legend()
        coll[2]=abs(np.sqrt(alt_list[:,4] ))
        coll[0]=abs(np.sqrt(1-(alt_list[:,4] )))
        ax6.scatter(alt_list[:,1],1-np.cos(alt_list[:,0]),c=abs(coll.transpose()),label='S TE (cross)')
        ax6.legend()
        coll[2]=abs(np.sqrt(alt_list[:,5] ))
        coll[0]=abs(np.sqrt(1-(alt_list[:,5] )))
        ax7.scatter(alt_list[:,1],1-np.cos(alt_list[:,0]),c=abs(coll.transpose()),label='S TM (cross)')        
        coll[2]=abs(np.sqrt(alt_list[:,6]))
        coll[0]=abs(np.sqrt(np.clip(1-alt_list[:,6],1e-6,1.-1e-6)))
        ax8.scatter(alt_list[:,1],1-np.cos(alt_list[:,0]),c=abs(coll.transpose()),label='S + T TE (Total)')
        ax8.legend()
        coll[2]=abs(np.sqrt(alt_list[:,7]))
        coll[0]=abs(np.sqrt(np.clip(1-(alt_list[:,7] ) ,1e-6,1-1e-6)))
        ax9.scatter(alt_list[:,1],1-np.cos(alt_list[:,0]),c=abs(coll.transpose()),label='S + T TM (Total)')
        ax9.legend()
        ax2.legend(['S TE','S TM','S TE xP', 'S TM xP', 'T TE','T TM','T TE xP','T TM xP',"I TE","I TM"])
        ax3.legend(['S TE','S TM','S TE xP', 'S TM xP', 'T TE','T TM','T TE xP','T TM xP'])
        
else: 
    EMLt = abs(wave_solver(6,25,sphr,k,sphr.axis,0,0,xi_0,zet,Hb,xi_0))
    text=['ETE0','ETE1','ETE2','HTE0','HTE1','HTE2','ETM0','ETM1','ETM2','HTM0','HTM1','HTM2']
    axx=0
    dashstyles=np.array([[3,1],[6,1,2,1],[3,1,2,1],[5,3],[5,3,1,3],[5,1,3,1,1,1],[2,1,2,2],[4,1,2,1,2,1]])
    fig=plt.figure()
    ax1=(fig.add_subplot(111,projection='3d'))
    axes=[ax1]
    x_nul=sphr.z((1,(sphr.xi_0),0))
    print(x_nul)
    aai = np.sum(xis<sphr.xi_0)
    print(np.shape(EMLt))
    print("TE: "+ str(np.trapz(np.sum(EMLt[0]*np.conj(EMLt[0]),1),np.linspace(-1,1,NUM2))))
    print("TM: " +str(np.trapz(np.sum(EMLt[1]*np.conj(EMLt[1]),1),np.linspace(-1,1,NUM2))))
    print("c:" + str(k*sphr.axis))    
    
    a1 = (np.log10(np.real(EMLt[0]**2)[:,0]))
    b1 = (np.log10(np.real(EMLt[0]**2)[:,1]))
    a2 = (np.log10(np.real(EMLt[1]**2)[:,0]))
    b2 = (np.log10(np.real(EMLt[1]**2)[:,1]))
    print(a1)
    print(b1)
    ax1.plot(np.linspace(0,np.pi,NUM2),a1,np.zeros_like(b1),dashes=dashstyles[2],label='Scattered TE wave',color=(0.8,0.1,0.1))
    ax1.plot(np.linspace(0,np.pi,NUM2),np.zeros_like(a2),b2,dashes=dashstyles[4],label='Scattered TM wave',color=(0.5,0.4,0.1))


    ax1.set_xlabel('Angle (radians)')
    ax1.set_ylabel('Amplitude (AU)')
    ax1.set_zlabel('Amplitude (AU)')
    ax1.set_title('$\zeta$ = ' + str(zet))
    #for ax in axes:
        #ax.plot(Xr*np.cos(zet),Xr*np.sin(zet)-x_nul*np.sin(zet)/np.cos(zet),Xr*0,dashes=dashstyles[7],alpha=0.8)
        #ax.plot(Xr*np.cos(zet),-Xr*np.sin(zet)+x_nul*np.sin(zet)/np.cos(zet),Xr*0,dashes=dashstyles[7],alpha=0.8)
        #ax.plot((x_nul,x_nul),(0,0),(0,0),'o')
        
        #ax.set_xlim((Xr[0],Xr[-1]))
        #ax.set_zlim((-Xr[0],Xr[-1]))   
        #ax.set_ylim((-Xr[0],Xr[-1]))

if plot_now:
    plt.show()
