from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import math
import numpy as np
import csv
import time
import os
import threading as thr
import time
import sys
import voxelgeo
from electro import *
#import props
from math import floor,ceil
from scipy import signal as sps
from scipy import fftpack as spf
from scipy import linalg as spl
from scipy import ndimage as spn
from abc import ABC,abstractmethod
from numpy import linalg as lgnp
from scipy import optimize as spo
from scipy import constants as con
from scipy.spatial.transform import Rotation as R

from mpl_toolkits.mplot3d.art3d import Poly3DCollection

np.random.seed(time.gmtime())

def cuboid_data(o, size=(1,1,1)):
    X = [[[0, 1, 0], [0, 0, 0], [1, 0, 0], [1, 1, 0]],
         [[0, 0, 0], [0, 0, 1], [1, 0, 1], [1, 0, 0]],
         [[1, 0, 1], [1, 0, 0], [1, 1, 0], [1, 1, 1]],
         [[0, 0, 1], [0, 0, 0], [0, 1, 0], [0, 1, 1]],
         [[0, 1, 0], [0, 1, 1], [1, 1, 1], [1, 1, 0]],
         [[0, 1, 1], [0, 0, 1], [1, 0, 1], [1, 1, 1]]]
    X = np.array(X).astype(float)
    for i in range(3):
        X[:,:,i] *= size[i]
    X += np.array(o)
    return X

def plotCubeAt(positions,sizes=None,colors=None, **kwargs):
    #if not isinstance(colors,(list,np.ndarray)): colors=["C0"]*len(positions)
    if not isinstance(sizes,(list,np.ndarray)): sizes=[(1,1,1)]*len(positions)
    g = []
    for p,s,c in zip(positions,sizes,colors):
        g.append( cuboid_data(p, size=s) )
    return Poly3DCollection(np.concatenate(g),  
                            facecolors=np.repeat(colors,6, axis=0), **kwargs)



c = incohwave(1e15)
PW = planeWave(np.array((0,0,1)), np.array((0,0,0)))
S = ptSource(c,np.array((1,0.5,0)),np.array((0,0,1)),np.array((0,0,0.005)),PW.waveform)
r = np.array([np.linspace(1,10),np.linspace(0,2),np.linspace(0,0)]).transpose()

ts = S.getI(r,1.2)
w = 0.2
nxy =9
nz=8
vx = voxelMesh(-w*(nxy//2+0.5),-w*(nxy//2+0.5),-w*0.5,nxy,nxy,nz,w,pmma,air,(2,nz-1),(2,0))

vx.mesh[4][4][3].Mat=bsglass()
g = grid(15,15,15,w/3)
npp =2000

num,r = mcphoton(vx.mesh[nxy//2][nxy//2][0],g,npp)

a = np.digitize(g.dra,np.linspace(np.amin(g.dra),np.amax(g.dra),5))
for n in range(5):
    print(np.sum(g.drw[a==n]))
print("Unscattered reflectance: " + str(np.sum(g.urw)/g.start))
print("Diffuse reflectance: "+str(np.sum(g.drw)/g.start))
print("Diffuse transmittance: " + str(np.sum(g.dtw)/g.start))
print("Early scatter: " + str(np.sum(g.early_scatter)/g.start))
print("Unscattered absorbance: " + str(np.sum(g.unsc_abs)/g.start))
print("Scattered absorbance: "+str(np.sum(g.scatter)/g.start))

print("Scattered transmittance: "+str(np.sum(g.str)/g.start))
print("Reflectance: " + str(np.sum(g.refl)/g.start))
print("Detector bin: " + str(np.sum(g.det_w)/g.start))
print("Filter bin: " + str(np.sum(g.fil_w)/g.start))
print("filter quotient:" + str(np.sum(g.fil_w)/(np.sum(g.det_w))))
print(sys.float_info)

print("In toto: " + str((np.sum(g.fil_w)+np.sum(g.urw)+np.sum(g.det_w)) + np.sum(g.drw)+np.sum(g.dtw) +np.sum(g.early_scatter) + np.sum(g.unsc_abs) +np.sum(g.scatter) + np.sum(g.refl)))
print("Relative error: " + str(((((np.sum(g.fil_w)+np.sum(g.urw)+np.sum(g.det_w)) + np.sum(g.drw))+(np.sum(g.dtw) +np.sum(g.early_scatter))) + ((np.sum(g.unsc_abs) +np.sum(g.scatter) )+( np.sum(g.refl)-g.start)))/npp))
k = np.zeros((npp,3))
for a in range(npp):
    k[a] = (r[a][-1])
grd = np.linspace(-w*(nxy//2+1),w*(nxy//2+2),nxy+1)
grd2 = np.linspace(-w*(5),w*(6),11)
##print(out.shape)
##ax.imshow(np.abs(out),norm=plt.Normalize(0,2*np.mean(abs(o2))))
fig = plt.figure()
ax = fig.add_subplot(111,projection='3d')
ax.scatter(k[:,2],k[:,0],k[:,1],color='red',alpha=0.3,s=32,label='Photon terminal position')
ax.set_yticks(np.arange(-0.5, 0.51, step=0.2))
ax.set_zticks(np.arange(-0.5, 0.51, step=0.2))
ax.set_xticks(np.arange(-0.1, 1.5, step=0.2))
ax.set_xlabel('z (m)',fontsize=16)
ax.set_ylabel('x (m)',fontsize=16)
ax.set_zlabel('y (m)',fontsize=16)
ax.tick_params(axis='both', which='major', labelsize=12)
# prepare some coordinates
N1 = nz-2
N2 = nxy-2
N3 = nxy-2
ma = np.ones((N1,N2,N3))
x,y,z = np.indices((N1,N2,N3))*0.2
x+=w/2
y-=w*((nxy-2)//2+0.5)
z-=w*((nxy-2)//2+0.5)
positions = np.c_[x[ma==1],y[ma==1],z[ma==1]]
colors= np.random.rand(len(positions),3)
#ax.set_aspect('equal')
colors=["#008faa05"]*len(positions)
colors[7*3 + 9*4 + 9*8 -7] = "#55555580"


pc = plotCubeAt(positions, sizes=np.ones_like(positions)*0.2, colors=colors,edgecolor="k",linewidth=0.05)
ax.add_collection3d(pc)
ax.set_ylim(-N2*w/2,N2*w/2)
ax.set_xlim(0,(N1+1)*w)
ax.set_zlim(-N2*w/2,N2*w/2)
#plotMatrix(ax, ma)
#ax.voxels(ma, edgecolor="k")

ax.legend()
ax.grid(False)

plt.show()
