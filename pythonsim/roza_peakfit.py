import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
from math import floor,ceil
from scipy import signal as sps
from numpy import linalg as lgnp
from scipy import optimize as spo
from scipy import integrate as spi

def fit_peak(xd,yd,ai): #This function estimates starting parameters

    yc1 = yd[ai+1:ai+5] #for the built-in curve fitting function in the function best_fit.
    yc2 = yd[ai-1:ai-5:-1] #There's no actual need for it to be this complicated!
    xc1 = xd[ai+1:ai+5]
    xc2 = xd[ai-1:ai-5:-1]
    l = lambda x,k,m: k*x + m #Anonymous function, works like a macro of sorts.
    c1 = spo.curve_fit(l,xc1,yc1) #Fits to straight line for finding peak.
    c2 = spo.curve_fit(l,xc2,yc2)
    c1=c1[0] #I just want the values, not the errors.
    c2=c2[0]
    lin=np.linspace(xc1[0],xc2[3],200)
    l1 = lin*c1[0] + c1[1] #Straight line equation.
    l2=lin*c2[0] + c2[1]
    min_arg = np.argmin(abs(l1-l2)) #Minimum index, for finding peak
    peak=np.array((np.sqrt((l1[min_arg]**2+l2[min_arg]**2)*0.5),lin[min_arg]))
    peaky_adj=np.sqrt(0.5*(peak[0]**2+yd[ai]**2)) #Takes RMS between peak data value
    peakx_adj=np.sqrt(0.5*(peak[1]**2+xd[ai]**2)) #and the interpolated value. Again, not strictly
                                                    #necessary.

    return(np.array((peaky_adj,peakx_adj)))
    
def FWHM(xdata,ydata): #Full width half maximum. Parameter estimation.
    ai = np.argmax(ydata)
    a = np.sqrt((np.amax(ydata)*np.sqrt(0.6))**2 + (ydata[np.argmax(ydata)+1]*np.sqrt(0.2))**2 + (np.sqrt(0.2)*ydata[np.argmax(ydata)-1])**2)
    count = np.sum(ydata>a/2)                    
    p = fit_peak(xdata,ydata,ai)
    return(p,abs(xdata[ai+count//2]-xdata[ai-count//2]))

def gaussian_curve(x,gamma1,x0,ymax): #This is a straightforward normal distribution.
                                     #In spectroscopy it usually represents thermal
                                     #doppler effects.
    sigma=gamma1/(2*np.sqrt(2*np.log(2))) #Standard deviation.
    out=(1/(sigma*np.sqrt(2*np.pi)))*np.exp(-((x-x0)**2)/(2*(sigma**2))) #Gaussian PDF.
    a=np.amax(out) #scaling
    return ymax*out/a

def cauchy_curve(xd,gammac,x0,ymax): #Cauchy, AKA Lorentzian, Breit-wigner, etc.
                                    #Usually represents Heisenberg uncertainty but shows up elsewhere too.
                                    #Notoriously, this is a "pathological" distribution
                                    #as it has no mean or variance. For this reason it can represent an estimate of a distribution based on a single sample (Student's t-distribution)
    out =1/(np.pi*0.5*gammac*(1 + ((xd-x0)/(0.5*gammac))**2))
    a=np.amax(out)
    return ymax*out/a
    
def voigt_curve(xd,gamma1,gamma2,x0,ymax): #Voigt is a kind of compromise between the two.
    x =xd
    va = gaussian_curve(x,gamma1,x0,ymax) #Note that it takes two FWHM values.
    vb=cauchy_curve(x,gamma2,x0,ymax)     #Their quotient tells you how "gaussian" vs
                                          #"Lorentzian" the distribution is.
    out =sps.convolve(va,vb)[::2]         #Convolution is a way of multiplying two functions together
                                          #under the integral sign. The multiplicative identity is the 
                                          #Dirac delta function (strictly speaking, it is not a function but a distribution or generalized function).
    a=np.amax(out)
    return ymax*out/a

def best_fit(yda,xda,a,b,c,spacing):
    print(a)
    print(spacing)
    print(len(yda))
    print(spacing)
    print(c)
    c,cc=spo.curve_fit(voigt_curve,xda,yda,p0=[a,a,b,c],bounds=(0,(min(spacing,b*1.05),min(spacing,b*1.05),1000,c*1.001)))
    return c,cc
    
plt.style.use('ggplot') #Plot styles. There are countless to choose from.
mpl.rcParams['font.size']=14 #Large font size because my sight sucks
mpl.rcParams['lines.linewidth']=2.5 #Default size is a bit too small for big plots
mpl.rcParams['lines.markersize']=8 #Default size is a bit too small for big plots

data_length=1
peak_number=3

y_data=np.loadtxt('infile.txt')

fig=plt.figure()
ax=fig.add_subplot(111)
y=y_data[(y_data[:,0]>769),1]# == (ydata[:,0<910]))),1]
x=y_data[(y_data[:,0]>769),0]# == (ydata[:,0<910]))),0]


y=y[x<911]# == (ydata[:,0<910]))),1]
x=x[x<911]# == (ydata[:,0<910]))),0]
y=sps.savgol_filter(y,3,2)
ax.plot(x,y,'+',label='data')


peaks,dictd=sps.find_peaks(y,0.0015)
peaks=np.flip(peaks)

print(peaks)


xm=np.linspace(np.amin(x),np.amax(x),1000)

vd=np.zeros(len(y))
print(len(y))
yd=y-vd
peak_number=len(peaks)
out_coeffs=np.zeros((peak_number,4))
curve_sum=np.zeros_like(x)
curve_sumlin=np.zeros_like(x)
area=np.zeros(peak_number)
for j in range(peak_number-1):
    peaks,dictd=sps.find_peaks(yd,0.0015)
    print(peaks)
    if len(peaks)>1:
        spacing=peaks[-1]-peaks[0]
        peaks=peaks[-1]
    else:
        peaks=peaks[0]
    print(peaks)
    print(yd[peaks])
    print("J = " + str(j))
    new_y=np.concatenate((yd[-1:peaks:-1],yd[peaks:]))
    
    new_x=x
    if len(new_y)<len(y):
        new_y=np.concatenate((np.zeros(len(y)-len(new_y)),new_y))
    else:
        new_y=new_y[0:len(y)]
    print(len(new_y))
    ydata=new_y
    xdata=new_x
    ax.plot(new_x,new_y)
    fmax=ydata[peaks]
    xpeak=xdata[peaks]
    peak,gamma= FWHM(xdata,ydata) #peak, fullwidth halfmaximum
    gamma=min(gamma,spacing)
    out_coeffs[j],err=best_fit(ydata,xdata,gamma,xpeak,fmax,spacing)
    v1=np.copy(voigt_curve(xm,out_coeffs[j,0],out_coeffs[j,1],out_coeffs[j,2],out_coeffs[j,3]))

    vd=np.copy(voigt_curve(x,out_coeffs[j,0],out_coeffs[j,1],out_coeffs[j,2],out_coeffs[j,3]))
    ax.plot(xm,v1,'--',label='voigt')
    area[j]=np.trapz(v1)

    j=1
    yd=yd-vd    
    #ax.plot(x,yd,'--',label='rest')

    


print(area)
ax.set_xlabel('Wavenumber (nm$^{-1}$)')
ax.set_ylabel('Intensity (AU)')
ax.legend()

fig=plt.figure()
ax=fig.add_subplot(111)
ax.plot(x)

plt.show()

