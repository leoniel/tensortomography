import matplotlib.pyplot as plt
import math
import numpy as np
import csv
import time
import os
import uncertainties as uc
import threading as thr
import time
import voxelgeo
#import props
from math import floor,ceil
from scipy import signal as sps
from scipy import fftpack as spf
from scipy import linalg as spl
from scipy import ndimage as spn
from abc import ABC,abstractmethod
from numpy import linalg as lg
from scipy import constants as con
from scipy import integrate as spi
from scipy.spatial.transform import Rotation as R
from scipy import stats as spst


def convolveDiff(x,f,o=1):
    dx = np.diff(x)
    dxdx = dx**2
    if o == 1:
        gf = spn.gaussian_filter1d(f[1:], sigma=1, order=1, mode='wrap') / dx
        return gf
    else:
        if o == 2:
            ggf = spn.gaussian_filter1d(f[1:], sigma=1, order=2, mode='wrap') / dxdx
            return ggf

def polar2z(r,theta):
    return r * np.exp( 1j * np.theta )

def z2polar(z):
    return ( abs(z), np.angle(z) )

class material:
    def __init__(self):
        return
    def set_property(self,*,density=0,numden=0,refractive=1,permitt=1,permeab=1, absorp=1, aniso=0,scatter=1):
        self.rho=density
        self.numden=numden
        self.refractive=refractive
        self.permitt=permitt
        self.permeab=permeab
        self.absorp=absorp
        self.aniso=aniso
        self.scatter=scatter
    def getEff(self):
        return np.sqrt(3*self.absorp*(self.absorp + self.scatter*(1-self.aniso)))
    def getD(self):
        return 1/(3*self.absorp+self.scatter*(1-self.aniso))
    def getExt(self):
        return self.scatter+self.absorp
    def spheroidParams(self):
        return 1e-8,1,self.refractive**2 #default particle is 10 nm, spherical, has refractive index squared (so that ns/nM = nM). replace in subclasses for other parameters.
        
class water(material):
    def __init__(self):
        self.set_property(density=1000,numden = 33.3679, refractive=1.333,permitt=80.1,permeab=(1.0-9.05e-6),absorp=6.9,aniso=0,scatter=5e-3)
        return

class waterg(material):
    def __init__(self):
        self.set_property(density=1000,numden = 33.3679, refractive=1.333,permitt=80.1,permeab=(1.0-9.05e-6),absorp=0.055,aniso=0,scatter=5e-3)
        return
class waterv(material):
    def __init__(self):
        self.set_property(density=1000,numden = 33.3679, refractive=1.333,permitt=80.1,permeab=(1.0-9.05e-6),absorp=0.0055,aniso=0,scatter=5e-3)
        return
class quartz(material):
    def __init__(self):
        self.set_property(density=2650,numden = 60.083/2.650, refractive=1.544,permitt=3.8,permeab=(0.999987),absorp=163.36,aniso=0.3,scatter=1500)
        return

class air(material):
    def __init__(self):
        self.set_property(density=1.225,numden = 0.02504, refractive=1.0003,permitt=1.0006,permeab=(1.0+3.7e-7),absorp=0.02504,aniso=0,scatter=1.23e-5)
        return
class bsglass(material): #borosilicate glass
    def __init__(self):
        self.set_property(density=2230,numden = 40.54, refractive=1.54,permitt=4.6,permeab=(1.0-9.05e-6),absorp=0.208,aniso=0,scatter=1e-3)
        return
    
class pmma(material): #plexiglass
    def __init__(self):
        self.set_property(density=1190,numden = 0, refractive=1.49,permitt=3.4,permeab=(1.0-9.06e-6),absorp=0.08,aniso=0,scatter=5e-4)
        return
    
    
class EVec(ABC):
    @abstractmethod
    def r2():
        pass
    @abstractmethod
    def r1():
        pass
    @abstractmethod
    def r():
        pass
    @abstractmethod
    def x():
        pass
    @abstractmethod
    def y():
        pass
    @abstractmethod
    def z():
        pass
    @abstractmethod
    def __add__():
        pass
    @abstractmethod
    def __mul__():
        pass
    @abstractmethod
    def __iadd__():
        pass
    @abstractmethod
    def __imul__():
        pass

    
class point(EVec):
    def __init__(self,x,y,z):
        self.rv = np.array((x,y,z))
    def r2(self):
        return np.dot(self.rv,self.rv)
    def r1(self):
        return spl.norm(self.rv)
    def r(self):
        return np.copy(self.rv)
    def x(self):
        return self.rv[0]
    def y(self):
        return self.rv[1]
    def z(self):
        return self.rv[2]
    def __iadd__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot add  points!")
        if isinstance(o,displacement):
            self.rv += o.rv
            return self
    def __isubtract__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot implicitly subtract points!")
        if isinstance(o,displacement):
            self.rv -= o.rv()
            return self
    def __add__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot add  points!")
        if isinstance(o,displacement):
            return point(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
        try: return self.rv+o
        except TypeError:
            print("Cannot add!")
    def __radd__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot add  points!")
        if isinstance(o,displacement):
            return point(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
    def __sub__(self,o):
        

        if isinstance(o,displacement):
            return point(self.x()-o.x(), self.y()-o.y(),self.z()-o.z())
        if isinstance(o,point):
            return displacement(self.x() - o.x(), self.y() - o.y(), self.z() - o.z())
        try:
            return self.rv+(-o)
        except TypeError:
            print("Invalid type!")
    def __rsub__(self,o):
        

        if isinstance(o,displacement):
            raise TypeError("Cannot subtract points from displacements!")
        if isinstance(o,point):
            return displacement(o.x() - self.x(), o.y() - self.y(), o.z() - self.z())
        try:
            return self.rv+(-o)
        except TypeError:
            print("Invalid type!")
    def __mul__():
        raise TypeError("Cannot mul points! Use point.r to get vector!")
    def __imul__():
        raise TypeError("Cannot mul points! Use point.r to get vector!")
        
class displacement(EVec):
    def __init__(self,x,y,z):
        self.rv = np.array((x,y,z))
    def r2(self):
        return spl.norm(self.rv)**2
    def r1(self):
        return spl.norm(self.rv)
    def r(self):
        return np.copy(self.rv)
    def x(self):
        return self.rv[0]
    def y(self):
        return self.rv[1]
    def z(self):
        return self.rv[2]
    def __iadd__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot implicitly add points to displacements!")
        if isinstance(o,displacement):
            self.rv += o.rv
            return self
    def __isubtract__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot implicitly subtract points from displacements!")
        if isinstance(o,displacement):
            self.rv -= o.rv
            return self
    def __add__(self,o):
        t = type(o)
        if isinstance(o,point):
            return point(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
        if isinstance(o,displacement):
            return displacement(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
    def __radd__(self,o):
        t = type(o)
        if isinstance(o,point):
            return point(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
        if isinstance(o,displacement):
            return displacement(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
    def __sub__(self,o):
        
        if isinstance(o,point):
            raise TypeError("Cannot subtract points from displacements!")
        if isinstance(o,displacement):
            return displacement(self.x()-o.x(), self.y()-o.y(),self.z()-o.z())
    def __rsub__(self,o):
        
        if isinstance(o,point):
            return point(o.x() - self.x(), o.y() - self.y(), o.z() - self.z())
        if isinstance(o,displacement):
            return displacement(o.x()-self.x(), o.y()-self.y(),o.z()-self.z())
    def __mul__(self,o):
        
        if isinstance(o,point):
            raise TypeError("Cannot mul point by displacement!")
        if isinstance(o,displacement):
            return np.dot(self.rv,o.rv())
        try:
            return self.rv*o
        except TypeError:
            Print("Must be allowed type, or a number!")
    def __rmul__(self,o):
        
        if isinstance(o,point):
            raise TypeError("Cannot mul point by displacement!")
        if isinstance(o,displacement):
            return np.dot(self.rv,o.rv())
        try:
            return self.rv*o
        except TypeError:
            Print("Must be allowed type, or a number!")
    def __imul__(self,o):
        
        if isinstance(o,point):
            raise TypeError("Cannot mul point by displacement!")
        if isinstance(o,displacement):
            raise TypeError("Cannot implicitly multiply displacements!")
class voxel(point):
    def __init__(self,x,y,z,w,mesh=0,is_ambient=False):
        self.rv = np.array((x,y,z))
        self.side=w
        self.mesh=mesh
        self.is_ambient = is_ambient
        self.is_detector=False
        self.is_edge=False
        self.is_beginning=False
    def xrang(self):
        return np.array([self.x(),self.x()+self.side])
    def yrang(self):
        return np.array([self.y(),self.y()+self.side])
    def zrang(self):
        return np.array([self.z(),self.z()+self.side])
    def get_neighbour(self,sign,ind):
        ind = np.int32(ind)
        a = np.zeros(3)
        a[abs(ind)]+=sign
        i = np.array(a + self.indices,dtype=np.int32)
        return i
    def set_indices(self,xyz,mxyz):
        self.indices=xyz
        self.maxind = mxyz
    def checkNbh(self,o):
        if not(isinstance(o,voxel)):
            raise TypeError("Argument must be a voxel!")
        d = (self-o).r1()
        if abs(d - self.side) < 1e-14:
            return True
        else:
            return False
    def setMaterial(self,o):
        self.Mat = o

class voxelMesh(point):
    def __init__(self,x,y,z,nx,ny,nz,w,mater=water,amb_mat=air,is_detector=(0,0),is_beginning=(0,0)):
        self.rv = np.array([x,y,z])
        M = mater()
        A = amb_mat()
        self.nums = np.ndarray.astype(np.array((nx,ny,nz)),int)
        self.w = w
        self.mesh=[[['' for i in range(nz)]for j in range(ny)] for k in range(nx)]
        for i in range(nx):
            for j in range(ny):
                for k in range(nz):
                    self.mesh[i][j][k] =voxel(self.rv[0] + w*i,self.rv[1] + w*j,self.rv[2] + w*k,w,self)
                    self.mesh[i][j][k].setMaterial(M)
                    self.mesh[i][j][k].set_indices(np.array([i,j,k]),np.array([nx,ny,nz])-1)
                    if ((k==0)|(k==nz-1)|(j==0)|(j==ny-1)|(i==0)|(i==nx-1)):
                        self.mesh[i][j][k].is_ambient = True
                        self.mesh[i][j][k].setMaterial(A)

                    if (((k==is_detector[1])&(is_detector[0]==2))|((j==is_detector[1])&(is_detector[0]==1))|((i==is_detector[1])&(is_detector[0]==0))):
                        self.mesh[i][j][k].is_detector=True

                    if (((k==is_beginning[1])&(is_beginning[0]==2))|((j==is_beginning[1])&(is_beginning[0]==1))|((i==is_beginning[1])&(is_beginning[0]==0))):
                        self.mesh[i][j][k].is_beginning=True
                    if (self.mesh[i][j][k].is_ambient&(not(self.mesh[i][j][k].is_beginning))):
                        self.mesh[i][j][k].is_edge=True
    def vx(self,i,j,k):
        return self.mesh[i][j][k]
    def nx(self):
        return self.nums[0]
    def ny(self):
        return self.nums[1]
    def nz(self):
        return self.nums[2]
    def geth(self):
        return self.w*self.nz()
    def getw(self):
        return self.w*self.nx()
    def getd(self):
        return self.w*self.ny()
    def getrangez(self,z1,z2):
        z_u = max(z1,z2)+self.w
        z_l = min(z1,z2)-self.w
        if ((z_u < self.z())|(z_l > (self.z()+self.geth()))):
            return range(0,0)
        z_l = max(z_l,self.z())
        z_u = min(z_u,self.z()+self.geth())
        rn = max(self.z() + self.geth()-z_l,0) - max(self.z()+ self.geth()-z_u,0)
        return range(floor((z_l-self.z())/self.w), ceil((z_l + rn-self.z())/self.w))
    def getrangey(self,y1,y2):
        y_u = max(y1,y2)+self.w
        y_l = min(y1,y2)-self.w
        if (y_u < self.y())|(y_l > (self.y()+self.getd())):
            return range(0,0)
        y_l = max(y_l,self.y())
        y_u = min(y_u,self.y()+self.getd())
        rn = max(self.y() + self.getd()-y_l,0) - max(self.y()+ self.getd()-y_u,0)
        return range(floor((y_l-self.y())/self.w), ceil((y_l + rn-self.y())/self.w))
    def getrangex(self, x1,x2):
        x_u = max(x1,x2)+self.w
        x_l = min(x1,x2)-self.w
        if (x_u < self.x())|(x_l > (self.x()+self.getw())):
            return range(0,0)
        x_l = max(x_l,self.x())
        x_u = min(x_u,self.x()+self.getw())
        rn = max(self.x() + self.getw()-x_l,0) - max(self.x()+ self.getw()-x_u,0)
        return range(floor((x_l-self.x())/self.w), ceil((x_l + rn-self.x())/self.w))
    def getrange(self, p1,p2):
        return np.array([self.getrangex(p1.x(),p2.x()),self.getrangey(p1.y(),p2.y()),self.getrangez(p1.z(),p2.z())])
class segm:
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2
    def getD(self):
        d = ((self.p2-self.p1).r())
        return d
    def getd(self):
        d = ((self.p2-self.p1).r())
        return d/(spl.norm(d))
    def x(self):
        return np.array((self.p1[0],self.p2[0]))
    def y(self):
        return np.array((self.p1[1],self.p2[1]))
    def z(self):
        return np.array((self.p1[2],self.p2[2]))
    def dz(self):
        return self.p1[2]-self.p2[2]
    def dx(self):
        return self.p1[0]-self.p2[0]
    def dy(self):
        return self.p1[1]-self.p2[1]
    def dp(self):
        return  self.p2-self.p1
    def findVoxels(self,vxm):
        rng = vxm.getrange(self.p1,self.p2)
        self.vx = []
        for i in (rng[0]):
            for j in (rng[1]):
                for k in (rng[2]):
                    self.vx.append(vxm.vx(i,j,k))
        return self.vx
    def checkInt(self,vx):
        d = self.getD()
        n = np.array([(0,1),(0,2),(1,2)])
        b = np.array([[0,0,0],[0,0,0],[0,0,0],[0,0,0]])
        n0 = np.array([[1,0],[0,1],[1,0],[0,1],[1,0],[0,1]])
        p0 = np.array([[[0,0,0],[0,0,0],[0,1,0],[1,0,0]],[[0,0,0],[0,0,0],[0,0,1],[1,0,0]],[[0,0,0],[0,0,0],[0,0,1],[0,1,0]]])
        for i in range(3):
            for k in range(4):
                    v1 = (self.p1.r() - vx.r()-p0[i,k]*vx.side)
                    D = (vx.side*n0[k,0]*d[n[i,1]]-vx.side*n0[k,1]*d[n[i,0]])
                    with np.errstate(divide='ignore',invalid='ignore'):
                        M =np.array([[d[n[i,1]],-d[n[i,0]]],[-vx.side*n0[k,1],vx.side*n0[k,0]]])*(1/D)
                        a = M@np.transpose([v1[n[i,0]],v1[n[i,1]]])
                    b[k,i]=(((a[0]>=0)&(a[0]<=1))&(((-a[1])>=0)&((-a[1])<=1)))
        if ((int(sum(b)[0]>0)+int(sum(b)[1]>0)+int(sum(b)[2]>0))>2):
            return True
        if ((int(sum(b)[0]>0)+int(sum(b)[1]>0)+int(sum(b)[2]>0))==2):
            i = np.argmin(sum(b))
            if ((vx.r()[i]<=self.p1.r()[i] <=vx.r()[i]+vx.side))&((vx.r()[i]<=self.p2.r()[i] <=vx.r()[i]+vx.side)):
                return True
        else:
            return False
class line(segm):
    def __init__(self,f = np.array([[1.0],[1.0]]),d=np.array((1,0,0)),m=np.array((0,0,0))):
        self.p1 = m
        self.p2 = m+d/spl.norm(d)
        self.d=d/spl.norm(d)
    def getM():
        return self.p1.r()

class particle(point):
    def __init__(self,r,vx,vy,vz):
        self.rv = np.array((x,y,z))
        self.vv = np.array((vx,vy,vz))
    def v2(self):
        return np.dot(self.vv,self.vv)
    def v1(self):
        return spl.norm(self.vv)
    def v(self):
        return np.copy(self.vv)
    def vx(self):
        return self.vv[0]
    def vy(self):
        return self.vv[1]
    def vz(self):
        return self.vv[2]


def null(x):
    return np.zeros(np.shape(x))

class ray(segm):
    def __init__(self,d=np.array((1,0,0)),p1=np.array((0,0,0))):
        self.p1 = p1
        self.p2 = p1+d/spl.norm(d)
        self.d=d/spl.norm(d)

        
class fSeries:
    def __init__(self,a0,af,bf,fmax):
        #y = spf.rfft(f) / (f.size)
        #print(y)
        self.a0 = a0
        self.a = af
        self.aw = (np.arange(len(af))/len(af))*fmax
        self.b = bf
        self.bw = (np.arange(len(bf))/len(bf))*fmax
        self.a = self.a[abs(self.a)>1e-8]
        self.aw = self.aw[abs(af)>1e-8]
        self.b = self.b[abs(self.b)>1e-8]
        self.bw = self.bw[abs(bf)>1e-8]
    def F(self,arg):
        At = np.array([arg for i in range(self.a.size)])
        Bt = np.array([arg for i in range(self.b.size)])
        A = self.a@[np.sin(self.aw@At) for i in range(self.a.size)]
        B = self.b@[np.cos(self.bw@Bt) for i in range(self.b.size)]
        return self.a0*np.ones_like(arg) + A + B 

class polwave:
    def __init__(self,freq=1/(2*math.pi),axes=np.array([[1,0],[0,1]]),phase=np.array([0,0]),amp=np.array([1,0]),coh=1,poltype='custom'):
        if poltype=='r':
            phase=np.array((0,math.pi/2))
        if poltype=='l':
            phase=np.array((0,3*math.pi/2))
        if poltype=='u':
            amp=np.array([1,0])
        if poltype=='v':
            amp=np.array([0,1])
        if poltype=='uv':
            amp=np.sqrt(1/2)*np.array([1,1])
        if poltype=='x':
            amp=np.array([1,0])
            axes=np.array([1,0])
        if poltype=='y':
            amp=np.array([0,1])
            axes=np.array([0,1])
        if poltype=='xy':
            amp=np.sqrt(1/2)*np.array([1,1])
            axes=np.sqrt(1/2)*np.array([1,1])
        self.e=axes/spl.norm(axes)
        self.a=amp
        self.coh=coh
        self.phase=phase
        self.freq=freq
    def u(self):
        return np.copy(self.e[0])
    def v(self):
        return np.copy(self.e[1])
    def coh(self):
        return np.copy(coh)
    def I(self):
        return self.amp@self.amp
    def amp3(self):
        return np.array([self.amp[0], self.amp[1],0])
    
class cohwave(polwave):
    def __init__(self,freq=1/(2*math.pi),axes=np.array([[1,0],[0,1]]),phase=np.array([0,0]),amp=np.array([1,0]),poltype='custom'):
        if poltype=='r':
            phase=np.array((0,math.pi/2))
        if poltype=='l':
            phase=np.array((0,3*math.pi/2))
        if poltype=='u':
            amp=np.array([1,0])
        if poltype=='v':
            amp=np.array([0,1])
        if poltype=='uv':
            amp=np.sqrt(1/2)*np.array([1,1])
        if poltype=='x':
            amp=np.array([1,0])
            axes=np.array([1,0])
        if poltype=='y':
            amp=np.array([0,1])
            axes=np.array([0,1])
        if poltype=='xy':
            amp=np.sqrt(1/2)*np.array([1,1])
            axes=np.sqrt(1/2)*np.array([1,1])
        self.e=axes/spl.norm(axes)
        self.phi=phase
        self.freq=freq
        self.a=amp
        self.coh=1
    def getJones(self,t,x):
        return polar2z(self.amp,self.freq*(x/con.c + t) + self.phase)

class incohwave(polwave):
    def __init__(self,freq=1/(2*math.pi),axes=np.array([[1,0],[0,1]]),phase=np.array([0,0]),amp=np.array([1,0]),coh=np.array([0,0,0]),poltype='custom'):
        if poltype=='r':
            phase=np.array((0,math.pi/2))
        if poltype=='l':
            phase=np.array((0,3*math.pi/2))
        if poltype=='u':
            amp=np.array([1,0])
        if poltype=='v':
            amp=np.array([0,1])
        if poltype=='uv':
            amp=np.sqrt(1/2)*np.array([1,1])
        if poltype=='x':
            amp=np.array([1,0])
            axes=np.array([1,0])
        if poltype=='y':
            amp=np.array([0,1])
            axes=np.array([0,1])
        if poltype=='xy':
            amp=np.sqrt(1/2)*np.array([1,1])
            axes=np.sqrt(1/2)*np.array([1,1])
        self.e=axes/spl.norm(axes)
        self.phi=phase
        self.freq=freq
        self.a=amp
        self.coh=coh/max(1,spl.norm(coh))
    def S1(self):
        return coh[0]*(self.amp[0]**2-self.amp[1]**2)
    def S2(self):
        r=R.from_rotvec([0,0,np.pi/4])
        return coh[1]*(r.apply(self.amp3())[0]**2-r.apply(self.amp3())[1]**2)
    def S3(self):
        return coh[2]*((self.amp[0] + self.amp[1]*1j)**2 - (self.amp[0] - self.amp[1]*1j)**2)
    def getStokes(self,t,x):
        return np.array(self.I(),self.S1(),self.S2(),self.S3())
    
class planeWave(ray):
    def __init__(self,d=np.array((0,0,1)),p1=np.array((0,0,0)),F=[np.sin,np.sin]):
        self.p1 = p1
        self.p2 = p1+d/spl.norm(d)
        self.Fu = F[0]
        self.Fv = F[1]
        self.d=d/spl.norm(d)
        if int(abs(np.dot(self.d,np.array((0,-1,0)))) + 1e-8) ==1:
            self.e_u = np.cross(self.d,np.array((0,0,-1)))
        else:
            self.e_u = np.cross(self.d,np.array((0,-1,0)))            
        self.e_v = np.cross(self.e_u,-self.d)
    def u(self):
        return np.copy(self.e_u)
    def v(self):
        return np.copy(self.v_u)
    def z(self):
        return np.copy(self.d)
    def waveform(self,arg):
        return self.e_u*self.Fu(arg) + self.e_v*self.Fv(arg)
    
class ptSource(point):
    def __init__(self,l = incohwave(),A=np.array((1,1,0)),r0 = np.array((0,0,1)),S=np.array((0,0,0)),F=np.sin,G=null):
        self.A = A
        self.S = S
        self.F = F
        self.G = G
        self.r0 = r0
        self.p1 = S
        self.l=l
        self.kf = self.l.freq*2*np.pi/con.c
    def getAmp(self,r,t):
        if len(r.shape)>1:
            s = spl.norm(r-np.array([self.S for i in range(np.size(r,0))]),axis=1)
            r0 = np.array([spl.norm(self.r0-self.S) for i in range(np.size(r,0))])
            A = np.array([self.A for i in range(np.size(r,0))])
            return (A.transpose()*(r0/(s))).transpose()*(self.F(self.kf*(r-con.c*t)) + self.G(self.kf*(r+con.c*t)))
        else:
            return self.A*(self.r0/(spl.norm(r-self.S)))*(self.F(self.kf*(spl.norm(r)-con.c*t)) + self.G(self.kf*(spl.norm(r)+con.c*t)))
    def getI(self,r,t):
        if np.mean(spl.norm(np.diff(r)))/(r.shape[1])>(con.c/self.l.freq):
            s = spl.norm(r-np.array([self.S for i in range(np.size(r,0))]),axis=1)
            r0 = np.array([spl.norm(self.r0-self.S) for i in range(np.size(r,0))])
            A = np.array([self.A for i in range(np.size(r,0))])
            return 0.5*((A.transpose()*(r0/(s))).transpose())**2
        else:
            A = self.getAmp(r,t)
            return A*A
    def GFun(self,m,t=0):
        a = np.zeros(10000)
        a[len(a)//2+1]+=1/m
        return a

    def k(self,r):
        return self.kf*((r.transpose()-self.S))
    
class extsource(ptSource):
    def GFun(self,r,t,absorp):
        return (np.exp(-10*spl.norm(r,2,0)))/absorp
    
class photon(particle):
    def __init__(self,x,y,z,vx,vy,vz,nu=1,phi=0):
        self.rv = np.array((x,y,z))
        self.vv = np.array((vx,vy,vz))
        self.nu = nu #Internal frequency variable
        self.phi = phi #Internal phase variable
        self.rl = np.array((1,1)) #Internal polarization variable in R-L-space. Default is xy-polarization.
    def get_spin(self):
        return self.rl[0]**2 - self.rl[1]**2
    def set_freq(self, nu):
        self.nu=nu
        return
    def set_wl(self,lm):
        self.nu=con.c/lm
        return
    def set_pol(self,p):
        if isinstance(p,(tuple,list,np.array))&(np.size(p)==2):
            self.xy = np.array(p)
        else:
            raise TypeError("Polarization state must be tuple!")
    def freq(self):
        return self.nu
    def wl(self):
        return con.c/self.nu


class Radiance():
    def __init__(self):
        return
        
class Div():
    def __init__():
        return
        
class RTE:
    def __init__(self, div=0, ext=0, sct = 0, src = 0):
        self.div=div
        self.ext=ext
        self.sct=sct
        self.src=src
        
class grid:
    def __init__(self,nx,ny,nz,gs):
        self.n = np.array((nx,ny,nz))
        self.scatter = np.zeros((nx*2,ny*2,nz*2))
        self.unsc_abs = np.array((0.))
        self.early_scatter=np.array((0.))
        self.gs = gs
        self.rest=0.
        self.start=0.
        self.dtr = np.zeros((3,1))
        self.dta = np.zeros((1,1))
        self.dtw = np.zeros((1,1))
        self.drr = np.zeros((3,1))
        self.dra = np.zeros((1,1))
        self.drw = np.zeros((1,1))
        self.urr = np.zeros((3,1))
        self.ura = np.zeros((1,1))
        self.urw = np.zeros((1,1))
        self.det_r = np.zeros((3,1))
        self.det_a = np.zeros((1,1))
        self.det_w = np.zeros((1,1))
        self.fil_r = np.zeros((3,1))
        self.fil_a = np.zeros((1,1))
        self.fil_w = np.zeros((1,1))
        self.str = np.zeros((3,1))
        self.sta = np.zeros((1,1))
        self.stw = np.zeros((1,1))
        self.refl=np.array((0.))
        self.rn = np.array([nx,ny,nz],dtype=np.uint32)
    def add_scatter(self,r,dw):
        rbin = np.floor(r/self.gs).astype(np.int32)
        

        for i in range(3):
            if (abs(rbin[i]) >= (self.n[i]*2-1)):
                rbin[i]=np.copy(self.n[i]*2-1)
        self.scatter[rbin[0],rbin[1],rbin[2]]+=dw
    def bin_diff_trans(self,r,a,w):
        rbin = np.floor(r/self.gs).astype(np.int32)
        for i in range(3):
            if (abs(rbin[i]) >= (self.n[i]*2-1)):
                rbin[i]=np.copy(self.n[i]*2-1)
        self.dtr = np.append(self.dtr,rbin)
        self.dta = np.append(self.dta,a)
        self.dtw = np.append(self.dtw,w)
    def bin_scattered_trans(self,r,a,w):
        rbin = np.floor(r/self.gs).astype(np.int32)
        for i in range(3):
            if (abs(rbin[i]) >= (self.n[i]*2-1)):
                rbin[i]=np.copy(self.n[i]*2-1)
        self.str = np.append(self.str,rbin)
        self.sta = np.append(self.sta,a)
        self.stw = np.append(self.stw,w)
    def bin_diff_ref(self,r,a,w):
        rbin = np.floor(r/self.gs).astype(np.int32)
        for i in range(3):
            if (abs(rbin[i]) >= (self.n[i]*2-1)):
                rbin[i]=np.copy(self.n[i]*2-1)
        self.drr = np.append(self.drr,rbin)
        self.dra = np.append(self.dra,a)
        self.drw = np.append(self.drw,w)
    def bin_unsc_ref(self,r,a,w):
        rbin = np.floor(r/self.gs).astype(np.int32)
        for i in range(3):
            if (abs(rbin[i]) >= (self.n[i]*2-1)):
                rbin[i]=np.copy(self.n[i]*2-1)
        self.urr = np.append(self.urr,rbin)
        self.ura = np.append(self.ura,a)
        self.urw = np.append(self.urw,w)
    def detector_bin(self,r,a,w):
        rbin = np.floor(r/self.gs).astype(np.int32)
        for i in range(3):
            if (abs(rbin[i]) >= (self.n[i]*2-1)):
                rbin[i]=np.copy(self.n[i]*2-1)
        self.det_r = np.append(self.det_r,rbin)
        self.det_a = np.append(self.det_a,a)
        self.det_w = np.append(self.det_w,w)
    def filter_bin(self,r,a,w):
        rbin = np.floor(r/self.gs).astype(np.int32)
        for i in range(3):
            if (abs(rbin[i]) >= (self.n[i]*2-1)):
                rbin[i]=np.copy(self.n[i]*2-1)
        self.fil_r = np.append(self.fil_r,rbin)
        self.fil_a = np.append(self.fil_a,a)
        self.fil_w = np.append(self.fil_w,w)
        
class muellerMatrix:
    def __init__(self):
        self.M = np.identity(4)
    def applyM(self,S):
        return self.M@S

class fresnelM(muellerMatrix):
    def __init__(self):
        self.aR = lambda alph, alpht: 0.5*(np.tan(alph-alpht)**2)/(np.tan(alph+alpht)**2)
        self.bR = lambda alph, alpht: 0.5*(np.sin(alph-alpht)**2)/(np.sin(alph+alpht)**2)
        self.cR = lambda alph, alpht: (np.tan(alph-alpht)*np.sin(alph-alpht))/(np.tan(alph+alpht)*np.sin(alph+alpht))
        self.aT = lambda alph, alpht: 0.5*np.abs((2*np.sin(alpht)*np.cos(alph))/(np.sin(alph+alpht)*(np.cos(alph-alpht))))**2
        self.bT = lambda alph, alpht: 0.5*np.abs((2*np.sin(alpht)*np.cos(alph))/(np.sin(alph+alpht)))**2
        self.cT = lambda alph, alpht: (4*(np.sin(alpht)**2)*(np.cos(alph)**2))/((np.sin(alph+alpht)**2)*(np.cos(alph-alpht)))
    def getMR(self,alph,alpht):
        return np.array(((self.aR(alph,alpht) + self.bR(alph,alpht),self.aR(alph,alpht)-self.bR(alph,alpht),0,0),(self.aR(alph,alpht) - self.bR(alph,alpht),self.aR(alph,alpht)+self.bR(alph,alpht),0,0),(0,0,self.cR(alph,alpht),0),(0,0,0,self.cR(alph,alpht))))
    def getMT(self,alph,alpht):
        return np.array(((self.aT(alph,alpht) + self.bT(alph,alpht),self.aT(alph,alpht)-self.bT(alph,alpht),0,0),(self.aT(alph,alpht) - self.bT(alph,alpht),self.aT(alph,alpht)+self.bT(alph,alpht),0,0),(0,0,self.cT(alph,alpht),0),(0,0,0,self.cT(alph,alpht))))
    def applyM(self,S,alph,alpht):
        return self.getMR(alph,alpht)@S,self.getMT(alph,alpht)@S
    
class birefM(muellerMatrix):
    def __init__(self):
        self.a = lambda psi: -np.cos(2*psi)
        self.b = lambda psi,delta: np.sin(2*psi)*np.cos(delta)
        self.c = lambda psi,delta: np.sin(2*psi)*np.sin(delta)
    def getMB(self,psi,delta):
        return np.array(((1,self.a(psi),0,0),(self.a(psi),1,0,0),(0,0,self.b(psi,delta),self.b(psi,delta)),(0,0,-self.b(psi,delta),self.a(psi,delta))))
    def applyM(self,S,psi,delta):
        return getMB(psi,delta)@S

class M1M(muellerMatrix): #randomly oriented, no optical activity, 10 unique elements
    def __init__(self, M0,M1,M2,M3): #len(M0) = 4, len(M1) = 3, len(M2) = 2, len(M3) = 1
        M0 = np.array(M0)
        M1 = np.concatenate((M0[1],M1))
        M2 = np.concatenate((-M0[2],-M1[2],M2))
        M3 = np.concatenate((M0[3],M1[3],-M2[3],M3))
        self.M = np.concatenate((M0[np.newaxis,:],M1[np.newaxis,:],M2[np.newaxis,:],M3[np.newaxis,:]),1)

def retarderM(theta, delta):
    return np.array([[1, 0, 0, 0],
        [0, np.cos(2*self.theta)**2 + (np.sin(2*self.theta)**2)*np.cos(delta),
        np.cos(2*self.theta)*np.sin(2*self.theta)*(1 - np.cos(delta)),
        np.sin(2*self.theta)*np.sin(delta)],
        [0, np.cos(2*self.theta)*np.sin(2*self.theta)*(1 - np.cos(delta)),
        (np.cos(2*self.theta)**2)*np.cos(delta) + np.sin(2*self.theta)**2,
        -np.cos(2*self.theta)*np.sin(delta)],
        [0, -np.sin(2*self.theta)*np.sin(delta), np.cos(2*self.theta)*np.sin(delta), np.cos(delta)]])


def polarizer(pol='horizontal',a=0):
    if pol=='horizontal':
        M = np.zeros((4,4))
        M[0:2,0:2]=1/2
    if pol=='vertical':
        M=np.zeros((4,4))
        M[0:2,0:2]=1/2
        M[1,0]*=-1
        M[0,1]*=-1
    if pol=='rc':    
        M=np.zeros((4,4))
        M[0,0] = 1/2
        M[3,3] = 1/2
        M[0,3] = 1/2
        M[3,0] = 1/2
    if pol=='lc': 
        M=np.zeros((4,4))
        M[0,0] = 1/2
        M[3,3] = 1/2
        M[0,3] = -1/2
        M[3,0] = -1/2
    if pol=='linear':
        M = np.zeros((4,4))
        M[0] = np.array((1,np.cos(2*a),np.sin(2*a),0))
        M[1] = np.array((np.cos(2*a),np.cos(2*a)**2,np.cos(2*a)*np.sin(2*a),0))
        M[2] = np.array((np.sin(2*a),np.sin(2*a)*np.cos(2*a),np.sin(2*a)**2,0))
        M[3] = np.array((0,0,0,0))
        M*=0.5
        print(M)
    if pol=='partlinear':
        M = np.zeros((4,4))
        M[0] = np.array((1,np.cos(2*a),0,0))
        M[1] = np.array((np.cos(2*a),1,0,0))
        M[2] = np.array((0,0,np.sin(2*a),0))
        M[3] = np.array((0,0,0,np.sin(2*a)))
        M*=0.5
    return M

def rotatorM(theta):
    return np.array([[1, 0, 0, 0],
                    [0, np.cos(2*self.theta),
                    -np.sin(2*self.theta),
                    0],
                    [0, np.sin(2*self.theta),
                    np.cos(2*self.theta),
                    0],
                    [0,0,0,1]])

def complex_quad(g, a, b):
    t = np.linspace(a,b,2501)
    x = g(t)
    return spi.simps(y=x, x=t)

def cft(g, f):
    """Numerically evaluate the Fourier Transform of g for the given frequencies"""    
    result = np.zeros(len(f), dtype=complex)
    
    # Loop over all frequencies and calculate integral value
    for i, ff in enumerate(f):
        # Evaluate the Fourier Integral for a single frequency ff, 
        # assuming the function is time-limited to abs(t)<5
        result[i] = complex_quad(lambda t: g(t)*np.exp(-2j*np.pi*ff*t), 0, 10)
    return result
        
def prodFun(n,np,m):
    if (n-np) & 1:
        return 0
    else:
        
        
def spheroidM2M(a,p,ns,nm,th,k,wl): #polar axis length, shape parameter (orthogonal axis b = a/p), scatterer OptInd, medium OptInd, cos( scattering angle), scattering vector size
    mu = np.cos(th)
    
    if p==1:
        Lb = 1/3
    else:
        if p>1: #prolate
            Lb = ((p**2)/(2*(p**2-1)))*(1-(1/(p*(p**2-1)**(1/2)))*np.log(p+(p**2-1)**(1/2)))
        else: #oblate
            Lb = ((p**2)/(2*(1-p**2)))*((1/(p*np.sqrt(1-p**2)))*np.arccos(p)-1)
    La = 1-2*Lb
    m=ns/nm
    A=((m**2)/((1 + (m**2 -1)*Lb)*3*(p**2)))
    B=((m**2 - 1)/((1 + (m**2 -1)*La)*3*(p**2)))
    if p==1:
        B = 0
        A=1
    A1 = 0.5*(A**2 + (2/3)*A*B + (2/15)*(B**2))
    m00 = A1*(1+mu**2) + (2/15)*(B**2)
    m01 = A1*(mu**2 - 1)
    m11 = A1*(1+mu**2)
    m22 = 2*A1*mu
    m23 = 0
    m33 = 2*(A1-(1/15)*(B**2))*mu
    M = M2M(np.array((m00,m01,m11)),np.array((m22,m23,m33)))
    return M
    
    
class M2M(muellerMatrix): #Randomly oriented particles with a plane of symmetry & equally distributed
    def __init__(self,M01,M23): #with their chiral isomers. 6 unique elements (len(M01) = 3, len(M23) = 3 
        self.M = np.zeros((4,4))
        self.M[0:2,0:2] = np.array(((M01[0:2]),(M01[1:3])))
        self.M[2:4,2:4] = np.array(((M23[0:2]),(-M23[1],M23[2])))

        
class M3M(muellerMatrix): #Spherical particles, monatomic gas? 4 elements
    def __init__(self,M1,M2):
        self.M = np.zeros((4,4))
        self.M[0:2,0:2] = np.array(((M1),(np.flip(M1))))
        self.M[2:4,2:4] = np.array(((M2),(-M2[1],M2[0])))

            
class packet(particle): #Idealized 550nm monochromatic unpolarized light
    def __init__(self,vx, grid, r=np.array([0.,0.,0.]),pdir=np.array([0.,0.,1.]),w=1,wth = 1e-9,m=10,azim_spread=0, ur=np.array([0.,0.,1.])):
        self.rv=r
        self.rhist = np.zeros((3,1,1))
        self.pdir = pdir/spl.norm(pdir)
        if not(azim_spread==0): #currently does not allow for directional biases
                                #but this is easy to implement
            th = np.arccos(2*np.random.uniform()-1)
            ph = 2*np.pi*np.random.uniform()
            ur[0] = ur[0]+ azim_spread*np.sin(th)*np.cos(ph)
            ur[1] = ur[1]+ azim_spread*np.sin(th)*np.sin(ph)
            ur[2] = ur[2]
            ur/=spl.norm(ur)
        self.w=w
        self.scatters = 0
        self.vx = vx
        self.dead=0
        self.grid=grid
        self.grid.start+=w
        self.wth=wth
        self.entering=True
        self.m=m
        self.bounces=0
        rr = R.from_rotvec([[-np.arcsin(pdir[1]),0,0],[0,np.arcsin(pdir[0]),0]])
        self.rot = R.from_dcm(rr.as_dcm()[0]@rr.as_dcm()[1])
        self.vv=self.rot.apply(ur,inverse=True)
        #pdir is the z-axis of the local photon coordinate system which the
        #direction cosines "ur" are given in. This allows consistent scattering
        #angle computations (the only time when the cosines are transformed
        #back into this local system) while independently allowing for the spreading
        #of the photon beam via azim_spread, and a choice of a "beam source coordinate system".
        
    def Fresnel(self,alph,alpht):
        if ((alph-alpht==0)|(alph+alpht>=np.pi)):
            a = 0
            b = 0
        else:
            a = (np.tan(alph-alpht)**2)/(np.tan(alph+alpht)**2)
            b = (np.sin(alph-alpht)**2)/(np.sin(alph+alpht)**2)
        return 0.5*(a+b) #Must be changed for polarized light!
    def cross_boundary(self,vx1,vx2,mn):
        if (self.entering):
            dw = ((vx1.Mat.refractive-vx2.Mat.refractive)/(vx1.Mat.refractive+vx2.Mat.refractive))**2
            self.w-=dw
            self.grid.refl+=dw
            self.entering=False
            self.vx = vx2
            return
        if not(isinstance(vx2.Mat,type(vx1.Mat))):
            alph = np.arccos(abs(self.vv[mn]))
            alpht = np.arcsin(min(vx1.Mat.refractive*np.sin(alph)/(vx2.Mat.refractive),1))
            if (vx1.Mat.refractive>vx2.Mat.refractive)&(alph>np.arcsin(min(vx2.Mat.refractive/vx1.Mat.refractive,1))):
                Ri = 1
            else:
                Ri = self.Fresnel(alph,alpht)
            if ((np.random.uniform() <= Ri)&(self.bounces<100)):
                self.vv[mn] = -self.vv[mn]
                self.bounces+=1
                return
            else:
                f = vx1.Mat.refractive/vx2.Mat.refractive
                ta = np.array([f,f,f])
                ta[mn]=0
                mnf = np.sign(self.vv[mn])*np.cos(alpht)
                self.vv *=ta
                self.vv[mn] = mnf
                self.vv/=spl.norm(self.vv)#Floating point error often puts it slightly above 1
                if (vx2.is_detector):
                    self.grid.detector_bin(self.rv, alpht,self.w)
                    self.dead=1
                    return
                else:
                    if (self.scatters==0)&(vx2.is_ambient):
                        self.grid.bin_unsc_ref(self.rv,alpht,self.w)
                        self.dead=1
                        return
                    else:
                        if vx2.is_edge:
                            self.grid.bin_scattered_trans(self.rv, alpht,self.w)
                            self.dead=1
                            return
                        else:
                            if vx2.is_ambient:
                                self.grid.bin_diff_ref(self.rv,np.sin(np.arccos(abs(self.vv[mn]))),self.w)
                                self.dead=1
                                return
        else:
            if (vx2.is_detector):
                  self.grid.detector_bin(self.rv, np.arccos(abs(self.vv[mn])),self.w)
                  self.dead=1
                  return
            else:
                if vx2.is_edge:
                     self.grid.bin_scattered_trans(self.rv, np.arccos(abs(self.vv[mn])),self.w)
                     self.dead=1
                     return
                else:
                     if vx2.is_ambient:
                         self.grid.bin_unsc_ref(self.rv,np.sin(np.arccos(abs(self.vv[mn]))),self.w)
                         self.dead=1
                         return
        self.vx = vx2
        return
        #Specular reflectance at boundary
    def getsts(self):
        return -np.log(np.random.uniform())
    def relpos(self):
        return self.rv - (self.vx.rv+np.array((0.5,0.5,0.5))*self.vx.side)
    def chkbnds(self,l):
        rp = self.vx.side/2- np.sign(self.vv)*(self.relpos())
        mn = np.argmin(rp/abs(self.vv))

        if rp[mn] < abs(self.vv[mn]*l/self.vx.Mat.getExt()):
            vxi = self.vx.get_neighbour(np.sign(self.vv[mn]),mn)
            vxn = self.vx.mesh.mesh[vxi[0]][vxi[1]][vxi[2]]
            q = abs(rp[mn])/(abs(self.vv[mn]*l/self.vx.Mat.getExt()))
            if (q>1)|(q<0):
                print(q)
                print(self.vv)
                print(self.rv)
                print(mn)
                print(rp[mn])
                raise ValueError
            self.rv += q*l*self.vv/self.vx.Mat.getExt()
            l-=q*l
            self.cross_boundary(self.vx,vxn,mn)
            if self.dead==1:
                self.bounces=0
                return
            self.chkbnds(l)
            self.bounces=0
            return
        else:
            self.rv += l*self.vv/self.vx.Mat.getExt()
            return
    def scatter(self):
        if self.vx.Mat.aniso == 0:
            th = np.arccos(2*np.random.uniform()-1)
        else:
            g=self.vx.Mat.aniso
            th = (1/(2*g))*(1 + g**2 - ((1-g**2)/(1-g+2*g*np.random.uniform()))**2)
        ph = 2*np.pi*np.random.uniform()
        self.rot.apply(self.vv)
        if 1-abs(self.vv[2])<1e-8:
            self.vv[0] = np.sin(th)*np.cos(ph)
            self.vv[1] = np.sin(th)*np.sin(ph)
            self.vv[2] = np.sign(self.vv[2])*np.cos(th)
            self.vv/=spl.norm(self.vv)
        else:
            tv = np.copy(self.vv)
            self.vv[0] = tv[0]*np.cos(th) + np.sin(th)*(tv[0]*tv[2]*np.cos(ph)-tv[1]*np.sin(ph))/(np.sqrt(1-tv[2]**2))
            self.vv[1] =  tv[1]*np.cos(th) + np.sin(th)*(tv[1]*tv[2]*np.cos(ph)+tv[0]*np.sin(ph))/(np.sqrt(1-tv[2]**2))
            self.vv[2] = (np.sqrt(1-tv[2]**2))*np.sin(th)*np.cos(ph) + tv[2]*np.cos(th)
            self.vv/=spl.norm(self.vv)
        self.scatters+=1
        if (self.entering)&(self.vv[2]<=0):
            self.dead=1
            self.grid.early_scatter=np.append(self.grid.early_scatter,self.w)
        self.rot.apply(self.vv,inverse=True)
        return
    def russian_roulette(self):
        if np.random.uniform()<=1/self.m:
            self.w*=self.m
        else:
            self.grid.rest+=self.w
            self.w=0
            self.dead=1
        return #Terminate but conserve energy
        
    def move(self,cycles):
        l = self.getsts()
        self.chkbnds(l) #Propagate updating location and weight as necessary
        self.rhist = np.concatenate((self.rhist.flatten(),self.rv)).reshape(self.rhist.size//3 + 1,3)
        if self.dead==1:
            return cycles
        dw = self.w * self.vx.Mat.absorp/self.vx.Mat.getExt()
        self.w-=dw
        if self.scatters < 1:
            self.grid.unsc_abs=np.append(self.grid.unsc_abs,dw)
        else:        
            self.grid.add_scatter(self.rv,dw)
        if self.w<=self.wth: #If below threshold risk termination
            self.russian_roulette()
            if self.dead==1:
                return cycles
        self.scatter()
        if self.dead==1:
                return cycles
        cycles = self.move(cycles+1)
        return cycles
        


class chromPacket(packet):
    def __init__(self,vx, grid, r=np.array([0.,0.,0.]),pdir=np.array([0.,0.,1.]),w=1,wth = 1e-9,m=10,azim_spread=0, ur=np.array([0.,0.,1.]),cd=lambda: chromDistFun()):
        self.rv=r
        self.rhist = np.zeros((3,1,1))
        self.pdir = pdir/spl.norm(pdir)
        if not(azim_spread==0): #currently does not allow for directional biases
                                #but this is easy to implement
            th = np.arccos(2*np.random.uniform()-1)
            ph = 2*np.pi*np.random.uniform()
            ur[0] = ur[0]+ azim_spread*np.sin(th)*np.cos(ph)
            ur[1] = ur[1]+ azim_spread*np.sin(th)*np.sin(ph)
            ur[2] = ur[2]
            ur/=spl.norm(ur)
        self.w=w
        self.wl = cd()
        self.scatters = 0
        self.vx = vx
        self.dead=0
        self.grid=grid
        self.grid.start+=w
        self.wth=wth
        self.entering=True
        self.m=m
        self.bounces=0
        rr = R.from_rotvec([[-np.arcsin(pdir[1]),0,0],[0,np.arcsin(pdir[0]),0]])
        self.rot = R.from_dcm(rr.as_dcm()[0]@rr.as_dcm()[1])
        self.vv=self.rot.apply(ur,inverse=True)
        #pdir is the z-axis of the local photon coordinate system which the
        #direction cosines "ur" are given in. This allows consistent scattering
        #angle computations (the only time when the cosines are transformed
        #back into this local system) while independently allowing for the spreading
        #of the photon beam via azim_spread, and a choice of a "beam source coordinate system".
        
def chromDistFun(ftype='blackbody',wavelength=550e-7,error=1e-2,temperature=4000):
    if ftype=='blackbody':
        wlp = con.h*con.c/(con.k*temperature*5)
        wl = np.linspace(wlp/10,wlp*10,1000)
        dist = (1/wl**5)*2*con.h*(con.c**3)*(1/(np.exp(con.h*con.c/(wl*con.k*temperature))-1))
        dist/=np.amax(dist)
        while True:
            r1 = np.random.uniform()
            ri = np.random.randint(0,1000)
            r2 = dist[ri]
            if r2 > r1:
                return wl[ri]
    if ftype=='mono':
        return wavelength*(1+np.random.normal(0,1e-2))
        
class polPacket(chromPacket):
    def __init__(self,vx, grid, r=np.array([0.,0.,0.]),pdir=np.array([0.,0.,1.]),w=1,wth = 1e-9,m=10,azim_spread=0, ur=np.array([0.,0.,1.]),cd=False,stokes=np.array((1,0,0,0)),detfilter=polarizer('linear',np.pi/4)):
        self.rv=r
        self.stokes=stokes
        self.detfilter=detfilter
        self.rhist = np.zeros((3,1,1))
        self.pdir = pdir/spl.norm(pdir)
        if not(azim_spread==0): #currently does not allow for directional biases
                                #but this is easy to implement
            th = np.arccos(2*np.random.uniform()-1)
            ph = 2*np.pi*np.random.uniform()
            ur[0] = ur[0]+ azim_spread*np.sin(th)*np.cos(ph)
            ur[1] = ur[1]+ azim_spread*np.sin(th)*np.sin(ph)
            ur[2] = ur[2]
            ur/=spl.norm(ur)
        self.fres = fresnelM()
        self.w=w
        if not(cd):
            self.wl = chromDistFun()
        else:
            self.wl = cd()
        self.scatters = 0
        self.vx = vx
        self.dead=0
        self.grid=grid
        self.grid.start+=w
        self.wth=wth
        self.entering=True
        self.m=m
        self.bounces=0
        rr = R.from_rotvec([[-np.arcsin(pdir[1]),0,0],[0,np.arcsin(pdir[0]),0]])
        self.rot = R.from_dcm(rr.as_dcm()[0]@rr.as_dcm()[1])
        self.vv=self.rot.apply(ur,inverse=True)
        #pdir is the z-axis of the local photon coordinate system which the
        #direction cosines "ur" are given in. This allows consistent scattering
        #angle computations (the only time when the cosines are transformed
        #back into this local system) while independently allowing for the spreading
        #of the photon beam via azim_spread, and a choice of a "beam source coordinate system".
    def Fresnel(self,alph,alpht):
        return self.fres.applyM(self.stokes,alph,alpht)
    def cross_boundary(self,vx1,vx2,mn):
        if (self.entering):
            dw = ((vx1.Mat.refractive-vx2.Mat.refractive)/(vx1.Mat.refractive+vx2.Mat.refractive))**2
            self.w-=dw
            self.grid.refl+=dw
            self.entering=False
            self.vx = vx2
            return
        if not(isinstance(vx2.Mat,type(vx1.Mat))):
            S = np.zeros(2)
            alph = np.arccos(abs(self.vv[mn]))
            alpht = np.arcsin(min(vx1.Mat.refractive*np.sin(alph)/(vx2.Mat.refractive),1))
            if (vx1.Mat.refractive>vx2.Mat.refractive)&(alph>np.arcsin(min(vx2.Mat.refractive/vx1.Mat.refractive,1))):
                S[0] = 1
            else:
                S = self.Fresnel(alph,alpht)[0]
            if ((np.random.uniform() <= S[0]/self.stokes[0])&(self.bounces<100)):
                self.vv[mn] = -self.vv[mn]
                self.stokes[3]*=-1
                self.bounces+=1
                return
            else:
                f = vx1.Mat.refractive/vx2.Mat.refractive
                ta = np.array([f,f,f])
                ta[mn]=0
                mnf = np.sign(self.vv[mn])*np.cos(alpht)
                self.vv *=ta
                self.vv[mn] = mnf
                self.vv/=spl.norm(self.vv)#Floating point error often puts it slightly above 1
                if (vx2.is_detector):
                    w1 = (self.detfilter@self.stokes)[0]/self.stokes[0]
                    w2 = 1-w1
                    self.grid.detector_bin(self.rv, alpht,w1*self.w)
                    self.grid.filter_bin(self.rv,alpht,w2*self.w)
                    self.dead=1
                    return
                else:
                    if (self.scatters==0)&(vx2.is_ambient):
                        self.grid.bin_unsc_ref(self.rv,alpht,self.w)
                        self.dead=1
                        return
                    else:
                        if vx2.is_edge:
                            self.grid.bin_scattered_trans(self.rv, alpht,self.w)
                            self.dead=1
                            return
                        else:
                            if vx2.is_ambient:
                                self.grid.bin_diff_ref(self.rv,np.sin(np.arccos(abs(self.vv[mn]))),self.w)
                                self.dead=1
                                return
        else:
            if (vx2.is_detector):
                  self.grid.detector_bin(self.rv, np.arccos(abs(self.vv[mn])),self.w)
                  self.dead=1
                  return
            else:
                if vx2.is_edge:
                     self.grid.bin_scattered_trans(self.rv, np.arccos(abs(self.vv[mn])),self.w)
                     self.dead=1
                     return
                else:
                     if vx2.is_ambient:
                         self.grid.bin_unsc_ref(self.rv,np.sin(np.arccos(abs(self.vv[mn]))),self.w)
                         self.dead=1
                         return
        self.vx = vx2
        return
        #Specular reflectance at boundary
    def scatter(self):
        if self.vx.Mat.aniso == 0:
            th = np.arccos(2*np.random.uniform()-1)
        else:
            g=self.vx.Mat.aniso
            th = (1/(2*g))*(1 + g**2 - ((1-g**2)/(1-g+2*g*np.random.uniform()))**2)
        ph = 2*np.pi*np.random.uniform()
        a,p,ns = self.vx.Mat.spheroidParams()
        M = spheroidM2M(a,p,ns,self.vx.Mat.refractive,th,2*np.sin(th/2)/(self.wl),self.wl)
        Sz = self.stokes
        print(self.stokes)
        self.stokes = M.applyM(self.stokes)
        print(M.M)
        print(Sz-self.stokes)
        self.rot.apply(self.vv)
        if 1-abs(self.vv[2])<1e-8:
            self.vv[0] = np.sin(th)*np.cos(ph)
            self.vv[1] = np.sin(th)*np.sin(ph)
            self.vv[2] = np.sign(self.vv[2])*np.cos(th)
            self.vv/=spl.norm(self.vv)
        else:
            tv = np.copy(self.vv)
            self.vv[0] = tv[0]*np.cos(th) + np.sin(th)*(tv[0]*tv[2]*np.cos(ph)-tv[1]*np.sin(ph))/(np.sqrt(1-tv[2]**2))
            self.vv[1] =  tv[1]*np.cos(th) + np.sin(th)*(tv[1]*tv[2]*np.cos(ph)+tv[0]*np.sin(ph))/(np.sqrt(1-tv[2]**2))
            self.vv[2] = (np.sqrt(1-tv[2]**2))*np.sin(th)*np.cos(ph) + tv[2]*np.cos(th)
            self.vv/=spl.norm(self.vv)
        self.scatters+=1
        if (self.entering)&(self.vv[2]<=0):
            self.dead=1
            self.grid.early_scatter=np.append(self.grid.early_scatter,self.w)
        self.rot.apply(self.vv,inverse=True)
        return
    def move(self,cycles):
        l = self.getsts()
        self.chkbnds(l) #Propagate updating location and weight as necessary
        self.rhist = np.concatenate((self.rhist.flatten(),self.rv)).reshape(self.rhist.size//3 + 1,3)
        if self.dead==1:
            return cycles
        dw = self.w * self.vx.Mat.absorp/self.vx.Mat.getExt()
        self.w-=dw
        if self.scatters < 1:
            self.grid.unsc_abs=np.append(self.grid.unsc_abs,dw)
        else:        
            self.grid.add_scatter(self.rv,dw)
        if self.w<=self.wth: #If below threshold risk termination
            self.russian_roulette()
            if self.dead==1:
                return cycles
        self.scatter()
        if self.dead==1:
                return cycles
        cycles = self.move(cycles+1)
        return cycles
        

        
def randdist(x, pdf, nvals):
    """Produce nvals random samples from pdf(x), assuming constant spacing in x."""

    # get cumulative distribution from 0 to 1
    cumpdf = np.cumsum(pdf)
    cumpdf *= 1/cumpdf[-1]

    # input random values
    randv = np.random.uniform(size=nvals)

    # find where random values would go
    idx1 = np.searchsorted(cumpdf, randv)
    # get previous value, avoiding division by zero below
    idx0 = np.where(idx1==0, 0, idx1-1)
    idx1[idx0==0] = 1

    # do linear interpolation in x
    frac1 = (randv - cumpdf[idx0]) / (cumpdf[idx1] - cumpdf[idx0])
    randdist = x[idx0]*(1-frac1) + x[idx1]*frac1

    return randdist
        
def pr_traj(v1,v2):
    l = []
    if np.size(v2)==1:
        l.append(v1>v2)
    for v in range(len(v2)-1):
        l.append(np.intersect1d(v1[v1>v2[v]],v1[v1<v2[v+1]]))
    return l
def contains_point(self, p):
    out1=([p[0] <= self.xrang()[1],
             p[1] <= self.yrang()[1],
             p[2] <= self.zrang()[1]])
    out2=([p[0] > self.xrang()[0],
             p[1] > self.yrang()[0],
             p[2] > self.zrang()[0]])
    return np.all((np.logical_and(out1,out2)),0)
def Rf(th,th_in,th_crit,th_ref,nr):
    if th_in<th_crit:
        return 0.5*(((nr*np.cos(th_ref) - np.cos(th))/(nr*np.cos(th_ref) +np.cos(th)))**2+((nr*np.cos(th_in) - np.cos(th_ref))/(nr*np.cos(th) +np.cos(th)))**2)
    else:
        return 1

def getD3(r):
    return r/(spl.norm(r))

def getCr(matin,matout,r,n):
    nr = matout.refractive/matin.refractive
    th_in = np.arccos(np.dot(r/spl.norm(r),n/spl.norm(n)))
    th_ref = np.arcsin(np.sin(th_in)*matout.refractive/matin.refractive)
    th_crit = np.arcsin(1/(matout.refractive/matin.refractive))
    fun = lambda th:np.sin(th)*np.cos(th)*Rf(th,th_in,th_crit,th_ref,nr)*(2 + 3*np.cos(th))
    Rsum = spi.quad(fun, 0.01,np.pi/2-0.01)[0]
    return (1 + (Rsum)/(2-Rsum))/(1 - (Rsum)/(2-Rsum)),Rsum


def getDiffEq(n,norm,mat,S0,r,stepsize=0.01,l=1,stat=True,t=0):
    if stat:
        S = S0
        for i in range(1,n):
            cr,reff=getCr(mat[i-1],mat[i],r[i-1][:,-2],norm)
            bVal = 2*cr*mat[i].getD()
            print("Reflected: " + str(reff))
            print("Transmitted: " + str(1-reff))
        print(bVal)
        r[0] = np.concatenate((r[0],r[0]+np.array(np.array((r[0][:,-1]+getD3(r[0][:,-1])*stepsize,)).transpose())),1)
        rhs1,rhs2=S.GFun(r[0],t,mat[0].absorp,bVal*getD3(r[0][:,-1]),stepsize*getD3(r[0][:,-1]))
        az=np.argmax(rhs1)
        ax = np.argmax(rhs2)
        na=10
        rhs1 = (spf.fft((rhs1)/(mat[0].absorp),na*len(rhs1)))
        rhs2 = (spf.fft(((rhs2))/(mat[0].absorp),na*len(rhs2)))        
        v1 = l[0]*na/(spf.fft(np.linspace(0,np.amax(r[0]),len(rhs1)),len(rhs1)))
        v2 = l[0]*na/(spf.fft(np.linspace(0,np.amax(r[0]),len(rhs2)),len(rhs2)))
        rhs1=rhs1/(1+(abs(v1)**2)/(mat[0].getEff()**2))
        rhs2=rhs2/(1+(abs(v2)**2)/(mat[0].getEff()**2))
        rhs1=spf.fftshift(spf.ifft((rhs1)))
        oz=spf.fftshift(((((spf.ifft(np.flip(rhs2)))))))
        out1 =rhs1[len(rhs1)//2-1:-1:5]-oz[0:len(oz)//2:5]
        out_list = []
        out_list.append(out1)
        for i in range(1,n):
            nsource = np.zeros(len(r[i][1]))
            nsource[0]+=1/mat[i].absorp
            print("new length: " + str(len(nsource)))
            fns = spf.fft(nsource,na*len(nsource))
            vf = l[i]*na/(spf.fft(np.linspace(0,np.amax(r[i]),len(fns),len(fns))))
            fns=fns/(1+(abs(vf)**2)/(mat[i].getEff()**2))
            fns =spf.fftshift(spf.ifft(fns))
            out_list.append(fns[0:len(fns)//2:5])
    return r,out_list



def zf(t):
    return np.uint32(t-1<5e-2)

def diffusion(source,r,mat,grid,position,bounds,time=0):
    n=5000
    z =(np.fft.fft(sps.unit_impulse(2*n,100),2*n))
    #z=cft(zf,np.linspace(0,100,2*n))
    zxx = np.copy(z)
    v=(1/r)*abs(spf.fftfreq(4*n))[0:2*n]
    z*=(mat.getEff()**22)/(mat.absorp*((mat.getEff()**2)+((((2*np.pi**2)*(v))**2))))
    zz = np.copy(z)
    print(mat.getEff())
    z=((np.fft.ifft((z)/(np.sqrt(2*n)))))[0:len(z)//2:1]
    #z/=abs(np.linspace(0,r,len(z)))
    pr = np.argmax(np.abs(z))
    zdz=(np.abs(z))/abs(np.linspace(-r*pr/n,r*(n-pr)/n,len(z)))
    return zdz/np.amax(zdz),np.linspace(0,r,n)

def mcphoton(vx,grid,n=1000):
    num = np.zeros(n)
    vxi = vx.indices
    p=[]
    r=[]
    for i in range(n):
        rhz = np.random.normal(0,0.02/np.sqrt(2))
        np.clip(rhz,-0.05/np.sqrt(2),0.05/np.sqrt(2))
        thz = 2*np.pi*np.random.uniform()
        p.append(polPacket(vx,grid,np.array([0.,0.,0.])+np.array((rhz*np.cos(thz),rhz*np.sin(thz),0.)),np.array([0.,0.,1.]),1,1e-5,10,azim_spread=0.2,ur=np.array([0.,0.,1.]),stokes=np.array((1,1,0,0))))
        n= p[i].move(1)
        num[i] = abs(n)
        r.append(p[i].rhist)
    return num,r
    
def propagate(S,norm,start,stop,vxm,stepsize=0.0001):
    seg = segm(point(start[0],start[1],start[2]),point(stop[0],stop[1],stop[2]))
    voxels = np.array(seg.findVoxels(vxm)).flatten()

    vx = np.array(np.zeros(len(voxels)),dtype=bool)
    for n in range(len(voxels)):
        vx[n] = seg.checkInt(voxels[n])
    voxels = voxels[vx]
    d=[]
    l=[]
    proprange = np.arange(0,spl.norm(np.array(start)-np.array(stop)),stepsize)
    pvec = (start + proprange[:,np.newaxis]*seg.getd()[np.newaxis,:]).transpose()
    for v in range(len(voxels)):
        d.append(contains_point(voxels[v],pvec))
    for n in range(len(np.array(d)[np.any(d,1)==True])):
        l.append(spl.norm(pvec[:,d[n]][0]-pvec[:,d[n]][-1]))
    r,o = getDiffEq(2,norm,[air(),water()],S,[pvec[:,d[0]],pvec[:,d[1]]],stepsize,l)
    return r,o
