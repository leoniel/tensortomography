import matplotlib.pyplot as plt
import math
import numpy as np
import csv
import time
import os
import threading as thr
import time
from math import floor,ceil
from scipy import signal as sps
from scipy import fftpack as spf
from scipy import linalg as spl
from scipy import ndimage as spn
from abc import ABC,abstractmethod
from numpy import linalg as lg

class EVec(ABC):
    @abstractmethod
    def r2():
        pass
    @abstractmethod
    def r1():
        pass
    @abstractmethod
    def r():
        pass
    @abstractmethod
    def x():
        pass
    @abstractmethod
    def y():
        pass
    @abstractmethod
    def z():
        pass
    @abstractmethod
    def __add__():
        pass
    @abstractmethod
    def __mul__():
        pass
    @abstractmethod
    def __iadd__():
        pass
    @abstractmethod
    def __imul__():
        pass
    
class point(EVec):
    def __init__(self,x,y,z):
        self.rv = np.array((x,y,z))
    def r2(self):
        return np.dot(self.rv,self.rv)
    def r1(self):
        return np.norm(self.rv)
    def r(self):
        return np.copy(self.rv)
    def x(self):
        return self.rv[0]
    def y(self):
        return self.rv[1]
    def z(self):
        return self.rv[2]
    def __iadd__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot add  points!")
        if isinstance(o,displacement):
            self.rv += o.rv
            return self
    def __isubtract__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot implicitly subtract points!")
        if isinstance(o,displacement):
            self.rv -= o.rv()
            return self
    def __add__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot add  points!")
        if isinstance(o,displacement):
            return point(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
        try: return self.rv+o
        except TypeError:
            print("Cannot add!")
    def __radd__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot add  points!")
        if isinstance(o,displacement):
            return point(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
    def __sub__(self,o):
        

        if isinstance(o,displacement):
            return point(self.x()-o.x(), self.y()-o.y(),self.z()-o.z())
        if isinstance(o,point):
            return displacement(self.x() - o.x(), self.y() - o.y(), self.z() - o.z())
        try:
            return self.rv+(-o)
        except TypeError:
            print("Invalid type!")
    def __rsub__(self,o):
        

        if isinstance(o,displacement):
            raise TypeError("Cannot subtract points from displacements!")
        if isinstance(o,point):
            return displacement(o.x() - self.x(), o.y() - self.y(), o.z() - self.z())
        try:
            return self.rv+(-o)
        except TypeError:
            print("Invalid type!")
    def __mul__():
        raise TypeError("Cannot mul points! Use point.r to get vector!")
    def __imul__():
        raise TypeError("Cannot mul points! Use point.r to get vector!")
        
class displacement(EVec):
    def __init__(self,x,y,z):
        self.rv = np.array((x,y,z))
    def r2(self):
        return spl.norm(self.rv)**2
    def r1(self):
        return spl.norm(self.rv)
    def r(self):
        return np.copy(self.rv)
    def x(self):
        return self.rv[0]
    def y(self):
        return self.rv[1]
    def z(self):
        return self.rv[2]
    def __iadd__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot implicitly add points to displacements!")
        if isinstance(o,displacement):
            self.rv += o.rv
            return self
    def __isubtract__(self,o):
        t = type(o)
        if isinstance(o,point):
            raise TypeError("Cannot implicitly subtract points from displacements!")
        if isinstance(o,displacement):
            self.rv -= o.rv
            return self
    def __add__(self,o):
        t = type(o)
        if isinstance(o,point):
            return point(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
        if isinstance(o,displacement):
            return displacement(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
    def __radd__(self,o):
        t = type(o)
        if isinstance(o,point):
            return point(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
        if isinstance(o,displacement):
            return displacement(self.x()+o.x(),self.y()+o.y(),self.z()+o.z())
    def __sub__(self,o):
        
        if isinstance(o,point):
            raise TypeError("Cannot subtract points from displacements!")
        if isinstance(o,displacement):
            return displacement(self.x()-o.x(), self.y()-o.y(),self.z()-o.z())
    def __rsub__(self,o):
        
        if isinstance(o,point):
            return point(o.x() - self.x(), o.y() - self.y(), o.z() - self.z())
        if isinstance(o,displacement):
            return displacement(o.x()-self.x(), o.y()-self.y(),o.z()-self.z())
    def __mul__(self,o):
        
        if isinstance(o,point):
            raise TypeError("Cannot mul point by displacement!")
        if isinstance(o,displacement):
            return np.dot(self.rv,o.rv())
        try:
            return self.rv*o
        except TypeError:
            Print("Must be allowed type, or a number!")
    def __rmul__(self,o):
        
        if isinstance(o,point):
            raise TypeError("Cannot mul point by displacement!")
        if isinstance(o,displacement):
            return np.dot(self.rv,o.rv())
        try:
            return self.rv*o
        except TypeError:
            Print("Must be allowed type, or a number!")
    def __imul__(self,o):
        
        if isinstance(o,point):
            raise TypeError("Cannot mul point by displacement!")
        if isinstance(o,displacement):
            raise TypeError("Cannot implicitly multiply displacements!")
class voxel(point):
    def __init__(self,x,y,z,w):
        self.rv = np.array((x,y,z))
        self.side=w
    def checkNbh(self,o):
        if not(isinstance(o,voxel)):
            raise TypeError("Argument must be a voxel!")
        d = (self-o).r1()
        if abs(d - self.side) < 1e-14:
            return True
        else:
            return False

class voxelMesh(point):
    def __init__(self,x,y,z,nx,ny,nz,w):
        self.rv = np.array([x,y,z])
        self.nums = np.ndarray.astype(np.array((nx,ny,nz)),int)
        self.w = w
        self.mesh=[[['' for i in range(nz)]for j in range(ny)] for k in range(nx)]
        for i in range(nx):
            for j in range(ny):
                for k in range(nz):
                    self.mesh[i][j][k] =voxel(self.rv[0] + w*i,self.rv[1] + w*j,self.rv[2] + w*k,w)
    def vx(self,i,j,k):
        return self.mesh[i][j][k]
    def nx(self):
        return self.nums[0]
    def ny(self):
        return self.nums[1]
    def nz(self):
        return self.nums[2]
    def geth(self):
        return self.w*self.nz() - self.z()
    def getw(self):
        return self.w*self.nx() - self.x()
    def getd(self):
        return self.w*self.ny() - self.y()
    def getrangez(self,z1,z2):
        z_u = max(z1,z2)
        z_l = min(z1,z2)
        if ((z_u < self.z())|(z_l > (self.z()+self.geth()))):
            return range(0,0)
        z_l = max(z_l,self.z())
        z_u = min(z_u,self.z()+self.nz()*self.geth())
        rn = max(self.z() + self.geth()-z_l,0) - max(self.z()+ self.geth()-z_u,0)
        return range(floor(z_l/self.w), ceil((z_l + rn)/self.w))
    def getrangey(self,y1,y2):
        y_u = max(y1,y2)
        y_l = min(y1,y2)
        if (y_u < self.y())|(y_l > (self.y()+self.getd())):
            return range(0,0)
        y_l = max(y_l,self.y())
        y_u = min(y_u,self.y()+self.ny()*self.getd())
        rn = max(self.y() + self.getd()-y_l,0) - max(self.y()+ self.getd()-y_u,0)
        return range(floor(y_l/self.w), ceil((y_l + rn)/self.w))
    def getrangex(self, x1,x2):
        x_u = max(x1,x2)
        x_l = min(x1,x2)
        if (x_u < self.x())|(x_l > (self.x()+self.getw())):
            return range(0,0)
        x_l = max(x_l,self.x())
        x_u = min(x_u,self.x()+self.nx()*self.getw())
        rn = max(self.x() + self.getw()-x_l,0) - max(self.x()+ self.getw()-x_u,0)
        return range(floor(x_l/self.w), ceil((x_l + rn)/self.w))
    def getrange(self, p1,p2):
        return np.array([self.getrangex(p1.x(),p2.x()),self.getrangey(p1.y(),p2.y()),self.getrangez(p1.z(),p2.z())])
class segm:
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2
    def getD(self):
        d = ((self.p2-self.p1).r())
        return d
    def x(self):
        return np.array((self.p1[0],self.p2[0]))
    def y(self):
        return np.array((self.p1[1],self.p2[1]))
    def z(self):
        return np.array((self.p1[2],self.p2[2]))
    def dz(self):
        return self.p1[2]-self.p2[2]
    def dx(self):
        return self.p1[0]-self.p2[0]
    def dy(self):
        return self.p1[1]-self.p2[1]
    def dp(self):
        return  self.p2-self.p1
    def findVoxels(self,vxm):
        rng = vxm.getrange(self.p1,self.p2)
        self.vx = [[['' for i in rng[2]]for j in rng[1]] for k in rng[0]]
        for i in (rng[0]):
            for j in (rng[1]):
                for k in (rng[2]):
                    self.vx[i][j][k] = vxm.vx(i,j,k)
        return self.vx
    def checkInt(self,vx):
        d = self.getD()
        n = np.array([(0,1),(0,2),(1,2)])
        b = np.array([[0,0,0],[0,0,0],[0,0,0],[0,0,0]])
        n0 = np.array([[1,0],[0,1],[1,0],[0,1],[1,0],[0,1]])
        p0 = np.array([[[0,0,0],[0,0,0],[0,1,0],[1,0,0]],[[0,0,0],[0,0,0],[0,0,1],[1,0,0]],[[0,0,0],[0,0,0],[0,0,1],[0,1,0]]])
        for i in range(3):
            for k in range(4):
                    v1 = (self.p1.r() - vx.r()-p0[i,k]*vx.side)
                    D = (vx.side*n0[k,0]*d[n[i,1]]-vx.side*n0[k,1]*d[n[i,0]])
                    M =np.array([[d[n[i,1]],-d[n[i,0]]],[-vx.side*n0[k,1],vx.side*n0[k,0]]])*(1/D)
                    a = M@np.transpose([v1[n[i,0]],v1[n[i,1]]])
                    b[k,i]=(((a[0]>=0)&(a[0]<=1))&(((-a[1])>=0)&((-a[1])<=1)))
        if ((int(sum(b)[0]>0)+int(sum(b)[1]>0)+int(sum(b)[2]>0))>2):
            return True
        if ((int(sum(b)[0]>0)+int(sum(b)[1]>0)+int(sum(b)[2]>0))==2):
            i = np.argmin(sum(b))
            if ((self.p1.r()[i] in range(vx.r()[i],vx.r()[i]+vx.side))&(self.p2.r()[i] in range(vx.r()[i],vx.r()[i]+vx.side))):
                return True
        else:
            return False
class line(segm):
    def __init__(self,f = np.array([[1.0],[1.0]]),d=np.array((1,0,0)),m=np.array((0,0,0))):
        self.p1 = m
        self.p2 = m+d/spl.norm(d)
        self.d=d/spl.norm(d)
    def getM():
        return self.p1.r()
    
#l = voxelMesh(0,0,0,100,100,100,5)
#s = segm(point(1,1,1),point(500,15,1))
#fv = np.shape(s.findVoxels(l))
#print(fv)
#red = []
#c=0
#v=0
#for i in range(0,fv[0]):
    #for j in range(0,fv[1]):
        #for k in range(0,fv[2]):
            #c+=1
            #if s.checkInt(s.vx[i][j][k]):
                #red.append(s.vx[i][j][k])
                #v+=1


#print(v)
#print(c)
#print(v/c)
