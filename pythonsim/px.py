from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
import math
import numpy as np
import csv
import time
import os
import threading as thr
import time
import sys
import voxelgeo
from electro import *
#import props
from math import floor,ceil
from scipy import signal as sps
from scipy import fftpack as spf
from scipy import linalg as spl
from scipy import ndimage as spn
from abc import ABC,abstractmethod
from numpy import linalg as lgnp
from scipy import optimize as spo
from scipy import constants as con
from scipy.spatial.transform import Rotation as R
np.set_printoptions(precision=2)
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

hz = np.array([7,18,46,65])*2*np.pi
phinul = 0*np.pi*np.ones((4,1)) 
phinul[1] = 0 
phinul[2] = 0
phihalf = (np.pi/2)*np.ones((4,1))
phione = 1*np.pi*np.ones((4,1))
phinul=phinul@np.ones((1,64))
phihalf=phihalf@np.ones((1,64))
phione=phione@np.ones((1,64))
Fs = 64
tmax=1.
t = np.arange(0,tmax,1/Fs)

f_t = (np.sin(hz[0]*t+phinul[0]))
g_t = (np.sin(hz[1]*t+phinul[1]))
h_t = (np.sin(hz[2]*t+phinul[2]))
j_t = (np.sin(hz[3]*t+phinul[3]))

#f_t = np.sum(np.sin(hz[0,np.newaxis]@t[np.newaxis]+phinul[0]),0)
#g_t = np.sum(np.sin(hz[1,np.newaxis]@t[np.newaxis]+phinul[1]),0)
#h_t = np.sum(np.sin(hz[2,np.newaxis]@t[np.newaxis]+phinul[2]),0)
#j_t = np.sum(np.sin(hz[3,np.newaxis]@t[np.newaxis]+phinul[3]),0)

F_w = spf.fftshift(spf.fft(f_t))
G_w = spf.fftshift(spf.fft(g_t))
H_w = spf.fftshift(spf.fft(h_t))
J_w = spf.fftshift(spf.fft(j_t))

fr=np.linspace(-Fs//2+1,Fs//2,Fs)
gs = gs.GridSpec(2,3)

fig = plt.figure()
ax1 = fig.add_subplot(gs[0,:])
ax2 = fig.add_subplot(gs[1,0])
ax3=fig.add_subplot(gs[1,1])
ax4=fig.add_subplot(gs[1,2])

ax1.plot(t,f_t,'--',label='sin(t*1*2*pi + 0)')
ax1.plot(t,g_t,'-.',label='sin(t*18*2*pi + $\pi$)')
ax1.plot(t,h_t,'-',label='sin(t + $\pi)$')
ax1.plot(t,j_t,'-',label='sin(tw + $\pi)$')
ax1.set_title('Signal samples (Frequencies: ' + str(hz/(2*np.pi)) + ' s$^{-1}$)')
ax1.set_xlabel('Time (s)')
ax1.set_ylabel('Amplitude (AU)')
ax1.legend()

ax2.plot(fr,F_w.real,'-',color='C0',label='Re[F(w,0)]')
ax3.plot(fr,G_w.real,'-',color='C1',label='Re[F(w,$\pi$/2)]')
ax4.plot(fr,H_w.real,'-',color='C2',label='Re[F(w,$\pi$)]')
ax4.plot(fr,J_w.real,'-',color='C2',label='Re[F(w,$\pi$)]')

ax2.plot(fr,F_w.imag+40-np.amin(F_w.imag),'-.',color='C0',label='Im[F(w,0)]')
ax3.plot(fr,G_w.imag+40-np.amin(G_w.imag),'-.',color='C1',label='Im[F(w,$\pi/2$)]')
#ax4.plot(fr,H_w.imag+40-np.amin(H_w.imag),'-.',color='C2',label='Im[F(w,$\pi$)]')
ax4.plot(fr,H_w.imag+40-np.amin(H_w.imag),'-.',color='C2',label='Im[F(w,$\pi$)]')

ax2.set_title('Frequency spectrum at Fs = ' + str(Fs) + ' s$^{-1}$, and $\phi = 0$')
ax2.set_xlabel('Frequency (s$^{-1}$)')
ax2.set_ylabel('Intensity (AU)')
ax3.set_title('Frequency spectrum at Fs = ' + str(Fs) + ' s$^{-1}$, and $\phi = \pi/2$')
ax3.set_xlabel('Frequency (s$^{-1}$)')
ax3.set_ylabel('Intensity (AU)')
ax4.set_title('Frequency spectrum at Fs = ' + str(Fs) + ' s$^{-1}$, and $\phi = \pi$')
ax4.set_xlabel('Frequency (s$^{-1}$)')
ax4.set_ylabel('Intensity (AU)')
ax3.legend()
ax2.legend()
ax2.set_ylim((-10,140))
ax3.set_ylim((-10,140))
ax4.set_ylim((-10,140))
ax4.legend()

plt.show()
