import scipy
import matplotlib.pyplot as plt
from scipy.fftpack import fft, ifft
import math
import numpy as np

class mueller:
    def __init__(self,theta,freq, phase, amp,mtype):
        self.theta = theta
        self.amp = amp
        self.freq = freq
        self.mtype = mtype
        self.phase = phase
    def editValue(self,label,val):
        if label == "f":
            self.freq = val
        if label == "a":
            self.amp = val
        if label == "theta":
            self.theta = val
        if label == "phase":
            self.phase = val
    def getBiMatrix(self,t):
        if self.mtype == "bi":
            delta = self.amp*np.sin(self.phase - self.freq*t)
            self.mat = np.array([[1, 0, 0, 0],
                                [0, np.cos(2*self.theta)**2 + (np.sin(2*self.theta)**2)*np.cos(delta),
                                np.cos(2*self.theta)*np.sin(2*self.theta)*(1 - np.cos(delta)),
                                np.sin(2*self.theta)*np.sin(delta)],
                                [0, np.cos(2*self.theta)*np.sin(2*self.theta)*(1 - np.cos(delta)),
                                (np.cos(2*self.theta)**2)*np.cos(delta) + np.sin(2*self.theta)**2,
                                -np.cos(2*self.theta)*np.sin(delta)],
                                [0, -np.sin(2*self.theta)*np.sin(delta), np.cos(2*self.theta)*np.sin(delta), np.cos(delta)]])
            return self.mat
        else:
            raise ValueError("Wrong matrix type!")
    def getRotMatrix(self):
        if self.mtype == "rot":
            self.mat = np.array([[1, 0, 0, 0],
                                [0, np.cos(2*self.theta),
                                -np.sin(2*self.theta),
                                0],
                                [0, np.sin(2*self.theta),
                                np.cos(2*self.theta),
                                0],
                                [0,0,0,1]])
            return self.mat
        else:
            raise ValueError("Wrong matrix type!")
        
    def getPolMatrix(self):
        if self.mtype == "pol":
            thmat = np.array([[1, 0, 0, 0],
                                [0, np.cos(2*self.theta),
                                -np.sin(2*self.theta),
                                0],
                                [0, np.sin(2*self.theta),
                                np.cos(2*self.theta),
                                0],
                                [0,0,0,1]])
            polmat = np.array([[0.5,0.5,0,0],[0.5,0.5,0,0],[0,0,0,0],[0,0,0,0]])
            self.mat = np.dot(np.dot(thmat,polmat),np.transpose(thmat))
            return self.mat
        else:
            raise ValueError("Wrong matrix type!")

class stokes:
    def __init__(self,i,p,chi,psi):
        self.invec = np.array([i,i*p*np.cos(2*psi)*np.cos(2*chi),i*p*np.sin(2*psi)*np.cos(2*chi),i*p*np.sin(2*chi)])
        self.outvec = self.invec
        self.elems = []
    def addElem(self,m):
        self.elems.append(m)
    def updateOutvec(self,t):
        self.outvec = self.invec
        for m in self.elems:
            if m.mtype == "bi":
                m.getBiMatrix(t)
            elif m.mtype=="pol":
                m.getPolMatrix()
            self.outvec = np.dot(m.mat,self.outvec)
          #  print(m.mat)
           # print(self.outvec)
        

m0 = mueller(0,0,0,1,"pol")
m3 = mueller(math.pi/4.0,0,0,1,"pol")

m = mueller(math.pi/4.0,0,math.pi/2.0,math.pi,"bi")
m1 = mueller(math.pi/3.0,0,math.pi/2.0,math.pi/4.0,"bi")
m2 = mueller(0,0,math.pi/2.0,math.pi,"bi")
print(m0.getPolMatrix())
print(m3.getPolMatrix())
print(m.getBiMatrix(0))
print(m2.getBiMatrix(0))
s = stokes(1,1,0,0.0)
s.addElem(m0)
s.addElem(m)
s.addElem(m1)
s.addElem(m2)
s.addElem(m3)
print(s.elems)
print(s.invec)
s.updateOutvec(0)
print(s.outvec)
y = [[] for i in range(16)]
yf=[[] for i in range(16)]
phasevar = [0, 0, math.pi/6.0,math.pi/6.0]
phasevar2 = [math.pi/6.,-math.pi/6.,0,math.pi/2.0]
fig, (ax1, ax2) = plt.subplots(2, 1)
for j in range(4):
    m.editValue("phase",phasevar[j])
    print(m.getBiMatrix(0))
    m2.editValue("phase",phasevar2[j])
    print(m2.getBiMatrix(0))
    #m2.editValue("phase",-math.pi/5.0 + phasevar[j])
    for i in range(1000):
        #m1.editValue("a",(j)*1/2.0)
        s.updateOutvec(i*0.005)
        y[j].append(s.outvec[0])
    #yf[j].extend(fft(y[j]))
    #ax1.plot(1.0/2500.0 * np.real(yf[j][200:300]),label="Th = pi"+str((j)/6.0))
    ax2.plot(np.multiply(np.transpose(np.array([range(100)])),0.005),y[j][0:100],label="j = "+ str(j))

ipp=np.divide((-y[0][0]+y[1][0]),(y[0][0]+y[1][0]))
ip=np.divide((-y[3][0]+y[2][0]),(y[3][0]+y[2][0]))
print("ipp:" +str(ipp))
print("ip:" + str(ip))
print([y[0][0], y[1][0], y[2][0], y[3][0]])
print("DELTA: " + str(np.arcsin((np.sqrt(np.square(ip) + np.square(ipp))))))

print("PSI: " + str(2*np.arctan((ip)/(ipp))))
print(
str(np.divide(np.mean(np.square(y[0]))-np.mean(np.square(y[2])),np.mean(np.square(y[0]))+np.mean(np.square(y[2])))) + ' ---- ' +
str(np.divide(np.mean(np.square(y[1]))-np.mean(np.square(y[3])),np.mean(np.square(y[1]))+np.mean(np.square(y[3])))))

print(s.outvec)
ax1.legend()
ax2.legend()
ax1.set_ylabel('Intensity (AU)')
ax1.set_xlabel('Frequency')
ax2.set_xlabel('Time')
ax2.set_ylabel('Amplitude')
plt.show()
