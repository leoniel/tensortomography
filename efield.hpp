#include <algorithm>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <random>
#include <vector>
#include <typeinfo>
#include <tuple>
#include <vector>

#include <"Matrix.hpp">
#include <mpi.h>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
namespace champion{
    
template <typename T, size_type N> class EMWave;
template <typename T, size_type N> class Stokes;
template <typename T, size_type N> class Jones;
template <typename T, size_type N, size_type M> class Mueller;
template <typename T, size_type N, size_type M> class JonesM;


template <typename T> class EMWave {
  public:
    std::vector<T> w;
    std::vector<T> e;
    std::vector<T> k;
    using typename std::vector<T>::iterator = T *;
    using typename std::vector<T>::const_iterator = const T *;
    // Default constructor
    EMWave() : w{0}, e{0}, k{0} {}
    // Copy constructor
    EMWave(const EMWave() &) = default;
    // Copy assignment
    EMWave &operator=(const EMWave &) = default;
    // Move constructor
    EMWave(EMWave &&) = default;
    // Move assignment
    EMWave &operator=(EMWave &&) = default;
    // Generalized copy constructor
    template <typename T2>
    EMWave(const EMWave<T2> &E)
        : e{static_cast<std::vector<T>>(E.w)}, w{static_cast<std::vector<T>>(E.e)}, k{static_cast<std::vector<T>>(
                                                              E.k)} {}
    // Generalized copy assignment
    template <typename T2> EMWave &operator=(const EMWave<T2> &E) {
        for (size_type i = 0; i != this->size(); ++i) {
            (*this)[i] = E[i];
        }
        return *this;
    }
    // Construct from parameters
    EMWave{std::vector<T> s0_, T s1_, T s2_, T s3_) : s0{s0_}, s1{s1_}, s2{s2_}, s3{s3_} {}
    // Construct from Point<T> (definition in Point<T>.hpp)
}


template <typename T> class Stokes {
  public:
    using iterator = T *;
    using const_iterator = const T *;
    // Default constructor
    Stokes() : s0{0}, s1{0}, s2{0}, s3{0} {}
    // Copy constructor
    Stokes(const Stokes &) = default;
    // Copy assignment
    Stokes &operator=(const Stokes &) = default;
    // Move constructor
    Stokes(Stokes &&) = default;
    // Move assignment
    Stokes &operator=(Stokes &&) = default;
    // Generalized copy constructor
    template <typename T2>
    Stokes(const Stokes<T2> &S)
        : s0{static_cast<T>(S.s0)}, s1{static_cast<T>(S.s1)}, s2{static_cast<T>(
                                                              S.s2)}, s3{static_cast<T>(S.s3)} {}
    // Generalized copy assignment
    template <typename T2> Stokes &operator=(const Stokes<T2> &V) {
        for (size_type i = 0; i != this->size(); ++i) {
            (*this)[i] = V[i];
        }
        return *this;
    }
    // Construct from parameters
    Stokes(T s0_, T s1_, T s2_, T s3_) : s0{s0_}, s1{s1_}, s2{s2_}, s3{s3_} {}
    // Construct from Point<T> (definition in Point<T>.hpp)
}

template <typename T> class Jones {
  public:
    using iterator = T *;
    using const_iterator = const T *;
    // Default constructor
    Jones() : j0{0}, j1{0} {}
    // Copy constructor
    Jones(const Jones &) = default;
    // Copy assignment
    Jones &operator=(const Jones &) = default;
    // Move constructor
    Jones(Jones &&) = default;
    // Move assignment
    Jones &operator=(Jones &&) = default;
    // Generalized copy constructor
    template <typename T2>
    Jones(const Jones<T2> &V)
        : s0{static_cast<T>(J.j0)}, s1{static_cast<T>(J.j1)} {}
    // Generalized copy assignment
    template <typename T2> Jones &operator=(const Jones<T2> &V) {
        for (size_type i = 0; i != this->size(); ++i) {
            (*this)[i] = V[i];
        }
        return *this;
    }
    // Construct from parameters
    Jones(T j0_, T j1_) : j0{j0_}, j1{j1_} {}
}

//Element_Type should be real, dimensions should be D^2xD^2!
template <typename element_type, size_type N, size_type M>
struct matrix_type<element_type, N, M, true, false, false> {
    using type = Mueller<element_type, N>;
};

//Element_Type should be complex, dimensions should be DxD!
template <typename element_type, size_type N, size_type M>
struct matrix_type<element_type, N, M, true, false, false> {
    using type = Jones<element_type, N>;
};


template <typename T, size_type N = 0>
class Mueller: public SquareMatrix<T, N, N> {
  public:
   
  private:
    // Private member functions
    virtual size_type get_index(size_type row, size_type col) const override {
        return row * num_cols() + col;
    }
    // Private data members
    size_type dim_;
};

}
